
import { Component, OnInit,} from '@angular/core';
import {MatDialog, } from '@angular/material';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AdmittedDialogComponent } from './admitted-dialog/admitted-dialog.component';
//import { LookupCodeComponent } from './lookup-code/lookup-code.component';
import {LookupCodeComponent} from './../../../../academic/enrollment-manager/lookup-code/lookup-code.component';
import { SharedServicesService } from './../../../shared-services.service';


@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit {

  codeNo = '';
  subjectID;
  descTitle;
  lecUnits;
  labUnits;
  instructor;

  officialEnrollees;
  admittedStuds;
  wpStuds;


  // day
  // time
  // room
  // instructor

  showContentCollege = false;
  dataLoad = true;
  codesummaryArr
  classlistArr
  admittedstudsArr
  wpStudsArr=[];


  CNum = null;
  lastTen

  newCodeListArr

  employeeCtrl='';
  noclasslist = false;
  pic=[]
  newCodeScheduleArr
  generatetemp=false

  //codeenable = false;
  arraysubjects=[];
  //codeno='';

  constructor(private excelService:SharedServicesService,public dialog: MatDialog,private domSanitizer: DomSanitizer,public global: GlobalService,private api: ApiService) { }
  departments2=[]
  departments=[]
  dept=''
  ngOnInit() {
     /*  for (var i = 0; i < this.global.departments.length; ++i) {
          for (var i2 = 0; i2 < this.global.viewdomain.length; ++i2) {
              if (this.global.departments[i].departmentId==this.global.viewdomain[i2]) {
                  this.departments2.push(this.global.departments[i])
              }
          }
      }
      if (this.global.domain=="ELEMENTARY") {
          for (var i = 0; i < this.departments2.length; ++i) {
              if (this.departments2[i].departmentCode=="ELEM") {
                  this.departments.push(this.departments2[i])
              }
          }
      }
      if (this.global.domain=="HIGHSCHOOL") {
          for (var i = 0; i < this.departments2.length; ++i) {
              if (this.departments2[i].departmentCode=="HS") {
                  this.departments.push(this.departments2[i])
              }
          }
      }
      if (this.global.domain=="GRADUATE SCHOOL") {
          for (var i = 0; i < this.departments2.length; ++i) {
              if (this.departments2[i].departmentCode=="GS") {
                  this.departments.push(this.departments2[i])
              }
          }
      }
      if (this.global.domain=="COLLEGE") {
          for (var i = 0; i < this.departments2.length; ++i) {
              if (this.departments2[i].departmentCode!="ELEM"&&this.departments2[i].departmentCode!="HS"&&this.departments2[i].departmentCode!="GS") {
                  this.departments.push(this.departments2[i])
              }
          }
      }
    if(this.departments2[0]!=undefined)
      this.dept = this.departments2[0].departmentId
    else
       this.dept = '' */
  }


  presentCodeNum = ''
  lastNewCodeNum = '';

  presentDay = ''
  lastNewDay = '';

  presentTime = ''
  lastNewTime = '';

  presentSchedCodeNum = ''
  lastNewSchedCodeNum = '';

  presentSchedDay = ''
  lastNewSchedDay = '';

  moduleID='ReportsFacultyClassList';

  openDialog(): void {
    const dialogRef = this.dialog.open(LookupCodeComponent, {
        width: '95%', disableClose: false
        //,data:this.arraysubjects
        ,data: { dataSource: this.arraysubjects, moduleID: this.moduleID},
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result==undefined) {
          // code...
        }else
        if (result.see=='success') {
          this.CNum=result.result;
          this.getCodeSummary();
        }
      });

    }

  getCodeSummary(){
    //console.log(this.dept)
    this.global.swalLoading('')
    this.getAdmitted(this.CNum)
    this.generatetemp=true
    this.api.getReportTeacherCodeSummaryNoDept(this.global.syear,this.CNum)
      .map(response => response.json())
      .subscribe(res => {
        //console.log(res.data);
        this.codesummaryArr=get_unique_array_values(res.data);
        if(this.codesummaryArr.length>0){
                    this.noclasslist = false;
                    this.showContentCollege = true;
                    this.subjectID = res.data[0].subjectId;
                    this.descTitle = res.data[0].subjectTitle;
                    this.lecUnits = res.data[0].lecUnits;
                    this.labUnits = res.data[0].labUnits;
                    this.codeNo = res.data[0].codeNo
                    this.officialEnrollees = res.data[0].oe;
                    this.instructor=res.data[0].instructor;
                  }else{
                    this.noclasslist = true;
                    this.showContentCollege = false;

                  }
                  
          this.api.getReportTeacherClassList(this.global.syear,this.CNum)
              .map(response => response.json())
              .subscribe(res => {

                  //get Withdrawm-With-Permission count and Array
                  if(res.data.length > 0)
                  {
                    this.wpStuds=0;
                   for(var i = 0; i < res.data.length; i++)
                   {
                      if(res.data[i].enrollmentStatusDesc=='Withdrawn with Permission')
                      {
                        this.wpStuds=this.wpStuds+1;
                        this.wpStudsArr.push(res.data[i]);
                      }
                   }
                  }

                  this.generatetemp=false
                  this.classlistArr = res.data
                  this.global.swalClose();
                  this.pic = []
                  for (var i = 0; i < res.data.length; ++i) {
                    this.api.getReportTeacherIDInfo(res.data[i].idNumber)
                    .map(response => response.json())
                    .subscribe(res => {
                        if(res.data!=null){
                          this.pic.push({
                              picture:this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+res.data.idPicture),
                              idNumber: res.data.idNumber
                            }
                          );
                        }
                        else{
                          this.pic.push({
                              picture:null,
                              idNumber: res.data.idNumber
                            }
                          );
                        }
                      },Error=>{
                        this.generatetemp=false
                        console.log(Error);
                        this.global.swalAlert("Forbidden!","API: ReportTeacher/IDInfo/","error");
                        //console.log(Error)
                    });
                  }
                },Error=>{
                   this.global.swalAlertError(Error);
              });
         },Error=>{
             this.global.swalAlertError(Error);
        });


      function get_unique_array_values(array){
        const uniqueArray = array.filter((value, index) => {
          const _value = JSON.stringify(value);
          return index === array.findIndex(obj => {
            return JSON.stringify(obj) === _value;
          });
        });
        return uniqueArray;
      }
  }


  keyDownFunction(event){
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.CNum != '') {
        this.getCodeSummary();
      }
    }
  }


  getAdmitted(code){
    this.api.getReportTeacherAdmittedInCode(this.CNum,this.global.syear)
        .map(response => response.json())
        .subscribe(res => {
            this.admittedstudsArr=res.data;
            this.admittedStuds = this.admittedstudsArr.length;

            this.global.swalClose();
          },Error=>{
                  //console.log(Error);
                  this.global.swalAlert("Forbidden!","API: ReportTeacher/AdmittedInCode/","error");
                  //console.log(Error)
        });
  }

  openAdmittedDialog(listToDisplay): void {

    if(listToDisplay=='admitted'){
      const dialogRef = this.dialog.open(AdmittedDialogComponent, {
        width: '900px', disableClose: false,data:{selectedData:this.admittedstudsArr, listToDisplay:listToDisplay}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
        }
      });
    }

    if(listToDisplay=='wp'){
      const dialogRef = this.dialog.open(AdmittedDialogComponent, {
        width: '900px', disableClose: false,data:{selectedData:this.wpStudsArr, listToDisplay:listToDisplay}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
        }
      });
    }
  }

  ctr = 0;
  getLast10(rank): boolean{

    /*if(this.ctr==this.officialEnrollees){

      this.dataLoad = false;
    }else{
      this.ctr+=1;
      this.dataLoad = false;
    }*/
    this.lastTen = this.officialEnrollees-9;
    //console.log('Start of Last 10: '+this.lastTen)

    if(rank>=this.lastTen)
      return true
    else
      return false
  }

  getimage(x){
      for (var i = 0; i < this.pic.length; ++i) {
        if (this.pic[i].idNumber==x) {
          if(this.pic[i].picture != null)
            return this.pic[i].picture
        }
      }
      return 'assets/noimage.jpg'

  }



  clear(){
      this.showContentCollege = false;
  }


  exportAsXLSX():void {
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc!='Withdrawn with Permission'){
        arr.push(
          {
            'ID NUMBER':this.classlistArr[i].idNumber,
            "STUDENT'S NAME":this.classlistArr[i].fullName,
            GENDER:this.classlistArr[i].gender,
            'CRSE & YR':this.classlistArr[i].courseYr,
            "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,

          }
        )
      }
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateExcel(arr,CLData, this.codesummaryArr[0].instructor,'officiallyEnrolled');
  }

  exportWPAsXLSX():void {
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc=='Withdrawn with Permission'){
          arr.push(
            {
              'ID NUMBER':this.classlistArr[i].idNumber,
              "STUDENT'S NAME":this.classlistArr[i].fullName,
              GENDER:this.classlistArr[i].gender,
              'CRSE & YR':this.classlistArr[i].courseYr,
              "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,

            }
          )
        }
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateExcel(arr,CLData, this.codesummaryArr[0].instructor,'WP');
  }

  exportAdmittedAsXLSX():void{
    var arr = []
    for (var i = 0; i < this.admittedstudsArr.length; ++i) {
      arr.push(
      {
        'ID NUMBER':this.admittedstudsArr[i].idNumber,
        "STUDENT'S NAME":this.admittedstudsArr[i].fullName,
        GENDER:this.admittedstudsArr[i].gender,
        'CRSE & YR':this.admittedstudsArr[i].course+' '+this.admittedstudsArr[i].yearOrGradeLevel,
        "LEARNING MODALITY":this.admittedstudsArr[i].preferredLearningModality,
      }
        )
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateAdmittedExcel(arr,CLData, this.codesummaryArr[0].instructor);
  }

  exportAllAsXLSX():void{
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc!='Withdrawn with Permission'){
        arr.push(
          {
            'ID NUMBER':this.classlistArr[i].idNumber,
            "STUDENT'S NAME":this.classlistArr[i].fullName,
            GENDER:this.classlistArr[i].gender,
            'CRSE & YR':this.classlistArr[i].courseYr,
            "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,
          }
        )
      }
    }

    var arr2 = []
    for (var i = 0; i < this.admittedstudsArr.length; ++i) {
      arr2.push(
      {
        'ID NUMBER':this.admittedstudsArr[i].idNumber,
        "STUDENT'S NAME":this.admittedstudsArr[i].fullName,
        GENDER:this.admittedstudsArr[i].gender,
        'CRSE & YR':this.admittedstudsArr[i].course+' '+this.admittedstudsArr[i].yearOrGradeLevel,
        "LEARNING MODALITY":this.admittedstudsArr[i].preferredLearningModality,
      }
        )
    }

    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })

   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateAllExcel(arr,arr2,CLData, this.codesummaryArr[0].instructor);
  }



}
