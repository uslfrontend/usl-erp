import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../../global.service';
import { ApiService } from './../../../../../api.service';

import {ExcelService} from './../../../../../academic/curriculum/excel.service';

@Component({
  selector: 'app-college-preenrolmentstat-course',
  templateUrl: './college-preenrolmentstat-course.component.html',
  styleUrls: ['./college-preenrolmentstat-course.component.css']
})
export class CollegePreenrolmentstatCourseComponent implements OnInit {
  option=''
  array=[]
  constructor(private excelService:ExcelService,public global: GlobalService,private api: ApiService,) { }

  ngOnInit() {
  }
  
  loaddata(){
  	this.array=undefined
    this.api.getReportSummaryPreEnrollmentStatisticsCourse(this.global.syear,this.option)
        .map(response => response.json())
        .subscribe(res => {
          this.array=res.data
        },Error=>{
          this.global.swalAlertError(Error);
        });
  }

  exportAsXLSX():void {
    var x=[]
    for (var i = 0; i < this.array.length; ++i) {
      x.push({
        'Course Code':this.array[i].courseCode,
        'Previous Not Verified':this.array[i].previousPreEnrolled_NotVerifiedOnly,
        'Previous Verified':this.array[i].previousPreEnrolled_VerifiedOnly,
        'Previous All':this.array[i].previousPreEnrolled_All,
        'Current Not Verified':this.array[i].currentPreEnrolled_NotVerifiedOnly,
        'Current Verified':this.array[i].currentPreEnrolled_VerifiedOnly,
        'Current All':this.array[i].currentPreEnrolled_All,
      })
    }
   this.excelService.exportAsExcelFile(x, 'Pre-enrollment_Summary_ByCourse_'+this.global.syear);
  }
}
