import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolgradfromComponent } from './schoolgradfrom.component';

describe('SchoolgradfromComponent', () => {
  let component: SchoolgradfromComponent;
  let fixture: ComponentFixture<SchoolgradfromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolgradfromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolgradfromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
