import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../global.service';
import { ApiService } from '../../../api.service';
import * as ExcelJS from "exceljs/dist/exceljs"
@Component({
  selector: 'app-schoolgradfrom',
  templateUrl: './schoolgradfrom.component.html',
  styleUrls: ['./schoolgradfrom.component.css']
})
export class SchoolgradfromComponent implements OnInit {
  schoolYear = this.global.syear
  level = this.global.domain
  StudentsRequirementList = [];



  isLoading: boolean = true;


  currentPage: number = 1;
  itemsPerPage: number = 10; // You can adjust this number based on your preference


  filterText: string = '';

  idnumber = ''
  fullname = ''
  gender = ''
  yearOrGradeLevel = ''
  companyName = ''
  dateofbirth = ''
  course = ''

  filteredStudentsList = []; // Initialize an empty array for the filtered list

  configList: any
  ctr = 0

  yearlevel = []
  coursee

  newgradelevel
  updatedYearLevel
  constructor(public global: GlobalService, private api: ApiService) {
    this.configList = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  pageChangedList(event) {
    this.configList.currentPage = event;
  }

  clearFilter() {
    this.idnumber = ''
    this.fullname = ''
    this.gender = ''
    this.yearOrGradeLevel = ''
    this.companyName = ''
    this.dateofbirth = ''
    this.course = ''
    // this.filteredStudentsList
    this.filterall(event)
  }

  ngOnInit() {
    if (this.level == "HIGHSCHOOL") {
      this.level = 'highschool'
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    if (this.level == 'ELEMENTARY') {
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    this.isLoading = true; // Start loading

    // this.syear()
    // console.log(this.global.syear)
    // console.log(this.global.domain)


    this.api.getSchoolGradfromChecklist(this.schoolYear, this.level)
      .map(response => response.json())
      .subscribe(res => {
        this.StudentsRequirementList = res.data;
        this.filteredStudentsList = res.data

        // if (this.level === 'highschool') {
        //   for (var y in this.filteredStudentsList) {
        //     if (this.filteredStudentsList[y].yearOrGradeLevel) {
        //       this.filteredStudentsList[y].yearOrGradeLevel = parseInt(this.filteredStudentsList[y].yearOrGradeLevel) + 6;
        //     }
        //   }
        // }

        if (this.level === 'highschool') {
          for (var y in this.filteredStudentsList) {
            if (this.filteredStudentsList[y].yearOrGradeLevel) {
              const currentValue = parseInt(this.filteredStudentsList[y].yearOrGradeLevel);
              if (!isNaN(currentValue)) { // Check if it's a valid number
                this.filteredStudentsList[y].yearOrGradeLevel = (currentValue + 6).toString();
              }
            }
          }
        }
        

        // this.form137A = this.StudentsRequirementList.form137A;
        // this.form138 = this.StudentsRequirementList.form138;
        // this.nso = this.StudentsRequirementList.nso;

        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].yearOrGradeLevel !== null) {
            unique.add(res.data[x].yearOrGradeLevel);
          }
        }
        this.yearlevel = Array.from(unique);


        // Assuming this.yearlevel is your array
        this.updatedYearLevel = this.yearlevel.map(element => element + 6);

        // console.log(this.yearlevel)
        // console.log('List: ', this.gradeLevels);

        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].course !== null) {
            unique.add(res.data[x].course);
          }
        }
        this.coursee = Array.from(unique);
        // console.log('List: ', this.gradeLevels);

        // console.table('List: ', this.StudentsRequirementList);

        this.isLoading = false; // Data is fetched, loading finished
      });
  }
  filterall(event: any): void {
    // console.log(this.civilStatus);
    this.filteredStudentsList = this.StudentsRequirementList.filter(student => {
      // console.log(employee.civilStatus, employee.idnumber);
      return (
        (this.idnumber === '' || (student.idNumber && student.idNumber.toLowerCase().includes(this.idnumber.toLowerCase()))) &&
        (this.fullname === '' || (student.fullName && student.fullName.toLowerCase().includes(this.fullname.toLowerCase()))) &&
        (this.gender === '' || (student.gender && student.gender.toLowerCase().includes(this.gender.toLowerCase()))) &&
        (this.yearOrGradeLevel === '' || (student.yearOrGradeLevel && student.yearOrGradeLevel.toLowerCase().includes(this.yearOrGradeLevel.toLowerCase()))) &&
        (this.companyName === '' || (student.companyName && student.companyName.toLowerCase().includes(this.companyName.toLowerCase()))) &&
        (this.dateofbirth === '' || (student.dateofbirth && student.dateofbirth.toLowerCase().includes(this.dateofbirth.toLowerCase()))) &&
        (this.course === '' || (student.course && student.course.toLowerCase().includes(this.course.toLowerCase())))
      );
     
    });
    console.log(this.yearOrGradeLevel)
    // Now 'this.filteredList' holds the filtered results
    // console.log(this.filteredList);
  }
  // checkAssessmentStatus(form137A, form138, nso) {
  //   let result = 'N/A';

  //   if (form137A == 1) {
  //     result = '✓';
  //   }

  //   if (form138 == 1) {
  //     result = '✓';
  //   }

  //   if (nso == 1) {
  //     result = '✓';
  //   }

  //   return result;
  // }

  exportExcel() {

    const date = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .reverse()
      .join("/");
    // console.log(date);
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("My Sheet");

    // await worksheet.protect('CiCT#2020') // locks the whole excel file

    // Header labels
    const headerLabels = [
      'ID Number',
      'Full Name',
      'Gender',
      'Level',
      'School',
      'Birth Date',
      'Course/Section',
    ];

    // Add header row
    const headerRow = worksheet.addRow(headerLabels);
    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    worksheet.getColumn(1).width = 13;
    worksheet.getColumn(2).width = 48;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 7;
    worksheet.getColumn(5).width = 35;
    worksheet.getColumn(6).width = 13;
    worksheet.getColumn(7).width = 20;
    // worksheet.getColumn(8).width = 15;


    // for (var x = 2; x < 5; x++) {
    //   worksheet.getColumn(x).width = 48;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(5).width = 10;
    // worksheet.getColumn(6).width = 10;

    // for (var x = 7; x < 11; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(11).width = 10;

    // for (var x = 12; x < 16; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   worksheet.getColumn(x).alignment = { wrapText: true };
    // }

    var dataRow: any
    var form137A: any
    var form138: any
    var nso: any
    for (var x = 0; x < this.filteredStudentsList.length; x++) {

      // if(this.StudentsRequirementList[x].form137A == '1'){
      //   form137A = '✓'
      // }
      // else{
      //   form137A = ''
      // }

      // if(this.StudentsRequirementList[x].form138 == '1'){
      //   form138 = '✓'
      // }
      // else{
      //   form138 = ''
      // }

      // if(this.StudentsRequirementList[x].nso == '1'){
      //   nso = '✓'
      // }
      // else{
      //   nso = ''
      // }

      dataRow = worksheet.addRow([
        this.filteredStudentsList[x].idNumber,
        this.filteredStudentsList[x].fullName,
        this.filteredStudentsList[x].gender,
        this.filteredStudentsList[x].yearOrGradeLevel,
        this.filteredStudentsList[x].companyName,
        this.filteredStudentsList[x].dateofbirth,
        this.filteredStudentsList[x].course


      ])
      dataRow.font = { size: 12 };
      dataRow.alignment = { vertical: 'middle', wrapText: true };;

    }


    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    workbook.xlsx.writeBuffer().then((data: any) => {
      // console.log("buffer");
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = url;
      a.download = "SchoolGradFrom_" + this.global.domain + ' ' + this.global.displaysem + ' ' + this.global.displayyear;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
      // Swal.fire(
      //   'Download Succes!',
      //   '',
      //   'success')
      this.global.swalSuccess('Download Success!')
    });

  }
}
