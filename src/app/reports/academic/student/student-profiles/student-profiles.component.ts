import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-student-profiles',
  templateUrl: './student-profiles.component.html',
  styleUrls: ['./student-profiles.component.css']
})
export class StudentProfilesComponent implements OnInit {

  proglevel;
  array=null;
  arrayAge=null;
  arrayGender=null;
  arrayReligion=null;
  arrayHomePlace=null;
  arraySHS=null
  arrayJHS=null
  sy;
  profile='All';
  accessDeniedMessage='You are not allowed to access this report.';
  accessDeniedAllMessage='You are not allowed to access one or more of these reports.';

  constructor(public global: GlobalService,private api: ApiService) { }

  ngOnInit() {
    this.sy=this.global.syear;
    this.proglevel=this.global.domain;
    //console.log(this.sy);
    //console.log(this.global.domain);
  }

  generate() {     
    switch(this.profile){
      case 'All':{    
             
        this.api.getReportSummaryStudentProfileAge(this.sy,this.proglevel)        
        .map(response => response.json())
        .subscribe(res => {
            this.arrayAge=[];            
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayAge=res.data;     
            //console.log(this.array[0]);                  
        },Error=>{
          this.arrayAge=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
        });  
               
        this.api.getReportSummaryStudentProfileGender(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayGender=[];            
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayGender=res.data;                      
            //console.log(this.array[1]);                   
        },Error=>{
          this.arrayGender=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
        });        
        
        this.api.getReportSummaryStudentProfileReligion(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayReligion=[];  
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayReligion=res.data;
            //console.log(this.array[2]);
        },Error=>{
          this.arrayReligion=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
        });
        
        this.api.getReportSummaryStudentProfileHomePlace(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayHomePlace=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayHomePlace=res.data;
            //console.log(this.array[3]);
        },Error=>{
          this.arrayHomePlace=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
        }); 

      if (this.global.domain==='COLLEGE') {       
          this.api.getReportSummaryStudentProfileCollegeSHSGraduatedFrom(this.sy)
          .map(response => response.json())
          .subscribe(res => {
              this.arraySHS=[];
            if (res.data==null) {
              this.global.swalAlert(res.message,'','warning')
            }else
              this.arraySHS=res.data;
              //console.log(this.array[4]);
          },Error=>{
            this.arraySHS;
            this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
          }); 
      }
   
      if (this.global.domain==='HIGHSCHOOL') {       
        this.api.getReportSummaryStudentProfileSHSJHSGraduatedFrom(this.sy)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayJHS=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayJHS=res.data;
            //console.log(this.array);
        },Error=>{
          this.arrayJHS=[];
          ///this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedAllMessage,'','warning')
        }); 
     }               

        break;
      }
      case 'Age':{           
        this.api.getReportSummaryStudentProfileAge(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayAge=[];            
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayAge=res.data;       
            //console.log(this.arrayAge);          
        },Error=>{
          this.arrayAge=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      case 'Gender':{
        this.api.getReportSummaryStudentProfileGender(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayGender=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayGender=res.data;
            //console.log(this.array);
        },Error=>{
          this.arrayGender=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      case 'Religion':{
        this.api.getReportSummaryStudentProfileReligion(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayReligion=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayReligion=res.data;
            //console.log(this.array);
        },Error=>{
          this.arrayReligion=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      case 'HomePlace':{
        this.api.getReportSummaryStudentProfileHomePlace(this.sy,this.proglevel)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayHomePlace=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayHomePlace=res.data;
            //console.log(this.array);
        },Error=>{
          this.arrayHomePlace=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      case 'SeniorHighSchoolGraduatedFrom':{
        this.api.getReportSummaryStudentProfileCollegeSHSGraduatedFrom(this.sy)
        .map(response => response.json())
        .subscribe(res => {
            this.arraySHS=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arraySHS=res.data;
            //console.log(this.array);
        },Error=>{
          this.arraySHS=[];
          //this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      case 'Senior_JuniorHighSchoolGraduatedFrom':{
        this.api.getReportSummaryStudentProfileSHSJHSGraduatedFrom(this.sy)
        .map(response => response.json())
        .subscribe(res => {
            this.arrayJHS=[];
          if (res.data==null) {
            this.global.swalAlert(res.message,'','warning')
          }else
            this.arrayJHS=res.data;
            //console.log(this.array);
        },Error=>{
          this.arrayJHS=[];
          ///this.global.swalAlertError()
          this.global.swalAlert(this.accessDeniedMessage,'','warning')
        });        
        break;
      }
      default:{
        this.array=[];        
        break;
      }
    }
  }

  exportexcel(elementId:string){
    var fileName= 'StudentProfile'+this.profile+'('+this.proglevel+')-'+this.global.syear+'.xlsx'
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    if (elementId != 'All')
    {
      /* table id is passed over here */   
      let element = document.getElementById(elementId); 
      const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

      /* generate workbook and add the worksheet */
      //const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    }
    else{
      /* table id is passed over here */   
      let elementAge = document.getElementById('Age'); 
      let elementGender = document.getElementById('Gender'); 
      let elementReligion = document.getElementById('Religion'); 
      let elementHomePlace = document.getElementById('HomePlace'); 

      if(this.global.domain==='COLLEGE'){
        let elementSHS = document.getElementById('SeniorHighSchoolGraduatedFrom'); 

        const wsSHS: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementSHS);
        const wsAge: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementAge);      
        const wsGender: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementGender);
        const wsReligion: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementReligion);
        const wsHomePlace: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementHomePlace);
      
        XLSX.utils.book_append_sheet(wb, wsAge, 'Age');
        XLSX.utils.book_append_sheet(wb, wsGender, 'Gender');
        XLSX.utils.book_append_sheet(wb, wsReligion, 'Religion');
        XLSX.utils.book_append_sheet(wb, wsHomePlace, 'HomePlace');      
        XLSX.utils.book_append_sheet(wb, wsSHS, 'SHSGraduatedFrom');
      }

      if(this.global.domain==='HIGHSCHOOL') {
        let elementJHS = document.getElementById('Senior_JuniorHighSchoolGraduatedFrom'); 

        const wsJHS: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementJHS);
        const wsAge: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementAge);      
        const wsGender: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementGender);
        const wsReligion: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementReligion);
        const wsHomePlace: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementHomePlace);

        XLSX.utils.book_append_sheet(wb, wsAge, 'Age');
        XLSX.utils.book_append_sheet(wb, wsGender, 'Gender');
        XLSX.utils.book_append_sheet(wb, wsReligion, 'Religion');
        XLSX.utils.book_append_sheet(wb, wsHomePlace, 'HomePlace');            
        XLSX.utils.book_append_sheet(wb, wsJHS, 'JHSGraduatedFrom')

      }

      if(this.global.domain==='ELEMENTARY'||this.global.domain==='GRADUATE SCHOOL') {        
        const wsAge: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementAge);      
        const wsGender: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementGender);
        const wsReligion: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementReligion);
        const wsHomePlace: XLSX.WorkSheet =XLSX.utils.table_to_sheet(elementHomePlace);

        XLSX.utils.book_append_sheet(wb, wsAge, 'Age');
        XLSX.utils.book_append_sheet(wb, wsGender, 'Gender');
        XLSX.utils.book_append_sheet(wb, wsReligion, 'Religion');
        XLSX.utils.book_append_sheet(wb, wsHomePlace, 'HomePlace');                    
      }          
    }


    /* save to file */
    XLSX.writeFile(wb, fileName);	
  }

  onChangeMatSelect(){
    this.array=null;
    this.arrayAge=null;
    this.arrayGender=null;
    this.arrayReligion=null;
    this.arrayHomePlace=null;
    this.arraySHS=null
    this.arrayJHS=null   
  }


}
