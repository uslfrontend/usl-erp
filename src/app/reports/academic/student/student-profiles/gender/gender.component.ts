import { Component, OnInit } from '@angular/core';
import { StudentProfilesComponent } from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-gender',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.css']
})
export class GenderComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }


  setBGColor(deptName): object {
    switch (deptName) {
      case 'School of Accountancy, Business and Hospitality': {
        return { background: 'orange', color: 'white' }
        break;
      }
      case 'School of Education, Arts and Sciences': {
        return { background: 'blue', color: 'white' }
        break;
      }
      case 'School of Engineering, Architecture, Interior Design and Information Technology Education': {
        return { background: 'maroon', color: 'white' }
        break;
      }
      case 'School of Health and Allied Sciences': {
        return { background: 'green', color: 'white' }
        break;
      }
      default: {
        return { background: 'white', color: 'black' }
        break;
      }
    }
  }

  ngOnInit() {
  }

  /* getTotal(x){
    var total1=0
    var total2=0
    var total3=0
    var total4=0
    var total5=0
    var total6=0
    var total7=0
    var total8=0
    var total9=0
    var total10=0
    var total11=0
    var total12=0
    var total13=0
    var total14=0
    //console.log(this.studentProfiles.arrayGender) ;
    for (var i = 0; i < this.studentProfiles.arrayGender.length; ++i) {
      total1 = this.studentProfiles.arrayGender[i].mx+total1
      total2 = this.studentProfiles.arrayGender[i].fx+total2
      total3 = this.studentProfiles.arrayGender[i].m1+total3
      total4 = this.studentProfiles.arrayGender[i].f1+total4
      total5 = this.studentProfiles.arrayGender[i].m2+total5
      total6 = this.studentProfiles.arrayGender[i].f2+total6
      total7 = this.studentProfiles.arrayGender[i].m3+total7
      total8 = this.studentProfiles.arrayGender[i].f3+total8
      total9 = this.studentProfiles.arrayGender[i].m4+total9
      total10 = this.studentProfiles.arrayGender[i].f4+total10
      total11 = this.studentProfiles.arrayGender[i].m5+total11
      total12 = this.studentProfiles.arrayGender[i].f5+total12
      total13 = this.studentProfiles.arrayGender[i].m6+total13
      total14 = this.studentProfiles.arrayGender[i].f6+total14
    }
    switch(x){
      case 1:{
        return total1;
        break;
      }
      case 2:{
        return total2;
        break;
      }
      case 3:{
        return total3;
        break;
      }
      case 4:{
        return total4;
        break;
      }
      case 5:{
        return total5;
        break;
      }
      case 6:{
        return total6;
        break;
      }
      case 7:{
        return total7;
        break;
      }
      case 8:{
        return total8;
        break;
      }
      case 9:{
        return total9;
        break;
      }
      case 10:{
        return total10;
        break;
      }
      case 11:{
        return total11;
        break;
      }
      case 12:{
        return total12;
        break;
      }
      case 13:{
        return total13;
        break;
      }
      case 14:{
        return total14;
        break;
      }
    }
  }  */

}
