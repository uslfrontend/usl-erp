import { Component, OnInit } from '@angular/core';
import {StudentProfilesComponent} from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-religion',
  templateUrl: './religion.component.html',
  styleUrls: ['./religion.component.css']
})
export class ReligionComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }

  ngOnInit() {
  }

}
