import { Component, OnInit } from '@angular/core';
import { StudentProfilesComponent } from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-junior-highschool-graduated-from',
  templateUrl: './junior-highschool-graduated-from.component.html',
  styleUrls: ['./junior-highschool-graduated-from.component.css']
})
export class JuniorHighschoolGraduatedFromComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }

  ngOnInit() {
  }

}
