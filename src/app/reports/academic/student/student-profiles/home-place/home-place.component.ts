import { Component, OnInit } from '@angular/core';
import { StudentProfilesComponent } from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-home-place',
  templateUrl: './home-place.component.html',
  styleUrls: ['./home-place.component.css']
})
export class HomePlaceComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }

  ngOnInit() {
  }

}
