import { Component, OnInit } from '@angular/core';
import { StudentProfilesComponent } from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-age',
  templateUrl: './age.component.html',
  styleUrls: ['./age.component.css']
})
export class AgeComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }

  ngOnInit() {
    
  }

}
