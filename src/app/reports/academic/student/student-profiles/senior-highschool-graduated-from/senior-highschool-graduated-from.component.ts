import { Component, OnInit } from '@angular/core';
import { StudentProfilesComponent } from './../../student-profiles/student-profiles.component';

@Component({
  selector: 'app-senior-highschool-graduated-from',
  templateUrl: './senior-highschool-graduated-from.component.html',
  styleUrls: ['./senior-highschool-graduated-from.component.css']
})
export class SeniorHighschoolGraduatedFromComponent implements OnInit {

  constructor(public studentProfiles: StudentProfilesComponent) { }

  ngOnInit() {
  }

}
