import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import * as ExcelJS from "exceljs/dist/exceljs"
import * as fs from 'file-saver';

@Component({
  selector: 'app-student-academic-achiever',
  templateUrl: './student-academic-achiever.component.html',
  styleUrls: ['./student-academic-achiever.component.css']
})
export class StudentAcademicAchieverComponent implements OnInit {

  tabbing = 0;
  array;
  ctr = null;
  imageIDPic: any = 'assets/noimage.jpg';
  IsWait = true;
  selectedDeptCode = 'SHS';
  title = 'angular-export-to-excel';
  subTitle = '';
  dataForExcel;
  targetCell = 'A1';
  finalDepCode = 'SHAS';
  arrayIdPic;
  deptID;
  retrievedIDPicSBAA = false;
  retrievedIDPicSHAS = false;
  retrievedIDPicSEAITE = false;
  retrievedIDPicSEAS = false;
  deptCodeVar;
  submitted = false;
  departments = [];
  departments2 = []

  constructor(public global: GlobalService, private api: ApiService, public domSanitizer: DomSanitizer) { }

  ngOnInit() {
     this.loaddata();
    for (var i = 0; i < this.global.departments.length; ++i) {
      for (var i2 = 0; i2 < this.global.viewdomain.length; ++i2) {
        if (this.global.departments[i].departmentId == this.global.viewdomain[i2]) {
          this.departments.push(this.global.departments[i])
        }
      }
    }
    if (this.global.domain == "COLLEGE") {
      for (var i = 0; i < this.departments.length; ++i) {
        if (this.departments[i].departmentCode != "ELEM" &&
          this.departments[i].departmentCode != "HS" &&
          this.departments[i].departmentCode != "GS" &&
          this.departments[i].departmentCode != "DKLC" &&
          this.departments[i].departmentCode != "SHVED" &&
          this.departments[i].departmentCode != "SICS"&&
          this.departments[i].departmentCode != "HSS"
        ) {
          this.departments2.push(this.departments[i])
        }
      }
    }
  }

  matSelectonChange(){
    this.LoadIDPictures(this.deptCodeVar);
  }

  setDeptCode(dept) {
    switch (dept) {
      case 'SEAID': {
        return 'SEAITE'
        break;
      }
      case 'SBAA': {
        return 'SABH'
        break;
      }
      case 'SEAS': {
        return 'SEAS'
        break;
      }
      case 'SHS': {
        return 'SHAS'
        break;
      }
      default: {
        return dept
        break;
      }
    }
  }

  getLength(dept, remark) {
    this.ctr = 0;
    if (this.array !== null) {
      for (var i = 0; i < this.array.length; ++i) {
        if (this.array[i].departmentCode === dept && this.array[i].remarks === remark) {
          this.ctr++;
        }
      }
      return this.ctr;
    }
    else {
      return 0;
    }
  }

  loaddata() {
    this.array = undefined;
    //this.submitted=true;
    this.global.swalLoading('Retrieving List of Academic Achievers');
    this.api.getReportSummaryAcademicAchievers(this.global.syear,1)
      .map(response => response.json())
      .subscribe(res => {
        this.array = [];
        this.array = res.data;
        console.log(this.array)
        this.IsWait = false;
        this.global.swalClose();
      }, Error => {
        this.array = [];
        this.global.swalAlertError(Error);
      });
  }

  getIdPic(dept){
    var that = this;
    function CallbackFunctionToFindDepartmentByCode(department_Code) {
      return department_Code.departmentCode === that.selectedDeptCode;
    }

    if(this.array){
      if(!this.array.find(CallbackFunctionToFindDepartmentByCode)){
        this.global.swalAlert('','Selected department has no data for academic achievers on the selected school year.','info');
      }else{
        this.global.swalLoading('Retrieving ID Pictures ('+this.finalDepCode+')');
        this.api.getReportSummaryAcademicAchieversWithPic(this.global.syear,dept,0)
          .map(response => response.json())
          .subscribe(res => {
            this.arrayIdPic = [];
            this.arrayIdPic = res.data;
            switch (dept) {
              case '0006': {
                this.retrievedIDPicSHAS = true;
                break;
              }
              case '0002': {
                this.retrievedIDPicSBAA = true;
                break;
              }
              case '0003': {
                this.retrievedIDPicSEAITE = true;
                break;
              }
              case '0005': {
                this.retrievedIDPicSEAS = true;
                break;
              }
            }
              for(var i=0; i < this.arrayIdPic.length; i++){
                if(this.arrayIdPic[i].idPicture!=null){
                    //this.arrayIdPic[i].idPicture=this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + this.arrayIdPic[i].idPicture);
                    for(var x=0; x < this.array.length; x++){
                      if(this.array[x].idnumber===this.arrayIdPic[i].idnumber)
                      {
                        this.array[x].idPicture=this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + this.arrayIdPic[i].idPicture);
                      }
                    }
                }
              }
              this.IsWait = false;
              this.global.swalClose();
              this.submitted=true;

          }, Error => {
            this.array.idPicture = [];
            this.global.swalAlertError(Error);
          });
      }
    }else{
      this.global.swalAlert('','Selected department has no data for academic achievers on the selected school year.','info');
    }

  }

  LoadIDPictures(dept) {
    switch (dept) {
      case 'SHS': {
        this.selectedDeptCode = 'SHS';
        this.finalDepCode='SHAS';
        this.deptID='0006';
        if(this.retrievedIDPicSHAS==false){
          this.getIdPic(this.deptID);
        }
        break;
      }
      case 'SBAA': {
        this.selectedDeptCode = 'SBAA';
        this.finalDepCode='SABH';
        this.deptID='0002';
        if(this.retrievedIDPicSBAA==false){
          this.getIdPic(this.deptID);
        }
        break;
      }
      case 'SEAID': {
        this.selectedDeptCode = 'SEAID';
        this.finalDepCode='SEAITE'
        this.deptID='0003';
        if(this.retrievedIDPicSEAITE==false){
          this.getIdPic(this.deptID);
        }
        break;
      }
      case 'SEAS': {
        this.selectedDeptCode = 'SEAS';
        this.finalDepCode='SEAS';
        this.deptID='0005';
        if(this.retrievedIDPicSEAS==false){
          this.getIdPic(this.deptID);
        }
        break;
      }
    }
  }

  setBottomBorderColor(dept) {
    switch (dept) {
      case 'SHS': {
        //return { background: 'green', color: 'white' }
        return 'tdClassSHAS'
        break;
      }
      case 'SBAA': {
        return 'tdClassSABH'
        break;
      }
      case 'SEAID': {
        return 'tdClassSEAITE'
        break;
      }
      case 'SEAS': {
        return 'tdClassSEAS'
        break;
      }
    }
  }

  setExcelTitle() {
    console.log(this.deptCodeVar);
    switch (this.deptCodeVar) {
      case 'SHS': {
        return this.title = 'School of Health and Allied Sciences';
        break;
      }
      case 'SBAA': {
        return this.title = 'School of Accountancy, Business and Hospitality';
        break;
      }
      case 'SEAID': {
        return this.title = 'School of Engineering, Architecture, Interior Design and Information Technology Education';
        break;
      }
      case 'SEAS': {
        return this.title = 'School of Education, Arts and Sciences';
        break;
      }
    }
  }

  exportMyDataToExcel(a) {
    this.dataForExcel = [];
    if (this.array !== null) {
      for (var i = 0; i < this.array.length; ++i) {
        this.dataForExcel.push({
          'ID NUMBER': this.array[i].idnumber,
          'STUDENT\'S NAME': this.array[i].name,
          'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
          'AVERAGE': this.array[i].average,
          'remarks': this.array[i].remarks,
          'deptCode': this.array[i].departmentCode
        });
      }
    }
    var finalTitle = this.setExcelTitle();
    let reportData = {
      title: finalTitle,
      data: this.dataForExcel,
      //headers: Object.keys(this.dataForExcel[0]),
      deptCode: this.selectedDeptCode
    }
    //if(a==='single'){
      this.exportExcel(reportData);
    //}
    // else if(a==='multiple'){
    //   this.exportExcel_XLSX(reportData);
    // }
  }

  /* exportExcel_XLSX(excelData) {
    //declare data
    const title = excelData.title;
    //const header = excelData.headers
    const data = excelData.data;

    //declare and set exact department code
    var departmentCode = excelData.deptCode;
    switch (departmentCode) {
      case 'SHS': {
        departmentCode = 'SHAS'
        break;
      }
      case 'SBAA': {
        departmentCode = 'SABH'
        break;
      }
      case 'SEAID': {
        departmentCode = 'SEAITE'
        break;
      }
      case 'SEAS': {
        departmentCode = 'SEAS'
        break;
      }
    }

    //declare and set array per remark
    var dataFullAcadScho = [];
    var dataHalfAcadScho = [];
    var dataDLPercent = [];
    var dataDL = [];
    if (data != null) {
      for (var i = 0; i < data.length; i++) {
        //full
        if (data[i].deptCode === this.selectedDeptCode && data[i].remarks === 'Full Academic Scholar') {
          dataFullAcadScho.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
        //half
        if (data[i].deptCode === this.selectedDeptCode && data[i].remarks === 'Half Academic Scholar') {
          dataHalfAcadScho.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
        //10% discount
        if (data[i].deptCode === this.selectedDeptCode && data[i].remarks === 'Dean\'s Lister (No Grade Below 85)') {
          dataDLPercent.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
        //dean's lister
        if (data[i].deptCode === this.selectedDeptCode && data[i].remarks === 'Dean\'s Lister') {
          dataDL.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
      }
    }

    //declare filename
    var fileName = 'AcademicAchievers_' + this.finalDepCode + '_' + this.global.displaysem + '_' + this.global.displayyear + '.xlsx';
    //declare workbook
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    //declare column width
    var wscols = [
      { wch: 15 },
      { wch: 40 },
      { wch: 25 },
      { wch: 10 }
    ];


    //declare worksheet then assign data
    const wsFullAcadScho: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataFullAcadScho);
    const wsHalfAcadScho: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataHalfAcadScho);
    const wsDLPercent: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataDLPercent);
    const wsDL: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataDL);

    //set workshet column width
    wsFullAcadScho['!cols'] = wscols;
    wsHalfAcadScho['!cols'] = wscols;
    wsDLPercent['!cols'] = wscols;
    wsDL['!cols'] = wscols;

    //add sheet to workbook
    XLSX.utils.book_append_sheet(wb, wsFullAcadScho, 'Full Academic Scholar');
    XLSX.utils.book_append_sheet(wb, wsHalfAcadScho, 'Half Academic Scholar');
    XLSX.utils.book_append_sheet(wb, wsDLPercent, 'Dean\'s Lister (10% Discount)');
    XLSX.utils.book_append_sheet(wb, wsDL, 'Dean\'s Lister');

    // save to file
    XLSX.writeFile(wb, fileName);
  } */

  exportExcel(excelData) {
    //Title, Header & Data
    const title = excelData.title;
    const header = ['ID NUMBER', 'STUDENT\'S NAME', 'COURSE AND YEAR', 'AVERAGE'];
    const data = excelData.data;

    //arr to pa
    var arr = [];
    if (data != null) {
      for (var i = 0; i < data.length; i++) {
        //full
        if (data[i].deptCode === this.selectedDeptCode) {
          arr.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
      }
    }

    //Create a workbook with a worksheet
    let workbook = new ExcelJS.Workbook();

    //Add Worksheet
    let ws = workbook.addWorksheet(this.finalDepCode);

    //add title row
    this.addRowTitle(ws, title);

    //add header row
    this.addHeaderRow(ws, header);

    /*--Full Academic Scholar--*/
    //add subtitle row
    this.addRowSubTitle(ws, 'Full Academic Scholar', 'A3', 'A3', 'D3')
    //add data
    var arrFull=[];
    arrFull=this.getArr(data,'Full Academic Scholar')
    //write data to worksheet
    this.addData(arrFull, header, ws);

    /*--Half Academic Scholar--*/
    //add subtitle row
    var targetCellLoc=this.getLength(this.selectedDeptCode,'Full Academic Scholar')+4;
    this.addRowSubTitle(ws, 'Half Academic Scholar', 'A'+targetCellLoc, 'A'+targetCellLoc, 'D'+targetCellLoc)
    //add data
    var arrHalf = [];
    arrHalf = this.getArr(data, 'Half Academic Scholar')
    //write data to worksheet
    this.addData(arrHalf, header, ws);

    /*--Dean's Lister (No Grade Below 85)--*/
    //add subtitle row
    var targetCellLoc=this.getLength(this.selectedDeptCode,'Full Academic Scholar')+this.getLength(this.selectedDeptCode,'Half Academic Scholar')+5;
    this.addRowSubTitle(ws, 'Dean\'s Lister (No Grade Below 85)', 'A'+targetCellLoc, 'A'+targetCellLoc, 'D'+targetCellLoc)
    //add data
    var arrDisc = [];
    arrDisc = this.getArr(data, 'Dean\'s Lister (No Grade Below 85)')
    //write data to worksheet
    this.addData(arrDisc, header, ws);

    /*--Dean's Lister--*/
    //add subtitle row
    var targetCellLoc=this.getLength(this.selectedDeptCode,'Full Academic Scholar')
      +this.getLength(this.selectedDeptCode,'Half Academic Scholar')
      +this.getLength(this.selectedDeptCode, 'Dean\'s Lister (No Grade Below 85)')+6;
    this.addRowSubTitle(ws, 'Dean\'s Lister', 'A'+targetCellLoc, 'A'+targetCellLoc, 'D'+targetCellLoc)
    //add data
    var arrDL = [];
    arrDL = this.getArr(data, 'Dean\'s Lister')
    //write data to worksheet
    this.addData(arrDL, header, ws);

    //add Blank Row
    //worksheet.addRow([]);

    //Generate & Save Excel File
    ///*
    var fileName = 'AcademicAchievers_' + this.finalDepCode + '_' + this.global.displaysem + '_' + this.global.displayyear + '.xlsx';
    workbook.xlsx.writeBuffer().then((arr) => {
      let blob = new Blob([arr], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fileName);
    })
    //*/
  }

  getArr(data, remark) {
    var arr = [];
    if (data != null) {
      for (var i = 0; i < data.length; i++) {
        //full
        if (data[i].deptCode === this.selectedDeptCode && data[i].remarks === remark) {
          arr.push({
            'ID NUMBER': this.array[i].idnumber,
            'STUDENT\'S NAME': this.array[i].name,
            'COURSE AND YEAR': this.array[i].course + ' ' + this.array[i].yearorgradelevel,
            'AVERAGE': this.array[i].average,
          })
        }
      }
    }
    return arr;
  }

  addData(arr, header, ws) {
    arr.forEach((element) => {
      let eachRow = [];
      header.forEach((headers) => {
        eachRow.push(element[headers])
      });
      const row = ws.addRow(eachRow);
      row.eachCell((cell, number) => {
        cell.border = {
          top: {
            style: 'thin'
          },
          left: {
            style: 'thin'
          },
          bottom: {
            style: 'thin'
          },
          right: {
            style: 'thin'
          }
        };
      });
    })
  }

  addRowSubTitle(ws, title, targetCell, mergeCellStart, mergeCellEnd) {
    ws.mergeCells(mergeCellStart, mergeCellEnd);
    let titleRow = ws.getCell(targetCell);
    titleRow.value = title
    titleRow.font = {
      name: 'Calibri',
      size: 12,
      //underline: 'single',
      bold: true,
      color: { argb: '262626' }
    }
    titleRow.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'D9D9D9' }
    }
    titleRow.border = {
      top: {
        style: 'thin'
      },
      left: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      }
    }

    titleRow.alignment = { vertical: 'middle', horizontal: 'left' }
  }

  addRowTitle(ws, title) {
    ws.mergeCells('A1', 'D1');
    let titleRow = ws.getCell(this.targetCell);
    titleRow.value = title
    titleRow.font = {
      name: 'Calibri',
      size: 12,
      //underline: 'single',
      bold: true,
      color: { argb: 'FFFFFF' }
    }
    titleRow.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: '006600' }
    }
    titleRow.border = {
      top: {
        style: 'thin'
      },
      left: {
        style: 'thin'
      },
      bottom: {
        style: 'thin'
      },
      right: {
        style: 'thin'
      }
    }

    titleRow.alignment = { vertical: 'middle', horizontal: 'center' }
  }

  addHeaderRow(ws, header) {
    let headerRow = ws.addRow(header);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '000000' },
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 11
      }
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      }
    })
    //set column width
    ws.getColumn(1).width = 15; //idnumber
    ws.getColumn(2).width = 40; //name
    ws.getColumn(3).width = 30; //course and year
    ws.getColumn(4).width = 10; //average
  }

}
