import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../../global.service';
import { ApiService } from '../../../../api.service';
import { FormsModule } from '@angular/forms';
import * as ExcelJS from "exceljs/dist/exceljs"
@Component({
  selector: 'app-requirementlist',
  templateUrl: './requirementlist.component.html',
  styleUrls: ['./requirementlist.component.css']
})
export class RequirementlistComponent implements OnInit {
  schoolYear = this.global.syear
  level = this.global.domain
  StudentsRequirementList = [];



  isLoading: boolean = true;


  currentPage: number = 1;
  itemsPerPage: number = 10; // You can adjust this number based on your preference


  filterText: string = '';
  idnumber = ''
  fullname = ''
  deptname = ''
  course = ''
  gradelevel = ''
  form137A = ''
  form138 = ''
  nso = ''
  filteredStudentsList = []; // Initialize an empty array for the filtered list

  configList: any
  ctr = 0

  dept = []
  courseee = []
  grade = []

 
  constructor(public global: GlobalService, private api: ApiService) {
    this.configList = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  pageChangedList(event) {
    this.configList.currentPage = event;
  }

  ngOnInit() {
    if (this.level == "HIGHSCHOOL") {
      this.level = 'high school'
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    if (this.level == 'ELEMENTARY') {
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    this.isLoading = true; // Start loading

    // this.syear()
    // console.log(this.global.syear)
    // console.log(this.global.domain)


    this.api.getStudentRequirementChecklist(this.schoolYear, this.level)
      .map(response => response.json())
      .subscribe(res => {
        this.StudentsRequirementList = res.data;
        this.filteredStudentsList = res.data
        // this.form137A = this.StudentsRequirementList.form137A;
        // this.form138 = this.StudentsRequirementList.form138;
        // this.nso = this.StudentsRequirementList.nso;
        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].departmentname !== null) {
            unique.add(res.data[x].departmentname);
          }
        }
        this.dept = Array.from(unique);
        // console.log('List: ', this.gradeLevels);


        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].course !== null) {
            unique.add(res.data[x].course);
          }
        }
        this.courseee = Array.from(unique);
        // console.log('List: ', this.gradeLevels);


        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].yearorgradelevel !== null) {
            unique.add(res.data[x].yearorgradelevel);
          }
        }
        this.grade = Array.from(unique).sort();
        // console.log('List: ', this.gradeLevels);

        // console.table('List: ', this.StudentsRequirementList);

        this.isLoading = false; // Data is fetched, loading finished
      });
  }

  clearFilter() {
    this.idnumber = ''
    this.fullname = ''
    this.deptname = ''
    this.course = ''
    this.gradelevel = ''
    this.form137A = ''
    this.form138 = ''
    this.nso = ''
    // this.filteredStudentsList
    this.filterall(event)
  }
  filterall(event: any): void {
    // console.log(this.civilStatus);
    this.filteredStudentsList = this.StudentsRequirementList.filter(student => {
      // console.log(employee.civilStatus, employee.idnumber);
      return (
        (this.idnumber === '' || (student.idnumber && student.idnumber.toLowerCase().includes(this.idnumber.toLowerCase()))) &&
        (this.fullname === '' || (student.fullname && student.fullname.toLowerCase().includes(this.fullname.toLowerCase()))) &&
        (this.deptname === '' || (student.departmentname && student.departmentname.toLowerCase().includes(this.deptname.toLowerCase()))) &&
        (this.course === '' || (student.course && student.course.toLowerCase().includes(this.course.toLowerCase()))) &&
        (this.gradelevel === '' || (student.yearorgradelevel && student.yearorgradelevel.toLowerCase().includes(this.gradelevel.toLowerCase()))) &&
        (this.form137A === '' || (student.form137A && student.form137A.toLowerCase().includes(this.form137A.toLowerCase()))) &&
        (this.form138 === '' || (student.form138 && student.form138.toLowerCase().includes(this.form138.toLowerCase()))) &&
        (this.nso === '' || (student.nso && student.nso.toLowerCase().includes(this.nso.toLowerCase())))
      );
    });

    // Now 'this.filteredList' holds the filtered results
    // console.log(this.filteredList);
  }
  checkAssessmentStatus(form137A, form138, nso) {
    let result = 'N/A';

    if (form137A == 1) {
      result = '✓';
    }

    if (form138 == 1) {
      result = '✓';
    }

    if (nso == 1) {
      result = '✓';
    }

    return result;
  }

  exportExcel() {

    const date = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .reverse()
      .join("/");
    // console.log(date);
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("My Sheet");

    // await worksheet.protect('CiCT#2020') // locks the whole excel file

    // Header labels
    const headerLabels = [
      'ID Number',
      'Full Name',
      'Department',
      'Course',
      'Year',
      'Form 137A',
      'Form 138',
      'NSO',
    ];

    // Add header row
    const headerRow = worksheet.addRow(headerLabels);
    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    worksheet.getColumn(1).width = 13;
    worksheet.getColumn(2).width = 48;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 6;
    worksheet.getColumn(6).width = 13;
    worksheet.getColumn(7).width = 12;
    worksheet.getColumn(8).width = 8;


    // for (var x = 2; x < 5; x++) {
    //   worksheet.getColumn(x).width = 48;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(5).width = 10;
    // worksheet.getColumn(6).width = 10;

    // for (var x = 7; x < 11; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(11).width = 10;

    // for (var x = 12; x < 16; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   worksheet.getColumn(x).alignment = { wrapText: true };
    // }

    var dataRow: any
    var form137A: any
    var form138: any
    var nso: any
    for (var x = 0; x < this.filteredStudentsList.length; x++) {

      if (this.filteredStudentsList[x].form137A == '1') {
        form137A = '✓'
      }
      else {
        form137A = ''
      }

      if (this.filteredStudentsList[x].form138 == '1') {
        form138 = '✓'
      }
      else {
        form138 = ''
      }

      if (this.filteredStudentsList[x].nso == '1') {
        nso = '✓'
      }
      else {
        nso = ''
      }

      dataRow = worksheet.addRow([
        this.filteredStudentsList[x].idnumber,
        this.filteredStudentsList[x].fullname,
        this.filteredStudentsList[x].departmentname,
        this.filteredStudentsList[x].course,
        this.filteredStudentsList[x].yearorgradelevel,
        form137A,
        form138,
        nso,

      ])
      dataRow.font = { size: 12 };
      dataRow.alignment = { vertical: 'middle', wrapText: true };;

    }


    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    workbook.xlsx.writeBuffer().then((data: any) => {
      // console.log("buffer");
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = url;
      a.download = "REQSLIST_" + this.global.domain + ' ' + this.global.displaysem + ' ' + this.global.displayyear;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
      // Swal.fire(
      //   'Download Succes!',
      //   '',
      //   'success')
      this.global.swalSuccess('Download Success!')
    });

  }
}
