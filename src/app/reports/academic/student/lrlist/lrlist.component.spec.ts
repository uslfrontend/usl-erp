import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LrlistComponent } from './lrlist.component';

describe('LrlistComponent', () => {
  let component: LrlistComponent;
  let fixture: ComponentFixture<LrlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LrlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LrlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
