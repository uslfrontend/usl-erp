import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../../global.service';
import { ApiService } from '../../../../api.service';
import * as ExcelJS from "exceljs/dist/exceljs"
@Component({
  selector: 'app-lrlist',
  templateUrl: './lrlist.component.html',
  styleUrls: ['./lrlist.component.css']
})
export class LrlistComponent implements OnInit {
  schoolYear = this.global.syear
  level = this.global.domain
  StudentsRequirementList = [];



  isLoading: boolean = true;


  currentPage: number = 1;
  itemsPerPage: number = 10; // You can adjust this number based on your preference


  filterText: string = '';
  idnumber = ''
  name = ''
  birthdate = ''
  gradelevel = ''
  courseCode = ''
  strand = ''
  section = ''
  lrn = ''
  filteredStudentsList = []; // Initialize an empty array for the filtered list

  configList: any
  ctr = 0




  gradeLevels = [];
  course = [];
  stranddd = []
  sectionnn = []

  constructor(public global: GlobalService, private api: ApiService) {
    this.configList = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  pageChangedList(event) {
    this.configList.currentPage = event;
  }

  ngOnInit() {
    if (this.level == "HIGHSCHOOL") {
      this.level = 'highschool'
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    if (this.level == 'ELEMENTARY') {
      this.schoolYear = this.global.syear.slice(0, -1);
    }

    this.isLoading = true; // Start loading

    // this.syear()
    // console.log(this.global.syear)
    // console.log(this.global.domain)


    this.api.getLRNChecklist(this.schoolYear, this.level)
      .map(response => response.json())
      .subscribe(res => {
        this.StudentsRequirementList = res.data;
        this.filteredStudentsList = res.data
        // this.form137A = this.StudentsRequirementList.form137A;
        // this.form138 = this.StudentsRequirementList.form138;
        // this.nso = this.StudentsRequirementList.nso;


        // console.table('List: ', this.StudentsRequirementList);

        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].gradeLevel !== null) {
            unique.add(res.data[x].gradeLevel);
          }
        }
        this.gradeLevels = Array.from(unique);
        // console.log('List: ', this.gradeLevels);


        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].courseCode !== null) {
            unique.add(res.data[x].courseCode);
          }
        }
        this.course = Array.from(unique);
        // console.log('List: ', this.course);

        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].strand !== null) {
            unique.add(res.data[x].strand);
          }
        }
        this.stranddd = Array.from(unique);
        // console.log('List: ', this.stranddd);


        var unique = new Set();
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].strand !== null) {
            unique.add(res.data[x].strand);
          }
        }
        this.stranddd = Array.from(unique);
        // console.log('List: ', this.stranddd);

        this.SectionArray();



        this.isLoading = false; // Data is fetched, loading finished
      });
  }

  SectionArray() {

    var unique = new Set();
    for (var x = 0; x < this.StudentsRequirementList.length; x++) {
      if (this.StudentsRequirementList[x].section !== null && this.StudentsRequirementList[x].section !== '') {
        unique.add(this.StudentsRequirementList[x].section);
      }
    }
    this.sectionnn = Array.from(unique);
    // console.log('List: ', this.sectionnn);
  }
  clearFilter() {
    this.idnumber = ''
    this.name = ''
    this.birthdate = ''
    this.gradelevel = ''
    this.courseCode = ''
    this.strand = ''
    this.section = ''
    this.lrn = ''
    // this.filteredStudentsList
    this.filterall(event)
    this.SectionArray();
  }
  isOptionDisabled(optionValue: string): boolean {
    // Check if gradelevel is 7, 8, 9, or 10 and the option is 'SHS'
    if ((this.gradelevel === '7' || this.gradelevel === '8' || this.gradelevel === '9' || this.gradelevel === '10') && optionValue === 'SHS') {
      return true;
    }

    // Check if gradelevel is 11 or 12 and the option is 'HSSci' or 'HSAdvSci'
    if ((this.gradelevel === '11' || this.gradelevel === '12') && (optionValue === 'HSSci' || optionValue === 'HSAdvSci')) {
      return true;
    }

    if ((this.strand === 'STEM(HS)' || this.strand === 'STEM(NHS)' || this.strand === 'ABM' || this.strand === 'HUMSS') && (optionValue === '7' || optionValue === '8' || optionValue === '9' || optionValue === '10')) {
      return true;
    }

    if ((this.courseCode === 'HSSci' || this.courseCode === 'HSAdvSci') && (optionValue === '11' || optionValue === '12')) {
      return true;
    }

    if ((this.courseCode === 'SHS') && (optionValue === '7' || optionValue === '8' || optionValue === '9' || optionValue === '10')) {
      return true;
    }

    if ((this.strand === 'STEM(HS)' || this.strand === 'STEM(NHS)' || this.strand === 'ABM' || this.strand === 'HUMSS') && (optionValue === 'HSSci' || optionValue === 'HSAdvSci')) {
      return true;
    }
    return false;
  }

  Unique() {
    var temparray = []
    for (var x = 0; x < this.StudentsRequirementList.length; x++) {
      if (this.courseCode == this.StudentsRequirementList[x].courseCode) {
        temparray.push(this.StudentsRequirementList[x].section)
      }
    }
    console.log('sad', temparray)

    const uniqueArray = [...new Set(temparray)];
    const filteredArray = uniqueArray.filter(value => value !== null && value !== '');
    this.sectionnn = filteredArray
    console.log(this.sectionnn)
  }
  UniqueSection() {

    var temparray = []
    for (var x = 0; x < this.StudentsRequirementList.length; x++) {
      if (this.gradelevel == this.StudentsRequirementList[x].gradeLevel) {
        temparray.push(this.StudentsRequirementList[x].section)
      }
    }
    // console.log('sad', temparray)

    const uniqueArray = [...new Set(temparray)];
    const filteredArray = uniqueArray.filter(value => value !== null && value !== '');
    this.sectionnn = filteredArray

    console.log(this.sectionnn)

    // console.log('List: ', this.sectionnn);

  }
  filterall(event: any): void {
    // if(this.gradelevel != ''){
    //   this.UniqueSection()
    // }
    // if(this.courseCode != ''){
    //   this.Unique()
    // }

    // console.log(this.civilStatus);
    this.filteredStudentsList = this.StudentsRequirementList.filter(student => {
      // console.log(employee.civilStatus, employee.idnumber);
      return (
        (this.idnumber === '' || (student.idNumber && student.idNumber.toLowerCase().includes(this.idnumber.toLowerCase()))) &&
        (this.name === '' || (student.name && student.name.toLowerCase().includes(this.name.toLowerCase()))) &&
        (this.birthdate === '' || (student.dateOfBirth && student.dateOfBirth.toLowerCase().includes(this.birthdate.toLowerCase()))) &&
        (this.gradelevel === '' || (student.gradeLevel && student.gradeLevel.toLowerCase().includes(this.gradelevel.toLowerCase()))) &&
        (this.courseCode === '' || (student.courseCode && student.courseCode.toLowerCase().includes(this.courseCode.toLowerCase()))) &&
        (this.strand === '' || (student.strand && student.strand.toLowerCase().includes(this.strand.toLowerCase()))) &&
        (this.section === '' || (student.section && student.section.toLowerCase().includes(this.section.toLowerCase()))) &&
        (this.lrn === '' || (student.lrn && student.lrn.toLowerCase().includes(this.lrn.toLowerCase())))
      );
    });

    // Now 'this.filteredList' holds the filtered results
    // console.log(this.filteredList);
  }
  // checkAssessmentStatus(form137A, form138, nso) {
  //   let result = 'N/A';

  //   if (form137A == 1) {
  //     result = '✓';
  //   }

  //   if (form138 == 1) {
  //     result = '✓';
  //   }

  //   if (nso == 1) {
  //     result = '✓';
  //   }

  //   return result;
  // }

  exportExcel() {

    const date = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .reverse()
      .join("/");
    // console.log(date);
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("My Sheet");

    // await worksheet.protect('CiCT#2020') // locks the whole excel file

    // Header labels
    const headerLabels = [
      'ID Number',
      'Full Name',
      'Birth Date',
      'Level',
      'Course',
      'Strand',
      'Section',
      'LR No.',
    ];

    // Add header row
    const headerRow = worksheet.addRow(headerLabels);
    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    worksheet.getColumn(1).width = 13;
    worksheet.getColumn(2).width = 48;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 7;
    worksheet.getColumn(5).width = 15;
    worksheet.getColumn(6).width = 13;
    worksheet.getColumn(7).width = 20;
    worksheet.getColumn(8).width = 15;


    // for (var x = 2; x < 5; x++) {
    //   worksheet.getColumn(x).width = 48;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(5).width = 10;
    // worksheet.getColumn(6).width = 10;

    // for (var x = 7; x < 11; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    // }

    // worksheet.getColumn(11).width = 10;

    // for (var x = 12; x < 16; x++) {
    //   worksheet.getColumn(x).width = 10;
    //   worksheet.getColumn(x).alignment = { wrapText: true };
    // }

    var dataRow: any
    var form137A: any
    var form138: any
    var nso: any
    for (var x = 0; x < this.filteredStudentsList.length; x++) {

      // if(this.StudentsRequirementList[x].form137A == '1'){
      //   form137A = '✓'
      // }
      // else{
      //   form137A = ''
      // }

      // if(this.StudentsRequirementList[x].form138 == '1'){
      //   form138 = '✓'
      // }
      // else{
      //   form138 = ''
      // }

      // if(this.StudentsRequirementList[x].nso == '1'){
      //   nso = '✓'
      // }
      // else{
      //   nso = ''
      // }

      dataRow = worksheet.addRow([
        this.filteredStudentsList[x].idNumber,
        this.filteredStudentsList[x].name,
        this.filteredStudentsList[x].dateOfBirth,
        this.filteredStudentsList[x].gradeLevel,
        this.filteredStudentsList[x].courseCode,
        this.filteredStudentsList[x].strand,
        this.filteredStudentsList[x].section,
        this.filteredStudentsList[x].lrn,


      ])
      dataRow.font = { size: 12 };
      dataRow.alignment = { vertical: 'middle', wrapText: true };;

    }


    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    workbook.xlsx.writeBuffer().then((data: any) => {
      // console.log("buffer");
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = url;
      a.download = "LR List_" + this.global.domain + ' ' + this.global.displaysem + ' ' + this.global.displayyear;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
      // Swal.fire(
      //   'Download Succes!',
      //   '',
      //   'success')
      this.global.swalSuccess('Download Success!')
    });

  }
}
