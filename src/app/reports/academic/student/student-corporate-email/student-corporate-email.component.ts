import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-student-corporate-email',
  templateUrl: './student-corporate-email.component.html',
  styleUrls: ['./student-corporate-email.component.css']
})
export class StudentCorporateEmailComponent implements OnInit {

  array;
  arraycomp;
  sy=this.global.syear;
  accessDeniedMessage = 'You are not allowed to access this report.';
  idNumber = '';
  lastName = '';
  deptCodeVar;
  deptCode;
  departments=[];
  departments2=[]
  submitted=false;
  config: any;

  constructor(public global: GlobalService, private api: ApiService) { }

  ngOnInit() {
    if (this.global.domain === 'HIGHSCHOOL' || this.global.domain === 'ELEMENTARY') {
      this.sy = this.global.syear.slice(0, -1);
    }

    for (var i = 0; i < this.global.departments.length; ++i) {
      for (var i2 = 0; i2 < this.global.viewdomain.length; ++i2) {
        if (this.global.departments[i].departmentId == this.global.viewdomain[i2]) {
          this.departments.push(this.global.departments[i])
        }
      }
    }
    if (this.global.domain == "GRADUATE SCHOOL") {
      for (var i = 0; i < this.departments.length; ++i) {
        if (this.departments[i].departmentCode == "GS") {
          this.departments2.push(this.departments[i])
        }
      }
    }
    if (this.global.domain == "COLLEGE") {        
      for (var i = 0; i < this.departments.length; ++i) {
        if (this.departments[i].departmentCode != "ELEM" && 
          this.departments[i].departmentCode != "HS" && 
          this.departments[i].departmentCode != "GS" &&
          this.departments[i].departmentCode != "DKLC" &&
          this.departments[i].departmentCode != "SHVED" &&
          this.departments[i].departmentCode != "SICS"
        ) {
          this.departments2.push(this.departments[i])
        }
      }
    }    

    this.config = {
      itemsPerPage: 8,
      currentPage: 1,
      totalItems: 0
    };
  }

  pageChanged(event){
    this.config.currentPage = event;
  }

  matSelectonChange(){    
  this.submitted=false; 
    this.loaddata();
  }

  setDeptCode(dept) {
    switch (dept) {
      case 'SEAID': {       
        return 'SEAITE'
        break;
      }
      case 'SBAA': {
        return 'SABH'
        break;
      }
      case 'SEAS': {
        return 'SEAS'
        break;
      }
      case 'SHS': {
        return 'SHAS'
        break;
      }
      default: {
        return dept
        break;
      }
    }
  }

  setBGColor(dept): object {
    switch (dept) {
      case 'SEAITE': {
        return { background: 'maroon', color: 'white' }
        break;
      }
      case 'SABH': {
        return { background: 'orange', color: 'white' }
        break;
      }
      case 'SEAS': {
        return { background: 'blue', color: 'white' }
        break;
      }
      case 'SHAS': {
        return { background: 'green', color: 'white' }
        break;
      }
      default: {
        return { background: 'white', color: 'black' }
        break;
      }
    }
  }

  loaddata() {   
    this.submitted=true;   
    this.array = undefined;
    this.arraycomp=undefined;
    this.global.swalLoading('Retrieving '+this.deptCodeVar+' Corporate Emails');
    this.api.getReportSummaryStudentCorporateEmailAll(this.sy,this.global.domain)
      .map(response => response.json())
      .subscribe(res => {
        this.array = [];
        this.arraycomp=[];
        //console.log(this.sy);
        // for (var i = 0; i < res.data.length; ++i) {
        //   if (!this.deptCode.includes(res.data[i].departmentCode)) {
        //     this.deptCode.push(res.data[i].departmentCode)
        //   }
        //   if (res.data[i].deptCode == null)
        //     res.data[i].deptCode = ''
        // }        
        //console.log(this.setDeptCode(this.deptCodeVar));        
        switch (this.setDeptCode(this.deptCodeVar)) {
          case 'All': {
            this.array = res.data;
            this.arraycomp = res.data;
            break;
          }
          case 'SABH': {
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].departmentCode == this.setDeptCode(this.deptCodeVar)) {
                this.array.push(res.data[i]);
                this.arraycomp.push(res.data[i]);
              }
            }
            break;
          }
          case 'SEAITE': {
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].departmentCode == this.setDeptCode(this.deptCodeVar)) {
                this.array.push(res.data[i]);
                this.arraycomp.push(res.data[i]);
              }
            }
            break;
          }
          case 'SHAS': {
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].departmentCode == this.setDeptCode(this.deptCodeVar)) {
                this.array.push(res.data[i]);
                this.arraycomp.push(res.data[i]);
              }
            }
            break;
          }
          case 'SEAS': {
            for (var i = 0; i < res.data.length; i++) {
              if (res.data[i].departmentCode == this.setDeptCode(this.deptCodeVar)) {
                this.array.push(res.data[i]);
                this.arraycomp.push(res.data[i]);
              }
            }
            break;
          }
          default:{
            this.array=undefined;
            this.arraycomp=undefined;
          }
        } 
        this.global.swalClose();              
      }, Error => {
        console.log(Error);    
        this.global.swalAlertError(Error);
      });
  }

  Filter() {
    this.array = [];
    for (var i = 0; i < this.arraycomp.length; ++i) {
      if (
        (this.idNumber == '' || this.arraycomp[i].idNumber.includes(this.idNumber)) &&
        (this.lastName.toLowerCase() == '' || this.arraycomp[i].lastName.toLowerCase().includes(this.lastName.toLowerCase()))
        //(this.deptCodeVar == 'all' || this.arraycomp[i].departmentCode == this.deptCodeVar)
      ) {
        this.array.push(this.arraycomp[i])
      }
    }
  }

  exportexcel(): void {
    var arr = [];

    for (var i = 0; i < this.array.length; ++i) {
      arr.push(
        {
          //"ID Number": this.array[i].idNumber,
          "First Name": this.array[i].firstName,
          "Last Name": this.array[i].lastName,
          "Email Address": this.array[i].emailAddress,
          "Department": '/Students/' + this.array[i].departmentCode,
          "Password": this.array[i].password,
          "Date Created": this.array[i].dateCreated
        }
      )
    }

    //this.global.swalClose();

    var wscols = [
      //{wch:15},
      { wch: 30 },
      { wch: 30 },
      { wch: 20 },
      { wch: 20 },
      { wch: 20 },
      { wch: 15 },
    ];



    var fileName = 'StudentCorporateEmails-' + this.global.syear + '.xlsx'
    /* table id is passed over here */
    //let element = document.getElementById('excel-table'); 

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(arr);
    ws['!cols'] = wscols;

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'CorporateEmails');

    /* save to file */
    XLSX.writeFile(wb, fileName);
  }

}
