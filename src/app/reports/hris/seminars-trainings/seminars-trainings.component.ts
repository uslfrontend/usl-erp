import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SharedServicesService} from './../../shared-services.service';
import * as XLSX from 'xlsx';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { EmployeeLookupComponent } from './../../../academic/lookup/employee-lookup/employee-lookup.component';

@Component({
  selector: 'app-seminars-trainings',
  templateUrl: './seminars-trainings.component.html',
  styleUrls: ['./seminars-trainings.component.css']
})
export class SeminarsTrainingsComponent implements OnInit {

  id = '';
  fname = '';
  mname = '';
  lname = '';
  suffix = '';

  showContent = false;
  name = '';
  position = '';
  idnumber= '';
  dtridnum="DTR ID#: "

  ctr = 0;
  configSeminars: any;
  SeminarTableArr = [];

  constructor(private excelService:SharedServicesService,public global: GlobalService,private http: Http,public dialog: MatDialog,) {
    this.configSeminars = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  ngOnInit() {

    this.clear();
  }

  pageChangedSeminars(event){
    this.configSeminars.currentPage = event;
  }

  keyDownFunction(event){
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus'){
      if(this.id!=''){
        this.http.get(this.global.api+'Employee/'+this.id,this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            //console.log(res);
            this.global.swalClose();
            if (res.message != "IDNumber does not exist.") {

              this.name = res.data[0].fullname;
              this.position = res.data[0].position;
              this.idnumber=res.data[0].idnumber;
              this.dtridnum="DTR ID#: "+res.data[0].dtrid;
              this.getSeminars();
            }else{
              //console.log('1111')
              this.clear();
              this.global.swalAlert("Employee not found",'','warning');
            }
          },Error=>{
            this.clear();
            this.global.swalAlertError();
          });
      }else{

      }
    }
  }

  employeelookup(): void {

    const dialogRef = this.dialog.open(EmployeeLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result!='cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }


  getSeminars(){
    this.showContent = true;
    this.http.get(this.global.api+'Employee/SeminarsAndTrainings/'+this.id,this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          //console.table(res.data);
          this.SeminarTableArr=res.data;
          if(this.SeminarTableArr != undefined || this.SeminarTableArr != null)
          {
            var count = Object.keys(this.SeminarTableArr).length;
            this.ctr = count
          }

        },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });

  }

  export(datatoExport){
  	console.log(datatoExport);
  	var arr = []
      for (var i = 0; i < datatoExport.length; ++i) {
        // if(datatoExport[i].status !='For Approval')
        arr.push(
        {
          "SEMINAR DESCRIPTION": datatoExport[i].seminardescription,
          "SPONSOR": datatoExport[i].companyname,
          "VENUE": datatoExport[i].venue,
          "START DATE":datatoExport[i].startdate,
          "END DATE":datatoExport[i].enddate,
          "CATEGORY":datatoExport[i].trainingType,

        }
          )
      }
    var employeeData = [
      {
        "name": this.name,
        "position": this.position,
        "idnumber": this.idnumber
      }
    ];
    console.log(arr)
    this.excelService.generateHRISseminarsTrainings(arr,employeeData);
  }

  clear(){
    this.id = '';
    this.fname = '';
    this.mname = '';
    this.lname = '';
    this.suffix = '';

    this.showContent = false;
    this.name = '';
    this.position = '';
    this.idnumber= '';
    this.dtridnum="DTR ID#: "

    this.SeminarTableArr = [];
    this.ctr=0
  }

}
