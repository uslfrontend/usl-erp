import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveEmployeeMasterlistComponent } from './active-employee-masterlist.component';

describe('ActiveEmployeeMasterlistComponent', () => {
  let component: ActiveEmployeeMasterlistComponent;
  let fixture: ComponentFixture<ActiveEmployeeMasterlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveEmployeeMasterlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveEmployeeMasterlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
