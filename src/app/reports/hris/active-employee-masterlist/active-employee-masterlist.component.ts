import { Component, OnInit } from '@angular/core';
import { HrisApiService } from './../../../hris-api.service';
import { GlobalService } from './../../../global.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

import * as ExcelJS from "exceljs/dist/exceljs"
import Swal from 'sweetalert2'

@Component({
  selector: 'app-active-employee-masterlist',
  templateUrl: './active-employee-masterlist.component.html',
  styleUrls: ['./active-employee-masterlist.component.css']
})
export class ActiveEmployeeMasterlistComponent implements OnInit {

  constructor(
    private hrisApi: HrisApiService,
    private global: GlobalService,
    private FormBuilder: FormBuilder
  ) {

    this.configList = {
      itemsPerPage: 15,
      currentPage: 1,
      totalItems: this.ctr
    };

  }

  idNumber: string = '';
  lastName: string = '';
  firstName: string = '';
  middleName: string = '';
  dateOfBirth: string = '';
  civilStatus: string = '';
  pagIbigNo: string = '';
  philHealthNo: string = '';
  sssNo: string = '';
  tinNo: string = '';

  masterlistArray = []
  filteredList = []
  departments = []

  configList: any
  ctr = 0

  departmentArr: string[] = []
  deparmentValue = new FormControl('', [Validators.required]);
  deparmentOptions: Observable<string[]>;

  deptValue = ''

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.departmentArr.filter(option => option.toLowerCase().includes(filterValue));
  }

  ngOnInit() {

    this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value || ' ')));

    if (this.global.checkaccess(':ReportHRIS:ActiveEmployeeMasterListRGet')) {
      this.global.swalLoading('Loading Information');
      this.hrisApi.getReportActiveEmployeesMasterList().map(response => response.json()).subscribe(res => {
        this.masterlistArray = res.data;
        this.global.swalClose();
        this.filteredList = res.data;

        var uniqueDepartments = new Set();

        for (var x = 0; x < res.data.length; x++) {
          uniqueDepartments.add(res.data[x].departmentName);
        }

        // If you need to convert the Set back to an array, you can do so like this:
        this.departments = Array.from(uniqueDepartments);
        this.departmentArr = this.departments

      });
    }


  }

  fillInDepartment() {
    this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value || ' ')));
  }

  pageChangedList(event) {
    this.configList.currentPage = event;
  }

  filterall(event: any): void {
    // console.log(this.civilStatus);
    this.filteredList = this.masterlistArray.filter(employee => {
      // console.log(employee.civilStatus, employee.idnumber);
      return (
        (this.idNumber === '' || (employee.idnumber && employee.idnumber.toLowerCase().includes(this.idNumber.toLowerCase()))) &&
        (this.lastName === '' || (employee.lastname && employee.lastname.toLowerCase().includes(this.lastName.toLowerCase()))) &&
        (this.firstName === '' || (employee.firstname && employee.firstname.toLowerCase().includes(this.firstName.toLowerCase()))) &&
        (this.middleName === '' || (employee.middlename && employee.middlename.toLowerCase().includes(this.middleName.toLowerCase()))) &&
        (this.dateOfBirth === '' || (employee.dateOfBirth && employee.dateOfBirth.toLowerCase().includes(this.dateOfBirth.toLowerCase()))) &&
        (this.civilStatus === '' || (employee.civilStatus && employee.civilStatus.toLowerCase().includes(this.civilStatus.toLowerCase()))) &&
        (this.deptValue === '' || (employee.departmentName && employee.departmentName.toLowerCase().includes(this.deptValue.toLowerCase())))&&
        (this.deparmentValue.value === '' || (employee.departmentName && employee.departmentName.toLowerCase().includes(this.deparmentValue.value.toLowerCase())))
        // (this.pagIbigNo === '' || (employee.pagIbigNo && employee.pagIbigNo.toLowerCase().includes(this.pagIbigNo.toLowerCase()))) &&
        // (this.philHealthNo === '' || (employee.philHealthNo && employee.philHealthNo.toLowerCase().includes(this.philHealthNo.toLowerCase()))) &&
        // (this.sssNo === '' || (employee.sssNo && employee.sssNo.toLowerCase().includes(this.sssNo.toLowerCase()))) &&
        // (this.tinNo === '' || (employee.tin && employee.tin.toLowerCase().includes(this.tinNo.toLowerCase())))
      );
    });

    // Now 'this.filteredList' holds the filtered results
    // console.log(this.filteredList);
  }


  filterClear(){
    this.idNumber = ''
    this.lastName = ''
    this.firstName = ''
    this.middleName = ''
    this.dateOfBirth = ''
    this.civilStatus = ''
    this.deparmentValue.setValue('')
    this.filterall(void 123)
  }

  exportExcel() {

    const date = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .reverse()
      .join("/");
    // console.log(date);
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("My Sheet");

    // await worksheet.protect('CiCT#2020') // locks the whole excel file

    // Header labels
    const headerLabels = [
      'ID Number',
      'Last Name',
      'First Name',
      'Middle Name',
      'Date Of Birth',
      'Civil Status',
      'Pag-ibig Number',
      'PhilHealth Number',
      'SSS Number',
      'TIN Number',
      'Date Hired',
      'Position',
      'Department Name',
      'Current Address',
      'Permanent Address'
    ];

    // Add header row
    const headerRow = worksheet.addRow(headerLabels);
    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    worksheet.getColumn(1).width = 18;

    for (var x = 2; x < 5; x++) {
      worksheet.getColumn(x).width = 22;
      // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    }

    worksheet.getColumn(5).width = 18;
    worksheet.getColumn(6).width = 18;

    for (var x = 7; x < 11; x++) {
      worksheet.getColumn(x).width = 25;
      // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    }

    worksheet.getColumn(11).width = 18;

    for (var x = 12; x < 16; x++) {
      worksheet.getColumn(x).width = 70;
      worksheet.getColumn(x).alignment = { wrapText: true };
    }

    var dataRow: any

    for (var x = 0; x < this.filteredList.length; x++) {

      dataRow = worksheet.addRow([
        this.filteredList[x].idnumber,
        this.filteredList[x].lastname,
        this.filteredList[x].firstname,
        this.filteredList[x].middlename,
        this.filteredList[x].dateOfBirth,
        this.filteredList[x].civilStatus,
        this.filteredList[x].pagIbigNo,
        this.filteredList[x].philHealthNo,
        this.filteredList[x].sssNo,
        this.filteredList[x].tin,
        this.filteredList[x].dateHired,
        this.filteredList[x].position,
        this.filteredList[x].departmentName,
        this.filteredList[x].currentAddress,
        this.filteredList[x].permanentAddress
      ])
      dataRow.font = { size: 12 };
      dataRow.alignment = { vertical: 'middle', wrapText: true };;

    }


    headerRow.alignment = { vertical: 'top', horizontal: 'center' };
    headerRow.font = { name: 'Calibri', family: 4, size: 14, bold: true, strike: false };

    workbook.xlsx.writeBuffer().then((data: any) => {
      // console.log("buffer");
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = url;
      a.download = "Acive-Employees-Masterlist.xlsx";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
      Swal.fire(
        'Download Succes!',
        '',
        'success')
    });

  }



}
