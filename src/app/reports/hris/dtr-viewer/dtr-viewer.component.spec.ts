import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtrViewerComponent } from './dtr-viewer.component';

describe('DtrViewerComponent', () => {
  let component: DtrViewerComponent;
  let fixture: ComponentFixture<DtrViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtrViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtrViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
