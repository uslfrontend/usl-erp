import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from './../../../hris-api.service';
import { MatDialog, } from '@angular/material';
import { EmployeeLookupComponent } from './../../../academic/lookup/employee-lookup/employee-lookup.component';

import * as moment from 'moment';
import * as pdfMake from "pdfmake/build/pdfmake";
import { ChangeDetectorRef,AfterContentChecked} from '@angular/core'

@Component({
  selector: 'app-dtr-viewer',
  templateUrl: './dtr-viewer.component.html',
  styleUrls: ['./dtr-viewer.component.css']
})
export class DtrViewerComponent implements OnInit, AfterContentChecked {
  id=''
  lastdate=''
  edate=''
  sdate=''

  currentSDate
  currentEDate
  curmonth
  curyear
  monthlasday
  fname='';
  lname='';
  mname='';
  suffix='';
  exportPDFTrigger = false;
  generateTrigger = true;
  totalLateMins;
  totalUndertimeMins = 0;

  displayTotalLate=''
  displayTotalUndertime=''

  attendanceArray=[];
  position='';
  dtrid = '';
  image: any = 'assets/noimage.jpg';

  ///////////////////PAGINATION VARIABLES///////////////////////
  dtrCtr = 0;
  dtrConfig: any;
  collection = { count: 60, data: [] };
  //////////////////////////////////////////////////////////////
  constructor(private cdRef : ChangeDetectorRef,private formBuilder: FormBuilder, public dialog: MatDialog,private domSanitizer: DomSanitizer,public global: GlobalService,private api: ApiService, private hrisApi:HrisApiService, private datePipe : DatePipe) {
    this.dtrConfig = {
      itemsPerPage: 15,
      currentPage: 1,
      totalItems: this.dtrCtr
    };
  }

  ngOnInit() {

  }
    ngAfterContentChecked() : void {
      this.cdRef.detectChanges();
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.id != '') {
        this.global.swalLoading('Retrieving Person and Employee Informations');
        this.hrisApi.getReportHrisPerson(this.id)
          .map(response => response.json())
          .subscribe(res => {
            //this.global.swalClose();
            if (res.message != undefined && res.message == 'Person found.') {
              this.fname = res.data.firstName;
              this.mname = res.data.middleName;
              this.lname = res.data.lastName;
              this.suffix = res.data.suffixName;

            //get PersonIDInfo
            this.getPersonIDInfo();

            this.global.swalClose();
            } else {
              this.global.swalAlert(res.message, '', 'warning');
            }
          }, Error => {
            this.global.swalAlertError(Error);
          });
      }
    }
  }
  getPersonIDInfo(){
    this.api.getDTRPersonIDInfo(this.id)
    .map(response => response.json())
    .subscribe(res => {
      if (res.data != null) {
        this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
        this.api.getDTREmployee(this.id)
        .map(response => response.json())
        .subscribe(res => {
          if (res.data != null) {
            this.dtrid = res.data[0].dtrid;
            this.position = res.data[0].position;
          }
        }, Error => {
          this.global.swalAlertError(Error);
        })

      }
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

  clearValues(){
    this.totalLateMins = 0;
    this.totalUndertimeMins = 0;
    this.attendanceArray = [];
    this.displayTotalLate=''
    this.displayTotalUndertime=''
    this.exportPDFTrigger = false;
    this.lastdate='';
  }

  getdtr(){
    this.clearValues();

    this.exportPDFTrigger = true;

    this.api.getDTR(this.id,this.currentSDate,this.currentEDate)
          .map((response) => response.json())
          .subscribe(
            (res) => {
              mutate_array(res.data)
              this.getTotalLate();
              this.getTotalUndertime();
            },
            (Error) => {
              this.global.swalAlertError(Error);
            }
          );
    var that = this;
    function mutate_array(array){
      that.totalLateMins=0;
      array.forEach(element => {
        that.attendanceArray.push({
          comment: element.comment,
          correctedTI: element.correctedTI,
          correctedTO: element.correctedTO,
          employeeId: element.employeeId,
          hours: element.hours,
          scheduledTI: element.scheduledTI,
          scheduledTO: element.scheduledTO,
          theDate: element.theDate,
          timeIn: element.timeIn,
          timeOut: element.timeOut,
          late: that.getLate(element.timeIn,element.correctedTI,element.scheduledTI,element.hours),
          undertime: that.getUndertime(element.timeOut,element.correctedTO,element.scheduledTO,element.timeIn,element.comment)
        })
      });
    }

  }

  setLastDate(lastdate){
    this.lastdate = lastdate;
    return lastdate
  }

  checkDTR(){
    this.attendanceArray = [];
    if(this.sdate!=""&&this.edate!=""){
      this.currentSDate = this.datePipe.transform(this.sdate, 'MM-dd-yyyy');
      this.currentEDate = this.datePipe.transform(this.edate, 'MM-dd-yyyy');
      this.getdtr();

    }
  }

  activateGenerate(){
    if(this.generateTrigger == true)
      return true
    else
      return false;
  }

  checkDateRange(){
    if((this.datePipe.transform(this.edate, 'MM-dd-yyyy'))>=(this.datePipe.transform(this.sdate, 'MM-dd-yyyy')))
      this.generateTrigger =  false
    else
      this.generateTrigger =  true
  }

  getLate(timeIn,correctedTI,scheduleTI,dtHours){
    var time_in
    var schedule_TI
    var late;

    if(scheduleTI){
      if(correctedTI)
        time_in = correctedTI.substring(0,5);
      else{
        if(timeIn)
          time_in = timeIn.substring(0,5);
        else
          time_in = 'absent'
      }
      schedule_TI = scheduleTI.substring(0,5);
    }
    else
      return '';

    if(time_in!='absent'){
      var startTime = moment(timeIn.substring(0,5)+':00 am', 'HH:mm:ss a');
      var endTime = moment(schedule_TI+':00 am', 'HH:mm:ss a');

      if(startTime>endTime){
        let hours = startTime.diff(endTime, 'hours')
        let mins = moment.utc(moment(startTime, "HH:mm:ss").diff(moment(endTime, "HH:mm:ss"))).format("mm")

        this.totalLateMins = (this.totalLateMins) + (hours*60);
        this.totalLateMins = this.totalLateMins + parseInt(mins);
        if(hours>0)
          late = hours+'H '+mins+"m"
        else
          late = mins+"m"
      }else
        late = '';
    }else{

      late = dtHours * 60
      let hours = Math.floor(late / 3600);
      let mins = Math.floor((late % 3600) / 60);

      this.totalLateMins = this.totalLateMins+dtHours;

      if(hours>0)
        late = hours+'H '+mins+'m'
      else
        late = mins+'m'
    }

    return late;
  }

  getUndertime(timeOut,correctedTO,scheduleTO,timeIn,remarks){
    var time_out
    var schedule_T0
    var undertime;

    if(scheduleTO){
      if(correctedTO)
        time_out = correctedTO.substring(0,5);
      else{
        if(timeOut)
          time_out = timeOut.substring(0,5);
        else
          time_out = 'absent'
      }

      schedule_T0=scheduleTO.substring(0,5);
      if(time_out == 'absent'){
        if(timeIn)
          time_out = timeIn.substring(0,5)
        else
          time_out = ''
      }

    }
    else
      return '';

    if(time_out!=''){

      var startTime = moment(time_out+':00 am', 'HH:mm:ss a');
      var endTime = moment(schedule_T0+':00 am', 'HH:mm:ss a');

      if(endTime>startTime){

        var hours = endTime.diff(startTime, 'hours')
        var mins = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")

        this.totalUndertimeMins = (this.totalUndertimeMins) + (hours*60);
        this.totalUndertimeMins = this.totalUndertimeMins + parseInt(mins);

        if(hours>0)
          undertime = hours+'H '+mins+'m'
        else
          undertime = mins+'m'

      }
      else
        undertime = ''
    }
    return undertime;
  }

  getTotalLate(){
    var res = ''
    let temp = this.totalLateMins * 60;
    let hours = Math.floor(temp / 3600);
    let mins = Math.floor((temp % 3600) / 60);

    if(hours>0)
      res = hours.toString()+'H '+mins.toString()+'m'
    else
      res = mins.toString()+'m'

    this.displayTotalLate = res
  }
  getTotalUndertime(){
    var res = ''
    let temp = this.totalUndertimeMins * 60;
    let hours = Math.floor(temp / 3600);
    let mins = Math.floor((temp % 3600) / 60);

    if(hours>0)
      res = hours.toString()+'H '+mins.toString()+'m'
    else
      res = mins.toString()+'m'

    this.displayTotalUndertime = res
  }


  dtrPageChanged(event){
      this.dtrConfig.currentPage = event;
  }

  personlookup(): void {
    const dialogRef = this.dialog.open(EmployeeLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }


  //---------------------------------------------------------------------------------PDF GENERATION
  generatePDF(){

    var that = this;
    function checkNull(param){
      if(param==null || param == undefined)
        return '';
      else
        return param
    }


    var SY = SY
    var daterange =  this.datePipe.transform(this.sdate,'MMMM d, y')+' - '+ this.datePipe.transform(this.edate,'MMMM d, y');
    var idNumber = this.id;
    var fullName = this.lname+', '+this.fname+' '+ this.mname+' '+ this.suffix;
    var verticalAxis = 65;
    var baseX_Axis = 45;
    var dtrID = this.dtrid;
    var position = this.position
    var totalLate = this.displayTotalLate;
    var totalUndertime = this.displayTotalUndertime;
    var printDate = new Date();
    var headertext = {
      table:{
        widths: [350,200],
        body:[
          [
            {
                stack:[
                    {
                      text: "EMPLOYEE'S ATTENDANCE SUMMARY", bold: true, fontSize: 12, alignment: 'center',
                      absolutePosition: { x: 30, y: 30 }
                    },
                    {
                      text: daterange, bold: false, fontSize: 9.75, alignment: 'center',
                      absolutePosition: { x: 30, y: 45 }
                    },
                    {
                      text: 'Print Date: '+printDate.toLocaleDateString(), bold: false, fontSize: 8.75, alignment: 'left',
                      absolutePosition: { x: 810,y: 45 }
                    },
                ]

            },

          ]
        ]
      },
			layout: 'noBorders'
    }

    var subHeader = {
        stack:[
            {
                text: "EMPLOYEE ID NUMBER: "+idNumber, bold: true, fontSize: 9.75, alignment: 'left',
                absolutePosition: { x: baseX_Axis, y: verticalAxis }
            },
            {
                text: "EMPLOYEE NAME: "+fullName, bold: true, fontSize: 9.75, alignment: 'left',
                absolutePosition: { x: baseX_Axis, y: verticalAxis+=12 }
            },
            {
                text: "DTRID: "+dtrID, bold: true, fontSize: 9.75, alignment: 'left',
                absolutePosition: { x: baseX_Axis+450, y: verticalAxis-12 }
            },
            {
                text: "POSITION: "+position, bold: true, fontSize: 9.75, alignment: 'left',
                absolutePosition: { x: baseX_Axis+450, y: verticalAxis}
            }
        ]
    }

    function getData(){
        var array = mutate_final_array();
        return array;
    }

    function mutate_final_array(){
      var res=[];
      that.attendanceArray.forEach(element => {

          res.push({
            theDate: checkNull(element.theDate),
            timeIn:  checkNull(element.timeIn),
            timeOut:  checkNull(element.timeOut),
            correctedTI:  checkNull(element.correctedTI),
            correctedTO:  checkNull(element.correctedTO),
            scheduledTI:  checkNull(element.scheduledTI),
            scheduledTO:  checkNull(element.scheduledTO),
            late:  checkNull(element.late),
            undertime: checkNull(element.undertime),
            comment:  checkNull(element.comment),
          });
        });
      return res;
    }

    function getTheDate(theDate){
      if(theDate.toString()!== that.lastdate.toString()){
        that.setLastDate(theDate);
        return theDate;
      }
      else
        return '';
    }

    function buildTableBody(data) {
        var body = [];
        data.forEach(function(row) {
            var dataRow = [];
            dataRow.push(
              {text:getTheDate(row.theDate),bold:true, alignment: 'center'},
              {text:row.timeIn,bold:true, alignment: 'center'},
              {text:row.timeOut,bold:true, alignment: 'center'},
              {text:row.correctedTI,bold:true, alignment: 'center'},
              {text:row.correctedTO,bold:true, alignment: 'center'},
              {text:row.scheduledTI,bold:true, alignment: 'center'},
              {text:row.scheduledTO,bold:true, alignment: 'center'},
              {text:row.late,bold:true},
              {text:row.undertime,bold:true},
              {text:row.comment,bold:true}
            )
            body.push(dataRow);
        });

        body.push(
          [
            {text:'',bold:true, colSpan:7,border:[false,false,false,false]},
            {text:'',bold:true, alignment: 'center'},
            {text:'',bold:true, alignment: 'center'},
            {text:'',bold:true, alignment: 'center'},
            {text:'',bold:true, alignment: 'center'},
            {text:'',bold:true, alignment: 'center'},
            {text:'',bold:true, alignment: 'center'},
            {text:totalLate,fontSize:8,bold:true,border:[false,false,false,false]},
            {text:totalUndertime,fontSize:8,bold:true,border:[false,false,false,false]},
            {text:'',bold:true,border:[false,false,false,false]}
          ]
        );
        return body;
    }

    function table(data) {
        return {
            stack:[
                {
                    table: {
                        widths:[60,50,50,50,50,50,50,50,50,300],
                        dontBreakRows: true,

                        body: buildTableBody(data)
                    },absolutePosition: { x: baseX_Axis, y: verticalAxis+=31}
                },
            ]
        };
    }

    function tableHeader(){
        return {
            table: {
                headerRows:2,
                widths:[60,50,50,50,50,50,50,50,50,300],
                body:[
                    [
                        {text:'DATE',bold:true,rowSpan:2, alignment: 'center'},
                        {text:'ACTUAL DTR RECORD',bold:true,colSpan:2, alignment: 'center'},'',
                        {text:'CORRECTED RECORD',bold:true,colSpan:2, alignment: 'center'},'',
                        {text:'SCHEDULE',bold:true,colSpan:2, alignment: 'center'},'',
                        {text:'LATE',bold:true,rowSpan:2, alignment: 'center'},
                        {text:'UND',bold:true,rowSpan:2, alignment: 'center'},
                        {text:'REMARKS',bold:true,rowSpan:2, alignment: 'center'}
                    ],
                    [
                        {text:'',bold:true},
                        {text:'TIME IN',bold:true, alignment: 'center'},
                        {text:'TIME OUT',bold:true, alignment: 'center'},
                        {text:'TIME IN',bold:true, alignment: 'center'},
                        {text:'TIME OUT',bold:true, alignment: 'center'},
                        {text:'TIME IN',bold:true, alignment: 'center'},
                        {text:'TIME OUT',bold:true, alignment: 'center'},
                        {text:'',bold:true},
                        {text:'',bold:true},
                        {text:'',bold:true}
                    ]

                ]
            },fontSize:9,absolutePosition: { x: baseX_Axis, y: verticalAxis+=15}
        }
    }

    var dd = {
      pageSize:'FOLIO',
      pageMargins: [ 30, 124.0938, 30, 87 ],pageOrientation: 'landscape',
      header:{
        margin:20,
        columns:[
            headertext,
            tableHeader(),
        ]
      },

      content: [
        {
            stack:[
                subHeader,
                table(
                    getData()
                ),

            ]
        }

      ],
      footer:function(currentPage, pageCount, pageSize) {
            if(currentPage == pageCount){
                return [
                {
                    stack:[
                        {text:'Employee signature', fontSize:9,absolutePosition: { x: 45,y: -6.5}},
                        {text:'Noted by:', fontSize:9,absolutePosition: { x: 450,y: -6.5}},
                        {text:'___________________________________________', fontSize:9,absolutePosition: { x: 45,y: 20}},
                        {text:'___________________________________________', fontSize:9,absolutePosition: { x: 450,y: 20}},
                        {text:fullName, fontSize:9,absolutePosition: { x: 45,y: 30}},
                        {text:'Head of office', fontSize:9,absolutePosition: { x: 450,y: 30}},
                        {
                            text:'NOTE: This serves as a reference for the FINAL REPORT to be submitted to the accounting office. Any attendance summary that is not returned to the HRDO within two to four (2-4) days shall be considered ',
                            alignment:'left',
                            fontSize:9,
                            margin:[40,0,40,0]
                            ,absolutePosition: { x: 45,y: 45}
                        },
                        {
                            text:'confirmed/final and any incurred tardiness/absences shall be deducted from your salary.',
                            alignment:'left',
                            fontSize:9,
                            margin:[40,0,40,0],
                            absolutePosition: { x: 45,y: 55}

                        }
                    ]
                }
            ]
            }
            else{
                return [
                {
                    stack:[
                        {
                            text:'NOTE: This serves as a reference for the FINAL REPORT to be submitted to the accounting office. Any attendance summary that is not returned to the HRDO within two to four (2-4) days shall be considered ',
                            alignment:'left',
                            fontSize:9,
                            margin:[40,0,40,0]
                            ,absolutePosition: { x: 45,y: 45}
                        },
                        {
                            text:'confirmed/final and any incurred tardiness/absences shall be deducted from your salary.',
                            alignment:'left',
                            fontSize:9,
                            margin:[40,0,40,0],
                            absolutePosition: { x: 45,y: 55}

                        }
                    ]
                }
            ]
            }
        },
        defaultStyle: {
          fontSize: 10
        }

    }

    pdfMake.createPdf(dd).download('DTR - '+ fullName+' - '+ daterange+".pdf");
    // pdfMake.createPdf(dd).open();
  }
}
