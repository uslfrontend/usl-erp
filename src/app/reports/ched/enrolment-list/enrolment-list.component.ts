import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';

import { ExcelService } from './../../../academic/curriculum/excel.service';
import { ApiService } from './../../../api.service';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as XLSX from 'xlsx';

@Component({
	selector: 'app-enrolment-list',
	templateUrl: './enrolment-list.component.html',
	styleUrls: ['./enrolment-list.component.scss']
})
export class EnrolmentListComponent implements OnInit {
	proglevel = ''
	array = null
	sy
	pageSize = 15
	pageNumber = 1
	//#region USL logo
	logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAB8CAYAAACfZxWiAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAR3FJREFUeNrsnXd4VMX+/1/nnK3Z9B7SIIQSeu/SpIOAgGAFKSqL14INGyqIDXsjCiKiAioCgggIUqQ36TUJqaT3tn3P+f2xhYTi1Svc672/7zzPeTbZPWfOzLzn0z8zIyiKwn95iQBGAWFAJrAWqPlv7YzqvxyMBYDxKt8nAzP+vwSkSfOpv/u71eogKSmWlJSLdO7UlIqKWtIu5BETHYrd7qRjh0TMFhsHD52nqspEkybRmEwWnpk1nri4cOx25xV1durYJBSYCxirq83s+PUEWdlFtGwRT88eLdBoVEaAQ4dTfhcUSRIpK6vmxZeWUVZeRauW8QQF+bFn72kiI4NRZAWL1U5hYTlOp4y/vw8NGoQgCMJ1A2Dj+rn/HRSiUqkQBLhG3+cCxpoaM88+v4Qf1x/A6ZTR6TXce89AHnl4FL6+emPnTk3/KSiiKHAdx/d/j2UJgoDDIbN33xnatknAYrFBnQG7+87+cR4wbhk1hzNns9Bo1AiCgMVs4533VlNUVM6770xHFAVj505NHzt1OstS/y0KsqwgSiLl5TWYzbbrOuv/52SILMus/+kgEeGBREYG4XTKdX+eCrDll6OcOJmOwaCrB6bBoOPblbsYO7YXvW9qDXDH18u2LQEXrgoKgiCg12txOJwUFlZgtzv/D5DfZ1cSpaVVLFq8iajIIGpNVmTZBcqE8X36AqxctQu9XnvV5zUaFbt2nfYAkvjpwg1uwMDgqycsxJ+gYD8iI4Jw2J2IokB9zP8PkKuCUlFRi1qtZsDNbQkM9MXpAqUYoHmzWLZtO45Gc2UXnE6ZgECD519748ZRqCQRs8WG06kQEGBAq1GjVqtQ5L+X2i/+nVVASRIRRYHy8lqCg3zx99UDLAG4dXQPbDY7l9tRDocTg0HLnXf09Xy16rV59/Lcc3cQGhqIw+Hk72x7/a0B8VBKemYhP28+4hLwsBGgVct4Pv/sMdRqF4VYLDbsdidhYQEs+vQRgoP8AJLffX/1yeMn0nn/g7Wkp+ejVkt/6/7+bQFJPbdYm566pMfeXW9P3bJxntpqc9CuXSIHDp6TgWRBEBg9qjvTHxiGxWJjxPCuBAYaeOH5O+nTu42nmm/8/HxITcvj9OlM1GqJk8eSG+/YNl/zf5b6HyxbNr3iD7x+mQX+2fq1LyUDLwOzAaPD4eTThRv4dOFG7A4nE++5mWdmjeeJpz5DURTGjb0J4NdpU4YkT5sy5JFPkx++yV3nOIAN6+cmAw/dedcbzv8D5LKyeNGjHdyDFQSMBbDZHJw8lUl2dhG33NIVlSR5ADKWl1cz65nPadOmEYmJDdiz5zQ1NRaaNInmyy+e4KW5X3M+JZdHHx6NwaDzPgeQk1NMZFQwapVkBIzLl81KBp4FAoCc+6d/IP9/y7L27XlH+ujDGQuA34BpHjC++HILI0e/yNx5yzh+Ir2uHWI8czabaQ98wPT7h/OPGSNdtkUdIR0QYODdtx+gSWIDHjB+SHp6PoCxttbCjH98zIMPLWDo8Nls+eUoskvDMgLlbsekc+EnDy/4/w6Q9NQliTt3vLkAcHgG65nnljDt/nfJzCxEo1bRsmVD1q5+kWefvh2tRg2wGWDhoo2EhvjRoUOiy+a+hsY0/rbeVFbW8OXXWwHIyi7i+Il0liyeyRuvTubtd1dRVWUCoLraTF5eqacu4+kTn7bYuvm1Fv/zgHz3zbO+mze9sgBIBYweY+/lV1YQGGBg1MgePPb4QpKS4sjIKOT2O1/n1de+8Tx+AaBr12akXcj/p+/KzSvl5Kks+vVtC0DD+HACAwwMGvocx09ksPyrWQQGGlAUmPXM59wyeg6VlbWex08Dp7f98vqCvbveNvxPApKR9sUCoBowmkxW3npnFSUlVQBkZhYydcoQbhnRlYceGsXPm39Dr9cwfFhnHnl4tFfeA8THhWO12HA4fl8Wp6XlUltrpnFCFIoCBw6c57tvnuXZp2/n3ffXcORoGgCff/Ez+QVlNG0Sjb+/j7c9mVmFHnZWcyHl8wX/M4BkZyyN/3nDvAWA0WKxsWfvGXx8tGRnF1NdYwagZ48W7N13FoBmzWIoK62mf7+2mMxWgoJ8wRXfSAOIiw0nv6CMnJzi333v2bMXiYkOJTTUH6fTybcrd/L4k4tQqSWCgnxp2DCCV1//lnXr9vOPGbfg769HFEUcDifPPLeEzVuOUFlZ67F9jJkXvlhw4ugC8b8akNRzi3XALMB48lQm815dwedLfubFOV8hyzLZ2a5BvfPOfqxZs4dz53K4cCGf8ooa+vdvR0FBuceHZfS4TPz89IiiwJmz2b/77pTUXLp2aY5Op0GlkvjgPSMdOzRh/foDzH7uDnbvOUNGRgF9+7Thjfkr8fNzUcey5dsBuH/aUKbP+Ii585Z7ZQvg3PTTywv+1oBYLLYrLpvNTuq5xQsAM2BcvWYPd979Om3bJLDwk0do3aoRhw+nUlhYDkBIsB+zn7+TZ55fwmtvfMPUKYNp1DCCF2ffhSiKHgqxuwDxoU2bBH77Le2abbLbHRw4eI7QsACvHaPRqLhv2hAWffoIAwd0IDwsgDdem8LMR2+lT5/W+Bp01JosfL1sG6+/NoU1P+zl150n0eu1CIJAVZXJq5VtXD+3vce7/Fev626HjB3T6wr/09tv3qcDjFarnTU/7GXR4k20bBFPbGwYkiQybmwvOnZI5KW5XzN0SCcCAgwkJESyeuVs7Hanx2GYDDiBbGAAUAKugFK/Pm35decJvC7cy0p+fhl5eaXEx4d7vprm/lwN6ICsYUM7ew3P6AYhOBxOFi3ayKiR3XE6ZN7/cC0T7+mPTqcGYPqDHzF+3E2MHtUd4MjG9XOTBw557rqHif8yID171NcObxt3kzfOvXPXSdau28+6NS/idMqMHT+Pjz94kKZNo2nUKJIhgzux/qeD3HVnP9zuEGMdMMI8VjVATY2Z9IwCcnKK2bP3NJWVJhRF4WpRjOMnMlyh4rRcNv18mMYJUcTEhKHXa8bUue1T4BPg6L2TBlJWVs33q3YzfFgXHn40mRdn30Vqai5ms5U5Ly8jPi4cURSYO285s5+7A0EQjFs2vUKjxHtn/K0Aqavt3HF73wWA0emUWb5iB3FxYXTr1px331vD88/dwaJPHmHuvGW8+cY0QkL8mTC+Dzab3fP4LGCbG0wjQFFxBTt3nmT7jhPs23+OvLxSzGYrTqdMQkIkFRW1SJJ4FYGejd3uIPmTDSxctAm9XkNCQiTdujZn0MCO9OzRAoNB9wDwALASKAkO9jPef99QTp7MoG+fNvTr24YTJzP4ctk2undtxnvvTMfhcLLux/3MfHwh7759P4IgGDPSviAm/p7rBorwV13Rw255AYANP85VAXanU2bOy8tITc1j+gPD6NO7NW+/s5raWgsvzL6TX7Ye48jRNJ58fGxdHpqMK6A3HeDsuRyWr9jO6tV7yCsow2F3YjDoiIkOoWmzGEJD/Fn/00EWffoIH368ji1bjvDtimcZMbwLiqIwcfJbHD+WzoCb25OZVUhKai4FBeXYbHbUajVNmjRg/LibGDWqO40Touq2wetikWWFyVPfJju7iB/WvESAvw9ZWUU8/GgyhYXlDB7UkSefGIevKyQQEJ8wqepfGb+s9KXXl0Jqaizs3D7fF5jvkSHHjl3g8ZljOH/+Is+/sJSpkwej1aqY/9b3PPHYGPr0buUBo94gnDyVyWefbeLblTsxmayo1RId2icybGhn+tzUimbNYvHz0wNw5EgaW7YcQa2q706vqKjl119Pctcd/Xjt1ckAlJRUcep0Jtu2H2frtmOcPp3Fy6+s4IOP1jFhfB8eeXgkDaJCjHWAMQKMvKUbHTs0IcDfh5ycYm6/8zWemTWBwYM78vmSnz0BM4Bmjz166yHl70AhbiH5DmDct/8sGzYeol3bBJZ/s4P09HzmzZ3EL1uPMXxYZ8wWG4MGdECrVdcDw253sCD5J15/41tMZis+PlqGDO7E7eP7cPPN7VGprmRL02d8SEVFDRarnS2/HOXb5c8wYngXDh1Oof+AWcx58R4emznmiufMZhs/bTjIl19tZc/e08iyQlxcGLOeHM+E8b09LDC5rrf54MHzPP7UIua8eA/9+7W91jgkr16950+zrjFjel53b68RMKal5fH8C19yx+19+WXrMea/NpXw8EDOnc8hM7MAk8nKyFu6XcEeLl4sZsZDC9iz5zSKotC3Txsef3wsvXu1+t2Xdu3ajI8+/hF/Px+PagzAoUMp2B0ybdsmXPU5vV7DuLG9GDe2Fz+s3cv7H/zAkaMXmPn4p+zcdZJ5cycRGupvdLfxN+Azk9nGRx88SNs2jeq1varKxIJP1jP21l40adLAOGZMzyWjx7x86D8JSLhnJk27/z3GjbuJaVMG8/Pm3/jy618wTh/BN9/+Sr9+bRk+vIvnmTbACYBDh1MYM3YuNbUWfH31PPXkbS4P7h9IAElqHkdNjRlBEBDFS5lCqWl5BAf50rJl/D+tY/SoHgwc0IF5r6zgs8838d33uzh0OIU1q2YTFxvuASW5b5/WV7CzU6cymfnEQtq1bYyPjzfZouTAwXP8FZ7zVwF5CWiya9cpHn5oFDt+PcmMf3yMwUdLbGw4r7/xLf36tmXUyO51KeMEwIaNh5g05W0cdieNGkWx4MMZdOuW9IdfHBrqj81mp7ioApUoIYguSI4eSyM2NoyQYL8/VI/BoOO1VyfTvn1jnnluCZmZhfS7eRarV86mbdsEDyi3uv1wv9TWWvhs8c9s2HiIRx8ezfBhXer2rUqjUf2lmP1fsdQXAMaMzAIenvkJPXu05IP3ptOsaTSyrNC1czPeeev+ug1u4vnDA4YiK3Tp3JT16+b8KTBc3tsIwsICsdrsIIBOq8ZqtZOalkeH9o29sfY/Wsbf1pvVK2cTFxdOVZWZUWPmcPTYBQ9LHgTcDPDF0i2sW7+f1d8/7+nbyjqsuyQrfemC7Iwv9f9uChEAo6IoPD97KU88NhYfHw3fr9rNIw+P5uSpTJZ++QvPPj2BYNdMTQYeA4xHjqZx18T56LQaIqMDeemFu5FEgZyLxX+yAQItkuJITy9AEARKS6vZuu0YFeU1hIYGcPFiCcqfZB5RUcG88PydzHz8U6xWO+PGv8LunW8RFRnsoRTGju3F5l+OcPZcDp06NgG4LS+/jJKSSuJiwwgM9PWwt3/JNvlXtaxmwLmiogoee3IRUZHBlJRU0rhxFM8/ewfPzV7KTb1aMnhQJ488SAaMBYXl9Lt5FlVVJkRRQKfToNGosdvt/9KcUBQFi8WVBqrRqJBlBbvdgVardgt65U/XqdGoMJut2GxOHA4HSc3jWL9ujkdOJAPGX3ee5K23V/HN8qdZ/s0O1v24Hz9fPXn5Zbz/zgMehSL5jxiMF7O+ui6AvAc88vob3xEXF0aDBiF8tvhnIiKCaNumEZ8t3sT6dXM88YVkj2o7dMRsTp7M9Ki9KIqC0ymjKJcif7KiIDtl7/8qlYRKdfXUHVeitAtxt+MPURSQZeWafNxud7hCwoLLOSiJotvR56Y7wWVLeeo1m61MmTyI11+d4v3O6ZQpKali5apdnDqVyfzXp+Lv78Padfv4fMlmVn8/26M+q1u1NTp+byBPHU++LixLA2CcPpz5b31Pba2VpUse57cjabz73mrmzrmnLhgBAJ8s3MDx4+mo1SqvdiRJIhqNGkkS0WpVqFQSgQEGQsMC0es1KLJCUXEF2dnFWK32q7pJ6oJT/+9L/yuKK1/YYNDSMD6OkBB/nE4Zk8lKcXEFVdVm7HYndrsdh0PGYrF5QfXx0bLos00MG9qFPr1bAyRLkmiMiAjkxx/389nCR72BrRYt4snNK8NqtXsoSqfVqmtuNMtqDKRlZxfh66vH39+HFd/8ytlz2Twza4LXkq5va5TQqevDKIpCXFw4Dz14C/7+PoSFBRARHoTeR4tep8FkslJWXk16ej7bth/n9Jls8vJKsdsdqFQSZrMNm8014TQaiX/WdM+gajQqtFo1dpsDH4OO2JhQ2rVL4KZerYmLCyM4yA+NVoXZZKPWZCE/v4yysmpKSqp4573VVFTUEhoawK4db3oCZhuAYfc98D5JzeMYf9tN5OaVMmfuMqY/MKyevRWfMHHG77tOvvzLgCwAjPPf+p6cnGJkWSEgwIcTJzKorKrl0+SHaZEUB9Ad2CfLCpMmv8WWX45gtztZ9tVTDBncqU6oNY9jx9PZtfsUR49eIO1CHk6nTIMGIYSHBWAw6CgsKqe4uBJFgVtGdEUURZav2HFVC74uGKNGdnO5dnadRKfTEBkZRHCQLxWVJgoKyigqqkCv15LUPJZOHZvQvXsL2rZpRGxsmLeeDz9ay4tzvkaSJJ59ZgIzH7kV4Gvg7oKCcua8/DUmk5XAQF+GDe1M44QoNmw6xMS7byYw0BcgrH2nB0uu1c6jhz/+SyyrNWBMu5CHKApMmjiAxMQG6HUa7HYnOReLiY3xdmaixy7Y9PNhRFGkQ/vGRIQH8snCnzhw8DxHj14gPb0Ah8NJdHQIHdonMnBAOwRRJCOjgLNnc9xqbCJPPDaOnj1bEB4WyPZfT/DF0i2oVNprk74AI4Z3YeiQzmRlFbJn71mWfvULJ09lERMTSt++bYiJDqW62syxY+ks/Wor77y3Br1eQ7NmMXRol0ivXi3p2bMlCQlR5OaW8Mb87xhza0/i48LvBpIjI4OMyR8/RFlZNV99vZXlK3aQnV1E27YJdVd+aeQ/ken1ZwExAvga9AQH+fHD2r2UlFTRtEk07dsn0rVLs3raiNMp88KLX6FWqxBFkbQL+QwY/CzV1WYiIgJp164xE8b3JjGxAQ67k7Pncti24wSpqbk0SYxm0qQBDOjfnoSEqHrWu93m+Oe8WBCornbF7OPjI4iPj2D8bTdx6nQWq1btZuPPh9lQeogWSXH07JHE5HsHYrM5SEnJZefuk3y/ejeLFm8kONjPGwa22RwsX76dZ56eABDiCYZNvPctOnVswgzjCFq3bohP/WUSz5eVVc24ETJEAhyyLPPs80tRSSItWzYkLjYUlVri0OFUunVt7tHNJwDfHj+RwdDhzyNJIrKs0Lp1Q3p0b0Gvni1oGB9BRmYhu/ecZtv245w7l0NS81imTB5Mn96t60b7rijbtx9n3IRXMRi0v8uyPl3wUF2XzVUyU/L48acDLP78Z0rLqmndsiGDBnWga5dmREUGk5Kay67dp9i79wwX0gvcGpjEr9vme9u3ddsxnn72cw7t/6Be3cXFlQQF+6KSJAB1fMJExx+RIX+GQpxAsiiKxscevZWz53I4fDiVz5f8jCSJdO7U1CM7vJS0aPFGZFnBarXyjwdHMuvJ2zh3Pod1P+7npbnLOHUqk4iIIAYP6sC7b92PVqdGq9VgdyikpBZ4VVlQvAJckkQOHMrE4dRgsV47k91mc3Aho4TUtAIcDtnLxjyqq0vLUzFsaBeGDelMYVE5K7/fxQcfrsNqtbrZZwcmTRzAC8/fyaQpb7N9+3FEUeTXnSeZeM/N3ohpUJAfH3y0loRGUezec5qMzAJqayws+fwxwkIDAEK1Wk3B9aYQH6DWarWTlpbHnr1nyMwswCkrNGoYQatWDenWtbnXZigpqaLHTY9hMltBUejRvQUXc0s4cyaLoCB/hg/rzLixvejQIRF/d8bHfQ98yJbNm2jSxIpyTa6koFar3K4R5XeNPFmWsVjs7vuu9FgKEpxP0fLuu88xdkwPAEpLq9jyyxHWbzjE5s2HcTpd7h2T2UZqai4g0KVzU1atfN6rhqem5vJx8np8fXW0atmQxo2jCA8PxN/Px6OVhTVuOuWqgv1Cyuf/MoX0Bdh/4Bz3T38fs9nG2DG96NnjkmbiBiMZMB4/kU5lZS1qtQqnrLBz10n6923LIw+Npm+f1kREBF1JgoqaLu2rmfXEORSTgCAoaDVOrHYJRb40oIKo8E91XgFQBBSljn0iKahVMlabCIqAoFOYN785Dqf3nl9CQvwH3D6hL7dP6Et+fhkbNh7mu5W/cvZsDpIkIIoChw6ncPpMFm1aN/LKqAfuH8aFtDwKCstZ88NeUlIuMmniQG4Z0RXgZrvd8e31Fuq5AOHhgSxeNBNBEMjLK6W8vIYf1u6jW7ckburVEo+Pa/EXmxFFgQ4dGjPm1l4MGtCeBg1C6ta3EfgO+BnIcxlwAlqNk+AgKyo/GZtV5PufGjN6cAZ6gwOH1TUjrXZwOuy/SyGiIKJSq1GpFFBAa3CSn2dg6+5oxt9yAZtDRFELaNXOutiedLPbxwBjVFQwU6cM4t5JA0hJvciePWdZuXInR49fYNOmw15AUlJz+eTTDQQFGWgQFULPHi2ZML4PiY294eENHuPxegIyDuDnzb9x4OB5fPRatFo1BoOOfn3beMBIxr2Yv0unpjz71HhatIiva0UfcqfifA9ogYtAVX2G5PJBX8j0Z9P2OL74thnFpXpuGZxJbEwNTpvA0lWDGTD0KbTXlulkZlaQcuJZptxzDtkJh4+F8eOmhmzZFYvNLjGoTw4B4barPZrjmVSV1ioCdQFIkkhS8ziSmscxceLN/PZbKllZRa6sF0GgVct4Jt87kAB/Hxo2jGDuvOVs2FjFSy/e7VklPOLJx8euuN6AhAA86s61dSXF2cnPL2PWs5+j06rp37+dEUj29/cxPnopJ9cD1MLnXlxyLMX3ECufXFB3S4zkS7NaoaZWTVaOL0dPhfLrvgbodE52H4wkLqYaGQGHzUl4qC8Dbu74u42NjCrlwmmB9GxfRAT2H47g6KlQRFHhl13RNIypIsxiocakvjwgFgdQbq4gozyTjtHtyarIISagAZIgoVGr6N4tie51wgUWi40X53zFnBfvYc7Lyzh7LocO7RJ5+pnPWbxoJoBx/U8HrwrIpIkD/yVA3O522H/gLJIkolarEAQBu91BrclSd734WuBD94CvBPY+MWuRk5gqTkbs5ecZqxYAxvzqAoL0QehUWm/sWlYEDD4OYiJriR1aS5e2RUwwDuKVWQdpmFCNbAVFUEjLtmKxgE537QZbLFYC/W3ExdSCFe6bfJakxApe+6gDH768G0mtoKjBoLdfLo5mAtTaa2kYFE9OZS4+aj2S4JWPTwJD3R7vZUBGRUUNFRW1dOyQyLxXltO+fSKDBnbg4ZkHPHU22Lnr1HVlWYLHO3vw4HnKyqu9cWxZhodmjKRLl2aee7eMG/+yrNNqH66ptdBvcEtuujOWUW2H+MPjXQCjU3ayKXULk9rfdcVLZBmsNgks4KN38PYLe4kKNyHbBI8CfFWN6Z81X7ZAm6RS5j5xELtDxOF08UdZEa5q/FZbXT7BvKp8usR6XT0Pr/pxuWP9DxXfh4bqePONKWpX9DKAFkmxTJz0FudTLvLE4+P4Ye1eBtzc3vPc1svk518GRHbZIILxkfqs6IrMiy+/+kU2m200iAlmwn1duaV3H6kOxQCw/MR3bE3bweQO9wDsBHq7lXCcThGzWUIxCwgqhbAQCxu2BhPgX+ty0+OkoESFRvv7DVar4WyKgK+/3gtmRaU/vbtXYrGpUGQQZAWHU7wCXgUFtaQmryqfKP8GCK47PgM6DOqz56CaEPbsjadN+0ftJ46+l6xSScYP3jPyw9p93H//UHp0T8Lf34fQEH/vuFRU1Fx3oT4DmAc87x7c5MvkwFOzZ39Zo6Bw+9QetO4YS7vYVl5ZYbabUYAfz23grV3vM6BxPywOKzqVtne9OKCgIEkKSApoIDXdQKn1CcKD/ZEkFbW1JmzFVWRnF3H4cCqhoQHEx4Wx9sf9BAb40rlzU86dy6FVyxj6DppNdXUtOj8NigIVpcUUFr9CXLzDtXZL5ZJbl6U0UW6uIK30AsW15XSM9s7yacA0P/+Pk0fewlutWs5Mr66WadvhkRnHj7yPTqcx3j6hzyWnX6uGdeXncecf3CpC/NO0D8aSkkoAY2lpFQUF5R4yr3755YkLyitqaN0ujnaxrSRvhkbhGd7bu4DPDn3B96d+wC47OHjxNyaunEa1tbqeiiWKoFHLqNUyapWMKNoYNfImJk4cz+AhQ8jN19CjR1eKi6tITc3l+PF0Xn39G5o2iWb4sM4YDDpeeGkZXyzdxolTNeQXapg48TYmTbqNQQPag2L11q9Ry4hCvUCvUVEULpSm88nBLwjS+YKcQ1XVU1RWv4rdcc7T1ydiYmp50GhDUewAj4IrrdazCis9vaDuGkY5O+PLBYriCgfUvf4KIAvcairbth9n8ZLNvPr6t2RkFlBcXOld2PLxhw/6nklPB+gEUGIqJUDnz9O9H+Mf3R9gYvs7+GTU+2y69wf6J/RhZ+aeeh5ah0Og1qSi1qTGZFJhtUlYra7NfE4cT2Xr1kP06Z1EaIgf0dGhVNdYGDy4CwcOpZJ2oQCz2cbAAe04fTaHzVt+o03rmHrakNmi9tZfa1Jhv4xlKSiUmcs5WZiK4NyKpaov1bXLqK35kJKSsTgcrsQHjWbhzRWVeqxWhyeCikol8dKcr9m58ySvvvYNJrOVgsJy7/qSnMyvBEmSqHv9qyzLH/eWSMtWbKeivIYPP16HKIrkF5TRqGEkzz1zu/dmh+IEmAQQrA9C9AnxJiaUmyvoGN0BvVrH3e3vQK++pCrJCqhVMn6+dhTRZanotA60Wtc9/fu3JSu7jOUr9tC/f3vat2+DpDrFuXMnOPbbLizmEnr37sXEe27Bz0/FDz/soUMHr7KBVqvB4GPH1yCDAwSd63115mmyKIjG+MB4QvQqfIQ15NeAn0ZCViRkuYya2kUEBrwOYDSZ1FvPn1nYGTCeOZPNjp0nOHUmm2+++5WgIF+mTnuHyZMHM2RQR4DkBx/6WBk+rPN1kSFVQLKvr94YGx3Gy/NWeLWsnTtP0a5NQt0seM3Em0cvAIxmu4Ufz20gvzqftlGt6R7Xjd1Z+6ix1nJXuwkE6PyvGseQJAVFcvmXJUlk67bfqKgoQBQEEhvrqa218eu2r6it2ki75sdofVMxI3uAqF5Odm40u053IDh8FJ06tuDsmZOcPeMyOA8fTqVzUwlJ5XR9IV2xvMQCYHFYCNU72Zrlx5gmNQRo7ciKgCCoMJvX4Of7EJIUPXbggPldgCPgWor31rurOHs2G7VaRXW1hYOHUpj+wAicThlJEo0ff/jgzM1bfrNeD0CaAMbsnGLWrNuLWi15146LosCpM1kcOHiegQPa49aYjBaHhZ9Tt9A+qg2DEvuTX1PIe3s+oqC6kBRVCt+fXsNtrcbgr/WrB4bVJlFWrnVpWZJCSLBMbv7LZJ0xociXYuThoWbadrRTVaFw4PhIYuL7czHjO5IST9K17S4yMg5w7jcdgkdoCyBYDKjVGspKBZAFBK2CzXalluWrMVBrB6cMzUNsOGQBrSRjcwrYZRMWy0YMhmkAPYCGABs3HSI1NRdJEt0WPOi0GjZv/o3ExChP4K7Rpws3nlPcjk9FgUEDO/5LgBQDBAX6MvvZO8jKLOLM2SwEQUClVnHrqJ4eMJKBXwEcsoNbmg9DEl18MlAfyMGcw2RWZLNiwhIMGgN2ub5LV1FctkeDyFoUk2uYYiJq6dreidMpXMbrneQXq6myz2Dw6JeIi9Fz5uztXDj1GAkxK+l/k4TsrK4f0JGKsVglr8NR0Cvodc4rPGKy4qTMrEItOvklK4LMShlJsNI5ykpCQA0qlTfnrwLYC9CpY1M+/vBBbh37MjabHZ1OjSiJ3DdtqAeM5MTmD5yTJNCqrRh89VeVIX9UqFcAyX5+ehpEh9KnbzuSFzxC69aN2bLxVYKD/JAvLYysBJJ9Nb4eMJI98mRw0wFMan8nBo1r6bdaVNVznXjkiNMp4nQKOJ0CNptIba0ai0WF2XM5VOw82Iqcqs3cMvYN4mJciRUtkiLoM2QZRy98x55D0ZiddZ6xqKipVeNwXKrb6RSv6jSuslYjCE42ZwWzu2A0Jbbe/JTug3FzBIcLfBDwrmcXX351wkV3OBfZqTDzkTHcMaE/908bwYplT9dl5ckBvmW0aWElMjyHbp21DOwf9i+zrNbeBySR556+DYAO7RKIjg4hMfHSopf4lt3k9jPjZvww7bvngFeosyIqRB9Mx+j2LDq0BIvDyvQuU1FLamNdlmWxShSV6FHM126MIiqIooF+/btwecaovx906tyWozsliop14Ly2VS9oXe+7/I6immJkxYlNVnio+xQa+DmpLF3OrzmB5NSEoVZ7BfO+6MgYgMWApl27hKnt2iVgtdqprTUTHFxPRhr7dP1thsnRmaoqwZsD9mcBae0ezEbAkP17luG0fom5toqgoETUPj2xmjsQHdsWrcv1asw6vZ9VW3+Y4QHD4rBysuAUF8oy6N2oJwdzDtMjvhu7MnfzyE9P8f7w+agltZdladQyAf42FNW1kxc0WgeK/RjfrVjItPuN9QbU4YTD+z6lQ4t8tBqw2X+nizpQq+UrWFZ25UViA2KY1ulehi2dwPLxrxCvbUXvmFNYlHYIYqg3vjRlytte47eiooyUcztQnKcQHb9xtLoQH784QiKfp2mzNsZ3Pk5vCOwBkmfOnFl2Neq8VmujgefqujsK8jPp1GQiqgDZpaE49wNfo1ggO60VuaUz6Nh1Clqt1jj25tEAxozyLJ79+UVOFZ5BRka/S0+3uM48FT+TVuFJ9Fw4kGP5J+gc07Ge5SlJMookXNUsdTqc7DkQRY+uuZhtyRQX30t42KXc5hMnzhPh+yVOReTgsXC6dszHcQ1QBElBrP+aKIDi2mJ6xHVlXKvRJB9YxJu7v+XDoY8g8RVR/vM9vrRkt/1hLCjII+30PJonrKZLi8JLoyoB6gOcOliMw7EZlUo91O2YnPfuu+8mz5kzZ8YfAcTPA0ZeXjb52ckE+5/DXJWJ7CegrpDQqEGvldFoXatg4+NO0SBoBnt+zaDvoPkARrPDwpHcYySPfh+7087Fylz2Zu9n1em1WBxWREEkSBdIQnAj6rrfzRYVeQUG1wr3y8BQkMm42JJGSXM4ceZ+RIo4fHA9w4bf5qWw86e+I1BnI6egD5GJj7Ntz4M0TShDcV6dQswWqS7rCACw2C00DHKtL9Gr9dTaLQQYRiL4jq7rDnkBKDaZaii6cCu92h505XPaARuYzCIWK1hsAmGBu7lwYihqjT+19hEkNr8DvV5vfPHFF69Iyr4aIG8Axgtpx9DbB9OxbRHYgBjACnaHgNkqUlEt4ihzqaX2agFFgW4tP6Wk+AlCw8LRSVpGJg3zsqMwQyjtG7TFT+uHzWHFT+fH+yPmE6QPrKNlCajVTgIDrCjqyyhEVCgp0RDXdA59+/Zkx+YJRPi/yeGTizGZb8NHD7l5VajllcREOrBrJ9ChS29M1Y+jVj2O3leAyzy7gk5xsSylngFMiamMUS1GuORJbQmvDHwQQRBx2xzJnyT3+2y6cfvtALnZ22nT9iB5GRJhYTI2m0CtSUIUZXRaGX8/GZUKItRb3SO+hhNHl5PYbh0+Pj5XZMqrrkIdRpvNRgDTCI0tumJbe7VKQa1x4h8AtTUC6ZlaQoOdhIU6QF3FidM/ERo2GUEQPGAkAx+7SfXNKL9ICmuL8df513Xc1ZMRrk/lchcs/kEOzLb3mHrfQQ4dLuPecfHotBd5+OHXsDt9EeSL9OhgYfGKJDb8eppRw17n1iFbUEtuJnN5nUK9D4CeTsVJcW0xpaYyHLKD4c0G0y/hJs/vI37ZMjl/8ec1TDdyF0BJ3gqaxEFomExungqnExo1ciCKrtAxsvvymINWaNNuK2kZ79C4xfMIAkbgQU+w9HJA7nVlZK+mQ9PfrmQbdQbHZhGorpJIaGhHq5NdL1UgJugrZHmSx5L3f+yLp6tjEqN5rNdD/QEkQcQpe/nHw7gW8Fs97ndZdqmkitvuUKlkHG71VCWAXvUzPdru5+Txjrz5SWf8fa1UVR2h1qQQ4C+y/2Anyip0dGx1mA4tTqCVzAiyBoebJUqSgsMhuqIqDuUKtbfGWktaWTqZFdlIosRzfZ9C5Vbf95zelL9tq8ihQ4d6AyNyss/Rusl3YAaNSqFhnJ2KSoGiIpGIcOcV+HuLFUK0r5CbexcxMY0AWgKnrmaHDHE9sPZ39S9ZgdJSFSHBTrQ+Mg6b6LKi7dAgcjspZ9d72V/gRRsH0w/g4qxgtlvw0XgD/jvcXLeecWizS15gUtIDsdtFZNllk8gOH9q3szL1jjT8fG08aTzKP6YWM3JkT54wpjLptnMEBVqYfm8KTRs7sVt12OwuAExmNWmZAW5tTMTmkK4IUJkdZvQqPdsu7MDhdHjBOFpwYsZvu87z6uuLDR5lp6p4Ab5BbjeMxjUhA4MU9FqF0jKVS6hfrTggKMRCScFWzzfDrmUYRgKEBxdcNkz1TcnychV+vk7UPgoWk8SUx/u6Oqp2V2J4CZvNCmB84fl3ejbQRIL7VwWFIJ13Uxh1fcoTkCQFH70Di13i0PEwHnq+F/t+i8DqlNDrnei0TkSHQu/eecwyHsXhELl1aCo9O+UwZGAmNbUaXnnqAImJlUiygk7rRK93UmnSsHF7LE+83J2UjABEUUGvcyCJ9SOQguLyKhzJO865kpRLVr5TIMDgC/AmcHt+3lmaxy12TTMF9u6JdEVT9BAQImO3CdTWSNc2vRUID86uN+5XA0QP4Odrd1GI9kqm5nQIWG0Svv4yiPDQszfRokk5TVpXuvrlgMCAo2SnvO41iN65/a3GuDa4RBIlb3j0ckAURcZqlagyadhzIJK3Pm2HRiPzweet2XcgggqThvJKLRVVGooLdMQ0qMGgd1JYJOAjbSAv10CLJmWEBlsoLdJRUem+36RhzY8JfL2qKQ6HyKsfduBChj+VNVrs9vorrfx0/hjUetSimhMFp719aBPdWph0z1SXn85ixllxF5LahCLBnHc6sWVXDJs3x/Ll0mZYzRLhYQ6cDrF+ppJAvXE1XNLW1dcS6plAUk5BAqXVZhwWHSGBGYSE5Lugs4PVLBAU4ALstTc7sHV3DEnNynn86R7065bLiOFZYIfY0Je4kNqPxk163+X2Fp93uUvUOC7JkHrOLB+9xMl8A8F6C8OGZBMTWctTr3TnhUcP071rATq1E9kpYHdIVFY6CIuqpWVTEJBo0aQQu10kMd5JRaWAyexLcDgIKEhqmSn3nCU6opbF3yTxxrP7aRhXjSHIjkZjx+7wRvOSfdR644KR77ExZTMTWo+pq+a29PjpMs7OIanxURBh6+YY7E6RPt3yeH5+F5wOkRaJ5XTqXoS5FAw+ApLaFf1UzFBc1ozKmigMhkJqHGEERF3yFV4NkGRgaJO2C9FoNAiCSFVVBecu7kewzKVZ833YK0CnU8DpkuN3jzuPJMqsWp9A57ZFLhqzgVYHfpVDyck+TGxcknfR5M2JfetGyo6v3rpdGXNzv2TA+MhDw+jw1W6efKE7ye/tpEuHIowTT9G9UyHFJRLBfiYiohUupEVyImcppuojCMoRNFIhskNGUqkxO6JQqXtgdwYwsONUwhtYyclUI6hl+vXORZIUWjUtQ+Uvs+6HOI6fb8eCAS097VkCEOkXYZzc8Z66Y/IR8A+Andve46b2b7hYlT9U1WjYua8Buw9EseKjX5j7Xie0WtfgaDVOnE4FQS1y6swUdIHTiItvQ7hOj9VqIfySczH593J76+ZMHQE6ANjtNg7vX0B84PNENahFkN2k5wPzXupIrUnN7eNTOXMqmDtGp3oJsbikKbLvdiIiGnB5HP6xNc/N8DvTHMUKc+feswS49/X53zH/zXXcPTabcaMuoFM7KK8QOJn+MAYfhQsXNiMJOp59aS0B/josVqiqsuN0yqjVEv7+KjRqOHToFF8snIx/UBBNm42kojyT1olf4+sDdkXk9NlgXn2vKR9+MItRI7sCJC//ftuMbfJqPhv/USdguLu9ER5/3NHDP9AucQKCbAM1lBVpKS7Rs21fNE0bVrLnYCSyAi8+cxjBAaYaOJPWBP/opTRt1r3uGB8EutQBY0Ydvq1c7QpUFEV0/x2uKMoCxV0O7v9BkUtRlGIUpQLl/P5AJanl7cq4Wwcr/zDepLz3Whtl29po1z1FrnvyziUpBQUXPVUsUBSl0fQX72PSP+7h3Xc/Y8PGIyiK0kVRFMXpdCqTJr+jBIbeqbz/emvl0K4w5aflYcoPa9YriqIoaWmVyvmUIkWWZeX3itlsVU6czFOyc0yKoijKRx8uVDYsD1MObQ9TtqyNUdq0Ga7Mf2td3Uca7dx5hplfzOKtze/Q9+X+KIrS2vPj+bM/K9VZkqKUoSgmlB+WNVJuGTZU6dN7lPLOq22V7GO+ygfzW7t+L0FRSlEqMhooJSVFnip+UBRlsKIoftOndkZRFIOiKH5XxNivAcjVruaKoigOh0PJP9dOUcpdA+4sEJTcEwZFLkWxFEnKQw/0UqZO6qsoNS4wlEIUpQol63ickpebUReU1vfcNYH7po5j/vy3+XXnWTzAV1bWKp26Pqro/CYp8+e0V3JS9MqnH96tnDtfo/wrZdeeHOXrhT2UnDS9snVNjNIg5jbl9jvfURwOp7c9x46l8/prr3HftIm8MPsp6k3CfV8r1nxcfa5G+fWnKGXyPf2UwjS9UpymU3r2vFU5sj1UUezuieruc9rp9+r2N3DC+OFMuXc006d25oH7RrBq1eorxvnPJDmcA5IlScKpX4jT7tIaREmhQeNa9hyKov/okURGmnjsgeO8/X5bMtL8XaqgGeLis/GnKynnfvXETU58+fU3C+wOB5s2rmDN6s/4ftWuGe5UVNatmU27NqG8s7Api5a0pnnCenZsHsX2bdswmf9Yg8vK7fy0/lvOHRlJy6RTbNiYwIxnOtGjZ2eWLH7Qu/PPll/2zdi4YRlHjuxi4aKlIXPmvrEAMDocDtLPf0qnJvegcdsZ+bk+zJzdi6H9sglvbCY02EJcdDUGvcPl1VBckrm6LJKI2Ls9Tflg9arPKkJCwnA4f3/1l/TSSy/9mTSgncBA/4DoBhfO7yYkLMOlJwlQkOdDq2bl3H1PCi/P70R5uY7dh6Jom1RKQIgNbKDR1uKn+4IDh0QaxPZCFMXOo0ffHjFp0v0p816eWVaQn8aYsRMOAb5+fvrOt43tyc5d51n5A0RFwID+x0k/v55lK9IoKtHh7x+KRqNFUrlcLrIM1dWQmVnCim83sXHtqzQI+ZCOHYrYtTeOuW+24OaBA/h80Qy0Wg1A8qpVG2Z8ufRtKiqKWbZ83UBcB47dU1paxOnD42jd9H2XvHR7IjR6majwWma/0YUwXws//xJLbHQNw0dkX3KPqOBi5RIiG3QCSF68+LOlDocKrU5NRXkJglIBoi+NE9uTlJT0Ly/Y8ZQpwOKS4nT87E1dGoXT5Tm11YrMmtedof2z+XpVU8YMSyfzoh+PPnrClT5gc7ukNXA2ZQR+4e8SE5tYV9OYefuEUdZvvl0bAbyIexvyJ2Yt5oulB2mSYOLlWYdp3yaH8jKJ0vIECsvbYrMHo9XqsFpN6LUFhAedJCwkC39/gaOnGvDqu+04fjaIp58YzDPP3IbGFdVK/uqrFTP279vMxwuWxOPezhbg/NktBKlnEB6VBia3Liq6lXTZ5fE7fjiU+x7rQ59uebw5b5/LkJZdikxObkfCE3d7smUaLljwUZZn9VdxcSGZaetQaWIYNGQKY8bc+pcB8Qze9HPH59M8ftYlq94HvvqqGRnZfvx2Ioxbh6YzfHAWj83uydP/OErL9mUushbcru8af85dnEfTFlMwGAx1637EbSy9BRhlWeGbb39l3qvfYzEV0al9OX26FtCuTSESVqxWkJ0CKpWCWgsmi569ByM4fDKCk6f9iIxuxltv3FM3Yz3ZOH3ajORPPovCffyFK+aTQ2n+q7Rs+InX7iIAdm2OYsO2eF6dvd9l0+tg+Yom6CQnQwdno9e6MyFFsDsg33KUuPh2AMlvzp87o+5SCx+9jjMnViITyaChVwLyZ1lWvTUU/sGdyU7bQlB4rqvxTmjbtpTMLH8Ky/WMHJjJ0/O6E9ughh37oomLqiYy2uyiKAeo1VaiIjaSl7WZ9OxIIqOaIghCZ/cgBbtjDn6CIHRu3bohd97RG7Xah41bTGzdqSY7VwuiitAQB/4BCkVlOnYfjOKr72JYt7kRkqYVzz17Ny/PucOzt+Jyt4q5fcSIkXOBb4DOVVWVnDz6CZF+k4ht8Kt3cGVFYM26BFo3K2PBl604cy6YAcMv8sXi5mzZEcM9d6YQEmy5NCE1kJY3n8ZNxwIsAh7Pysy0+/sH4rl8/ULIy83E4XSS2KTjdWFZ9eyV1NTTBAo9CQurdLEkATCAbBIYducwenfN59m5Ryg8q8f4dB8WvbODkAiLi9/KlzqCAqdT+iNrnyKp5c2oVF6b9XM31UzxzOSfNhwiI6OQM+eKOXgoHZvVhKzY8PcLxGZTGD6sPXl5uQwZ3JGxl3ZsSwY2uesY4gq5lpOfsx5/1StEx5z3TiokyMn1RQBmvtiTFk3LeObxI4y4fTiyDF3aF/HCrMP46B0uViy4uMOxo31o2fUXz9ZQHWbOnHn0ivCCqKGk8BgGg5qBg6deQSF/ZQOzhwCaNGlp3LMrmZCQuxAllwVPLciCwJhhGVzI8KcoU09Wjh95hT5UVWi4WGAgMaYSQ4jD5eJ3A9kyaRvI2zh1uCsOzRM0bjIQP7+AKW4wkt0C9wW9Xs34cb0ICPDh2Il0SkqqyblYwv3ThvDL1qMMGtiBTxducO2l5V4XA3Rzr12hsDCHnIxVRPm/RVJ8rmtimN1MUu/yOH/2dRJZuX589cEv9Bg5hsRGVXz6xg6WrW7K84//5uqr1Q2GBgry4mjc9gcPGMmfffbZ0ZYtW14Z7xFV0DyME8f3oSjyX8p+v2KNpju6SM+b7jCe+K2aVg0fQJRcv6iQuf/+M/y8MZZHn+pJbqGByRPOMXNODwL8baglmXvGp9Cnd56rYwre2daq5QFQbqOwIJozR8fTquNcDAZfI64jj5BlhVqTBUnlWv8uSSIqlejdCchmc2C12vG/LEk8Pz+T3AtP0TTuJzq1MrlYk9U9Cr6QdiqAtCx/hgzP4cmHj3LL3cNY8UMTVnyyhbseHECXdoW88NJh195yddhUeVkATv0W/PwCAZJXrFgxo45MvCwAJ4ESiChp/3Ky9dVKlscP06bj/aTkuYWhx4FaA4MH5fD5O9tZv3IDdrtE4/gqln69jXlPH+SDRa0pzPVx3a92P+MBxgoRkbl0bPohFvO111Zcnkn+eyy4ujKdTp1X4u9rclGE4NKY8vN8OHEyhIpqLU/N605Wqh++0Q4+eW0nb33ajpjIWrZ89yNNEypcWWeeia2FygodJtUeomOaetjiHKfTybUuh8MOggZRUl91JfFf3ox/ypQpJ3FtbEnz1g+w/eDLLpng8ZuZQad34udrJ6lpuSvrvEiNwcdOUame9Ew/xk4dzOatsS7HpK4+3aZmtSUwKIzrUXQ+cZgKVW7yFkjP8OfNt9vx5ifteWRWTyqr1Txw9xkefOYmUs8G8N2PjZlwSxpaXwdBgdb6+Vs6KC8PJKVwDdGx3gWvb65cubJQq9Xy+5fOE6O//oAIgsDkyZO9oPQb9DzHzn+JxVbHyy+75MrNAy5yc++LTH+kN7dOHsLgvjkcPxPKzv1RFJfowQ/2740gL9fgbZngc68n5TL5LzQzBCAmJoHiql6ghfIKLbdNG4RakLltdBrBgVbeXdiWvj3yCAuz8MjMXjSKr+aFpw6jUpT6gQId5Oc1xKY9TOcuQzxte/e7777L+KOuqOu1YOea5amnnvKC0q7TPaQVbaeyJsE164VL1HLb2Au8P3c3iz/cwYibs9i4LY45TxzibFogb85rx4hJw1i5vjEYoKo0nNCoO+qu/ZvtWq6mRaXSIUkaRFFCEFyXR2iqVCoQNEgqb9znbVdiuEi1/BKyRSA0xkLf7nl8t74x+w5E8swTR7DbRfYdjmDJZ9tYv3QDd9+VcskY9IyWD5xPHYVs2EFEZGMPGG+sXbs29XqM43U9Nu++++47uWjRojaAsVWbvsby8gMcPzWTti2+dnXK7gIlOMRKsNZKaaGOhx84ialaxfLVTbl9dCoJcVXcNSYFbJB90YCJjznnCEYQg1FpwpBUfuTn5aOSigkOCqGy0kxNjRmTqZby8iqqq8rISD9NbXUahYqOA/udSGINVZW5aNVliEoW+YKO6MZmJk84x09b49FonXzxVXOGD87irvGpUO3y0WGq0zktOGwqDh2ZS6duj6NWazxgvPTUU08V9ezZk78dIAC9evU4uXv33rkAQUGhxsCuX3L86AgifZ8iIsbt73G4WFiHjsWghvwMH9Yt38BHn7amd9c8QuMsVF6UiInKIjDyxUtmrsOlwTUPFTBbJBRFQ0icwZXRmKRAuZreLa0I9hoeGCcTFCgjqGSvaupJy7mYocFeKdCqdRnjhl+gqEjPkw8dIz6xGmrd+iN1bCQgLWMImsD5dL/Jm+ac/M03385Yt24tMTEx1238bsjBkv379Sho0qTZjE8XLkEQBGPbDhMoK7uZA0c+JiEymbCoQm+GH1aIauCair275dEssQJntUCNWSS6gb3+LHWzPt9QBV+7A1QOEE3eBZzeHCjJXbfsHlyljpoqQGS4ndIyiQiNg3nPHnQF2qy41FkPa3JrfZk5XaiRn6NF6xF1T/tJ7t27x8kZMx657mN3Q8+geu7ZmTPcGRXJwcGhdO31IgQc5/jZd0jPisfmFFyD5x64vv3yiGpgoqRYJCzEceVWJu5VT+vWJlCY78OBfZEsW9aMyjIN+3dH8s13TcnLMTD/nfZs3Naw3oY1detQqRU0aoXKSnf3PVThVtlrTBrOp/XmVOYqoprspFWbkV4wpk29a8ato3qfvFFjdsNPHXv8sYcLp065awbQHEgOC4ugbaeZhDc+SXFZS4pLBSqrJMwWAcUKFSUSOp2CRqtcfW8ZCcorfTBZ1JxNCyYzx581PzVm6cpmlJbrqajUEhRkIyvHj6JS/dV7KENQkBOTWcBcK+BUoNYsUlomUlwGhZVGmrbfQau2YzxZ/cmA5oXZs2bc6PH6N51jKHD33XecHzWy/wwgwWVty+g1xQQFK6gkhaoakfNpaixWhYAI+Xc3a/AzWPAz2ElqUobDIZLUrJyGMdVU16gpq9LRrl0pNTVqas3qK+sR3SxNgqgIJwWFEhfz1NjtYNArhESCxVzg2ehsC+A7efK9M2bMeMBeN9X1RpW/LENkWXavl1OQZRmHw4EkXrKgHU4nFksVVVU16F0HbIkAZnMVfpoyVCKo/GS0GgEBAUXRcni/Hw1jqwkOtCCq3P6xOi0dPeQCokpBp7XTMLqKiHgTbZqVUGNSExZlpjhfT8PbKgmLM7tkg3QpwFRbq6aqWoNO68Qpi0RFmakolwgMkL2JSVFhpV563Px9YO3U28M5nxZHdmF79+aWrr5KkuTdEUilUuFwOP7Q5JQk4ZrA/mVAAgP9URQZvV5LYKAfMTENUEkCoihRVFRCZVU1KA5EESxWO8DjACXFGUQ0cG2kb6oRqa4WiIh0kJ+vZd3PjWjXqoyWTUs4cTYUX187oUEWAvyslFdqkGWBsBAzxaU+dGxTRPq5AMJCzKSkuzLpHQ4Rvd7BpvUNaZVUQk2tmoycANQSpGb4k9iwgvMXAmnXqoRevU0EyE7y8yVCQ2XUBgVTTS6+Nisajbb/oHEV+skTu5udzhqqTacw1Zrx8fHF31+muLgQnU6L0+kgPz//D2+Zrla70mSvdmDAXwZEpfIFwGSSEUUDCQmug1g8q3SDgt27TAsCb85/WYX7DFwd34EaSgtca/7Cwly6pk7jpFP7YqxWkcPHIygu1RIdXUtqegQNY6v47UQYrZqVUVjig7+vnWOnw/h2XROG9Mvk5x1xdG5bxKHjEYQEWejTLZdtu2IICLDSoXUJp84G0yCihsRGFfj62gnwtYIT9HoFUVAoK1Ph53AS0+As59O20azFUIC3o+OGzUAAUZARRdfSC1EUyMsvJD4+AUVRyMzM/sN74oiChEodSmlZxZX081ePPIpPmPjHPZHpX4YCxeXl1Rw78BxhQTuJDT9NQFgdjcodCq2o1FJcpkencXIqJZiwIDPF5T6guPbUiggzk5YZgM0mEehvQaVS8Pe1UljiS3CgmayL/oSHmGnWuJygQAtpGYHExVTjZ7Dha7Cj93G/U+1mooLr3dk5wVSau1Fmup3efe/2yBKxectJyvWTFIrXl3X21BfXF5DYhhP/1P05mV96E/Fqaszk5+dSWnySAEMasu0QAYaTBPvl4KOrdW27KbkHzVHHvnDbGLINRK3bePPEM2x1HJQe2SPU8UA7XN5km1WkpjaE8ppmmGytENSdsDqbERmVSFRURN1d8JJj4ifeIO1KuW6nI3hLXKOJf1IJULiY9dUoNyiDL//dZLJSUVFBZWU5NdUFOO35SEIZWnUV5WXZhIXasdScIDLcgUplxWGrQlacyLKMTqvFagOdPoSiYhOyEIfd2Qiz1YDeEIPF6oOkjkKrjyYoKByDwY/AQP9rnb6QDLwS2/CeXOEGqlbZGV/+5wHx0Wto3z6eb1e8EAgkATe5P5vi2h3h92PHn/zM4EHd0GrhwoVsKipcEcN7Jw5g2/ZT9OnbiQXJG2neNJRbR3f9I836GdeCmRPAPiD9/unvOH/ZehK73cG/ExAV/8HSq/cjFZJK2BcU5LcvqXksaWl5rPz2pWAgFGgAxLs/g3EdxRoCjAA7fr4KarUaH70Oq9WBVqPD19cHH70Gg49AgJ+C2eLNqFuDa1fVaqAQ165GGbjO1y0cf/sca0KjCM6n5JGVVUjHjk2uGa+40eU/CsjVypOzFpYdOnS2TBCkFEEQUBQnWq2En68fK7974TlgBN4UWNl7eXzkl76rd07u2n43z1xqsTjdR2NoEUUFWVZIurQb99+iiPzNi1otUVBYzZFjGX+pnvMpxTic8t+9u38/Cqlv44gUF1dRXm7G+RcHU5JECgqqUGQw+OjclrLyt+vz35ZCXANYSWlZLbIs1zvS6F/yprkBKCyqpLi4ym0L8H+A/POBcx3UdTG3jLLyWu9316tuEMjLLycltcC1zZ4o/h8g12yMKOBwOMkrqKSqyvKXqeLalALV1VZycsoxm6035D3/9YCo1SrKyqrZuu0EFZXm3z1f6nqxxKLiSn786TBOJzf8ff81Ql0QQBJFcnPLOXe+0LVWUCX92yjSZnNy7HgWKpWIWi39x+WK6j8LhisGkp1dysFDGW7NSvq3tkGSRExmK7v3pBATHcS1Nhb7nwdEdJ+wefFiBWaLo95xRP92jU50bVyZnVPqZV2/d4jl/5wMEUSB6moLBYVVmC32ekeo/ke1O0nE4ZDZvz+NwsLK/0ibVP9+yhCwWu2cOn0Rh8Pp3f/372T/lJbVUPHbhf8I1f5HWJbrSFT5b6VuXg4K/PNjrv4rAPlzLo4/3WMBXCdK/7OJKwiuzYo9/9bZrvUGuHikvycggiAQHOz3L55l/scR1GrUv8vqJFHE4XCiv3TiphwXG3b9W+M+YaikpArlOpHTdQPEbnfSqGE4Y27tQWlZNeKN4b0/AfMGDerAuh/3M2F8nyvBUEnk5BRx+kwW900b4vl601NPjrsh8lAUJZ6bvZSysqqr7lT9H6QQBZVaRU52Adk5xYiSgMD1BWXcbS8d+37lS8nxceFGBYX0jALUmvpdMPhoWbJ0C7eNu8lDIcmvvLqs6EbMDofDSXx8xN+XZclOGb2PlnbtmiBK4o2iko8A44Tb+vD0s58zdcqQeppQaloeVZUmbu7v3WDziUGDOt8Q6jCbrVy4kHfd2NUNEuoKzZvHEhsTht0hc70h2bnz5JnevVsn+/rqjcbpIzhzJpuwsACvRpSRUcADDwzzCP3kF+csNV1/LREMBi29era+7mrxDVF7BUFg797TnDqdeUMs3t69W88BjEnN41i8eBN9+7XFx0dDSupFAoMMNGsaA5B8592v3JD0HbvdQcOGkdwIE+qGACKKImVl1Zw5m42PQXvdFa6+/R8v3LHt7WRRFIwP3D+cbduPERIawKrVe3j4H6M8t308cmSvG+DaEKiuMXH+fNYNsVNumGHocDoZMaIb3bomuROUr7O6teHgjOHDumiaNYuZeiE9j6+XbeP++4Z5DrhMXvPDntNazXXuntsz7esXTFpazn+fpe5n0HP8eBrZ2cU3JN4wfFiXZGBqly7N+emng/Tq6d054eWFi9Zf/0lmd5DQuAGjbul5w9Yl3FBAZEXhQno+Z85kerZEut7lOLhO/rn33oHe48N79X4k/0a8zGazU1tjQbiBLh9BUf5+mRd/slx+0PGM/+bO/C8A4qH0PsDW//aO/K8A8j9T/t8Ajn3H+H6xynYAAAAASUVORK5CYII='
	//#endregion
	today = ''
	registrarBody = []
	asstRegistrarBody = []
	programTitle = ''

	//person signatures
	registrar = '9600007'
	asstRegistrar = '9708277'
	registrarTitle = ', Ph.D.'
	asstRegistrarTitle = ''


	constructor(private excelService: ExcelService, public global: GlobalService, private api: ApiService,) { }

	ngOnInit() {
		this.sy = this.global.syear
		this.api.getPublicAPICurrentServerTime()
			.map(response => response.json())
			.subscribe(res => {
				var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				var today: any = new Date(res.data);
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
				this.today = mL[today.getMonth()] + ' ' + dd + ', ' + yyyy;
			}, Error => {
				this.global.swalAlertError(Error)
			});
		this.api.getReportCHEDPersonSignatureInfo(this.registrar).map(response => response.json())
			.subscribe(res => {
				this.registrarBody = res.data
				// @ts-ignore
				this.registrarBody.signature = 'data:image/jpeg;base64,' + this.registrarBody.signature
				// @ts-ignore
				this.registrarBody.fullname = this.registrarBody.fullname + this.registrarTitle
				// @ts-ignore
			})
		this.api.getReportCHEDPersonSignatureInfo(this.asstRegistrar).map(response => response.json())
			.subscribe(res => {
				this.asstRegistrarBody = res.data
				// @ts-ignore
				this.asstRegistrarBody.signature = 'data:image/jpeg;base64,' + this.asstRegistrarBody.signature
				// @ts-ignore
				this.asstRegistrarBody.fullname = this.asstRegistrarBody.fullname + this.asstRegistrarTitle
			})
	}


	generate() {
		if (this.proglevel == '') {
			this.global.swalAlert("Program level is required!", '', 'warning')
		} else {
			this.array = undefined
			this.api.getReportCHEDEnrollmentListCHED(this.sy, this.proglevel, this.pageSize, this.pageNumber, '0')
				.map(response => response.json())
				.subscribe(res => {
					this.array = [];
					if (res.data == null) {
						this.global.swalAlert(res.message, '', 'warning')
					} else
						this.array = res.data;
				}, Error => {
					this.array = [];
					this.global.swalAlertError(Error)
				});
		}
	}

	export() {
		var arr = []
		for (var i = 0; i < this.array.length; ++i) {
			arr.push(
				{
					"ID Number": this.array[i].idNumber,
					"Full Name": this.array[i].fullName,
					"Year or Grade": this.array[i].yearOrGradeLevel,
					"Gender": this.array[i].gender,
					"Course": this.array[i].courseCode + ' - ' + this.array[i].programTitle,
					"Subjects": this.array[i].subjects,
					"Total Units": this.array[i].totalUnits,
				}
			)
		}

		this.global.swalClose();
		this.excelService.exportAsExcelFile(arr, 'CHEDEnrollmentList-' + this.proglevel);

	}
	display(x) {
		var y = x.substring(0, 4)
		var z = parseInt(y) + 1
		var a = y.toString() + " - " + z.toString();
		var b = x.substring(6, 7)
		var c
		if (b == 1)
			c = "First Semester"
		else if (b == 2)
			c = "Second Semester"
		else
			c = "Summer"
		return "School Year " + a + " " + c
	}

	breakOpeningParenthesis(data){
		if(!data.includes('(')){
			return
		}
		var text = data.slice(0, data.indexOf('(')) + "\n" + data.slice(data.indexOf('('));
		return text
	}

	generateC2Annex() {

		var header = [
			{
				image: this.logo,
				width: 60,
				opacity: 1,
				absolutePosition: { x: 50, y: 40 },
			},
			{
				text: 'University of Saint Louis', bold: true, fontSize: 15, alignment: 'left',
				absolutePosition: { x: 120, y: 46 }
			},
			{
				text: 'Mabini Street, Tuguegarao City Cagayan, Philippines\nTel. No. (078) 844-1872/73\nFax No. (078) 844-0889', italics: true, fontSize: 10, alignment: 'left',
				absolutePosition: { x: 120, y: 63 }
			},
			{
				text: 'Start right...', italics: true, fontSize: 9, alignment: 'left',
				absolutePosition: { x: 847, y: 79 }
			},
			{
				text: 'Come over to the Louisian Side!', italics: true, fontSize: 9, alignment: 'left',
				absolutePosition: { x: 770, y: 89 }
			},
			{
				canvas:
					[
						{
							type: 'line',
							x1: 43, y1: 117,
							x2: 893, y2: 117,
							lineWidth: 2,
						},
					]
			},
			{
				text: 'CHED FORM C-2 Annex\nENROLLMENT LIST\nINSTITUTIONAL NAME:\tUNIVERSITY OF SAINT LOUIS\nINSTITUTIONAL ADDRESS:\tMabini Street, Tuguegarao City\n' + this.global.ayDisplay(this.sy), bold: true, fontSize: 10, alignment: 'left',
				absolutePosition: { x: 47, y: 127 }
			},
		]

		var tableContent = []
		var pageBreak = ''

		for (var x in this.array) {

			if (this.array[x].programTitle != this.programTitle) {

				this.programTitle = this.array[x].programTitle
				tableContent.push(
					[
						{
							text: this.array[x].programTitle, bold: true, alignment: 'center', fontSize: 50, colSpan: 6, border: [false, false, false, false], pageBreak: pageBreak, margin: [0, 60, 0, 0], color: '#006298'
						}, '', '', '', '', '',
					]
				)

				pageBreak = 'before'
				tableContent.push(
					[
						{ text: '', margin: [0, 1, 0, 1], border: [false, false, false, false], colSpan: 6, pageBreak: pageBreak }, '', '', '', '', ''
					],
					[
						// @ts-ignore
						{ text: 'Name of Student', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
						// @ts-ignore
						{ text: 'Sex', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
						// @ts-ignore
						{ text: 'Course', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
						// @ts-ignore
						{ text: 'Year', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
						// @ts-ignore
						{ text: 'Subjects Enrolled', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
						// @ts-ignore
						{ text: 'Total No. of Units', bold: true, alignment: 'center', fontSize: 9, border: [true, true, true, true] },
					]
				)
			}

			tableContent.push([
				// @ts-ignore
				{ text: this.array[x].fullName, bold: false, alignment: 'left', fontSize: 9 },
				// @ts-ignore
				{ text: this.array[x].gender, bold: false, alignment: 'left', fontSize: 9 },
				// @ts-ignore
				{ text: this.array[x].courseCode, bold: false, alignment: 'left', fontSize: 9 },
				// @ts-ignore
				{ text: this.array[x].yearOrGradeLevel, bold: false, alignment: 'left', fontSize: 9 },
				// @ts-ignore
				{ text: this.array[x].subjects, bold: false, alignment: 'left', fontSize: 9 },
				// @ts-ignore
				{ text: this.array[x].totalUnits, bold: false, alignment: 'left', fontSize: 9 }
			])

			if (parseInt(x) < this.array.length - 1) {
				if (this.array[parseInt(x) + 1].programTitle == undefined || this.array[parseInt(x) + 1].programTitle != this.programTitle) {
					tableContent.push([
						{ text: '', margin: [0, 1, 0, 1], border: [false, false, false, false], colSpan: 6, }, '', '', '', '', ''
					],)
				}
			}

		}

		var content = [
			{
				table: {
					widths: ['30%', '*', '*', '*', '50%', '*'],
					body: tableContent
				},
				absolutePosition: { x: 43, y: 195 },
			}
		]

		var footer = [
			{
				text: 'NOTE: Only data from Higher Education including ladderized courses are asked in this form.', bold: true, fontSize: 10, alignment: 'left',
				absolutePosition: { x: 47, y: 0 }
			},
			{
				columns: [
					{
						// auto-sized columns have their widths based on their content
						width: '50%',
						text: 'Prepared by:', bold: true, fontSize: 9, alignment: 'center', margin: [0, 0, 120, 0]
					},
					{
						// fixed width
						width: '50%',
						text: 'Noted by:', bold: true, fontSize: 9, alignment: 'center', margin: [0, 0, 120, 0]
					}
				],
				// optional space between columns
				columnGap: 10,
				absolutePosition: { x: 20, y: 27 }
			},
			{
				columns: [
					{
						// @ts-ignore
						image: this.asstRegistrarBody.signature,
						width: 150,
						opacity: 1,
						absolutePosition: { x: 180, y: 37 },
					},
					{
						// @ts-ignore
						image: this.registrarBody.signature,
						width: 120,
						opacity: 1,
						absolutePosition: { x: 650, y: 40 },
					},
				],
				// optional space between columns
				columnGap: 10,
				absolutePosition: { x: 20, y: 60 }
			},
			{
				columns: [
					{
						// auto-sized columns have their widths based on their content
						width: '50%',
						// @ts-ignore
						text: '______________________________________\n' + this.asstRegistrarBody.fullname + '\nAssistant to the Registrar\nDate: ' + this.today, bold: true, fontSize: 9, alignment: 'center',
						// @ts-ignore
					},
					{
						// fixed width
						width: '50%',
						// @ts-ignore
						text: '______________________________________\n' + this.registrarBody.fullname + '\nUniversity Registrar\nDate: ' + this.today, bold: true, fontSize: 9, alignment: 'center',
					}
				],
				// optional space between columns
				columnGap: 10,
				absolutePosition: { x: 20, y: 60 }
			},
		]

		var dd = {
			pageSize: 'FOLIO',
			pageOrientation: 'landscape', pageMargins: [40, 190, 40, 143],
			header: header,
			content: content,
			footer: footer,

		}

		pdfMake.createPdf(dd).download('CHED FORM C-2 Annex ' + this.global.ayDisplay(this.sy) + '.pdf');
		// pdfMake.createPdf(dd).open();

	}
}
