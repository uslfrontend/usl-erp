import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as ExcelJS from "exceljs/dist/exceljs"
import * as fs from 'file-saver';

declare const ExcelJS: any;

@Injectable()
export class AcademicService {

  constructor(private datePipe: DatePipe) { }

  //09/07/2022
  async generateBasicEdScheduleExcel(dataArr) {
    //new excel file
    var workbook = new ExcelJS.Workbook();

    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = "Schedule List"

    // console.log("Printlog 2: ", dataArr[0])

    let headerRow = worksheet.addRow(['Grade Level', 'Section', 'Time', 'Day', 'Subject', 'Adviser Name', 'Teacher Name', 'Coordinator Name', 'Area']);

    for (var x = 0; x < dataArr.length; x++) {
      worksheet.addRow([
        dataArr[x].gradeLevel,
        dataArr[x].section,
        dataArr[x].time,
        dataArr[x].day,
        dataArr[x].subject,
        dataArr[x].adviserName,
        dataArr[x].teacherName,
        dataArr[x].coordinatorName,
        dataArr[x].area
      ]);
    }

    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  //09/29/2022
  async downloadBasicEdScheduleExcelFormat(grade, section, data, classAdviser) {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = "Grade" + grade + "-" + section
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    worksheet.getCell('A3').value = "Basic Education Schedule Manager"
    worksheet.getCell('A4').value = "Schedule Uploader"
    worksheet.mergeCells('A1:E1');
    worksheet.mergeCells('A2:E2');
    worksheet.mergeCells('A3:E3');
    worksheet.mergeCells('A4:E4');
    worksheet.getColumn(1).width = 14;
    worksheet.getColumn(2).width = 14;
    worksheet.getColumn(3).width = 14;
    worksheet.getColumn(4).width = 14;
    worksheet.getColumn(5).width = 28;
    // console.log("Printlog 2: ", dataArr[0])
    let headerRow = worksheet.addRow(['Grade', grade])
    this.rowBoldAndOutline(headerRow)
    headerRow = worksheet.addRow(['Section', section])
    this.rowBoldAndOutline(headerRow)
    headerRow = worksheet.addRow(['Adviser', classAdviser])
    this.rowBoldAndOutline(headerRow)
    let dataRow = worksheet.addRow(['Subject', 'Time', 'Day', 'Teacher ID', 'Area']);
    this.rowBoldAndOutline(dataRow)
    worksheet.mergeCells('B5:E5');
    worksheet.mergeCells('B6:E6');
    worksheet.mergeCells('B7:E7');
    for (var x = 0; x < data.length; x++) {
      data[x].time = data[x].time.replace(/[A-Z]/g, '')
      if (data[x].subject == null || data[x].subject == undefined || data[x].subject == '') {
        data[x].subject = ''
      }
      if (data[x].time == null || data[x].time == undefined || data[x].time == '') {
        data[x].time = ''
      }
      if (data[x].day == null || data[x].day == undefined || data[x].day == '') {
        data[x].day = ''
      }
      if (data[x].adviserID == null || data[x].adviserID == undefined || data[x].adviserID == '') {
        data[x].adviserID = ''
      }
      if (data[x].teacherID == null || data[x].teacherID == undefined || data[x].teacherID == '') {
        data[x].teacherID = ''
      }
      if (data[x].area == null || data[x].area == undefined || data[x].area == '') {
        data[x].area = ''
      }
    }
    if (data.length != 0) {
      for (var x = 0; x < data.length; x++) {
        dataRow = worksheet.addRow([
          data[x].subject,
          data[x].time,
          data[x].day,
          data[x].teacherID,
          data[x].area
        ])
        this.leftRightOutline(dataRow)
        worksheet.getCell('E' + x).alignment = { wrapText: true };
        worksheet.getCell('A' + x).alignment = { wrapText: true };
        if (x == 99) {
          this.leftRightBottomOutline(dataRow)
        }
        dataRow.eachCell((cell, number) => {
          if (cell._address.includes('')) {
            cell._address = null
            cell.protection = {
              locked: false,
            };
          }
        })
      }
    }
    for (var x = 0; x < 100 - data.length; x++) {
      dataRow = worksheet.addRow([
        '',
        '',
        '',
        '',
        ''
      ])
      this.leftRightOutline(dataRow)
      worksheet.getCell('E' + x).alignment = { wrapText: true };
      worksheet.getCell('A' + x).alignment = { wrapText: true };
      if (x == 99 - data.length) {
        this.leftRightBottomOutline(dataRow)
      }
      dataRow.eachCell((cell, number) => {
        if (cell._address.includes('')) {
          cell._address = null
          cell.protection = {
            locked: false,
          };
        }
      })
    }
    //Align headings to center and put style
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A3').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A4').alignment = { vertical: 'top', horizontal: 'center' };
    //make format of the cells as text
    worksheet.getColumn(1).numFmt = '@';
    worksheet.getColumn(2).numFmt = '@';
    worksheet.getColumn(3).numFmt = '@';
    worksheet.getColumn(4).numFmt = '@';
    worksheet.getColumn(5).numFmt = '@';
    worksheet.getColumn(6).numFmt = '@';
    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  //10/06/2022
  async downloadFacultyEvalProgramExcelFormat(programCode, programTitle, facultyDataList) {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = (programCode + " Faculty List")
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A3').value = "FACULTY EVALUATION MANAGER"
    worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A3').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A4').value = "Upload Faculty List"
    worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A4').alignment = { vertical: 'top', horizontal: 'center' };
    let headerRow = worksheet.addRow(['Code: ' + programCode]);
    this.rowBoldAndOutline(headerRow)
    headerRow = worksheet.addRow(['Program: ' + programTitle]);
    this.rowBoldAndOutline(headerRow)
    headerRow = worksheet.addRow(['ID Number']);
    this.rowBoldAndOutline(headerRow)
    worksheet.getCell('A7').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getColumn(1).width = 75.87;

    // console.log("Printlog: ", facultyDataList)
    if (facultyDataList != '') {
      let dataRow
      for (var x = 0; x < 50; x++) {
        if (x < facultyDataList.length) {
          dataRow = worksheet.addRow([facultyDataList[x].facultyID]);
          this.leftRightOutline(dataRow)
          dataRow.eachCell((cell, number) => {
            if (cell._address.includes('')) {
              cell._address = null
              cell.protection = {
                locked: false,
              };
            }
          })
        } else {
          // console.log("Printlog all faculty printed")
          dataRow = worksheet.addRow(['']);
          this.leftRightOutline(dataRow)
          if (x == 50 - facultyDataList.length) {
            this.leftRightBottomOutline(dataRow)
          }
          dataRow.eachCell((cell, number) => {
            if (cell._address.includes('')) {
              cell._address = null
              cell.protection = {
                locked: false,
              };
            }
          })
        }
      }
    } else {
      let dataRow
      for (var x = 0; x < 50; x++) {
        dataRow = worksheet.addRow(['']);
        this.leftRightOutline(dataRow)
        if (x == 49) {
          this.leftRightBottomOutline(dataRow)
        }
        dataRow.eachCell((cell, number) => {
          if (cell._address.includes('')) {
            cell._address = null
            cell.protection = {
              locked: false,
            };
          }
        })

      }
    }
    //make format of the cells as text
    worksheet.getColumn(1).numFmt = '@';
    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  //11/08/2022
  async downloadFacultyEvalAreaExcelFormat(areaName, areaDescription, areaFacultyList) {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = (areaName + " Faculty List")
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getColumn(1).width = 75.87;
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A3').value = "FACULTY EVALUATION MANAGER"
    worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A3').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A4').value = "Upload Area Faculty List"
    worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A4').alignment = { vertical: 'top', horizontal: 'center' };
    let headerRow = worksheet.addRow(['Area: ' + areaName]);
    this.rowBoldAndOutline(headerRow)
    worksheet.getCell('A5').alignment = { wrapText: true };
    headerRow = worksheet.addRow(['Description: ' + areaDescription]);
    this.rowBoldAndOutline(headerRow)
    worksheet.getCell('A6').alignment = { wrapText: true };
    headerRow = worksheet.addRow(['ID Number']);
    this.rowBoldAndOutline(headerRow)
    worksheet.getCell('A7').alignment = { vertical: 'top', horizontal: 'center' };
    // console.log("Printlog: ", facultyDataList)
    if (areaFacultyList != '') {
      let dataRow
      for (var x = 0; x < 50; x++) {
        if (x < areaFacultyList.length) {
          dataRow = worksheet.addRow([areaFacultyList[x].facultyID]);
          this.leftRightOutline(dataRow)
          dataRow.eachCell((cell, number) => {
            if (cell._address.includes('')) {
              cell._address = null
              cell.protection = {
                locked: false,
              };
            }
          })
        } else {
          // console.log("Printlog all faculty printed")
          dataRow = worksheet.addRow(['']);
          this.leftRightOutline(dataRow)
          if (x == 51 - areaFacultyList.length) {
            this.leftRightBottomOutline(dataRow)
          }
          dataRow.eachCell((cell, number) => {
            if (cell._address.includes('')) {
              cell._address = null
              cell.protection = {
                locked: false,
              };
            }
          })
        }
      }
    } else {
      let dataRow
      for (var x = 0; x < 50; x++) {
        dataRow = worksheet.addRow(['']);
        this.leftRightOutline(dataRow)
        if (x == 49) {
          this.leftRightBottomOutline(dataRow)
        }
        dataRow.eachCell((cell, number) => {
          if (cell._address.includes('')) {
            cell._address = null
            cell.protection = {
              locked: false,
            };
          }
        })

      }
    }
    //make format of the cells as text
    worksheet.getColumn(1).numFmt = '@';
    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  async sectionManagerExcel() {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = 'Section Uploader'
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    let h3 = worksheet.addRow(['High School Section Uploader']);
    let h4 = worksheet.addRow(['ID Number', 'Section']);

    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    h3.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h3.alignment = { vertical: 'top', horizontal: 'center' };
    h4.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h4.alignment = { vertical: 'top', horizontal: 'center' };

    worksheet.mergeCells('A1:B1');
    worksheet.mergeCells('A2:B2');
    worksheet.mergeCells('A3:B3');
    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 35;


    this.rowBoldAndOutline(h1)
    this.rowBoldAndOutline(h2)
    this.rowBoldAndOutline(h3)
    this.rowBoldAndOutline(h4)



    let dataRow
    for (var x = 0; x < 70; x++) {
        dataRow = worksheet.addRow(["", ""]);
        this.leftRightOutline(dataRow)
        dataRow.eachCell((cell, number) => {
          if (cell._address.includes('')) {
            cell._address = null
            cell.protection = {
              locked: false,
            };
          }
        })
        if (x == 69){
          this.leftRightBottomOutline(dataRow)
        }

    }


    worksheet.getColumn(1).numFmt = '@';

    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  //make entire rown bold and outlined
  rowBoldAndOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'FFFFFFFF'
        },
        bgColor: {
          argb: 'FFFFFFFF'
        },
      };
      cell.font = {
        color: {
          argb: '00000000',
        },
        bold: true
      }
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire row outlined
  rowOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make the left and right of the cell outlined
  leftRightOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire the bottom of the cell outlined
  leftRightBottomOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire row bold
  rowBold(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.font = {
        color: {
          argb: '00000000',
        },
        bold: true
      }
    });
    return cellRow
  }

  //make entire rown bold and centered
  rowBoldAndCenter(cellRow) {

  }

}
