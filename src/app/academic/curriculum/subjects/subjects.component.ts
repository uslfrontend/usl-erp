import { Component, OnInit, Optional } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { MatDialog } from '@angular/material';
import { LookUpCurriculumComponent } from './../look-up-curriculum/look-up-curriculum.component';
import { AddUpdateSubjectsComponent } from './add-update-subjects/add-update-subjects.component';
import { PreRequisitesComponent } from './pre-requisites/pre-requisites.component';
import { OtherPreRequisitesComponent } from './other-pre-requisites/other-pre-requisites.component';
import { CoRequisitesComponent } from './co-requisites/co-requisites.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';



import { ExcelService } from './../excel.service';
import Swal from 'sweetalert2';
import { version } from 'process';
const swal = Swal;
@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {
  progid = ""
  tabledata


  floadingDeptCode = ''
  fhours = ''
  fsubjectId = ''
  frecordId = ''
  fsubjectTitle = ''
  funits = ''
  flabUnits = ''
  flecUnits = ''
  fyearLevel = ''
  fterm = ''
  programCode = ''


  arraydept
  tableArr = []
  constructor(@Optional() public dialogRef: MatDialogRef<SubjectsComponent>, private excelService: ExcelService, public global: GlobalService, private api: ApiService, public dialog: MatDialog, @Optional() @Inject(MAT_DIALOG_DATA) public codesManagerData: any) { }

  ngOnInit() {
    if (this.codesManagerData != null) {
      this.progid = this.codesManagerData
      this.keyDownFunction('onoutfocus')
    }

    this.api.getPublicAPIDepartments()
      .map(response => response.json())
      .subscribe(res => {
        this.arraydept = res;
      }, Error => {
        this.global.swalAlertError(Error);
      });
  }

  close(){
    this.dialogRef.close({result: 'cancel'});
  }

  selectCourse(courseID, courseTitle, program, version, status, recordID){
    this.dialogRef.close({courseID: courseID, courseTitle: courseTitle, program: program, version: version, status: status, recordID: recordID});
  }

  filterall(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      var filter = {
        recordId: this.frecordId,
        subjectId: this.fsubjectId,
        subjectTitle: this.fsubjectTitle,
        units: this.funits,
        labUnits: this.flabUnits,
        lecUnits: this.flecUnits,
        yearLevel: this.fyearLevel,
        term: this.fterm,
        hours: this.fhours,
        loadingDeptCode: this.floadingDeptCode
      };

      var users = this.tableArr

			users= users.filter(function(item) {

			  for (var key in filter) {
			    if (!item[key].toString().toUpperCase().includes(filter[key].toString().toUpperCase()))
			      return false;
			  }
			  return true;

			});
			this.tabledata = users
		    }

  }

  loadsubjects() {
    this.tabledata = undefined
    this.tableArr = undefined
    this.api.getCurriculumSubjects(this.progid)
      .map(response => response.json())
      .subscribe(res => {
        for (var i = 0; i < res.data.length; ++i) {
          if (res.data[i].subjectTitle == null) {
            res.data[i].subjectTitle = ''
          }
          if (res.data[i].subjectTypeId == null) {
            res.data[i].subjectTypeId = ''
          }
          if (res.data[i].hours == null) {
            res.data[i].hours = ''
          }
        }
        this.tabledata = res.data
        this.tableArr = res.data
        this.filterall('onoutfocus')
      }, Error => {
        this.global.swalAlertError(Error);
      }
      );

  }
  resetsearch() {

    this.floadingDeptCode = ''
    this.fhours = ''
    this.fsubjectId = ''
    this.frecordId = ''
    this.fsubjectTitle = ''
    this.funits = ''
    this.flabUnits = ''
    this.flecUnits = ''
    this.fyearLevel = ''
    this.fterm = ''
  }
  data
  openDialoglookup(x, a): void {
    const dialogRef = this.dialog.open(LookUpCurriculumComponent, {
      width: '95%', disableClose: false, data: { data: x, array: a }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.progid = result.result.programId
          this.keyDownFunction('onoutfocus');
        }
      }
    });
  }
  openDialogaddupdatesubjects(x, a): void {
    const dialogRef = this.dialog.open(AddUpdateSubjectsComponent, {
      width: '600px', disableClose: false, data: { data: x, array: a, curr: this.data }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.loadsubjects();
        }
      }
    });
  }
  openDialogPrerequisites(x): void {
    const dialogRef = this.dialog.open(PreRequisitesComponent, {
      width: '800px', disableClose: false, data: { data: x, subjects: this.tableArr }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.loadsubjects();
        }
      }
    });
  }
  openDialogCorequisites(x): void {
    const dialogRef = this.dialog.open(CoRequisitesComponent, {
      width: '800px', disableClose: false, data: { data: x, subjects: this.tableArr }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.loadsubjects();
        }
      }
    });
  }
  openDialogOtherrequisites(x): void {
    const dialogRef = this.dialog.open(OtherPreRequisitesComponent, {
      width: '500px', disableClose: false, data: { data: x }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.loadsubjects();
        }
      }
    });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {

      this.api.getCurriculum(this.progid)
        .map(response => response.json())
        .subscribe(res => {
          this.data = undefined
          if (res.data == null) {
            this.data = []
            this.global.swalAlert("Acadims Alert!", 'No Program ID found.', 'warning')
          } else
            if (this.global.checkdomain(res.data.departmentId)) {
              this.data = res.data
              this.resetsearch();
              this.loadsubjects();
            } else {
              this.global.swalAlert("Acadims Alert!", 'Your view domain does not allow you to view this curriculum.', 'warning')
            }

        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }


  removesub(id, rid) {
    this.swalConfirm("Are you sure?", "You won't be able to revert this!", 'warning', 'Remove Subject', 'Subject has been Removed', '', 'sy', id, rid);
  }

  swalConfirm(title, text, type, button, d1, d2, remove, id, rid) {
    swal.fire({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {
        if (remove == 'sy') {
          this.api.deleteCurriculumSubject(id, rid)
            .map(response => response.json())
            .subscribe(res => {
              this.global.swalSuccess(res.message)
              this.keyDownFunction('onoutfocus')
            }, Error => {
              this.global.swalAlertError(Error);
            });
        }
      }
    })
  }


  export() {
    let x = []
    this.global.swalLoading('')

    let recid = this.data.programId
    this.api.getCurriculumSubjectListWithPrerequisites(recid)
      .map(response => response.json())
      .subscribe(res => {
        for (var i = 0; i < res.data.length; ++i) {
          var t = ''
          if (res.data[i].otherRequisite != null) {
            t = res.data[i].otherRequisite
          }
          x.push({
            SubjectID: res.data[i].subjectId,
            SubjectTitle: res.data[i].subjectTitle,
            YearLevel: res.data[i].yearLevel,
            Term: res.data[i].term,
            PreRequesite: res.data[i].preRequisite,
            CoRequesite: res.data[i].coRequisite,
            OtherRequesites: t,
            LoadingDepartment: res.data[i].loadingDept,
          })
        }
        this.global.swalClose();
        this.excelService.exportAsExcelFile(x, this.data.courseCode);
      }, Error => {
        this.global.swalAlertError(Error)
      });

  }
}
