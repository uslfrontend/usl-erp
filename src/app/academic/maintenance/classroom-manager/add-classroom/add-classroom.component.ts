import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { ApiService } from './../../../../api.service'
import { GlobalService } from './../../../../global.service';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ClassroomManagerComponent } from '../classroom-manager.component';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-add-classroom',
  templateUrl: './add-classroom.component.html',
  styleUrls: ['./add-classroom.component.css']
})
export class AddClassroomComponent implements OnInit {
  dataToAdd = []
  roomNoError = false;
  selectedType: any;
  tabledata
  tableArr = []
  findby
  x
  string = ''

  typeList = []

  roomNo = ''
  seatingCapacity = ''
  type = ''
  typeDesc = ''
  statusCode = ''

  aroomNo = ''
  aseatingCapacity = ''
  atype = ''
  atypeDesc = ''
  astatusCode = ''

  search = ''
  arr
  location = this.data.selectedData[0]

  uniqueOfLocation = []
  filteredArray = []
  filteredArray2 = []

  pagesize = 50
  pageno = 1
  totalpageno = 0

  constructor(public dialogRef: MatDialogRef<AddClassroomComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private api: ApiService, public global: GlobalService,) {
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
    // console.log('success')
  }

  submitData() {
    this.dialogRef.close(this.data);
  }
  ////filter the arr array based on the selected location and store the filtered result in the filteredArray and filteredArray2 arrays.
  generate() {
    this.filteredArray = []
    for (var x = 0; x < this.arr.length; x++) {
      if (this.location == this.arr[x].location) {
        this.filteredArray.push(this.arr[x])
      }
    }
    this.filteredArray2 = this.filteredArray
  }

  //for saving a classroom object in an API, with validation to ensure that all required fields are filled in before the update is made.
  saveadd() {
    if (this.location != '' && this.aroomNo != '' && this.aseatingCapacity != '' && this.selectedType != '') {
      this.api.postClassroom({
        "roomNo": this.aroomNo,
        "seatingCapacity": this.aseatingCapacity.toString(),
        "location": this.location,
        "type": this.selectedType.type,
        "typeDesc": this.selectedType.typeDesc
      })
        .map(response => response.json()).subscribe(res => {
          if(res.message =='Classroom successfully added.'){
            this.global.swalSuccess(res.message);
          }
          else  {
            this.global.swalAlert('Room ' + '"' + this.aroomNo + '"' + ' Already Exist!', '', 'warning')
          }
          // console.log(res.message)
          // this.global.swalAlert('Room ' + '"' + this.aroomNo + '"' + ' Already Exist!', '', 'warning')
    
          // console.log(res.message)
          // this.dialogRef.close({ result: 'nice' });
        }
        , Error => {
           
          // this.global.swalAlertError(Error);
      
          // console.log(Error)
        });
    } else
      this.global.swalAlert("Please fill in the required fields!", "", 'warning');
    // console.log('Type Desc:', this.selectedType.typeDesc)
    // console.log('Type:', this.selectedType.type)
    // console.log('location:', this.location)
    // console.log('Room Number:', this.aroomNo)
    // console.log('Seating Capacity:', this.aseatingCapacity)
  }

  ngOnInit() {
    Swal.showLoading()
    this.api.getClassroomList().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.arr = result.data;
        // console.log('data', result.data)
      }
      for (var x in this.arr) {
        if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
          this.uniqueOfLocation.push(this.arr[x].location)
          Swal.close()
        }

      }
    })
    this.api.getClassroomTypes().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.typeList = result.data;
        // console.log('data', result.data)
        this.typeList.sort((a, b) => a.type.localeCompare(b.type));
      }
    });
  }
}
