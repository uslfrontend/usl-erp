import { Component, OnInit, Optional } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service'
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2'
import { MatDialog } from '@angular/material';
import { Inject } from '@angular/core';
import { ExcelService } from '../../curriculum/excel.service';
import * as ExcelJS from "exceljs/dist/exceljs"
import { AddClassroomComponent } from './add-classroom/add-classroom.component';
import { UpdateClassroomComponent } from './update-classroom/update-classroom.component';
import { HttpHeaders } from '@angular/common/http';
import { first } from 'rxjs/internal/operators/first';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

// Added by Kurt. This import is used for starting the tab to classroom schedule when this component is opnnened form codes manager
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';




@Component({
  selector: 'app-classroom-manager',
  templateUrl: './classroom-manager.component.html',
  styleUrls: ['./classroom-manager.component.css']
})

export class ClassroomManagerComponent implements OnInit {



  tabledata
  tableArr = []
  findby

  string = ''
  dataToEdit = []
  dataToAdd = []
  selectedType
  roomNo = ''
  roomNo2 = ''
  roomNo3 = ''
  seatingCapacity = ''
  type = ''
  typeDesc = ''

  aroomNo = ''
  aseatingCapacity = ''
  atype = ''
  atypeDesc = ''

  broomNo = ''
  btype = ''
  bTheDay = ''
  bVacantSchedule = ''

  rowSpan
  arr
  arr2
  arr3

  location = ''
  location2 = ''
  location3 = ''

  schoolYear = this.global.syear
  MergeClass = 'MERGECLASS'
  roomNumber = ''
  roomType = ''
  theDay = ''
  schedule = ''
  minutes = ''
  vacantSched = ''
  remarks = ''
  instructor = ''
  codeNo = ''
  rooms = ''

  selectedRoom = ''
  startTime = ''
  endTime = ''
  day = ''
  classroomType = ''

  uniqueOfLocation = []
  uniqueOfLocation2 = []
  uniqueOfLocation3 = []
  newFilteredArray = []
  newFilteredArray2 = []
  newFilteredArray3 = []
  filteredArray = []
  filteredArray2 = []
  filteredArray3 = []
  classroomArray = []
  classroomArray2 = []
  classroomAvailArray = []
  finalData = []
  container = []
  container2 = []

  tempArray = []
  tempArray2 = []
  clicked = true
  inputValue: string;
  isDisabled = true
  originalHtml = null

  disabled = true
  checkedRoomNo = false
  checkedDay = false
  typeListFinal = []
  typeList: any
  checkedType = false
  reloading

  configRooms: any
  ctr = 0

  // Added by Kurt. This variable is used to start the tab to classroom schedule when this component is opnnened form codes manager
  tab = 0

  weekdays = [
    { value: '1', viewValue: 'Monday', },
    { value: '2', viewValue: 'Tuesday', },
    { value: '3', viewValue: 'Wednesday', },
    { value: '4', viewValue: 'Thursday', },
    { value: '5', viewValue: 'Friday', },
    { value: '6', viewValue: 'Saturday', },
    { value: '7', viewValue: 'Sunday', },
  ];



  myControl = new FormControl();
  container3: string[] = [];
  filteredcontainer3: Observable<any[]>;


  constructor(public dialog: MatDialog, public global: GlobalService, private api: ApiService, public http: HttpClient, public excel: ExcelService, @Optional() public dialogRef: MatDialogRef<ClassroomManagerComponent>, @Optional() @Inject(MAT_DIALOG_DATA) public codesManagerData: any) {
    this.configRooms = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  private filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.container3.filter(option => option.toLowerCase().includes(filterValue));
  }

  // Added by Kurt. This funciton is used to close the Classroom Schedule Component
  closeComponent() {
    this.dialogRef.close({ result: 'cancel' });
  }

  ngOnInit() {
    // Added by Kurt. This block is used to access Classroom Schedule from Codes Manager
    if (this.codesManagerData) {
      this.global.swalLoading("Loading Data...")
      // Set a timer for 3 seconds
      setTimeout(() => {
        // Code to execute after 3 seconds
        this.tab = 1
        this.location = this.codesManagerData.building
        this.roomNo = this.codesManagerData.room
        this.location2 = this.codesManagerData.building
        this.roomNo2 = this.codesManagerData.room
        this.location3 = this.codesManagerData.building
        this.roomNo3 = this.codesManagerData.room
        this.selectedRoom = this.codesManagerData.room
        if (this.codesManagerData.action == 'viewClassroom')
          this.generateRoom()
        this.searchButton(null)
        this.global.swalClose()
      }, 700) // 3000 milliseconds = 3 seconds
    }

    this.filteredcontainer3 = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value))
    );

    this.api.getClassroomTypes().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.typeList = result.data;
        this.typeList.sort((a, b) => a.type.localeCompare(b.type));
        // console.log(this.typeList)

        for (var x in this.typeList) {
          this.typeListFinal.push(this.typeList[x].type + " - " + this.typeList[x].typeDesc)
        }
      }
    });

    this.api.getClassroomList().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.arr = result.data;
        // console.log('data', result.data)
      }

      for (var x in this.arr) {
        if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
          this.uniqueOfLocation.push(this.arr[x].location)
        }
      }
      this.generateManageClassroomBuilding()
    })

    this.api.getClassroomList().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.arr2 = result.data;
        // console.log('data', result.data)
      }
      for (var x in this.arr2) {
        if (!this.uniqueOfLocation2.includes(this.arr2[x].location)) {
          this.uniqueOfLocation2.push(this.arr2[x].location)
        }
      }
      // console.log('data', this.uniqueOfLocation2)
      // this.generate()
      this.generateClassroomScheduleBuilding()
    })

    this.api.getClassroomList().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.arr3 = result.data;
        // console.log('data', result.data)
      }

      for (var x in this.arr3) {
        if (!this.uniqueOfLocation3.includes(this.arr3[x].location)) {
          this.uniqueOfLocation3.push(this.arr3[x].location)
        }
      }
      this.generateClassroomAvailabilityBuilding()
    })
  }

  codesManagerDisplay(view){
    if(!this.codesManagerData){
      return false
    }
    if (this.codesManagerData.action == view){
      return true
    }
  }



  // ---------------------------------------------------------------CODES FOR MANAGE CLASSROOM--------------------------------------------------

  //add a new classroom location and update the component with the new data after the dialog for adding a classroom location is closed.
  AddClassroom(location): void {
    this.dataToAdd = [location]
    // console.log('DATA TO ADD: ', this.dataToAdd)
    const dialogRef = this.dialog.open(AddClassroomComponent, {
      data: { selectedData: this.dataToAdd }
    });
    dialogRef.afterClosed().subscribe(data => {
      Swal.showLoading()
      this.api.getClassroomList().map(response => response.json()).subscribe(result => {
        if (result.data != null) {
          this.arr = result.data;
          // console.log('data', result.data)
        }
        for (var x in this.arr) {
          if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
            this.uniqueOfLocation.push(this.arr[x].location)
          }
        }
        Swal.close()
        this.generateManageClassroomBuilding()
      })
      this.filteredArray = []
      this.filteredArray2 = []
    })
  }

  //edit an existing classroom location and 
  //update the component with the edited data after the dialog for editing the classroom location is closed.
  EditEntry(roomNo, seatingCapacity, type, typeDesc): void {
    this.dataToEdit = [this.location, roomNo, seatingCapacity, type, typeDesc];
    // console.log('DATA TO EDIT:', this.dataToEdit);
    const dialogRef = this.dialog.open(UpdateClassroomComponent, {
      data: { selectedData: this.dataToEdit }
    });

    dialogRef.afterClosed().subscribe(data => {
      this.filteredArray = []
      this.filteredArray2 = []
      Swal.showLoading()
      this.api.getClassroomList().map(response => response.json()).subscribe(result => {
        if (result.data != null) {
          this.arr = result.data;
          // console.log('data', result.data)
        }
        for (var x in this.arr) {
          if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
            this.uniqueOfLocation.push(this.arr[x].location)
          }
        }
        Swal.close()
        this.generateManageClassroomBuilding()
      })
    });
  }

  //remove a classroom from the list of classrooms 
  //and update the component with the new data after the classroom is successfully deleted.
  funcRemove(index, roomNo) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.api.deleteClassroom(roomNo).map(response => response.json()).subscribe(result => {
          if (result.message.includes('Classroom cannot be deleted')) {
            this.global.swalAlert(result.message, '', 'warning')
          } else {
            this.filteredArray.splice(index, 1);
            // this.global.swalSuccess('Classroom List was deleted')

            this.api.getClassroomList().map(response => response.json()).subscribe(result => {
              if (result.data != null) {
                this.arr = result.data;
                // console.log('data', result.data)
              }
              for (var x in this.arr) {
                if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
                  this.uniqueOfLocation.push(this.arr[x].location)
                }
              }


              this.generateManageClassroomBuilding()
            })
          }
        })
        // If the user confirms the action to delete the file, execute the delete action and display a success message

        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else {
        // If the user cancels the action, do nothing
        // The dialog box will simply close
      }
    });


  }

  //Clear Filter Search
  funcClear() {
    this.aroomNo = '';
    this.aseatingCapacity = '';
    this.atype = '';
    this.atypeDesc = '';
    return this.filterall(event)
  }


  //This is a function that filters the elements of an array (filteredArray2)
  filterall(event) {
    this.filteredArray = []
    for (var x in this.filteredArray2) {
      if (
        this.filteredArray2[x].roomNo.toLowerCase().includes(this.aroomNo.toLowerCase()) &&
        this.filteredArray2[x].seatingCapacity.toString().includes(this.aseatingCapacity) &&
        this.filteredArray2[x].type.includes(this.atype) &&
        this.filteredArray2[x].typeDesc.toLowerCase().includes(this.atypeDesc.toLowerCase())
      ) {
        this.filteredArray.push(this.filteredArray2[x])
      }
    }
    // console.log('filterall', this.filteredArray2)

  }

  //filter the arr array based on the selected location and store the filtered result in the filteredArray and filteredArray2 arrays.
  generateManageClassroomBuilding() {
    this.filteredArray = []

    for (var x = 0; x < this.arr.length; x++) {
      if (this.location == this.arr[x].location) {
        this.filteredArray.push(this.arr[x])
      }
    }

    this.filteredArray2 = this.filteredArray
    // console.log('generateFilteredAray2', this.filteredArray2)
    // console.log(this.aroomNo)
  }

  // ---------------------------------------------------------------CODES FOR CLASSROOM SCHEDULE--------------------------------------------------

  reloadTable() {
    this.reloading = []
    const classroomTable1 = document.getElementById('classroomTable')
    this.reloading = classroomTable1.innerHTML;
    if (this.reloading != '')
      document.getElementById('classroomTable').innerHTML = this.reloading
  }

  generateClassroomScheduleBuilding() {

    this.newFilteredArray = []
    for (var x = 0; x < this.arr2.length; x++) {
      if (this.location2 == this.arr2[x].location) {
        this.newFilteredArray.push(this.arr2[x])
      }
    }

    this.roomNo = ''
    // console.log(this.newFilteredArray)

    if (this.location != '' && this.roomNo != '') {
      const classroomTable = document.getElementById('classroomTable')
      this.originalHtml = classroomTable.innerHTML;
    }
    this.resetTable()
  }

  resetTable() {

    if (this.originalHtml !== null)

      document.getElementById('classroomTable').innerHTML = this.originalHtml
  }

  async loadData() {
    try {
      const response = await this.api.getClassroomSchedule(this.schoolYear, this.roomNo2).toPromise();
      const result = response.json();
      this.container = result.data;
      if (result != '') {
        for (var i = 0; i < this.container.length; i++) {

          if ((this.container[i].subjectID != "" && this.container[i].departmentCode != "" && this.container[i].codeNo != '' && this.container[i].minutes != 0)) {
            this.tempArray.push({
              codeNo: this.container[i].codeNo,
              departmentCode: this.container[i].departmentCode,
              instructor: this.container[i].instructor,
              minutes: this.container[i].minutes,
              remarks: this.container[i].remarks,
              roomNumber: this.container[i].roomNumber,
              schedule: this.container[i].schedule,
              subjectID: this.container[i].subjectID,
              theDay: this.container[i].theDay,
              type: this.container[i].type,
              vacantSched: this.container[i].vacantSched
            });
          }
        }

        for (var x in this.tempArray) {
          // if ((this.classroomArray[x].subjectID == "" && this.classroomArray[x].departmentCode == ""))
          //   this.classroomArray.splice(parseInt(x), 1)

          for (var y = parseInt(x) + 1; y < this.tempArray.length; y++) {
            if (this.tempArray[y].vacantSched == this.tempArray[x].vacantSched && this.tempArray[y].theDay == this.tempArray[x].theDay) {
              this.tempArray[x].codeNo = this.tempArray[x].codeNo + ' / ' + this.tempArray[y].codeNo
              this.tempArray[x].departmentCode = this.MergeClass

              if (this.tempArray[x].instructor == "Unassigned" && this.tempArray[y].instructor == "Unassigned") {
                this.tempArray[x].instructor = this.tempArray[x].instructor

              } else if (this.tempArray[x].instructor == "Unassigned" && this.tempArray[y].instructor != "") {
                this.tempArray[x].instructor = this.tempArray[y].instructor

              } else if (this.tempArray[x].instructor != "" && this.tempArray[y].instructor == "Unassigned") {
                this.tempArray[x].instructor = this.tempArray[x].instructor

              } else if (this.tempArray[x].instructor == this.tempArray[y].instructor) {
                const lastName = this.tempArray[x].instructor.slice(0, this.tempArray[x].instructor.indexOf(',') + 3) + '.';
                this.tempArray[x].instructor = lastName
              }

              else if (this.tempArray[x].instructor != this.tempArray[y].instructor) {

                const instructor1 = this.tempArray[x].instructor.slice(0, this.tempArray[x].instructor.indexOf(',') + 3) + '.';
                const instructor2 = this.tempArray[y].instructor.slice(0, this.tempArray[y].instructor.indexOf(',') + 3) + '.';

                this.tempArray[x].instructor = instructor1 + ' / ' + instructor2
              }
              this.tempArray.splice(y, 1)

            }
          }
        }

        for (var j = 0; j < this.tempArray.length; j++) {
          if (this.tempArray[j].departmentCode != 'MERGECLASS') {
            const lastName = this.tempArray[j].instructor.slice(0, this.tempArray[j].instructor.indexOf(',') + 3) + '.';
            this.tempArray[j].instructor = lastName
          }

        }

        // console.log('afterfiltered', this.tempArray)

        this.tempArray.forEach(classroom => {

          // console.log(this.tempArray);
          const day = classroom.theDay;

          //['07:00AM','08:00AM']
          const splitSched = classroom.vacantSched.split('-');

          const cell = document.getElementById(`${day}-${splitSched[0]}`)
          if (!cell || cell.innerHTML != '') {
            // console.log('Conflict Schedule');

            if (`${classroom.theDay}` === '1') {
              classroom.theDay = 'MONDAY'
            } else if (`${classroom.theDay}` === "2") {
              classroom.theDay = 'TUESDAY'
            } else if (`${classroom.theDay}` === "3") {
              classroom.theDay = 'WEDNESDAY'
            } else if (`${classroom.theDay}` === "4") {
              classroom.theDay = 'THURSDAY '
            } else if (`${classroom.theDay}` === "5") {
              classroom.theDay = 'FRIDAY'
            } else if (`${classroom.theDay}` === "6") {
              classroom.theDay = 'SATURDAY'
            } else if (`${classroom.theDay}` === "7") {
              classroom.theDay = 'SUNDAY'
            }

            this.tempArray2.push({
              codeNo: classroom.codeNo,
              departmentCode: classroom.departmentCode,
              instructor: classroom.instructor,
              minutes: classroom.minutes,
              remarks: classroom.remarks,
              roomNumber: classroom.roomNumber,
              schedule: classroom.schedule,
              subjectID: classroom.subjectID,
              theDay: classroom.theDay,
              type: classroom.type,
              vacantSched: classroom.vacantSched
            })

          } else {

            // console.log(cell.id)
            const rowSpanValue = classroom.minutes / 30; //3
            // const lastName = classroom.instructor.slice(0, classroom.instructor.indexOf(',') + 3) + '.';
            cell.innerHTML = '<b>' + classroom.codeNo + '</b>' + '<br>' + classroom.subjectID + '<br>' + classroom.instructor;
            cell.style.textAlign = 'center';

            switch (`${classroom.departmentCode}`) {
              case 'SEAITE':
                cell.style.backgroundColor = '#660000';
                cell.style.color = '#ffffff';
                break;
              case 'SEAS':
                cell.style.backgroundColor = '#0000FF';
                cell.style.color = '#ffffff';
                break;
              case 'SABH':
                cell.style.backgroundColor = '#C79500';
                cell.style.color = '#ffffff';
                break;
              case 'SHAS':
                cell.style.backgroundColor = '#006633';
                cell.style.color = '#ffffff';
                break;
              case 'MERGECLASS':
                cell.style.backgroundColor = '#7f7f7f';
                cell.style.color = '#ffffff';
                break;
              // case 'GS':
              //   cell.style.backgroundColor = '#006633';
              //   cell.style.color = '#ffffff';
              //   break;
            }

            cell.setAttribute('rowSpan', rowSpanValue.toString());

            var currentTime = splitSched[0]
            // console.log()
            //Initial Value : 11:00AM && rowSpanValue: 4
            for (var x = 1; x < rowSpanValue; x++) {

              currentTime = handleIncrement(currentTime);
              //11:30AM && 12:00PM && 12:30PM
              // console.log(`CELL TO BE DELETED: ${day}-${currentTime}`);
              document.getElementById(`${day}-${currentTime}`).remove();
              // console.log((`${day}-${currentTime}`))
            }
          }
        })

        this.reloadTable()

        function formatTime(time) {
          if (time < 10) {
            return "0" + time;
          }
          return time;
        }

        function handleIncrement(time) {
          let splitTime = time.split(":");
          let minutesWODayTime = splitTime[1].substring(0, 2);
          let dayofTime = splitTime[1].substring(2, 4);
          let updatedTime = "";

          if (minutesWODayTime === '30') {
            updatedTime = `${formatTime(parseInt(splitTime[0]) + 1)}:00${dayofTime}`;
            if (parseInt(splitTime[0]) + 1 == 13) {
              updatedTime = "01:00PM"
              dayofTime = "PM";
            }
            else if (parseInt(splitTime[0]) + 1 === 12) {
              updatedTime = "12:00PM";
              dayofTime = "PM";
            }
          }

          else {
            updatedTime = `${splitTime[0]}:30${dayofTime}`;
          }
          return updatedTime;
        }
      }
    } catch (error) {
      console.error(error);
    }
  }

  generateRoom() {
    this.tempArray = []
    this.tempArray2 = []

    if (this.originalHtml === null && document.getElementById('classroomTable')) {
      this.originalHtml = document.getElementById('classroomTable').innerHTML;
    }
    this.resetTable()
    this.loadData()

  }

  leftRightOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.font = { name: 'Calibri', family: 4, size: 12, strike: false };
      cell.alignment = { horizontal: 'center' };
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        top: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //generates an Excel spreadsheet containing the list of classrooms in a specific building location. 
  async downloadClassroomScheduleExcel() {
    if (this.location != null) {
      const date = new Date()
        .toISOString()
        .slice(0, 10)
        .split("-")
        .reverse()
        .join("/");
      // console.log(date);
      const workbook = new ExcelJS.Workbook();
      const worksheet = workbook.addWorksheet("My Sheet");

      let input = this.schoolYear;

      let year1 = input.substring(0, 4); //gets the data from index 0 to index 4
      let year2 = input.substring(4, 6);
      let suffix = input.substring(6, 7);

      function getSemesterName(suffix: string): string {
        if (suffix === '1') {
          return "First Semester ";
        }
        else if (suffix === '2') {
          return "Second Semester ";
        }
        else {
          return "Summer ";
        }
      }

      let output = year1 + " - " + '20' + year2;
      let output2 = 'School Year ' + output
      await worksheet.protect('CiCT#2020') // locks the whole excel file

      let h1 = worksheet.addRow([getSemesterName(suffix) + output2]);
      h1.font = { name: 'Calibri', family: 4, size: 15, bold: true, strike: false };
      h1.alignment = { vertical: 'top', horizontal: 'center' };
      h1.fill = { type: 'pattern' };

      worksheet.getCell('A3').value = (this.roomNo2)
      worksheet.getCell('B3').value = (this.location2)
      worksheet.getCell('B4').value = "MON"
      worksheet.getCell('C4').value = "TUE"
      worksheet.getCell('D4').value = "WED"
      worksheet.getCell('E4').value = "THU"
      worksheet.getCell('F4').value = "FRI"
      worksheet.getCell('G4').value = "SAT"
      worksheet.getCell('H4').value = "SUN"

      let dataRow
      for (var x = 2; x <= 4; x++) {
        dataRow = worksheet.addRow(
          [
            '',
            '1-0' + (x + 5) + ':00' + 'AM',
            '2-0' + (x + 5) + ':00' + 'AM',
            '3-0' + (x + 5) + ':00' + 'AM',
            '4-0' + (x + 5) + ':00' + 'AM',
            '5-0' + (x + 5) + ':00' + 'AM',
            '6-0' + (x + 5) + ':00' + 'AM',
            '7-0' + (x + 5) + ':00' + 'AM',
          ]
        )
        this.leftRightOutline(dataRow)
        dataRow = worksheet.addRow(
          [
            '',
            '1-0' + (x + 5) + ':30' + 'AM',
            '2-0' + (x + 5) + ':30' + 'AM',
            '3-0' + (x + 5) + ':30' + 'AM',
            '4-0' + (x + 5) + ':30' + 'AM',
            '5-0' + (x + 5) + ':30' + 'AM',
            '6-0' + (x + 5) + ':30' + 'AM',
            '7-0' + (x + 5) + ':30' + 'AM',
          ]
        )
        this.leftRightOutline(dataRow)//sets each cells border property to thin
      }

      for (var x = 5; x <= 6; x++) {
        dataRow = worksheet.addRow(
          [
            '',
            '1-' + (x + 5) + ':00' + 'AM',
            '2-' + (x + 5) + ':00' + 'AM',
            '3-' + (x + 5) + ':00' + 'AM',
            '4-' + (x + 5) + ':00' + 'AM',
            '5-' + (x + 5) + ':00' + 'AM',
            '6-' + (x + 5) + ':00' + 'AM',
            '7-' + (x + 5) + ':00' + 'AM',
          ]
        )
        this.leftRightOutline(dataRow)
        dataRow = worksheet.addRow(
          [
            '',
            '1-' + (x + 5) + ':30' + 'AM',
            '2-' + (x + 5) + ':30' + 'AM',
            '3-' + (x + 5) + ':30' + 'AM',
            '4-' + (x + 5) + ':30' + 'AM',
            '5-' + (x + 5) + ':30' + 'AM',
            '6-' + (x + 5) + ':30' + 'AM',
            '7-' + (x + 5) + ':30' + 'AM',
          ]
        )
        this.leftRightOutline(dataRow)//sets each cells border property to thin
      }

      for (var x = 7; x <= 7; x++) {
        dataRow = worksheet.addRow(
          [
            '',
            '1-' + (x + 5) + ':00' + 'PM',
            '2-' + (x + 5) + ':00' + 'PM',
            '3-' + (x + 5) + ':00' + 'PM',
            '4-' + (x + 5) + ':00' + 'PM',
            '5-' + (x + 5) + ':00' + 'PM',
            '6-' + (x + 5) + ':00' + 'PM',
            '7-' + (x + 5) + ':00' + 'PM',
          ]
        )
        this.leftRightOutline(dataRow)
        dataRow = worksheet.addRow(
          [
            '',
            '1-' + (x + 5) + ':30' + 'PM',
            '2-' + (x + 5) + ':30' + 'PM',
            '3-' + (x + 5) + ':30' + 'PM',
            '4-' + (x + 5) + ':30' + 'PM',
            '5-' + (x + 5) + ':30' + 'PM',
            '6-' + (x + 5) + ':30' + 'PM',
            '7-' + (x + 5) + ':30' + 'PM',
          ]
        )
        this.leftRightOutline(dataRow)//sets each cells border property to thin
      }

      for (var x = 1; x <= 9; x++) {
        dataRow = worksheet.addRow(
          [
            '',
            '1-0' + (x) + ':00' + 'PM',
            '2-0' + (x) + ':00' + 'PM',
            '3-0' + (x) + ':00' + 'PM',
            '4-0' + (x) + ':00' + 'PM',
            '5-0' + (x) + ':00' + 'PM',
            '6-0' + (x) + ':00' + 'PM',
            '7-0' + (x) + ':00' + 'PM',
          ]
        )
        this.leftRightOutline(dataRow)
        dataRow = worksheet.addRow(
          [
            '',
            '1-0' + (x) + ':30' + 'PM',
            '2-0' + (x) + ':30' + 'PM',
            '3-0' + (x) + ':30' + 'PM',
            '4-0' + (x) + ':30' + 'PM',
            '5-0' + (x) + ':30' + 'PM',
            '6-0' + (x) + ':30' + 'PM',
            '7-0' + (x) + ':30' + 'PM',
          ]
        )
        this.leftRightOutline(dataRow)//sets each cells border property to thin
      }

      this.tempArray.forEach(element => {
        const splitSched = element.vacantSched.split('-');
        const day = element.theDay
        const rowSpanValue = element.minutes / 30; //3

        let cell;
        worksheet.eachRow({ includeEmpty: true }, function (row, rowNumber) {
          row.eachCell({ includeEmpty: true }, function (c, colNumber) {

            if (c.value === `${day}-${splitSched[0]}`) {

              cell = c;

              if (colNumber == 2) {
                colNumber = 'B'
              } else if (colNumber == 3) {
                colNumber = 'C'
              } else if (colNumber == 4) {
                colNumber = 'D'
              } else if (colNumber == 5) {
                colNumber = 'E'
              } else if (colNumber == 6) {
                colNumber = 'F'
              } else if (colNumber == 7) {
                colNumber = 'G'
              }

              // console.log(colNumber, rowNumber, rowSpanValue)

              worksheet.mergeCells(`${(colNumber)}${rowNumber}:${(colNumber)}${rowNumber + rowSpanValue - 1}`);
              cell.value = {
                richText: [
                  { 'font': { 'bold': true, 'color': { 'argb': 'FFFFFF' }, 'size': 11, }, text: element.codeNo },
                  { text: '\n' },
                  { 'font': { 'color': { 'argb': 'FFFFFF' }, 'size': 8, }, text: element.subjectID },
                  { text: '\n' },
                  { 'font': { 'color': { 'argb': 'FFFFFF' }, 'size': 8, }, text: element.instructor },
                ],
              };

              if (`${element.departmentCode}` === 'SEAITE') {
                cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '660000' } };
                // cell.style.backgroundColor = '#660000';
              } else if (`${element.departmentCode}` === "SEAS") {
                cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '0000ff' } };
                // cell.style.backgroundColor = '#0000FF';
              } else if (`${element.departmentCode}` === "SABH") {
                cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: 'C79500' } };
                // cell.style.backgroundColor = '#C79500';
              } else if (`${element.departmentCode}` === "SHAS") {
                cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '006633' } };
                // cell.style.backgroundColor = '#006633';
              } else if (`${element.departmentCode}` === "MERGECLASS") {
                cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '7f7f7f' } };
                // cell.style.backgroundColor = '#d1a9ae';
              }
              // else if (`${element.departmentCode}` === "GS") {
              //   cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '006633' } };
              //   // cell.style.backgroundColor = '#d1a9ae';
              // }

            }

          });
        });
      });

      worksheet.eachRow({ includeEmpty: true }, function (row, rowNumber) {
        if (rowNumber > 2) {
          row.eachCell({ includeEmpty: true }, function (cell, colNumber) {
            // check if the cell background color is not set
            if (!cell.fill) {
              // reset cell value to empty
              cell.value = null;
            }
          });
        }
      });


      worksheet.getCell('A3').value = (this.roomNo2)
      worksheet.getCell('B3').value = (this.location2)
      worksheet.getCell('B4').value = "MON"
      worksheet.getCell('C4').value = "TUE"
      worksheet.getCell('D4').value = "WED"
      worksheet.getCell('E4').value = "THU"
      worksheet.getCell('F4').value = "FRI"
      worksheet.getCell('G4').value = "SAT"
      worksheet.getCell('H4').value = "SUN"
      worksheet.getCell('A5').value = "07:00 AM"
      worksheet.getCell('A7').value = "08:00 AM"
      worksheet.getCell('A9').value = "09:00 AM"
      worksheet.getCell('A11').value = "10:00 AM"
      worksheet.getCell('A13').value = "11:00 AM"
      worksheet.getCell('A15').value = "12:00 PM"
      worksheet.getCell('A17').value = "01:00 PM"
      worksheet.getCell('A19').value = "02:00 PM"
      worksheet.getCell('A21').value = "03:00 PM"
      worksheet.getCell('A23').value = "04:00 PM"
      worksheet.getCell('A25').value = "05:00 PM"
      worksheet.getCell('A27').value = "06:00 PM"
      worksheet.getCell('A29').value = "07:00 PM"
      worksheet.getCell('A31').value = "08:00 PM"
      worksheet.getCell('A33').value = "09:00 PM"

      worksheet.mergeCells('A1:H1');
      worksheet.mergeCells('A2:H2');
      worksheet.mergeCells('A3:A4');
      worksheet.mergeCells('B3:H3');

      worksheet.getColumn(1).width = 10;
      worksheet.getColumn(1).alignment = { horizontal: 'center' };


      for (var x = 2; x < 9; x++) {
        worksheet.getColumn(x).width = 11.5;
        worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      }
      for (var x = 5; x < 33; x++) {
        worksheet.getRow(x).height = 22;

      }


      worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('A3').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('A3').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('B3').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('B3').alignment = { vertical: 'top', horizontal: 'center', wrapText: true };
      worksheet.getCell('B3').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin', color: { argb: '#FFFFFF' } }, right: { style: 'thin' } };
      worksheet.getCell('B3').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('A4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('A4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('B4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('B4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('B4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('B4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('C4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('C4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('C4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('C4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('D4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('D4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('D4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('D4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('E4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('E4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('E4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('E4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('F4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('F4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('F4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('F4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('G4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('G4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('G4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('G4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('H4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('H4').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('H4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }
      worksheet.getCell('H4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

      worksheet.getCell('A5').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A5').alignment = { vertical: 'top', horizontal: 'center' };
      worksheet.getCell('A7').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A7').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A9').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A9').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A11').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A11').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A13').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A13').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A15').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A15').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A17').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A17').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A19').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A19').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A21').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A21').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A23').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A23').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A25').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A25').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A27').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A27').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A29').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A29').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A31').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A31').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A33').font = { name: 'Calibri', family: 4, size: 12, strike: false };
      worksheet.getCell('A33').alignment = { vertical: 'middle', horizontal: 'center' };

      worksheet.mergeCells('A34:H34');
      worksheet.getCell('A34').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      // console.log("Printlog: ", this.downloadExcel)

      workbook.xlsx.writeBuffer().then((data: any) => {
        // console.log("buffer");
        const blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });

        let url = window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = "Classroom-Schedule.xlsx";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
        Swal.fire(
          'Download Succes!',
          '',
          'success')
      });
    } else {
      this.global.swalAlert("Please Select Building Location", 'NO INPUT!', 'warning')
    }
  }

  // ---------------------------------------------------------------CODES FOR CLASSROOM AVAILABILTY--------------------------------------------------

  pageChangedRooms(event) {
    this.configRooms.currentPage = event;
  }

  generateClassroomAvailabilityBuilding() {
    this.filteredArray3 = []

    for (var x = 0; x < this.arr3.length; x++) {
      if (this.location3 == this.arr3[x].location) {
        this.filteredArray3.push(this.arr3[x])
      }
    }
  }


  searchButton(event) {
    // console.log(this.selectedRoom)
    this.api.getClassroomAvailable(this.schoolYear, this.selectedRoom, this.startTime, this.endTime, this.day, this.classroomType).map(response => response.json()).subscribe(result => {
      this.container2 = result.data
      this.classroomAvailArray = this.container2
      this.newFilteredArray2 = this.classroomAvailArray
      // console.log(this.container2)
      var count = Object.keys(this.classroomAvailArray).length;
      this.ctr = count

      // added by Kurty
      if (this.codesManagerData.action == 'selectSchedule') {
        this.classroomAvailArray = this.removeShortDurationRows(this.classroomAvailArray);
      }
    })
  }

  // added by Kurty
  removeShortDurationRows(classroomAvailArray) {
    var array = []
    for (var x in classroomAvailArray) {
      const [start, end] = classroomAvailArray[x].vacantSched.split('-');
      var duration = this.isDurationLessThanOneHour(start, end)
      classroomAvailArray[x].duration = this.convertToTimeFormat(duration)
      classroomAvailArray[x].checked = false

      // @ts-ignore
      if (duration >= 60) {
        array.push(classroomAvailArray[x])
      }
    }
    return array
  }

  // added by Kurty
  isDurationLessThanOneHour(startTime: string, endTime: string) {
    // Parse start time
    const startParts = startTime.match(/(\d{2}):(\d{2})(AM|PM)/);
    if (!startParts) return false;
    let startHour = parseInt(startParts[1]);
    const startMinute = parseInt(startParts[2]);
    const startSuffix = startParts[3];
    // Adjust start hour based on AM/PM
    if (startSuffix === 'PM' && startHour !== 12) {
      startHour += 12;
    } else if (startSuffix === 'AM' && startHour === 12) {
      startHour = 0;
    }
    // Parse end time
    const endParts = endTime.match(/(\d{2}):(\d{2})(AM|PM)/);
    if (!endParts) return false;
    let endHour = parseInt(endParts[1]);
    const endMinute = parseInt(endParts[2]);
    const endSuffix = endParts[3];
    // Adjust end hour based on AM/PM
    if (endSuffix === 'PM' && endHour !== 12) {
      endHour += 12;
    } else if (endSuffix === 'AM' && endHour === 12) {
      endHour = 0;
    }
    // Calculate the duration in minutes
    const durationMinutes = (endHour * 60 + endMinute) - (startHour * 60 + startMinute);
    // Check if duration is less than 60 minutes
    return durationMinutes;
  }

  // added by Kurty
  convertToTimeFormat(minutes) {
    if (minutes === 0) {
      return 'Zero minutes';
    }
    let hours = Math.floor(minutes / 60);
    let remainingMinutes = minutes % 60;
    let result = '';
    if (hours > 0) {
      result += hours === 1 ? 'One hour' : `${hours} hours`;
      if (remainingMinutes > 0) {
        result += ' and ';
      }
    }
    if (remainingMinutes > 0) {
      result += remainingMinutes === 1 ? 'One minute' : `${remainingMinutes} minutes`;
    }
    return result;
  }

  // added by Kurty
  selectSchedule(day, time) {
    this.dialogRef.close({ result: 'single', day: day, time: time });
  }

  // added by Kurty
  selectMultiple() {
    var array = []
    for (var x in this.classroomAvailArray){
      if (this.classroomAvailArray[x].checked){
        array.push(this.classroomAvailArray[x])
      }
    }
    this.dialogRef.close({ result: 'multiple', array: array });
  }

  // added by Kurty
  detectMultipleSelection() {
    for (var x in this.classroomAvailArray) {
      if (this.classroomAvailArray[x].checked) {
        return true
      }
    }
    return false
  }

  filterall2(event) {
    this.classroomAvailArray = []
    for (var x in this.newFilteredArray2) {
      if (
        this.newFilteredArray2[x].roomNumber.toLowerCase().includes(this.broomNo.toLowerCase()) &&
        this.newFilteredArray2[x].type.toLowerCase().includes(this.btype.toLowerCase()) &&
        this.newFilteredArray2[x].theDay.toLowerCase().includes(this.bTheDay.toLowerCase()) &&
        this.newFilteredArray2[x].vacantSched.toLowerCase().toString().includes(this.bVacantSchedule.toLowerCase())
      ) {
        this.classroomAvailArray.push(this.newFilteredArray2[x])
      }
    }
    // console.log('filterall2', this.classroomAvailArray)
  }


  // classroom() {
  //   this.classroomAvailArray = []

  //   for (var x = 0; x < this.container2.length; x++) {
  //     if (this.selectedRoom.toLowerCase() == (this.container2[x].roomNumber).toLowerCase()) {
  //       this.classroomAvailArray.push(this.container2[x]
  //       )
  //     }
  //   }

  //   this.newFilteredArray2 = this.classroomAvailArray
  //   console.log('classroomfilteredArray', this.newFilteredArray2)
  // }

  async downloadClassroomAvailabilityExcel() {
    if (this.location != null) {
      const date = new Date()
        .toISOString()
        .slice(0, 10)
        .split("-")
        .reverse()
        .join("/");
      // console.log(date);
      const workbook = new ExcelJS.Workbook();
      const worksheet = workbook.addWorksheet("My Sheet");

      let input = this.schoolYear;

      let year1 = input.substring(0, 4); //gets the data from index 0 to index 4
      let year2 = input.substring(4, 6);
      let suffix = input.substring(6, 7);

      function getSemesterName(suffix: string): string {
        if (suffix === '1') {
          return "FIRST SEMESTER ";
        }
        else if (suffix === '2') {
          return "SECOND SEMESTER ";
        }
        else {
          return "SUMMER ";
        }
      }

      let output = year1 + " - " + '20' + year2;
      let output2 = 'SCHOOL YEAR ' + output
      await worksheet.protect('CiCT#2020') // locks the whole excel file

      let h1 = worksheet.addRow(['UNIVERSITY OF SAINT LOUIS']);
      this.leftRightOutline(h1)
      h1.alignment = { vertical: 'top', horizontal: 'center' };
      h1.font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false };

      let h2 = worksheet.addRow([getSemesterName(suffix) + output2]);
      this.leftRightOutline(h2)
      h2.font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false };
      h2.alignment = { vertical: 'top', horizontal: 'center' };

      let h3 = worksheet.addRow(['LIST OF VACANT ROOM SCHEDULE']);
      this.leftRightOutline(h3)
      h3.alignment = { vertical: 'top', horizontal: 'center' };
      h3.font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false };

      worksheet.mergeCells('A1:E1');
      worksheet.mergeCells('A2:E2');
      worksheet.mergeCells('A3:E3');

      worksheet.getColumn(1).width = 10;
      for (var x = 2; x < 6; x++) {
        worksheet.getColumn(x).width = 20;
        // worksheet.getColumn(x).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      }

      worksheet.getCell('A4').value = ('NO.')
      worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('A4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('A4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('B4').value = ('ROOM NUMBER')
      worksheet.getCell('B4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('B4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('B4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('B4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('C4').value = ('CLASSROOM TYPE')
      worksheet.getCell('C4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('C4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('C4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('C4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('D4').value = ('DAY')
      worksheet.getCell('D4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('D4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('D4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('D4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      worksheet.getCell('E4').value = ('VACANT SCHEDULE')
      worksheet.getCell('E4').font = { name: 'Calibri', family: 4, size: 12, bold: true, strike: false, color: { argb: 'FFFFFFFF' } };
      worksheet.getCell('E4').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      worksheet.getCell('E4').border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      worksheet.getCell('E4').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '2c4d7f' } }

      var dataRow: any

      for (var x = 0; x < this.classroomAvailArray.length; x++) {

        dataRow = worksheet.addRow([
          1 + x,
          this.classroomAvailArray[x].roomNumber,
          this.classroomAvailArray[x].type,
          this.classroomAvailArray[x].theDay,
          this.classroomAvailArray[x].vacantSched])
        this.leftRightOutline(dataRow)
      }


      // console.log("Printlog: ", this.downloadExcel)

      workbook.xlsx.writeBuffer().then((data: any) => {
        // console.log("buffer");
        const blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });

        let url = window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = "Vacant-Room-Schedules.xlsx";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
        Swal.fire(
          'Download Succes!',
          '',
          'success')
      });
    }
    // else {
    //   this.global.swalAlert("Please Select Building Location", 'NO INPUT!', 'warning')
    // }
  }
}