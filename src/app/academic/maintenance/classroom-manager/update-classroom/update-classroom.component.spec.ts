import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClassroomComponent } from './update-classroom.component';

describe('UpdateClassroomComponent', () => {
  let component: UpdateClassroomComponent;
  let fixture: ComponentFixture<UpdateClassroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClassroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClassroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
