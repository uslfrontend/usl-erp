import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { ApiService } from './../../../../api.service'
import { GlobalService } from './../../../../global.service';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ClassroomManagerComponent } from '../classroom-manager.component';
import Swal from 'sweetalert2'
import { type } from 'os';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-update-classroom',
  templateUrl: './update-classroom.component.html',
  styleUrls: ['./update-classroom.component.css']
})
export class UpdateClassroomComponent implements OnInit {
  dataToEdit: any[];
  tabledata
  tableArr = []
  findby
  x
  string = ''
  typeListFinal = []
  dataSource = new MatTableDataSource<any>([]);


  typeList: any
  // seatingCapacity = ''
  // type = ''
  // typeDesc = ''
  // statusCode = ''
  
  // aroomNo = ''
  // aseatingCapacity= ''
  // astatusCode = ''

  search = ''
  arr

  location = this.data.selectedData[0]
  aroomNo = this.data.selectedData[1]
  aseatingCapacity = this.data.selectedData[2]
  atypeDesc = this.data.selectedData[3] + ' - ' + this.data.selectedData[4]

  uniqueOfLocation = []
  filteredArray = []
  filteredArray2 = []

  pagesize = 50
  pageno = 1
  totalpageno = 0
  constructor(public dialogRef: MatDialogRef<UpdateClassroomComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private api: ApiService, public global: GlobalService) { }

  submitData() {
    this.dialogRef.close(this.data);
  }
  ngOnInit() {
    Swal.showLoading()
    this.api.getClassroomList().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.arr = result.data;
        // console.log('data', result.data)
      }
      for (var x in this.arr) {
        if (!this.uniqueOfLocation.includes(this.arr[x].location)) {
          this.uniqueOfLocation.push(this.arr[x].location)
          Swal.close()
        }
      }
    })
    this.api.getClassroomTypes().map(response => response.json()).subscribe(result => {
      if (result.data != null) {
        this.typeList = result.data;
        this.typeList.sort((a, b) => a.type.localeCompare(b.type));
        // console.log(this.typeList)

        for (var x in this.typeList) {
          this.typeListFinal.push(this.typeList[x].type + " - " + this.typeList[x].typeDesc)

        }

        for (var x in this.typeListFinal) {
          if (this.atypeDesc == this.typeListFinal[x]) {
          }
        }
      }
    });
  }
  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
  //filter the arr array based on the selected location and store the filtered result in the filteredArray and filteredArray2 arrays.
  generate() {
    this.filteredArray = []
    for (var x = 0; x < this.arr.length; x++) {
      if (this.location == this.arr[x].location) {
        this.filteredArray.push(this.arr[x])
      }
    }
    this.filteredArray2 = this.filteredArray
  }

  //updating a classroom object in an API, with validation to ensure that all required fields are filled in before the update is made.
  Update() {
    for (var x in this.typeList) {
      if (this.typeList[x].type + " - " + this.typeList[x].typeDesc === this.atypeDesc) {

        if (this.location != '' && this.aroomNo != '' && this.aseatingCapacity != '' && this.typeList[x].type != '' && this.typeList[x].typeDesc != '') {
          // console.log("Location: ", this.location,
          //   "Room Number: ", this.aroomNo,
          //   "Seating Capacity: ", this.aseatingCapacity,
          //   "Type: ", this.typeList[x].type,
          //   "Type Description: ", this.typeList[x].typeDesc);

          this.api.putClassroom(this.aroomNo, {
            "roomNo": this.aroomNo,
            "seatingCapacity": this.aseatingCapacity.toString(),
            "location": this.location,
            "type": this.typeList[x].type,
            "typeDesc": this.typeList[x].typeDesc
          })

            .map(response => response.json())
            .subscribe(res => {
              const newData = res.data; // extract the data from the response
              this.dataSource.data = newData; // update the data source with the new data
              this.global.swalSuccess(res.message);
              this.dialogRef.close({ result: 'nice' });
            }, error => {
              this.global.swalAlertError(Error);
              // console.error(error)
            });
        } else {
          this.global.swalAlert("Please fill in the required fields!", "", 'warning');
        }
        break;
      }
    }
  }
}