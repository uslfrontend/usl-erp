import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import Swal from 'sweetalert2';

import {ClearOnlineSubmittedRequirementsComponent} from './../clear-online-submitted-requirements.component';

const swal = Swal;

@Component({
  selector: 'app-clear-online-submitted-requirements-preenrollment',
  templateUrl: './preenrollment.component.html',
  styleUrls: ['./preenrollment.component.css']
})
export class ClearOnlineSubmittedRequirementsPreenrollmentComponent implements OnInit {

  reqToClear=0;
  reqToClearName;
  StudPreEnrolledWithSubmittedReq;
  listCount: number=0;
  timeLeft: number=0;
  interval;
  recordCleared: number = 0;
  IsWait = false;
  student = '';
  disablePauseBtn = true;
  disableResetBtn = true;
  newSY;
  sublevel;
  disableReqToClear;
  disableSubLevel=false;
  
  constructor(public global: GlobalService, private api: ApiService, private clearMain:  ClearOnlineSubmittedRequirementsComponent) { }

  ngOnInit() {
    if(this.global.domain==='HIGHSCHOOL' || this.global.domain==='ELEMENTARY'){
      this.newSY=this.global.syear.slice(0,-1);
    } else {
      this.newSY=this.global.syear;
    }
  }

  onChangeSubLevel(value){
    this.disableReqToClear=false;
    this.clear();
    this.sublevel=value.value;
  }

  onChange(value){
    this.disableResetBtn=true;
    this.reqToClear=value.value;
    this.recordCleared=0;
    this.getList(this.reqToClear);      
    switch (value.value){
      case "1" : {
        this.reqToClearName = 'Proof of Payment';
        break;
      }
      case "2" : {
        this.reqToClearName = 'Birth Certificate';
        break;
      }
      case "3" : {
        this.reqToClearName = 'ID Picture';
        break;
      }
      case "4" : {
        this.reqToClearName = 'Signature';
        break;
      }
      case "5" : {
        this.reqToClearName = 'Report Card';
        break;
      }
    }
  }

  clearSubmittedReq(schoolYear, submittedReq, idNumber) {
    this.api.putreEnrolledStudent({
      "schoolYear": schoolYear,
      "submittedReq": submittedReq,
      "idNumber": idNumber
    })
      .map(respose => respose.json())
      .subscribe(res => {
        if (res.data == null) {
          this.resetTimer();
          this.global.swalAlert("Error", "Something went wrong. Please contact your system provider.", "");
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getList(x) {
    this.global.swalLoading('Retrieving List');
    var subL='';
    if(this.global.domain==='HIGHSCHOOL'){
      subL=this.sublevel;
    }
    this.api.getPreEnrolledStudents(this.newSY, parseInt(x),this.global.domain,subL) //SubmittedReq (1=ProofOfPaymentCount, 2=ReportCardCount)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          //console.log(res);
          this.StudPreEnrolledWithSubmittedReq = [];
          this.listCount = res.data.length;
          this.timeLeft = res.data.length;
          //this.timeLeft=2;
          this.StudPreEnrolledWithSubmittedReq = res.data;
          this.global.swalClose();
        } else {
          //console.log('no data');
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  swalConfirm(){
    swal.fire({
      type: 'error',
      title: 'Are you sure?',
      html: "<p>You won't be able to revert this! <br/> Double check your Active Configuration!</p>",
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.startTimer();
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire({
          title: 'Cancelled',
          type: 'error',
          text: 'Changes are not saved!',
          timer: 1500
        })
      }
    })
  }

  startTimer() {
    this.disableSubLevel=true;
    this.disableReqToClear=true;
    this.clearMain.disableTab=true;
    this.recordCleared=0;       
    this.IsWait = true;
    this.disablePauseBtn = false;
    this.disableResetBtn = false;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        this.recordCleared++;
        this.student = this.StudPreEnrolledWithSubmittedReq[this.timeLeft].name;
        this.clearSubmittedReq(this.newSY, this.reqToClear, this.StudPreEnrolledWithSubmittedReq[this.timeLeft].idNumber);
        //stop process if tab index changed    
        console.log(this.clearMain.tabbing)
        if(this.clearMain.tabbing == 0){
          this.clear();
        }   
      } else {
        this.global.swalAlert("Success", "Your have successfully cleared " + this.recordCleared + " record/s of " + this.reqToClearName, "info");    
        let holdSublevel=this.sublevel;
        let holdTeqToClear=this.reqToClear;   
        this.clear();
        if(this.global.domain==='HIGHSCHOOL' || this.global.domain==='ELEMENTARY'){
          this.disableReqToClear=true;
        }
        this.sublevel=holdSublevel;
        this.reqToClear=holdTeqToClear;
        this.disableResetBtn=true;
        this.disableReqToClear=false;
        this.recordCleared=0;
        this.getList(this.reqToClear);
      }
    }, 300)
  }

  pauseTimer(){
    this.disablePauseBtn = true;
    this.student = '';
    this.IsWait = false;
    clearInterval(this.interval);
    this.clearMain.disableTab=false;  
    this.disableSubLevel=false;   
    this.disableReqToClear=false;  
  }

  resetTimer(){
    this.recordCleared = 0;
    this.disablePauseBtn = true;
    this.disableResetBtn = true;
    this.student = '';
    clearInterval(this.interval);
    this.IsWait = false;
    this.getList(this.reqToClear);
    this.clearMain.disableTab=false;    
    this.disableSubLevel=false;
    this.disableReqToClear=false;
  }

  clear(){
    this.recordCleared = 0;
    this.disablePauseBtn = true;
    this.disableResetBtn = true;
    this.student = '';
    clearInterval(this.interval);
    this.IsWait = false;
    this.reqToClear=0;
    this.clearMain.disableTab=false;    
    this.sublevel='';
    this.disableSubLevel=false;
    this.disableReqToClear=false;
  }

}
