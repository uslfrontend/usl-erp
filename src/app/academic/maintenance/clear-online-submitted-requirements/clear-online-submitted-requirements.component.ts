import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';

@Component({
  selector: 'app-clear-online-submitted-requirements',
  templateUrl: './clear-online-submitted-requirements.component.html',
  styleUrls: ['./clear-online-submitted-requirements.component.css']
})
export class ClearOnlineSubmittedRequirementsComponent implements OnInit {
  tabbing;
  disableTab=false;
  tabChanged=false;

  constructor(public global: GlobalService) { }

  ngOnInit() {}

  getindex(tab){
    this.tabbing = tab.index;
  }

}