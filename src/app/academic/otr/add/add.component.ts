import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  EnrolledCourse = []
  Degree
  SelectedDegree
  Course = ''
  Major = ''
  Date = ''
  SONumber = ''
  AwardsReceived = ''
  SerialNo = ''
  Evaluation: number
  Clearance: number
  CourseVersion = ''
  //
  idNumber = ''
  otrType: number

  OTRInfo = []
  Address = ''
  AdditionalRemarks = ''
  courseCollege = ''
  courseDoctoral = ''
  courseMasteral = ''
  coursePB = ''
  courseTV = ''
  FullName = ''
  highSchool = ''
  IDNumber = ''
  image: any = 'assets/noimage.jpg';
  intermediate = ''
  remarks = ''

  //ENROLLED API

  programId = ''
  courseCode = ''
  programTitle = ''
  version = ''
  major = ''
  programLevel = ''
  //OTR DETAILS
  OTRDetails = []
  //
  Courses
  Majors
  currentDate
  PutProgramID
  constructor(private domSanitizer: DomSanitizer, public global: GlobalService, public http: HttpClient, public dialogRef: MatDialogRef<AddComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService) {
    this.EnrolledCourse = data.Course
    this.otrType = data.OTR
    this.Major = data.majorr
    this.idNumber = data.ID
    this.currentDate = new Date();
  }

  DateFunction() {
    if (this.Date > this.currentDate) {
      this.Date = this.currentDate
    }
  }
  ngOnInit() {
    // console.log(this.EnrolledCourse)
    // console.log(this.idNumber)
    // console.log(this.otrType)
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  onDegreeSelectionChange() {
    // console.log('all',this.Degree)
    this.PutProgramID = this.Degree.programId
    // console.log(this.PutProgramID)
    for (var x = 0; x < this.EnrolledCourse.length; x++) {
      //  console.log(this.EnrolledCourse[x].courseCode + ' - ' + this.EnrolledCourse[x].version)
    }

    // console.log(this.Degree.programTitle)
    this.Course = this.Degree.programTitle
    

    if (!this.Major) {
      this.Major = 'Not Applicable';
    }

    if (!this.AwardsReceived) {
      this.AwardsReceived = 'Not Applicable';
    }


    this.CourseVersion = this.EnrolledCourse[0].courseCode + " - " + this.EnrolledCourse[0].version;
  }



  saveadd() {
    // console.log(this.CourseVersion)
    // console.log(this.Course)
    // console.log(this.Major)
    // console.log(this.Date)
    // console.log(this.SONumber)
    // console.log(this.AwardsReceived)
    // console.log(this.SerialNo)
    // console.log(this.Evaluation)
    // console.log(this.Clearance)
    // console.log(this.Date)
    // console.log('BELOW IS PUT')
    // console.log(this.PutProgramID)
    // console.log(this.Date)
    // console.log(this.SONumber)
    // console.log(this.AwardsReceived)
    // console.log(this.SerialNo)
    // console.log(this.otrType)
    // console.log(this.Evaluation)
    // console.log(this.Clearance)
    this.global.swalSuccess('Successfully Addded')
    if (!this.SerialNo) {
      this.SerialNo = 'Not Applicable';
    }
    if (this.Date != '' && this.SONumber != '' && this.Evaluation != null && this.Clearance != null) {
      this.dialogRef.close({
        CourseVersion: this.CourseVersion,
        Course: this.Course,
        Major: this.Major,
        Date: this.Date,
        SONumber: this.SONumber,
        AwardsReceived: this.AwardsReceived,
        SerialNo: this.SerialNo,
        Evaluation: this.Evaluation,
        Clearance: this.Clearance
      });
    } else
      this.global.swalAlert("Please fill in the required fields!", "", 'warning');
  }

  Update() {
    if (this.PutProgramID != '' && this.Date != '' && this.Evaluation != null && this.Clearance != null && this.SONumber != '') {
      this.api.putGraduationRecord(this.idNumber,
        {
          "programid": this.PutProgramID,
          "dateofgraduation": this.Date,
          "sonumber": this.SONumber,
          "awards": this.AwardsReceived,
          "nstpSerialNumber": this.SerialNo,
          "otrType": this.otrType,
          "withEvaluation": this.Evaluation,
          "withClearance": this.Clearance
        }).map(response => response.json()).subscribe(res => {
        }, Error => {
          this.global.swalAlertError(Error);
        });
      Swal.fire(
        'Saved!',
        'Graduation record added.',
        'success'
      )
      this.dialogRef.close({
        Major: this.Major
      });
      // console.log(this.Major)
      // console.log('BELOW IS PUT')
      // console.log(this.PutProgramID)
      // console.log(this.Date)
      // console.log(this.SONumber)
      // console.log(this.AwardsReceived)
      // console.log(this.SerialNo)
      // console.log(this.otrType)
      // console.log(this.Evaluation)
      // console.log(this.Clearance)
    
    }
    else{
      this.global.swalAlert("Please fill in the required fields!", "", 'warning');
    }
    
  
  }
}