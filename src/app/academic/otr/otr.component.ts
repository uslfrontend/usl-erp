import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { GlobalService } from '../../global.service';
import { ApiService } from '../../api.service';
import { ImageService } from "../image.service"
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Router } from "@angular/router";
import { AddComponent } from './add/add.component';
import { PersonLookupComponent } from './../../academic/lookup/person-lookup/person-lookup.component';
import { StudentLookupComponent } from '../../academic/lookup/student-lookup/student-lookup.component'
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTabGroup } from '@angular/material/tabs';
import { ViewChild } from '@angular/core';
import * as pdfMake from "pdfmake/build/pdfmake";
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { group } from 'console';
import { groupBy } from 'rxjs-compat/operator/groupBy';
import { resolve } from 'url';

import Swal from 'sweetalert2'

// import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import pdfFonts from "pdfmake/build/vfs_fonts"
// const pdfFonts = require("pdfmake/build/vfs_fonts");
const pdfFonts = require('pdfmake/build/vfs_fonts');
// import { ActiveConfigurationComponent } from 'src/app/control-panel/active-configuration/active-configuration.component';
// import { ActiveconfigComponent } from './activeconfig/activeconfig.component';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.vfs = pdfFonts.pdfMake.vfs;
//

// pdfMake.defaultFont = 'TimesNewRoman';
@Component({
	selector: 'app-otr',
	templateUrl: './otr.component.html',
	styleUrls: ['./otr.component.scss']
})
export class OtrComponent implements OnInit {

	// currentPage: number = 1; // Declare currentPage as a global variable

	purpose = 'scholarship purposes only.'
	purposeGrad = 'reference purposes only'
	idNumber = ''
	otrType: number = 1

	OTRInfo = []
	Address = ''
	AdditionalRemarks = ''
	courseCollege = ''
	courseDoctoral = ''
	courseMasteral = ''
	coursePB = ''
	courseTV = ''
	FullName = ''
	highSchool = ''
	IDNumber = ''
	image: any = 'assets/noimage.jpg';
	intermediate = ''
	remarks = ''

	//ENROLLED API
	EnrolledCourse = []
	programId = ''
	courseCode = ''
	programTitle = ''
	version = ''
	major = ''
	programLevel = ''
	//OTR DETAILS
	OTRDetails = []
	//
	Degree = ''
	Course = ''
	Major = ''
	Date = ''
	DateGradFormmated = ''
	SONumber = ''
	AwardsReceived = ''
	SerialNo = ''
	Evaluation = ''
	Clearance = ''
	CourseVersion = ''
	AddedRecord = []
	DateGradFormat
	DateGrad
	Latest
	//
	EducBg = []
	Elem
	JHS
	SHS
	COL
	// College = ''
	//
	Baccalaureate = ''
	Masteral = ''
	Doctoral = ''
	College = ''
	PreBaccalaureate = ''
	//
	tabIndex
	//PersonInfo
	PersonInfo = []
	id
	firstName
	middleName
	lastName
	suffixName
	dateOfBirth
	placeOfBirth
	gender
	genderfinal
	civilStatus
	idNo
	personType
	mobileNo
	telNo
	emailAddress
	lrNumber
	nationality
	religion

	otr1s1y = []
	otr2s1y = []
	otr1s2y = []
	otr1s3y = []
	otr2s3y = []
	otr1s4y = []
	otr2s4y = []
	otr1s5y = []
	otr2s5y = []
	otr1s6y = []
	otr2s6y = []

	groupedSy = []

	AdmissionData
	admissionStatus
	admissionCredentials
	FinalCred
	dateOfAdmission
	programEnrolled

	postGradMaster
	postGradMasteral
	postGradMasteralSchool

	postGradDoctoral
	postGradDoctoralSchool
	postGradDoctor

	postGRAD

	// y1
	// y2
	// term
	// c
	// a
	// schoolyearFormatted


	level = 1
	placeofbirth
	convertedImage
	pictureID
	today

	registrarCertificateData = []
	// image:any = 'assets/noimage.jpg';
	// id:any;
	name: any;
	x = 1

	schoolYear = ''
	schoolyearFormatted = ''

	a
	c
	y1
	y2

	proglevel = '';
	activeterm = 0;
	term

	Certificate
	// toggleValue = false;
	// lockfeesSetUp

	GetGradRec
	degreeTitle
	majorr
	dateofGraduation
	specialOrderNumber
	awardsReceived
	nstpSerialNo
	withEvaluation
	withClearance
	ProgramIDD

	dateofGradFormatted

	DegreeLevel
	deglev1
	deglev2
	deglev3
	deglev4
	deglev5

	codeCourse

	OTRDetails2
	SY
	array_sy_item
	firstValue
	lastValue
	groupsyyy
	truecopy

	DegreeLevelT
	DegreeLevelROTC = 1
	Subject
	constructor(private route: ActivatedRoute, private datePipe: DatePipe, public http: HttpClient, public dialog: MatDialog, public global: GlobalService, private api: ApiService, private images: ImageService, private domSanitizer: DomSanitizer, private cookieService: CookieService, private router: Router) {

	}

	// toggleChanged() {

	// 	// console.log(this.toggleValue ? 1 : 0); // Display the value in the console
	// 	this.lockfeesSetUp = this.toggleValue ? 1 : 0
	// 	console.log(this.lockfeesSetUp)
	//   }
	// <mat-select [(ngModel)]="Certificate" style="font-size: 14px;">
	// <mat-option [value]="'Graduation'">Graduation</mat-option>
	// <mat-option [value]="'GradesCompletion'">Completion of Grades</mat-option>
	// <mat-option [value]="'GWA'">General Weighted Average</mat-option>
	// <mat-option [value]="'Spes'">SPES</mat-option>
	// <mat-option [value]="'NSTP'">NSTP</mat-option>
	// <mat-option [value]="'ROTC'">ROTC</mat-option>
	// <mat-option [value]="'WithHonor'">With Honors</mat-option>
	// <mat-option [value]="'Enrollment'">Enrolment/Attendance</mat-option>
	// <mat-option [value]="'GradesCert'">Grades</mat-option>
	// <!-- <mat-option [value]="'1'">Cert 1</mat-option>

	CertFunction() {

		// console.log(this.Certificate)
		if (this.Certificate === 'Graduation') {
			this.generateGraduationPDF();

		} else if (this.Certificate === 'GradesCompletion') {
			this.generateCompletionGrades();

		} else if (this.Certificate === 'GWA') {
			this.generateGWA();

		} else if (this.Certificate === 'Spes') {
			this.generateSPES();

		} else if (this.Certificate === 'NSTP') {
			this.generateNSTP();

		} else if (this.Certificate === 'Registrar') {
			this.generateRegistrar();

		} else if (this.Certificate === 'WithHonor') {
			this.generateWithHonors();

		} else if (this.Certificate === 'Enrollment') {
			this.generateEnrolment();

		} else if (this.Certificate === 'GradesCert') {
			this.generateGrades();

		} else if (this.Certificate === 'ROTC') {
			this.generateROTC();

		}



	}



	ngOnInit() {
		// const FONTS = pdfMake.vfs
		// console.log(FONTS)
		// console.log('PDF FONTS: ',pdfFonts)
		// console.log('FONTS',pdfFonts.pdfMake.vfs)
		// this.proglevel = this.global.domain
		// this.term = this.global.displaysem
		// this.schoolyear = this.global.displayyear


		if (this.idNumber != '') {
			this.idNumber = this.IDNumber
			this.keyDownFunction('onoutfocus')
		}

		this.api.getPublicAPICurrentServerTime()
			.map(response => response.json())
			.subscribe(res => {
				// var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				// var today: any = new Date(res.data);
				// var dd = String(today.getDate()).padStart(2, '0');
				// var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
				// var yyyy = today.getFullYear();
				// this.today = mL[today.getMonth()] + ' ' + dd + ', ' + yyyy;
				var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				var today: any = new Date(res.data);
				// console.log(res.data)
				var dd = String(today.getDate());
				var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
				var monthName = mL[today.getMonth()];
				var daySuffix = "th";

				// Determine the day suffix based on the date
				if (dd === "1" || dd === "21" || dd === "31") {
					daySuffix = "st";
				} else if (dd === "2" || dd === "22") {
					daySuffix = "nd";
				} else if (dd === "3" || dd === "23") {
					daySuffix = "rd";
				}

				this.today = dd + daySuffix + ' day of ' + monthName + ' ' + yyyy;


			}, Error => {
				this.global.swalAlertError(Error)
			});
	}

	// delete(){
	// 	this.Latest = []
	// }
	// openDialog(x = null): void {
	// 	const dialogRef = this.dialog.open(ActiveconfigComponent, {
	// 		width: '700px', disableClose: true, data: { x: x }
	// 	});

	// 	dialogRef.afterClosed().subscribe(result => {
	// 		//console.log(result)
	// 		if (result != undefined) {
	// 			if (result.result == 'save') {
	// 			}
	// 		}
	// 	});
	// }
	AddRecord() {
		const dialogRef = this.dialog.open(AddComponent, {

			data: {
				Course: this.EnrolledCourse,
				OTR: this.otrType,
				Major: this.majorr,
				ID: this.idNumber
			}

		});
		dialogRef.afterClosed().subscribe(result => {
			if (result.result !== 'cancel') {
				if (result.AwardsReceived == null) {
					result.AwardsReceived = 'Not Applicable'
				}
				this.AddedRecord.push(result); // Push the result to the data array
				// console.log('ADDED', this.AddedRecord)
				this.Latest = this.AddedRecord.length - 1

				this.GraduationRefresh()
			} else {
				// console.log("result is cancel")
			}
		});
		// console.log(this.EnrolledCourse)
	}


	tabClick(event: MatTabChangeEvent) {
		this.Clear()
		this.otrType
		this.groupedSy = []
		this.otrType = event.index + 1; // Update value based on selected tab
		// console.log("selected Index: ", this.otrType);

		this.callOtrApy()
		// console.log(this.otrType)
	}


	GraduationRefresh() {
		// this.global.swalLoading('Loading')
		this.GetGradRec = []
		this.degreeTitle = ''
		this.majorr = ''
		this.dateofGraduation = ''
		this.specialOrderNumber = ''
		this.awardsReceived = ''
		this.nstpSerialNo = ''
		this.withEvaluation = ''
		this.withClearance = ''
		this.api.getGraduationRecord(this.idNumber).map(response => response.json()).subscribe(res => {
			// console.log('GetGrad', res.data)

			this.GetGradRec = res.data

			// if (!this.majorr) {
			// 	this.majorr = 'Not Applicable';
			// }



			// console.log(this.GetGradRec)
			for (var x in this.GetGradRec) {

				if (this.otrType == 1 && this.GetGradRec[x].otrType == 1) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance

					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// console.log('Degree: ', this.degreeTitle)
					// console.log('Major: ', this.majorr)
					// console.log('Date: ', this.dateofGraduation)
					// console.log('SO: ', this.specialOrderNumber)
					// console.log('Awards: ', this.awardsReceived)
					// console.log('NSTP: ', this.nstpSerialNo)
					// console.log('Eval: ', this.withEvaluation)
					// console.log('Clearance: ', this.withClearance)
					// console.log('---------------')
				}
				if (this.otrType == 2 && this.GetGradRec[x].otrType == 2) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance

					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// 	console.log('Degree: ', this.degreeTitle)
					// 	console.log('Major: ', this.majorr)
					// 	console.log('Date: ', this.dateofGraduation)
					// 	console.log('SO: ', this.specialOrderNumber)
					// 	console.log('Awards: ', this.awardsReceived)
					// 	console.log('NSTP: ', this.nstpSerialNo)
					// 	console.log('Eval: ', this.withEvaluation)
					// 	console.log('Clearance: ', this.withClearance)
					// 	console.log('---------------')
				}
				if (this.otrType == 3 && this.GetGradRec[x].otrType == 3) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance

					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// console.log('Degree: ', this.degreeTitle)
					// console.log('Major: ', this.majorr)
					// console.log('Date: ', this.dateofGraduation)
					// console.log('SO: ', this.specialOrderNumber)
					// console.log('Awards: ', this.awardsReceived)
					// console.log('NSTP: ', this.nstpSerialNo)
					// console.log('Eval: ', this.withEvaluation)
					// console.log('Clearance: ', this.withClearance)
					// console.log('---------------')

				}

			}

			// this.degreeTitle
			// this.majorr
			// this.dateofGraduation
			// this.specialOrderNumber
			// this.awardsReceived
			// this.nstpSerialNo
			// this.withEvaluation
			// this.withClearance

		});
		// this.global.swalClose()
	}
	callOtrApy() {
		// this.Baccalaureate = ''
		// this.Masteral = ''
		// this.Doctoral = ''
		// this.PreBaccalaureate = ''
		// this.College = ''
		this.GetGradRec = []
		this.degreeTitle = ''
		this.majorr = ''
		this.dateofGraduation = ''
		this.specialOrderNumber = ''
		this.awardsReceived = ''
		this.nstpSerialNo = ''
		this.withEvaluation = ''
		this.withClearance = ''
		this.api.getGraduationRecord(this.idNumber).map(response => response.json()).subscribe(res => {
			// console.log('GetGrad', res.data)

			this.GetGradRec = res.data
			if (!this.majorr) {
				this.majorr = 'Not Applicable';
			}
			// console.log(this.GetGradRec)
			for (var x in this.GetGradRec) {

				if (this.otrType == 1 && this.GetGradRec[x].otrType == 1) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					// console.log('codecourse',this.codeCourse)
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance
					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// console.log('ProgID: ', this.ProgramIDD)
					// console.log('Degree: ', this.degreeTitle)
					// console.log('Major: ', this.majorr)
					// console.log('Date: ', this.dateofGraduation)
					// console.log('SO: ', this.specialOrderNumber)
					// console.log('Awards: ', this.awardsReceived)
					// console.log('NSTP: ', this.nstpSerialNo)
					// console.log('Eval: ', this.withEvaluation)
					// console.log('Clearance: ', this.withClearance)
					// console.log('---------------')
				}
				if (this.otrType == 2 && this.GetGradRec[x].otrType == 2) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance

					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// console.log('ProgID: ', this.ProgramIDD)degreeTitle
					// console.log('Degree: ', this.degreeTitle)
					// console.log('Major: ', this.majorr)
					// console.log('Date: ', this.dateofGraduation)
					// console.log('SO: ', this.specialOrderNumber)
					// console.log('Awards: ', this.awardsReceived)
					// console.log('NSTP: ', this.nstpSerialNo)
					// console.log('Eval: ', this.withEvaluation)
					// console.log('Clearance: ', this.withClearance)
					// console.log('---------------')
				}
				if (this.otrType == 3 && this.GetGradRec[x].otrType == 3) {
					this.ProgramIDD = this.GetGradRec[x].programID
					this.codeCourse = this.GetGradRec[x].courseCode
					this.degreeTitle = this.GetGradRec[x].degreeTitle
					this.majorr = this.GetGradRec[x].major
					this.dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					this.awardsReceived = this.GetGradRec[x].awardsReceived
					this.nstpSerialNo = this.GetGradRec[x].nstpSerialNo
					this.withEvaluation = this.GetGradRec[x].withEvaluation
					this.withClearance = this.GetGradRec[x].withClearance

					if (!this.majorr) {
						this.majorr = 'Not Applicable';
					}

					if (this.withEvaluation == 1) {
						this.withEvaluation = 'Yes'
					}
					if (this.withClearance == 1) {
						this.withClearance = 'Yes'
					}

					if (this.withEvaluation == 0) {
						this.withEvaluation = 'No'
					}
					if (this.withClearance == 0) {
						this.withClearance = 'No'
					}
					// console.log('ProgID: ', this.ProgramIDD)
					// console.log('Degree: ', this.degreeTitle)
					// console.log('Major: ', this.majorr)
					// console.log('Date: ', this.dateofGraduation)
					// console.log('SO: ', this.specialOrderNumber)
					// console.log('Awards: ', this.awardsReceived)
					// console.log('NSTP: ', this.nstpSerialNo)
					// console.log('Eval: ', this.withEvaluation)
					// console.log('Clearance: ', this.withClearance)
					// console.log('---------------')

				}
			}

			// this.degreeTitle
			// this.majorr
			// this.dateofGraduationgroupedSy
			// this.specialOrderNumber
			// this.awardsReceived
			// this.nstpSerialNo
			// this.withEvaluationgroupedSy
			// this.withClearance

		});
		this.api.getOTRDetails(this.idNumber, this.otrType).map(response => response.json()).subscribe(res => {
			if (res.data != '') {
				this.OTRDetails = res.data
				// console.log(this.OTRDetails)

				const groupedBySchoolYear = this.OTRDetails.reduce((acc, item) => {
					if (acc[item.schoolYear]) {
						acc[item.schoolYear].push(item);
					} else {
						acc[item.schoolYear] = [item];
					}
					return acc;
				}, {});

				this.groupedSy = Object.values(groupedBySchoolYear);
				// console.log('Grouped: ', this.groupedSy);
				this.api.getAdmissionCred(this.idNumber, this.otrType).map(response => response.json()).subscribe(res => {
					this.AdmissionData = []
					// console.log('Cleared',this.AdmissionData)
					this.AdmissionData = res.data
					// console.log('New',this.AdmissionData)
					// console.log('Admission: ', this.AdmissionData)

					if (this.AdmissionData != null) {

						this.admissionStatus = this.AdmissionData.admissionStatus
						this.admissionCredentials = this.AdmissionData.admissionCredentials
						this.FinalCred = this.admissionCredentials.replace(/MarriageContract/, 'Marriage Contract')
						// console.log(FinalCred)
						// var Values = this.admissionCredentials.split(",")
						// Values = Values.map(value => value.trim());
						// var FinalAdmissionCred = Values[1]
						// console.log(FinalAdmissionCred)
						this.dateOfAdmission = this.AdmissionData.dateOfAdmission
						this.programEnrolled = this.AdmissionData.programEnrolled
						// console.log('A: ', this.admissionStatus)
						// console.log('B: ', this.admissionCredentials)
						// console.log('C: ', this.dateOfAdmission)
						// console.log('D: ', this.programEnrolled)
					}
					else {
						this.admissionStatus = 'Not Applicable'
						this.admissionCredentials = 'Not Applicable'
						this.dateOfAdmission = 'Not Applicable'
						this.programEnrolled = 'Not Applicable'
						// console.log('A: ', this.admissionStatus)
						// console.log('B: ', this.admissionCredentials)
						// console.log('C: ', this.dateOfAdmission)
						// console.log('D: ', this.programEnrolled)
					}


				});
				this.api.getOTRInfo(this.idNumber).map(response => response.json()).subscribe(res => {
					if (res.data != '') {

						this.OTRInfo = res.data
						// console.log(this.OTRInfo)
						this.remarks = this.OTRInfo[0].remarks
						if (this.otrType == 1) {
							this.Baccalaureate = this.remarks
							this.AdditionalRemarks = ''
							// this.Masteral = ''
							// this.Doctoral = ''
						}
						if (this.otrType == 2) {
							this.Baccalaureate = this.remarks
							this.AdditionalRemarks = this.OTRInfo[0].additionalRemarks
							// this.Doctoral = ''
						}
						if (this.otrType == 3) {
							this.Baccalaureate = this.remarks
							// this.Masteral = this.AdditionalRemarks
							this.AdditionalRemarks = this.OTRInfo[0].additionalRemarks
							// this.Masteral = ''
						}
						if (this.otrType == 4) {
							this.College = 'NO REMARKS'
						}
						if (this.otrType == 5) {
							this.PreBaccalaureate = 'NO REMARKS'
						}
						// console.log(res.data.length)
						// console.log(this.remarks)

						// console.log('REMARKS',this.Baccalaureate)


						// this.College = this.remarks = ''
						// this.PreBaccalaureate = this.remarks = ''
					}
					else {
						// this.remarks = ''
						// this.groupedSy = []
						// console.log('No Value', this.remarks)
					}

				}, Error => {
					this.global.swalAlertError(Error);
				});
			}
			else {
				this.remarks = ''
				// this.groupedSy = []
				// console.log('No Value', this.remarks)
			}

		}, Error => {
			this.global.swalAlertError(Error);
		});

	}
	UpdateRemarks() {

		this.api.putOTRDetails(this.idNumber,
			{
				"remarks": this.Baccalaureate,
				"addRemarks": this.AdditionalRemarks
			}).map(response => response.json()).subscribe(res => {
			}, Error => {
				this.global.swalAlertError(Error);
			});
		Swal.fire(
			'Saved!',
			'Graduation remarks updated.',
			'success'
		)
	}
	delete(value) {
		// console.log(value)

		// for (var x = 0; x < this.GetGradRec.length; x++) {
		// 	if (value == this.GetGradRec[x].programID) {
		// 		console.log(this.GetGradRec[x])
		// 		this.api.deleteGradudationRecord(this.idNumber, this.GetGradRec[x].programID).map(response => response.json()).subscribe(result => {
		// 			console.log('deleted', result)
		// 			this.GraduationRefresh()
		// 		})
		// 	}
		// }

		Swal.fire({
			title: 'You are about to delete a Graduation Record',
			text: "Do you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				// this.AddedRecord = [], // Push the result to the data array
				// 	// this.Latest = this.AddedRecord.length - 1
				// 	this.Latest = [],
				for (var x = 0; x < this.GetGradRec.length; x++) {
					if (value == this.GetGradRec[x].programID) {
						// console.log(this.GetGradRec[x])
						this.api.deleteGradudationRecord(this.idNumber, this.GetGradRec[x].programID).map(response => response.json()).subscribe(result => {
							// console.log('deleted', result)
							this.GraduationRefresh()
						})
					}
				}

				// If the user confirms the action to delete the file, execute the delete action and display a success message
				Swal.fire(
					'Success!',
					'Graduation record has been deleted.',
					'success'
				)
			} else {
				// If the user cancels the action, do nothing
				// The dialog box will simply close
			}
		});
	}
	studentlookup(): void {
		const dialogRef = this.dialog.open(StudentLookupComponent, {
			width: '600px', disableClose: true
		});
		dialogRef.afterClosed().subscribe(result => {
			if (result.result != 'cancel') {
				this.idNumber = result.result;
				this.keyDownFunction('onoutfocus')

			}
		});
	}

	Update() {

		this.api.putOTRDetails(this.idNumber,
			{
				"remarks": this.Baccalaureate,
				"addRemarks": this.AdditionalRemarks
			}).map(response => response.json()).subscribe(res => {
			}, Error => {
				this.global.swalAlertError(Error);
			});
		Swal.fire(
			'Saved!',
			'Graduation remarks updated.',
			'success'
		)
	}

	Clear() {
		this.AddedRecord = []
		// this.Baccalaureate = ''
		// this.Masteral = ''
		// this.Doctoral = ''
		// this.College = ''
		// this.PreBaccalaureate = ''

	}


	AddingRecordValidation() {

		this.Elem
		this.JHS
		this.SHS
		this.postGradMasteral
		this.postGradMasteralSchool
		// console.log(this.AddedRecord)
		// var Date = this.AddRecord[0].Date
		if (this.AddedRecord.length === 0) {
			// this.global.swalAlert('NOTE','Please Add Graduation Record','warning')
			// var AwardsReceived = "N/A";
			// var Clearance = "N/A";
			// var Course = "N/A";
			// var CourseVersion = "N/A";
			// // var formattedDateGrad = "N/A";
			// var Evaluation = "N/A";
			// var Major = "N/A";
			// var SONumber = "N/A";
			// var SerialNo = "N/A";
			this.Latest = 0
			this.AddedRecord.push({
				AwardsReceived: 'Not Applicable', Clearance: 'Not Applicable',
				Course: 'Not Applicable', CourseVersion: 'Not Applicable', Date: 'Not Applicable', Evaluation: 'Not Applicable', Major: 'Not Applicable', SONumber: 'Not Applicable', SerialNo: 'Not Applicable'
			})
			// console.log(this.AddedRecord)
			this.generateTOR()
			this.Clear()

		}
		else {
			this.generateTOR()
		}
	}
	syDisplay(x) {
		this.y1 = x.substring(0, 4)
		this.y2 = parseInt(this.y1) + 1
		this.a = this.y1.toString() + " - " + this.y2.toString();
		this.term = x.substring(6, 7)
		// console.log('TERM: ', this.term)
		this.c
		if (this.term == '1')
			this.c = "1st Semester"
		else if (this.term == '2')
			this.c = "2nd Semester"
		else if (this.term == '3')
			this.c = "Summer"
		else
			this.c = ""
		return this.c + " SY " + this.a

	}

	year1() {
		var term = parseInt(this.schoolYear) % 10;
		this.y2 = this.y1 + 1;
		var sy = 20 + '' + this.y1 % 100 + '' + this.y2 % 100;
		this.schoolYear = sy + '' + term;

		// Define a recursive function to check for data and handle loading
		const checkForData = () => {
			// Assuming this.global.swalLoading('Loading Information') shows the loading message
			this.global.swalLoading('Loading Information');

			this.api
				.getRegistrarCertificationInfo(this.idNumber, this.schoolYear)
				.map(response => response.json())
				.subscribe(
					res => {
						// If res.data has a result, store the data and close the loading message
						this.registrarCertificateData = res.data;
						setTimeout(() => {
							this.global.swalClose(); // Close swal after a timeout
						}, 2500); // Adjust the timeout duration as needed
					},
					error => {
						// If there was an error, close the loading message and handle it as needed
						setTimeout(() => {
							this.global.swalClose(); // Close swal after a timeout
							// For example, you can display an error message using a toast or alert
							console.error('Error fetching data:', error);
						}, 2500); // Adjust the timeout duration as needed
					}
				);
		};

		// Call the recursive function immediately
		checkForData();

	}



	year2() {

		var term = parseInt(this.schoolYear) % 10;
		this.y1 = this.y2 - 1;
		var sy = 20 + '' + this.y1 % 100 + '' + this.y2 % 100
		this.schoolYear = sy + '' + term
		// console.log(this.schoolYear)



		// Define a recursive function to check for data and handle loading
		const checkForData = () => {
			// Assuming this.global.swalLoading('Loading Information') shows the loading message
			this.global.swalLoading('Loading Information');

			this.api
				.getRegistrarCertificationInfo(this.idNumber, this.schoolYear)
				.map(response => response.json())
				.subscribe(
					res => {
						// If res.data has a result, store the data and close the loading message
						this.registrarCertificateData = res.data;
						this.global.swalClose();
					},
					error => {
						// If there was an error, close the loading message and handle it as needed
						this.global.swalClose();

						// For example, you can display an error message using a toast or alert
						console.error('Error fetching data:', error);
					}
				);
		};

		// Call the recursive function immediately
		checkForData();

	}

	Term(term) {
		// Convert the schoolYear to a string
		var yearString = this.schoolYear.toString();

		// Get the last character of the string (last digit of schoolYear)
		var lastDigit = yearString.charAt(yearString.length - 1);

		// Replace the last digit with the new term
		var newYearString = yearString.slice(0, yearString.length - 1) + term;

		// Parse the new year string back to a number
		this.schoolYear = newYearString;
		// console.log(this.schoolYear)


		// Define a recursive function to check for data and handle loading
		const checkForData = () => {
			// Assuming this.global.swalLoading('Loading Information') shows the loading message
			this.global.swalLoading('Loading Information');

			this.api
				.getRegistrarCertificationInfo(this.idNumber, this.schoolYear)
				.map(response => response.json())
				.subscribe(
					res => {
						// If res.data has a result, store the data and close the loading message
						this.registrarCertificateData = res.data;
						this.global.swalClose();
					},
					error => {
						// If there was an error, close the loading message and handle it as needed
						this.global.swalClose();

						// For example, you can display an error message using a toast or alert
						console.error('Error fetching data:', error);
					}
				);
		};

		// Call the recursive function immediately
		checkForData();
	}

	yearchange1() {


		var term = parseInt(this.schoolYear) % 10;
		this.y2 = this.y1 + 1;
		var sy = 20 + '' + this.y1 % 100 + '' + this.y2 % 100
		this.schoolYear = sy + '' + term
		// console.log(this.schoolYear)


		// Assuming this.global.swalLoading('Loading Information') shows the loading message
		this.global.swalLoading('Loading Information');

		// Set the delay time in milliseconds (e.g., 3000ms for 3 seconds)
		const delayTime = 1000;

		// Call the UpdateFunction() after the delay
		setTimeout(() => {
			this.UpdateFunction();

			// Assuming this.global.swalClose() closes the loading message
			this.global.swalClose();
		}, delayTime);

	}


	yearchange2() {

		var term = parseInt(this.schoolYear) % 10;
		this.y1 = this.y2 - 1;
		var sy = 20 + '' + this.y1 % 100 + '' + this.y2 % 100
		this.schoolYear = sy + '' + term
		// console.log(this.schoolYear)



		// Assuming this.global.swalLoading('Loading Information') shows the loading message
		this.global.swalLoading('Loading Information');

		// Set the delay time in milliseconds (e.g., 3000ms for 3 seconds)
		const delayTime = 1000;

		// Call the UpdateFunction() after the delay
		setTimeout(() => {
			this.UpdateFunction();

			// Assuming this.global.swalClose() closes the loading message
			this.global.swalClose();
		}, delayTime);

	}

	setTerm(term) {
		// Convert the schoolYear to a string
		var yearString = this.schoolYear.toString();

		// Get the last character of the string (last digit of schoolYear)
		var lastDigit = yearString.charAt(yearString.length - 1);

		// Replace the last digit with the new term
		var newYearString = yearString.slice(0, yearString.length - 1) + term;

		// Parse the new year string back to a number
		this.schoolYear = newYearString;
		// console.log(this.schoolYear)

		// Assuming this.global.swalLoading('Loading Information') shows the loading message
		this.global.swalLoading('Loading Information');

		// Set the delay time in milliseconds (e.g., 3000ms for 3 seconds)
		const delayTime = 1000;

		// Call the UpdateFunction() after the delay
		setTimeout(() => {
			this.UpdateFunction();

			// Assuming this.global.swalClose() closes the loading message
			this.global.swalClose();
		}, delayTime);
	}


	// yearchange1() {
	// 	this.y2 = this.y1 + 1;
	// }
	// yearchange2() {
	// 	this.y1 = this.y2 - 1;
	// }

	// setschoolyear() {
	// 	let errorMsg = '';

	// 	// Check if the entered year is valid
	// 	if (this.y1 < 1900 || this.y2 > 2999) {
	// 		errorMsg += '*Must Enter Appropriate School Year\n';
	// 	}

	// 	// Check if a program level is selected
	// 	if (!this.proglevel) {
	// 		errorMsg += '*Program level is required\n';
	// 	}

	// 	// Check if the active term is required
	// 	if (this.proglevel === 'COLLEGE' || this.proglevel === 'GRADUATE SCHOOL') {
	// 		if (!this.term) {
	// 			errorMsg += '*Active Term is required\n';
	// 		}
	// 	} else {
	// 		this.term = '1'; // Set default term for elementary and high school
	// 	}

	// 	// Check if the selected school year is valid
	// 	let validSchoolYear = false;
	// 	const schoolYearWithTerm = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
	// 	// console.log('SchoolYearWithTerm',schoolYearWithTerm)
	// 	for (let i = 0; i < this.global.allsyoptions.length; i++) {
	// 		if (schoolYearWithTerm === this.global.allsyoptions[i].syWithSem) {
	// 			validSchoolYear = true;

	// 		}
	// 	}

	// 	if (!validSchoolYear) {
	// 		errorMsg += '*Invalid school year setting!\n';

	// 	}

	// 	if (errorMsg) {
	// 		if (this.global.allsyoptions.length === 0) {
	// 			this.global.swalAlert('No School Year Found!', 'Please contact your system administrator!', 'warning');
	// 		} else {
	// 			this.global.swalAlert(errorMsg, '', 'warning');
	// 		}
	// 		return;
	// 	}

	// 	// Update the school year variable
	// 	if (this.term === '3') {
	// 		// Summer term
	// 		this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + '3';
	// 	} else {
	// 		// Regular semester
	// 		this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
	// 	}

	// 	// Save the program level and school year to cookies
	// 	// this.cookieService.set('domain', this.proglevel);
	// 	// this.cookieService.set('year', this.schoolYear);
	// 	this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
	// 	console.log('Save', this.schoolYear)

	// 	// Display the updated school year and term
	// 	// this.syDisplay(this.schoolYear);
	// 	// this.schoolYear = this.syDisplay(this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString());

	// 	this.UpdateFunction()

	// }

	UpdateFunction() {

		this.api.getRegistrarCertificationInfo(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(res => {
			// console.log(res.data)

			this.registrarCertificateData = res.data
			// console.log(this.registrarCertificateData)
		})

	}

	keyDownFunction(event) {
		this.Clear()

		this.schoolYear = this.global.syear
		var schoolYearFormatted = this.syDisplay(this.schoolYear)
		this.proglevel = this.global.domain

		// console.log('Formatted',schoolYearFormatted)
		// console.log(this.c)
		// console.log(this.a)
		// console.log(this.y1)
		// console.log(this.y2)

		// console.log(this.global.domain)
		// console.log(this.global.displaysem)
		// console.log(this.global.displayyear)
		// console.log(this.global.syear)

		this.EducBg = []

		this.Elem = ''
		this.JHS = ''
		this.SHS = ''
		this.COL = ''
		this.postGRAD = ''

		this.admissionStatus = ''
		this.FinalCred = ''

		this.dateOfAdmission = ''
		this.programEnrolled = ''

		this.gender = ''
		this.dateOfBirth = ''
		this.placeofbirth = ''
		this.nationality = ''
		this.civilStatus = ''
		this.religion = ''
		// this.postGradMasteral = ''
		// this.postGradMasteralSchool = ''
		// this.postGradDoctor = ''
		// this.postGradMaster = ''

		if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
			if (this.idNumber != '') {

				// clear variables

				this.global.swalLoading('Loading Person Information')

				this.api.getRegistrarCertificationInfo(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(res => {
					// console.log(res.data)

					this.registrarCertificateData = res.data
					// console.log(this.registrarCertificateData)
				})

				this.api.getStudentInfo(this.idNumber, this.schoolYear, this.level).map(response => response.json()).subscribe(res => {
					this.placeofbirth = res.data.placeOfBirth
					// console.log(this.placeofbirth)

				});


				this.api.getPersonInformation(this.idNumber).map(response => response.json()).subscribe(res => {
					if (res.data != null) {
						this.PersonInfo = res.data
						this.id = res.data.idNumber
						this.firstName = res.data.firstName
						this.middleName = res.data.middleName
						this.lastName = res.data.lastName + ','
						this.suffixName = res.data.suffixName
						this.dateOfBirth = res.data.dateOfBirth
						this.placeOfBirth = res.data.placeOfBirth
						this.gender = res.data.gender
						if (this.gender == 'M') {
							this.gender = 'Male'
						} else {
							this.gender = 'Female'
						}
						this.civilStatus = res.data.civilStatus
						if (this.civilStatus == 'S') {
							this.civilStatus = 'Single'
						}
						if (this.civilStatus == 'M') {
							this.civilStatus = 'Married'
						}
						if (this.civilStatus == 'W') {
							this.civilStatus = 'Widowed'
						}

						this.idNo = res.data.idNo
						this.personType = res.data.personType
						this.mobileNo = res.data.mobileNo
						this.telNo = res.data.telNo
						this.emailAddress = res.data.emailAddress
						this.lrNumber = res.data.lrNumber
						this.nationality = res.data.nationality
						this.religion = res.data.religion
						// console.log(this.religion)

						// console.log('Person Info', res.data.idNumber)



						this.api.getEducBg(this.idNumber).map(response => response.json()).subscribe(res => {
							if (res.data != null) {
								this.EducBg = res.data
								// console.log('Educ', this.EducBg)

								for (var x = 0; x < this.EducBg.length; x++) {
									//Elem
									if (this.EducBg[x].programName == "Primary Education") {
										this.Elem = this.EducBg[x].schoolName
										// console.log('ELEM: ', this.Elem)
									}
									// if (this.Elem == '') {
									// 	this.Elem = 'Not Applicable'
									// 	// console.log(this.Elem)
									// }


									//JHS
									if (this.EducBg[x].programName == "Secondary Education (Enhanced Academic Curriculum)") {
										this.JHS = this.EducBg[x].schoolName
										// console.log('JHS: ', this.JHS)
									}
									// if (this.JHS == '') {
									// 	this.JHS = 'Not Applicable'
									// }


									//SHS
									if (this.EducBg[x].programName == "Senior High School") {
										this.SHS = this.EducBg[x].schoolName
										// console.log('SHS: ', this.SHS)
									}
									// if (this.SHS == '') {
									// 	this.SHS = 'Not Applicable'
									// 	// console.log(this.SHS)
									// }


									//COLLEGE
									if (this.EducBg[x].programLevel == 50) {
										this.COL = this.EducBg[x].schoolName
										// console.log('COLLEGE', this.COL)
									}

									// if (this.COL == '') {
									// 	this.COL = 'Not Applicable'
									// 	// console.log('COLLEGE ', this.COL)
									// }


									//MASTERAL
									if (this.EducBg[x].programLevel == 80) {
										this.postGradMaster = this.EducBg[x].programLevel
										this.postGradMasteral = this.EducBg[x].programName
										this.postGradMasteralSchool = this.EducBg[x].schoolName
										// console.log('SHS: ', this.SHS)
									}
									// if (this.postGradMasteral == '') {
									// 	this.postGradMasteral = 'Not Applicable'
									// }


									//DOCTORAL
									if (this.EducBg[x].programLevel == 90) {
										this.postGradDoctor = this.EducBg[x].programLevel
										this.postGradDoctoral = this.EducBg[x].programName
										this.postGradDoctoralSchool = this.EducBg[x].schoolName
										// console.log('SHS: ', this.SHS)
									}
									if (this.postGradDoctoral == '') {
										this.postGradDoctoral = 'Not Applicable'
									}

									if (this.postGradDoctor == '' && this.EducBg[x].programLevel == 80) {
										this.postGRAD = this.EducBg[x].schoolName
									}

									if (this.postGradDoctor > this.postGradMaster && this.EducBg[x].programLevel == 90) {
										this.postGRAD = this.EducBg[x].schoolName
									}

									if (this.postGRAD == '') {
										this.postGRAD = 'Not Applicable'
									}
									//---------


								}
								// console.log('POST GRAD: ', this.postGRAD)
								// // this.College = this.EducBg[0]
								// console.log (this.Elem)
								// console.log (this.JHS)
								// console.log (this.SHS)
							} else {
								this.global.swalAlert('NOTE', 'NO RECORD FOUND', 'warning',)
							}
						},
							Error => {
								this.global.swalAlertError(Error);
							});
					} else {
						this.global.swalAlert('NOTE', 'NO RECORD FOUND', 'warning',)
					}
				},
					Error => {
						this.global.swalAlertError(Error);
					});

				this.api.getOTRInfo(this.idNumber).map(response => response.json()).subscribe(res => {
					if (res.data != '') {
						this.OTRInfo = res.data

						this.FullName = this.OTRInfo[0].fullName
						this.Address = this.OTRInfo[0].address
						// console.log('Addreees', this.OTRInfo[0].address)
						// if(this.OTRInfo[0].address = ''){
						// 	this.OTRInfo[0].address = 'Not Applicable'
						// }
						this.AdditionalRemarks = this.OTRInfo[0].additionalRemarks
						this.courseCollege = this.OTRInfo[0].courseCollege
						this.courseMasteral = this.OTRInfo[0].courseMasteral
						this.courseDoctoral = this.OTRInfo[0].courseDoctoral
						if (!this.courseCollege) {
							this.courseCollege = 'Not Applicable'
						}
						if (!this.courseMasteral) {
							this.courseMasteral = 'Not Applicable'
						}
						if (!this.courseDoctoral) {
							this.courseDoctoral = 'Not Applicable'
						}
						this.coursePB = this.OTRInfo[0].coursePB
						this.courseTV = this.OTRInfo[0].courseTV
						this.coursePB = this.OTRInfo[0].coursePB
						this.highSchool = this.OTRInfo[0].highSchool
						this.coursePB = this.OTRInfo[0].coursePB
						this.IDNumber = this.OTRInfo[0].idNumber
						this.intermediate = this.OTRInfo[0].intermediate
						this.remarks = this.OTRInfo[0].remarks
						this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + this.OTRInfo[0].idPicture);
						this.pictureID = this.OTRInfo[0].idPicture
						// console.log(this.OTRInfo[0].idPicture)
						this.remarks = this.OTRInfo[0].remarks
						// console.log(res.data.length)
						// console.log(this.remarks)
						this.Baccalaureate = this.remarks
						// console.log('Length', this.Baccalaureate.length)
						this.Masteral = this.AdditionalRemarks
						this.Doctoral = this.AdditionalRemarks
						this.College = this.remarks = ''
						this.PreBaccalaureate = this.remarks = ''

						this.api.getOTREnrolledCourse(this.idNumber).map(response => response.json()).subscribe(res => {
							if (res.data != '') {

								this.EnrolledCourse = res.data
								// console.log('Enrolled', this.EnrolledCourse)
								// this.programId = this.EnrolledCourse[0].programId
								// this.courseCode = this.EnrolledCourse[0].courseCode
								// this.programTitle = this.EnrolledCourse[0].programTitle
								// this.version = this.EnrolledCourse[0].version
								// this.major = this.EnrolledCourse[0].major
								// this.programLevel = this.EnrolledCourse[0].programLevel

								for (var x = 0; x < this.EnrolledCourse.length; x++) {
									this.programId = this.EnrolledCourse[x].programId
									this.courseCode = this.EnrolledCourse[x].courseCode
									this.programTitle = this.EnrolledCourse[x].programTitle
									this.version = this.EnrolledCourse[x].version
									this.major = this.EnrolledCourse[x].major
									this.programLevel = this.EnrolledCourse[x].programLevel
									// console.log(this.programId)
									// console.log(this.courseCode)
									// console.log(this.programTitle)
									// console.log(this.version)
									// console.log(this.major)
									// console.log(this.programLevel)
									// console.log('--------------------')

								}

								this.callOtrApy()

								this.global.swalClose()
							}
							else {
								this.global.swalAlert(res.message, 'No OTR Information associated with this ID Number!', 'warning')
							}
						}, Error => {
							this.global.swalAlertError(Error);
						});
					}
					else {
						this.remarks = ''
						// console.log('No Value', this.remarks)
						// this.global.swalAlert(res.message, 'No OTR Information associated with this ID Number', 'warning')
					}
				}, Error => {
					this.global.swalAlertError(Error);
				});
			} else {
				this.global.swalAlert('NOTE', 'Please Input ID Number', 'warning')
			}
		}

	}
	// convertBackgroundToWhite(pictureID): void {
	// 	const img = new Image();
	// 	img.onload = () => {
	// 		const canvas = document.createElement('canvas');
	// 		canvas.width = img.width;
	// 		canvas.height = img.height;

	// 		const ctx = canvas.getContext('2d');
	// 		ctx.fillStyle = '#ffffff'; // Set white as the background color
	// 		ctx.fillRect(0, 0, canvas.width, canvas.height);
	// 		ctx.drawImage(img, 0, 0);

	// 		this.convertedImage = canvas.toDataURL(); // The converted image in base64 format
	// 	};

	// 	img.src = 'data:image/png;base64,' + pictureID;
	// }

	generateTOR() {
		// this.Elem = ''
		// this.Address = ' ';
		// this.JHS = ' ';
		// this.SHS = ' ';
		// this.COL = ' ';
		// this.postGRAD = ' ';

		// this.degreeTitle = ' ';
		// this.majorr = ' ';
		// this.DateGrad = ' ';
		// this.specialOrderNumber = ' ';
		// this.awardsReceived = ' ';
		// this.nstpSerialNo = ' ';

		// this.OTRInfo[0].idNumber = ' ';
		// this.gender = ' ';
		// this.dateOfBirth = ' ';
		// this.placeofbirth = ' ';
		// this.nationality = ' ';
		// this.civilStatus = ' ';
		// this.religion = ' ';
		// this.admissionStatus = ' ';
		// this.FinalCred = ' ';
		// this.dateOfAdmission = ' ';
		// this.programEnrolled = ' ';

		var array = this.groupedSy
		// console.log(array)

		// #region noImage
		var NoImage = this.images.NoImage
		var header = 'header'
		var content = 'content'
		var footer = 'footer'
		var pageCtr = 1;
		var topmargin = 150;
		var headerDisplay = [];
		var footerDisplay = [];
		var LastName = this.lastName.toUpperCase();
		var FirstName = this.firstName.toUpperCase() + ' Y ';
		var MiddleName = this.middleName.toUpperCase();

		var Completename = this.FullName
		var Address = this.OTRInfo[0].address
		if (!Address) {
			Address = 'Not Applicable'
		}
		// console.log('Gen tor add', Address)
		var tempImage = this.OTRInfo[0].idPicture
		var IDPicture = tempImage
		// console.log(tempImage)

		if (!tempImage)
			IDPicture = NoImage;

		var IDNumber = this.OTRInfo[0].idNumber
		// console.log(IDPicture)
		// if(IDPicture == null || IDPicture == '' || IDPicture == undefined){
		// 	IDPicture = 'assets/noimage.jpg'
		// }
		// console.log("ID NUMBER:",IDNumber)

		// Address = ''
		// Elem = ''
		// HS = ''
		// SHS = ''
		// College = ''
		// PostGrad = ''

		// Degree = ''
		// Major = ''
		// this.DateGrad = ''
		// SONumber = ''
		// AwardsReceived = ''
		// SerialNo = ''

		var Elem = this.Elem
		// console.log('Generate', Elem)
		if (this.Elem == null || this.Elem == '') {
			this.Elem = "Not Applicable"
		}

		// var HS = this.JHS
		if (this.JHS == null || this.JHS == '') {
			this.JHS = "Not Applicable"
		}

		// var SHS = this.SHS
		if (this.SHS == null || this.SHS == '') {
			this.SHS = "Not Applicable"
		}

		// var College = this.COL
		if (this.COL == null || this.COL == '') {
			this.COL = "Not Applicable"
		}

		// var PostGrad = this.postGRAD
		if (this.postGRAD == null || this.postGRAD == '') {
			this.postGRAD = "Not Applicable"
		}

		// this.admissionStatus
		// this.FinalCred

		// this.dateOfAdmission
		// this.programEnrolled


		if (this.admissionStatus == '') {
			this.admissionStatus = 'Not Applicable'
		}

		if (this.FinalCred == '') {
			this.FinalCred = 'Not Applicable'
		}

		if (this.dateOfAdmission == '') {
			this.dateOfAdmission = 'Not Applicable'
		}

		if (this.programEnrolled == '') {
			this.programEnrolled = 'Not Applicable'
		}




		// console.log(Elem)
		// console.log(HS)
		// console.log(SHS)
		// console.log(College)
		// console.log(PostGrad)

		// this.gender = ''
		// this.dateOfBirth = ''
		// this.placeofbirth = ''
		// this.nationality = ''
		// this.civilStatus = ''
		// this.religion = ''

		if (this.gender == '' || this.gender == null) {
			this.gender = 'Not Applicable'
		}

		// if(this.dateOfBirth == ''){
		// 	this.dateOfBirth = 'Not Applicable'
		// }

		// if(this.placeofbirth == ''){
		// 	this.placeofbirth = 'Not Applicable'
		// }

		if (this.nationality == '' || this.nationality == null) {
			this.nationality = 'Not Applicable'
		}

		if (this.civilStatus == '' || this.civilStatus == null) {
			this.civilStatus = 'Not Applicable'
		}

		if (this.religion == '' || this.religion == null) {
			this.religion = 'Not Applicable'
		}


		var DateOfBirth = this.datePipe.transform(this.dateOfBirth, 'MMM d, y');
		if (DateOfBirth == null) {
			DateOfBirth = 'Not Applicable'
		}

		// this.placeofbirth = ''
		// console.log('PoB', this.placeofbirth)
		var formattedPlaceOfBirth

		if (this.placeofbirth == '' || this.placeofbirth == null) {
			this.placeofbirth = 'Not Applicable'
		}

		var placeOfBirth = this.placeofbirth
		formattedPlaceOfBirth = placeOfBirth.toLowerCase().replace(/^(.)|\s(.)/g, ($1) => $1.toUpperCase());
		// if(formattedPlaceOfBirth == null){
		// 	formattedPlaceOfBirth = 'Not Applicable'
		// }
		// console.log(formattedPlaceOfBirth)
		// console.log('place of birth',placeOfBirth)
		var Degree = this.degreeTitle
		if (!Degree) {
			Degree = 'Not Applicable'
		}
		var Major = this.majorr
		if (!Major) {
			Major = 'Not Applicable'
		}
		var Clearance = this.AddedRecord[this.Latest].Clearance
		var that = this;
		// if (this.AddedRecord[this.Latest].Date === 'Not Applicable') {
		// 	this.DateGrad = 'Not Applicable'
		// } else {


		// }
		this.DateGrad = this.datePipe.transform(this.dateofGraduation, 'MMM d, y');
		if (!this.DateGrad) {
			this.DateGrad = 'Not Applicable'
		}
		var SONumber = this.specialOrderNumber
		if (!SONumber) {
			SONumber = 'Not Applicable'
		}
		var AwardsReceived = this.awardsReceived
		if (!AwardsReceived) {
			AwardsReceived = 'Not Applicable'
		}
		var SerialNo = this.nstpSerialNo
		if (!SerialNo) {
			SerialNo = 'Not Applicable'
		}
		// console.log(this.Baccalaureate.length)
		// if (this.Baccalaureate.length > 114) {
		// 	this.Baccalaureate = this.Baccalaureate.replace(/(.{97})/g, "$1\n");
		// }
		// var remarksLength = this.Baccalaureate.replace(/\n/g, '').length;
		// if (remarksLength > 97) {
		// 	var dividedText = this.Baccalaureate.match(/.{1,97}/g);
		// 	this.Baccalaureate = dividedText.join('\n');
		// }

		// console.log('length', remarksLength)
		// if(this.otrType == 2)
		// 	remarksLength+= this.AdditionalRemarks.length

		// console.log('RemarksLength: ', remarksLength)
		var ProgEnrolled = this.programEnrolled
		var WaterMark = this.images.WaterMark
		var imageData = this.images.imageData
		//  #endregion

		// var watermark = {
		// 	image: WaterMark,
		// 	width: 200,
		// 	height: 200,
		// 	opacity: 0.2,
		// 	angle: 45
		// };
		pageCtr = 1;
		var topmargin = 150;
		var headerDisplay = [];
		var footerDisplay = [];
		function validateHeaderDisplay(curPage, pCount) {


			headerDisplay = [];
			pageCtr = curPage;
			if (curPage == 1) {
				return headerDisplay;
			}
			else {
				headerDisplay.push({
					table: {
						widths: [100, 325, 100],
						body: [
							[
								{ image: "data:image/png;base64," + imageData, width: 100, margin: [25, 0, 0, 0] },
								// { image: "data:image/png;base64," + imageData, width: 72.16, alignment: 'right' },
								{
									stack: [
										{ text: 'UNIVERSITY OF SAINT LOUIS', bold: true, fontSize: 13.5, },
										{ text: 'Formerly Saint Louis College of Tuguegarao', italics: true, fontSize: 12 },
										{ text: 'Tuguegarao City, Philippines 3500', italics: true, margin: [0, 0, 0, 7], fontSize: 10.5 },
										{ text: '\nGranted FULL AUTONOMY', bold: true, fontSize: 11 },
										{ text: 'by the Commission on Higher Education\n\n', fontSize: 10.5 },
										{ text: 'OFFICIAL TRANSCRIPT OF RECORDS\n', bold: true, fontSize: 12, margin: [0, 7, 0, 0] },
										{
											canvas: [
												{
													type: 'rect',
													x: 0,
													y: -58,
													w: 230,
													h: 36,
													lineWidth: 0.75,
												}
											]
										},

									], alignment: 'center'
								},
								// { image: "data:image/png;base64," + IDPicture, width: 72.16, alignment: 'right' },
								// { image: "data:image/png;base64," + IDPicture, width: 80, margin: [0, 0, 0, 0] },

							],
							[
								{
									stack: [
										{ text: '\n' },
										{
											table: {
												widths: [435, 100],
												body:
													[
														[
															{
																table: {
																	widths: ['*'],
																	body:
																		[
																			[
																				{
																					table: {
																						widths: [70, '*'],
																						body:
																							[
																								[
																									{ text: 'NAME:', fontSize: 11 }, { text: LastName + ' ' + FirstName + '' + MiddleName, fontSize: 11 }
																								],
																								[
																									{ text: 'Degree/Title:' }, { text: Degree }
																								],
																								[
																									{ text: 'Major:' }, { text: Major }
																								]
																							]
																					},
																					bold: true,
																					fontSize: 9,
																					alignment: 'left',
																					layout: {
																						hLineWidth: function (i, node) { return 0; },
																						vLineWidth: function (i, node) { return 0; },
																						paddingRight: function (i, node) { return 0; },
																						paddingLeft: function (i, node) { return 0; },
																						paddingTop: function (i, node) { return 1; },
																						paddingBottom: function (i, node) { return 1; },
																					}
																				}
																			]
																		]
																},
																layout: {
																	hLineWidth: function (i, node) { return 0.5; },
																	vLineWidth: function (i, node) { return 0.5; }
																}
															},
															{
																table: {
																	widths: ['*'],
																	body:
																		[
																			[
																				{
																					table: {
																						widths: [100],
																						body:
																							[
																								[
																									{
																										stack: [
																											{ text: 'ID Number: ' + IDNumber + '\n\n\n', margin: [0, 0, 0, 2] },
																											{ text: ' ', fontSize: 4 },
																										]
																									}

																								]
																							]
																					},
																					bold: true,
																					fontSize: 9,
																					alignment: 'left',
																					layout: {
																						hLineWidth: function (i, node) { return 0; },
																						vLineWidth: function (i, node) { return 0; },
																						paddingRight: function (i, node) { return 0; },
																						paddingLeft: function (i, node) { return 0; },
																						paddingTop: function (i, node) { return 1; },
																						paddingBottom: function (i, node) { return 1; },
																					}
																				}
																			]
																		]
																},
																layout: {
																	hLineWidth: function (i, node) { return 0.5; },
																	vLineWidth: function (i, node) { return 0.5; }
																}
															}
														]
													]
											}, layout: 'noBorders', margin: [0, 0, 0, 0]
										}
									],
									colSpan: 3
								}, {}, {}
							]
						]
					}, margin: [30, 30, 30, 600],
					layout: 'noBorders'
				});
				return headerDisplay;
			}

		}
		// const pdfMake = require('pdfmake');
		// const printer = new pdfMake();
		// const pdfDoc = printer.createPdfKitDocument(dd);
		// function getCurrentPage() {
		// 	// Get the current page number
		// 	return pdfDoc._pdfMakePagesTracker.pages.length;
		//   }
		//   const currentPage = getCurrentPage();
		//   console.log('Current page:', currentPage);
		// function fillsemestertable(){
		// 	var semTableArray = [];

		// 	for(var x = 0; x<=9;x++){
		// 		semTableArray.push({
		// 			'COURSENO':'Engl '+x.toString(),
		// 			'DESC':'Communication Arts and Skills'+x.toString(),
		// 			'GRADES':'89',
		// 			'UNITS':'3',
		// 			'SY':'First Semester 2010-2011'
		// 		});
		// 	}
		// 	for(var y = 0; y<=8;y++){
		// 		semTableArray.push({
		// 			'COURSENO':'English '+(y+10).toString(),
		// 			'DESC':'Communication Arts and Skills '+(y+10).toString(),
		// 			'GRADES':'89',
		// 			'UNITS':'3',
		// 			'SY':'Second Semester 2010-2011'
		// 		});
		// 	}
		// 	for(var z = 0; z<=5;z++){
		// 		semTableArray.push({
		// 			'COURSENO':'Engl 1',
		// 			'DESC':'Communication Arts and Skills 1',
		// 			'GRADES':'89',
		// 			'UNITS':'3',
		// 			'SY':'First Semester 2011-2012'
		// 		});
		// 	}
		// 	for(var v = 0; v<=5;v++){
		// 		semTableArray.push({
		// 			'COURSENO':'Engl 1',
		// 			'DESC':'Communication Arts and Skills 1',
		// 			'GRADES':'89',
		// 			'UNITS':'3',
		// 			'SY':'Second Semester 2011-2012'
		// 		});
		// 	}
		// 	//return buildTableBody(semTableArray,['COURSE NO','DESCRIPTIVE TITLE','GRADES','UNITS']) 

		// 	var res=[];
		// 	var lastSY = 'start';
		// 	var columns = [
		// 		{text:'COURSE NO',bold:true,border:[true,true,false,true],fillColor: '#dddddd',alignment:'center'},
		// 		{text:'DESCRIPTIVE TITLE',bold:true,border:[false,true,false,true],fillColor: '#dddddd',alignment:'center'},
		// 		{text:'GRADES',bold:true,border:[false,true,false,true],fillColor: '#dddddd',alignment:'center'},
		// 		{text:'UNITS',bold:true,border:[false,true,true,true],fillColor: '#dddddd',alignment:'center'}
		// 	];

		// 	var lastRowDetails = [];
		// 	var mergedDataDetails = [];
		// 	res.push(columns);
		// 	res.push([{text:'UNIVERSITY OF SAINT LOUIS',colSpan:4},{},{},{}]);
		//   //  console.log(semTableArray[0].SY);
		// 	var lastRow = false;
		// 	var dataRow=[];
		// 	var tempdataRow = [];
		// 	var finaldataRow=[];
		// 	var feCtr = 0;
		// 	var CN='';
		// 	var D='';
		// 	var G='';
		// 	var U='';

		// 	semTableArray.forEach(function(row){ 
		// 		if(lastSY === row.SY){
		// 			if(lastRowDetails[0].sy === true){
		// 			   dataRow = [];   
		// 			   dataRow.push(
		// 				  // {text:row.COURSENO,border: [true, false, true, false]},{text:row.DESC,border: [true, false, true, false]},{text:row.GRADES,alignment:'center',border: [true, false, true, false]},{text:row.UNITS,alignment:'center',border: [true, false, true, false]}
		// 				  {text:row.COURSENO},
		// 				  {text:row.DESC},
		// 				  {text:row.GRADES,alignment:'center'},
		// 				  {text:row.UNITS,alignment:'center'}
		// 			   ); 
		// 			   lastSY = row.SY; 
		// 			   lastRowDetails = [];
		// 			   lastRowDetails.push({
		// 					   crsNo:row.COURSENO,
		// 					   desc:row.DESC,
		// 					   grd:row.GRADES,
		// 					   un:row.UNITS,
		// 					   sy:false
		// 				   });
		// 			   CN = row.COURSENO;
		// 			   D = row.DESC;
		// 			   G = row.GRADES;
		// 			   U = row.UNITS; 
		// 			}
		// 			else{

		// 			   CN = CN.concat(('\n'+row.COURSENO).toString());
		// 			   D = D.concat('\n'+row.DESC);
		// 			   G = G.concat('\n'+row.GRADES);
		// 			   U = U.concat('\n'+row.UNITS); 

		// 			   dataRow.splice(0,1,{text:CN,lineHeight: 1.15}); 
		// 			   dataRow.splice(1,1,{text:D,lineHeight: 1.15});
		// 			   dataRow.splice(2,1,{text:G,alignment:'center',lineHeight: 1.15});
		// 			   dataRow.splice(3,1,{text:U,alignment:'center',lineHeight: 1.15});


		// 			   lastRowDetails.push({
		// 				   crsNo:CN, 
		// 				   desc:D,
		// 				   grd:G,
		// 				   un:U,
		// 				   sy:false
		// 			   }); 
		// 			  lastSY = row.SY;  
		// 			}
		// 		}
		// 		else{ 
		// 			if(lastSY!='start'){ 
		// 			   res.push(dataRow); 
		// 			} 

		// 			dataRow=[];  
		// 			dataRow.push(
		// 				{text:" ",border: [true, true, false, true]},
		// 				{text:row.SY,bold:true,colSpan:3,border: [false, true, true, true]},
		// 				{},
		// 				{});
		// 			lastRowDetails=[]; 
		// 			lastRowDetails.push({
		// 				   crsNo:"",
		// 				   desc:'',
		// 				   grd:'',
		// 					un:'',
		// 					sy:true
		// 			   });
		// 			lastSY = row.SY;
		// 			res.push(dataRow);
		// 		}

		// 		feCtr+=1;
		// 		if(feCtr === semTableArray.length){
		// 		   res.push(dataRow); 
		// 		}
		// 	});
		// 	return res;
		// }
		function fillsemestertable() {
			// console.log(array)
			// var current_page = Array.from({length: 10}, (_, index) => index + 1);
			// console.log('PAGE: ',current_page[0])
			var dataRow = [];
			var semTableArray = [];
			var companyNames = []
			// console.log("groupedSy: ", array)
			var res = [];

			// { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
			// { text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
			// { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
			// { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }

			var columns = [
				{ text: 'COURSE NO', bold: true, border: [true, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				{ text: 'DESCRIPTIVE TITLE', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				{ text: 'GRADES', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				{ text: 'CREDIT UNITS', bold: true, border: [false, true, true, true], fillColor: '#dddddd', alignment: 'center' }
			];


			res.push(columns);
			var lastCompany = ''
			if (array.length != 0) {
				companyNames = get_company_names_per_group(array)

				for (var i = 0; i < array.length; i++) {

					var array_sy = '';
					var date = array[i][0].schoolYear;
					var y1 = date.substring(0, 4);
					var y2 = parseInt(y1) + 1;
					var a = y1.toString() + " - " + y2.toString();
					var term = date.substring(6, 7);
					var c = "";

					if (term == '1')
						c = "First Semester";
					else if (term == '2')
						c = "Second Semester";
					else if (term == '3')
						c = "Summer";

					array_sy = c + ' ' + a;
					// console.log('sy', array_sy)
					if (companyNames[i].companyName != lastCompany)
						res.push([{ text: companyNames[i].companyName, colSpan: 4, bold: true, }, {}, {}, {}]);

					lastCompany = companyNames[i].companyName
					res.push([{ text: array_sy, colSpan: 4, margin: [99, 0, 0, 0], bold: true, }, {}, {}, {}]);
					var tempres = [];
					for (var j = 0; j < array[i].length; j++) {
						dataRow = []

						dataRow.push(
							{ text: array[i][j].subjectId, border: [false, false, true, false] },
							{ text: array[i][j].subjectTitle, border: [true, false, true, false] },
							{ text: array[i][j].grade, alignment: 'center', border: [false, false, true, false] },
							{ text: array[i][j].units, alignment: 'center', border: [false, false, false, false] },

						);




						// var currentpage = 1
						// var pagecount = 1
						// Check if it is the last row and apply a border to all columns
						// if (j === array[i].length - 1) {
						// 	dataRow.forEach(function (cell) {
						// 		// console.log('Cell: ',cell)
						// 		// cell.border[2] = true; // Apply border to the right side
						// 		cell.border[3] = true; // Apply border to the bottom side

						// 	});
						// }

						tempres.push(dataRow);
						// console.log(dataRow[3])
					}
					res.push(
						[
							{
								table: {
									widths: [90, 345, 40, 40],
									body:
										tempres

								}, fontSize: 9,
								layout: {
									hLineWidth: function (i, node) { return 0.5; },
									vLineWidth: function (i, node) { return 0.5; },
								}
								, colSpan: 4
							},
							{ text: "" },
							{ text: "" },
							{ text: "" },
						]
					)


				}

			}

			else {

				dataRow = []
				dataRow.push(

					{ text: '' },
					{ text: '' },
					{ text: '', alignment: 'center' },
					{ text: '', alignment: 'center' }
				);
				res.push(dataRow)
			}


			// var lastRow = res[res.length - 1];
			// for (var k = 0; k < lastRow.length; k++) {
			// 	lastRow[k].border[2] = true;

			// }

			// res.push([
			// 	{ text: '' },
			// 	{ text: '' },
			// 	{ text: '' },
			// 	{ text: '' }
			//   ]);
			return res;
		}

		function get_company_names_per_group(array) {
			var res = []
			for (let index = 0; index < array.length; index++) {
				res.push({
					companyName: array[index][0].companyName
				})
			}
			return res;
		}

		// const pdfMake = require('pdfmake/build/pdfmake.js');
		// const pdfFonts = require('pdfmake/build/vfs_fonts.js')
		// pdfMake.fonts = {
		// 	TimesNewRomanRegular: {
		// 	  normal: `data:application/font-woff;base64,${TimesNewRomanRegular}`
		// 	}
		//   };

		var dd = {

			background: function (currentPage, pageSize) {
				// console.log(`Current page: ${currentPage}`);
				const pageWidth = pageSize.width;
				const pageHeight = pageSize.height;
				const watermarkWidth = 612;
				const watermarkHeight = 936;
				const pageRatio = pageWidth / pageHeight;
				const watermarkRatio = watermarkWidth / watermarkHeight;
				let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

				if (watermarkRatio > pageRatio) {
					scale = pageWidth / watermarkWidth;
				} else {
					scale = pageHeight / watermarkHeight;
				}

				scaledWatermarkWidth = watermarkWidth * scale;
				scaledWatermarkHeight = watermarkHeight * scale;
				x = (pageWidth - scaledWatermarkWidth) / 2;
				y = (pageHeight - scaledWatermarkHeight) / 2;

				return {
					image: "data:image/png;base64," + WaterMark,
					width: scaledWatermarkWidth,
					height: scaledWatermarkHeight,
					opacity: 30,
					absolutePosition: { x: x, y: y }
				};

			},
			pageSize: {
				width: 612, // 8.5 inches * 72 = 612
				height: 936 // 14 inches * 72 = 792
			},
			// watermark: { text: 'USL USL USL USL USL USL USL USL USL USL USL USL USL USL USL ', color: 'gray', opacity: 0.3, bold: true, italics: true },
			content: [
				{
					toc: {
						id: 'mainToc',
						title: {
							table: {
								widths: [100, 325, 100],
								body: [
									[
										// { image: "data:image/png;base64," + imageData, width: 72.16, alignment: 'right' },
										{ image: "data:image/png;base64," + imageData, width: 100, margin: [25, 0, 0, 0] },
										{

											stack: [
												{ text: 'UNIVERSITY OF SAINT LOUIS', bold: true, fontSize: 13.5 },
												{ text: 'Formerly Saint Louis College of Tuguegarao', italics: true, fontSize: 11 },
												{ text: 'Tuguegarao City, Philippines 3500', italics: true, margin: [0, 0, 0, 7], fontSize: 10.5 },
												{ text: '\nGranted FULL AUTONOMY', bold: true, fontSize: 11 },

												{ text: 'by the Commission on Higher Education\n\n', fontSize: 10.5 },
												{ text: 'OFFICIAL TRANSCRIPT OF RECORDS\n', bold: true, fontSize: 12, margin: [0, 6, 0, 0] },
												{ text: '', margin: [0, 8, 0, 0] },
												{
													canvas: [
														{
															type: 'rect',
															x: 0,
															y: -65,
															w: 230,
															h: 36,
															lineWidth: 0.75,
														}
													]
												},
											], alignment: 'center'
										},
										// { image: "data:image/png;base64," + IDPicture, width: 72.16, alignment: 'right' },
										{ image: "data:image/png;base64," + IDPicture, width: 80, margin: [0, 0, 0, 0] },
									]
								]
							}, margin: [0, -190, 30, 0],
							layout: 'noBorders'
						}
					}
				},

				{
					stack: [
						{
							table: {
								widths: ['*'],
								body: [
									[
										{ text: 'NAME: ' + LastName + ' ' + FirstName + '' + MiddleName, bold: true, fontSize: 11, margin: [0, 5, 0, 3] }
									]

								]
							}, fontSize: 9,
							layout: {
								hLineWidth: function (i, node) { return 0.5; },
								vLineWidth: function (i, node) { return 0.5; },
								paddingRight: function (i, node) { return 0; },
							}
						}, { text: '\n', fontSize: 5 },
						{
							table: {
								widths: ['*'],
								body: [
									[
										{
											stack:
												[
													// {
													// 	table: {
													// 		widths: [105, 10, '*'],
													// 		heights: 16,
													// 		body:
													// 			[
													// 				// [{ text: 'Complete Home Adress', bold: true, fontSize: 9, margin: [4, 0, 4, 0], }, { text: ':', fontSize: 9 }, { text: Address, fontSize: 9 }],
													// 			]
													// 	}, layout: 'noBorders'
													// },
													{
														table: {
															widths: [105, 10, '*'],
															body: [
																[{ text: 'Complete Adress', bold: true, fontSize: 9, margin: [4, 0, 4, 0], }, { text: ':', fontSize: 9 }, { text: Address, fontSize: 9 }],
																[{ text: '', bold: true, fontSize: 9, margin: [0, 0, 0, 0], }, { text: '', fontSize: 9 }, { text: '', fontSize: 9 }],
																[{ text: 'ENTRANCE DATA:', decoration: 'underline', bold: true, fontSize: 9, margin: [4, 0, 4, 0], colSpan: 3 }, {}, {}],
																[{ text: 'Elementary', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.Elem, fontSize: 9 }],
																[{ text: 'Junior High School', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.JHS, fontSize: 9 }],
																[{ text: 'Senior High School', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.SHS, fontSize: 9 }],
																[{ text: 'College', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.COL, fontSize: 9 }],
																[{ text: 'Post Graduate (if any)', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.postGRAD, fontSize: 9 },],
																[{ text: '', bold: true, fontSize: 10 }, {}, {}],
																[{ text: 'RECORD OF GRADUATION:', decoration: 'underline', bold: true, fontSize: 9, margin: [4, 3, 4, 0], colSpan: 3 }, {}, {}],
																[{ text: 'Degree/Title', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: Degree, fontSize: 9 }],
																// [{ text: '', bold: true, fontSize: 10 }, {}, {}],
																[{ text: 'Major', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: Major, fontSize: 9 }],
																[{ text: 'Date of Graduation', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: this.DateGrad, fontSize: 9 }],
																[{ text: 'Special Order Number', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: SONumber, fontSize: 9 }],
																[{ text: 'Awards Received', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: AwardsReceived, fontSize: 9 }],
																[{ text: 'NSTP Serial No.', bold: true, fontSize: 9, margin: [4, 0, 4, 0] }, { text: ':', fontSize: 9 }, { text: SerialNo, fontSize: 9 }],
															]
														}, layout: {
															hLineWidth: function (i, node) { return 0; },
															vLineWidth: function (i, node) { return 0; },
															paddingRight: function (i, node) { return 0; },
															paddingLeft: function (i, node) { return 0; },
															paddingTop: function (i, node) { return 1; },
															paddingBottom: function (i, node) { return 1; },
														}
													}
												]
										}
									]
								]
							}, layout: {
								hLineWidth: function (i, node) { return 0.5; },
								vLineWidth: function (i, node) { return 0.5; },
							}

						}, { text: '\n', fontSize: 5 },
						{
							table: {
								widths: [350, '*'],
								body:
									[
										[
											{
												stack:
													[
														{
															table: {
																widths: ["*"],
																body:
																	[
																		[
																			{
																				table: {
																					widths: [105, 10, '*'],
																					body:
																						[
																							[
																								{ text: 'Student ID Number', bold: true, fontSize: 8, margin: [0, 3, 0, 0] },
																								{ text: ':', fontSize: 8, margin: [0, 3, 0, 0] },
																								{ text: IDNumber, fontSize: 8, margin: [0, 3, 0, 0] }
																							],
																							[
																								{ text: 'Gender', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.gender, fontSize: 8 }
																							],
																							[
																								{ text: 'Date of Birth', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: DateOfBirth, fontSize: 8 }
																							],
																							[
																								{ text: 'Place of Birth', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: formattedPlaceOfBirth, fontSize: 8 }
																							],
																							[
																								{ text: 'Citizenship', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.nationality, fontSize: 8 }
																							],
																							[
																								{ text: 'Civil Status', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.civilStatus, fontSize: 8 }
																							],
																							[
																								{ text: 'Religion', bold: true },
																								{ text: ':', fontSize: 8 },
																								{ text: this.religion, fontSize: 8 }
																							],
																							[{ text: 'ADMISSION CREDENTIALS:', decoration: 'underline', bold: true, colSpan: 3, alignment: 'left', fontSize: 8 }, {}, {}],
																							[
																								{ text: 'Admission Status', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.admissionStatus, fontSize: 8 }
																							],
																							[
																								{ text: 'Admission Credentials', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.FinalCred, rowSpan: 2, fontSize: 8 }
																							],
																							[
																								{ text: ' ', bold: true, fontSize: 8 },
																								{ text: '', fontSize: 8 },
																								{}
																							],
																							[
																								{ text: 'Date of Admission', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.dateOfAdmission, fontSize: 8 }
																							],
																							[
																								{ text: 'Course Enrolled', bold: true, fontSize: 8 },
																								{ text: ':', fontSize: 8 },
																								{ text: this.programEnrolled, rowSpan: 2, fontSize: 8 }
																							],
																							[
																								{ text: ' ', bold: true, fontSize: 8 },
																								{ text: ' ', fontSize: 8 },
																								{}
																							],
																						]
																				}, fontSize: 9, layout: {
																					hLineWidth: function (i, node) { return 0; },
																					vLineWidth: function (i, node) { return 0; },
																					paddingRight: function (i, node) { return 0; },
																					paddingLeft: function (i, node) { return 0; },
																					paddingTop: function (i, node) { return 1; },
																					paddingBottom: function (i, node) { return 1; },
																				}
																			}
																		]
																	]
															}, layout: {
																hLineWidth: function (i, node) { return 0.5; },
																vLineWidth: function (i, node) { return 0.5; },
																paddingRight: function (i, node) { return 0; },
																paddingLeft: function (i, node) { return 8; },
																paddingTop: function (i, node) { return 1; },
																paddingBottom: function (i, node) { return 1; },
															}
														}
													]
											},
											{
												table:
												{
													widths: ['*'],
													body:
														[
															[
																{
																	stack:
																		[
																			{
																				table: {
																					widths: ['*'],
																					body:
																						[
																							[{ text: 'Official Marks', alignment: 'center', fontSize: 8 }]
																						]
																				}, fontSize: 8, bold: true, margin: [4, 2]
																			},
																			{
																				table: {
																					widths: [40, 50, '*'],
																					body:
																						[
																							[
																								{ text: '95-100', fontSize: 8 },
																								{ text: '- 1.25-1.00', fontSize: 8 },
																								{ text: 'Distinguished', fontSize: 8 }
																							],
																							[
																								{ text: '90-94', fontSize: 8 },
																								{ text: '- 1.50-1.30', fontSize: 8 },
																								{ text: 'Excellent', fontSize: 8 }
																							],
																							[
																								{ text: '85-89', fontSize: 8 },
																								{ text: '- 2.00-1.60', fontSize: 8 },
																								{ text: 'Very Good', fontSize: 8 }
																							],
																							[
																								{ text: '80-84', fontSize: 8 },
																								{ text: '- 2.50-2.10', fontSize: 8 },
																								{ text: 'Good', fontSize: 8 }
																							],
																							[
																								{ text: '75-79', fontSize: 8 },
																								{ text: '- 3.00-2.60', fontSize: 8 },
																								{ text: 'Passing', fontSize: 8 }
																							],
																							[
																								{ text: '74-below', fontSize: 8 },
																								{ text: '- 5.00', fontSize: 8 },
																								{ text: 'Failed', fontSize: 8 }
																							],
																							[
																								{ text: 'N - No Final Exam', colSpan: 3, fontSize: 8 },
																								{},
																								{}
																							],
																							[
																								{ text: 'WP - Withdrawn w/ Permission', colSpan: 3, fontSize: 8 },
																								{},
																								{}
																							],
																							[
																								{ text: 'D - Dropped', colSpan: 3, fontSize: 8 },
																								{},
																								{}
																							],
																							[
																								{ text: 'P - Passed', colSpan: 3, fontSize: 8 },
																								{},
																								{}
																							],
																							[
																								{ text: 'F - Failed', colSpan: 3, fontSize: 8 },
																								{},
																								{}
																							],
																							[
																								{ text: ' ', fontSize: 2.2, colSpan: 3, },
																								{},
																								{}
																							],
																							[
																								{ text: '', fontSize: 2.2, colSpan: 3 },
																								{},
																								{}
																							],

																						]
																				}, fontSize: 9, margin: [10, 0, 0, 9.7], bold: true, layout: {
																					hLineWidth: function (i, node) { return 0; },
																					vLineWidth: function (i, node) { return 0; },
																					paddingRight: function (i, node) { return 0; },
																					paddingLeft: function (i, node) { return 0; },
																					paddingTop: function (i, node) { return 1; },
																					paddingBottom: function (i, node) { return 1; }
																				}
																			}
																		]
																}
															]
														]
												}, margin: [8, 0, 0, 0], layout: {
													hLineWidth: function (i, node) { return 0.5; },
													vLineWidth: function (i, node) { return 0.5; },
													paddingRight: function (i, node) { return 0; },
													paddingLeft: function (i, node) { return 0; },
													paddingTop: function (i, node) { return 1; },
													paddingBottom: function (i, node) { return 1; },
												}

											}
										]
									]
							}, layout: {
								hLineWidth: function (i, node) { return 0; },
								vLineWidth: function (i, node) { return 0; },
								paddingRight: function (i, node) { return 0; },
								paddingLeft: function (i, node) { return 0; },
								paddingTop: function (i, node) { return 1; },
								paddingBottom: function (i, node) { return 1; },
							}
						}, { text: '\n', fontSize: 5 }, { text: 'Remarks', bold: true, fontSize: 10 },
						{
							table: {
								widths: ['*'],
								body: [
									[
										[
											{ text: this.Baccalaureate, fontSize: 10, },
											{
												text: this.otrType === 2 ? this.AdditionalRemarks : (this.otrType === 3 || this.otrType === 4 || this.otrType === 5) ? this.AdditionalRemarks : '',
												fontSize: 10
											}

										]
									]
								]
							}, fontSize: 10, layout: {
								hLineWidth: function (i, node) { return 0.5; },
								vLineWidth: function (i, node) { return 0.5; },
							}
						}
					]
				},
				{
					stack: [
						{ text: '\n' },
						{
							style: 'tableExample',
							table: {
								headerRows: 1,
								pageBreak: 'before',
								dontBreakRows: false,
								// keepWithHeaderRows: 1,
								widths: [90, 345, 40, 43],
								body: fillsemestertable(),
							},
							fontSize: 9,
							layout: {
								paddingTop: function (i, node) {
									return 0.2;
									//return (i === node.table.widths.length - 1) ? 5 : 5;
								},
								paddingBottom: function (i, node) {
									return 0.2;
									//return (i === node.table.widths.length - 1) ? 5 : 5;
								},
								hLineWidth: function (i, node) { return 0.5; },
								vLineWidth: function (i, node) { return 0.5; },
							}
						}
						// layout: {
						// 	hLineWidth: function (i, node) { return (i === 0 || i === node.table.body.length) ? 0.5 : 0; },
						// 	vLineWidth: function (i, node) { return 0.5; },
						// 	hLineColor: function (i, node) { return (i === 0 || i === node.table.body.length) ? '#000000' : '#dddddd'; },
						// 	vLineColor: function (i, node) { return '#000000'; },
						//   },
						//   pageBreakBefore: function(currentNode) {
						// 	return currentNode.head !== null && currentNode.pageNumber > 1;
						//   },
						//   didDrawPage: function (data) {
						// 	var pageCount = data.table.pageCount;
						// 	var currentPage = data.table.pageNumber;
						// 	var isLastPage = currentPage === pageCount;
						// 	var rows = data.table.body;

						// 	for (var i = 0; i < rows.length; i++) {
						// 		console.log('rows',rows)
						// 	  var row = rows[i];
						// 	  var cells = row.map(function (cell) {
						// 		if (isLastPage) {
						// 		  cell.borderBottomColor = '#000000';
						// 		  cell.borderBottomWidth = 0.5;
						// 		} else {
						// 		  cell.borderBottomColor = '#dddddd';
						// 		  cell.borderBottomWidth = 0.5;
						// 		}
						// 		return cell;
						// 	  });
						// 	  rows[i] = cells;
						// 	}
						//   }
						// }


						// { text: '\n', margin: [0, 0, 1, 0] } // Add a margin to the element after the table
					]

				},
				{ text: "------------------------------------------end of transcript------------------------------------------", alignment: 'center', fontSize: 9, margin: [0, 8, 0, 0] }
			],
			pageMargins: [30, 220, 30, 140],
			// pageMargins: [ 30,300, 30, 140 ],
			// watermark:{text:'UNIVERSITY\nof SAINT LOUIS\nTUGUEGARAO',bold:true,fontSize:16},
			header: function (currentPage, pageCount, pageSize) {
				//console.log(pageCount)
				return [
					validateHeaderDisplay(currentPage, pageCount),
				]
			},
			footer: function (currentPage, pageCount, pageSize) {
				// console.log('CURRENTPAGE',currentPage ,'PAGECOUNT',pageCount)
				// console.log(currentPage)
				// console.log(currentPage)
				// var lineBreak

				// if (currentPage == 1) {
				// 	lineBreak = { canvas: [{ type: 'line', x1: 0, y1: -8.6, x2: 549.3, y2: -8.6, lineWidth: 0.5 }] }
				// }

				// else if (currentPage == 2) {
				// 	lineBreak = { canvas: [{ type: 'line', x1: 0, y1: -12.7, x2: 549.6, y2: -12.7, lineWidth: 0.5 }] }
				// }
				// else if (currentPage < pageCount) {
				// 	lineBreak = { canvas: [{ type: 'line', x1: 0, y1: -8.8, x2: 549.3, y2: -8.8, lineWidth: 0.5, }] }
				// }
				return [
					// { text: 'simple text', alignment: (currentPage % 2) ? 'left' : 'right' },
					{

						stack: [

							// lineBreak
							// ,
							{ text: 'Prepared by:', alignment: 'left', fontSize: 8, margin: [0, 8, 0, 0] },
							{
								table: {
									widths: [265.2, 265.2],
									body: [
										[
											{
												// border: [false, false, false, false],
												stack: [
													{ text: '\n\nMARY ANN D. MOLINA', bold: true },
													{ text: 'Records Clerk\n' }
												]
											},
											{
												// border: [false, false, false, false],
												stack: [
													{ text: '\n\nLIZA M. EMPEDRAD Ph.D.', bold: true, },
													{ text: 'University Registrar\n\n\n' }
												]
											}
										],
									]
								},
								fontSize: 9,
								alignment: 'center',
								layout: {
									hLineWidth: function (i, node) { return 0; },
									vLineWidth: function (i, node) { return 0; },
								}
							},
							{ canvas: [{ type: 'line', x1: 0, y1: 0, x2: 553, y2: 0, lineWidth: 0.5 }] },
							{
								table: {
									widths: ['*'],

									body: [
										[
											{
												ul: [
													{ text: 'This transcript is valid only when it bears the dry seal of the University and the original signature in ink of the University Registrar. Any erasure or alteration made in this copy will make the whole transcript invalid.', alignment: 'justify', italics: true, fontSize: 5.5 },
													{ text: 'One UNIT OF CREDIT is one hour of lecture or recitation, three hours of laboratory, drafting or shop work each week, for the period of a complete semester.', alignment: 'justify', italics: true, fontSize: 5.5 },
												]

											}
										],
										[
											{ text: 'Page ' + currentPage + ' of ' + pageCount, alignment: 'right', margin: [0, 18, 0, 0] }
										]
									]
								}, fontSize: 9,
								layout: 'noBorders'
							}
						], margin: [30, 0, 30, 30]
					}
				]
			},
		}

		// pdfMake.fonts = {
		// 	TimesNewRoman: {
		// 		normal: 'Times-Roman.afm',
		// 		bold: 'Times-Bold.afm',
		// 		italics: 'Times-Italic.afm',
		// 		bolditalics: 'Times-BoldItalic.afm'
		// 	},

		//  }
		//FONT
		// Function to convert a font file to a base64 string
		// function fontToBase64(fontPath) {
		// 	return new Promise((resolve, reject) => {
		// 		const xhr = new XMLHttpRequest();
		// 		xhr.open('GET', fontPath, true);
		// 		xhr.responseType = 'blob';
		// 		xhr.onload = () => {
		// 			if (xhr.status === 200) {
		// 				const reader = new FileReader();
		// 				reader.onloadend = () => {
		// 					if (typeof reader.result === 'string') {
		// 						const base64String = btoa(reader.result);
		// 						resolve(base64String);
		// 					} else if (reader.result instanceof ArrayBuffer) {
		// 						const binary = String.fromCharCode.apply(null, new Uint8Array(reader.result));
		// 						const base64String = btoa(binary);
		// 						resolve(base64String);
		// 					} else {
		// 						reject(new Error(`Invalid result type: ${typeof reader.result}`));
		// 					}
		// 				};
		// 				reader.readAsBinaryString(xhr.response);
		// 			} else {
		// 				reject(new Error(`Failed to load font: ${fontPath}`));
		// 			}
		// 		};
		// 		xhr.onerror = () => {
		// 			reject(new Error(`Failed to load font: ${fontPath}`));
		// 		};
		// 		xhr.send();
		// 	});
		// }

		// // Usage example
		// const fontPaths = {
		// 	'Times-Roman': {
		// 	  normal: 'assets/fonts/times-new-roman-regular.ttf',
		// 	  bold: 'assets/fonts/times-new-roman-bold.TTF',
		// 	  italics: 'assets/fonts/times-new-roman-italic.ttf',
		// 	  bolditalics: 'assets/fonts/times-new-roman-bold-italic.ttf'
		// 	}
		//   };


		// const fontPromises = Object.keys(fontPaths).map((fontName) => {
		// 	const fontPath = fontPaths[fontName].normal;
		// 	return fontToBase64(fontPath).then((base64String) => ({
		// 		[fontName]: {
		// 			normal: `data:application/x-font-ttf;base64,${base64String}`
		// 		}
		// 	}));
		// });

		// Promise.all(fontPromises)
		// 	.then((fontArray) => {
		// 		console.log(fontPromises)
		// 		var fonts = Object.assign({}, ...fontArray);
		// 		var docDefinition = {
		// 			content: [
		// 				{ text: 'Hello World!', style: 'header' },
		// 				{ text: 'This is a sample text.', style: 'body' }
		// 			],
		// 			styles: {
		// 				header: { fontSize: 24, font: 'Times-Roman', bold: true },
		// 				body: { fontSize: 14, font: 'Times-Roman', lineHeight: 1.5 }
		// 			},
		// 			defaultStyle: { font: 'Arial' },
		// 			fonts: fonts
		// 		};
		// 		// Use docDefinition to generate a PDF document
		// 		console.log(docDefinition);
		// 		// pdfMake.createPdf(docDefinition).open();
		// 	})
		// 	.catch((err) => console.error(err));
		//FONT
		// var dd = {
		//   pageSize: 'FOLIO',
		//   pageOrientation: 'portrait', pageMargins: [40, 40, 40, 40],
		//   pageCtr: pageCtr,
		//   topmargin: topmargin,
		//   headerDisplay: headerDisplay,
		//   footerDisplay: footerDisplay,

		//   header: header,
		//   content: content,
		//   footer: footer
		// }
		// pdfMake.createPdf(dd).download('OTR' + '-' + this.FullName + '.pdf');
		pdfMake.createPdf(dd).open();
		// pdfMake.createPdf(dd, null, null, fonts).download();


		this.callOtrApy()
		// this.Refresh()
		// console.log(this.AddedRecord)
	}


	downloadpdf() {
		// #region
		var logo = this.images.logo
		// #endregion

		// this.logo
		// var x in this.registrarCertificateData
		// lname
		// fname
		// mname
		// this.convertNumberToLetter
		var header = [
			{
				image: "data:image/png;base64," + logo,
				width: 70,
				opacity: 1,
				absolutePosition: { x: 155, y: 40 },
			},
			{
				text: 'University of Saint Louis', bold: true, fontSize: 15, alignment: 'left',
				absolutePosition: { x: 225, y: 46 }
			},
			{
				text: 'Mabini Street, Tuguegarao City Cagayan, Philippines\nTel. No. (078) 844-1872/73\nFax No. (078) 844-0889', italics: true, fontSize: 10, alignment: 'left',
				absolutePosition: { x: 225, y: 63 }
			},
			{
				canvas:
					[
						{
							type: 'line',
							x1: 43, y1: 117,
							x2: 570, y2: 117,
							lineWidth: 2,
						},
					]
			},
			{
				text: 'OFFICE OF THE UNIVERSITY REGISTRAR \nCERTIFICATION', italics: false, fontSize: 12, alignment: 'center', bold: true,
				absolutePosition: { x: 0, y: 125 }
			}
		]


		var tableContent = [
			[
				{ text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				{ text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				{ text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				{ text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }
			]
		]

		for (var x in this.registrarCertificateData) {
			tableContent.push(
				[
					//@ts-ignore
					{ text: this.registrarCertificateData[x].subjectID, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
					//@ts-ignore
					{ text: this.registrarCertificateData[x].subjectTitle, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
					//@ts-ignore
					{ text: this.registrarCertificateData[x].units, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 },
					//@ts-ignore
					{ text: this.registrarCertificateData[x].grade, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 }
				]
			)
		}

		tableContent.push(
			[
				//@ts-ignore
				{ text: '', border: [false, true, false, false], colSpan: 4 }
			],
			[
				//@ts-ignore
				{ text: '', border: [false, false, false, false], colSpan: 4 }
			],
			[
				//@ts-ignore
				{ text: '', border: [false, false, false, false], colSpan: 4 }
			],
			[

				{
					//@ts-ignore
					text: '\u200B           This certification is issued this ' + this.today + ' upon the request of Mr./Mrs. ' + this.lastName + ' ' + this.firstName + ' ' + this.middleName + ' for ' + this.purpose, italics: false, fontSize: 11, colSpan: 4, border: [false, false, false, false],
				},
			]
		)


		var content = [
			{
				text: 'TO WHOM MAY IT CONCERN', italics: false, fontSize: 11, alignment: 'left',
				absolutePosition: { x: 45, y: 165 }
			},
			{
				text: '\u200B           This is to certify that Mr./Mrs. ' + this.lastName + ' ' + this.firstName + ' ' + this.middleName + ' took the following subject/s to wit: ', italics: false, fontSize: 11, alignment: 'left',
				absolutePosition: { x: 45, y: 185 }
			},
			{
				text: this.convertNumberToLetter(this.global.syDisplay(this.schoolYear)), italics: true, fontSize: 11, alignment: 'left', bold: true,
				absolutePosition: { x: 45, y: 215 }
			},
			{
				table: {
					layout: {
						margin: [10, 10, 10, 10] // specify the margins as an array of [left, top, right, bottom]
					},
					widths: ['15%', '70%', '*', '*'],
					body: tableContent
				},
				absolutePosition: { x: 41, y: 230 },
			}
		]

		var footer = [
			{
				text: 'Very truly yours,', italics: false, fontSize: 11, alignment: 'left',
				absolutePosition: { x: 430, y: 32 }
			},
			{
				text: 'University Seal', italics: false, fontSize: 11, alignment: 'left',
				absolutePosition: { x: 45, y: 77 }
			},
			{
				text: 'LIZA M. EMPEDRAD, Ph.D.', italics: false, fontSize: 11, alignment: 'left', bold: true,
				absolutePosition: { x: 430, y: 77 }
			},
			{
				text: 'University Registrar', italics: false, fontSize: 11, alignment: 'left',
				absolutePosition: { x: 443, y: 88 }
			},
		]

		var dd = {
			pageSize: 'LETTER',
			pageOrientation: 'portrait', pageMargins: [40, 170, 40, 170],
			header: header,
			content: content,
			footer: footer
			// this.lastName + ' ' + this.firstName + '' + this.middleName
		}
		// pdfMake.createPdf(dd).download('Registrar Certificate ' + this.lastName + ', ' + this.firstName + '.pdf');
		pdfMake.createPdf(dd).open();
	}
	convertNumberToLetter(str) {
		if (str.includes('1st'))
			return str.replace(/1st/g, 'First');
		return str.replace(/2nd/g, 'Second');
	}

	getDaySuffix(day: number): string {
		if (day >= 11 && day <= 13) {
			return 'th';
		}

		const lastDigit = day % 10;

		switch (lastDigit) {
			case 1:
				return 'st';
			case 2:
				return 'nd';
			case 3:
				return 'rd';
			default:
				return 'th';
		}
	}
	dateformat(date) {
		// console.log('ID: ', date)
		var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var today: any = new Date(date);
		// console.log(this.dateofGraduation)
		var dd = String(today.getDate());
		var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
		var monthName = mL[today.getMonth()];
		var daySuffix = "th";

		// Determine the day suffix based on the date
		if (dd === "1" || dd === "21" || dd === "31") {
			daySuffix = "st";
		} else if (dd === "2" || dd === "22") {
			daySuffix = "nd";
		} else if (dd === "3" || dd === "23") {
			daySuffix = "rd";
		}
		this.dateofGradFormatted = dd + daySuffix + ' day of ' + monthName + ' ' + yyyy;
		// console.log(this.dateofGradFormatted)
	}

	generateGraduationPDF() {
		// console.log('civilstatus', this.civilStatus)
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}
		degreeTitle = ''
		dateofGraduation = ''
		this.dateofGradFormatted = ''
		specialOrderNumber = ''
		CodeCourse = ''

		if (this.DegreeLevel == null || this.DegreeLevel == '') {

			this.global.swalAlert('Please Select Degree', '', 'warning')
		}
		else {
			var LAST = this.lastName
			var MIDDLE = this.middleName
			var FIRST = this.firstName
			var Course = this.programTitle
			var CourseCode = this.courseCode
			var HeaderFooterPicture = this.images.header
			var headerlogo = this.images.header

			var DegreeLevel = this.DegreeLevel.programTitle + " (" + this.DegreeLevel.courseCode + ')'

			// console.log(DegreeLevel)

			//----------
			// console.log(this.programTitle)
			// console.log(this.courseCode)
			// console.log('GGGGG', this.GetGradRec)
			for (var x in this.GetGradRec) {
				if (this.DegreeLevel == 1 && this.GetGradRec[x].otrType == 1) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
				if (this.DegreeLevel == 2 && this.GetGradRec[x].otrType == 2) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
				if (this.DegreeLevel == 3 && this.GetGradRec[x].otrType == 3) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
			}

			var header = {

			};

			// [left, top, right, bottom]
			var content = [
				{

				},
				{
					text: 'C E R T I F I C A T I O N',
					fontSize: 18,
					bold: true,
					alignment: 'center',
					// marginTop: 70,
					margin: [50, 70, 50, 0],
					decoration: 'underline',
				},
				{
					text: 'TO WHOM IT MAY CONCERN:',
					fontSize: 12,
					bold: true,
					// marginTop: 40
					margin: [50, 40, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          THIS IS TO CERTIFY that ' },
						{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
						{ text: ' is a graduate of the University in ' + degreeTitle + ' (' + CodeCourse + ') on ' + this.dateofGradFormatted + ' with Special Order ' + specialOrderNumber }
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
					fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.',
					fontSize: 11,
					bold: true,
					alignment: 'right',
					// marginTop: 40

					margin: [50, 50, 50, 0]
				},
				{
					text: 'University Registrar',
					fontSize: 11,
					alignment: 'right',
					margin: [50, 0, 50, 0]
				},
				{
					text: 'University Seal',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 250
					margin: [50, 250, 50, 0]
				},
				{
					text: '/vcc 2023',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 2
					margin: [50, 2, 50, 0]
				}
			];

			var footer = {
			};

			var dd = {

				background: function (currentPage, pageSize) {
					// console.log(`Current page: ${currentPage}`);
					const pageWidth = pageSize.width;
					const pageHeight = pageSize.height;
					const watermarkWidth = 612;
					const watermarkHeight = 936;
					const pageRatio = pageWidth / pageHeight;
					const watermarkRatio = watermarkWidth / watermarkHeight;
					let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					if (watermarkRatio > pageRatio) {
						scale = pageWidth / watermarkWidth;
					} else {
						scale = pageHeight / watermarkHeight;
					}

					scaledWatermarkWidth = watermarkWidth * scale;
					scaledWatermarkHeight = watermarkHeight * scale;
					x = (pageWidth - scaledWatermarkWidth) / 2;
					y = (pageHeight - scaledWatermarkHeight) / 2  // Increase the value here to move it lower

					return {
						image: "data:image/png;base64," + HeaderFooterPicture,
						width: scaledWatermarkWidth,
						height: scaledWatermarkHeight,
						opacity: 30,
						absolutePosition: { x: x, y: y }
					};
				},

				// pageSize: {
				// 	width: 612, // 8.5 inches * 72 = 612
				// 	height: 936 // 14 inches * 72 = 792
				// },
				pageSize: 'LETTER',
				pageOrientation: 'portrait',
				pageMargins: [40, 100, 40, 100],
				header: header,
				content: content,
				footer: footer
			};

			var degreeeee
			if (degreeTitle == null || degreeTitle == '') {

				if (this.DegreeLevel == 1) {
					degreeeee = 'College'
				}
				if (this.DegreeLevel == 2) {
					degreeeee = 'Masteral'
				}
				if (this.DegreeLevel == 3) {
					degreeeee = 'Doctoral'
				}

				this.global.swalAlert('No graduation record for ' + degreeeee, 'Please add record to generate', 'warning')
			}
			else {
				pdfMake.createPdf(dd).open();
			}
			// pdfMake.createPdf(dd).open();
		}


	}
	generateCompletionGrades() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		degreeTitle = ''
		dateofGraduation = ''
		this.dateofGradFormatted = ''
		specialOrderNumber = ''
		CodeCourse = ''
		if (this.DegreeLevel == null || this.DegreeLevel == '') {

			this.global.swalAlert('Please Select Degree', '', 'warning')
		}
		else {
			var LAST = this.lastName
			var MIDDLE = this.middleName
			var FIRST = this.firstName

			var HeaderFooterPicture = this.images.header
			var headerlogo = this.images.header

			for (var x in this.GetGradRec) {
				if (this.DegreeLevel == 1 && this.GetGradRec[x].otrType == 1) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
				if (this.DegreeLevel == 2 && this.GetGradRec[x].otrType == 2) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
				if (this.DegreeLevel == 3 && this.GetGradRec[x].otrType == 3) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
				}
			}

			var header = {

			};

			// {
			// 	image: "data:image/png;base64," + headerlogo,
			// 	width: 70,
			// 	opacity: 1,
			// 	absolutePosition: { x: 155, y: 40 },
			// },
			var content = [
				{

				},
				{
					text: 'C E R T I F I C A T I O N',
					fontSize: 18,
					bold: true,
					alignment: 'center',
					// marginTop: 70,
					margin: [50, 70, 50, 0],
					decoration: 'underline',
				},
				{
					text: 'TO WHOM IT MAY CONCERN:',
					fontSize: 12,
					bold: true,
					// marginTop: 40
					margin: [50, 40, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          THIS IS TO CERTIFY that ' },
						{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
						{ text: ' has completed the academic requirements for the degree ' + degreeTitle + ' (' + CodeCourse + ').' }
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          This is to further certify that ' },
						{ text: gender + LAST, bold: true },
						{ text: ' graduated from the degree ' + degreeTitle + ' (' + CodeCourse + ') on ' + this.dateofGradFormatted + ' with Special Order ' + specialOrderNumber }
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
					fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.',
					fontSize: 11,
					bold: true,
					alignment: 'right',
					// marginTop: 40

					margin: [50, 50, 50, 0]
				},
				{
					text: 'University Registrar',
					fontSize: 11,
					alignment: 'right',
					margin: [50, 0, 50, 0]
				},
				{
					text: 'University Seal',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 250
					margin: [50, 200, 50, 0]
				},
				{
					text: '/vcc 2023',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 2
					margin: [50, 2, 50, 0]
				}
			];

			var footer = {
			};

			var dd = {
				background: function (currentPage, pageSize) {
					// console.log(`Current page: ${currentPage}`);
					const pageWidth = pageSize.width;
					const pageHeight = pageSize.height;
					const watermarkWidth = 612;
					const watermarkHeight = 936;
					const pageRatio = pageWidth / pageHeight;
					const watermarkRatio = watermarkWidth / watermarkHeight;
					let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					if (watermarkRatio > pageRatio) {
						scale = pageWidth / watermarkWidth;
					} else {
						scale = pageHeight / watermarkHeight;
					}

					scaledWatermarkWidth = watermarkWidth * scale;
					scaledWatermarkHeight = watermarkHeight * scale;
					x = (pageWidth - scaledWatermarkWidth) / 2;
					y = (pageHeight - scaledWatermarkHeight) / 2;

					return {
						image: "data:image/png;base64," + HeaderFooterPicture,
						width: scaledWatermarkWidth,
						height: scaledWatermarkHeight,
						opacity: 30,
						absolutePosition: { x: x, y: y }
					};

				},
				// pageSize: {
				// 	width: 612, // 8.5 inches * 72 = 612
				// 	height: 936 // 14 inches * 72 = 792
				// },
				pageSize: 'LETTER',
				pageOrientation: 'portrait',
				pageMargins: [40, 100, 40, 100],
				header: header,
				content: content,
				footer: footer
			};
			var degr
			if (degreeTitle == null || degreeTitle == '') {

				if (this.DegreeLevel == 1) {
					degr = 'College'
				}
				if (this.DegreeLevel == 2) {
					degr = 'Masteral'
				}
				if (this.DegreeLevel == 3) {
					degr = 'Doctoral'
				}

				this.global.swalAlert('No graduation record for ' + degr, 'Please add record to generate', 'warning')
			}
			else {
				pdfMake.createPdf(dd).open();
			}
		}


	}

	async generateGWA() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		degreeTitle = ''
		dateofGraduation = ''
		this.dateofGradFormatted = ''
		specialOrderNumber = ''
		CodeCourse = ''
		if (this.DegreeLevel == null || this.DegreeLevel == '') {

			this.global.swalAlert('Please Select Degree', '', 'warning')
		}
		else {
			try {
				const response = await this.api.getOTRDetails(this.idNumber, this.DegreeLevel).toPromise();
				const result = response.json();
				this.OTRDetails2 = result.data
				if (result != '') {

					const grades = this.OTRDetails2.map(obj => obj.grade);
					const units = this.OTRDetails2.map(obj => obj.units);
					// console.log('grades', grades);
					// console.log(units)

					// Check if the arrays have the same length
					if (grades.length !== units.length) {
						console.error("Error: The number of grades and units should be the same.");
					} else {
						var weightedSum = 0;
						var totalUnits = 0;

						// Calculate the weighted sum
						for (var i = 0; i < grades.length; i++) {
							var grade = parseFloat(grades[i]);
							var unit = parseFloat(units[i]);

							// Check if the conversion resulted in valid numbers
							if (!isNaN(grade) && !isNaN(unit)) {
								weightedSum += grade * unit;
								totalUnits += unit;
							}
						}

						// Calculate the weighted average
						var weightedAverage = weightedSum / totalUnits;

						// Round the weighted average to two decimal places and convert it back to a number
						weightedAverage = Number(weightedAverage.toFixed(2));

						// console.log("Weighted Average:", weightedAverage);
					}
					// var OTRDetss = this.OTRDetails2
					// console.log('sadasd', OTRDetss)
					this.groupsyyy = this.OTRDetails2.reduce((acc, item) => {
						if (acc[item.schoolYear]) {
							acc[item.schoolYear].push(item);
						} else {
							acc[item.schoolYear] = [item];
						}
						return acc;
					}, {});

					this.SY = Object.values(this.groupsyyy);

					// console.log(this.SY)

					var SchoolYear = this.SY
					var array_sy = []; // Create an empty array to store the values of array_sy
					for (var i = 0; i < this.SY.length; i++) {
						var date = this.SY[i][0].schoolYear;
						var y1 = date.substring(0, 4);
						var y2 = parseInt(y1) + 1;
						var a = y1.toString() + " - " + y2.toString();
						var term = date.substring(6, 7);
						var c = "";

						if (term == '1')
							c = "First Semester";
						else if (term == '2')
							c = "Second Semester";
						else if (term == '3')
							c = "Summer";

						this.array_sy_item = c + ' SY ' + a;
						array_sy.push(this.array_sy_item); // Add the current value to the array_sy array
						// console.log('sy', this.array_sy_item);
					}

					this.firstValue = array_sy[0]; // Get the first value from the array_sy array
					this.lastValue = array_sy[array_sy.length - 1]; // Get the last value from the array_sy array

					// console.log('First value:', this.firstValue);
					// console.log('Last value:', this.lastValue);



					if (!this.DegreeLevel) {

						this.global.swalAlert('Please Select Degree', '', 'warning')
					}

					var LAST = this.lastName
					var MIDDLE = this.middleName
					var FIRST = this.firstName

					var HeaderFooterPicture = this.images.header
					var headerlogo = this.images.header


					for (var x in this.GetGradRec) {
						if (this.DegreeLevel == 1 && this.GetGradRec[x].otrType == 1) {
							var degreeTitle = this.GetGradRec[x].degreeTitle
							var dateofGraduation = this.GetGradRec[x].dateofGraduation
							var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
							var CodeCourse = this.GetGradRec[x].courseCode
						}
						if (this.DegreeLevel == 2 && this.GetGradRec[x].otrType == 2) {
							var degreeTitle = this.GetGradRec[x].degreeTitle
							var dateofGraduation = this.GetGradRec[x].dateofGraduation
							var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
							var CodeCourse = this.GetGradRec[x].courseCode
						}
						if (this.DegreeLevel == 3 && this.GetGradRec[x].otrType == 3) {
							var degreeTitle = this.GetGradRec[x].degreeTitle
							var dateofGraduation = this.GetGradRec[x].dateofGraduation
							var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
							var CodeCourse = this.GetGradRec[x].courseCode
						}
					}


					var header = {

					};

					// {
					// 	image: "data:image/png;base64," + headerlogo,
					// 	width: 70,
					// 	opacity: 1,
					// 	absolutePosition: { x: 155, y: 40 },
					// },
					var content = [
						{

						},
						{
							text: 'C E R T I F I C A T I O N',
							fontSize: 18,
							bold: true,
							alignment: 'center',
							// marginTop: 70,
							margin: [50, 70, 50, 0],
							decoration: 'underline',
						},
						{
							text: 'TO WHOM IT MAY CONCERN:',
							fontSize: 12,
							bold: true,
							// marginTop: 40
							margin: [50, 40, 50, 0],
							alignment: 'justify'
						},
						{
							text: [
								{ text: '\u200B          THIS IS TO CERTIFY that ' },
								{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
								{ text: ' a graduate of the University in ' + degreeTitle + ' (' + CodeCourse + ') obtained a general weighted average of ' },
								{ text: + weightedAverage + '% ', bold: true },
								{ text: 'from ' + this.firstValue + ' to ' + this.lastValue },
							], fontSize: 11,
							// marginTop: 15
							margin: [50, 15, 50, 0],
							alignment: 'justify'
						},
						{
							text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
							fontSize: 11,
							// marginTop: 15
							margin: [50, 15, 50, 0],
							alignment: 'justify'
						},
						{
							text: 'LIZA M. EMPEDRAD, Ph.D.',
							fontSize: 11,
							bold: true,
							alignment: 'right',
							// marginTop: 40

							margin: [50, 50, 50, 0]
						},
						{
							text: 'University Registrar',
							fontSize: 11,
							alignment: 'right',
							margin: [50, 0, 50, 0]
						},
						{
							text: 'University Seal',
							fontSize: 9,
							alignment: 'left',
							// marginTop: 250
							margin: [50, 250, 50, 0]
						},
						{
							text: '/vcc 2023',
							fontSize: 9,
							alignment: 'left',
							// marginTop: 2
							margin: [50, 2, 50, 0]
						}
					];

					var footer = {
					};

					var dd = {
						background: function (currentPage, pageSize) {
							// console.log(`Current page: ${currentPage}`);
							const pageWidth = pageSize.width;
							const pageHeight = pageSize.height;
							const watermarkWidth = 612;
							const watermarkHeight = 936;
							const pageRatio = pageWidth / pageHeight;
							const watermarkRatio = watermarkWidth / watermarkHeight;
							let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

							if (watermarkRatio > pageRatio) {
								scale = pageWidth / watermarkWidth;
							} else {
								scale = pageHeight / watermarkHeight;
							}

							scaledWatermarkWidth = watermarkWidth * scale;
							scaledWatermarkHeight = watermarkHeight * scale;
							x = (pageWidth - scaledWatermarkWidth) / 2;
							y = (pageHeight - scaledWatermarkHeight) / 2;

							return {
								image: "data:image/png;base64," + HeaderFooterPicture,
								width: scaledWatermarkWidth,
								height: scaledWatermarkHeight,
								opacity: 30,
								absolutePosition: { x: x, y: y }
							};

						},
						// pageSize: {
						// 	width: 612, // 8.5 inches * 72 = 612
						// 	height: 936 // 14 inches * 72 = 792
						// },
						pageSize: 'LETTER',
						pageOrientation: 'portrait',
						pageMargins: [40, 100, 40, 100],
						header: header,
						content: content,
						footer: footer
					};
					var deggggg
					if (degreeTitle == null || degreeTitle == '') {

						if (this.DegreeLevel == 1) {
							deggggg = 'College'
						}
						if (this.DegreeLevel == 2) {
							deggggg = 'Masteral'
						}
						if (this.DegreeLevel == 3) {
							deggggg = 'Doctoral'
						}

						this.global.swalAlert('No graduation record for ' + deggggg, 'Please add record to generate', 'warning')
					}
					else {
						pdfMake.createPdf(dd).open();
					}
				}
			} catch (error) {
				console.error(error);
			}
		}


	}

	// generateGWA() {	
	// 	this.loadData();
	// }
	generateSPES() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		var LAST = this.lastName
		var MIDDLE = this.middleName
		var FIRST = this.firstName

		var SpesSY = '';
		var date = this.schoolYear;
		var y1 = date.substring(0, 4);
		var y2 = parseInt(y1) + 1;
		var a = y1.toString() + "-" + y2.toString();
		var term = date.substring(6, 7);
		var c = "";

		if (term == '1')
			c = "1st Semester";
		else if (term == '2')
			c = "2nd Semester";
		else if (term == '3')
			c = "Summer";

		SpesSY = c + ' School Year ' + a;

		// console.log('SpesSY', SpesSY)
		// console.log('Spes', this.registrarCertificateData)

		const grades = this.registrarCertificateData.map(obj => obj.grade);
		const units = this.registrarCertificateData.map(obj => obj.units);
		// console.log('grades', grades);
		// console.log(units)

		// Check if the arrays have the same length
		if (grades.length !== units.length) {
			console.error("Error: The number of grades and units should be the same.");
		} else {
			var weightedSum = 0;
			var totalUnits = 0;

			// Calculate the weighted sum
			for (var i = 0; i < grades.length; i++) {
				var grade = parseFloat(grades[i]);
				var unit = parseFloat(units[i]);

				// Check if the conversion resulted in valid numbers
				if (!isNaN(grade) && !isNaN(unit)) {
					weightedSum += grade * unit;
					totalUnits += unit;
				}
			}

			// Calculate the weighted average
			var weightedAverage = weightedSum / totalUnits;

			// Round the weighted average to two decimal places and convert it back to a number
			weightedAverage = Number(weightedAverage.toFixed(2));

			// console.log("Weighted Average:", weightedAverage);
		}


		var HeaderFooterPicture = this.images.header
		var headerlogo = this.images.header
		var header = {

		};

		// {
		// 	image: "data:image/png;base64," + headerlogo,
		// 	width: 70,
		// 	opacity: 1,
		// 	absolutePosition: { x: 155, y: 40 },
		// },
		var content = [
			{

			},
			{
				text: 'C E R T I F I C A T I O N',
				fontSize: 18,
				bold: true,
				alignment: 'center',
				// marginTop: 70,
				margin: [50, 70, 50, 0],
				decoration: 'underline',
			},
			{
				text: 'TO WHOM IT MAY CONCERN:',
				fontSize: 12,
				bold: true,
				// marginTop: 40
				margin: [50, 40, 50, 0],
				alignment: 'justify'
			},
			{
				text: [
					{ text: '\u200B          THIS IS TO CERTIFY that ' },
					{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
					{ text: ' is a bonafide student of the University, during the ' },
					{ text: SpesSY + '.', decoration: 'underline' }
				], fontSize: 11,
				// marginTop: 15
				margin: [50, 15, 50, 0],
				alignment: 'justify'
			},
			{
				text: [
					{ text: '\u200B          This is to certify further that his/her average passing grade/rating during the period is ' },
					{ text: weightedAverage + '%.', bold: true, decoration: 'underline' },
				], fontSize: 11,
				// marginTop: 15
				margin: [50, 15, 50, 0],
				alignment: 'justify'
			},
			{
				text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
				fontSize: 11,
				// marginTop: 15
				margin: [50, 15, 50, 0],
				alignment: 'justify'
			},
			{
				text: 'LIZA M. EMPEDRAD, Ph.D.',
				fontSize: 11,
				bold: true,
				alignment: 'right',
				// marginTop: 40

				margin: [50, 50, 50, 0]
			},
			{
				text: 'University Registrar',
				fontSize: 11,
				alignment: 'right',
				margin: [50, 0, 50, 0]
			},
			{
				text: 'University Seal',
				fontSize: 9,
				alignment: 'left',
				// marginTop: 250
				margin: [50, 220, 50, 0]
			},
			{
				text: '/vcc 2023',
				fontSize: 9,
				alignment: 'left',
				// marginTop: 2
				margin: [50, 2, 50, 0]
			}
		];

		var footer = {
		};

		var dd = {
			background: function (currentPage, pageSize) {
				// console.log(`Current page: ${currentPage}`);
				const pageWidth = pageSize.width;
				const pageHeight = pageSize.height;
				const watermarkWidth = 612;
				const watermarkHeight = 936;
				const pageRatio = pageWidth / pageHeight;
				const watermarkRatio = watermarkWidth / watermarkHeight;
				let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

				if (watermarkRatio > pageRatio) {
					scale = pageWidth / watermarkWidth;
				} else {
					scale = pageHeight / watermarkHeight;
				}

				scaledWatermarkWidth = watermarkWidth * scale;
				scaledWatermarkHeight = watermarkHeight * scale;
				x = (pageWidth - scaledWatermarkWidth) / 2;
				y = (pageHeight - scaledWatermarkHeight) / 2;

				return {
					image: "data:image/png;base64," + HeaderFooterPicture,
					width: scaledWatermarkWidth,
					height: scaledWatermarkHeight,
					opacity: 30,
					absolutePosition: { x: x, y: y }
				};

			},
			// pageSize: {
			// 	width: 612, // 8.5 inches * 72 = 612
			// 	height: 936 // 14 inches * 72 = 792
			// },
			pageSize: 'LETTER',
			pageOrientation: 'portrait',
			pageMargins: [40, 100, 40, 100],
			header: header,
			content: content,
			footer: footer
		};
		if (isNaN(weightedAverage)) {
			this.global.swalAlert('Not Enrolled in ' + SpesSY, '', 'warning')
		}
		else {
			pdfMake.createPdf(dd).open();
		}

	}
	generateNSTP() {

		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		var LAST = this.lastName
		var MIDDLE = this.middleName
		var FIRST = this.firstName

		const nstp = this.GetGradRec.map(obj => obj.nstpSerialNo);
		// console.log(nstp)

		if (nstp == null || nstp == '') {
			this.global.swalAlert('No nstp record found', 'Please add record to generate', 'warning')
		}
		else {
			var HeaderFooterPicture = this.images.header
			var headerlogo = this.images.header
			var header = {


			};

			// {
			// 	image: "data:image/png;base64," + headerlogo,
			// 	width: 70,
			// 	opacity: 1,
			// 	absolutePosition: { x: 155, y: 40 },
			// },
			var content = [
				{

				},
				{
					text: 'C E R T I F I C A T I O N',
					fontSize: 18,
					bold: true,
					alignment: 'center',
					// marginTop: 70,
					margin: [50, 70, 50, 0],
					decoration: 'underline',
				},
				{
					text: 'TO WHOM IT MAY CONCERN:',
					fontSize: 12,
					bold: true,
					// marginTop: 40
					margin: [50, 40, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          THIS IS TO CERTIFY that ' },
						{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
						{ text: ' has completed the National Service Training Program (NSTP) at University of Saint Louis with NSTP Serial No. ' + nstp },
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
					fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.',
					fontSize: 11,
					bold: true,
					alignment: 'right',
					// marginTop: 40

					margin: [50, 50, 50, 0]
				},
				{
					text: 'University Registrar',
					fontSize: 11,
					alignment: 'right',
					margin: [50, 0, 50, 0]
				},
				{
					text: 'University Seal',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 250
					margin: [50, 220, 50, 0]
				},
				{
					text: '/vcc 2023',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 2
					margin: [50, 2, 50, 0]
				}
			];

			var footer = {
			};

			var dd = {
				background: function (currentPage, pageSize) {
					// console.log(`Current page: ${currentPage}`);
					const pageWidth = pageSize.width;
					const pageHeight = pageSize.height;
					const watermarkWidth = 612;
					const watermarkHeight = 936;
					const pageRatio = pageWidth / pageHeight;
					const watermarkRatio = watermarkWidth / watermarkHeight;
					let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					if (watermarkRatio > pageRatio) {
						scale = pageWidth / watermarkWidth;
					} else {
						scale = pageHeight / watermarkHeight;
					}

					scaledWatermarkWidth = watermarkWidth * scale;
					scaledWatermarkHeight = watermarkHeight * scale;
					x = (pageWidth - scaledWatermarkWidth) / 2;
					y = (pageHeight - scaledWatermarkHeight) / 2;

					return {
						image: "data:image/png;base64," + HeaderFooterPicture,
						width: scaledWatermarkWidth,
						height: scaledWatermarkHeight,
						opacity: 30,
						absolutePosition: { x: x, y: y }
					};

				},
				// pageSize: {
				// 	width: 612, // 8.5 inches * 72 = 612
				// 	height: 936 // 14 inches * 72 = 792
				// },
				pageSize: 'LETTER',
				pageOrientation: 'portrait',
				pageMargins: [40, 100, 40, 100],
				header: header,
				content: content,
				footer: footer
			};

			pdfMake.createPdf(dd).open();
		}

	}
	generateRegistrar() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}


		if (this.registrarCertificateData.length == 0) {
			this.global.swalAlert('Not enrolled in ' + this.convertNumberToLetter(this.global.syDisplay(this.schoolYear)), '', 'warning')
		}
		else {
			// #region
			var logo = this.images.logo
			// #endregion

			// this.logo
			// var x in this.registrarCertificateData
			// lname
			// fname
			// mname
			// this.convertNumberToLetter
			var header = [
				{
					image: "data:image/png;base64," + logo,
					width: 70,
					opacity: 1,
					absolutePosition: { x: 155, y: 40 },
				},
				{
					text: 'University of Saint Louis', bold: true, fontSize: 15, alignment: 'left',
					absolutePosition: { x: 225, y: 46 }
				},
				{
					text: 'Mabini Street, Tuguegarao City Cagayan, Philippines\nTel. No. (078) 844-1872/73\nFax No. (078) 844-0889', italics: true, fontSize: 10, alignment: 'left',
					absolutePosition: { x: 225, y: 63 }
				},
				{
					canvas:
						[
							{
								type: 'line',
								x1: 43, y1: 117,
								x2: 570, y2: 117,
								lineWidth: 2,
							},
						]
				},
				{
					text: 'OFFICE OF THE UNIVERSITY REGISTRAR \nCERTIFICATION', italics: false, fontSize: 12, alignment: 'center', bold: true,
					absolutePosition: { x: 0, y: 125 }
				}
			]


			var tableContent = [
				[
					{ text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					{ text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					{ text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					{ text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }
				]
			]

			for (var x in this.registrarCertificateData) {
				tableContent.push(
					[
						//@ts-ignore
						{ text: this.registrarCertificateData[x].subjectID, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
						//@ts-ignore
						{ text: this.registrarCertificateData[x].subjectTitle, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
						//@ts-ignore
						{ text: this.registrarCertificateData[x].units, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 },
						//@ts-ignore
						{ text: this.registrarCertificateData[x].grade, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 }
					]
				)
			}

			tableContent.push(
				[
					//@ts-ignore
					{ text: '', border: [false, true, false, false], colSpan: 4 }
				],
				[
					//@ts-ignore
					{ text: '', border: [false, false, false, false], colSpan: 4 }
				],
				[
					//@ts-ignore
					{ text: '', border: [false, false, false, false], colSpan: 4 }
				],
				[

					{
						//@ts-ignore
						text: '\u200B           This certification is issued this ' + this.today + ' upon the request of ' + gender + this.lastName + ' ' + this.firstName + ' ' + this.middleName + ' for ' + this.purpose, italics: false, fontSize: 11, colSpan: 4, border: [false, false, false, false],
					},
				]
			)


			var content = [
				{
					text: 'TO WHOM MAY IT CONCERN', italics: false, fontSize: 11, alignment: 'left',
					absolutePosition: { x: 45, y: 165 }
				},
				{
					text: '\u200B           This is to certify that ' + gender + this.lastName + ' ' + this.firstName + ' ' + this.middleName + ' took the following subject/s to wit: ', italics: false, fontSize: 11, alignment: 'left',
					absolutePosition: { x: 45, y: 185 }
				},
				{
					text: this.convertNumberToLetter(this.global.syDisplay(this.schoolYear)), italics: true, fontSize: 11, alignment: 'left', bold: true,
					absolutePosition: { x: 45, y: 215 }
				},
				{
					table: {
						layout: {
							margin: [10, 10, 10, 10] // specify the margins as an array of [left, top, right, bottom]
						},
						widths: ['15%', '70%', '*', '*'],
						body: tableContent
					},
					absolutePosition: { x: 41, y: 230 },
				}
			]

			var footer = [
				{
					text: 'Very truly yours,', italics: false, fontSize: 11, alignment: 'left',
					absolutePosition: { x: 430, y: 32 }
				},
				{
					text: 'University Seal', italics: false, fontSize: 11, alignment: 'left',
					absolutePosition: { x: 45, y: 77 }
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.', italics: false, fontSize: 11, alignment: 'left', bold: true,
					absolutePosition: { x: 430, y: 77 }
				},
				{
					text: 'University Registrar', italics: false, fontSize: 11, alignment: 'left',
					absolutePosition: { x: 443, y: 88 }
				},
			]

			var dd = {
				pageSize: 'LETTER',
				pageOrientation: 'portrait', pageMargins: [40, 170, 40, 170],
				header: header,
				content: content,
				footer: footer
				// this.lastName + ' ' + this.firstName + '' + this.middleName
			}
			// pdfMake.createPdf(dd).download('Registrar Certificate ' + this.lastName + ', ' + this.firstName + '.pdf');
			pdfMake.createPdf(dd).open();
		}

	}
	// generateROTC() {
	// 	var LAST = this.lastName
	// 	var MIDDLE = this.middleName
	// 	var FIRST = this.firstName

	// 	const nstp = this.GetGradRec.map(obj => obj.nstpSerialNo);
	// 	console.log(nstp)

	// 	var HeaderFooterPicture = this.images.header
	// 	var headerlogo = this.images.header
	// 	var header = {


	// 	};

	// 	var tableContent = [
	// 		[
	// 			{ text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
	// 			{ text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
	// 			{ text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
	// 			{ text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }
	// 		]
	// 	]
	// 	// {
	// 	// 	image: "data:image/png;base64," + headerlogo,
	// 	// 	width: 70,
	// 	// 	opacity: 1,
	// 	// 	absolutePosition: { x: 155, y: 40 },
	// 	// },
	// 	var content = [
	// 		{

	// 		},
	// 		{
	// 			text: 'C E R T I F I C A T I O N',
	// 			fontSize: 18,
	// 			bold: true,
	// 			alignment: 'center',
	// 			// marginTop: 70,
	// 			margin: [50, 70, 50, 0],
	// 			decoration: 'underline',
	// 		},
	// 		{
	// 			text: 'TO WHOM IT MAY CONCERN:',
	// 			fontSize: 12,
	// 			bold: true,
	// 			// marginTop: 40
	// 			margin: [50, 40, 50, 0],
	// 			alignment: 'justify'
	// 		},
	// 		{
	// 			text: [
	// 				{ text: '\u200B          THIS IS TO CERTIFY that ' },
	// 				{ text: 'MR./MRS. ' + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
	// 				{ text: ' has completed the National Service Training Program (NSTP) at University of Saint Louis with NSTP Serial No. ' + nstp },
	// 			], fontSize: 11,
	// 			// marginTop: 15
	// 			margin: [50, 15, 50, 0],
	// 			alignment: 'justify'
	// 		},
	// 		{
	// 			table: {
	// 				layout: {
	// 					margin: [10, 10, 10, 10] // specify the margins as an array of [left, top, right, bottom]
	// 				},
	// 				widths: ['15%', '70%', '*', '*'],
	// 				body: tableContent
	// 			},
	// 			absolutePosition: { x: 41, y: 230 },
	// 		},
	// 		{
	// 			text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of MR./MRS. ' + LAST + ' ' + 'for ' + this.purposeGrad,
	// 			fontSize: 11,
	// 			// marginTop: 15
	// 			margin: [50, 15, 50, 0],
	// 			alignment: 'justify'
	// 		},
	// 		{
	// 			text: 'LIZA M. EMPEDRAD, Ph.D.',
	// 			fontSize: 11,
	// 			bold: true,
	// 			alignment: 'right',
	// 			// marginTop: 40

	// 			margin: [50, 50, 50, 0]
	// 		},
	// 		{
	// 			text: 'University Registrar',
	// 			fontSize: 11,
	// 			alignment: 'right',
	// 			margin: [50, 0, 50, 0]
	// 		},
	// 		{
	// 			text: 'University Seal',
	// 			fontSize: 9,
	// 			alignment: 'left',
	// 			// marginTop: 250
	// 			margin: [50, 220, 50, 0]
	// 		},
	// 		{
	// 			text: '/vcc 2023',
	// 			fontSize: 9,
	// 			alignment: 'left',
	// 			// marginTop: 2
	// 			margin: [50, 2, 50, 0]
	// 		}
	// 	];

	// 	var footer = {
	// 	};

	// 	var dd = {
	// 		background: function (currentPage, pageSize) {
	// 			// console.log(`Current page: ${currentPage}`);
	// 			const pageWidth = pageSize.width;
	// 			const pageHeight = pageSize.height;
	// 			const watermarkWidth = 612;
	// 			const watermarkHeight = 936;
	// 			const pageRatio = pageWidth / pageHeight;
	// 			const watermarkRatio = watermarkWidth / watermarkHeight;
	// 			let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

	// 			if (watermarkRatio > pageRatio) {
	// 				scale = pageWidth / watermarkWidth;
	// 			} else {
	// 				scale = pageHeight / watermarkHeight;
	// 			}

	// 			scaledWatermarkWidth = watermarkWidth * scale;
	// 			scaledWatermarkHeight = watermarkHeight * scale;
	// 			x = (pageWidth - scaledWatermarkWidth) / 2;
	// 			y = (pageHeight - scaledWatermarkHeight) / 2;

	// 			return {
	// 				image: "data:image/png;base64," + HeaderFooterPicture,
	// 				width: scaledWatermarkWidth,
	// 				height: scaledWatermarkHeight,
	// 				opacity: 30,
	// 				absolutePosition: { x: x, y: y }
	// 			};

	// 		},
	// 		// pageSize: {
	// 		// 	width: 612, // 8.5 inches * 72 = 612
	// 		// 	height: 936 // 14 inches * 72 = 792
	// 		// },
	// 		pageSize: 'LETTER',
	// 		pageOrientation: 'portrait',
	// 		pageMargins: [40, 100, 40, 100],
	// 		header: header,
	// 		content: content,
	// 		footer: footer
	// 	};

	// 	pdfMake.createPdf(dd).open();
	// }
	generateWithHonors() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		if (this.degreeTitle == null || this.degreeTitle == '') {
			this.global.swalAlert('No graduation record', 'Please add record to generate', 'warning')
		}
		else {
			this.DegreeLevel = 1
			var LAST = this.lastName
			var MIDDLE = this.middleName
			var FIRST = this.firstName
			for (var x in this.GetGradRec) {
				if (this.DegreeLevel == 1 && this.GetGradRec[x].otrType == 1) {
					var degreeTitle = this.GetGradRec[x].degreeTitle
					var dateofGraduation = this.GetGradRec[x].dateofGraduation
					this.dateformat(dateofGraduation);
					var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
					var CodeCourse = this.GetGradRec[x].courseCode
					var AwardsReceive = this.GetGradRec[x].awardsReceived
				}
			}
			var HeaderFooterPicture = this.images.header
			var headerlogo = this.images.header
			var header = {

			};

			// {
			// 	image: "data:image/png;base64," + headerlogo,
			// 	width: 70,
			// 	opacity: 1,
			// 	absolutePosition: { x: 155, y: 40 },
			// },
			var content = [
				{

				},
				{
					text: 'C E R T I F I C A T I O N',
					fontSize: 18,
					bold: true,
					alignment: 'center',
					// marginTop: 70,
					margin: [50, 70, 50, 0],
					decoration: 'underline',
				},
				{
					text: 'TO WHOM IT MAY CONCERN:',
					fontSize: 12,
					bold: true,
					// marginTop: 40
					margin: [50, 40, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          THIS IS TO CERTIFY that ' },
						{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
						{ text: ' is a graduate of the University in ' + degreeTitle + ' (' + CodeCourse + ') on ' + this.dateofGradFormatted + ' with Special Order ' + specialOrderNumber }
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          This to further attest that he graduated as ' },
						{ text: AwardsReceive + '.', bold: true },
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
					fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.',
					fontSize: 11,
					bold: true,
					alignment: 'right',
					// marginTop: 40

					margin: [50, 50, 50, 0]
				},
				{
					text: 'University Registrar',
					fontSize: 11,
					alignment: 'right',
					margin: [50, 0, 50, 0]
				},
				{
					text: 'University Seal',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 250
					margin: [50, 210, 50, 0]
				},
				{
					text: '/vcc 2023',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 2
					margin: [50, 2, 50, 0]
				}
			];

			var footer = {
			};

			var dd = {
				background: function (currentPage, pageSize) {
					// console.log(`Current page: ${currentPage}`);
					const pageWidth = pageSize.width;
					const pageHeight = pageSize.height;
					const watermarkWidth = 612;
					const watermarkHeight = 936;
					const pageRatio = pageWidth / pageHeight;
					const watermarkRatio = watermarkWidth / watermarkHeight;
					let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					if (watermarkRatio > pageRatio) {
						scale = pageWidth / watermarkWidth;
					} else {
						scale = pageHeight / watermarkHeight;
					}

					scaledWatermarkWidth = watermarkWidth * scale;
					scaledWatermarkHeight = watermarkHeight * scale;
					x = (pageWidth - scaledWatermarkWidth) / 2;
					y = (pageHeight - scaledWatermarkHeight) / 2;

					return {
						image: "data:image/png;base64," + HeaderFooterPicture,
						width: scaledWatermarkWidth,
						height: scaledWatermarkHeight,
						opacity: 30,
						absolutePosition: { x: x, y: y }
					};

				},
				// pageSize: {
				// 	width: 612, // 8.5 inches * 72 = 612
				// 	height: 936 // 14 inches * 72 = 792
				// },
				pageSize: 'LETTER',
				pageOrientation: 'portrait',
				pageMargins: [40, 100, 40, 100],
				header: header,
				content: content,
				footer: footer
			};
			if (AwardsReceive == 'Not Applicable') {
				this.global.swalAlert('No Awards for this student', '', 'warning')
			}
			else {
				pdfMake.createPdf(dd).open();
			}
		}


	}
	generateEnrolment() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		var SpesSY = '';
		var date = this.schoolYear;
		var y1 = date.substring(0, 4);
		var y2 = parseInt(y1) + 1;
		var a = y1.toString() + "-" + y2.toString();
		var term = date.substring(6, 7);
		var c = "";

		if (term == '1')
			c = "1st Semester";
		else if (term == '2')
			c = "2nd Semester";
		else if (term == '3')
			c = "Summer";

		SpesSY = c + ' School Year ' + a;

		var data = this.registrarCertificateData
		// console.log('data', data)
		if (data.length == 0) {
			this.global.swalAlert('Not Enrolled in ' + SpesSY, '', 'warning')
		}
		else {
			var LAST = this.lastName
			var MIDDLE = this.middleName
			var FIRST = this.firstName


			// console.log(this.EnrolledCourse[0].programTitle)
			// console.log(this.EnrolledCourse[0].courseCode)

			var ProgTitle = this.EnrolledCourse[0].programTitle
			var courseCode = this.EnrolledCourse[0].courseCode

			var HeaderFooterPicture = this.images.header
			var headerlogo = this.images.header
			var header = {

			};

			// {
			// 	image: "data:image/png;base64," + headerlogo,
			// 	width: 70,
			// 	opacity: 1,
			// 	absolutePosition: { x: 155, y: 40 },
			// },
			var content = [
				{

				},
				{
					text: 'C E R T I F I C A T I O N',
					fontSize: 18,
					bold: true,
					alignment: 'center',
					// marginTop: 70,
					margin: [50, 70, 50, 0],
					decoration: 'underline',
				},
				{
					text: 'TO WHOM IT MAY CONCERN:',
					fontSize: 12,
					bold: true,
					// marginTop: 40
					margin: [50, 40, 50, 0],
					alignment: 'justify'
				},
				{
					text: [
						{ text: '\u200B          THIS IS TO CERTIFY that ' },
						{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
						{ text: ' was officially enrolled in the University in ' + ProgTitle + ' (' + courseCode + ') for ' + SpesSY },
					], fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
					fontSize: 11,
					// marginTop: 15
					margin: [50, 15, 50, 0],
					alignment: 'justify'
				},
				{
					text: 'LIZA M. EMPEDRAD, Ph.D.',
					fontSize: 11,
					bold: true,
					alignment: 'right',
					// marginTop: 40

					margin: [50, 50, 50, 0]
				},
				{
					text: 'University Registrar',
					fontSize: 11,
					alignment: 'right',
					margin: [50, 0, 50, 0]
				},
				{
					text: 'University Seal',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 250
					margin: [50, 220, 50, 0]
				},
				{
					text: '/vcc 2023',
					fontSize: 9,
					alignment: 'left',
					// marginTop: 2
					margin: [50, 2, 50, 0]
				}
			];

			var footer = {
			};

			var dd = {
				background: function (currentPage, pageSize) {
					// console.log(`Current page: ${currentPage}`);
					const pageWidth = pageSize.width;
					const pageHeight = pageSize.height;
					const watermarkWidth = 612;
					const watermarkHeight = 936;
					const pageRatio = pageWidth / pageHeight;
					const watermarkRatio = watermarkWidth / watermarkHeight;
					let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					if (watermarkRatio > pageRatio) {
						scale = pageWidth / watermarkWidth;
					} else {
						scale = pageHeight / watermarkHeight;
					}

					scaledWatermarkWidth = watermarkWidth * scale;
					scaledWatermarkHeight = watermarkHeight * scale;
					x = (pageWidth - scaledWatermarkWidth) / 2;
					y = (pageHeight - scaledWatermarkHeight) / 2;

					return {
						image: "data:image/png;base64," + HeaderFooterPicture,
						width: scaledWatermarkWidth,
						height: scaledWatermarkHeight,
						opacity: 30,
						absolutePosition: { x: x, y: y }
					};

				},
				// pageSize: {
				// 	width: 612, // 8.5 inches * 72 = 612
				// 	height: 936 // 14 inches * 72 = 792
				// },
				pageSize: 'LETTER',
				pageOrientation: 'portrait',
				pageMargins: [40, 100, 40, 100],
				header: header,
				content: content,
				footer: footer
			};

			pdfMake.createPdf(dd).open();
		}

	}
	async generateGrades() {
		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}

		this.DegreeLevelT


		if (this.DegreeLevelT == null || this.DegreeLevelT == '')
			this.global.swalAlert('Please Select Degree', '', 'warning')

		try {


			const response = await this.api.getOTRDetails(this.idNumber, this.DegreeLevelT).toPromise();
			const result = response.json();
			this.OTRDetails2 = result.data
			if (result != '') {
				// console.log(this.OTRDetails)

				const groupedBySchoolYear = this.OTRDetails2.reduce((acc, item) => {
					if (acc[item.schoolYear]) {
						acc[item.schoolYear].push(item);
					} else {
						acc[item.schoolYear] = [item];
					}
					return acc;
				}, {});

				this.truecopy = Object.values(groupedBySchoolYear);

				for (var x in this.GetGradRec) {
					if (this.DegreeLevelT == 1 && this.GetGradRec[x].otrType == 1) {
						var degreeTitle = this.GetGradRec[x].degreeTitle
						var dateofGraduation = this.GetGradRec[x].dateofGraduation
						this.dateformat(dateofGraduation);
						var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
						var CodeCourse = this.GetGradRec[x].courseCode
					}
					if (this.DegreeLevelT == 2 && this.GetGradRec[x].otrType == 2) {
						var degreeTitle = this.GetGradRec[x].degreeTitle
						var dateofGraduation = this.GetGradRec[x].dateofGraduation
						this.dateformat(dateofGraduation);
						var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
						var CodeCourse = this.GetGradRec[x].courseCode
					}
					if (this.DegreeLevelT == 3 && this.GetGradRec[x].otrType == 3) {
						var degreeTitle = this.GetGradRec[x].degreeTitle
						var dateofGraduation = this.GetGradRec[x].dateofGraduation
						this.dateformat(dateofGraduation);
						var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
						var CodeCourse = this.GetGradRec[x].courseCode
					}
				}


				var footer
				var LAST = this.lastName
				var MIDDLE = this.middleName
				var FIRST = this.firstName

				var HeaderFooterPicture = this.images.header
				var headerlogo = this.images.header
				var array = this.truecopy
				// console.log('arr', array)

				// this.DegreeLevel = null
				// if(this.DegreeLevel == null){
				// 	this.global.swalAlert('Please Select Degree', '', 'warning')
				// }
				function fillsemestertableTrue() {
					// console.log(array)
					// var current_page = Array.from({length: 10}, (_, index) => index + 1);
					// console.log('PAGE: ',current_page[0])
					var dataRow = [];
					var semTableArray = [];
					var companyNames = []
					// console.log("groupedSy: ", array)
					var res = [];

					// { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }

					var columns = [
						{ text: 'COURSE NO', bold: true, border: [true, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'DESCRIPTIVE TITLE', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'GRADES', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'UNITS', bold: true, border: [false, true, true, true], fillColor: '#dddddd', alignment: 'center' }
					];


					res.push(columns);
					var lastCompany = ''
					if (array.length != 0) {
						companyNames = get_company_names_per_group(array)

						for (var i = 0; i < array.length; i++) {

							var array_sy = '';
							var date = array[i][0].schoolYear;
							var y1 = date.substring(0, 4);
							var y2 = parseInt(y1) + 1;
							var a = y1.toString() + " - " + y2.toString();
							var term = date.substring(6, 7);
							var c = "";

							if (term == '1')
								c = "First Semester";
							else if (term == '2')
								c = "Second Semester";
							else if (term == '3')
								c = "Summer";

							array_sy = c + ' ' + a;
							// console.log('sy', array_sy)
							if (companyNames[i].companyName != lastCompany)
								res.push([{ text: companyNames[i].companyName, colSpan: 4, bold: true, }, {}, {}, {}]);

							lastCompany = companyNames[i].companyName
							res.push([{ text: array_sy, colSpan: 4, margin: [99, 0, 0, 0], bold: true, }, {}, {}, {}]);
							var tempres = [];
							for (var j = 0; j < array[i].length; j++) {
								dataRow = []

								dataRow.push(
									{ text: array[i][j].subjectId, border: [false, false, true, false] },
									{ text: array[i][j].subjectTitle, border: [false, false, true, false] },
									{ text: array[i][j].grade, alignment: 'center', border: [false, false, true, false] },
									{ text: array[i][j].units, alignment: 'center', border: [false, false, false, false] },

								);




								// var currentpage = 1
								// var pagecount = 1
								// Check if it is the last row and apply a border to all columns
								// if (j === array[i].length - 1) {
								// 	dataRow.forEach(function (cell) {
								// 		// console.log('Cell: ',cell)
								// 		// cell.border[2] = true; // Apply border to the right side
								// 		cell.border[3] = true; // Apply border to the bottom side

								// 	});
								// }

								tempres.push(dataRow);
								// console.log(dataRow[3])
							}
							res.push(
								[
									{
										table: {
											widths: [90, 345, 40, 40],
											body:
												tempres

										}, fontSize: 9,
										layout: {
											hLineWidth: function (i, node) { return 0.5; },
											vLineWidth: function (i, node) { return 0.5; },
										}
										, colSpan: 4
									},
									{ text: "" },
									{ text: "" },
									{ text: "" },
								]
							)


						}

					}

					else {

						dataRow = []
						dataRow.push(

							{ text: '' },
							{ text: '' },
							{ text: '', alignment: 'center' },
							{ text: '', alignment: 'center' }
						);
						res.push(dataRow)
					}


					// var lastRow = res[res.length - 1];
					// for (var k = 0; k < lastRow.length; k++) {
					// 	lastRow[k].border[2] = true;

					// }

					// res.push([
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' }
					//   ]);
					return res;
				}

				function get_company_names_per_group(array) {
					var res = []
					for (let index = 0; index < array.length; index++) {
						res.push({
							companyName: array[index][0].companyName
						})
					}
					return res;
				}
				var dd = {
					background: function (currentPage, pageSize) {
						// console.log(`Current page: ${currentPage}`);
						const pageWidth = pageSize.width;
						const pageHeight = pageSize.height;
						const watermarkWidth = 612;
						const watermarkHeight = 936;
						const pageRatio = pageWidth / pageHeight;
						const watermarkRatio = watermarkWidth / watermarkHeight;
						let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

						if (watermarkRatio > pageRatio) {
							scale = pageWidth / watermarkWidth;
						} else {
							scale = pageHeight / watermarkHeight;
						}

						scaledWatermarkWidth = watermarkWidth * scale;
						scaledWatermarkHeight = watermarkHeight * scale;
						x = (pageWidth - scaledWatermarkWidth) / 2;
						y = (pageHeight - scaledWatermarkHeight) / 2;

						return {
							image: "data:image/png;base64," + HeaderFooterPicture,
							width: scaledWatermarkWidth,
							height: scaledWatermarkHeight,
							opacity: 30,
							absolutePosition: { x: x, y: y }
						};

					},

					// 		
					pageSize: {
						width: 612, // 8.5 inches * 72 = 612
						height: 936 // 14 inches * 72 = 792
					},

					// watermark: { text: 'USL USL USL USL USL USL USL USL USL USL USL USL USL USL USL ', color: 'gray', opacity: 0.3, bold: true, italics: true },
					content: [
						{
							stack: [
								{
									canvas:
										[
											{
												type: 'line',
												x1: 0, y1: -10,
												x2: 550, y2: -10,
												lineWidth: 2,
											},
										]
								},

								{
									text: [
										{ text: 'TRUE COPY OF SCHOLASTIC RECORD' },
									], fontSize: 11,
									// marginTop: 15
									bold: true,
									margin: [50, 5, 50, 0],
									alignment: 'center'
								},
								// Existing content...
								{
									// ...
									// New table content
									table: {
										widths: ['auto', '89%'],
										body: [
											[{ text: 'NAME', fontSize: 9, bold: true, margin: [31.5, 0] }, { text: LAST + ' ' + FIRST + ' ' + MIDDLE, fontSize: 9, bold: true, }],
											[{ text: 'COURSE', fontSize: 9, bold: true, margin: [26.5, 0] }, { text: degreeTitle, fontSize: 9, bold: true }],
										],
										layout: {
											hLineWidth: function (i, node) {
												if (i === 0 || i === node.table.body.length) {
													return 0;  // Hide horizontal lines at table's top and bottom
												}
												return i === 1 ? 0.5 : 0;  // Customize the line width for the first data row
											},
											vLineWidth: function (i) {
												return 0;  // Hide vertical lines
											},
											paddingLeft: function (i) {
												return i === 0 ? 0 : 5;  // Add padding to the left of the "Name" column
											},
											paddingRight: function (i) {
												return i === 0 ? 5 : 0;  // Add padding to the right of the "Name" column
											}
										}
									},
									margin: [0, 10, 40, 0]
								},
							]
						},
						{
							stack: [
								{ text: '\n' },
								{
									style: 'tableExample',
									table: {
										headerRows: 1,
										pageBreak: 'before',
										dontBreakRows: false,
										keepWithHeaderRows: 1,
										widths: [90, 345, 40, 40],
										body: fillsemestertableTrue(),
									},
									fontSize: 9,
									layout: {
										paddingTop: function (i, node) {
											return 0.2;
											//return (i === node.table.widths.length - 1) ? 5 : 5;
										},
										paddingBottom: function (i, node) {
											return 0.2;
											//return (i === node.table.widths.length - 1) ? 5 : 5;
										},
										hLineWidth: function (i, node) { return 0.5; },
										vLineWidth: function (i, node) { return 0.5; },
									}
								},

								// { text: '\n', margin: [0, 0, 1, 0] } // Add a margin to the element after the table
							]

						},
						// {
						// 	text: [
						// 		{
						// 			text: 'LIZA M. EMPEDRAD, Ph.D.',
						// 			fontSize: 11,
						// 			bold: true,
						// 			alignment: 'right',
						// 			// marginTop: 40

						// 			margin: [50, 50, 50, 0]
						// 		},
						// 		{
						// 			text: 'LIZA M. EMPEDRAD, Ph.D.',
						// 			fontSize: 11,
						// 			bold: true,
						// 			alignment: 'left',
						// 			// marginTop: 40

						// 			margin: [50, 50, 50, 0]
						// 		},

						// 	], fontSize: 11,
						// 	// marginTop: 15
						// 	margin: [50, 15, 50, 0],
						// 	alignment: 'justify'
						// },
						{ text: "------------------------------------------end of transcript------------------------------------------", alignment: 'center', fontSize: 9, margin: [0, 8, 0, 0] },

						{
							text: [
								{
									text: 'University Seal                                                                                                                                            ',
									fontSize: 9,
									alignment: 'left',
									// marginTop: 40

									margin: [0, 30, 0, 0]
								},
								{
									text: 'LIZA M. EMPEDRAD, Ph.D.',
									fontSize: 11,
									bold: true,
									alignment: 'right',
									// marginTop: 40

									margin: [0, 30, 0, 0]
								}
							],

							marginTop: 30


						},
						{
							text: [
								{
									text: '/vcc 2023                                                                                                                                                              ',
									fontSize: 9,
									alignment: 'left',
									// marginTop: 40
								},
								{
									text: 'University Registrar',
									fontSize: 11,
									alignment: 'right',
									// marginTop: 40
								}
							],
						},
						// {
						// 	text: '/vcc 2023                                                                                                                                University Registrar',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	margin: [0, 0, 0, 0]
						// },
						// {
						// 	text: 'University Seal',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	// marginTop: 250
						// 	margin: [0, 0, 0, 0]
						// },
						// {
						// 	text: '/vcc 2023',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	// marginTop: 2
						// 	margin: [0, 2, 0, 0]
						// }

					],
					pageMargins: [30, 150, 30, 140],
					// pageMargins: [ 30,300, 30, 140 ],
					// watermark:{text:'UNIVERSITY\nof SAINT LOUIS\nTUGUEGARAO',bold:true,fontSize:16},


				}


				var degreelevvv
				if (this.OTRDetails2 == null || this.OTRDetails2 == '' || this.OTRDetails2 == 0) {

					if (this.DegreeLevelT == 1) {
						degreelevvv = 'College'
					}
					if (this.DegreeLevelT == 2) {
						degreelevvv = 'Masteral'
					}
					if (this.DegreeLevelT == 3) {
						degreelevvv = 'Doctoral'
					}

					this.global.swalAlert('Not enrolled for ' + degreelevvv, '', 'warning')
				}
				else {

					pdfMake.createPdf(dd).open();

				}


				// console.log('Grouped: ', this.groupedSy);
			}
		} catch (error) {
			console.error(error);
		}



	}

	async generateROTC() {

		if (this.gender.toLowerCase() === 'male' || this.gender.toLowerCase() === 'm') {
			// Your code here for the true condition
			var gender = 'MR. '
		}

		if (this.gender.toLowerCase() === 'female' || this.gender.toLowerCase() === 'f') {
			// Your code here for the true condition

			if (this.civilStatus.toLowerCase() == 'single' || this.civilStatus.toLowerCase() == 's') {
				var gender = 'MS. '
			}

			if (this.civilStatus.toLowerCase() == 'married' || this.civilStatus.toLowerCase() == 'm') {
				var gender = 'MRS. '
			}

		}


		var array = this.groupedSy



		var groupedByROTC = this.OTRDetails.reduce((acc, item) => {
			if (item.subjectId.includes("ROTC")) { // Add a filter for "ROTC" in item.subjectId
				if (acc[item.subjectId]) {
					acc[item.subjectId].push(item);
				} else {
					acc[item.subjectId] = [item];
				}
			}
			return acc;
		}, {});

		// console.log('GGSIMADAA', groupedByROTC)

		// console.log('arr', array)


		// for (var subjectId in groupedByROTC) {
		// 	var items = groupedByROTC[subjectId];
		// 	var subjectTitle = items[0].subjectTitle; // Assuming subjectTitle is the same for all items in the same subjectId group
		// 	var grade = items[0].grade; // Assuming grade is the same for all items in the same subjectId group
		// 	var unit = items[0].units; // Assuming unit is the same for all items in the same subjectId group

		// 	console.log("Subject ID:", subjectId);
		// 	console.log("Subject Title:", subjectTitle);
		// 	console.log("Grade:", grade);
		// 	console.log("Unit:", unit);
		// 	console.log("---");
		// }
		// console.log('ROTC: ', groupedByROTC)
		const response = await this.api.getOTRDetails(this.idNumber, this.DegreeLevelROTC).toPromise();
		const result = response.json();
		this.OTRDetails2 = result.data
		if (result != '') {
			// console.log(this.OTRDetails)

			const groupedBySchoolYear = this.OTRDetails2.reduce((acc, item) => {
				if (acc[item.schoolYear]) {
					acc[item.schoolYear].push(item);
				} else {
					acc[item.schoolYear] = [item];
				}
				return acc;
			}, {});

			this.truecopy = Object.values(groupedBySchoolYear);

			try {



				// console.log(array)
				var LAST = this.lastName
				var MIDDLE = this.middleName
				var FIRST = this.firstName

				var HeaderFooterPicture = this.images.header
				var header
				var footer


				function fillROTC() {
					// console.log(array)
					// var current_page = Array.from({length: 10}, (_, index) => index + 1);
					// console.log('PAGE: ',current_page[0])
					var dataRow = [];
					var semTableArray = [];
					var companyNames = []
					// console.log("groupedSy: ", array)
					var res = [];

					// { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }

					var columns = [
						{ text: 'COURSE NO', bold: true, border: [true, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'DESCRIPTIVE TITLE', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'GRADES', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'UNITS', bold: true, border: [false, true, true, true], fillColor: '#dddddd', alignment: 'center' }
					];


					res.push(columns);
					var lastCompany = ''
					if (array.length != 0) {


						for (const key in groupedByROTC) {
							if (groupedByROTC.hasOwnProperty(key)) {
								const currentArray = groupedByROTC[key];
								// console.log(`Contents of ${key} array:`);

								for (var j = 0; j < currentArray.length; j++) {
									// console.log('cur', currentArray[j].subjectTitle);

									dataRow = []

									dataRow.push(
										{ text: currentArray[j].subjectId, border: [true, false, true, false] },
										{ text: currentArray[j].subjectTitle, border: [true, false, true, false] },
										{ text: currentArray[j].grade, alignment: 'center', border: [true, false, true, false] },
										{ text: currentArray[j].units, alignment: 'center', border: [true, false, true, false] },

									);




									// var currentpage = 1
									// var pagecount = 1
									// Check if it is the last row and apply a border to all columns

									dataRow.forEach(function (cell) {
										// console.log('Cell: ',cell)
										// cell.border[2] = true; // Apply border to the right side
										cell.border[3] = true; // Apply border to the bottom side

									});


									res.push(dataRow);
									// You can perform actions on the individual elements here
								}
							}
						}


						for (var i = 0; i < array.length; i++) {

							for (var j = 0; j < array[i].length; j++) {

								// console.log(dataRow[3])
							}
							// res.push(
							// 	[
							// 		{
							// 			table: {
							// 				widths: [90, 345, 40, 40],
							// 				body:
							// 					tempres

							// 			}, fontSize: 9,
							// 			layout: {
							// 				hLineWidth: function (i, node) { return 0.5; },
							// 				vLineWidth: function (i, node) { return 0.5; },
							// 			}
							// 			, colSpan: 4
							// 		},
							// 		{ text: "" },
							// 		{ text: "" },
							// 		{ text: "" },
							// 	]
							// )


						}

					}

					else {

						dataRow = []
						dataRow.push(

							{ text: '' },
							{ text: '' },
							{ text: '', alignment: 'center' },
							{ text: '', alignment: 'center' }
						);
						res.push(dataRow)
					}


					// var lastRow = res[res.length - 1];
					// for (var k = 0; k < lastRow.length; k++) {
					// 	lastRow[k].border[2] = true;

					// }

					// res.push([
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' }
					//   ]);
					return res;
				}
				function get_company_names_per_group(array) {
					var res = []
					for (let index = 0; index < array.length; index++) {
						res.push({
							companyName: array[index][0].companyName
						})
					}
					return res;
				}
				// function fillROTC() {

				// 	// console.log(array)
				// 	// var current_page = Array.from({length: 10}, (_, index) => index + 1);
				// 	// console.log('PAGE: ',current_page[0])
				// 	var dataRow = [];
				// 	var semTableArray = [];
				// 	var companyNames = []
				// 	// console.log("groupedSy: ", array)
				// 	var res = [];

				// 	// { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				// 	// { text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				// 	// { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
				// 	// { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }

				// 	var columns = [
				// 		{ text: 'COURSE NO', bold: true, border: [true, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				// 		{ text: 'DESCRIPTIVE TITLE', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				// 		{ text: 'GRADES', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
				// 		{ text: 'CREDIT UNITS', bold: true, border: [false, true, true, true], fillColor: '#dddddd', alignment: 'center' }
				// 	];


				// 	res.push(columns);
				// 	var lastCompany = ''
				// 	if (groupedByROTC.length != 0) {


				// 		for (var i = 0; i < this.OTRDetails.length; i++) {

				// 			var tempres = [];
				// 			for (var j = 0; j < this.OTRDetails[i].length; j++) {
				// 				dataRow = []

				// 				dataRow.push(
				// 					{ text: this.OTRDetails[i][j], border: [true, false, true, true] },
				// 					{ text: this.OTRDetails[i][j].subjectTitle, border: [true, false, true, true] },
				// 					{ text: this.OTRDetails[i][j].grade, alignment: 'center', border: [true, false, true, true] },
				// 					{ text: this.OTRDetails[i][j].units, alignment: 'center', border: [true, false, true, true] },

				// 				);




				// 				// var currentpage = 1
				// 				// var pagecount = 1
				// 				// Check if it is the last row and apply a border to all columns
				// 				if (j === groupedByROTC[i].length - 1) {
				// 					dataRow.forEach(function (cell) {
				// 						// console.log('Cell: ',cell)
				// 						// cell.border[2] = true; // Apply border to the right side
				// 						cell.border[3] = true; // Apply border to the bottom side

				// 					});
				// 				}

				// 				// tempres.push(dataRow);
				// 				// console.log(dataRow[3])
				// 			}
				// 			res.push(dataRow)


				// 		}

				// 	}

				// 	else {

				// 		dataRow = []
				// 		dataRow.push(

				// 			{ text: '' },
				// 			{ text: '' },
				// 			{ text: '', alignment: 'center' },
				// 			{ text: '', alignment: 'center' }
				// 		);
				// 		res.push(dataRow)
				// 	}


				// 	// var lastRow = res[res.length - 1];
				// 	// for (var k = 0; k < lastRow.length; k++) {
				// 	// 	lastRow[k].border[2] = true;

				// 	// }

				// 	// res.push([
				// 	// 	{ text: '' },
				// 	// 	{ text: '' },
				// 	// 	{ text: '' },
				// 	// 	{ text: '' }
				// 	//   ]);
				// 	return res;
				// }

				var content = [
					{

					},
					{
						text: 'C E R T I F I C A T I O N',
						fontSize: 18,
						bold: true,
						alignment: 'center',
						// marginTop: 70,
						margin: [50, 70, 50, 0],
						decoration: 'underline',
					},
					{
						text: 'TO WHOM IT MAY CONCERN:',
						fontSize: 12,
						bold: true,
						// marginTop: 40
						margin: [50, 40, 50, 0],
						alignment: 'justify'
					},
					{
						text: [
							{ text: '\u200B          THIS IS TO CERTIFY that ' },
							{ text: gender + FIRST + ' ' + MIDDLE + ' ' + LAST, bold: true },
							{ text: ' has completed the following Reserved Officer Courses, to wit: ' },
						], fontSize: 11,
						// marginTop: 15
						margin: [50, 15, 50, 0],
						alignment: 'justify'
					},
					{
						stack: [
							{ text: '\n' },
							{
								style: 'tableExample',
								table: {
									headerRows: 1,
									pageBreak: 'before',
									dontBreakRows: false,
									keepWithHeaderRows: 1,
									widths: [120, 300, 40, 40],
									body: fillROTC(),
									heights: [10, 10, 10, 10, 10] // Adjust these values for the desired row heights
								},
								fontSize: 9,
								layout: {
									hLineWidth: function (i, node) { return 0.5; },
									vLineWidth: function (i, node) { return 0.5; },
								}
							}

							// { text: '\n', margin: [0, 0, 1, 0] } // Add a margin to the element after the table
						]

					},
					{
						text: '\u200B		  This certification is issued this ' + this.today + ' upon the request of ' + gender + LAST + ' ' + 'for ' + this.purposeGrad,
						fontSize: 11,
						// marginTop: 15
						margin: [50, 15, 50, 0],
						alignment: 'justify'
					},
					{
						text: 'LIZA M. EMPEDRAD, Ph.D.',
						fontSize: 11,
						bold: true,
						alignment: 'right',
						// marginTop: 40

						margin: [50, 50, 50, 0]
					},
					{
						text: 'University Registrar',
						fontSize: 11,
						alignment: 'right',
						margin: [50, 0, 50, 0]
					},
					{
						text: 'University Seal',
						fontSize: 9,
						alignment: 'left',
						// marginTop: 250
						margin: [50, 180, 50, 0]
					},
					{
						text: '/vcc 2023',
						fontSize: 9,
						alignment: 'left',
						// marginTop: 2
						margin: [50, 2, 50, 0]
					}
				];

				var dd = {
					background: function (currentPage, pageSize) {
						// console.log(`Current page: ${currentPage}`);
						const pageWidth = pageSize.width;
						const pageHeight = pageSize.height;
						const watermarkWidth = 612;
						const watermarkHeight = 936;
						const pageRatio = pageWidth / pageHeight;
						const watermarkRatio = watermarkWidth / watermarkHeight;
						let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

						if (watermarkRatio > pageRatio) {
							scale = pageWidth / watermarkWidth;
						} else {
							scale = pageHeight / watermarkHeight;
						}

						scaledWatermarkWidth = watermarkWidth * scale;
						scaledWatermarkHeight = watermarkHeight * scale;
						x = (pageWidth - scaledWatermarkWidth) / 2;
						y = (pageHeight - scaledWatermarkHeight) / 2;

						return {
							image: "data:image/png;base64," + HeaderFooterPicture,
							width: scaledWatermarkWidth,
							height: scaledWatermarkHeight,
							opacity: 30,
							absolutePosition: { x: x, y: y }
						};

					},
					// pageSize: {
					// 	width: 612, // 8.5 inches * 72 = 612
					// 	height: 936 // 14 inches * 72 = 792
					// },
					pageSize: 'LETTER',
					pageOrientation: 'portrait',
					pageMargins: [40, 100, 40, 100],
					header: header,
					content: content,
					footer: footer
				};


				if (Object.keys(groupedByROTC).length === 0) {
					this.global.swalAlert('No Record Found', '', 'warning')
				}
				else {
					pdfMake.createPdf(dd).open();
				}

			} catch (error) {
				console.error(error);
			}
		}
		// async generateGrades() {
		// 	try {
		// 		const response = await this.api.getOTRDetails(this.idNumber, this.DegreeLevel).toPromise();
		// 		const result = response.json();
		// 		this.OTRDetails2 = result.data
		// 		if (result != '') {
		// 			console.log('TrueCopy: ',this.OTRDetails2)
		// 		}
		// 	} catch (error) {
		// 		console.error(error);
		// 	}
		// 	for (var x in this.GetGradRec) {
		// 		if (this.DegreeLevel == 1 && this.GetGradRec[x].otrType == 1) {
		// 			var degreeTitle = this.GetGradRec[x].degreeTitle
		// 			var dateofGraduation = this.GetGradRec[x].dateofGraduation
		// 			this.dateformat(dateofGraduation);
		// 			var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
		// 			var CodeCourse = this.GetGradRec[x].courseCode
		// 		}
		// 		if (this.DegreeLevel == 2 && this.GetGradRec[x].otrType == 2) {
		// 			var degreeTitle = this.GetGradRec[x].degreeTitle
		// 			var dateofGraduation = this.GetGradRec[x].dateofGraduation
		// 			this.dateformat(dateofGraduation);
		// 			var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
		// 			var CodeCourse = this.GetGradRec[x].courseCode
		// 		}
		// 		if (this.DegreeLevel == 3 && this.GetGradRec[x].otrType == 3) {
		// 			var degreeTitle = this.GetGradRec[x].degreeTitle
		// 			var dateofGraduation = this.GetGradRec[x].dateofGraduation
		// 			this.dateformat(dateofGraduation);
		// 			var specialOrderNumber = this.GetGradRec[x].specialOrderNumber
		// 			var CodeCourse = this.GetGradRec[x].courseCode
		// 		}
		// 	}


		// 	var Degree = this.degreeTitle
		// 	var LAST = this.lastName
		// 	var MIDDLE = this.middleName
		// 	var FIRST = this.firstName

		// 	var HeaderFooterPicture = this.images.header
		// 	var headerlogo = this.images.header
		// 	var header = [
		// 		// {
		// 		// 	canvas:
		// 		// 		[
		// 		// 			{
		// 		// 				type: 'line',
		// 		// 				x1: 43, y1: 117,
		// 		// 				x2: 570, y2: 117,
		// 		// 				lineWidth: 2,
		// 		// 			},
		// 		// 		]
		// 		// },
		// 	]

		// 	// {
		// 	// 	image: "data:image/png;base64," + headerlogo,
		// 	// 	width: 70,
		// 	// 	opacity: 1,
		// 	// 	absolutePosition: { x: 155, y: 40 },
		// 	// },
		// 	var content = [
		// 		// {
		// 		// 	text: 'University of Saint Louis',
		// 		// 	alignment: 'center',
		// 		// 	fontSize: 20,
		// 		// 	bold: true,
		// 		// 	margin: [40, 60, 40, 0],
		// 		// 	decoration: 'underline',
		// 		// },
		// 		{
		// 			canvas:
		// 				[
		// 					{
		// 						type: 'line',
		// 						x1: 43, y1: 15,
		// 						x2: 473, y2: 15,
		// 						lineWidth: 2,
		// 					},
		// 				]
		// 		},

		// 		{
		// 			text: [
		// 				{ text: 'TRUE COPY OF SCHOLASTIC RECORD' },
		// 			], fontSize: 11,
		// 			// marginTop: 15
		// 			bold: true,
		// 			margin: [50, 15, 50, 0],
		// 			alignment: 'center'
		// 		},
		// 		// Existing content...
		// 		{
		// 			// ...
		// 			// New table content
		// 			table: {
		// 				widths: ['auto', '*'],
		// 				body: [
		// 					[{text: 'NAME', bold: true, margin: [5, 0]}, {text: LAST + ' '+ FIRST + ' ' +MIDDLE, bold: true}],
		// 					[{text: 'COURSE', bold: true,}, {text: degreeTitle, bold:true}],
		// 				],
		// 				layout: {
		// 					hLineWidth: function (i, node) {
		// 						if (i === 0 || i === node.table.body.length) {
		// 							return 0;  // Hide horizontal lines at table's top and bottom
		// 						}
		// 						return i === 1 ? 0.5 : 0;  // Customize the line width for the first data row
		// 					},
		// 					vLineWidth: function (i) {
		// 						return 0;  // Hide vertical lines
		// 					},
		// 					paddingLeft: function (i) {
		// 						return i === 0 ? 0 : 5;  // Add padding to the left of the "Name" column
		// 					},
		// 					paddingRight: function (i) {
		// 						return i === 0 ? 5 : 0;  // Add padding to the right of the "Name" column
		// 					}
		// 				}
		// 			},
		// 			margin: [40, 10, 40, 0]
		// 		},
		// 		{
		// 			text: 'Grades',
		// 			fontSize: 24,
		// 			bold: true,
		// 			margin: [40, 10, 40, 0]
		// 		},
		// 		{
		// 			text: 'has successfully completed the Lorem Ipsum course',
		// 			fontSize: 16,
		// 			margin: [40, 10, 40, 0]
		// 		},
		// 		{
		// 			text: 'on ' + new Date().toLocaleDateString(),
		// 			fontSize: 16,
		// 			margin: [40, 10, 40, 0]
		// 		},
		// 		{
		// 			text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
		// 			fontSize: 14,
		// 			margin: [40, 30, 40, 0]
		// 		}
		// 	];

		// 	var footer = {
		// 	};

		// 	var dd = {
		// 		background: function (currentPage, pageSize) {
		// 			// console.log(`Current page: ${currentPage}`);
		// 			const pageWidth = pageSize.width;
		// 			const pageHeight = pageSize.height;
		// 			const watermarkWidth = 612;
		// 			const watermarkHeight = 936;
		// 			const pageRatio = pageWidth / pageHeight;
		// 			const watermarkRatio = watermarkWidth / watermarkHeight;
		// 			let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

		// 			if (watermarkRatio > pageRatio) {
		// 				scale = pageWidth / watermarkWidth;
		// 			} else {
		// 				scale = pageHeight / watermarkHeight;
		// 			}

		// 			scaledWatermarkWidth = watermarkWidth * scale;
		// 			scaledWatermarkHeight = watermarkHeight * scale;
		// 			x = (pageWidth - scaledWatermarkWidth) / 2;
		// 			y = (pageHeight - scaledWatermarkHeight) / 2;

		// 			return {
		// 				image: "data:image/png;base64," + HeaderFooterPicture,
		// 				width: scaledWatermarkWidth,
		// 				height: scaledWatermarkHeight,
		// 				opacity: 30,
		// 				absolutePosition: { x: x, y: y }
		// 			};

		// 		},
		// 		// pageSize: {
		// 		// 	width: 612, // 8.5 inches * 72 = 612
		// 		// 	height: 936 // 14 inches * 72 = 792
		// 		// },
		// 		pageSize: 'LETTER',
		// 		pageOrientation: 'portrait',
		// 		pageMargins: [40, 100, 40, 100],
		// 		header: header,
		// 		content: content,
		// 		footer: footer
		// 	};

		// 	pdfMake.createPdf(dd).open();
		// }


	}
}