import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveconfigComponent } from './activeconfig.component';

describe('ActiveconfigComponent', () => {
  let component: ActiveconfigComponent;
  let fixture: ComponentFixture<ActiveconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
