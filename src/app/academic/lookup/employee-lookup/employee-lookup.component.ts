import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
	selector: 'app-employee-lookup',
	templateUrl: './employee-lookup.component.html',
	styleUrls: ['./employee-lookup.component.scss']
})
export class EmployeeLookupComponent implements OnInit {

	x = 1
	arraystud = [];
	keyword = ''
	tempkeyword = ''

	constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<EmployeeLookupComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService) { }

	ngOnInit() {
	}

	select(param) {
		this.dialogRef.close({ result: param.employeeId, name: param.fullName,data:param });
	}

	keyDownFunction(event) {
		if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
			this.x = 0;
			if (this.keyword != '') {
				this.tempkeyword = this.keyword;
				this.arraystud = undefined;
        try {
          if(this.data.searchType == 'faculty_evaluation'){
            this.api.getFacultyEvalPersonLookup(this.keyword)
                .map(response => response.json())
                .subscribe(res => {
                this.arraystud = res.data
              },Error=>{
                this.global.swalAlertError(Error)
              });
          }
        } catch (error) {
          this.api.getEmployeeEmployeeLookup(this.keyword)
              .map(response => response.json())
              .subscribe(res => {
                this.arraystud = res.data
              }, Error => {
                this.global.swalAlertError(Error)
              });
        }

			}
		}
	}


	close(): void {
		this.dialogRef.close({ result: 'cancel' });
	}
	keyDownFunctionCODE(event) {
		if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
			if (this.keyword == '') {
			} else {
				this.keyDownFunction('onoutfocus');
			}
		}
	}
}
