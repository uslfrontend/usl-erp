import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';


@Component({
	selector: 'app-person-lookup',
	templateUrl: './person-lookup.component.html',
	styleUrls: ['./person-lookup.component.scss']
})
export class PersonLookupComponent implements OnInit {


	x = 1
	arraystud = [];
	keyword = ''
	tempkeyword = ''

	exactmatch = false

	constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<PersonLookupComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService) { }

	ngOnInit() {
	}

	select(id) {
		this.dialogRef.close({ result: id });
	}

	keyDownFunction(event) {
		if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
			this.x = 0;
			if (this.keyword != '') {
				this.tempkeyword = this.keyword;
				this.arraystud = undefined;
				this.api.getPersonLookup(this.keyword, this.exactmatch)
					.map(response => response.json())
					.subscribe(res => {
						//console.log(res)
						this.arraystud = res.data[0]
						this.arraystud = this.arraystud.concat(res.data[1])
						this.arraystud = this.arraystud.concat(res.data[2])
					}, Error => {
						this.global.swalAlertError(Error)
					});
			}
		}
	}


	close(): void {
		this.dialogRef.close({ result: 'cancel' });
	}
	keyDownFunctionCODE(event) {
		if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
			if (this.keyword == '') {
			} else {
				this.keyDownFunction('onoutfocus');
			}
		}
	}
}
