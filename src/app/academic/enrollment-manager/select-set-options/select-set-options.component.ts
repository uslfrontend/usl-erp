import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { GlobalService } from './../../../global.service';

import { ApiService } from './../../../api.service';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-select-set-options',
  templateUrl: './select-set-options.component.html',
  styleUrls: ['./select-set-options.component.scss']
})
export class SelectSetOptionsComponent implements OnInit {
  res=[]
  codesWithRequisites = [];
  codeRequisite = false
  requisiteMessage = '';
  tempRequisites = '';
  constructor(public dialogRef: MatDialogRef<SelectSetOptionsComponent>,@Inject(MAT_DIALOG_DATA) public data2: any,public global: GlobalService,private api: ApiService) {


  }

  ngOnInit() {
    this.res.splice(0, this.res.length)
    this.codesWithRequisites.splice(0, this.codesWithRequisites.length)

    this.codeRequisite = false
    this.requisiteMessage = '';
    this.tempRequisites = '';

    for (var i = 0; i < this.data2.selectedData.length; ++i) {
      if (!this.res.includes(this.data2.selectedData[i].codeNo)&&this.data2.selectedData[i].availableSlot>0) {
        this.res.push(this.data2.selectedData[i].codeNo)
      }
    }
   this.checkCodeRequisites(this.res)
  }

  onNoClickclose(): void {
       this.dialogRef.close({result:'cancel'});
  }

  checkCodeRequisites(res){

    // console.log(res)
    for(let x = 0; x< res.length; x++){
      // console.log("detailID:",this.getCodeDetailID(res[x]))
      this.checkCodeRequisitesAPIImplement(this.getCodeDetailID(res[x]),res[x])
    }

    if(this.tempRequisites!=''){

      this.codeRequisite = true

    }else{
      this.codeRequisite = false;
    }
  }
  checkCodeRequisitesAPIImplement(detailID,codeNo){
    this.tempRequisites = '';
    this.api.getEnrollmentCodeRequisitesGet(this.data2.idnumber+'/'+codeNo+'/'+this.global.syear)
              .map(response => response.json())
              .subscribe(res2 => {
                if(res2.data.length>=1){

                  this.tempRequisites = "Code "+codeNo+"<br>";

                  for(let i = 0; i<res2.data.length; i++){
                    this.tempRequisites = this.tempRequisites+res2.data[i].requisiteSubjectID+': '+res2.data[i].requisiteSubjectTitle+"<br>";

                    this.codesWithRequisites.push({
                      'code': codeNo,
                      'requisites': res2.data[i].requisiteSubjectID+': '+res2.data[i].requisiteSubjectTitle
                    })
                  }
                }
              this.requisiteMessage += this.tempRequisites;
              if(this.tempRequisites == '')
                this.codeRequisite = false
              else
                this.codeRequisite = true

              },Error=>{
               this.global.swalAlertError(Error);
              });

  }

  getCodeDetailID(codeNo){
    for (var i = 0; i < this.data2.selectedData.length; ++i) {
      if (this.data2.selectedData[i].codeNo == codeNo) {
        return this.data2.selectedData[i].detailId;
        break;
      }
     }
  }

  showhide(x){
  	if (x==0) {
  		return false
  	}else{
  		if (this.data2.selectedData[x].codeNo==this.data2.selectedData[x-1].codeNo) {
  			return true
  		}else
  			return false
  	}
  }

  checkdisabled(x){
  	if (x<=0) {
  		return true
  	}
  		return false

  }
  checkdisabled2(x){
    var res = 0;
    for(let code of this.codesWithRequisites){
      if(x == code.code){
        if(!this.global.checkaccess(':Enrollment:DropCode')){
          for(let index of this.res.keys()){
            if (this.res.includes(code.code)) {
              this.res.splice( this.res.indexOf(code.code), 1 );
            }else{
              this.res.push(code.code)
            }
          }
        }
        res = 1;
        break;
      }
      else
        res = 0;
    }
    if(res == 1)
      return true;
    else
      return false
  }

  spliceTrigger = true;
  checkthis(x,codeNo){
      if(codeNo.length<=0){
        if (x<=0) {
          return false
        }else{
          if(this.codesWithRequisites.length>0){
            for(let code of this.codesWithRequisites){
              if(codeNo == code.code){

                if(this.spliceTrigger == true){
                  for(const index of this.res.keys()){
                    if (this.res.includes(codeNo)) {
                      this.res.splice( index, 1 );
                      // this.clickcheck(index)
                      this.spliceTrigger = true;
                      break;
                    }else{
                      this.spliceTrigger = false;
                    }
                  }
                }
                return false
              }else{
                return true
              }
            }
          }else{
            return true
          }

        }
      }
      else{
        if(x<=0)
          return false
        else
          return true
      }
  }
  clickcheck(index){
  	if (this.res.includes(this.data2.selectedData[index].codeNo)) {
  		  this.res.splice( this.res.indexOf(this.data2.selectedData[index].codeNo), 1 );
  	}else{
        this.res.push(this.data2.selectedData[index].codeNo)
  	}
  }

  confirm(){
  	var finalresult=[]
    finalresult.splice(0, finalresult.length)
  	for (var i = 0; i < this.data2.selectedData.length; ++i) {
  		if (this.res.includes(this.data2.selectedData[i].codeNo)) {
  			finalresult.push(this.data2.selectedData[i])
  		}
  	}
    this.dialogRef.close({result:'success',data:finalresult});

  }
}
