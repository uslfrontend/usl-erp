import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { GlobalService } from './../../../global.service';
@Component({
  selector: 'app-grade-last-sy',
  templateUrl: './grade-last-sy.component.html',
  styleUrls: ['./grade-last-sy.component.scss']
})
export class GradeLastSyComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<GradeLastSyComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService) { 
 
  }
  ngOnInit() {
  }

  onNoClickclose(): void {
       this.dialogRef.close({result:'cancel'});
  }
}
