import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { PersonLookupComponent } from './../../academic/lookup/person-lookup/person-lookup.component';
import { MatCheckbox, MatDialog, MatSlideToggleChange, } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import * as ExcelJS from "exceljs/dist/exceljs"
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
const swal = Swal;

type AOA = any[][];

@Component({
  selector: 'app-grades-hiding-unhiding',
  templateUrl: './grades-hiding-unhiding.component.html',
  styleUrls: ['./grades-hiding-unhiding.component.css']
})
export class GradesHidingUnhidingComponent implements OnInit {

  image: any = 'assets/noimage.jpg';
  schoolYear = this.global.syear
  id = '';
  fName = '';
  mName = '';
  lName = '';
  suffix = '';
  address = '';
  contactNumber = '';
  recordID = '';
  x = '';
  y = '';
  yy = '';
  currentTab = 'sheet1'
  validation = 'LIST OF STUDENTS WITH ENROLLMENT DUES'
  saveArray = []
  yearArray = []
  mismatchedArray = []
  arr = []
  hiddenArray;
  gradearray;
  gradearray1;
  arrayNames;
  container;
  IsWait = true;
  unchecked = false
  isChecked = false;
  checked = false;
  result: any
  gradeArrayExcel: any
  semester: any
  average = 0;
  count = 0;

  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService) { }

  ngOnInit() {
    // console.log(this.global.syear)
    if (this.global.activeid != '') {
      this.id = this.global.activeid
      this.keyDownFunction('onoutfocus')
    }
  }

  clear() {
    this.image = 'assets/noimage.jpg';
    this.recordID = '';
    this.fName = '';
    this.mName = '';
    this.lName = '';
    this.suffix = '';
    this.address = '';
    this.contactNumber = '';
    this.x = '';
    this.y = '';
    this.yy = '';
    this.average = 0;
    this.count = 0;
    this.IsWait = true;
    this.checked = false;
    this.gradearray = [];
    this.gradearray1 = [];
    this.correctData = [];
    this.mismatchedArray = [];
    this.studentArray = [];
    this.arr = [];
    this.duplicate = [];
    this.duplicate1 = [];

  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.id != '') {
        // clear variables
        this.clear();
        this.global.swalLoading('Loading Person Information');
        this.api.getStudentIncompletePayment(this.id, this.schoolYear).map(response => response.json()).subscribe(res => {
          if (res.data != null) {
            //checks the status of the students incomplete payement record
              if (res.data.status == 0)
                this.isChecked = false
              else this.isChecked = true
            // console.log(res.data)
          }
          // else{this.disabled = true}
          // console.log(res.data)
        })
        this.api.getStudentInfoGHU(this.id, this.schoolYear).map(response => response.json()).subscribe(res => {

          this.global.swalClose();
          if (res.data != null) {
            // console.log('hello World')
            //pass the data from api into the variables
            this.fName = res.data.firstName;
            this.mName = res.data.middleName;
            this.lName = res.data.lastName;
            this.suffix = res.data.suffix;
            this.address = res.data.address;
            this.contactNumber = res.data.contactNo;
            //get id pic
            this.api.getStudentInfoGHU(this.id, this.schoolYear).map(response => response.json()).subscribe(res => {
              if (res.data != null) {
                this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
              }
            }, Error => {
              this.global.swalAlertError(Error);
            })
            //get AcademicHistory
            this.getAcademicHistory();
          } else {
            this.gradearray = null;
            this.global.swalAlert(res.message, 'No person information is associated with this ID number!', 'warning')
          }
        }, Error => {
          this.global.swalAlertError(Error);
        });
      }
    }
  }

  studentlookup(): void {
    const dialogRef = this.dialog.open(PersonLookupComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
 
      }
    });
  }


  getAcademicHistory() {

    this.gradearray = undefined;
    this.hiddenArray = undefined;
    this.api.getAcademicHistoryGHU(this.id).map(response => response.json()).subscribe(res => {

        //gets the whole academic history
      if (res.data.length != 0) {
        this.gradearray = [];
        this.gradearray = res.data
        this.IsWait = false;
      } else {
        this.IsWait = false;
      }
      // gets the academic history record of the selected school year
      if (res.data.length != 0) {
        this.gradearray1 = []
        for (var x = 0; x < res.data.length; x++) {
          if (res.data[x].schoolYear == this.schoolYear) {
            this.gradearray1.push({
              recordID: res.data[x].recordID,
              subjectID: res.data[x].subjectID,
              subjectTitle: res.data[x].subjectTitle,
              grade: res.data[x].grade,
              units: res.data[x].units,
              schoolYear: res.data[x].schoolYear,
              school: res.data[x].school,
              fullName: res.data[x].fullName,
              idNumber: res.data[x].idNumber,
              codeNo: res.data[x].codeNo,
              companyID: res.data[x].companyID,
              setNo: res.data[x].setNo,
              gradeWasHidden: res.data[x].gradeWasHidden,
              codeExists: res.data[x].codeExists
            })

          }
        }
      }

      // console.log('grade array1', this.gradearray1)
    }, Error => {
      this.global.swalAlertError(Error)
    });


  }

  calcgrade(y) { // calculates the avrage grade 
    var average = 0
    var count = 0
    for (var i = 0; i < this.gradearray.length; i++) {
      if (this.gradearray[i].schoolYear == y) {
        var x;
        if (this.gradearray[i].grade.replace(/\D/g, "") == '') { // replaces any non-digit characters with an empty string
          x = 0
        } else x = parseInt(this.gradearray[i].grade.replace(/\D/g, "")); //Parses grade string into integer also removes any non-digit characters from the string
        average = average + x * parseFloat(this.gradearray[i].units);
        count = count + parseFloat(this.gradearray[i].units);
      }
    }
    return "Average: " + (Math.floor((average / count) * 100) / 100).toFixed(2).toString() + "%"
  }

  calcgrade1(y) {  // calculates the avrage grade 
    var average = 0
    var count = 0
    for (var i = 0; i < this.gradearray1.length; i++) {
      if (this.gradearray1[i].schoolYear == y) {
        var x;
        if (this.gradearray1[i].grade.replace(/\D/g, "") == '') {// replaces any non-digit characters with an empty string
          x = 0
        } else x = parseInt(this.gradearray1[i].grade.replace(/\D/g, ""));  //Parses grade string into integer also removes any non-digit characters from the string
        average = average + x * parseFloat(this.gradearray1[i].units);
        count = count + parseFloat(this.gradearray1[i].units);
      }
    }
    return "Average: " + (Math.floor((average / count) * 100) / 100).toFixed(2).toString() + "%"
  }

  getsy(sem) {
    var y = parseInt(sem.substring(0, 4)) + 1; //  extracts the first four characters from the sem string using the substring
    return "SY " + sem.substring(0, 4) + "-" + y
  }

  getsem(sem) { // extracts the sixth character from the sem string using the substring
    if (sem.substring(6) == '1')
      return "First Semester";
    else if (sem.substring(6) == '2')
      return "Second Semester";
    else
      return "Summer";
  }

  sy(x) { //checks the school year if its different from the current one stored
    if (this.x.substring(0, 6) == x.substring(0, 6)) {
      this.x = x
      return false
    } else {
      this.x = x
      return true
    }
  }

  sem(y) { //checks the semester if its different from the current one stored
    if (this.y == y) {
      this.y = y
      return false
    } else {
      this.y = y
      return true
    }
  }

  sem2(y, i) {
    if (this.gradearray[i + 1] != undefined) {
      if (this.gradearray[i].schoolYear != this.gradearray[i + 1].schoolYear) {
        return true
      }
      return false
    }
    return true
  }

  sem3(y, i) {
    if (this.gradearray1[i + 1] != undefined) {
      if (this.gradearray1[i].schoolYear != this.gradearray1[i + 1].schoolYear) {
        return true
      }
      return false
    }
    return true
  }

  hide() {
    //updates the incomplete payment status of the student and hide its grade
    this.api.putStudentIncompletePayment({
      "idNumber": this.id,
      "schoolYear": this.schoolYear,
      "status": 1
    }).map(response => response.json()).subscribe(res => {
      this.isChecked = true
      this.getAcademicHistory();

    })
  }

  unhide() {
    //updates the incomplete payment status of the student and unhide its grade
    this.api.putStudentIncompletePayment({
      "idNumber": this.id,
      "schoolYear": this.schoolYear,
      "status": 0
    }).map(response => response.json()).subscribe(res => {
      this.isChecked = false
      this.getAcademicHistory();
    })

  }

  //hides and unhides the grade of students
  toggleGradeHideUnhide() { 
    this.global.swalLoading('Loading Person Information');
    swal.fire({
      title: 'Are you sure?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
       
        this.api.getStudentIncompletePayment(this.id, this.schoolYear).map(response => response.json()).subscribe(res => {
          if (res.data != null) {
            if (res.data.status == 0) {  
              this.hide() //Update student incomplete payment record(hides grade)
              this.global.swalSuccess('Grade is Hidden!')
            }

            if (res.data.status == 1) {   
              this.unhide() //Update student incomplete payment record (unhide grades)
              this.global.swalSuccess('Grade is Unhidden!')
            }
          } else {
            this.api.postStudentIncompletePayment({ //Adds student incomplete payment record
              "idNumber": this.id,
              "schoolYear": this.schoolYear,
              "status": 1
            }).map(response => response.json()).subscribe(res => {
              this.isChecked = true
              // this.postResult = res.data
              // console.log(this.postResult)  
              this.getAcademicHistory();
            })
            this.global.swalSuccess('Grade is Hidden!')
            this.global.swalClose();
          }
        })

      }else{
        this.isChecked = !this.isChecked
      }
    })

  }

  //formats the cell border, background color,forground color, type and pattern
  rowBoldAndOutline(cellRow: any) {
    cellRow.eachCell((cell: any, number: number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'FFFFFFFF'
        },
        bgColor: {
          argb: 'FFFFFFFF'
        },
      };
      cell.font = {
        color: {
          argb: '00000000',
        },
      }
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //upload variables
  data: AOA = [];
  firstData = undefined
  errorData = false
  duplicate = []
  duplicate1 = []
  duplicate2 = []
  correctData = []
  studentArray = []
  // finalIds = []
  fromUpload = false
  course = ''
  dept = ''
  delayInMilliseconds = 1500; //1.5 seconds
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;

  onFileChange(evt: any) {
    this.global.swalLoading('Uploading Excel Data...');

    /* wire up file reader */
    this.firstData = undefined
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      // console.log('onload')

      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = []

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.uploadthis.nativeElement.value = ''

      //check if the uploaded file's titles matches the correct file
      var extractedValidation = this.data[2][0].slice(0)
      try {
        if (extractedValidation != this.validation) {
          this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
          return
        }
      } catch (err) {
        this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
        return
      }
      //reset varriables and arrays
      this.errorData = false
      this.studentArray = []
      if (this.data[0] != undefined) {
        this.arrayNames = []
        this.gradeArrayExcel = []
        this.data.shift();
      
        // gets the entire column of id number from the excel file
        for (var i2 = 7; i2 < this.data.length; ++i2) {
          if (this.data[i2][1] != '' && this.data[i2][1] != undefined && this.data[i2][1] != null) {
            // this.data[i2][0].toString()
            this.gradeArrayExcel.push(this.data[i2][1]) 
          }
        }
        ///gets the entire column of First Name, Middle Name, LastName from the excel file
        for (var i2 = 7; i2 < this.data.length; ++i2) {
          if (this.data[i2][1] != '' && this.data[i2][1] != undefined && this.data[i2][1] != null) {
            // this.data[i2][0].toString()
            this.arrayNames.push({ id: this.data[i2][1], Lname: this.data[i2][2], Fname: this.data[i2][3], Mname: this.data[i2][4] }) 
          }
        }

        // variables for pushing data into array
        let counter = 1
        this.fromUpload = true
        this.duplicate = []
        this.correctData = []
        this.studentArray = []
        // this.finalIds = []
        this.container = []
        this.gradeArrayExcel.forEach((value) => {
          if (this.duplicate.includes(value)) { 
            //checks for matching data in the array if true it pushes the id and Last name into another array
            this.errorData = true
            this.studentArray.push({ id: value, Lname: 'Duplicated Value' })
            // this.finalIds.push(value)
            counter++
            // console.log("Printlog: Value", value)
          } else {
            this.api.getPersonInformationGHU(value).map(response => response.json()).subscribe(res => {
              this.container = res.data

              
              if (res.message != undefined && res.message == 'Person found.') {
                //compare the name in the excel file and the name in the table and pushes the correct  and filtered data into a new array
                for (var x = 0; x < this.arrayNames.length; x++) {
                  if (this.container.idNumber == this.arrayNames[x].id) {
                    if (this.container.lastName == this.arrayNames[x].Lname.toUpperCase()) {
                      if (this.container.firstName == this.arrayNames[x].Fname.toUpperCase()) {
                        if (this.container.middleName== this.arrayNames[x].Mname.toUpperCase()) {
                          if (this.duplicate1.includes(value)) { }

                          else {
                            this.fName = res.data.firstName;
                            this.mName = res.data.middleName;
                            this.lName = res.data.lastName;
                            this.correctData.push({ id: value, Lname: this.lName, Fname: this.fName, Mname: this.mName })
                          }

                          this.duplicate1.push(value)
                        } else {
                          if (this.duplicate1.includes(value)) {
                          }
                          else {
                            this.studentArray.push({ id: value, Name: 'Middle Name does not match!' })
                            this.duplicate1.push(value)
                            this.errorData = true
                          }
                        }
                      } else {
                        if (this.duplicate1.includes(value)) {

                        }
                        else {
                          this.studentArray.push({ id: value, Name: 'First Name does not match!' })
                          this.duplicate1.push(value)
                          this.errorData = true
                        }
                      }

                    } else {
                      if (this.duplicate1.includes(value)) {

                      }
                      else {
                        this.studentArray.push({ id: value, Name: 'Last Name does not match!' })
                        this.duplicate1.push(value)
                        this.errorData = true
                      }
                    }

                  }
                }
                // console.log(this.correctData)
                // console.log(this.mismatchedArray)

              } else {
                
                this.errorData = true
                this.studentArray.push({ id: value, Lname: 'ID Not Found' })
                // this.finalIds.push(value)
              }
              // console.log('counter: ', counter)
              // console.log(this.facultyDataList.length)
              if (counter == this.gradeArrayExcel.length) {
                // console.log(counter)
                for (var x = 0; x < this.correctData.length; x++) { 
                  this.studentArray.push({ // gets the correct data and pushes it to another array
                    id: this.correctData[x].id, Lname: this.correctData[x].Lname,
                    Fname: this.correctData[x].Fname, Mname: this.correctData[x].Mname
                  })
                }

                // console.log(this.employeeArray)
              }
              counter++
            })
          }
          this.duplicate.push(value)
        })
      } else {
        this.gradeArrayExcel = []
        this.global.swalAlert('Invalid Excel File!', '', 'warning')
      }
      this.global.swalClose();
    };
    reader.readAsBinaryString(target.files[0]);
    this.global.swalSuccess('Uploaded')
    this.clear() //clears all variable used
    // console
  }

  deleteUploadedError(i, data) {
    // console.log("Printlog index: ", x + ": " + id)
    this.studentArray.splice(i, 1); // 2nd parameter means remove one item only
    // this.finalIds.splice(i, 1); // 2nd parameter means remove one item only

    //checks if the array has duplicated value, unmatched id No and names, and incorrect id no
    for (var x = 0; x < this.studentArray.length; x++) {
      // console.log("Printlog data:", this.employeeArray[x].name)
      if (this.studentArray[x].Lname == 'ID Not Found' || 
      this.studentArray[x].Lname == 'Duplicated Value' || 
      this.studentArray[x].Name == 'Middle Name does not match!'|| 
      this.studentArray[x].Name == 'Last Name does not match!'|| 
      this.studentArray[x].Name == 'First Name does not match!' ) 
      {
        this.errorData = true 
        break
      } else {
        this.errorData = false
      }
    }
    // console.log(this.studentArray)
  }
  
  updateStatus() {
    //gets only the id data 
    for (var x = 0; x < this.studentArray.length; x++) {
      this.saveArray.push(this.studentArray[x].id
      )
    }
    swal.fire({
      title: 'Are you sure?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        //iterates each data of the array and gets the status payment through the api 
    this.saveArray.forEach((value) => {
      this.api.getStudentIncompletePayment(value, this.schoolYear).map(response => response.json()).subscribe(res => {
        if (res.data != null) {

          if (res.data.status == 0) { //checks the status if it is equal to 0 then it calls the api to update it to 1
            this.api.putStudentIncompletePayment({
              "idNumber": value,
              "schoolYear": this.schoolYear,
              "status": 1
            }).map(response => response.json()).subscribe(res => {
              this.isChecked = true
              // this.postResult = res.data
              // console.log(this.postResult)  
            })
            // console.log('Data is hidden!', value)
          }
          // if (res.data.status == 1) {
          //   // console.log('Data is already hidden!', value)
          // }

        } else {
          this.api.postStudentIncompletePayment({ //Adds student incomplete payment record
            "idNumber": value,
            "schoolYear": this.schoolYear,
            "status": 1
          }).map(response => response.json()).subscribe(res => {
            this.isChecked = true
          })
          // console.log('Data is created and hidden!', value)
        }
      })
    })
    this.studentArray = []
    this.global.swalSuccess('Updated!')
      }
    })

  }

  // sets the border property of each cells to thin
  leftRightOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.font = { name: 'Calibri', family: 4, size: 11, strike: false };
      cell.alignment = { horizontal: 'center' };
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        top: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  async exportList() {

    const date = new Date()
      .toISOString()
      .slice(0, 10)
      .split("-")
      .reverse()
      .join("/");
    // console.log(date);
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("My Sheet");
    await worksheet.protect('CiCT#2020') // locks the whole excel file

    let h1 = worksheet.addRow(['University of Saint Louis']);
    this.rowBoldAndOutline(h1)
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    this.rowBoldAndOutline(h2)
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    let h3 = worksheet.addRow(['LIST OF STUDENTS WITH ENROLLMENT DUES']);
    this.rowBoldAndOutline(h3)
    h3.alignment = { vertical: 'top', horizontal: 'center' };
    h3.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.getCell('A5').value = "School Year"
    worksheet.getCell('A5').alignment = { vertical: 'top', horizontal: 'left' };
    worksheet.getCell('A5').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A5').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('C5').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('C5').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('C5').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('F5').value = "Total:"
    worksheet.getCell('F5').alignment = { vertical: 'top', horizontal: 'left' };
    worksheet.getCell('F5').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('F5').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('G5').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('G5').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('G5').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('A6').value = "Semester:"
    worksheet.getCell('A6').alignment = { vertical: 'top', horizontal: 'left' };
    worksheet.getCell('A6').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A6').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('C6').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('C6').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('C6').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('A7').value = "Note: File should be saved using the filename format, enrollmentdue-<schoolyear>.xls (e.g.\n enrollmentdule-2019201.xls for first sem school year 2019-2020 )."
    worksheet.getCell('A7').alignment = { vertical: 'bottom', horizontal: 'center', wrapText: true };
    worksheet.getCell('A7').font = { name: 'Calibri', family: 4, size: 9, bold: false, strike: false, italic: true };

    //merges cells
    worksheet.mergeCells('A1:G1');
    worksheet.mergeCells('A2:G2');
    worksheet.mergeCells('A3:G3');
    worksheet.mergeCells('A5:B5');
    worksheet.mergeCells('A6:B6');
    worksheet.mergeCells('A7:G7');

    //sets the width of the column
    worksheet.getColumn(1).width = 4;
    worksheet.getColumn(2).width = 12;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 15;
    worksheet.getColumn(5).width = 15;
    worksheet.getColumn(6).width = 9;
    worksheet.getColumn(7).width = 12;

    //make format of the cells as text
    worksheet.getColumn(2).numFmt = '@';

    //sets the height of the column
    worksheet.getRow(7).height = 31;

    worksheet.getColumn(3).alignment = { horizontal: 'center' }
    worksheet.getColumn(4).alignment = { horizontal: 'center' }

    worksheet.getCell('A8').value = "No."
    worksheet.getCell('A8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('B8').value = "ID Number"
    worksheet.getCell('B8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('B8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('B8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('C8').value = "Last Name"
    worksheet.getCell('C8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('C8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('C8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('D8').value = "First Name"
    worksheet.getCell('D8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('D8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('D8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('E8').value = "Middle Name"
    worksheet.getCell('E8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('E8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('E8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('F8').value = "Course"
    worksheet.getCell('F8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('F8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('F8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    worksheet.getCell('G8').value = "Department"
    worksheet.getCell('G8').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('G8').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('G8').border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }

    let input = this.schoolYear;

    let year1 = input.substring(0, 4); //gets the data from index 0 to index 4
    let year2 = input.substring(4, 6);
    let suffix = input.substring(6, 7);

    let output = year1 + "-" + year2;
    let output2 = suffix;
    let output3 = 'SY-' + output + '-' + output2

    worksheet.getCell('C5').value = output
    worksheet.getCell('C6').value = output2

    //sets the font color to red
    const font = { color: { argb: 'FF0000' } };
    worksheet.getCell('G5').font = font;
 
    worksheet.getCell('G5').value = {
      formula: `COUNTA(B9:B169)`,   //count all the data inside a column
    };

    let dataRow
    for (var x = 1; x <= 100; x++) {
      dataRow = worksheet.addRow([x, '', '', '', '', '', ''])
      // worksheet.mergeCells('C' + (8 + x) + ':D' + (8 + x));
      this.leftRightOutline(dataRow)//sets each cells border property to thin
      dataRow.eachCell((cell, number) => {
        if (cell._address.includes('')) { 
          cell.protection = {locked: false,};//unlocks cell protection
        }
      })
    }

    //clears all the cotents of each cell
    for (var x = 1; x <= 100; x++) {
      worksheet.getCell('B' + (8 + x) + ':D' + (8 + x)).value = null
    }

    //set a column proterty to lock
    const column = worksheet.getColumn(1);
    column.eachCell((cell) => {
      cell.protection = { locked: true };
    });

    workbook.xlsx.writeBuffer().then((data: any) => {
      // console.log("buffer");
      const blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });
      let url = window.URL.createObjectURL(blob);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = url;
      a.download = "Enrollmentdue " + output3 + ".xlsx";//name of the document
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();

      Swal.fire(
        'Download Succesfully!',
        '',
        'success')

    });
  }
}
