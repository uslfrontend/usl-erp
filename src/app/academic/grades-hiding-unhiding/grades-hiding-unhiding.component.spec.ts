import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradesHidingUnhidingComponent } from './grades-hiding-unhiding.component';

describe('GradesHidingUnhidingComponent', () => {
  let component: GradesHidingUnhidingComponent;
  let fixture: ComponentFixture<GradesHidingUnhidingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesHidingUnhidingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesHidingUnhidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
