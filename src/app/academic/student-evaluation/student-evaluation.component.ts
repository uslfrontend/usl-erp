import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';

import { DomSanitizer } from '@angular/platform-browser';
import { StudentLookupComponent } from './../../academic/lookup/student-lookup/student-lookup.component';
import {MatDialog} from '@angular/material';
import { MappingComponent } from './../student-evaluation/mapping/mapping.component';
import Swal from 'sweetalert2';
const swal = Swal;

import { ImageService } from "../image.service"
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  selector: 'app-student-evaluation',
  templateUrl: './student-evaluation.component.html',
  styleUrls: ['./student-evaluation.component.scss']
})
export class StudentEvaluationComponent implements OnInit {
	data=undefined
	progid=''
  progid2=''
	tabledata=[]
	tableArr=[]
	id=''
  stud=[]
  status=[]

  lname=''
  mname=''
  fname=''
  suffix=''
  course=''
  yearOrGradeLevel=''
  image:any = 'assets/noimage.jpg';
  lastattended=''

  group
  constructor(private domSanitizer: DomSanitizer,public global: GlobalService,private api: ApiService,public dialog: MatDialog, private images: ImageService) { }

      ngOnInit() {
        
            if (this.global.activeid!='') {
              this.id=this.global.activeid
              this.keyDownFunctionstudent('onoutfocus')
            }
            
      }

      studentlookup(): void {
        const dialogRef = this.dialog.open(StudentLookupComponent, {
          width: '600px', disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result!='cancel') {
            this.id = result.result;
            this.keyDownFunctionstudent('onoutfocus')
          }
        });
      }

      
      generatePDF() {
        // console.log('Arr',this.grades)

        const groupedBySchoolYearAndTerm = this.tableArr.reduce((acc, item) => {
          const key = `${item.yearLevel}-${item.term}`; // Create a combined key
        
          if (acc[key]) {
            acc[key].push(item);
          } else {
            acc[key] = [item];
          }
        
          return acc;
        }, {});
        
        this.group = Object.values(groupedBySchoolYearAndTerm);
        
        console.log('Grouped: ', this.group);
        var array = this.group


        
                                  // Define an empty array to store the indices you want to check
                                  const indicesToCheck = [];

                                  // Example: Populate the array with indices based on a condition
                                  for (let i = 0; i < this.tableArr.length; i++) {
                                    // You can customize this condition as needed
                                  
                                      indicesToCheck.push(i);
                                    
                                  }
                
                                  // Create the content array dynamically based on the indices
                                  const content = [
                                    
                                  ];
                
                                  for (const index of indicesToCheck) {
                                    content.push({ text: this.checkgrade(index), style: 'grades' });
                                  }
                                  console.table(content)

        function fillsemestertableTrue() {

          
         
					// console.log(array)
					// var current_page = Array.from({length: 10}, (_, index) => index + 1);
					// console.log('PAGE: ',current_page[0])
					var dataRow = [];
					var semTableArray = [];
					var companyNames = []
					// console.log("groupedSy: ", array)
					var res = [];

					// { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'SUBJECT TITLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
					// { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }

					var columns = [
						{ text: 'COURSE NO', bold: true, border: [true, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'DESCRIPTIVE TITLE', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'GRADES', bold: true, border: [false, true, false, true], fillColor: '#dddddd', alignment: 'center' },
						{ text: 'UNITS', bold: true, border: [false, true, true, true], fillColor: '#dddddd', alignment: 'center' }
					];


					res.push(columns);
					var lastCompany = ''
					if (array.length != 0) {
						

						for (var i = 0; i < array.length; i++) {

              var array_sy = array[i][0].term
              console.log('sy',array_sy)

						
							res.push([{ text: array_sy, colSpan: 4, margin: [99, 0, 0, 0], bold: true, }, {}, {}, {}]);
							var tempres = [];
							for (var j = 0; j < array[i].length; j++) {
								dataRow = []




								dataRow.push(
									{ text: array[i][j].subjectId, border: [false, false, true, false] },
									{ text: array[i][j].subjectTitle, border: [false, false, true, false] },
									{ text: '80', alignment: 'center', border: [false, false, true, false] },
									{ text: array[i][j].totalUnits, alignment: 'center', border: [false, false, false, false] },

								);




								// var currentpage = 1
								// var pagecount = 1
								// Check if it is the last row and apply a border to all columns
								// if (j === array[i].length - 1) {
								// 	dataRow.forEach(function (cell) {
								// 		// console.log('Cell: ',cell)
								// 		// cell.border[2] = true; // Apply border to the right side
								// 		cell.border[3] = true; // Apply border to the bottom side

								// 	});
								// }

								tempres.push(dataRow);
								// console.log(dataRow[3])
							}
							res.push(
								[
									{
										table: {
											widths: [90, 345, 40, 40],
											body:
												tempres

										}, fontSize: 9,
										layout: {
											hLineWidth: function (i, node) { return 0.5; },
											vLineWidth: function (i, node) { return 0.5; },
										}
										, colSpan: 4
									},
									{ text: "" },
									{ text: "" },
									{ text: "" },
								]
							)


						}

					}

					else {

						dataRow = []
						dataRow.push(

							{ text: '' },
							{ text: '' },
							{ text: '', alignment: 'center' },
							{ text: '', alignment: 'center' }
						);
						res.push(dataRow)
					}


					// var lastRow = res[res.length - 1];
					// for (var k = 0; k < lastRow.length; k++) {
					// 	lastRow[k].border[2] = true;

					// }

					// res.push([
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' },
					// 	{ text: '' }
					//   ]);
					return res;
				}

				
        var dd = {
					// background: function (currentPage, pageSize) {
					// 	// console.log(`Current page: ${currentPage}`);
					// 	const pageWidth = pageSize.width;
					// 	const pageHeight = pageSize.height;
					// 	const watermarkWidth = 612;
					// 	const watermarkHeight = 936;
					// 	const pageRatio = pageWidth / pageHeight;
					// 	const watermarkRatio = watermarkWidth / watermarkHeight;
					// 	let scale, scaledWatermarkWidth, scaledWatermarkHeight, x, y;

					// 	if (watermarkRatio > pageRatio) {
					// 		scale = pageWidth / watermarkWidth;
					// 	} else {
					// 		scale = pageHeight / watermarkHeight;
					// 	}

					// 	scaledWatermarkWidth = watermarkWidth * scale;
					// 	scaledWatermarkHeight = watermarkHeight * scale;
					// 	x = (pageWidth - scaledWatermarkWidth) / 2;
					// 	y = (pageHeight - scaledWatermarkHeight) / 2;

					// 	return {
					// 		image: "data:image/png;base64," + WaterMark,
					// 		width: scaledWatermarkWidth,
					// 		height: scaledWatermarkHeight,
					// 		opacity: 30,
					// 		absolutePosition: { x: x, y: y }
					// 	};

					// },

					// 		
					pageSize: {
						width: 612, // 8.5 inches * 72 = 612
						height: 936 // 14 inches * 72 = 792
					},

					// watermark: { text: 'USL USL USL USL USL USL USL USL USL USL USL USL USL USL USL ', color: 'gray', opacity: 0.3, bold: true, italics: true },
					content: [
						{
							stack: [
								{
									text: [
										{ text: 'EVALUATION CHECKLIST' },
									], fontSize: 11,
									// marginTop: 15
									bold: true,
									margin: [50, -100, 50, 0],
									alignment: 'center'
								},
								// Existing content...
								{
									// ...
									// New table content
									table: {
										widths: ['auto', '89%'],
										body: [
											[{ text: 'NAME', fontSize: 9, bold: true, margin: [31.5, 0] }, { text: this.fname + ' ' + this.mname + ' ' + this.lname, fontSize: 9, bold: true, }],
											[{ text: 'COURSE', fontSize: 9, bold: true, margin: [26.5, 0] }, { text: this.course, fontSize: 9, bold: true }],
										],
										layout: {
											hLineWidth: function (i, node) {
												if (i === 0 || i === node.table.body.length) {
													return 0;  // Hide horizontal lines at table's top and bottom
												}
												return i === 1 ? 0.5 : 0;  // Customize the line width for the first data row
											},
											vLineWidth: function (i) {
												return 0;  // Hide vertical lines
											},
											paddingLeft: function (i) {
												return i === 0 ? 0 : 5;  // Add padding to the left of the "Name" column
											},
											paddingRight: function (i) {
												return i === 0 ? 5 : 0;  // Add padding to the right of the "Name" column
											}
										}
									},
									margin: [0, 10, 40, 0]
								},
							]
						},
						{
							stack: [
								{ text: '\n' },
								{
									style: 'tableExample',
									table: {
										headerRows: 1,
										pageBreak: 'before',
										dontBreakRows: false,
										keepWithHeaderRows: 1,
										widths: [90, 345, 40, 40],
										body: fillsemestertableTrue(),
									},
									fontSize: 9,
									layout: {
										paddingTop: function (i, node) {
											return 0.2;
											//return (i === node.table.widths.length - 1) ? 5 : 5;
										},
										paddingBottom: function (i, node) {
											return 0.2;
											//return (i === node.table.widths.length - 1) ? 5 : 5;
										},
										hLineWidth: function (i, node) { return 0.5; },
										vLineWidth: function (i, node) { return 0.5; },
									}
								},

								// { text: '\n', margin: [0, 0, 1, 0] } // Add a margin to the element after the table
							]

						},
						// {
						// 	text: [
						// 		{
						// 			text: 'LIZA M. EMPEDRAD, Ph.D.',
						// 			fontSize: 11,
						// 			bold: true,
						// 			alignment: 'right',
						// 			// marginTop: 40

						// 			margin: [50, 50, 50, 0]
						// 		},
						// 		{
						// 			text: 'LIZA M. EMPEDRAD, Ph.D.',
						// 			fontSize: 11,
						// 			bold: true,
						// 			alignment: 'left',
						// 			// marginTop: 40

						// 			margin: [50, 50, 50, 0]
						// 		},

						// 	], fontSize: 11,
						// 	// marginTop: 15
						// 	margin: [50, 15, 50, 0],
						// 	alignment: 'justify'
						// },
						// {
						// 	text: '/vcc 2023                                                                                                                                University Registrar',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	margin: [0, 0, 0, 0]
						// },
						// {
						// 	text: 'University Seal',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	// marginTop: 250
						// 	margin: [0, 0, 0, 0]
						// },
						// {
						// 	text: '/vcc 2023',
						// 	fontSize: 9,
						// 	alignment: 'left',
						// 	// marginTop: 2
						// 	margin: [0, 2, 0, 0]
						// }

					],
					pageMargins: [30, 150, 30, 140],
					// pageMargins: [ 30,300, 30, 140 ],
					// watermark:{text:'UNIVERSITY\nof SAINT LOUIS\nTUGUEGARAO',bold:true,fontSize:16},


				}
      
        pdfMake.createPdf(dd).open('');
      }
      
keyDownFunctionstudent(event){
    this.stud=[]
    this.notcredited=[]
    this.credited=[]
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.global.swalLoading('')   
      this.global.activeid=this.id 
          
          this.api.getStudentEvaluationStudentInfo(this.id,this.global.syear,this.global.domain)
              .map(response => response.json())
              .subscribe(res => {
                if (res.data==null) {
                  this.global.swalAlert(res.message,"","warning")
                }else{

                this.stud = res.data
                this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+res.data.idPicture);
                this.fname = res.data.firstName
                this.mname = res.data.middleName
                this.lname = res.data.lastName
                this.suffix = res.data.suffixName
                this.course = res.data.course
                this.yearOrGradeLevel = res.data.yearOrGradeLevel.toString();
                this.progid = res.data.programID
                this.lastattended=res.data.lastSY
                this.keyDownFunction('onoutfocus',0)
                }
              },Error=>{
                this.global.swalAlertError(Error)
              });
    }
  }

  showcred=false
  textcred='Show Remaining Subject(s)'
  invertcred(){
    if (this.showcred==true){
      this.showcred=false
      this.textcred='Show Remaining Subject(s)'
    }
    else{
      this.textcred='Hide Remaining Subject(s)'
      this.showcred=true
    }
  }

  checklistexist=false
  grades=[]
  acadhist=[]
  keyDownFunction(event,xedia){
  	if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
  		this.data=undefined
      this.tabledata=undefined
      this.tableArr=undefined
      this.progid2 = this.progid
		  this.api.getStudentEvaluationCurriculum(this.progid)
          .map(response => response.json())
          .subscribe(res => {
          	if (res.data!=null) {
	            if (this.global.checkdomain(res.data.departmentId)) {
	              this.data=res.data
                this.api.getStudentEvaluation(this.id,this.progid)      
                .map(response => response.json())
                .subscribe(res => {
                  if (res.data==null) {
                    this.tableArr=[]
                    this.tabledata=[]
                  }else{
                    this.tableArr=[]
                    this.tabledata=[]
                  for (var i1 = 0; i1 < res.data.length; ++i1) {
                      if (res.data[i1].subjectId!='FILI 1013'&&res.data[i1].subjectId!='FILI 1023') {
                        this.tableArr.push(res.data[i1])
                        this.tabledata.push(res.data[i1])
                      }
                    }
                  }
                  this.grades = [];

                  if (res.data.length==0) {
                    this.checklistexist = false
                  }else{
                    this.checklistexist = true
                  }  

                  if (this.checklistexist == true) {
                   this.api.getStudentEvaluationAcademicHistoryEvaluation(this.id)         
                    .map(response => response.json())
                          .subscribe(res => {
                            this.grades = res.data
                            this.api.getStudentEvaluationSubjectAcademicHistoryMapping(this.id,this.progid)        
                              .map(response => response.json())
                              .subscribe(res => {
                                  this.acadhist = res.data
                                  this.api.getStudentEvaluationStatusList()        
                                    .map(response => response.json())
                                    .subscribe(res => {
                                        if (xedia==0) {
                                          this.global.swalClose();
                                        }
                                          this.status=res.data
                                          this.passnotcredited()
                                          this.matchingunsaved()
                                        },Error=>{
                                          this.status=[]
                                          this.global.swalAlertError(Error);
                                        });
                                  },Error=>{
                                    this.acadhist=[]
                                    this.global.swalAlertError(Error);
                                  });
                            },Error=>{
                              this.grades=[]
                              this.global.swalAlertError(Error);
                           });
                  }else{
                     this.grades=[]
                     this.global.swalClose();
                  }
                  

			          },Error=>{
			            //console.log(Error);
			            this.global.swalAlertError(Error);
                  this.tableArr=[]
                  this.tabledata=[]
			         });
	            }else
	            {
	              this.global.swalAlert("Acadims Alert!",'Your view domain does not allow you to view this curriculum.','warning')
	            }
          		// code...
          	}else
          	{
    			    this.tableArr=[]
    			    this.tabledata=[]
          	}

          },Error=>{
            //console.log(Error);
            this.global.swalAlertError(Error);
            console.log(Error)
          });
  	  }
   }
 temporarymapping=[]
 gradecredited=[]
 gradenotincluded=[]
 checkgrade(i){
   var grade = ''
   var savedarr = []
   if (this.acadhist!=undefined&&this.grades!=undefined) {
     for (var b = 0; b < this.acadhist.length; ++b) {
       if (this.tableArr[i].id==this.acadhist[b].studentEvaluationCheckListId) {
                 for (var temp1 = 0; temp1 < this.grades.length; ++temp1) {
           if (this.acadhist[b].academicHistoryRecordId==this.grades[temp1].recordId) {
             if (grade=='') {
               grade = this.grades[temp1].grade
               savedarr.push(this.grades[temp1])
             }else{
               grade = grade + ", " + this.grades[temp1].grade
               savedarr.push(this.grades[temp1])
             }
           }
         }
       }
     }
     // code...
   }

   if (this.grades!=undefined) {
     for (var g = 0; g < this.grades.length; ++g) {
       if ( (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
         ) {
           var condition1 = ''
           for (var temp2 = 0; temp2 < savedarr.length; ++temp2) {
             if (savedarr[temp2].recordId==this.grades[g].recordId) {
               condition1 = 'passed'
             }
           }

           if (condition1=='') {
             if (grade == '') {
               grade = this.grades[g].grade
             }else{
               grade = grade + ", " + this.grades[g].grade
             }
           }

           if (this.temporarymapping.length == 0) {
             this.temporarymapping.push({id:this.tableArr[i].id,recordId:this.grades[g].recordId,subjectId:this.grades[g].subjectId,subjectTitle:this.grades[g].subjectTitle,units:this.grades[g].units,grade:this.grades[g].grade})
           }else{
             var condition2 = ''
             for (var temp3 = 0; temp3 < this.temporarymapping.length; ++temp3) {
               if (this.temporarymapping[temp3].recordId==this.grades[g].recordId) {
                 condition2 = 'passed'
                 break
               }
             }
             if (condition2=='') {
                this.temporarymapping.push({id:this.tableArr[i].id,recordId:this.grades[g].recordId,subjectId:this.grades[g].subjectId,subjectTitle:this.grades[g].subjectTitle,units:this.grades[g].units,grade:this.grades[g].grade})
             }
           }
         
       }
     }
   }
   return grade
 
 }

 notcredited=[]
 checkcurriculum(g){
   for (var i = 0; i < this.tableArr.length; ++i) {
     if (
         this.tableArr[i].subjectId==this.grades[g].subjectId&&
         this.tableArr[i].subjectTitle==this.grades[g].subjectTitle&&
         this.tableArr[i].totalUnits==this.grades[g].units
       )
       {
        return false
       }
   }
   return true
 }

 credited=[]
 passnotcredited(){
   var pass = 0
   var value = 0
   var pass2 = false
   for (var g = 0; g < this.grades.length; ++g) {
     for (var i = 0; i < this.tableArr.length; ++i) {
       if ( (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
       ){
         this.credited.push({checklistid:this.tableArr[i].id,acadhist:this.grades[g]})
         pass = 1
         break
       }
     }
     if (pass == 0) {
       pass2 = true 
       for (var temp = 0; temp < this.acadhist.length; ++temp) {
           if (this.grades[g].recordId==this.acadhist[temp].academicHistoryRecordId) {
             pass2 = false 
             break
           }else{
             pass2 = true 
           }
       }
       if (pass2) {
                  
                      if (this.grades[g].subjectId!='FILI 1013'&&this.grades[g].subjectId!='FILI 1023') {
                        this.notcredited.push(this.grades[g])
                      }
           
       }
     }
     pass=0
     pass2 = false
   }
 }

  x=''
  sy(x){
    if (x!=null) {
      if (this.x.substring(0,6)==x.substring(0,6)) {
        this.x=x
        return false
      }else
      {
        this.x=x
        return true
      }
    }else
      return false
    
  }

  y=''
  sem(y){
    if (this.y==y) {
      this.y=y
      return false
    }else
    {
      this.y=y
      return true
    }
  }


  getstudgrade(x){
    for (var i = 0; i < this.stud.length; ++i) {
      if (this.stud[i]==x) {
        
        break;
      }
    }
  }

  createchecklisttemp=false
  createchecklist(){
    if (this.createchecklisttemp == false) {
      this.createchecklisttemp=true
      this.api.postStudentEvaluation({
        "idNumber": this.id,
        "programId": this.progid.toString()
        })         
            .map(response => response.json())
             .subscribe(res => {
                  this.keyDownFunctionstudent('onoutfocus')
                  this.createchecklisttemp=false
                },Error=>{
                  this.createchecklisttemp=false
                  this.global.swalAlertError(Error);
        });
    }
  }

  deletechecklisttemp=false
  deletechecklist(){
    if (this.deletechecklisttemp == false) {
      this.deletechecklisttemp=true
      this.api.deleteStudentEvaluation(this.id,this.progid)       
        .map(response => response.json())
        .subscribe(res => {
              this.keyDownFunctionstudent('onoutfocus')
              this.deletechecklisttemp=false
            },Error=>{
              this.deletechecklisttemp=false
              this.global.swalAlertError(Error);
        });
    }
  }

  selectnotcreditedsubjvar = ''
  selectnotcreditedsubj(x){
    this.selectnotcreditedsubjvar = x;
  }

  openaddmapping(x,h){
    var y
    if (this.selectnotcreditedsubjvar!='') {
      y = this.selectnotcreditedsubjvar
    }else
      y = ''
    var mappingpass=[]
    for (var temp1 = 0; temp1 < this.temporarymapping.length; ++temp1) {
      if (x.id==this.temporarymapping[temp1].id) {
        mappingpass.push(this.temporarymapping[temp1])
      }
    }

    const dialogRef = this.dialog.open(MappingComponent, {
          width: '95%', disableClose: true, data:{lastsy:this.lastattended,map:mappingpass,color:h,grades:this.grades,checklist:x,status:this.status,acadhistid:y,notcredited:this.notcredited,credited:this.credited,mapping:this.acadhist,progid:this.progid,id:this.id}
        });

        dialogRef.afterClosed().subscribe(result => {
          //console.log(result)
          if (result!=undefined) {
            if (result.result==1) {
              this.keyDownFunctionstudent('onoutfocus')
            }
          }
        });
  }



  removechecklist(){
    this.swalConfirm("Are you sure?","You won't be able to revert this!",'warning','Delete checklist','checklist has been deleted','','sy');
  }

  swalConfirm(title,text,type,button,d1,d2,remove)
  {
    swal.fire({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          if (remove=='sy') {
            this.deletechecklist()
          }
        }
      })
  }

  notsaved=[]
  checkcolor(i){
  var grade = 'white'
  var subj=[]
  var acadhist=[]
     if (this.grades!=undefined) {
       for (var g = 0; g < this.grades.length; ++g) {

         if ( (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
           ) {
             subj.push(this.grades[g])
             if (this.acadhist.length==0) {
               grade = '#fcedd2'
             }else
             for (var l = 0; l < this.acadhist.length; ++l) {
               if (this.grades[g].recordId==this.acadhist[l].academicHistoryRecordId) {
                 acadhist.push(this.grades[g])
               }else{
                 grade = '#fcedd2' 
               }

             }
         }
       }
     }

     if (subj.length==acadhist.length) {
       grade = 'white'
     }

     return grade
   
  }
  
  matching=[]
  matchingunsaved(){
    for (var i = 0; i < this.tableArr.length; ++i) {
       for (var g = 0; g < this.grades.length; ++g) {
         if ( (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
           ) {
             if (this.matching.length==0) {
               this.matching.push({id:this.tableArr[i].id,acadhistid:this.grades[g].recordId,subjectTitle:this.tableArr[i].subjectTitle})
             }else
             {
               var condition = true
               for (var zero = 0; zero < this.matching.length; ++zero) {
                 if (this.matching[zero].acadhistid==this.grades[g].recordId) {
                   condition=false
                 }
               }
               if (condition) {
                 this.matching.push({id:this.tableArr[i].id,acadhistid:this.grades[g].recordId,subjectTitle:this.tableArr[i].subjectTitle})
               }
             }
            }
       }
    }
  }
  savefuncvar=false
  savefunc(){
    this.global.swalLoading('Saving...')
    this.savefuncvar=true
    this.saveunsave(this.matching.length-1)
  }

  saveunsave(x){
    if (x>=0) {
      this.api.postStudentEvaluationSubjectAcademicHistoryMapping({
          "studentEvaluationChecklistId": parseInt(this.matching[x].id),
          "academicHistoryRecordId": parseInt(this.matching[x].acadhistid),
          "statusId": 0
          })         
              .map(response => response.json())
               .subscribe(res => {
                    this.saveunsave(x-1)
                      this.api.postStudentEvaluationHistory({
                            "idNumber": this.id,
                            "programId": this.progid,
                            "schoolYear": this.lastattended,
                            "evaluatedBy": this.global.requestid()
                          })         
                          .map(response => response.json())
                          .subscribe(res => {
                              },Error=>{
                                this.global.swalAlertError(Error);
                              });
                  },Error=>{
                    this.savefuncvar=false
                    this.global.swalClose();
                    this.global.swalAlertError(Error);
          });
    }else{
      this.global.swalClose();
      this.global.swalSuccess('Evaluation Saved!')
      this.savefuncvar=false
      this.notcredited=[]
      this.credited=[]
      this.keyDownFunction('onoutfocus',1)
    }
  }

   eval(id){
    this.Confirmeval("Save evaluation?","",'warning','Save',' ','','sy');
  }
  
  Confirmeval(title,text,type,button,d1,d2,remove)
  {
    swal.fire({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          this.savefunc()
        }
      })
   }
}
