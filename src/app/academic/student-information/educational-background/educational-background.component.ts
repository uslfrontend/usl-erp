import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-educational-background',
  templateUrl: './educational-background.component.html',
  styleUrls: ['./educational-background.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EducationalBackgroundComponent implements OnInit {
  
  yeargrad=''
  otherinfo=''
  arrayproglevel
  proglevel=''
  arrayprog
  prog=''
  arrayschool
  options: string[] =[]

  progname=''
  schoolName
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  disabled=false;
  constructor(public dialogRef: MatDialogRef<EducationalBackgroundComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private api: ApiService,public global: GlobalService) { }
  
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  ngOnInit() {
      if(this.data.seek==1){
        this.proglevel = this.data.data.programLevel.toString()
        if (this.proglevel='0') {
          this.proglevel = '00';
        }
        this.getprogram(this.proglevel)
        this.disabled = true;
        this.prog = this.data.data.programID.toString()
        this.myControl.setValue(this.data.data.schoolName);
        this.otherinfo = this.data.data.otherInfo
        this.yeargrad = this.data.data.yearGraduated
        //this.myControl.valuesetValue() = this.data.data.programID
      }


    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.api.getPublicAPIProgramLevels()
      .map(response => response.json())
      .subscribe(res => {
        this.arrayproglevel = res
          this.api.getPublicAPISchools()
            .map(response => response.json())
            .subscribe(res => {
              this.arrayschool =res
              for (var i = 0; i < this.arrayschool.length; ++i) {
                  this.options.push(this.arrayschool[i].companyName)
              }
            });
      });


  }
  assign(x){
    this.progname=x
  }
  getprogram(prog){
    this.api.getPublicAPIProgramNames(prog)
      .map(response => response.json())
      .subscribe(res => {
        this.arrayprog = res
      },Error=>{
        this.global.swalAlertError(Error);
      });
  }
 onNoClickclose(): void {
       this.dialogRef.close({result:'cancel'});
  }

  save(){
    if (this.proglevel!=''&&this.prog!=''&&this.yeargrad!='') {
      // code...
    if (this.options.indexOf(this.myControl.value)!=-1) {
      this.api.putStudentEducBG(this.data.id ,{
                    "programLevel": this.proglevel.toString(),
                    "programID": this.prog.toString(),
                    "programName": this.progname,
                    "schoolName": this.myControl.value,
                    "yearGraduated": this.yeargrad,
                    "otherInfo": this.otherinfo,"companyID": this.arrayschool[this.options.indexOf(this.myControl.value)].companyID,})
                    .map(response => response.json())
                    .subscribe(res => {
                      this.global.swalSuccess(res.message);
                       this.dialogRef.close({result:'nice'});
                    },Error=>{
                      this.global.swalAlertError(Error);
                    });
    }else 
      this.global.swalAlert("School Name must be selected in the list!","",'warning');
  
    
    }else 
      this.global.swalAlert("Please fill in the required fields!","",'warning');
  
    
  }
  saveadd(){
    if (this.proglevel!=''&&this.prog!=''&&this.yeargrad!='') {
      // code...
    if (this.options.indexOf(this.myControl.value)!=-1) {
      this.api.postStudentEducBG(this.data.id ,{
                "programLevel": this.proglevel.toString(),
                "programID": this.prog.toString(),
                "programName": this.progname,
                "schoolName": this.myControl.value,
                "yearGraduated": this.yeargrad,
                "otherInfo": this.otherinfo,"companyID": this.arrayschool[this.options.indexOf(this.myControl.value)].companyID,
              })
                    .map(response => response.json())
                    .subscribe(res => {
                      this.global.swalSuccess(res.message);
                       this.dialogRef.close({result:'nice'});
                    },Error=>{
                      this.global.swalAlertError(Error);
                      console.log(Error)
                    });
    }else 
      this.global.swalAlert("School Name must be selected in the list!","",'warning');
  
    
    }else 
      this.global.swalAlert("Please fill in the required fields!","",'warning');
  
    
  }

}
