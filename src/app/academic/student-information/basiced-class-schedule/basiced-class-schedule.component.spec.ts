import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicedClassScheduleComponent } from './basiced-class-schedule.component';

describe('BasicedClassScheduleComponent', () => {
  let component: BasicedClassScheduleComponent;
  let fixture: ComponentFixture<BasicedClassScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicedClassScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicedClassScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
