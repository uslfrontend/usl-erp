import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import { Inject} from '@angular/core';

import { ApiService } from './../../../api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-basiced-class-schedule',
  templateUrl: './basiced-class-schedule.component.html',
  styleUrls: ['./basiced-class-schedule.component.css']
})
export class BasicedClassScheduleComponent implements OnInit {

  tableArr=[]
  tableArr2=[]
  sy=''
  elective='';
  section = '';
  constructor( public dialogRef: MatDialogRef<BasicedClassScheduleComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private domSanitizer: DomSanitizer,public global: GlobalService,public api:ApiService) { 
	this.tableArr = [];
	this.tableArr2=[];
	this.global.HSClassSchedule = [];
    this.global.noHSClassSchedule = [];
	this.elective='';
	this.section = '';
  }


  
  ngOnInit() {
	this.tableArr = [];
	this.tableArr2=[];
	this.global.HSClassSchedule = [];
    this.global.noHSClassSchedule = [];
	this.elective='';
	this.section = '';

    this.sy=this.global.syear
      if (this.data.studinfo.departmentCode=='ELEM'||this.data.studinfo.departmentCode=='HS') {
        if (this.global.syear.length==7) {
          this.sy = this.global.syear.slice(0, -1)
        }
      }
      this.getSection(this.sy);
      this.api.getStudentBasicEdEnrollmentElective(this.sy,this.data.id)
              .subscribe(res => {
                if (res.data!=null) {
                  if (res.data.length!=0) {
                    this.elective = res.data[0].name
                    if (this.elective == "Bread and Pastry Production") {
                      this.elective = 'BPP'
                    }
                  }
                }

          	   if (this.global.HSClassSchedule.length==0) {
                 if (this.data.enrollmentHistory[this.data.enrollmentHistory.length-1].section==""||this.data.enrollmentHistory[this.data.enrollmentHistory.length-1].section==null) {
                   this.data.enrollmentHistory[this.data.enrollmentHistory.length-1].section = 'noSection'
                 }
				 if(this.section != ''){
					this.api.getStudentBasicEdStudentSchedule(this.sy,this.section,this.data.id)
                    .subscribe(res => {
                    	this.tableArr=[]
                    	if (res.data!=null) {
                    		var temp=''
                    		var tableArrtemp=0
            				  this.tableArr.push({
                				time:'',
                				m:'',
                				t:'',
                				w:'',
                				th:'',
                				f:'',
                			})

                			var x
                			for (var i = 0; i < length; ++i) {
                				if (res.data[i].time != ' ') {
                					x = i
                					break
                				}
                			}
                      if (res.data[x]!=undefined) {
                  			this.tableArr[0] = {
                  				time: res.data[x].time,
                  				m:'',
                  				t:'',
                  				w:'',
                  				th:'',
                  				f:'',
                  			}
                        // code...
                      }
                			for (var i = 0; i < res.data.length; ++i) {
                    				if (res.data[i].time != ' ') {
                    					var time = res.data[i].time
                    					var check = false
        	            				for (var i2 = 0; i2 < this.tableArr.length; ++i2) {
        	            					if (this.tableArr[i2].time == res.data[i].time) {
        	            						check = true
        	            						break
        	            					}
        	            				}
        	            				if (!check) {
        	            					tableArrtemp++
        	            					this.tableArr[tableArrtemp] = {
        				        				time: res.data[i].time,
        				        				m:'',
        				        				t:'',
        				        				w:'',
        				        				th:'',
        				        				f:'',
        				        			}
        	            				}
                    				}
                    			}

                    			var start = 7
                    			var temparr=[]
                    			for (var i = 0; i < this.tableArr.length; ++i) {
                    				var keyA = this.tableArr[i].time.split(":")
                            if (keyA[1]!=undefined) {
                      				var keyb = keyA[1].split("-")
                      				temparr.push({sort:parseFloat(keyA[0]+'.'+keyb[0]),index:i})
                            }
                    			}
                    			sortByKey(temparr, 'sort')
                    			function sortByKey(array, key) {
          						    return array.sort(function(a, b) {
          						        var x = a[key]; var y = b[key];
          						        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
          						    });
        						}

        						var arr1 = []
        						var arr2 = []

        						for (var i = 0; i < temparr.length; ++i) {
        							if (temparr[i].sort>=7) {
        								arr1.push(temparr[i])
        							}else{
        								arr2.push(temparr[i])
        							}
        						}
        						var arr3 = arr1.concat(arr2)
        						var arr4 = []

        						for (var i = 0; i < arr3.length; ++i) {
        							arr4.push(this.tableArr[arr3[i].index])
        						}
        						this.tableArr = arr4
                    this.global.HSClassSchedule=this.tableArr
                    			for (var i3 = 0; i3 < this.tableArr.length; ++i3) {
        	            			for (var i = 0; i < res.data.length; ++i) {
        	            				if (this.tableArr[i3].time == res.data[i].time) {
        	            						if (res.data[i].teacher==null) {
        				        					res.data[i].teacher= ''
        				        				}else
        				        					res.data[i].teacher="<hr>"+res.data[i].teacher

                              if (res.data[i].subject.toUpperCase().includes("TLE")&&this.data.studinfo.departmentCode=='HS'&&this.data.studinfo.yearOrGradeLevel==4) {
                               res.data[i].subject=''
                               res.data[i].teacher=''
                              }

                              if (res.data[i].subject.toUpperCase().includes(this.elective.toUpperCase())) {
                                res.data[i].subject = '<span style="margin: 0;background-color: #006600;color: white;padding: 3px;border-radius: 5px">'+ res.data[i].subject+ "</span>"
                              }else
                                res.data[i].subject = '<span>'+ res.data[i].subject+ "</span>"


        	            			  if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('mon')) {
        				        				this.tableArr[i3].m =  this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('tues')) {
        				        				this.tableArr[i3].t =this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('wed')) {
        				        				this.tableArr[i3].w =  this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('thurs')){
        				        				this.tableArr[i3].th = this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('fri')) {
        				        				this.tableArr[i3].f = this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        	            				}
        	            			}
                    			}


                    			for (var i = 0; i < res.data.length; ++i) {
                    				if (res.data[i].time==' ') {
                    					this.tableArr2.push(res.data[i])
                    				}
                    			}
                          this.global.HSClassSchedule=this.tableArr
                          this.global.noHSClassSchedule=this.tableArr2
        	            	}
                    },Error=>{
                        this.global.swalAlertError()
                        //this.global.logout()
                      });
				 }else{
					this.global.swalAlert('The student does not belong to any section for schoolyear: '+this.sy,'','warning');
				 }
                 
          	   }else{
                 this.tableArr = this.global.HSClassSchedule;
                 this.tableArr2 = this.global.noHSClassSchedule;
          	   }
         },Error=>{
            this.global.swalAlertError()
            //this.global.logout()
          });
  }

 
  getSection(sy){
    this.data.enrollmentHistory.forEach(element => {
		  if (element.schoolYear == sy) {
			this.section = element.section;
		  }
	  });
	}
  checkcolor(sub){
      if (sub.toUpperCase().includes(this.elective.toUpperCase())) {
        return true
      }else
        return false
  }
  checkcolor2(sub){
      if (!sub.toUpperCase().includes(this.elective.toUpperCase())) {
        return true
      }else
        return false
  }

}
