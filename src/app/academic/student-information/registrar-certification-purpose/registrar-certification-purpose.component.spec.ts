import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarCertificationPurposeComponent } from './registrar-certification-purpose.component';

describe('RegistrarCertificationPurposeComponent', () => {
  let component: RegistrarCertificationPurposeComponent;
  let fixture: ComponentFixture<RegistrarCertificationPurposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarCertificationPurposeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarCertificationPurposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
