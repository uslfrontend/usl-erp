import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-registrar-certification-purpose',
  templateUrl: './registrar-certification-purpose.component.html',
  styleUrls: ['./registrar-certification-purpose.component.scss']
})
export class RegistrarCertificationPurposeComponent implements OnInit {
  purpose = this.data.purpose
  

  constructor(public dialogRef: MatDialogRef<RegistrarCertificationPurposeComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  download(){
    this.dialogRef.close({ result: this.purpose });
  }

}
