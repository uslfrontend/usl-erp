import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import { Inject} from '@angular/core';

import { ApiService } from './../../../api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-class-schedule',
  templateUrl: './class-schedule.component.html',
  styleUrls: ['./class-schedule.component.css']
})
export class ClassScheduleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ClassScheduleComponent>,@Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService) { }
  classScheduleArray
  ngOnInit() {
    this.api.getStudentClassSchedule(this.data.id, this.global.syear)
          .map(response => response.json())
          .subscribe(res => {
            this.classScheduleArray = res.data

          });
  }

}
