import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { AddressLookupComponent } from './../../academic/student-information/address-lookup/address-lookup.component';
import { FamilyBackgroundComponent } from './family-background/family-background.component';
import { EducationalBackgroundComponent } from './educational-background/educational-background.component';
import { FamilyBackgroundGuardianComponent } from './family-background-guardian/family-background-guardian.component';
import { FamilyBackgroundFamilymemberComponent } from './family-background-familymember/family-background-familymember.component';
import { ClassScheduleComponent } from './../../academic/student-information/class-schedule/class-schedule.component';

import { StudentLookupComponent } from './../../academic/lookup/student-lookup/student-lookup.component';
import Swal from 'sweetalert2'; import { map } from 'rxjs/operators';
const swal = Swal;
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

import * as moment from 'moment';
import { BasicedClassScheduleComponent } from './basiced-class-schedule/basiced-class-schedule.component';
import { RegistrarCertificationPurposeComponent } from './registrar-certification-purpose/registrar-certification-purpose.component'
import { Base64ImagesService } from './../../services/base64-images.service';
import { UploadIdSignatureComponent } from '../../academic/student-information/upload-id-signature/upload-id-signature.component';

@Component({
  selector: 'app-student-information',
  templateUrl: './student-information.component.html',
  styleUrls: ['./student-information.component.scss']
})
export class StudentInformationComponent implements OnInit {
  image: any = 'assets/noimage.jpg';
  signature: any = 'assets/nosignature.jpg'
  id = '';
  checkId = '';
  lrno = '';
  fname = '';
  mname = '';
  lname = '';
  suffix = '';
  //basic
  gender = '';
  cstatus = '';
  bdate = '';
  nationality = '';
  religion = '';
  placeob = '';
  //contact
  tno = '';
  cno = '';
  email = '';

  homeaddress = '';
  street = '';
  boardingaddress = '';
  street2 = '';
  permPSGC
  currPSGC

  familyarray
  educarray

  hidden = true

  //#region USL logo
  logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAB8CAYAAACfZxWiAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAR3FJREFUeNrsnXd4VMX+/1/nnK3Z9B7SIIQSeu/SpIOAgGAFKSqL14INGyqIDXsjCiKiAioCgggIUqQ36TUJqaT3tn3P+f2xhYTi1Svc672/7zzPeTbZPWfOzLzn0z8zIyiKwn95iQBGAWFAJrAWqPlv7YzqvxyMBYDxKt8nAzP+vwSkSfOpv/u71eogKSmWlJSLdO7UlIqKWtIu5BETHYrd7qRjh0TMFhsHD52nqspEkybRmEwWnpk1nri4cOx25xV1durYJBSYCxirq83s+PUEWdlFtGwRT88eLdBoVEaAQ4dTfhcUSRIpK6vmxZeWUVZeRauW8QQF+bFn72kiI4NRZAWL1U5hYTlOp4y/vw8NGoQgCMJ1A2Dj+rn/HRSiUqkQBLhG3+cCxpoaM88+v4Qf1x/A6ZTR6TXce89AHnl4FL6+emPnTk3/KSiiKHAdx/d/j2UJgoDDIbN33xnatknAYrFBnQG7+87+cR4wbhk1hzNns9Bo1AiCgMVs4533VlNUVM6770xHFAVj505NHzt1OstS/y0KsqwgSiLl5TWYzbbrOuv/52SILMus/+kgEeGBREYG4XTKdX+eCrDll6OcOJmOwaCrB6bBoOPblbsYO7YXvW9qDXDH18u2LQEXrgoKgiCg12txOJwUFlZgtzv/D5DfZ1cSpaVVLFq8iajIIGpNVmTZBcqE8X36AqxctQu9XnvV5zUaFbt2nfYAkvjpwg1uwMDgqycsxJ+gYD8iI4Jw2J2IokB9zP8PkKuCUlFRi1qtZsDNbQkM9MXpAqUYoHmzWLZtO45Gc2UXnE6ZgECD519748ZRqCQRs8WG06kQEGBAq1GjVqtQ5L+X2i/+nVVASRIRRYHy8lqCg3zx99UDLAG4dXQPbDY7l9tRDocTg0HLnXf09Xy16rV59/Lcc3cQGhqIw+Hk72x7/a0B8VBKemYhP28+4hLwsBGgVct4Pv/sMdRqF4VYLDbsdidhYQEs+vQRgoP8AJLffX/1yeMn0nn/g7Wkp+ejVkt/6/7+bQFJPbdYm566pMfeXW9P3bJxntpqc9CuXSIHDp6TgWRBEBg9qjvTHxiGxWJjxPCuBAYaeOH5O+nTu42nmm/8/HxITcvj9OlM1GqJk8eSG+/YNl/zf5b6HyxbNr3iD7x+mQX+2fq1LyUDLwOzAaPD4eTThRv4dOFG7A4nE++5mWdmjeeJpz5DURTGjb0J4NdpU4YkT5sy5JFPkx++yV3nOIAN6+cmAw/dedcbzv8D5LKyeNGjHdyDFQSMBbDZHJw8lUl2dhG33NIVlSR5ADKWl1cz65nPadOmEYmJDdiz5zQ1NRaaNInmyy+e4KW5X3M+JZdHHx6NwaDzPgeQk1NMZFQwapVkBIzLl81KBp4FAoCc+6d/IP9/y7L27XlH+ujDGQuA34BpHjC++HILI0e/yNx5yzh+Ir2uHWI8czabaQ98wPT7h/OPGSNdtkUdIR0QYODdtx+gSWIDHjB+SHp6PoCxttbCjH98zIMPLWDo8Nls+eUoskvDMgLlbsekc+EnDy/4/w6Q9NQliTt3vLkAcHgG65nnljDt/nfJzCxEo1bRsmVD1q5+kWefvh2tRg2wGWDhoo2EhvjRoUOiy+a+hsY0/rbeVFbW8OXXWwHIyi7i+Il0liyeyRuvTubtd1dRVWUCoLraTF5eqacu4+kTn7bYuvm1Fv/zgHz3zbO+mze9sgBIBYweY+/lV1YQGGBg1MgePPb4QpKS4sjIKOT2O1/n1de+8Tx+AaBr12akXcj/p+/KzSvl5Kks+vVtC0DD+HACAwwMGvocx09ksPyrWQQGGlAUmPXM59wyeg6VlbWex08Dp7f98vqCvbveNvxPApKR9sUCoBowmkxW3npnFSUlVQBkZhYydcoQbhnRlYceGsXPm39Dr9cwfFhnHnl4tFfeA8THhWO12HA4fl8Wp6XlUltrpnFCFIoCBw6c57tvnuXZp2/n3ffXcORoGgCff/Ez+QVlNG0Sjb+/j7c9mVmFHnZWcyHl8wX/M4BkZyyN/3nDvAWA0WKxsWfvGXx8tGRnF1NdYwagZ48W7N13FoBmzWIoK62mf7+2mMxWgoJ8wRXfSAOIiw0nv6CMnJzi333v2bMXiYkOJTTUH6fTybcrd/L4k4tQqSWCgnxp2DCCV1//lnXr9vOPGbfg769HFEUcDifPPLeEzVuOUFlZ67F9jJkXvlhw4ugC8b8akNRzi3XALMB48lQm815dwedLfubFOV8hyzLZ2a5BvfPOfqxZs4dz53K4cCGf8ooa+vdvR0FBuceHZfS4TPz89IiiwJmz2b/77pTUXLp2aY5Op0GlkvjgPSMdOzRh/foDzH7uDnbvOUNGRgF9+7Thjfkr8fNzUcey5dsBuH/aUKbP+Ii585Z7ZQvg3PTTywv+1oBYLLYrLpvNTuq5xQsAM2BcvWYPd979Om3bJLDwk0do3aoRhw+nUlhYDkBIsB+zn7+TZ55fwmtvfMPUKYNp1DCCF2ffhSiKHgqxuwDxoU2bBH77Le2abbLbHRw4eI7QsACvHaPRqLhv2hAWffoIAwd0IDwsgDdem8LMR2+lT5/W+Bp01JosfL1sG6+/NoU1P+zl150n0eu1CIJAVZXJq5VtXD+3vce7/Fev626HjB3T6wr/09tv3qcDjFarnTU/7GXR4k20bBFPbGwYkiQybmwvOnZI5KW5XzN0SCcCAgwkJESyeuVs7Hanx2GYDDiBbGAAUAKugFK/Pm35decJvC7cy0p+fhl5eaXEx4d7vprm/lwN6ICsYUM7ew3P6AYhOBxOFi3ayKiR3XE6ZN7/cC0T7+mPTqcGYPqDHzF+3E2MHtUd4MjG9XOTBw557rqHif8yID171NcObxt3kzfOvXPXSdau28+6NS/idMqMHT+Pjz94kKZNo2nUKJIhgzux/qeD3HVnP9zuEGMdMMI8VjVATY2Z9IwCcnKK2bP3NJWVJhRF4WpRjOMnMlyh4rRcNv18mMYJUcTEhKHXa8bUue1T4BPg6L2TBlJWVs33q3YzfFgXHn40mRdn30Vqai5ms5U5Ly8jPi4cURSYO285s5+7A0EQjFs2vUKjxHtn/K0Aqavt3HF73wWA0emUWb5iB3FxYXTr1px331vD88/dwaJPHmHuvGW8+cY0QkL8mTC+Dzab3fP4LGCbG0wjQFFxBTt3nmT7jhPs23+OvLxSzGYrTqdMQkIkFRW1SJJ4FYGejd3uIPmTDSxctAm9XkNCQiTdujZn0MCO9OzRAoNB9wDwALASKAkO9jPef99QTp7MoG+fNvTr24YTJzP4ctk2undtxnvvTMfhcLLux/3MfHwh7759P4IgGDPSviAm/p7rBorwV13Rw255AYANP85VAXanU2bOy8tITc1j+gPD6NO7NW+/s5raWgsvzL6TX7Ye48jRNJ58fGxdHpqMK6A3HeDsuRyWr9jO6tV7yCsow2F3YjDoiIkOoWmzGEJD/Fn/00EWffoIH368ji1bjvDtimcZMbwLiqIwcfJbHD+WzoCb25OZVUhKai4FBeXYbHbUajVNmjRg/LibGDWqO40Touq2wetikWWFyVPfJju7iB/WvESAvw9ZWUU8/GgyhYXlDB7UkSefGIevKyQQEJ8wqepfGb+s9KXXl0Jqaizs3D7fF5jvkSHHjl3g8ZljOH/+Is+/sJSpkwej1aqY/9b3PPHYGPr0buUBo94gnDyVyWefbeLblTsxmayo1RId2icybGhn+tzUimbNYvHz0wNw5EgaW7YcQa2q706vqKjl119Pctcd/Xjt1ckAlJRUcep0Jtu2H2frtmOcPp3Fy6+s4IOP1jFhfB8eeXgkDaJCjHWAMQKMvKUbHTs0IcDfh5ycYm6/8zWemTWBwYM78vmSnz0BM4Bmjz166yHl70AhbiH5DmDct/8sGzYeol3bBJZ/s4P09HzmzZ3EL1uPMXxYZ8wWG4MGdECrVdcDw253sCD5J15/41tMZis+PlqGDO7E7eP7cPPN7VGprmRL02d8SEVFDRarnS2/HOXb5c8wYngXDh1Oof+AWcx58R4emznmiufMZhs/bTjIl19tZc/e08iyQlxcGLOeHM+E8b09LDC5rrf54MHzPP7UIua8eA/9+7W91jgkr16950+zrjFjel53b68RMKal5fH8C19yx+19+WXrMea/NpXw8EDOnc8hM7MAk8nKyFu6XcEeLl4sZsZDC9iz5zSKotC3Txsef3wsvXu1+t2Xdu3ajI8+/hF/Px+PagzAoUMp2B0ybdsmXPU5vV7DuLG9GDe2Fz+s3cv7H/zAkaMXmPn4p+zcdZJ5cycRGupvdLfxN+Azk9nGRx88SNs2jeq1varKxIJP1jP21l40adLAOGZMzyWjx7x86D8JSLhnJk27/z3GjbuJaVMG8/Pm3/jy618wTh/BN9/+Sr9+bRk+vIvnmTbACYBDh1MYM3YuNbUWfH31PPXkbS4P7h9IAElqHkdNjRlBEBDFS5lCqWl5BAf50rJl/D+tY/SoHgwc0IF5r6zgs8838d33uzh0OIU1q2YTFxvuASW5b5/WV7CzU6cymfnEQtq1bYyPjzfZouTAwXP8FZ7zVwF5CWiya9cpHn5oFDt+PcmMf3yMwUdLbGw4r7/xLf36tmXUyO51KeMEwIaNh5g05W0cdieNGkWx4MMZdOuW9IdfHBrqj81mp7ioApUoIYguSI4eSyM2NoyQYL8/VI/BoOO1VyfTvn1jnnluCZmZhfS7eRarV86mbdsEDyi3uv1wv9TWWvhs8c9s2HiIRx8ezfBhXer2rUqjUf2lmP1fsdQXAMaMzAIenvkJPXu05IP3ptOsaTSyrNC1czPeeev+ug1u4vnDA4YiK3Tp3JT16+b8KTBc3tsIwsICsdrsIIBOq8ZqtZOalkeH9o29sfY/Wsbf1pvVK2cTFxdOVZWZUWPmcPTYBQ9LHgTcDPDF0i2sW7+f1d8/7+nbyjqsuyQrfemC7Iwv9f9uChEAo6IoPD97KU88NhYfHw3fr9rNIw+P5uSpTJZ++QvPPj2BYNdMTQYeA4xHjqZx18T56LQaIqMDeemFu5FEgZyLxX+yAQItkuJITy9AEARKS6vZuu0YFeU1hIYGcPFiCcqfZB5RUcG88PydzHz8U6xWO+PGv8LunW8RFRnsoRTGju3F5l+OcPZcDp06NgG4LS+/jJKSSuJiwwgM9PWwt3/JNvlXtaxmwLmiogoee3IRUZHBlJRU0rhxFM8/ewfPzV7KTb1aMnhQJ488SAaMBYXl9Lt5FlVVJkRRQKfToNGosdvt/9KcUBQFi8WVBqrRqJBlBbvdgVardgt65U/XqdGoMJut2GxOHA4HSc3jWL9ujkdOJAPGX3ee5K23V/HN8qdZ/s0O1v24Hz9fPXn5Zbz/zgMehSL5jxiMF7O+ui6AvAc88vob3xEXF0aDBiF8tvhnIiKCaNumEZ8t3sT6dXM88YVkj2o7dMRsTp7M9Ki9KIqC0ymjKJcif7KiIDtl7/8qlYRKdfXUHVeitAtxt+MPURSQZeWafNxud7hCwoLLOSiJotvR56Y7wWVLeeo1m61MmTyI11+d4v3O6ZQpKali5apdnDqVyfzXp+Lv78Padfv4fMlmVn8/26M+q1u1NTp+byBPHU++LixLA2CcPpz5b31Pba2VpUse57cjabz73mrmzrmnLhgBAJ8s3MDx4+mo1SqvdiRJIhqNGkkS0WpVqFQSgQEGQsMC0es1KLJCUXEF2dnFWK32q7pJ6oJT/+9L/yuKK1/YYNDSMD6OkBB/nE4Zk8lKcXEFVdVm7HYndrsdh0PGYrF5QfXx0bLos00MG9qFPr1bAyRLkmiMiAjkxx/389nCR72BrRYt4snNK8NqtXsoSqfVqmtuNMtqDKRlZxfh66vH39+HFd/8ytlz2Twza4LXkq5va5TQqevDKIpCXFw4Dz14C/7+PoSFBRARHoTeR4tep8FkslJWXk16ej7bth/n9Jls8vJKsdsdqFQSZrMNm8014TQaiX/WdM+gajQqtFo1dpsDH4OO2JhQ2rVL4KZerYmLCyM4yA+NVoXZZKPWZCE/v4yysmpKSqp4573VVFTUEhoawK4db3oCZhuAYfc98D5JzeMYf9tN5OaVMmfuMqY/MKyevRWfMHHG77tOvvzLgCwAjPPf+p6cnGJkWSEgwIcTJzKorKrl0+SHaZEUB9Ad2CfLCpMmv8WWX45gtztZ9tVTDBncqU6oNY9jx9PZtfsUR49eIO1CHk6nTIMGIYSHBWAw6CgsKqe4uBJFgVtGdEUURZav2HFVC74uGKNGdnO5dnadRKfTEBkZRHCQLxWVJgoKyigqqkCv15LUPJZOHZvQvXsL2rZpRGxsmLeeDz9ay4tzvkaSJJ59ZgIzH7kV4Gvg7oKCcua8/DUmk5XAQF+GDe1M44QoNmw6xMS7byYw0BcgrH2nB0uu1c6jhz/+SyyrNWBMu5CHKApMmjiAxMQG6HUa7HYnOReLiY3xdmaixy7Y9PNhRFGkQ/vGRIQH8snCnzhw8DxHj14gPb0Ah8NJdHQIHdonMnBAOwRRJCOjgLNnc9xqbCJPPDaOnj1bEB4WyPZfT/DF0i2oVNprk74AI4Z3YeiQzmRlFbJn71mWfvULJ09lERMTSt++bYiJDqW62syxY+ks/Wor77y3Br1eQ7NmMXRol0ivXi3p2bMlCQlR5OaW8Mb87xhza0/i48LvBpIjI4OMyR8/RFlZNV99vZXlK3aQnV1E27YJdVd+aeQ/ken1ZwExAvga9AQH+fHD2r2UlFTRtEk07dsn0rVLs3raiNMp88KLX6FWqxBFkbQL+QwY/CzV1WYiIgJp164xE8b3JjGxAQ67k7Pncti24wSpqbk0SYxm0qQBDOjfnoSEqHrWu93m+Oe8WBCornbF7OPjI4iPj2D8bTdx6nQWq1btZuPPh9lQeogWSXH07JHE5HsHYrM5SEnJZefuk3y/ejeLFm8kONjPGwa22RwsX76dZ56eABDiCYZNvPctOnVswgzjCFq3bohP/WUSz5eVVc24ETJEAhyyLPPs80tRSSItWzYkLjYUlVri0OFUunVt7tHNJwDfHj+RwdDhzyNJIrKs0Lp1Q3p0b0Gvni1oGB9BRmYhu/ecZtv245w7l0NS81imTB5Mn96t60b7rijbtx9n3IRXMRi0v8uyPl3wUF2XzVUyU/L48acDLP78Z0rLqmndsiGDBnWga5dmREUGk5Kay67dp9i79wwX0gvcGpjEr9vme9u3ddsxnn72cw7t/6Be3cXFlQQF+6KSJAB1fMJExx+RIX+GQpxAsiiKxscevZWz53I4fDiVz5f8jCSJdO7U1CM7vJS0aPFGZFnBarXyjwdHMuvJ2zh3Pod1P+7npbnLOHUqk4iIIAYP6sC7b92PVqdGq9VgdyikpBZ4VVlQvAJckkQOHMrE4dRgsV47k91mc3Aho4TUtAIcDtnLxjyqq0vLUzFsaBeGDelMYVE5K7/fxQcfrsNqtbrZZwcmTRzAC8/fyaQpb7N9+3FEUeTXnSeZeM/N3ohpUJAfH3y0loRGUezec5qMzAJqayws+fwxwkIDAEK1Wk3B9aYQH6DWarWTlpbHnr1nyMwswCkrNGoYQatWDenWtbnXZigpqaLHTY9hMltBUejRvQUXc0s4cyaLoCB/hg/rzLixvejQIRF/d8bHfQ98yJbNm2jSxIpyTa6koFar3K4R5XeNPFmWsVjs7vuu9FgKEpxP0fLuu88xdkwPAEpLq9jyyxHWbzjE5s2HcTpd7h2T2UZqai4g0KVzU1atfN6rhqem5vJx8np8fXW0atmQxo2jCA8PxN/Px6OVhTVuOuWqgv1Cyuf/MoX0Bdh/4Bz3T38fs9nG2DG96NnjkmbiBiMZMB4/kU5lZS1qtQqnrLBz10n6923LIw+Npm+f1kREBF1JgoqaLu2rmfXEORSTgCAoaDVOrHYJRb40oIKo8E91XgFQBBSljn0iKahVMlabCIqAoFOYN785Dqf3nl9CQvwH3D6hL7dP6Et+fhkbNh7mu5W/cvZsDpIkIIoChw6ncPpMFm1aN/LKqAfuH8aFtDwKCstZ88NeUlIuMmniQG4Z0RXgZrvd8e31Fuq5AOHhgSxeNBNBEMjLK6W8vIYf1u6jW7ckburVEo+Pa/EXmxFFgQ4dGjPm1l4MGtCeBg1C6ta3EfgO+BnIcxlwAlqNk+AgKyo/GZtV5PufGjN6cAZ6gwOH1TUjrXZwOuy/SyGiIKJSq1GpFFBAa3CSn2dg6+5oxt9yAZtDRFELaNXOutiedLPbxwBjVFQwU6cM4t5JA0hJvciePWdZuXInR49fYNOmw15AUlJz+eTTDQQFGWgQFULPHi2ZML4PiY294eENHuPxegIyDuDnzb9x4OB5fPRatFo1BoOOfn3beMBIxr2Yv0unpjz71HhatIiva0UfcqfifA9ogYtAVX2G5PJBX8j0Z9P2OL74thnFpXpuGZxJbEwNTpvA0lWDGTD0KbTXlulkZlaQcuJZptxzDtkJh4+F8eOmhmzZFYvNLjGoTw4B4barPZrjmVSV1ioCdQFIkkhS8ziSmscxceLN/PZbKllZRa6sF0GgVct4Jt87kAB/Hxo2jGDuvOVs2FjFSy/e7VklPOLJx8euuN6AhAA86s61dSXF2cnPL2PWs5+j06rp37+dEUj29/cxPnopJ9cD1MLnXlxyLMX3ECufXFB3S4zkS7NaoaZWTVaOL0dPhfLrvgbodE52H4wkLqYaGQGHzUl4qC8Dbu74u42NjCrlwmmB9GxfRAT2H47g6KlQRFHhl13RNIypIsxiocakvjwgFgdQbq4gozyTjtHtyarIISagAZIgoVGr6N4tie51wgUWi40X53zFnBfvYc7Lyzh7LocO7RJ5+pnPWbxoJoBx/U8HrwrIpIkD/yVA3O522H/gLJIkolarEAQBu91BrclSd734WuBD94CvBPY+MWuRk5gqTkbs5ecZqxYAxvzqAoL0QehUWm/sWlYEDD4OYiJriR1aS5e2RUwwDuKVWQdpmFCNbAVFUEjLtmKxgE537QZbLFYC/W3ExdSCFe6bfJakxApe+6gDH768G0mtoKjBoLdfLo5mAtTaa2kYFE9OZS4+aj2S4JWPTwJD3R7vZUBGRUUNFRW1dOyQyLxXltO+fSKDBnbg4ZkHPHU22Lnr1HVlWYLHO3vw4HnKyqu9cWxZhodmjKRLl2aee7eMG/+yrNNqH66ptdBvcEtuujOWUW2H+MPjXQCjU3ayKXULk9rfdcVLZBmsNgks4KN38PYLe4kKNyHbBI8CfFWN6Z81X7ZAm6RS5j5xELtDxOF08UdZEa5q/FZbXT7BvKp8usR6XT0Pr/pxuWP9DxXfh4bqePONKWpX9DKAFkmxTJz0FudTLvLE4+P4Ye1eBtzc3vPc1svk518GRHbZIILxkfqs6IrMiy+/+kU2m200iAlmwn1duaV3H6kOxQCw/MR3bE3bweQO9wDsBHq7lXCcThGzWUIxCwgqhbAQCxu2BhPgX+ty0+OkoESFRvv7DVar4WyKgK+/3gtmRaU/vbtXYrGpUGQQZAWHU7wCXgUFtaQmryqfKP8GCK47PgM6DOqz56CaEPbsjadN+0ftJ46+l6xSScYP3jPyw9p93H//UHp0T8Lf34fQEH/vuFRU1Fx3oT4DmAc87x7c5MvkwFOzZ39Zo6Bw+9QetO4YS7vYVl5ZYbabUYAfz23grV3vM6BxPywOKzqVtne9OKCgIEkKSApoIDXdQKn1CcKD/ZEkFbW1JmzFVWRnF3H4cCqhoQHEx4Wx9sf9BAb40rlzU86dy6FVyxj6DppNdXUtOj8NigIVpcUUFr9CXLzDtXZL5ZJbl6U0UW6uIK30AsW15XSM9s7yacA0P/+Pk0fewlutWs5Mr66WadvhkRnHj7yPTqcx3j6hzyWnX6uGdeXncecf3CpC/NO0D8aSkkoAY2lpFQUF5R4yr3755YkLyitqaN0ujnaxrSRvhkbhGd7bu4DPDn3B96d+wC47OHjxNyaunEa1tbqeiiWKoFHLqNUyapWMKNoYNfImJk4cz+AhQ8jN19CjR1eKi6tITc3l+PF0Xn39G5o2iWb4sM4YDDpeeGkZXyzdxolTNeQXapg48TYmTbqNQQPag2L11q9Ry4hCvUCvUVEULpSm88nBLwjS+YKcQ1XVU1RWv4rdcc7T1ydiYmp50GhDUewAj4IrrdazCis9vaDuGkY5O+PLBYriCgfUvf4KIAvcairbth9n8ZLNvPr6t2RkFlBcXOld2PLxhw/6nklPB+gEUGIqJUDnz9O9H+Mf3R9gYvs7+GTU+2y69wf6J/RhZ+aeeh5ah0Og1qSi1qTGZFJhtUlYra7NfE4cT2Xr1kP06Z1EaIgf0dGhVNdYGDy4CwcOpZJ2oQCz2cbAAe04fTaHzVt+o03rmHrakNmi9tZfa1Jhv4xlKSiUmcs5WZiK4NyKpaov1bXLqK35kJKSsTgcrsQHjWbhzRWVeqxWhyeCikol8dKcr9m58ySvvvYNJrOVgsJy7/qSnMyvBEmSqHv9qyzLH/eWSMtWbKeivIYPP16HKIrkF5TRqGEkzz1zu/dmh+IEmAQQrA9C9AnxJiaUmyvoGN0BvVrH3e3vQK++pCrJCqhVMn6+dhTRZanotA60Wtc9/fu3JSu7jOUr9tC/f3vat2+DpDrFuXMnOPbbLizmEnr37sXEe27Bz0/FDz/soUMHr7KBVqvB4GPH1yCDAwSd63115mmyKIjG+MB4QvQqfIQ15NeAn0ZCViRkuYya2kUEBrwOYDSZ1FvPn1nYGTCeOZPNjp0nOHUmm2+++5WgIF+mTnuHyZMHM2RQR4DkBx/6WBk+rPN1kSFVQLKvr94YGx3Gy/NWeLWsnTtP0a5NQt0seM3Em0cvAIxmu4Ufz20gvzqftlGt6R7Xjd1Z+6ix1nJXuwkE6PyvGseQJAVFcvmXJUlk67bfqKgoQBQEEhvrqa218eu2r6it2ki75sdofVMxI3uAqF5Odm40u053IDh8FJ06tuDsmZOcPeMyOA8fTqVzUwlJ5XR9IV2xvMQCYHFYCNU72Zrlx5gmNQRo7ciKgCCoMJvX4Of7EJIUPXbggPldgCPgWor31rurOHs2G7VaRXW1hYOHUpj+wAicThlJEo0ff/jgzM1bfrNeD0CaAMbsnGLWrNuLWi15146LosCpM1kcOHiegQPa49aYjBaHhZ9Tt9A+qg2DEvuTX1PIe3s+oqC6kBRVCt+fXsNtrcbgr/WrB4bVJlFWrnVpWZJCSLBMbv7LZJ0xociXYuThoWbadrRTVaFw4PhIYuL7czHjO5IST9K17S4yMg5w7jcdgkdoCyBYDKjVGspKBZAFBK2CzXalluWrMVBrB6cMzUNsOGQBrSRjcwrYZRMWy0YMhmkAPYCGABs3HSI1NRdJEt0WPOi0GjZv/o3ExChP4K7Rpws3nlPcjk9FgUEDO/5LgBQDBAX6MvvZO8jKLOLM2SwEQUClVnHrqJ4eMJKBXwEcsoNbmg9DEl18MlAfyMGcw2RWZLNiwhIMGgN2ub5LV1FctkeDyFoUk2uYYiJq6dreidMpXMbrneQXq6myz2Dw6JeIi9Fz5uztXDj1GAkxK+l/k4TsrK4f0JGKsVglr8NR0Cvodc4rPGKy4qTMrEItOvklK4LMShlJsNI5ykpCQA0qlTfnrwLYC9CpY1M+/vBBbh37MjabHZ1OjSiJ3DdtqAeM5MTmD5yTJNCqrRh89VeVIX9UqFcAyX5+ehpEh9KnbzuSFzxC69aN2bLxVYKD/JAvLYysBJJ9Nb4eMJI98mRw0wFMan8nBo1r6bdaVNVznXjkiNMp4nQKOJ0CNptIba0ai0WF2XM5VOw82Iqcqs3cMvYN4mJciRUtkiLoM2QZRy98x55D0ZiddZ6xqKipVeNwXKrb6RSv6jSuslYjCE42ZwWzu2A0Jbbe/JTug3FzBIcLfBDwrmcXX351wkV3OBfZqTDzkTHcMaE/908bwYplT9dl5ckBvmW0aWElMjyHbp21DOwf9i+zrNbeBySR556+DYAO7RKIjg4hMfHSopf4lt3k9jPjZvww7bvngFeosyIqRB9Mx+j2LDq0BIvDyvQuU1FLamNdlmWxShSV6FHM126MIiqIooF+/btwecaovx906tyWozsliop14Ly2VS9oXe+7/I6immJkxYlNVnio+xQa+DmpLF3OrzmB5NSEoVZ7BfO+6MgYgMWApl27hKnt2iVgtdqprTUTHFxPRhr7dP1thsnRmaoqwZsD9mcBae0ezEbAkP17luG0fom5toqgoETUPj2xmjsQHdsWrcv1asw6vZ9VW3+Y4QHD4rBysuAUF8oy6N2oJwdzDtMjvhu7MnfzyE9P8f7w+agltZdladQyAf42FNW1kxc0WgeK/RjfrVjItPuN9QbU4YTD+z6lQ4t8tBqw2X+nizpQq+UrWFZ25UViA2KY1ulehi2dwPLxrxCvbUXvmFNYlHYIYqg3vjRlytte47eiooyUcztQnKcQHb9xtLoQH784QiKfp2mzNsZ3Pk5vCOwBkmfOnFl2Neq8VmujgefqujsK8jPp1GQiqgDZpaE49wNfo1ggO60VuaUz6Nh1Clqt1jj25tEAxozyLJ79+UVOFZ5BRka/S0+3uM48FT+TVuFJ9Fw4kGP5J+gc07Ge5SlJMookXNUsdTqc7DkQRY+uuZhtyRQX30t42KXc5hMnzhPh+yVOReTgsXC6dszHcQ1QBElBrP+aKIDi2mJ6xHVlXKvRJB9YxJu7v+XDoY8g8RVR/vM9vrRkt/1hLCjII+30PJonrKZLi8JLoyoB6gOcOliMw7EZlUo91O2YnPfuu+8mz5kzZ8YfAcTPA0ZeXjb52ckE+5/DXJWJ7CegrpDQqEGvldFoXatg4+NO0SBoBnt+zaDvoPkARrPDwpHcYySPfh+7087Fylz2Zu9n1em1WBxWREEkSBdIQnAj6rrfzRYVeQUG1wr3y8BQkMm42JJGSXM4ceZ+RIo4fHA9w4bf5qWw86e+I1BnI6egD5GJj7Ntz4M0TShDcV6dQswWqS7rCACw2C00DHKtL9Gr9dTaLQQYRiL4jq7rDnkBKDaZaii6cCu92h505XPaARuYzCIWK1hsAmGBu7lwYihqjT+19hEkNr8DvV5vfPHFF69Iyr4aIG8Axgtpx9DbB9OxbRHYgBjACnaHgNkqUlEt4ihzqaX2agFFgW4tP6Wk+AlCw8LRSVpGJg3zsqMwQyjtG7TFT+uHzWHFT+fH+yPmE6QPrKNlCajVTgIDrCjqyyhEVCgp0RDXdA59+/Zkx+YJRPi/yeGTizGZb8NHD7l5VajllcREOrBrJ9ChS29M1Y+jVj2O3leAyzy7gk5xsSylngFMiamMUS1GuORJbQmvDHwQQRBx2xzJnyT3+2y6cfvtALnZ22nT9iB5GRJhYTI2m0CtSUIUZXRaGX8/GZUKItRb3SO+hhNHl5PYbh0+Pj5XZMqrrkIdRpvNRgDTCI0tumJbe7VKQa1x4h8AtTUC6ZlaQoOdhIU6QF3FidM/ERo2GUEQPGAkAx+7SfXNKL9ICmuL8df513Xc1ZMRrk/lchcs/kEOzLb3mHrfQQ4dLuPecfHotBd5+OHXsDt9EeSL9OhgYfGKJDb8eppRw17n1iFbUEtuJnN5nUK9D4CeTsVJcW0xpaYyHLKD4c0G0y/hJs/vI37ZMjl/8ec1TDdyF0BJ3gqaxEFomExungqnExo1ciCKrtAxsvvymINWaNNuK2kZ79C4xfMIAkbgQU+w9HJA7nVlZK+mQ9PfrmQbdQbHZhGorpJIaGhHq5NdL1UgJugrZHmSx5L3f+yLp6tjEqN5rNdD/QEkQcQpe/nHw7gW8Fs97ndZdqmkitvuUKlkHG71VCWAXvUzPdru5+Txjrz5SWf8fa1UVR2h1qQQ4C+y/2Anyip0dGx1mA4tTqCVzAiyBoebJUqSgsMhuqIqDuUKtbfGWktaWTqZFdlIosRzfZ9C5Vbf95zelL9tq8ihQ4d6AyNyss/Rusl3YAaNSqFhnJ2KSoGiIpGIcOcV+HuLFUK0r5CbexcxMY0AWgKnrmaHDHE9sPZ39S9ZgdJSFSHBTrQ+Mg6b6LKi7dAgcjspZ9d72V/gRRsH0w/g4qxgtlvw0XgD/jvcXLeecWizS15gUtIDsdtFZNllk8gOH9q3szL1jjT8fG08aTzKP6YWM3JkT54wpjLptnMEBVqYfm8KTRs7sVt12OwuAExmNWmZAW5tTMTmkK4IUJkdZvQqPdsu7MDhdHjBOFpwYsZvu87z6uuLDR5lp6p4Ab5BbjeMxjUhA4MU9FqF0jKVS6hfrTggKMRCScFWzzfDrmUYRgKEBxdcNkz1TcnychV+vk7UPgoWk8SUx/u6Oqp2V2J4CZvNCmB84fl3ejbQRIL7VwWFIJ13Uxh1fcoTkCQFH70Di13i0PEwHnq+F/t+i8DqlNDrnei0TkSHQu/eecwyHsXhELl1aCo9O+UwZGAmNbUaXnnqAImJlUiygk7rRK93UmnSsHF7LE+83J2UjABEUUGvcyCJ9SOQguLyKhzJO865kpRLVr5TIMDgC/AmcHt+3lmaxy12TTMF9u6JdEVT9BAQImO3CdTWSNc2vRUID86uN+5XA0QP4Odrd1GI9kqm5nQIWG0Svv4yiPDQszfRokk5TVpXuvrlgMCAo2SnvO41iN65/a3GuDa4RBIlb3j0ckAURcZqlagyadhzIJK3Pm2HRiPzweet2XcgggqThvJKLRVVGooLdMQ0qMGgd1JYJOAjbSAv10CLJmWEBlsoLdJRUem+36RhzY8JfL2qKQ6HyKsfduBChj+VNVrs9vorrfx0/hjUetSimhMFp719aBPdWph0z1SXn85ixllxF5LahCLBnHc6sWVXDJs3x/Ll0mZYzRLhYQ6cDrF+ppJAvXE1XNLW1dcS6plAUk5BAqXVZhwWHSGBGYSE5Lugs4PVLBAU4ALstTc7sHV3DEnNynn86R7065bLiOFZYIfY0Je4kNqPxk163+X2Fp93uUvUOC7JkHrOLB+9xMl8A8F6C8OGZBMTWctTr3TnhUcP071rATq1E9kpYHdIVFY6CIuqpWVTEJBo0aQQu10kMd5JRaWAyexLcDgIKEhqmSn3nCU6opbF3yTxxrP7aRhXjSHIjkZjx+7wRvOSfdR644KR77ExZTMTWo+pq+a29PjpMs7OIanxURBh6+YY7E6RPt3yeH5+F5wOkRaJ5XTqXoS5FAw+ApLaFf1UzFBc1ozKmigMhkJqHGEERF3yFV4NkGRgaJO2C9FoNAiCSFVVBecu7kewzKVZ833YK0CnU8DpkuN3jzuPJMqsWp9A57ZFLhqzgVYHfpVDyck+TGxcknfR5M2JfetGyo6v3rpdGXNzv2TA+MhDw+jw1W6efKE7ye/tpEuHIowTT9G9UyHFJRLBfiYiohUupEVyImcppuojCMoRNFIhskNGUqkxO6JQqXtgdwYwsONUwhtYyclUI6hl+vXORZIUWjUtQ+Uvs+6HOI6fb8eCAS097VkCEOkXYZzc8Z66Y/IR8A+Andve46b2b7hYlT9U1WjYua8Buw9EseKjX5j7Xie0WtfgaDVOnE4FQS1y6swUdIHTiItvQ7hOj9VqIfySczH593J76+ZMHQE6ANjtNg7vX0B84PNENahFkN2k5wPzXupIrUnN7eNTOXMqmDtGp3oJsbikKbLvdiIiGnB5HP6xNc/N8DvTHMUKc+feswS49/X53zH/zXXcPTabcaMuoFM7KK8QOJn+MAYfhQsXNiMJOp59aS0B/josVqiqsuN0yqjVEv7+KjRqOHToFF8snIx/UBBNm42kojyT1olf4+sDdkXk9NlgXn2vKR9+MItRI7sCJC//ftuMbfJqPhv/USdguLu9ER5/3NHDP9AucQKCbAM1lBVpKS7Rs21fNE0bVrLnYCSyAi8+cxjBAaYaOJPWBP/opTRt1r3uGB8EutQBY0Ydvq1c7QpUFEV0/x2uKMoCxV0O7v9BkUtRlGIUpQLl/P5AJanl7cq4Wwcr/zDepLz3Whtl29po1z1FrnvyziUpBQUXPVUsUBSl0fQX72PSP+7h3Xc/Y8PGIyiK0kVRFMXpdCqTJr+jBIbeqbz/emvl0K4w5aflYcoPa9YriqIoaWmVyvmUIkWWZeX3itlsVU6czFOyc0yKoijKRx8uVDYsD1MObQ9TtqyNUdq0Ga7Mf2td3Uca7dx5hplfzOKtze/Q9+X+KIrS2vPj+bM/K9VZkqKUoSgmlB+WNVJuGTZU6dN7lPLOq22V7GO+ygfzW7t+L0FRSlEqMhooJSVFnip+UBRlsKIoftOndkZRFIOiKH5XxNivAcjVruaKoigOh0PJP9dOUcpdA+4sEJTcEwZFLkWxFEnKQw/0UqZO6qsoNS4wlEIUpQol63ickpebUReU1vfcNYH7po5j/vy3+XXnWTzAV1bWKp26Pqro/CYp8+e0V3JS9MqnH96tnDtfo/wrZdeeHOXrhT2UnDS9snVNjNIg5jbl9jvfURwOp7c9x46l8/prr3HftIm8MPsp6k3CfV8r1nxcfa5G+fWnKGXyPf2UwjS9UpymU3r2vFU5sj1UUezuieruc9rp9+r2N3DC+OFMuXc006d25oH7RrBq1eorxvnPJDmcA5IlScKpX4jT7tIaREmhQeNa9hyKov/okURGmnjsgeO8/X5bMtL8XaqgGeLis/GnKynnfvXETU58+fU3C+wOB5s2rmDN6s/4ftWuGe5UVNatmU27NqG8s7Api5a0pnnCenZsHsX2bdswmf9Yg8vK7fy0/lvOHRlJy6RTbNiYwIxnOtGjZ2eWLH7Qu/PPll/2zdi4YRlHjuxi4aKlIXPmvrEAMDocDtLPf0qnJvegcdsZ+bk+zJzdi6H9sglvbCY02EJcdDUGvcPl1VBckrm6LJKI2Ls9Tflg9arPKkJCwnA4f3/1l/TSSy/9mTSgncBA/4DoBhfO7yYkLMOlJwlQkOdDq2bl3H1PCi/P70R5uY7dh6Jom1RKQIgNbKDR1uKn+4IDh0QaxPZCFMXOo0ffHjFp0v0p816eWVaQn8aYsRMOAb5+fvrOt43tyc5d51n5A0RFwID+x0k/v55lK9IoKtHh7x+KRqNFUrlcLrIM1dWQmVnCim83sXHtqzQI+ZCOHYrYtTeOuW+24OaBA/h80Qy0Wg1A8qpVG2Z8ufRtKiqKWbZ83UBcB47dU1paxOnD42jd9H2XvHR7IjR6majwWma/0YUwXws//xJLbHQNw0dkX3KPqOBi5RIiG3QCSF68+LOlDocKrU5NRXkJglIBoi+NE9uTlJT0Ly/Y8ZQpwOKS4nT87E1dGoXT5Tm11YrMmtedof2z+XpVU8YMSyfzoh+PPnrClT5gc7ukNXA2ZQR+4e8SE5tYV9OYefuEUdZvvl0bAbyIexvyJ2Yt5oulB2mSYOLlWYdp3yaH8jKJ0vIECsvbYrMHo9XqsFpN6LUFhAedJCwkC39/gaOnGvDqu+04fjaIp58YzDPP3IbGFdVK/uqrFTP279vMxwuWxOPezhbg/NktBKlnEB6VBia3Liq6lXTZ5fE7fjiU+x7rQ59uebw5b5/LkJZdikxObkfCE3d7smUaLljwUZZn9VdxcSGZaetQaWIYNGQKY8bc+pcB8Qze9HPH59M8ftYlq94HvvqqGRnZfvx2Ioxbh6YzfHAWj83uydP/OErL9mUushbcru8af85dnEfTFlMwGAx1637EbSy9BRhlWeGbb39l3qvfYzEV0al9OX26FtCuTSESVqxWkJ0CKpWCWgsmi569ByM4fDKCk6f9iIxuxltv3FM3Yz3ZOH3ajORPPovCffyFK+aTQ2n+q7Rs+InX7iIAdm2OYsO2eF6dvd9l0+tg+Yom6CQnQwdno9e6MyFFsDsg33KUuPh2AMlvzp87o+5SCx+9jjMnViITyaChVwLyZ1lWvTUU/sGdyU7bQlB4rqvxTmjbtpTMLH8Ky/WMHJjJ0/O6E9ughh37oomLqiYy2uyiKAeo1VaiIjaSl7WZ9OxIIqOaIghCZ/cgBbtjDn6CIHRu3bohd97RG7Xah41bTGzdqSY7VwuiitAQB/4BCkVlOnYfjOKr72JYt7kRkqYVzz17Ny/PucOzt+Jyt4q5fcSIkXOBb4DOVVWVnDz6CZF+k4ht8Kt3cGVFYM26BFo3K2PBl604cy6YAcMv8sXi5mzZEcM9d6YQEmy5NCE1kJY3n8ZNxwIsAh7Pysy0+/sH4rl8/ULIy83E4XSS2KTjdWFZ9eyV1NTTBAo9CQurdLEkATCAbBIYducwenfN59m5Ryg8q8f4dB8WvbODkAiLi9/KlzqCAqdT+iNrnyKp5c2oVF6b9XM31UzxzOSfNhwiI6OQM+eKOXgoHZvVhKzY8PcLxGZTGD6sPXl5uQwZ3JGxl3ZsSwY2uesY4gq5lpOfsx5/1StEx5z3TiokyMn1RQBmvtiTFk3LeObxI4y4fTiyDF3aF/HCrMP46B0uViy4uMOxo31o2fUXz9ZQHWbOnHn0ivCCqKGk8BgGg5qBg6deQSF/ZQOzhwCaNGlp3LMrmZCQuxAllwVPLciCwJhhGVzI8KcoU09Wjh95hT5UVWi4WGAgMaYSQ4jD5eJ3A9kyaRvI2zh1uCsOzRM0bjIQP7+AKW4wkt0C9wW9Xs34cb0ICPDh2Il0SkqqyblYwv3ThvDL1qMMGtiBTxducO2l5V4XA3Rzr12hsDCHnIxVRPm/RVJ8rmtimN1MUu/yOH/2dRJZuX589cEv9Bg5hsRGVXz6xg6WrW7K84//5uqr1Q2GBgry4mjc9gcPGMmfffbZ0ZYtW14Z7xFV0DyME8f3oSjyX8p+v2KNpju6SM+b7jCe+K2aVg0fQJRcv6iQuf/+M/y8MZZHn+pJbqGByRPOMXNODwL8baglmXvGp9Cnd56rYwre2daq5QFQbqOwIJozR8fTquNcDAZfI64jj5BlhVqTBUnlWv8uSSIqlejdCchmc2C12vG/LEk8Pz+T3AtP0TTuJzq1MrlYk9U9Cr6QdiqAtCx/hgzP4cmHj3LL3cNY8UMTVnyyhbseHECXdoW88NJh195yddhUeVkATv0W/PwCAZJXrFgxo45MvCwAJ4ESiChp/3Ky9dVKlscP06bj/aTkuYWhx4FaA4MH5fD5O9tZv3IDdrtE4/gqln69jXlPH+SDRa0pzPVx3a92P+MBxgoRkbl0bPohFvO111Zcnkn+eyy4ujKdTp1X4u9rclGE4NKY8vN8OHEyhIpqLU/N605Wqh++0Q4+eW0nb33ajpjIWrZ89yNNEypcWWeeia2FygodJtUeomOaetjiHKfTybUuh8MOggZRUl91JfFf3ox/ypQpJ3FtbEnz1g+w/eDLLpng8ZuZQad34udrJ6lpuSvrvEiNwcdOUame9Ew/xk4dzOatsS7HpK4+3aZmtSUwKIzrUXQ+cZgKVW7yFkjP8OfNt9vx5ifteWRWTyqr1Txw9xkefOYmUs8G8N2PjZlwSxpaXwdBgdb6+Vs6KC8PJKVwDdGx3gWvb65cubJQq9Xy+5fOE6O//oAIgsDkyZO9oPQb9DzHzn+JxVbHyy+75MrNAy5yc++LTH+kN7dOHsLgvjkcPxPKzv1RFJfowQ/2740gL9fgbZngc68n5TL5LzQzBCAmJoHiql6ghfIKLbdNG4RakLltdBrBgVbeXdiWvj3yCAuz8MjMXjSKr+aFpw6jUpT6gQId5Oc1xKY9TOcuQzxte/e7777L+KOuqOu1YOea5amnnvKC0q7TPaQVbaeyJsE164VL1HLb2Au8P3c3iz/cwYibs9i4LY45TxzibFogb85rx4hJw1i5vjEYoKo0nNCoO+qu/ZvtWq6mRaXSIUkaRFFCEFyXR2iqVCoQNEgqb9znbVdiuEi1/BKyRSA0xkLf7nl8t74x+w5E8swTR7DbRfYdjmDJZ9tYv3QDd9+VcskY9IyWD5xPHYVs2EFEZGMPGG+sXbs29XqM43U9Nu++++47uWjRojaAsVWbvsby8gMcPzWTti2+dnXK7gIlOMRKsNZKaaGOhx84ialaxfLVTbl9dCoJcVXcNSYFbJB90YCJjznnCEYQg1FpwpBUfuTn5aOSigkOCqGy0kxNjRmTqZby8iqqq8rISD9NbXUahYqOA/udSGINVZW5aNVliEoW+YKO6MZmJk84x09b49FonXzxVXOGD87irvGpUO3y0WGq0zktOGwqDh2ZS6duj6NWazxgvPTUU08V9ezZk78dIAC9evU4uXv33rkAQUGhxsCuX3L86AgifZ8iIsbt73G4WFiHjsWghvwMH9Yt38BHn7amd9c8QuMsVF6UiInKIjDyxUtmrsOlwTUPFTBbJBRFQ0icwZXRmKRAuZreLa0I9hoeGCcTFCgjqGSvaupJy7mYocFeKdCqdRnjhl+gqEjPkw8dIz6xGmrd+iN1bCQgLWMImsD5dL/Jm+ac/M03385Yt24tMTEx1238bsjBkv379Sho0qTZjE8XLkEQBGPbDhMoK7uZA0c+JiEymbCoQm+GH1aIauCair275dEssQJntUCNWSS6gb3+LHWzPt9QBV+7A1QOEE3eBZzeHCjJXbfsHlyljpoqQGS4ndIyiQiNg3nPHnQF2qy41FkPa3JrfZk5XaiRn6NF6xF1T/tJ7t27x8kZMx657mN3Q8+geu7ZmTPcGRXJwcGhdO31IgQc5/jZd0jPisfmFFyD5x64vv3yiGpgoqRYJCzEceVWJu5VT+vWJlCY78OBfZEsW9aMyjIN+3dH8s13TcnLMTD/nfZs3Naw3oY1detQqRU0aoXKSnf3PVThVtlrTBrOp/XmVOYqoprspFWbkV4wpk29a8ato3qfvFFjdsNPHXv8sYcLp065awbQHEgOC4ugbaeZhDc+SXFZS4pLBSqrJMwWAcUKFSUSOp2CRqtcfW8ZCcorfTBZ1JxNCyYzx581PzVm6cpmlJbrqajUEhRkIyvHj6JS/dV7KENQkBOTWcBcK+BUoNYsUlomUlwGhZVGmrbfQau2YzxZ/cmA5oXZs2bc6PH6N51jKHD33XecHzWy/wwgwWVty+g1xQQFK6gkhaoakfNpaixWhYAI+Xc3a/AzWPAz2ElqUobDIZLUrJyGMdVU16gpq9LRrl0pNTVqas3qK+sR3SxNgqgIJwWFEhfz1NjtYNArhESCxVzg2ehsC+A7efK9M2bMeMBeN9X1RpW/LENkWXavl1OQZRmHw4EkXrKgHU4nFksVVVU16F0HbIkAZnMVfpoyVCKo/GS0GgEBAUXRcni/Hw1jqwkOtCCq3P6xOi0dPeQCokpBp7XTMLqKiHgTbZqVUGNSExZlpjhfT8PbKgmLM7tkg3QpwFRbq6aqWoNO68Qpi0RFmakolwgMkL2JSVFhpV563Px9YO3U28M5nxZHdmF79+aWrr5KkuTdEUilUuFwOP7Q5JQk4ZrA/mVAAgP9URQZvV5LYKAfMTENUEkCoihRVFRCZVU1KA5EESxWO8DjACXFGUQ0cG2kb6oRqa4WiIh0kJ+vZd3PjWjXqoyWTUs4cTYUX187oUEWAvyslFdqkGWBsBAzxaU+dGxTRPq5AMJCzKSkuzLpHQ4Rvd7BpvUNaZVUQk2tmoycANQSpGb4k9iwgvMXAmnXqoRevU0EyE7y8yVCQ2XUBgVTTS6+Nisajbb/oHEV+skTu5udzhqqTacw1Zrx8fHF31+muLgQnU6L0+kgPz//D2+Zrla70mSvdmDAXwZEpfIFwGSSEUUDCQmug1g8q3SDgt27TAsCb85/WYX7DFwd34EaSgtca/7Cwly6pk7jpFP7YqxWkcPHIygu1RIdXUtqegQNY6v47UQYrZqVUVjig7+vnWOnw/h2XROG9Mvk5x1xdG5bxKHjEYQEWejTLZdtu2IICLDSoXUJp84G0yCihsRGFfj62gnwtYIT9HoFUVAoK1Ph53AS0+As59O20azFUIC3o+OGzUAAUZARRdfSC1EUyMsvJD4+AUVRyMzM/sN74oiChEodSmlZxZX081ePPIpPmPjHPZHpX4YCxeXl1Rw78BxhQTuJDT9NQFgdjcodCq2o1FJcpkencXIqJZiwIDPF5T6guPbUiggzk5YZgM0mEehvQaVS8Pe1UljiS3CgmayL/oSHmGnWuJygQAtpGYHExVTjZ7Dha7Cj93G/U+1mooLr3dk5wVSau1Fmup3efe/2yBKxectJyvWTFIrXl3X21BfXF5DYhhP/1P05mV96E/Fqaszk5+dSWnySAEMasu0QAYaTBPvl4KOrdW27KbkHzVHHvnDbGLINRK3bePPEM2x1HJQe2SPU8UA7XN5km1WkpjaE8ppmmGytENSdsDqbERmVSFRURN1d8JJj4ifeIO1KuW6nI3hLXKOJf1IJULiY9dUoNyiDL//dZLJSUVFBZWU5NdUFOO35SEIZWnUV5WXZhIXasdScIDLcgUplxWGrQlacyLKMTqvFagOdPoSiYhOyEIfd2Qiz1YDeEIPF6oOkjkKrjyYoKByDwY/AQP9rnb6QDLwS2/CeXOEGqlbZGV/+5wHx0Wto3z6eb1e8EAgkATe5P5vi2h3h92PHn/zM4EHd0GrhwoVsKipcEcN7Jw5g2/ZT9OnbiQXJG2neNJRbR3f9I836GdeCmRPAPiD9/unvOH/ZehK73cG/ExAV/8HSq/cjFZJK2BcU5LcvqXksaWl5rPz2pWAgFGgAxLs/g3EdxRoCjAA7fr4KarUaH70Oq9WBVqPD19cHH70Gg49AgJ+C2eLNqFuDa1fVaqAQ165GGbjO1y0cf/sca0KjCM6n5JGVVUjHjk2uGa+40eU/CsjVypOzFpYdOnS2TBCkFEEQUBQnWq2En68fK7974TlgBN4UWNl7eXzkl76rd07u2n43z1xqsTjdR2NoEUUFWVZIurQb99+iiPzNi1otUVBYzZFjGX+pnvMpxTic8t+9u38/Cqlv44gUF1dRXm7G+RcHU5JECgqqUGQw+OjclrLyt+vz35ZCXANYSWlZLbIs1zvS6F/yprkBKCyqpLi4ym0L8H+A/POBcx3UdTG3jLLyWu9316tuEMjLLycltcC1zZ4o/h8g12yMKOBwOMkrqKSqyvKXqeLalALV1VZycsoxm6035D3/9YCo1SrKyqrZuu0EFZXm3z1f6nqxxKLiSn786TBOJzf8ff81Ql0QQBJFcnPLOXe+0LVWUCX92yjSZnNy7HgWKpWIWi39x+WK6j8LhisGkp1dysFDGW7NSvq3tkGSRExmK7v3pBATHcS1Nhb7nwdEdJ+wefFiBWaLo95xRP92jU50bVyZnVPqZV2/d4jl/5wMEUSB6moLBYVVmC32ekeo/ke1O0nE4ZDZvz+NwsLK/0ibVP9+yhCwWu2cOn0Rh8Pp3f/372T/lJbVUPHbhf8I1f5HWJbrSFT5b6VuXg4K/PNjrv4rAPlzLo4/3WMBXCdK/7OJKwiuzYo9/9bZrvUGuHikvycggiAQHOz3L55l/scR1GrUv8vqJFHE4XCiv3TiphwXG3b9W+M+YaikpArlOpHTdQPEbnfSqGE4Y27tQWlZNeKN4b0/AfMGDerAuh/3M2F8nyvBUEnk5BRx+kwW900b4vl601NPjrsh8lAUJZ6bvZSysqqr7lT9H6QQBZVaRU52Adk5xYiSgMD1BWXcbS8d+37lS8nxceFGBYX0jALUmvpdMPhoWbJ0C7eNu8lDIcmvvLqs6EbMDofDSXx8xN+XZclOGb2PlnbtmiBK4o2iko8A44Tb+vD0s58zdcqQeppQaloeVZUmbu7v3WDziUGDOt8Q6jCbrVy4kHfd2NUNEuoKzZvHEhsTht0hc70h2bnz5JnevVsn+/rqjcbpIzhzJpuwsACvRpSRUcADDwzzCP3kF+csNV1/LREMBi29era+7mrxDVF7BUFg797TnDqdeUMs3t69W88BjEnN41i8eBN9+7XFx0dDSupFAoMMNGsaA5B8592v3JD0HbvdQcOGkdwIE+qGACKKImVl1Zw5m42PQXvdFa6+/R8v3LHt7WRRFIwP3D+cbduPERIawKrVe3j4H6M8t308cmSvG+DaEKiuMXH+fNYNsVNumGHocDoZMaIb3bomuROUr7O6teHgjOHDumiaNYuZeiE9j6+XbeP++4Z5DrhMXvPDntNazXXuntsz7esXTFpazn+fpe5n0HP8eBrZ2cU3JN4wfFiXZGBqly7N+emng/Tq6d054eWFi9Zf/0lmd5DQuAGjbul5w9Yl3FBAZEXhQno+Z85kerZEut7lOLhO/rn33oHe48N79X4k/0a8zGazU1tjQbiBLh9BUf5+mRd/slx+0PGM/+bO/C8A4qH0PsDW//aO/K8A8j9T/t8Ajn3H+H6xynYAAAAASUVORK5CYII='
  //#endregion
  registrarCertificateData = []
  today = ''
  purpose = 'scholarship purposes only.'


  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService, private imageService: Base64ImagesService) { }
  public calculateAge(birthdate: any): number {
    return moment().diff(birthdate);
  }
  clear() {
    this.image = 'assets/noimage.jpg';
    this.signature = 'assets/nosignature.jpg';
    this.id = '';
    this.checkId = '';
    this.lrno = '';
    this.fname = '';
    this.mname = '';
    this.lname = '';
    this.suffix = '';
    //basic
    this.gender = '';
    this.cstatus = '';
    this.bdate = '';
    this.nationality = '';
    this.religion = '';
    this.placeob = '';
    //contact
    this.tno = '';
    this.cno = '';
    this.email = '';

    this.homeaddress = '';
    this.street = '';
    this.boardingaddress = '';
    this.street2 = '';


  }

  nearestneighbor = ''
  medicaladvise = ''
  healthproblems = ''
  pdefects = ''
  contest = ''
  sabilities = ''
  birthorder = ''
  nofamily = ''

  quickSavePersonalInfo() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    }
    else {
      this.global.swalLoading('Updating Basic Info');
      this.api.putStudentPersonalInfo(this.checkId, {
        "numberOfSibling": this.nofamily,
        "birthOrder": this.birthorder,
        "skillTalent": this.sabilities,
        "contestToJoin": this.contest,
        "physicalDefect": this.pdefects,
        "healthProblem": this.healthproblems,
        "medicalAdvise": this.medicaladvise,
        "nearestNeighbor": this.nearestneighbor
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }
  updatenamevar = false
  updatename() {
    if (this.updatenamevar == true)
      this.updatenamevar = false
    else
      this.updatenamevar = true
  }
  updatenameapi() {

    var error = '';
    if (this.fname == '') {
      error = error + '*First name must not be blank<br>';
    }
    if (this.lname == '') {
      error = error + '*Last name must not be blank<br>';
    }

    if (error == '') {
      var x = ''
      let date = new Date(this.bdate).toLocaleString();
      this.global.swalLoading('Updating Student...');
      this.api.putStudentPersonInfo({
        'lrNumber': this.lrno,
        'firstName': this.fname,
        'middleName': this.mname,
        'lastName': this.lname,
        'suffixName': this.suffix,
        'gender': this.gender,
        'civilStatus': this.cstatus,
        'dateOfBirth': date,
        'mobileNo': this.cno,
        'telNo': this.tno,
        'idNumber': this.id,
        'idNo': this.id,
        "personType": 0,
        "nationality": this.nationality,
        "religion": this.religion,
        "emailAddress": this.email,
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
          this.updatenamevar = false
        }, Error => {
          this.global.swalAlertError(Error);
        });
    } else {
      this.global.swalAlert('The Following error has Occured', error, 'error');
    }

  }
  openDialog(lookup): void {
    const dialogRef = this.dialog.open(AddressLookupComponent, {
      width: '500px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        if (lookup == 1) {
          this.permPSGC = result.data;
          this.homeaddress = result.result;
        } else {
          this.currPSGC = result.data;
          this.boardingaddress = result.result;
        }
      }
    });
  }

  studentlookup(): void {
    const dialogRef = this.dialog.open(StudentLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }

  openDialog2(lookup): void {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(FamilyBackgroundComponent, {
        width: '600px', disableClose: false, data: { id: this.checkId, kind: lookup },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          if (result.result == 'cancel') {
          }
          if (result.result == 'saved') {
            this.loadfambg();
          }
        }
      });
    }
  }

  openDialog3(educBcgData, seek): void {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(EducationalBackgroundComponent, {
        width: '600px', disableClose: false, data: { data: educBcgData, id: this.checkId, seek: seek },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          if (result.result != 'cancel') {
            if (result.result == 'nice') {
              this.loadeducbg();
            }
          }
        }
      });
    }
  }

  openDialogguardian(lookup): void {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(FamilyBackgroundGuardianComponent, {
        width: '600px', disableClose: false, data: { id: this.checkId, kind: lookup },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          if (result.result == 'cancel') {
          }
          if (result.result == 'saved') {
            this.loadfambg();
          }
        }
      });
    }
  }


  openDialogfamilymember(lookup): void {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(FamilyBackgroundFamilymemberComponent, {
        width: '600px', disableClose: false, data: { id: this.checkId, kind: lookup },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          if (result.result == 'cancel') {
          }
          if (result.result == 'saved') {
            this.loadfambg();
          }
        }
      });
    }
  }

  openedit(x, y) {
    if (y == 'Mother' || y == 'Father') {
      this.openDialog2(x)
    }
    else if (y == 'Brother/Sister' || y == 'Guardian' || y == 'Cousin' || y == 'Uncle/Aunt' || y == 'Grand Parent' || y == 'StepFather' || y == 'StepMother' || y == 'Spouse') {
      this.openDialogguardian(x)
    } else {
      this.openDialogfamilymember(x)
    }
  }

  cForm137A = false
  cForm138 = false
  otr = false
  cnso = false
  marriageContract = false
  tableArr2
  g = 0

  studinfo
  userdemog
  userinfo
  rawIdPic
  rawSign
  keyDownFunction(event) {

    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.id != '') {
        this.global.activeid = this.id
        this.global.swalLoading('Loading Student Information');
        this.api.getStudent(this.id, this.global.syear, this.global.domain)
          .map(response => response.json())
          .subscribe(res => {
            this.api.getStudentPersonInfo(this.id)
              .map(response => response.json())
              .subscribe(res => {
                this.userinfo = res.data
                // console.log("userinfo",this.userinfo)
                if (res.data != null) {
                  this.lrno = res.data.lrNumber;
                }
                // console.log(res.data)
                this.api.getRegistrarCertification(this.id, this.global.syear).map(response => response.json()).subscribe(res => {
                  this.registrarCertificateData = res.data
                  // console.log(this.registrarCertificateData)
                })

              });
            this.global.swalClose();
            if (res.message == "Student found.") {
              if (res.data.level == 'denied') {
                this.global.swalAlert('ID Number: ' + this.id + ' is not a ' + this.global.domain + ' student', '', 'warning')
                // code...
              } else {
                this.hidden = false

                if(res.data.idPicture){
                  this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
                  this.signature = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.signature);
                  this.rawIdPic = res.data.idPicture;
                  this.rawSign = res.data.signature
                }

                else
                  this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + this.imageService.NoImage);

                // console.log(this.image)

                this.checkId = res.data.idNumber;
                this.fname = res.data.firstName;
                this.mname = res.data.middleName;
                this.lname = res.data.lastName;
                this.suffix = res.data.suffixName;
                this.gender = res.data.gender;
                this.cstatus = res.data.civilStatus;
                this.bdate = res.data.dateOfBirth;
                this.placeob = res.data.placeOfBirth;

                this.api.getStudentDemography(this.id, this.global.syear)
                  .map(response => response.json())
                  .subscribe(res => {

                    if (res.message == "Student demographical information found.") {
                      this.userdemog = res.data
                      this.religion = res.data.religion;
                      this.nationality = res.data.nationality;

                      if (res.data.cForm137A == false || res.data.cForm137A == null || res.data.cForm137A == "0")
                        this.cForm137A = false
                      else
                        this.cForm137A = true
                      // console.log(res.data.cForm138)
                      if (res.data.cForm138 == false || res.data.cForm138 == null || res.data.cForm138 == "0")
                        this.cForm138 = false
                      else
                        this.cForm138 = true

                      if (res.data.otr == false || res.data.otr == null || res.data.otr == "0")
                        this.otr = false
                      else
                        this.otr = true

                      if (res.data.cnso == false || res.data.cnso == null || res.data.cnso == "0")
                        this.cnso = false
                      else
                        this.cnso = true

                      if (res.data.marriageContract == false || res.data.marriageContract == null || res.data.marriageContract == "False")
                        this.marriageContract = false
                      else
                        this.marriageContract = true

                      this.tno = res.data.telNo;
                      this.cno = res.data.mobileNo;
                      this.email = res.data.emailAdd;
                      this.homeaddress = res.data.homeAddress;
                      this.street = res.data.permNoStreet;
                      this.boardingaddress = res.data.currentAddress;
                      this.street2 = res.data.currNoStreet;
                      this.permPSGC = res.data.permPSGC;
                      this.currPSGC = res.data.currPSGC;

                      this.nearestneighbor = res.data.nearestNeighbor
                      this.medicaladvise = res.data.medicalAdvise
                      this.healthproblems = res.data.healthProblem
                      this.pdefects = res.data.physicalDefect
                      this.contest = res.data.contestToJoin
                      this.sabilities = res.data.skillTalent
                      this.birthorder = res.data.birthOrder
                      this.nofamily = res.data.numberOfSibling

                      this.form137A = (res.data.form137A == "1")
                      this.form137E = (res.data.form137E == "1")
                      this.nso = (res.data.nso == "1")
                      this.reportCard = (res.data.reportCard == "1")
                      this.baptism = res.data.baptism
                      this.confession = res.data.confession
                      this.communion = res.data.communion
                      this.religiousInstruction = res.data.religiousInstruction
                      this.confirmation = res.data.confirmation

                      if (res.data.sgfName == null)
                        res.data.sgfName = ''
                      if (res.data.lsaName == null)
                        res.data.lsaName = ''

                      if (res.data.sgfid == null)
                        res.data.sgfid = ''
                      if (res.data.lsaid == null)
                        res.data.lsaid = ''

                      this.sgfId = res.data.sgfid.toString()
                      this.sgfsy = res.data.sgfsy
                      this.sgfName = res.data.sgfName
                      this.sgfAddress = res.data.sgfAddress
                      this.lsaName = res.data.lsaName

                      this.myControl.setValue(res.data.sgfName);
                      this.myControl2.setValue(res.data.lsaName);

                      this.lsaAddress = res.data.lsaAddress
                      this.slaId = res.data.lsaid.toString()
                      this.slaSy = res.data.lsasy
                      this.average = res.data.average

                      if (this.global.checkaccess(':Student:EnrollmentHistoryGet')) {
                        this.api.getStudentEnrollmentHistory(this.checkId)
                          .map(response => response.json())
                          .subscribe(res => {
                            this.tableArr2 = res.data; this.g = 0
                            // console.log("enrolment history:", this.tableArr2)
                            if (this.global.domain == "HIGHSCHOOL") {
                              this.api.getStudent(this.checkId, this.global.checknosemester(this.global.syear), this.global.domain)
                                .map(response => response.json())
                                .subscribe(res => {
                                  this.studinfo = res.data
                                  this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
                                  this.signature = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.signature);
                                })
                            } else if (this.global.domain == "COLLEGE") {
                              this.api.getStudent(this.checkId, this.global.checknosemester(this.global.syear), this.global.domain)
                                .map(response => response.json())
                                .subscribe(res => {
                                  this.studinfo = res.data
                                  this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
                                  this.signature = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.signature);
                                })
                            }

                          }, Error => {
                            this.g = 0
                            this.global.swalAlertError(Error);
                          });
                      }
                    } else {
                      this.clear();
                    }
                  }, Error => {
                    this.global.swalAlertError(Error);
                  });

                this.loadeducbg();
                this.loadfambg();
              }
            } else {
              this.global.swalAlert(res.message, '', 'warning')
              this.clear();

            }

          }, Error => {
            this.global.swalAlert("Invalid ID Number", "", 'warning');
          });
      }
    }
  }

  getformat(a) {
    var x = '';
    if (a.substring(6) != '') {
      if (a.substring(6) == '1')
        x = "1st Semester";
      else if (a.substring(6) == '2')
        x = "2nd Semester";
      else
        x = "Summer";
    }

    var y = parseInt(a.substring(0, 4)) + 1;

    a = x + ' SY ' + a.substring(0, 4) + "-" + y
    return a
  }

  getindex(x) {
    var index = this.options.indexOf(x);
    if (this.arrayschool[index].address == null) {
      this.sgfAddress = ''
    } else
      this.sgfAddress = this.arrayschool[index].address
    this.sgfId = this.arrayschool[index].companyID
  }
  getindex2(x) {
    var index = this.options2.indexOf(x);
    if (this.arrayschool[index].address == null) {
      this.lsaAddress = ''
    } else
      this.lsaAddress = this.arrayschool[index].address
    this.slaId = this.arrayschool[index].companyID
  }

  sibling
  loadfambg() {
    this.api.getStudentFamilyBG(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.familyarray = res.data
        // console.log("Family:",this.familyarray)
        this.sibling = []
        for (var i = 0; i < res.data.length; ++i) {
          if (res.data[i].relDesc == 'Sibling')
            this.sibling.push(res.data[i])
        }
      });
  }
  loadeducbg() {
    this.api.getStudentEducBG(this.id)
      .map(response => response.json())
      .subscribe(res => {

        this.educarray = res.data
        // console.log(this.educarray)
      });
  }

  quickSaveBasicInfo() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    }
    else {
      this.global.swalLoading('Updating Basic Info');
      let date = new Date(this.bdate).toLocaleString();
      //console.log(JSON.stringify(date));
      this.api.putStudentBasicInfo(this.checkId, {
        "gender": this.gender,
        "civilStatus": this.cstatus,
        "nationality": this.nationality,
        "religion": this.religion,
        "dateOfBirth": date,
        "placeOfBirth": this.placeob
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  quickSaveContactInfo() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    }
    else {
      this.global.swalLoading('Updating Basic Info');
      this.api.putStudentContactInfo(this.checkId, {
        "mobileNo": this.cno,
        "telNo": this.tno,
        "emailAddress": this.email,
        "permAddressNoStreet": this.street,
        "permAddressPSGC": this.permPSGC,
        "currAddressNoStreet": this.street2,
        "currAddressPSGC": this.currPSGC,
        "landLordLady": '',
        "boardingPhoneNo": ''
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  quickSaveCollegeEnrollmentRequirement() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      this.api.putStudentCollegeEnrollmentRequirement(this.checkId, {
        "form137A": this.cForm137A,
        "form138": this.cForm138,
        "nso": this.cnso,
        "otr": this.otr,
        "marriageContract": this.marriageContract
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  form137A = false
  form137E = false
  nso = false
  reportCard = false
  quickSaveEnrollmentRequirement() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      this.global.swalLoading('')
      this.api.putStudentEnrollmentRequirement(this.checkId, this.global.syear, {
        "reportCard": this.reportCard,
        "nso": this.nso,
        "form137A": this.form137A,
        "form137E": this.form137E
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  baptism = false
  confession = false
  communion = false
  religiousInstruction = false
  confirmation = false
  quickSaveSacramentsReceivedPut() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      this.api.putStudentSacramentsReceived(this.checkId, {
        "baptism": this.baptism,
        "confession": this.confession,
        "communion": this.communion,
        "confirmation": this.confirmation,
        "instruction": this.religiousInstruction
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  sgfId = ''
  sgfName = ''
  sgfsy = ''
  slaId = ''
  slaSy = ''
  lsaAddress = ''
  lsaName = ''

  average = ''
  sgfAddress = ''
  quickSaveEducationalBGHSPut() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      if (this.average == '' || this.average == null) {
        this.average = '0'
      }
      if (this.myControl.value == '') {
        this.sgfAddress = ''
        this.sgfId = ''
      }
      if (this.myControl2.value == '') {
        this.lsaAddress = ''
        this.slaId = ''
      }
      if (this.sgfsy == null) {
        this.sgfsy = ''
      }
      if (this.slaSy == null) {
        this.slaSy = ''
      }
      this.api.getStudentEducBGHS(this.checkId, {
        "sgfId": this.sgfId,
        "sgfSy": this.sgfsy,
        "sgfAv": this.average,
        "slaId": this.slaId,
        "slaSy": this.slaSy
      })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  options: string[] = []


  myControl2 = new FormControl();
  filteredOptions2: Observable<string[]>;
  options2: string[] = []


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  private _filter2(value: string): string[] {
    const filterValue2 = value.toLowerCase();
    return this.options2.filter(option => option.toLowerCase().includes(filterValue2));
  }


  arrayschool

  ngOnInit() {

    this.api.getPublicAPICurrentServerTime()
      .map(response => response.json())
      .subscribe(res => {
        var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var today: any = new Date(res.data);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth()).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        this.today = mL[today.getMonth()] + ' ' + dd + ', ' + yyyy;
      }, Error => {
        this.global.swalAlertError(Error)
      });

    this.api.getPublicAPISchools()
      .map(response => response.json())
      .subscribe(res => {
        this.arrayschool = res
        for (var i = 0; i < this.arrayschool.length; ++i) {
          this.options.push(this.arrayschool[i].companyName)
          this.options2.push(this.arrayschool[i].companyName)
        }
        if (this.global.activeid != '') {
          this.id = this.global.activeid
          this.keyDownFunction('onoutfocus')
        }
      });

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.filteredOptions2 = this.myControl2.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter2(value))
      );



  }
  deletefamily(id) {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else
      this.swalConfirm("Are you sure?", "You won't be able to revert this!", 'warning', 'Delete Family Member Info', 'Family Member Info has been Removed', 'deletefamily', id);
  }
  deleteeduc(id) {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else
      this.swalConfirm("Are you sure?", "You won't be able to revert this!", 'warning', 'Delete Educational Background', 'Educational Background has been Removed', 'deleteeb', id);
  }
  swalConfirm(title, text, type, button, successm, remove, id) {
    swal.fire({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {
        if (remove == 'deletefamily') {
          this.global.swalLoading('');
          this.api.deleteStudentFamilyMember(this.checkId, id)
            .map(response => response.json())
            .subscribe(res => {
              this.global.swalSuccess(successm);
              this.loadfambg();
            }, Error => {
              this.global.swalAlertError(Error);
            });
        }
        else
          if (remove == 'deleteeb') {
            this.global.swalLoading('');
            this.api.deleteStudentEducBG(id, this.checkId)
              .map(response => response.json())
              .subscribe(res => {
                this.global.swalSuccess(successm);
                this.loadeducbg();
              }, Error => {
                this.global.swalAlertError(Error);
              });
          }
      }
    })
  }

  showClassSchedule() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(ClassScheduleComponent, {
        width: '850px', disableClose: false, data: { id: this.checkId },
      });
    }
  }
  showBasicEdClassSchedule() {
    if (this.checkId != this.id || this.id == '') {
      this.global.swalAlert("Please Check the ID number of the Student", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(BasicedClassScheduleComponent, {
        width: '1200px', disableClose: false, data: { id: this.checkId, studinfo: this.studinfo, enrollmentHistory: this.tableArr2 },
      });
    }
  }

  Getdatab4pdf() {
    this.global.swalLoading('')
    this.clearFamilyBcgFields();
    if (this.familyarray.length != 0)
      this.getfamilydetails(0, this.familyarray[0].relDesc)
    else
      this.generatePDF()
  }
  nonull(x) {
    if (x == null || undefined == x)
      return ''
    return x
  }
  getgrade(x) {
    return x + 6
  }
  getwordgender(x) {
    if (x == "M")
      return 'Male'
    return 'Female'
  }
  getfamilydetails(x, rel) {
    if (x < this.familyarray.length) {
      this.api.getStudentParent(this.familyarray[x].memberIdNumber)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(rel.toLowerCase(),'father')

          if (rel != undefined && res.data != null) {
            if (rel.toLowerCase() == 'mother') {
              // console.log(res.data)

              this.mother = {
                parentName: res.data.parentName,
                cellphoneNo: res.data.cellphoneNo,
                landlineNo: res.data.landlineNo,
                occupation: res.data.occupation,
                officeAddress: res.data.officeAddress,
                relID: res.data.relID,
                ofw: res.data.ofw,
                status: res.data.status
              }
            }
            else if (rel.toLowerCase() === 'father') {


              this.father = {
                parentName: res.data.parentName,
                cellphoneNo: res.data.cellphoneNo,
                landlineNo: res.data.landlineNo,
                occupation: res.data.occupation,
                officeAddress: res.data.officeAddress,
                relID: res.data.relID,
                ofw: res.data.ofw,
                status: res.data.status
              }
              // console.log(this.father)
            }
            else {
              this.guardian = {
                parentName: res.data.parentName,
                cellphoneNo: res.data.cellphoneNo,
                landlineNo: res.data.landlineNo,
                occupation: res.data.occupation,
                officeAddress: res.data.officeAddress,
                relID: res.data.relID,
                ofw: res.data.ofw,
                status: rel
              }
            }
          }
          // console.log(this.father)
          if (this.familyarray[x + 1] == undefined)
            this.getfamilydetails(x + 1, '')
          else
            this.getfamilydetails(x + 1, this.familyarray[x + 1].relDesc)
        }, Error => {
          this.global.swalAlertError(Error);
        });
    } else {
      this.generatePDF()
    }
  }


  father = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }
  mother = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }
  guardian = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }

  preSchool = {
    schoolName: null,
    yrGraduated: null,
    schoolAddress: null
  }
  elementary = {
    schoolName: null,
    yrGraduated: null,
    schoolAddress: null
  }
  jhs = {
    schoolName: null,
    yrGraduated: null,
    schoolAddress: null
  }
  shs = {
    schoolName: null,
    yrGraduated: null,
    schoolAddress: null
  }



  fields = []
  clearFamilyBcgFields() {
    this.father = {
      parentName: null,
      cellphoneNo: null,
      landlineNo: null,
      occupation: null,
      officeAddress: null,
      ofw: null,
      relID: null,
      status: null
    }
    this.mother = {
      parentName: null,
      cellphoneNo: null,
      landlineNo: null,
      occupation: null,
      officeAddress: null,
      ofw: null,
      relID: null,
      status: null
    }
    this.guardian = {
      parentName: null,
      cellphoneNo: null,
      landlineNo: null,
      occupation: null,
      officeAddress: null,
      ofw: null,
      relID: null,
      status: null
    }
  }
  generatePDF() {
    // console.log(this.studinfo)
    this.global.swalClose()
    var SY = "";
    if (this.global.domain == "HIGHSCHOOL" || this.global.domain == "ELEMENTARY") {

    }

    if (this.global.domain == "HIGHSCHOOL" || this.global.domain == "ELEMENTARY") {
      SY = this.global.syDisplaynoSemester(this.global.syear)
      this.basicEdEnrollmentForm(SY);
    } else {
      SY = this.global.syDisplay(this.global.syear);
      this.higherEdEnrollmentForm(SY);
    }


  }

  higherEdEnrollmentForm(SY) {
    // console.log(this.father)
    // console.log(this.mother)
    var SY = SY
    var startRowX = 53, startColY = 159;
    var that = this;
    var bday: any = new Date(this.userinfo.dateOfBirth);
    // var dd = String(bday.getDate()-1).padStart(2, '0');
    var dd = String(bday.getDate()).padStart(2, '0');
    var mm = String(bday.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = bday.getFullYear();
    bday = mm + '/' + dd + '/' + yyyy;

    var dob = bday
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    var gender = "Male";

    if (this.studinfo.gender == "F")
      gender = "Female";
    else
      gender = "Male";

    var civilStatus = "Single";
    if (this.cstatus == "M")
      civilStatus = "Married"
    else if (this.cstatus == "SP")
      civilStatus = "Single Parent"
    else
      civilStatus = "Single"


    var baptism
    var confession
    var holyCommunion
    var confirmation

    var sibling = []
    var lastdigit = 543
    for (var i5 = 0; i5 < this.sibling.length; ++i5) {
      if (i5 == 0) {
        lastdigit += 26
        sibling.push({
          text: 'IDNumber: _____________ Name:_______________________________________',
          absolutePosition: { x: 240, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                       ' + this.sibling[i5].memberIdNumber + '                            ' + this.sibling[i5].fullName,
          absolutePosition: { x: 240, y: lastdigit },
          fontSize: 8
        })
      }
      else if (i5 % 2 === 1) {
        lastdigit += 12
        sibling.push({
          text: 'IDNumber: ________ Name:____________________________',
          absolutePosition: { x: 50, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                    ' + this.sibling[i5].memberIdNumber + '                    ' + this.sibling[i5].fullName,
          absolutePosition: { x: 50, y: lastdigit },
          fontSize: 8
        })
      } else {
        sibling.push({
          text: 'IDNumber: ________ Name:_______________________________',
          absolutePosition: { x: 300, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                   ' + this.sibling[i5].memberIdNumber + '                  ' + this.sibling[i5].fullName,
          absolutePosition: { x: 305, y: lastdigit },
          fontSize: 8
        })
      }
    }


    var header = {
      table: {
        widths: [350, 200],
        body: [
          [
            {
              stack: [
                { text: 'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nOFFICE OF THE STUDENT AFFAIRS AND SERVICES\n\n', fontSize: 10, alignment: 'center' },
                { text: '\nADMISSION FORM FOR INCOMING\nFIRST YEAR STUDENTS, TRANSFEREES & CROSS_ENROLEES', bold: true, fontSize: 10, alignment: 'center' }
              ]

            },
            {
              table: {
                width: [145],
                body: [
                  [{ text: "Document No.: FM-SAS-005\n\n\nRevision No.:00\n\n\nEffectivityDate: January 18, 2021" }]
                ]
              }
            }
          ]
        ]
      },
      layout: 'noBorders'
    }


    var docDefinition = {
      pageSize: 'FOLIO',
      pageMargins: [30, 30, 30, 20],
      content: [
        header,
        {
          text: [
            '\n',
            { text: SY, bold: true, decoration: 'underline', alignment: 'center' },
            { text: ' /Short Term ______\n\n', bold: true, alignment: 'center' },
          ], bold: true, alignment: 'center'
        },
        {
          table: {
            widths: [220, 313],
            body: [
              [
                { text: 'ID #:  _________ Course & Year:  __________________' },
                {
                  stack: [
                    {
                      canvas: [
                        {
                          type: 'polyline',
                          lineWidth: 1,
                          closePath: true,
                          points: [{ x: 2, y: 2 }, { x: 9, y: 2 }, { x: 9, y: 9 }, { x: 2, y: 9 }]
                        }
                      ]
                    },
                    {
                      canvas: [
                        {
                          type: 'polyline',
                          lineWidth: 1,
                          closePath: true,
                          points: [{ x: 149, y: -7 }, { x: 156, y: -7 }, { x: 156, y: 0 }, { x: 149, y: 0 }]

                        }
                      ]
                    },
                    {
                      canvas: [
                        {
                          type: 'polyline',
                          lineWidth: 1,
                          closePath: true,
                          points: [{ x: 232, y: -7 }, { x: 225, y: -7 }, { x: 225, y: 0 }, { x: 232, y: 0 }]

                        }
                      ]
                    },
                    {
                      text: 'Incoming 1st Year College',
                      absolutePosition: { x: 283, y: 159 }
                    },
                    {
                      text: 'Transferee',
                      absolutePosition: { x: 425, y: 159 }
                    },
                    {
                      text: 'Cross-Enrollee',
                      absolutePosition: { x: 500, y: 159 }
                    }

                  ]
                }
              ]
            ]
          }, layout: 'noBorders'
        },
        {
          table: {
            widths: [540],
            body: [
              [
                {
                  stack: [
                    { text: 'PERSONAL INFORMATION', bold: true },
                    { text: '\nName: ___________________________________________________________________________   Birth Date: _______________   Age:____' },
                    { text: 'Gender:  _________________  Civil Status: _____________________   Birth Place: ______________________________________________' },
                    { text: 'Citizenship: _____________________  CP No.: _____________________  Email Address: ________________________________________' },
                    { text: 'Permanent Home Address: ____________________________________________________________________________________________' },
                    { text: '\nFATHER:    ________________________________________________________  Occupation:  ______________________________________' },
                    { text: 'Address:    ________________________________________________________  Contact No:  ______________________________________' },
                    { text: 'MOTHER:  ________________________________________________________  Occupation:  ______________________________________' },
                    { text: 'Address:  _________________________________________________________  Contact No:  ______________________________________' },
                    { text: '\nGUARDIAN RESPONSIBLE FOR THE FINANCIAL SUPPORT IN COLLEFE (IF NOT PARENTS)' },
                    { text: 'Name:  ___________________________________________________________  Occupation:  ______________________________________' },
                    { text: 'Address:  _________________________________________________________  Contact No:  ______________________________________' },
                    { text: '\nEMPLOYER (IF WORKING STUDENT)' },
                    { text: 'Name:  ___________________________________________________________  Occupation:  ______________________________________' },
                    { text: 'Address:  _________________________________________________________  Contact No:  ______________________________________' },
                    { text: '\nLANDLORD/LANDLADY (IF STAYING IN A BOARDING HOUSE)' },
                    { text: 'Name:  ___________________________________________________________  Contact No:  ______________________________________' },
                    { text: 'Boarding House Address:  ________________________________________________________________________________________________', italics: true },
                    { text: '\nSCHOOL ATTENDED', bold: true },
                    {
                      table: {
                        body: [
                          [
                            { text: 'Pre-School:                __________________________________________________________________' },
                            { text: 'Yr. Graduated: _________________' }
                          ],
                          [
                            { text: 'Address:                     __________________________________________________________________________________________________', colSpan: 2 }
                          ],
                          [
                            { text: 'Elementary School:  _________________________________________________________________' },
                            { text: 'Yr. Graduated: _________________' }
                          ],
                          [
                            { text: 'Address:                     __________________________________________________________________________________________________', colSpan: 2 }
                          ],
                          [
                            { text: 'Junior High School:  _________________________________________________________________' },
                            { text: 'Yr. Graduated: _________________' }
                          ],
                          [
                            { text: 'Address:                     __________________________________________________________________________________________________', colSpan: 2 }
                          ],
                          [
                            { text: 'Senior High School:  _________________________________________________________________' },
                            { text: 'Yr. Graduated: _________________' }
                          ],
                          [
                            { text: 'Address:                     __________________________________________________________________________________________________', colSpan: 2 }
                          ]
                        ]
                      }, layout: 'noBorders'
                    },
                    { text: '\nLAST SCHOOL/COLLEGE ATTENDED (IF TRANSFEREE)' },
                    { text: 'Name of School:       __________________________________________________________________   School Year: __________________' },
                    { text: 'Course & Year:           __________________ Address: ______________________________________________________________________' },


                    { text: '\nOTHER INFORMATION\n\n', bold: true },
                    { text: 'Number of Siblings: ____________   Your Birth Order: ____________    Religion: __________________________' },
                    { text: 'Sacraments Received:          Baptism  (      )      Confession  (      )     Holy Communion  (      )     Confirmation (      ) ' },
                    { text: 'Did you received Religious Instruction during High School:     Yes (   )  No (   )' },
                    { text: 'Brothers/Sisters studying in this school:' },
                    { text: '  ' },
                    { text: 'Special Abilities/Experties/Talents: ____________________________________________________________________________________' },
                    { text: 'Language Spoken: _____________________________________________________    Dialect: _____________________________________' },
                    { text: 'Contest you want to join: ______________________________________________________________________________________________' },
                    { text: 'Physical Difficulties: __________________________________________________________________________________________________' },
                    { text: 'Health Concern: ______________________________________________________________________________________________________' },


                    { text: ['\nCREDENTIALS PRESENTED', { text: ' (to be filled out by OSAS)\n\n', italics: true, bold: false }], bold: true },
                    {
                      table: {
                        widths: [170, 170, 170],
                        body: [
                          [
                            { text: '(  ) Form 137' },
                            { text: '(  ) GMC Certificate' },
                            { text: '(  ) Honorable Dismissal' }
                          ],
                          [
                            { text: '(  ) Form 138 (Gr. 12 Card' },
                            { text: '(  ) Certificate of Live Birth from PSA' },
                            { text: '(  ) Certificate of True Copy of Grades' }
                          ],
                          [
                            { text: '(  ) USL Placement Exam Result' },
                            { text: '(  ) Church Marriage Certificate (if married)' },
                            { text: '(  ) Police Clearance' }
                          ],
                          [
                            { text: '(  ) Affidavit of Non - Cohabitation / Solo Parent ID (if single parent)', colSpan: 2 },
                            { text: '' },
                            { text: '(  ) Recommendation from any USL employee' }
                          ]
                        ]
                      }, fontSize: 8, layout: 'noBorders'
                    },
                    { text: '\n*S - Single, M - Married, SP - Single Parent', italics: true, bold: true }
                  ]
                }
              ]
            ]
          }, layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 2 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 2 : 1;
            }
          }
        },
        hEdHeader(),
        personalInfo(),
        contactInfo(),
        familyBCG(),
        guardian(),
        schoolAttended(),
        otherInformation()

      ],
      footer: function (currentPage, pageCount, pageSize) {
        return [
          { text: '', alignment: 'right', fontSize: 9, margin: [0, 0, 40, 0] }//currentPage+' of '+pageCount
        ]
      },
      defaultStyle: {
        fontSize: 10
      }

    }
    function hEdHeader() {
      var res = {
        stack: [
          {
            text: that.studinfo.idNumber,
            absolutePosition: { x: startRowX + 2, y: startColY },
            bold: true
          },
          {
            text: that.studinfo.course + " - " + that.studinfo.yearOrGradeLevel,
            absolutePosition: { x: startRowX + 118, y: startColY },
            bold: true
          }
        ]
      }
      return res
    }
    function personalInfo() {
      var name = that.studinfo.lastName + ", " + that.studinfo.firstName + " " + that.studinfo.middleName;
      var res = {
        stack: [
          {
            text: name,
            absolutePosition: { x: startRowX + 17, y: startColY += 40 },
            bold: true
          },
          {
            text: bday,
            absolutePosition: { x: startRowX + 410, y: startColY },
            bold: true
          },
          {
            text: age,
            absolutePosition: { x: startRowX + 507, y: startColY },
            bold: true
          },
          {
            text: gender,
            absolutePosition: { x: startRowX + 23, y: startColY += 12 },
            bold: true
          },
          {
            text: civilStatus,
            absolutePosition: { x: startRowX + 160, y: startColY },
            bold: true
          },
          {
            text: that.studinfo.placeOfBirth,
            absolutePosition: { x: startRowX + 315, y: startColY },
            bold: true
          }
        ]
      }
      return res
    }
    function contactInfo() {
      var res = {
        stack: [
          {
            text: that.userinfo.nationality,
            absolutePosition: { x: startRowX + 38, y: startColY += 12 },
            bold: true
          },
          {
            text: that.userinfo.mobileNo,
            absolutePosition: { x: startRowX + 173, y: startColY },
            bold: true
          },
          {
            text: that.userdemog.emailAdd,
            absolutePosition: { x: startRowX + 342, y: startColY },
            bold: true
          },
          {
            text: that.userdemog.homeAddress,
            absolutePosition: { x: startRowX + 106, y: startColY += 11.5 },
            bold: true
          },
        ]
      }
      return res
    }
    function familyBCG() {
      var res = {
        stack: [
          {
            text: that.father.parentName,
            absolutePosition: { x: startRowX + 33, y: startColY += 24 },
            bold: true
          },
          {
            text: that.father.occupation,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          },
          {
            text: that.father.officeAddress,
            absolutePosition: { x: startRowX + 33, y: startColY += 11.5 },
            bold: true
          },
          {
            text: that.father.cellphoneNo,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          },
          {
            text: that.mother.parentName,
            absolutePosition: { x: startRowX + 33, y: startColY += 11.5 },
            bold: true
          },
          {
            text: that.mother.occupation,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          },
          {
            text: that.mother.officeAddress,
            absolutePosition: { x: startRowX + 33, y: startColY += 11.5 },
            bold: true
          },
          {
            text: that.mother.cellphoneNo,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          }
        ]
      }
      return res
    }
    function guardian() {
      var res = {
        stack: [
          {
            text: that.guardian.parentName,
            absolutePosition: { x: startRowX + 18, y: startColY += 35 },
            bold: true
          },
          {
            text: that.guardian.occupation,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          },
          {
            text: that.guardian.officeAddress,
            absolutePosition: { x: startRowX + 28, y: startColY += 11.5 },
            bold: true
          },
          {
            text: that.guardian.cellphoneNo,
            absolutePosition: { x: startRowX + 347, y: startColY },
            bold: true
          },

        ]
      }
      return res
    }
    function schoolAttended() {
      var res = {
        stack: [
          {
            text: that.preSchool.schoolName,//pre-school name
            absolutePosition: { x: startRowX + 74, y: startColY += 132 },
            bold: true
          },
          {
            text: that.preSchool.yrGraduated,//yr graduated
            absolutePosition: { x: startRowX + 444, y: startColY },
            bold: true
          },
          {
            text: that.preSchool.schoolAddress,//address
            absolutePosition: { x: startRowX + 74, y: startColY += 15 },
            bold: true
          },
          {
            text: that.elementary.schoolName,// elem name
            absolutePosition: { x: startRowX + 74, y: startColY += 16 },
            bold: true
          },
          {
            text: that.elementary.yrGraduated,
            absolutePosition: { x: startRowX + 444, y: startColY },
            bold: true
          },
          {
            text: that.elementary.schoolAddress,
            absolutePosition: { x: startRowX + 74, y: startColY += 15 },
            bold: true
          },
          {
            text: that.jhs.schoolName,//jhs name
            absolutePosition: { x: startRowX + 74, y: startColY += 16 },
            bold: true
          },
          {
            text: that.jhs.yrGraduated,
            absolutePosition: { x: startRowX + 444, y: startColY },
            bold: true
          },
          {
            text: that.jhs.schoolAddress,
            absolutePosition: { x: startRowX + 74, y: startColY += 15 },
            bold: true
          },
          {
            text: that.shs.schoolName,//shs name
            absolutePosition: { x: startRowX + 74, y: startColY += 16 },
            bold: true
          },
          {
            text: that.shs.yrGraduated,
            absolutePosition: { x: startRowX + 444, y: startColY },
            bold: true
          },
          {
            text: that.shs.schoolAddress,
            absolutePosition: { x: startRowX + 74, y: startColY += 16 },
            bold: true
          },

        ]
      }
      return res
    }
    function otherInformation() {
      sacraments();
      var res = {
        stack: [
          {
            text: that.userdemog.numberOfSibling,// no. of siblings
            absolutePosition: { x: startRowX + 74, y: startColY += 96.5 },
            bold: true
          },
          {
            text: that.userdemog.birthOrder,//birth order
            absolutePosition: { x: startRowX + 210, y: startColY },
            bold: true
          },
          {
            text: that.userdemog.religion,//religion
            absolutePosition: { x: startRowX + 315, y: startColY },
            bold: true
          },
          baptism,
          confession,
          holyCommunion,
          confirmation,
          sibling,

          {
            text: that.userdemog.skillTalent,//Special Abilities/
            absolutePosition: { x: startRowX + 142, y: startColY += 58.8 },
            bold: true,
          },
          {
            text: that.userdemog.languageSpoken,//Language Spoken
            absolutePosition: { x: startRowX + 67, y: startColY += 12 },
            bold: true,
          },
          {
            text: that.userdemog.motherTongue,//Dialect
            absolutePosition: { x: startRowX + 352, y: startColY },
            bold: true,
          },
          {
            text: that.userdemog.contestToJoin,//Contest
            absolutePosition: { x: startRowX + 95, y: startColY += 11 },
            bold: true,
          },
          {
            text: that.userdemog.physicalDefect,//Physical Difculties
            absolutePosition: { x: startRowX + 75, y: startColY += 11.8 },
            bold: true,
          },
          {
            text: that.userdemog.healthProblem,//Health Concern
            absolutePosition: { x: startRowX + 58, y: startColY += 11.8 },
            bold: true,
          },

        ]
      }

      return res
    }

    function sacraments() {
      if (that.userdemog.baptism) {
        baptism = {
          text: '√',
          absolutePosition: { x: startRowX + 155, y: 688 },
          bold: true, fontSize: 13,
        }
      }
      if (that.userdemog.confession) {
        confession = {
          text: '√',
          absolutePosition: { x: startRowX + 247, y: 688 },
          bold: true, fontSize: 13,
        }
      }

      if (that.userdemog.communion) {
        holyCommunion = {
          text: '√',
          absolutePosition: { x: startRowX + 363, y: 688 },
          bold: true, fontSize: 13,
        }
      }

      if (that.userdemog.confirmation) {
        confirmation = {
          text: '√',
          absolutePosition: { x: startRowX + 458, y: 688 },
          bold: true, fontSize: 13,
        }
      }
    }

    pdfMake.createPdf(docDefinition).open();
  }
  getstatus(param){
    var status

    if (this.studinfo.enrollmentType == 2 && param  == "transferee")
      status = '√'

    if (this.tableArr2.length == 1 && param  == "old")
      status = '√'
    else if (this.tableArr2.length == 2 && param  == "new")
      status = '√'
    else
      status = ""

    return status;
  }

  getReportcardStatus(type){
    var status = ' ';
    if(type == "yes"){
      if(this.userdemog.reportCard == '1')
        status = '√';
      else
        status = ' ';
    }else{
      if(this.userdemog.reportCard == '1')
        status = ' ';
      else
        status = '√';
    }
    return status
  }
  getNSOStatus(type){
    let status

    if(type == "yes"){
      if (this.userdemog.nso == '1')
        status = '√';
      else
        status = ' ';
    }else{
      if (this.userdemog.nso == '1')
        status = ' ';
      else
        status = '√';
    }

    return status;
  }
  getGenderStatus(type){
    let status
    if(type == "male"){
      if (this.studinfo.gender == 'M')
        status = '√';
      else
        status = ' ';
    }else{
      if (this.studinfo.gender == 'M')
        status = ' ';
      else
        status = '√';
    }

    return status
  }
  checksy(sy){
    if(sy)
      return sy+"\u200B\t";
    else
      return "\u200B\t\u200B\t\u200B\t";
  }
  checkIfTrue(param){
    if(param)
      return param+"\u200B\t";
    else
      return "\u200B\t\u200B\t\u200B\t";
  }
  checkParentStatus(type){
    let fatherstatus

    switch (type) {
      case 'S':{
        if(this.father.status == 'S')
        fatherstatus = '√';
        break;
      }
      case 'P':{
        if(this.father.status == 'P')
        fatherstatus = '√';
        break;
      }
      case 'A':{
        if(this.father.status == 'A')
        fatherstatus = '√';
        break;
      }
      case 'W':{
        if(this.father.status == 'W')
        fatherstatus = '√';
        break;
      }
      case 'X':{
        if(this.father.status == 'X')
        fatherstatus = '√';
        break;
      }
      case 'ofw':{
        if(this.father.ofw)
        fatherstatus = '√';
        break;
      }
      default:{
        fatherstatus = ' ';
        break;
      }
    }
     if(fatherstatus != '√')
        fatherstatus = " ";
    return fatherstatus
  }
  checkParentMotherStatus(type){
    let motherstatus

    switch (type) {
      case 'S':{
        if(this.mother.status == 'S')
        motherstatus = '√';
        break;
      }
      case 'P':{
        if(this.mother.status == 'P')
        motherstatus = '√';
        break;
      }
      case 'A':{
        if(this.mother.status == 'A')
        motherstatus = '√';
        break;
      }
      case 'W':{
        if(this.mother.status == 'W')
        motherstatus = '√';
        break;
      }
      case 'X':{
        if(this.mother.status == 'X')
        motherstatus = '√';
        break;
      }
      case 'ofw':{
        if(this.mother.ofw)
        motherstatus = '√';
        break;
      }
      default:{
        motherstatus = ' ';
        break;
      }
    }
     if(motherstatus != '√')
      motherstatus = " ";
    return motherstatus
  }
  checkMarriedStatus(type){
    var married = " ";
    if(type == "yes"){
      if ((this.mother.status == 'M' && this.father.status == 'M')) {
        married = '√'
      } else {
        married = ' '
      }
    }else{
      if ((this.mother.status != 'M' && this.father.status != 'M')) {
        married = '√'
      } else {
        married = ' '
      }
    }

    return married;
  }

  checkSacramentStatus(param){
    var result = " ";
    switch (param) {
      case "Baptism":{
        if (this.userdemog.baptism)
          result = "√";
        break;
      }
      case "Confession":{
        if (this.userdemog.confession)
          result = "√";
        break;
      }
      case "Communion":{
        if (this.userdemog.communion)
          result = "√";
        break;
      }
      case "Confirmation":{
        if (this.userdemog.confirmation)
          result = "√";
        break;
      }

      default:
        break;
    }

    if(result != "√")
      result = " ";

    return result;
  }
  checkInstruction(type){
    var result = " ";
    if(type == "yes"){
      if(this.userdemog.religiousInstruction)
        result  = '√';
      else
        result = " ";
    }else{
      if(!this.userdemog.religiousInstruction)
        result  = '√';
      else
        result = " ";
    }

  }
  checkNeatScore(score){
    if(score!=null || score != '' || score != undefined)
      return score;
    else
      return " ";
  }
  basicEdEnrollmentForm(SY) {

    var status
    if (this.tableArr2.length == 1)
      status = {
        text: '√',
        absolutePosition: { x: 350, y: 100 },
        bold: true, fontSize: 13,
      }
    else
      status = {
        text: '√',
        absolutePosition: { x: 363, y: 100 },
        bold: true, fontSize: 13,
      }
    if (this.studinfo.enrollmentType == 2)
      status = {
        text: '√',
        absolutePosition: { x: 493, y: 100 },
        bold: true, fontSize: 13,
      }



    let elemschool
    let yeargrad
    let average
    let companyID
    for (var i2 = 0; i2 < this.educarray.length; ++i2) {
      if (this.educarray[i2].programName == 'Primary Education') {
        elemschool = this.educarray[i2].schoolName
        yeargrad = this.educarray[i2].yearGraduated
        average = this.educarray[i2].otherInfo
        companyID = this.educarray[i2].companyID
      }
    }
    var bday: any = new Date(this.userinfo.dateOfBirth);
    // var dd = String(bday.getDate()-1).padStart(2, '0');
    var dd = String(bday.getDate()).padStart(2, '0');
    var mm = String(bday.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = bday.getFullYear();
    bday = mm + '/' + dd + '/' + yyyy;






    var lastdigit = 543
    var sibling = []
    for (var i5 = 0; i5 < this.sibling.length; ++i5) {
      if (i5 == 0) {
        lastdigit += 26
        sibling.push({
          text: 'IDNumber: _____________ Name:_______________________________________',
          absolutePosition: { x: 240, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                       ' + this.sibling[i5].memberIdNumber + '                            ' + this.sibling[i5].fullName,
          absolutePosition: { x: 240, y: lastdigit },
          fontSize: 8
        })
      }
      else if (i5 % 2 === 1) {
        lastdigit += 12
        sibling.push({
          text: 'IDNumber: ________ Name:____________________________',
          absolutePosition: { x: 50, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                    ' + this.sibling[i5].memberIdNumber + '                    ' + this.sibling[i5].fullName,
          absolutePosition: { x: 50, y: lastdigit },
          fontSize: 8
        })
      } else {
        sibling.push({
          text: 'IDNumber: ________ Name:_______________________________',
          absolutePosition: { x: 300, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                   ' + this.sibling[i5].memberIdNumber + '                  ' + this.sibling[i5].fullName,
          absolutePosition: { x: 305, y: lastdigit },
          fontSize: 8
        })
      }
    }
    var header = { text: 'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nSENIOR HIGH SCHOOL DEPARTMENT\nENROLLMENT FORM\n', bold: true, fontSize: 10, alignment: 'center' }
    if (this.studinfo.yearOrGradeLevel < 5) {
      header = { text: 'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nJUNIOR HIGH SCHOOL DEPARTMENT\nENROLLMENT FORM\n', bold: true, fontSize: 10, alignment: 'center' }
    }
    var doc = {
      pageSize: 'FOLIO',
      pageMargins: [50, 30, 50, 20],
      content: [
        header,
        { text: ['School Year ', { text: SY + '\n\n', bold: true, decoration: 'underline', alignment: 'center' }], bold: true, alignment: 'center' },
        {
          table:{
            widths:['*','*','*','*','*','*','*','*'],
            body:[
              [
                {text:"I.D. No.:"},
                {text:this.id,bold:true},
                {text:"LRN No.:"},
                {text:this.studinfo.lrNumber,bold:true,colSpan:2},
                {text:""},
                {text:"Date:"},
                {text:this.global.serverdate,bold:true,colSpan:2},
                {text:""},
              ],
              [
                {text:"Grade:"},
                {text:this.getgrade(this.studinfo.yearOrGradeLevel),bold:true},
                {text:""},
                {text:""},
                {text:"Old ( "+this.getstatus('old')+" )   New ( "+this.getstatus('new')+" )   Transferee ( "+this.getstatus('transferee')+" )",colSpan:4},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:"Curriculum/Strand: ",colSpan:2},
                {text:""},
                {text:this.studinfo.curriculum,bold:true,colSpan:6},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:"Report Card or its equivalent submitted: Yes ("+this.getReportcardStatus("yes")+")  No ("+this.getReportcardStatus("no")+") ",colSpan:4},
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "General Average: ",
                    {text: this.userdemog.generalAverage,bold:true}
                  ],colSpan:2
                },
                {text:""},
                {
                  text:[
                    "NEAT Score: ",
                    {text:this.checkNeatScore(this.userdemog.neatScore),bold:true}
                  ]
                  ,colSpan:2
                },
                {text:""},
              ],
              [
                {text:"Photocopy of NSO Birth Certificate submitted:    Yes ("+this.getNSOStatus("yes")+")  No ("+this.getNSOStatus("no")+")   Gender:  Male ("+this.getGenderStatus("male")+")   Female ("+this.getGenderStatus("female")+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:"Name:"},
                {text:this.studinfo.lastName+", "+this.studinfo.firstName+" "+this.studinfo.middleName, bold:true, colSpan:7},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Date of Birth: ",
                    {text:bday, bold:true},
                    "\u200B\t\u200B\tPlace of Birth: ",
                    {text:this.userdemog.placeOfBirth, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Complete Home Address: ",
                    {text:this.userdemog.homeAddress, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Boarding House Address: ",
                    {text:this.userdemog.currentAddress, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:"Name of Landlord/Landlady: _______________________________________ Phone No.: __________________",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Elementary School Graduated from: ",
                    {text:this.userdemog.sgfName, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "School Year: ",
                    {text: this.checksy(this.userdemog.sgfsy), bold:true},
                    " Address: ",
                    {text:this.userdemog.sgfAddress, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:"For Transferees only:", bold:true, decoration: 'underline', colSpan:7},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "School Last Attended: ",
                    {text:this.userdemog.lsaName, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "School Year: ",
                    {text:this.checksy(this.userdemog.lsasy), bold:true},
                    " Address: ",
                    {text:this.userdemog.lsaAddress, bold:true}
                  ]
                  ,colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Father: ",
                    {text:this.checksy(this.father.parentName), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "C.P./Landline No.: ",
                    {text:this.father.cellphoneNo+" "+this.father.landlineNo, bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Occupation: ",
                    {text:this.checksy(this.father.occupation), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "Office Address: ",
                    {text:this.checksy(this.father.officeAddress), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "\u200B\tOFW ("+this.checkParentStatus('ofw')+")\u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tDeceased ("+this.checkParentStatus('X')+")\u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tWidow/Widower ("+this.checkParentStatus('W')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "\u200B\tSeparated ("+this.checkParentStatus('S')+") \u200B\t\u200B\t\u200B\t\u200B\t \u200B\t \u200B\t \u200B\t \u200B\tAnnulled ("+this.checkParentStatus('A')+")  \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tSingle ("+this.checkParentStatus('S')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Mother: ",
                    {text:this.checksy(this.mother.parentName), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "C.P./Landline No.: ",
                    {text:this.mother.cellphoneNo+" "+this.mother.landlineNo, bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Occupation: ",
                    {text:this.checksy(this.mother.occupation), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "Office Address: ",
                    {text:this.checksy(this.mother.officeAddress), bold:true},
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "\u200B\tOFW ("+this.checkParentMotherStatus('ofw')+")\u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tDeceased ("+this.checkParentMotherStatus('X')+")\u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tWidow/Widower ("+this.checkParentMotherStatus('W')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "\u200B\tSeparated ("+this.checkParentMotherStatus('S')+") \u200B\t\u200B\t\u200B\t\u200B\t \u200B\t \u200B\t \u200B\t \u200B\tAnnulled ("+this.checkParentMotherStatus('A')+")  \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\t \u200B\tSingle ("+this.checkParentMotherStatus('S')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "Parents living together: Yes ("+this.checkMarriedStatus('yes')+")  No ("+this.checkMarriedStatus('no')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text: [
                    "Guardian other than Parents supporting you: ",
                    {text:this.guardian.parentName,bold:true}
                  ],colSpan:5
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:"Relationship: "},
                {text:this.guardian.status,bold:true,colSpan:2},
                {text:""},
              ],
              [
                {text: "Address: "},
                {text:this.guardian.officeAddress,bold:true,colSpan:4},
                {text:""},
                {text:""},
                {text:""},
                {text:"Phone No.: "},
                {text:this.guardian.cellphoneNo,bold:true,colSpan:2},
                {text:""},
              ],
              [
                {text: " ",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {
                  text:[
                    "Number of Children in the family: ",
                    {text:this.checkIfTrue(this.userdemog.numberOfSibling),bold:true},
                    "Your Birth Order: ",
                    {text:this.checkIfTrue(this.userdemog.birthOrder),bold:true},
                    "Religion: ",
                    {text:this.checkIfTrue(this.userdemog.religion),bold:true},
                  ],colSpan:8
                },
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "Sacraments received:          Baptism  ("+this.checkSacramentStatus('Baptism')+")      Confession  ("+this.checkSacramentStatus('Confession')+")     Holy Communion  ("+this.checkSacramentStatus('Communion')+")     Confirmation ("+this.checkSacramentStatus('Confirmation')+")",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "Received Religious Instruction during Elementary:     Yes ("+this.checkInstruction("yes")+")  No ("+this.checkInstruction("no")+")",colSpan:5},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:"Ethnicity: ",colSpan:3},
                {text:this.userdemog.ethnicity,bold:true},
                {text:""},
              ],
              [
                {
                  text: [
                    "Language Spoken: ",
                    {text: this.userdemog.languageSpoken,bold:true}
                  ]
                  ,colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
                {
                  text:[
                    "Mother Tongue: ",
                    {text:this.userdemog.motherTongue,bold:true}
                  ],colSpan:4
                },
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: "Brothers/Sisters studying in this school: ",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  sibling
                ],colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  "Special Abilities/Experties/Talents: ",
                  {text:this.userdemog.skillTalent,bold:true}
                ],colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  "Contest you want to join: ",
                  {text:this.userdemog.contestToJoin,bold:true}
                ],colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  "Physical Difficulties: ",
                  {text:this.userdemog.physicalDefect,bold:true}
                ],colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  "Health Problems: ",
                  {text:this.userdemog.healthProblem,bold:true}
                ],colSpan:4},
                {text:""},
                {text:""},
                {text:""},
                {text: [
                  "Medical Advice: ",
                  {text:this.userdemog.medicalAdvise,bold:true}
                ],colSpan:4},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text: [
                  "Nearest neighbor who is studying in this school: ",
                  {text:this.userdemog.nearestNeighbor,bold:true}
                ],colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
              [
                {text:" ",colSpan:8},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
                {text:""},
              ],
            ]
          },layout:"noBorders"
        },




        { text: '\u200B\t    I, with the guidance of my parents/guardian declare that all the information provided is COMPLETE, ACCURATE, and TRUE. I certify that, to date, I understand that any information I have misrepresented, concealed or falsely given on my enrollment be a ground basis to invalidate and cancel my admission/enrollment in the USL, and the forfeiture of any down payment made by me in favor of USL, I also promise to comply with all the guidelines, requirements and instructions, and special conditions.', alignment: 'justify' },
        { alignment: 'justify', text: '\u200B\t    My registration in this school is considered as expression of my willingness to abide with all the standing rules and regulations of USL and whatsoever may be prescribed by the School Authorities from time to time.' },
        {
          alignment: 'justify', text: ['\u200B\t    I hereby agree to pay all assessed fees and obligations before the end of the School Year. Should I drop/withdraw my studies, I agree to pay the assessed fees according to the provisions sanctioned by Dept. Ed. and stipulated in the USL Handbook which states, ',
            { alignment: 'justify', text: "\"A student dropping from the school after enrollment shall be charged for his/her fees in accordance with the Manual Regulations for Private Schools, Article XIII, Section 66. When a student registers in a school, it is understood he/she is enrolling for the whole school year. A student who withdraws in writing within the first week of classes may be charged 10% of the total amount due or 20% if within the second week of classes regardless of whether or not he/she has actually attended classes. The student may be charged all the fees in full if he/she withdraws any time after the second week of classes. However, if the withdrawal is due to justifiable reason (i.e. serious illness) he/she should be charged the pertinent fees only up to the last month of attendance.\"", fontSize: 9, italics: true, bold: true, }]
        },

        { text: '\n\nFather\'s/Mother\'s Signature: __________________________ Guardian\'s Signature: _________________\n\n' },
        { text: 'Student\'s Signature: _________________________ Principal\'s/Representative\'s Signature: __________________' },
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAQAAAAHUWYVAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAFfuSURBVHjaxJ11nFzl1ce/596xdYm7e7BAIAES3Itb0UILLZRCC6VAS1+kLVagLVCc4i6hOMETXGMkEPdNsslu1ndH7j3vH3Nn9toEmrTL7CeyM3PvfZ7nPMd+Rx5R/vPXOCTwngbe63zH/Zmg+T+5d2wEMMD5X+53O3Av/78AxhD5kT1WTXOF/Rqz3HdWCB2DIs5zjMD7tvN79i6Gc5fsfXIj77x359/4nqUIc7dibSNs5cs92fApd74jPnK5N0F2OorhWwwJTDW3jN6rMdje3l2HY9p92MgSaVbfvTuv7HxfPNvFvZjiG7t6vqX55e4kh39N1DfG/+wl/y0OKcQpWuC7Ehi0iWLnJyQYqGs5/LvPuctgvZqjKHc+S8tMuVw/DW4P7w7uJDEejsveO8cZ2W2i+c3h/h4eLnfPxS0B5nUVh8gWOEV8xPBzjbim3SmywPDsL7+gEc/98uQYpI8z2fXAKPvpMDlVP9TQMWmoqO3kGA39tnj4SD2chzOXTm5RFx9tzcvYWnEVRgBc7Kq+qeAREeqbpngWh/xE3SKj8xrNbaarmKx+ThusN1INugWx4eIwz5MkQEZxCTmvOOskpF+wCdvyMraOQwqRRL6TiOrbjV52994/p+oFQTCcfyU36N31qE7h45Lxk/VocS2u+vSan9id20AD8j/Hzfo9NYN4uKoLOURc+zZM1Xvtqs4drwEl6pW+6uM1CZH/kiPRVCr9ajr7t7GvETMwHBKazr+GQ1z//ndrE/ER2D1+cT1bfeaAeAwOCciR/zmH4CyvePZZUAcQIpikgH3j3rtS0CrLv2cypKDEH2SX2djOdXZ+eW0fx2ioZYhPTwS1g9cs8G/WMPH3PyaIf69q4D0NXXKvtnBbKurbu+49K77FyosnKWhoiHgMUA3oMgkhu3gsKg3sfAnRkhQglHQth/gnIwGVLh53KriDvGLCOx31EMCt0rM6xcheZRmrC01a1tCS/a74hAkh3Ky+reCXAm5byq0jJODXuMmmXalDwugvIcLHu386WVlDtYK6TEgpsH9xBJGi2O/TEi7U7BmaVJSs2LI9+93rK3iF0ZZNfAkhaVBobpvA2kaRFWbw+r1yyatDCfVuCcAPGmIwiGuf5vftTKaHbRGdI8+rz05y++z+xS+8eOLaQhLQh52Gr/oEuGyD+WtsOyE0xGoJ31Fs8R2vzV/Yw89/J8WVsiDHeXZuKhvkUtZJKOfi8yq27F2pa1ndFqD43EA82k1/CD/Er3qDii6MXLjssnCfWUNMR/XYaOJVnvPlx8Zz0gpCiiSS4T05lel8D2PcK0zD/AcJOK9+UezeHrLNBu9/hUPEI4vx4UcacPwKq0/Js7/4TAdx7T3x7E/Qefp32mwSnE4FLRgP85YEuFQ8ekhD54BPl/hBRfWBjriEoN8Mka62svzGn3rYNuh4BacblONhLpUEDEn17EewT7KeoJuQ4QzuZkSk8QbrUkyjgNDUgBkrHgPcC65riCXl9pTwgJ+EGu1djGWpB9GRAiaw167XUKRXAzJfAzaWfxEkkbrKerD7AAxFaWUCD3FEj47r2/9Fd/fO1QIWk3r2v/jATfHpDTw6ZEv6UbcJgDe2VlDhsfGlgNPo3+3BWIP6Fih0Oibbya5ZTztP/n6t/zKvvCR2Gm15L7wPt/B7in/S9JzsZPgEid8/UA//is9KLKTgXW6pB2oR391+EB0ShupKwAGTgh6+H08NCjqFbnqwXs3LvGZf3emFgE5qfLH3yX/nZ0Q9gIjB2dzJmKlNz9tHu7ZPN36l9/F/7BoGtEhoXCOMW71+vPpstS0b112gQ8QH/4XD8RoCSmoBZ8wbhxPkSD7gKb2CA1jJ15Q6+9DglPS03Sc8wMFAKmBO7MGDHD3IfpTLJQZAqX0nt/Ez/qRv8GcxEQKweZgp7OUFb5xRXPYiIXbjD6ZDZIt+h4TGQPxWT+DORezJEQy2kQXai0Y5hv6yh/6Odmea/fSSSJ/fMrIg6N+Lq6kuTv9Ods5SSI7nPsZyOiv0j/q4gxFLGK/rFnjYbW+pS7lrAN6h69He74p7SEjsRDxRCykUD+nNw/b7vGB9wIm6WGbSJM+zkcnSUyzn23VGXYpVoeo591pEA+Z6rQOgB0i7tPEI++t8mmgHhTI9n10kEQ5sqkvRh2EQYa6mbMGn6RKRRaibJQVgD3V5Jf6QUu5fG8S+1Dyu9OHouYkN+gC78aAO1sd52X6N/XNaQtpYmuHbLYbF5tOOsZ4aAXQxm/V8fY1/SImcIBfQIQh6DLfyApODYSu3uRK8u/gMcD9I/4NwyJasqiAPaQDH0iAaFGcHDIGodUTxh9W/kLsqjy9fmf6TfCYNehJ7yCfijsIsNljuiWr4XwtQWEULCPIZv2QRY/i1vkqbtoOiJ9p3KCg1EgA8pSCCIKGLLD5PXbfJ0tpqtDeotDVEx/jtmSBnAMhIXuIibFAxzeTKug6bPsuqr5MDtQ8fyTy251CezCljhVWSXJOHeoNTt1mGCUsUoLdcoC/J9vxEP2QspwPoUfpo5aeRD2jSekXA1BNkqEb9NqL6wMVgXMUbO91y6KoLoRN8HrkQnnrjj6QIChGFlIzWEkWRFLMzQ0uKe9IKHybW2LvyPP0pxcZWzXPUcrO5nrUFR7WCtZhpyTLRIfoP+RVReViu0ex8d9c7yt8ccLoktVUyoGg/btdXZJA3CiIBSMgbDVGPpSU+8+AHQHs1NFbGFlWa+C2rM/ReymQFX0l/BVB9QncZsPc4NlG/ntUyhk+kSKZ6SWmslMZmVhZ8zhIakWZWgGIv5Eu93n5Vb+cmadLnZYg+avSxXltla3/ZqElB4CK687Ju4BDdYcuJGuoT2xoCyv8AEUN8+0W+A1APA7cF+nANp3AG03U0C6imCBTjVevTxdfO7amkqqweYlOjH+gJlHoWZoOxtoMVBVXnYjowWlgpAB/p0cZd7Ka/pC/nS40+LQnebL28faY9hFqSwG78nC+4zDTlal7XowhAmG7UWgNZWeKzFbeFP/4L8RAJ1Sxh3rzX/NXhPMIfjGc5iipmcgARugsCzcZvG/pvnm6fkv6TNVw+Ial38QBpz53VWKwOQQjlEBtjnaxz3MvVeq4xxNhZRsoT3KUT+LkcIqdZy+wYG7DE4BLiXGJa+mudSG+jOkwGiC/aKAHwxxvP2Xq1HtlWgoSlVIfn/nnAwkP4O4O4Xb/kCz7mKv2ppOmrqwH4NPLj5N+Sj4I8rtNBnsvey0AxHaBElsBKUsRCtkMDyxBksaZd79ZQoxDnNpmuM7B4k1bZn/WCfZgco3fJu7qb/g5I6UJBoVjb/HkBnalGYcGsLeMXXcYh6ktR0C1a4I4/UsZlPM0oTcqd8oVArfzSOEFszshP8S0OMX4sR/ELNnfiix4ob6HoOmpDn7KWGgxYTD6pLh9xSfIe90gzqMixiizB5GZ7uVxnFOtd2iQfoNquENNbOEpDvA/xCSUJGCphYewuI4iGQgyFmNX5dpFM4Wc0y9myTu/UKsdcfFGv4GzZJy+P1+pT8gItubRrw48RzTFbN7Mh9HmraSRqM1sCiWwuzlXeM17TpF6kI+RGWWVfbu/IVTqTuCQFLtSz+C0DxQimsvrjl/7slG1zC7cBy9JQ/D+sNsMHnfxDr5Ub5HC9j18zSW7Oi4L7ZKbezkBccUJBMQOpBlrJQIZHks2sI5gDDItIYiq9GUw1ppdrjU4CvsTh1HCafMKd7M2lMp17qEJlhf6Ia2QBF8lf9JJgSFlCM44lZFvSdTrEa6FLIN6Rm7o/s9xAsd+QY/Ukngbe4Nd6h87iNlBo1WvkUi3zah3DTfYKGaXjdSdG2yPpK5E0S0NwtTTfAIZp32nUsoSFOkfmsFA3EEhNUIuv5Qj6MJY7tVbOV6QP63QXuV1rOYFWncxpdiu3SSigHrYBKRB+6wKCuHGqsMieG2RzZelO4SC5j5P13/Icx9KkdxnjuJm5zADgA2aS9scWAYizl+7D7jpae+YiESawtDPXJP9qZBkmCob2prfuCTTKIj7nXd5ms/jT81awgl1okztYLFFKKOF+reIk5gMHc499s2Fzu39e/sIJf57yD2BlhQVovEEaPySnRfxVT6dc+xlnchLTuF9OIs3FMlL/xbEyByTpTZlzJtafE+0TGEVFdgEsbCxn4GtoCxBkI+uIkiaDYmJgYFQwkYmcaS/nBR5nfnaArpF+oQfTDlKiRVRQIWfxiqLIUr6RffVWmuQR3WKUkYCPsvVaJLL1yjwsGCM+ltVO3/xUfsU7/E1W21XykhyvT+g/OZcO/QXvcCRzQqsUt+N0Pdnua6BYpLAwKaMfQxlDGX9jBa1ZbvAA780kuIAE81jKGprIIMSIFOlYxnKe/Fv/pR9L2lOLtRGAkYxCuUz/JYBEuJHz5G366p3YPCtJf0mFnwThUccuFFluZev/XIOQ/M6a4Vd8I/vwByz+IafYj0ktV8kqDtL1IVkoo+UM+6f0UIQUHZj0ZQIT2J5RlADtTONzNgU4ZAEd9OBoegNJljCHL5nNKtpIEIdy+3T5sTyhdxmfeE0UA9Js5FW5ERTi3MSveF5+ag/gbf05r2sST1BK80BmEMeSbfBEtskx9OOe3v3irQsxnuNszmOtXqblgu6nx8o5erck+QuL1FOhJFCiZ+t5DM9WxbZTwn4cxC4Mcz07zlDeZwmmb0QLUPqTcL4zjnGczGrm8Sbvs5EEUTTGT/RQfYibZb1bw8ksjjJaVBWKuIWzeVTOtxto5AzWUScB05dQouTWwO5KDvEXCAt4Sr7cnytaLucwSO/nYq7TOPAUf+JauVd2pVxXSQCgZyI36dSsvkhSzhGcxCiKAvb6cIRF9PMIukZWYtDfC30xgAEcxDKm8W/WESeK9LAvloO4kuchV98IshgE7c51nMXdcqG2gyivaig85E3ZDkt5+oH8ECFYxOKyk4SbrBvkJH1PGjhA/mH8TH7MAnmbEeynt8jzPmAlzvn2qzpVUFqJcBSPcQM7BsgBMJg4C2n3THwRm4gwNGSfmYzgUp7lbEppJoOg2+kT3ESFN8VPDS7Rs7hZzqXds6jF7BG2WhLiev5AaUBSsEDNY6+fnjm710sVh0QftP8pJVzIc7ofp+rFtGaDfp4renGX3qrdhSTN7Mzt/J3xBZ/fhwpW0uqZwre0EfOINu+rH5fzAIeSoQ0DjetveZaxHq/KlnvlEv6EevKwyrhSX5YTBPeP4SODhMQQuzge4s76VpfsVKcug+0zN1bMHnfegE/7/kOL7amKipzDI8S5WOf6EpnH6zN6Bhi0EuU33M9eW3x+L/pQy0bHzspO/2sylNFni9dtz+1cSy9aspPfnxd1X/K1JwKLuZEmT+BpO31OL2EVVrauXwMRDwlUQ0rXx0MKJ8G5lvlnRg+zcWFzA8krbeQE+QvDuYkz2YP7fAU+u+kzTAGliSHcyUVUfMfTu9GHFpYSzT8zzVKUanp/56RP4FEm04QFMIynOVHw5sW4qulP4Q05QP8pB+kzQcGsoU7hD5SXVSh52hlKTCdxl3FR06gNHzXdvuGkyMv6PifqG9zPtyz13XGyPsVowaKJPXiQqd9jFDEGY7HMtRfXUIPQi27f4+ph3MMpdJDM0vZfnB6246VK/6mP0sZxcr6uB1UvMYJX6DY214hsGxWlQG8QAehl389qOZyZ9v9t/qWRlpvlPcpkf3aSld7ESybJYwwSLNo4mqup+p7jGIxJA5G8Yl1CI8KQUBMg+CrlGnpxGyniaAn/xOKxnMiVrC9yEBcxWR6VK3Q5njh6uAcmvjYcXSiyOlWxHdAiWcvcxj6dMXyrcflSjov8XFbp7XISzfI8V7DOE6Hegcd1SNbjOJW/fm9ywBDH38iNZRFtRBj0H4jcX/NHhCQCZXq7/Eg9/pBxInvJ45zGcsVfdCShQepC9ftdYvZ6yZEtK3MRZhi/ZQ5XY+toMnov+8pHeg9PZNfbZb33516GgEWSn3KNa4m/+zWKynz6dRGwBItiRv9HczmDq4lmI8QV3MturihHkstZau/JUDtQ2dVp1LgLPQmpjukiLCtoS3g75oihl0gV51Avh+ljXMU/ZBX/0G8YZLe7AQhJyK06EYQko9mbWbR7wDsJicfnfjNpI+EIDps51LAAkwQb+JKUzzkV/B2ucmM1Gc2evJONu/TmXg7VNXnFvYJL5VmulDNUQUzdnp0p5RtmaIe/TKEQyPofG0pbc9n4UDb1eOi9+IyB3MH9cqNO4Fx9wthOX+V5+a2d9uTIXqeX5RbMJEPmP9pX4gJObGwnzGt/J2zh7d9gEsPqFEX/5lRayffu0kflJD1H7mUQV+hPnbZnj3O+1AcDtv6Y+tddB534QZNONed8epyeK7vpxZyslTyqTxjohfRnVXbb5KAKjtMLOxcmnYX4QjpT4TMzpWBPFW8Kd7BEORc3VI/MTrnesY+Si+TPLg/8Jt3OyDDcet4cn3jQetnOJM5InWxtkIuCuVnhlY1dxiFhjcqcQQ3Vd5nDGVRwrp4qUe6WL+1njffkEFLZ3SwoMsx+WUb7kwa8u812GZFGSJUuAUA82GQMXyVv4Xwx59stcjjv5VsPxqWHrtNXzQPjp0aeTFsZyipaX+7YzRzuxuH8/JF9zesqpe5Wana+uUtOzdvYDVzMBD6UOJfIRO7jVJ2GwW2a6vTlRfQPWXJIvgOiTZo0STpop41WWmilhXbSCJGQthbBkpogAJidZAQTi478PVtpo50kSTKOuHJF3Ev5k1TlhVaSNeynexffwWOWZSO0NGY+NKKMloL+uG6DlbXNHeX8ql0ArecZmaX36QzO5FV+L3foBTLIftXTEeEIOS1LxhYHvjYQTCJEiBGniATFVFNJMRaNrGEFHQi20+tEA/WNEkKcnPofwCCqidJBE3U000EHSVKkyGS3kPPdEkwUmcK5XOu6+yCJpL8y6MCgiIxpb6fIuuDcNTRvqwtE1nY+6N0j3wV1zMBi/bP8gj/zV1TQqs4cK0WqdKaMz3LLLvSlmFIqKKeYIooppphSSoF1fMs3fMtq6mjHIooZyLgXvD0TOwWVgWCRxiBBJQMYySjGMggzzyUdtNFGE8000UIjH9Oe3aMNOlU6Jc6eOj3+cfyY5qYIRd1SlyYv1n/L8VhGoGOpl2fmdR1BwkK5+cjRhdqdOdoga6jhx1zF81ymtb5v/pE/g9LGntxFceA+65nFl8xnBfV0YGNQTDfKSdKQBTvwtngqNJooFZTSQh0tTii3igGMZgI7Z9tteQRNmhv4Vy6W8jQn5vWSqffJGcYcnWGU6l7WMH1PTpCNubX3b4XOLTG3qwgyLrTxqzOwKp7U7QRMTJQ19Ke71sixfOIi4jBm0C+rfx50dbFsYhPf8AVzWMlmUigGxXSnH32IUstK6rGRNEvlIz1ce0hoqzRBkZQ8pSPZzi42KGMgfRE2sIaNtJDBIEoZA9iOCWxPLxc2sIGj2UgCG1qNE/TVvM6qtK/U4yWmFmvkbn1cijmAiXqDbNAAeNT5+rorCRJmkgIwliF8QiXdqaSbVkopfXWcnO+pQbtRLxaEFo7hZqCFlSxlLnNZzmZSTpfSbgxjJL2JsoZvWEYrJpFVfMqrxqu6WRcxOCyG6SxLip2NJXqoHsru1pgMcQYyhsFEqGUhi9lEGhCilDOQ8ezIcAbRDXiIqynO6oKX9ERpz5n2NvSkmo1i6/Z6mBzFCG3jIPnAy6fepLr5XWn2hvXGElTkdp3EW8zkfRrzFxSRwsrrmdG8Rb+sAryKKLOZz0rqSTv3itOHHdiVAWxmIV+xjE0I8VTkXV5mpsxVBKubzGJAuH8sAGn2kQ9B0OEyWX9k7Z+stihnGNsxnh5s4ku+YhXtzsgjVDCA0ezIQP7AOuIA7Rwvr3gSrnvzcz2MXQWdxZsyi09zKFd4Lu/XXUmQQn6vcSaH6gGYbGa2vsybsiobdHA5kVfZV2YnGCXBZjocjohQzXAmMYk+bOJDPmAxjdhE7fg3Mo2X7W+lKdfll27MYsAWhphmbz7K+S5aJIM5VI9N7ZKKCqUMYhL7MIB6PudDvmUTKcdCilGFTXuukfLT8hM68vccyiPsztc8xZssoU48iEBYt6H5PwxBPITpLaWsUZuJHMHeDKSE+TzN3U7/CwTty7u5EvM07UCEMvoznilMxqSO93idb2jDIKLmGuNdeY53tKXTSQTQPIcE+7srkidI1tJy3o/LrhxnH2wNysQtYgxjXw6hJ3G+YCazWEUjaZQ48dwd23Uv+Tw/wV9yO7fz25xV8d1oVZdBJ94aCE8U4GT+pO/Ih/qZ/EMvlQHsp4czMm8YARycI0cHUUYyih3ZlR2ABbzA28ylGZMEJev1M3md6SxTX2er7D7S0BY1Lg9Awd0YUJAk79vvGz1k/8iPdBJDF/E19zCK/diTy4mwiE+ZzTespdUhiRbJqfJ5/hllCq92zkWzCHEj/9XXNpi9wSVR5Eg9gB2ZQJEulbm8zyesQqnptEP1bZmiQJLdOZsR9AYWM4MZLKAOJUFMmakv8joLgvBl/mlFfJvNlC8A36SZpF9JoKA5j1EPlr05hv3SxR3YVDCSqezJzkA9S3mOaUSyuNp6e4Ksc647Ql+Qv+tFWeLIWB3HofqB/KOQ+O5CHbKdz7ZSr5URoydjOY6zFengW07i2/xQJzOTCGQQ7mAfapjOO8ynAZsIcTWW8xLPM1f34UCNaqQgapPgR5QEPZD8/2xep9YLDHV+27DE0q+MRxjNoXKMNT5tpoESRrAXhzKSGo6h3qnO0nPkbkcK9NXXGKd/k81M1J2kkjJMnue4nCrRgHLvQrPX7x+7phuVAbqXHM6eGpcNOluW6M1Spw5Qbt/EbwEyVPNTZvMRDQgRTIx18qk8o69LPdhwp5yTKYATSSCyUaixZvipCopBBPtj2V0AKWYvjrf3sQfaRgaLEnbkQJ5hEdHsNW/IIdiOUD5Ob6EHSW1jg8zlK76ggflZoyWM9F1qZYnP+gdgNIcxgQOJ8Snvy8f6eaeENQBK7A/ZIVc1kiRNnAjSIF/o67zOfMljvPYt9gW7MJWObUxdDnslmM9byLvs6yreHCgH60FM0r4WSaC4c2k3yv46N2tSCDqCHWQT37ABFHowWpbpWglwR/b3LlPqhicC4irs2pebnK3xln5ILbagETL55Jg9dHhuCWyiJNAvdLpM58ucFeb2bKKUYgSAw2B5shTIonR/YtCZb1lMkcMnrtcqvYd7rZ2M/SIHRabaUbtzXt10H2Ouw1ulrGGNDmUyw3WEDNWeMpQTZC0BOHPrX1tFENsHLOZ9kG/0KrozmEH8jsvooN1ult/xpuYCQHtKSX6x1sur+gxfSW0OkhfnCKIcfN2PSXkOsZ3itpwoMkJz7r3ZUW4vwULzZaMJ2twp1tlUcGz66MSv3v2q8V5zrB5jHxkZ6lDMYIrems1jpKc+ooOkiGISAm0s4zNNSCBou6U2t/+jiGFoRMyw18gjNEla05RITwbJWCZJS/6bCdlTATbKl0yTV3OsDmkqnLTMFjL5iEcHTbRiU0Yd9wJn043mvJDpoLWgzlCKKM6TOIZyPzWcyhiasCmjxRVTsYkTZRKVQJwDeb2h/aPqj/Sa+v0ix+ue9AV2ZIgsA4FN2sxHslBrWK3fyErJAEW2r1IMT+v/LvJDggcKCSj/0P2NZaxnPeupYZH9pNyEmc8AHEVf+YTX5HU+c7elSdGNE2jHJMqjpPKFmRv4kg6UCJuYjfARPbAcE+sTFjGVTIiKzwLOXxLhR6QcoWrxBQ18QiNJhITTlCOH8PblEJqxkcpUeWz13lrDRKKbX3l26bOx0fZBHM72Ml6WgY22yolGMRZ1ajFOztUy3pOPCI3LdHnWSUh7vjjT5VutpIr+bK896S6LuEyfz0vzTVwgs9jo9Q3a6cWJxNlMFIud+Ni5m8VOnAps5ipqiKN8Th+upgoLkzE8zF+2MMIrGMSpZIiQ4TrmATEWsoFLGQW8wev5JSxmGI3ZtgR7S5H1RCmjSxsmFM3er8li5bfmt3KP7s4aJ7vEQu3XpYlzJMFzOgio42fywpYK27pIZGkYoJbiVlCISZyYxJnAA/yNN7XF2S9rda17yDZKjBOpJuEARjbD+IyMcwzLZxhY1PMOaYoR5vEtPajEwqD2OwSC8CENZDBI8Qr1lGDwLTZ3MxSDhXljJEMvzqONGbSYxij5RlHD/gUNHR+XMoDlRNB23rY7d/yxbM8/WMbdDJI79UWm2VfKa5IST/+gbWk3vg1l0QHb33beS0kKFGr0SzmI4lz5v7sRWAaDCiwmMYQMGUxDSmgnk8x/02Y0J9NCmoOo4a/YXEs/ehHFJsa3PLvFEVrsyhG0YWCxNy3cwFrOZye6UUQJrzMznzQ0mEpKKaGlnD58BBzOeH5ppJW0TycoirG3NspjtDMU+JzX+VzG0stpCRKosOpiDgl0NOnBEZpisxqCprB1Z9lLPpXN/vZ/Qoae9GQ3UiTZnLWsDmKQ9SAZ2yXMmlhIGigiRhtKnChrUYQI676TQzay1OlYGqeUdlKYxNmITYxNLuV/Oe0YDKS2TEqoI8FZXE+7jeV6QqeBbccF2oEK0DZBI2pL2i8vpOsJQkjSDVVcRw+PX/AJvyHt1RlKioHsg0k7NjbNWff0ZPNx7RCXQS3UsZJWbGK0MhFYTT0ZBJsoq79jFwrrWNoJozOOgbSygBRKKetdXsMq4kg0kyZhI+s4hU36YTYS30HSCXu6Yh4fcSK/4m0G0izzdHu2k6/YZPgg1m3RIttQFu225bO5JnJWtkMbgmhSamWN1rmJZqNUMokybNL5rEMTPZ7DzCs1n4mYFVpjOCLXlYkTiRB38ngFg4Xc8x2e0p4cSSbvFp5ICtPxcqLEeNUZk818omikvTdlRkrbOIFrsnktZVxBiv/jW2IOURTgEY7g55xBQu6yV8lTlHEPmeDxSXRtFW7YybYgm+RFf5Z4pytnYVNCMfsQx8LK53BFYHtu0CpJeLuFGMzjcdqdp0Roo4MqZ1kirHK79qE65G2aSDrdUpqAMsdkhuI8pCFALRE0wwTT1nr20Yz9ibIvw8lQSZx7+DVr2Ugit7yb9ShOkjHM0yfo4ELeZCYeD11/CB3irRqynShesLu7G40dhMUYBtCW37dKEcmBVpWex2CptdcogkXGETM2g9k7m+gIRLiVDxnj/G7Szog8L6RIAEsx6I/pXF3EtzQ5/BVjBdX8Nf/UOBZvuxZPkTS9NUq7HGm/pakDGEsSkxQZSnic2fycZhI5DdjCvfndv4hFYVbntij2yH/rInF2Pb5M9dxwpxAlRSu5FE1BqaBlbPM9lAFJ3V3f19YIY1ngLGqG1rzIUjKcyUQyCAYxlhNlLU08QgcXUMV11NKLen7Px6zkBM7gAAzSpBGEdTzL5mycHMiQci1XgggZrCIdJBlrRObuA9mepnx/CKWFnbifn9Lu9CXyd+228/nI8l1Ixv+OIGYB7z3j4hERVbcN1pEHRZzOJxfI8ZvQGYZFJegAnuctfm6sG5rtuYtBDXMckQVCHTsyxfntb2yijs+5nA8x6cMNPMfTVHI/sJm/kuIK3uYzTmfPLHLI43xFwgkCF7Myn1xtECVKBtuSSdqarj3y2+Fm294ykk+ZjZ1d5ma2437OcsavDtqe21amz8TpdAx+oF4nvsd2Yy95QS0Ol17cJ6GBLGAAN8oo/caKsI+WAilZrt3kR1yb+WVV+458hYHFWI5xwBGwWeIg+cqVzOBJYiyhnO1YRJy5lLMT8GeUKN2Zxd3cRIJTnEE1Usnx+VIgk4RzmpjF0STIxBmjY4wJKavP7wcf0H5hZopGtCP6kl7O4uzitjGYCFanqh6uw5gu8BNp0BcKN2/qUj8keOq5Y2vdqmPkVi61F3OfQEKS6hud9NKzjdfkNDstEOeverRcri/RXx63fyR/MZYaThe5BTyfBxCFZezrwAFfEnUKn9uoZCkrOIbreJ44HUxlNcfzBL3owyiHPyDCZp4m7hxgX8qc/PZYQXcMm17sb8d007gRXJGuFgRJZI7v6FV6grEhq2WKvE5xBffon5jGJfZT+oJAAs8su9wPCR73I6hgktE6WahX6FQmyir663m6Vm7Pqs7Onom6PZ/rS86gk/Qy7rcfKaK1QZ8yilnVWYTTm3Gk83vvK8dHibI/1/M3BvAVF9KDPdnIyWzkCXZhf1ppYAL7M5o3OdFlcxUxId8wM0KDMwuDL9mBWJrpxgOZa8tKep1JcScvp6d2/D56iZ2CBNfTkSWFYGpGvqJNb+UkHWMUMVZ/Jl/qE8G+QF1IECvcLzlTj5C31SDGgaAH8L6UyDG2Zo3ej5ji1EfJTEnm6/N66G7GY/b4IoNFyfnW15H0BuYQAWxi9HBxSCxvDp9PJZ+inEZPTuZo3uI2GmnlM6rYjUsp51RmcKCrONrEoMwxXZVSivJCN0orKWykXqkoKmYd5fk6FCF5UPTaSC3A3ryQO9LyQibp25qRYvYDPZ3TadaHUA3p//IDoL0u1aX6qh7OjbkNrt2lu/ySD7JxAZMJ+SIxDKYyVOv4jA0yWGJ6NLWpvwzuaHp7aTNYtBNFMVnGK3Tk+aoGaMcGIpzMsdhs5nE+o54qujOeYlYzn5mUMYHd2ZnJZBybLmsrvUHcGXeCb1zL5RTRFWVz75TdnHcUgVHsIc+DzfYIFgaq/JuDuU3E0Yh9BTlF50bwn7bepQSxA8rLBlgr1+qeUpUXa6qHUasfsEGAaO6KKVxjby81FNFoXyAR+rKveUvzfoveqG6uQPgc04Hfd+HsfI2tyTruY5aDCmcj8pvpyT5OaKmZDnoAbczhA/5GOSX5mGGUlSS4MC9mDabzjqs3ZDaJLkaKmrzWcQgp7b+JTdc2w4lcKsBiuVF3J5E3VNIcLW3MkM34DnbtYizL30lOhWO5ojORXBGxD5Od5Q/ygDqlmIocJA/oq8bvZakWywX6sLyqGa3masNMPtt0UZ86i2qWOz0VZ/EQbc4TItSykSX5c21tTIYzkM08j7AewSRFb4QYA4mxkHXZ0huUKKso5V5XAfW84P4tNtlMET2wPViUtZP0lSWuw8+inK6XkHCVBUX1ON0lcp686m0+3qV+iJXHlDqZVKo4WDOykFF5ErUav+J9acwuCghUyul6sTzuCL3fsL3+XA1ZKRfq2sjE4n7puggT+NzBjvoxhdb84kwl5oq6A6RJAmmmU8+5lHEvSzicMmx25AhPYSdkXFBLKUnHU+8EzYlDMyMpzqO8SgYbY4VsyJIjnR1JHzmERlnB4LyeqDfPtudkMbvOTid21yr1Sof+TY7nDIps5pdi6+FMy7eHLZER+qACGU6gJykkVn+15fSPsxH0n7qXwB28BfKNYQg2a/MesEm0U9BhB1KCTMpo5iOGcwxFwJW8zkdMoTdJXxctQZwWNaBEiODuc2UAWpJCKWc1DTSTJk5femOBpjLZ+E1fVmHCGk7G4ly9LR98qGYI0zqrHTNEqOhqkXUYYJBkHjVEqcnqcY2lBPZS5E12pIf8HyPsQ82Hsg38ltINA2ql1lMX+6Ys1N4yI+v3WrZJB687sIXBCt4MQIhuhMzgc0o5k0n5zw5hOP9iEZOIY4UmDWU99a99IIiNloLJtzSQxCIDFLELu2CZKoJSxtWchonakjLQyWLzpuyt7fJX9rCPkKd0TcbBKsoYwHjHgOgygmQRolaGEWModbRi0sJcIqZRZf3a+JfcJ3vJXxSZKPXZpJ0v2dHZozkMyATsZt6XPfg2u8QtZEhhOka1zc78NA9E+iFtweR94Me+Vkwj+CNPUcwxriv95+ZEeYl38llZBmBgVxhALeaqyD3Delb/aoOxhk/oyZD2jJW9ur3TvO+hqmfJs/ISJtdZpjHBaMuBRiMoYwBtmJ02ZVdGDNMkibIbNURoow+LrFXnd2sawhePmXMwDFs/V8eKieFvyOIQZ5Hdg978VNfLtHR9hgyGo1YNZvFgHu0NZgVGmcfEkM5YpQzhMTaFnM+OA8IkmO+MirwmpBwg2sJFqecGbrfrqQ3Vm3mHxQybZaRykL/pwIhWk/EbNoHeYZZGOcma/fk6tkexqALmk3IE7Q/QlTQrQFrpIEqGSNkhJUWaKd2Uib9T/3IVVayhlgH0oYIUTZ4TqfLtwWqkRB9mqkb5pfFb3k0whbcwUJT+THE5hn6QW5jIJ6yln29MjSzhNKpIBTIbc7qphA7eyJPEQiBmlwuqUiL/SI/c8Jiu1ure7M8S7FfLNZukl+Ak2tlEObOT/ZJxNtPz5Z6lPfuXMzGzpLW0OUGa3qzb5vPUt7FNrGBTQpQ2YnvudFmfgfEiS5saB66u/abl87rZ7St2ogUTk4hjmTlL00cm22NI8ZYssnfhMR6QQzmeR/VoPjNdWVapXAWUl6sc/owA73Cab0yz2cDkfHZXTix1orEGaY84EQS7xCi3kWdo1LNj166369+L7dBOOaMsWZ5ynvo6RaQwsamgfHDFjiN27TO6on9ZlUmv9upVi65v+qCYaD4EwQ/Ttzdr/ZSToLbfkQ/tMTSDnbVadskcXceyr+e9s/ap1EdunFdQkfP0TCYoiqy1H9JVxq0s4QlNcIz8lK80g+OHrOJzV3ait+lGFBshzVf8yNNfK8P7wEwsjLzf4c+BL2GJ+wBwFCq0TDI8qB/JweaATWfNv2qXtel+NtX3xhbZQCIXFiRNye5TThy/79Dx3YjkhXB0uw/HvDi1em1lvgEBP4RjiBPhy0i7lhy989BWJ96Q3b/V9Bi/y/iaU959fs01uiI/TENu4RyNiCIC/bjMWGf0sJeoTQtoD6K51OwM4zg6v6ieQ+0xeQmLIzB5j/c42jWiOVRxIvAp3/ATT8s9t7kc5e28yDIQqNAi3SyLpFG/MAbo8JWlk24w/2Tfb/yf3S4kmMkHxFEyg3e6fJ+j+3Yzscjk9ZuQYuehbx7d9s+0ZNTath2+bZ56O5V79j6qvEePRmvqSmIUESOGiY1NCsFgQLf9zpo9YvnR+S4Ox+iPjBOYyxCZxBjdTYdRpZW5/g+y0DkWFcFkEdNDIudCnFriVPE4ldSxnB/lvRV4hRSv0IpJN+5mkKti0+WUM8eVrC0AFcRZy1rFWKbE2FS28qZBT6c2ZAXa+3xAAiVTtcODO+7Vj6RTL2w6fSJStJFmwFmRkeUVvTdu+HfHB5EfgiA2Uj38DweeO7A4w2QsNtNGHTYRiqkgTrbcpo4W9t3rmdPW3xohgg39M9fK8wbmUt4CHSRXcIqWK6gsVou3Ok0Fm2oGOiFcb9uxFj7mMmLcx3iGEOejfDvZpSTYkxgfcgAjuIqBjMp34OrUIjFWOp5OLiNAuxGT9UaHDWkw63TGS/x8gwAR6nibYjKk6X3afnutpI4yJzcsSSPtpDGIU8bJO5g7WExm1blv3rnhWru+65Mcuu12395HRWl3ENNKqrBJ0UorjcTpQ5QMNfSnhL6HrvqXtkIMeWZo0mR9Lot9Jecxgu4A+p584O5VqpQx0NMZLkeULzmZgcBRtNGDNj5lirO479GToTSzNxOAs/iUbsSdBIxOsRWnKi/ns516tYqIttsIVIHOYG4kj0fNREgB6ZK+hxbTk9WMIkKaVXSQoJQSYmRPb8gAFoOKz/jte8PmnpUDU7qEIMXY9Lhyr6PIx7xzkeYYcapIsYlFDGATFZSQYfCu8wZlFghR7LW9SdBEA5Fs0KqDbykTbOzPE1dUdRh0dvBZyEsBuETIsI4YG9nIcyxmChZNzGYwShM1lPIOnxEHRrCRuaymOIArFfF1njsmEcOGasdhFR1DRh7UpJIhShH/5muKUJSSQYN3zVBCJSvpzmoqGeiga7YH/W4nyl5HbV698QKzKyOGRXtPPdfIHy7vB+bj9KOd2YykJzZpBlT1OKJmgdJCMZYD0znHGRmasqLZZhjGzJgIDQ5JLHbkzLzidCVPkOFLruBd2hiJTRkWv8DMqlb2wGIzn/IKEzmW31LS2aov/2+cabzn/FaddUT7KkYM7H7ST+8zXoIML3I6jax1AmNKjyMGVKVRelPLbHrTNyRQl12bNFGmnPvatPR7XUaQsp69zukWSRdwgQzSrCDBHhhYjm88+sxZd0qjzSCizhSdtmdK2ozlaztUeR3TgdcX8mJeqXfmBmYN19HMxWYNN2MjmI538i5vYRAhRgkjUd7ML5nbNSxlVv50Kyu7v/sJdjcRo4Pf8E7uMLJmbmdtLhJfsfeZUZIINr3pwVoWMzirFUNIkqFHZOA5GxYUONfvv0+Qhj122V22YAovp4y+WPmEmRTjRg4+bdM/E9gsxKTRMToFVckQyzF3lLfyO9mmjN5OlX62uVnG5Yz2YjJP8SIJomTbPFkUO/2x2pjI+dg0U5rXHRHX9UVU4un0VSTdFenHOP1aXs4Z1i08Syt9nRSm7qeNG5nbgBlgIOtYzlAfyO9+Ddh94R7ZwzC6QodMquhmFyCHsJpS+pF2cpZyNU+Tzn3tqdKNFusAI590JqB2xCaX/7gQi4izWN2ZQIOzbG0oJR48K8E5LOYbLmYoC5hPgmom08KVVPFrhtOGuztwGzZFjitXwWIvNlZhFtvYveUkvbxzHo1AT6f+MNNj0rlFnnYUGfpisZrBBVxAm8puxZO6jCB9h8eLtQBB6skwhAxQyjS6sytpIM3YsQvP2nhdzGkMnnZfEsklCDxNQ35ABl9zL0mEChqZhs2xVNHg8r4TTKSG6exFAwmiJFnAIlIcyidOAEqcpAaDZ6njUIbQRIYivs576hYWWmS32Ijo0fIvluUWtCgvJlMMPWvs2Fxaquk0y8nQj8XU082dr+XaMLHifsO7zsrqZhTQHzab6O8InG+4ll3YLT/Bnc759PnSb7Mm7PpO/0AkksvEsvO+tY3FbvwKmzRrWclTwE4MoR8xrLwZHGE8L3Em8C4NHIlwDafyG5eyFSxqaOB5koxiMr0owmQaH7oP9WuTdgGkv/RgWY6rujnVvtA8esdzikgBJSxkAYcSQUkj9GWtA9xoiCYt6tZlBElHwxlV2EyMMtLE2chFRDjBWT7IMGxg7aXLfpFOlQDRvH4happZZCqeL0bIao35PIVFA0862SOXUMKJVOaz2LOIVxO30ZuvaSBFB9+wJ486RXE5m2caNRjEuYXbOJLBRPg830gzTgLZmDYVgWI9XeZLSy5hTBDaiMaGXDp8YBqlmKWcT4QDnUyvJGVE2Zz3avw4eDraZQSJpwxMDEfuu5sSb6YnGUwMrmQddzCFDtLYKDZRtj9t9Vv1jyWwSTjGL2g5mlXaHq4TJcoo0tRSTJo0EKWIwfTIG9tZW+g82immDzZRkvyChIP02g4PpSgjStavMOnPCMdTV8mKTpuMrR9yKLAQIyc8i7FRDJrodfx2p0VIE6eRiynnb7zCfZTwB3YkTXfWO0KrU4fmzt6Jp7qMIK1t0GS1dUQj5fE4ZFPNyCbLlKAkuYv3OZ5iHuNrhnEqSZQ0VeaE69/6QhYaCOU0YqEiJabajphyhZJi8DU2Q+jGrazh98C19Md0BqxEsSjKwZsYTgJEtmtpB0YeCLe5kiS/ZzUXMhmIUkxPFGJgMoPDiSB/M1r1LP6sz4qdNYzLSGbvN2qn66vNJEKUB2jiZWZxLRXUcS93OIZGBzFH38SADpqTmUxRImq2tnUZQb58bdX6zCcrN/QtjQ/vv+eo3YaWZxehiSKEIj7iEeB1XiZCI73Yi/4kgRRj+2+8be4JpQ3KSqJUYSm309fGm5oqMDvGRi7lSIQo9bQBn9ATg3YyQITlrKedPgyklJiTNpGhlXWsIE4FYx1lW4JNinrSzCVNMxGivIqBMcdRvljQwS3yqp0RWzHIsJ619EdoqtzxtrH9UwgmDbzFAWzmYrrzMDdS47i3cZrpSZoEwrKmhZ+u+SC1ZG3LoF7mpLpZx3UVQb6+w7D3JEYRs5lT9MWug0/f/7Q+0QztlAJJxnMYCfoxgun8mx9RnXegLCYfsOrahl9H0p/Tn1IiWDMkrGf6Ixwf27eGOn6PTTN9gMOIMZfRTmOY+5lAGbNpoY0IMYQMKTKUchS9eZ/TiQAW8xhEBb2oY38GkCbOoyzFXMn12fF8xYTs/l6cM5FbWcoGDicTrbp20gGdJdEJpvMWKW6jL4uYSowUki0ypoia9NuPLH+49jPadySGMONeNbqMQ2J2zo+IEW9vnzHnw7lPH3zV3pNMZ2HiXEKCIr7hLfblEmw2UEoGwSLG4b94e+nam2NAE6VOxpZ6wBFFWvX3Mj1W+RLdGEQbFcCHLOMb+jKEBDEWcya7cSibaaGNNGBSTAkVVFDH87yO0E4NK+jDdkTow1d8RgkNPABErtBV2XyTNUwkQ667fIZ6p7N2iv4X7PeLGCmyHW97cD7/R4pr2I3LaOZUOhwOMxHe/mT6Vfp2NFPmaDiDRLakuCvjITmPOkJxpnb6wo+WX7vzWb0S2cI0E5sa/g/hOF7kY2bxJ3agDSFJlTHlulfq0g8KUE8VZU5iXCwfsBWAz7jOuCHNPfyUybRhEGUGZ2JwP+soxaY7UJxvpOQFP5XnSFHKjzmOWygn5rQpWM6tJIk/oE92LkAu6S9F2skeVlKUnzHluiojiRJDmEcdw3kZYTWnMJ/r6Eer4w2t6Hj7vtQfmpp7Oscmb0u8cBuOq/AefRWhpPmL8zc2j7nEMHMG7V3MI8HvSFHBKFYxkggZhA56Rg+67fNk6xOC0IFNK8VEaMh3gXdiF3cxyTw6zXscyghaEarowTimUsNySrdwWmERl7KOAQwF1tCNPlRgUUor95AiNk+vIkW+n0SO2xdRSQXZMQ066aDbekaz5IC/8CIWMX7Cr9hADddzgBNcNsB64+9r/zAxb2z4RG/XcYh6sqyUjVTSfsvSE0cPtZxkgu2Yx/YMYzgjSfB7PuFKJ18pSf/Sy+5+yOh4TIiwmWX0p5SV9MegJl9bTpP8RkbHxizhbA6jEptFrGUc0NfBmAq/RuY6bdLCUt4iQpRmXmMdiTrOZ1UndNKDRmrpThuNdAOUDkac8pM7+5d2OOHeK3mN8+nFv7iLGGcznh605A+hWbyy7ZZKX6uPH6STgz+Y30KCzIZ500ZdnH03yUHsSRVxhPlcxmJOACKkyLZeGlp29r1PF62+r9Q5TEIQ+mBQyqJOXHcVZ/HvRI+NfMJNdGM3HqEHO/0H41zNffyYyQgN/IG1FCmXyIxcKoLNKMpJsYkykg5QmGTgWcff2r8oa7OWcA+v8VcOQNiFM3me06h2jmVyDJxp5gbT0/Bp217bdI6h5oMyfZy9v/juJfXxPLnKSNPCk5zMav7CFTSSztfCttGn6LS7Bl/eEXNDdimGsQ8Z8v0WP5JfaGsRi7iaz2ihBxfyFN/PwLd5l3NpweQrPuMq5lFsyx/l/lzdfIa9GeY5m0ToiA2+/NS7+jjkiPAUd3MxB9JEE/2dnvedlcExFtUvvtvAxqAP7b70oh8k6wSEctIU59L3l8y5b/glRv4gU+WvPMcwrmF7/s4LDOCPDHOKDJJUmGf+5aVBH1+m9Z1pOhm6M5xG6nJ75Xl+qXfGi7/gG37OXuzIVyzgcHb5jrEt4iXWcxrlGMzmoexW+AvX59zFHpTTwyluy3V6bKmefP3hZydIOq5nI8/SzkpaiWGwkk85giJXZbDNnPuiSyL5tYjmw2tb/zKv2oqLnsagH81UUIvBYEqJkSBBEQk6lpUc2bvSykc4lmJyN0P4kqvYA+EJplDtQIsWCcbtbE1YNjtSW0QDVTRTApTRG6jP7bI5so59zHiKBvZnT3agkRf4CIMBBRh8Fg/xCv05hR0YzgoeopEo8mf9s1hZwHEIY6nGIEMrZTRQQRu63SH3Hn181KlrzAIvu7Ocl1jMVIq4APizk+qajYsuXLHkguLNCRIkiFNOGZuw6EET5axFXVWOXUSQSjbSHSVFmgxp0mRIbm6JDj0wlq+y2IlDmM9DjOMNevE3pjOHQ/INXWwMxg4bePDK9cn5TVTTTCkx2rDpSSMdudOqZrPI2MsoXcebfMwmQFnE13zGNzSjKBZpWtjIPKYzjRksox8VLOVN/slT1BHPyO+5VqysBdeN7UhhU0SaVsqop4iyE39y3947586Jy2U59mRfVvEW8/iQ2dzCgHxUxKSdGdfUv2KRdn5SpIjSRk+aKKMGe6sIsk1+SJooEV+GOqx4dO4JkydmXJmNm3iW07iQK3me87mAr5jsdFeEDDY7Dun3xNO7brg+vbHTmO5gBBHmszmb4jxNN3BbfKdWPqOSXzEYJcUm5vM1H2I50tukhIGcTB8SCK18yIssJkpirfyOJ7KR+mrGknEFm4Q0kR4DLjvhwh6SdCDFuHOAn9JCgpu5kUeI8je2o8m1cJ9/vuLRIsfzyJm7JhHnIIytPYVqqzjkKQz600oHxWTIkCGNRZoMFmloqa8bdkyxmeORNMN4j6+5hNU8zoGMYRyfUklZvug5Q6lMnFwxpWbdusVVRJ3lshBGs4HmbIPM1cZr9DC3E1nIh9jUsob1QDe6U0kZFfSmL72IUs96lrKAh7mHTRQhb8vZTAebNNVMoS3fIjaBxQZ6HXrYXccfXySp/HEZM+igT350aXaghUM5iqa8qo6wOf3mbzo+t/Kzt8hgYVNEG0opa7eSQ7ZBZLW4KpHEBT0rurB10IidOsthYoziXlZQxudM5BQ+4BJmsw+lee6ygFH9Bx3Z0bthQbyx3eEtgxgJBrKZJAY08pIsN0ZHutfzATNopS/lDrBfTDERTEyiNPM+D/IaizCJbeAGuZhlkKGYiXQjQdJpOSskaB84/k/HX7vz0KQjqgxK+JLfsoADSGBhUMET/IXzOIA2T3uZdx6qvVFVfMfa52bcxQR5CtMhiIT+oLWflx4+oFtOHlsMZAyP8xEj+DXvchnDyfASe1HpkESy+zc2YbfuRyxJt823MiYWBjFshjOEWloRsGU2Lxpt5jij2GI1q4gwiHGMZjjDGEw3mvmC1/iSJCaxFuMh4xdMo0NRunEAvWkhSgrFxEKKEmcfdfehh1TEOhyv3cBmOYPYxDss4wCiFPMS1zOFQzBcOZBx5iz69HSaKTD/DGXUbKVS36pW48cSYSKrO7tIhfgAZUce93TfWDK/f4qoZynj+ITf0o+HiXMWae6m3KODDKK0MmPGpzc0vGe3F1FKM30oo4451JBx0C4ZpmdzUSbaik2cEnpQQoQmNtFMCigipsaj9t+ZJQ6y1o8dqaKFdZTRTAdGUeXeu126116lzgFL2RF2cCXzeZzuXMB0juLPvMb/cRKXOK1zctbV2tSzJzS/YBTUrUn68TkZpnUdQUx2J0kE//mvnTWAltH3Lwf83nRF00yKeI4/M4ybGYDBBo7nUC6h3VcfFSHGBuudad8+sP6NXlYrfSimnuXEmIflCMVMvHzhBYM28THLyTgnsEE5Jn2ZyjpeTRu7ySycDK/tSDGUKlpZTynrzD4Hjj5zv2N6mqn8rhfirCPGwzzIXtxCkvP4ij2ZzbH8lg7PLCzeuG79Hw0bChzZQZoiPsTiua6zsmziDPVl7nk74pj2xutn7LTfwYYrFpKkiBhDGU4LJhWUMx+II659ms2s6maedPzawz5+beHjdS9luzZalLMd83IeTKkaUxjEj3mEnZjJWD7jKCp5l53Zk6d5gWiR6SzgeMpZ79w9Ga06/MCTJx/Sr9jyACARnuIBruQy6niRv3E5N3E2M7iAc2nNVlnlgY0Zr6evH2Vbnpl7+/aarN3qwuhtMHvTFMrNcjq1NS09v+/L40clXVccQk8u5jz+RAUPsIJDgc/JMI6q/AFd2dS6DH2Kjzm29pB5X856mOfSmzNkKGM7Zuef0UT2LLdx1HIITUwFFhAB2lzBgR0oI4NFCq3SY484ffudexWrK2c4AmRQyqjlJnbgSpbxMIM5hV9Ry0m0ucgBUb5euPT8Hk0ZrC1u1x8AfteQd8RX1mku+fSX8WkjKjpJ0spEbuMSjqGShYxnIqfyDXGq+RlHefhESSJ0K95/yqQp317yyVOb/m3NtdNxJ7Ui57xlSNNOB81OJUjS8QI6c7eKyaDRqu27HzXxxDEjSsiQ9KRltLGJftgcwTpu4Rpu5k/8nGsQjkFp95AjzuLGT39pLil0Cq78Fw7X2EpwUQvgv/7jIjLvvHZOTUvc9b0mxnA/+xHheK7jnyzgXO7gIG7gHqcrlfu8hQwdRNllxPl/PPPfjIw64SQNiEgNBT9txmaTTUee8e/z/zhxRJRkvtZRiRCngd9yJRYmHZzK7rzCE4znbIbTH4ukBy6MU9Py2jmZdwofS+FtWNWFHFL4cGL/SR5C8sl3yva7pXdRZwFYG934A0lKmMs8juY8MuxGKXcxiR2cM6O90+wgRrkRS8byWV7+IFDYqerZgyNjGEiyzLBIekRPnAYMqqjidV7gVFoo5WJ+wmMcySEcTHW+z4oTtmZd+zsXJZ9MeELNheWHdi2HGMSd85aTJOkgGfhpI0p3ujOUxL2zL6/PRF2PTNLqFL2Z9AOaSXIAFp8R4ym+JSzDbN1qXWHnu59qoLRSQ/5VBz2wV6xf3UnCGCXUcgMn8GPmcCW9uZ0VFBOhFOiFUEp5gBz1mdmXJ+4dSne6EaONJB3O7JP5dcj9r4OirRZfWy2yTKdURfPnqmv+t+z/hRgx4sSI/n3uxbXJuGcHWbQznCk8yWIqifMuKXqxnL9xAV9TBPkDXLJPXPNZJGM4DQjCmkyGHRZhMheDCJHM2s86v13DjZzMB+xPA/fQjfNo4iYa+ZhzqOYS51R3NzkSbEjOvTj69+xsYk4Omfpm7567udUwvLG1Sj1o7hE40js3SAO55Znz1zYX+2wRmz8wkJ/yFy7hZgYzhZtJku2tFUEoclLQTBp0xasVVLpCU+o55F5D9Fn27zYqKGfFqw2aPa/9YU7mXc7jNU6niBV8xuHsxTsczvn05W4GB8qQilnb/Mz5couRn8/3C91JVxLE31FOQ4jl/p5B6t6nfjavttgj7VNUcjsn8QkfsAd38iUzmUQckzh1/JG/UkMkmxY3V99vYCHfhDwr/Hy23LGri1lME9b7y+dGsTHpRRN78iNu5ERKMfgFs/kzP2FPruYO+ub7xXeSY17tkz9L3WuE9q7W79CuXUQQCZwnsyUFl5PCHc88cPz7c+Ourr9CBwZn8zBPcwdl/JUxHEcL8C5n8TpPsw4Dkw594V+rWzc5kRDLd5C8hvKu5AuGNtPAmtYX/tWuEWwOYQemcTRvcQ5P8lNamUcvfse1HETGaTHT6ZUneH/uA8e3PxP1CcPCnRU1QLQu4pDvn1+REx5R0jMfOeLJaUnnlDTHMKaNONXAA9Tya8pI8xf+QF9iTGICFkV80DRjkfRpqU4VdRhJ0nSQakzaJhDBIIKR74ZlZLsMkdRkSwdpknREOkqau8X7fbj0o+YiMhRxOB2M5HWO5y3uYyT70UYHHbTmgZQctpXkyWmPHJGZGSuQ4hHWvXhbeSSyLTrEjeRIQWRHXB0YrJUv/3jTJYdeNKK6w4WeZhAyHEZfxvMZHTRzA/P5guOI08FGpPiaB+vq6uqaapvrN21uru/Y2G5p8SqKWE8DNWxmI83UoDSwjpU00VOqD4/tnOhTWdWtuqy6rGf3bid2ayvZSClpptKHpTzCNGrYn3Pp7jGIc3hanKX1L//t078Wpc3AQhfiEXdf660lzFaBi8dgchjm9+QgpYwIs0mygRagG9a+B1+5+9Sor8Y2RpQUS/iYE4mwP4O5lygZllHBIGxHCNmk6CBJu9pikyZJhFaKaaQ7Bu3Z0xGJOnHuGEa+rdJKGhkKlHAtj1DFfpzEOJKeUrWceE3z4czpV0fe2QiU0ps4O5Kh2SdQtABZBItXyGxNRdu2nY5Q+HO3vLVdoktREtS8M3v23HMOvmBkr86TbIQUaWAIYzG5hkaOpZRWGrHpRlOe1wyixBFEcq1uLEwyRJyEHrDyx7BkY+25ozOq2UwT5WTYlXIOZihKiwcayR6MYbBow+u3Wnc11PcLMVLcckBcJrjgrhXeeuEV4X/wCh4A5I4rgklR/YJr//nv3f8w9fB+5bbTOyR3RTvCQZSwIyls1tPPfZZovmtoDBsLwykvA4s4GVLEiCMkQ5Bok17UUEY7k9kbi7Y8KpZzdmMIa5vef+mDa40FY50SUSNgWIvrwCcJtSy/v379HxAk3OIOb9avLlDBhAWfnbr4oBFn7XHQgLJOAD47+fHsRBsW6ynyhbByyQQbiFOGSQ09iGATYR3lxGhgE3X0oV8Ai85QQQPrGEA6f1BM5x0TKKuaP5q+6L7G6VEf/OPXmhKYk4TIhy7kENkCMYLnM2vAYeqMQpjTX3qzZp/+J+501OAeEVeTihRpTJppYmRIY/M48/grlzKBO/iGvpxJNQ+wjJ78jGksYAQGA0OCA9nK2WZKPPfMdgZauHH2v1c/9dW74+yIC0sTH4wZPlv5Dqe5y3RI0KbCY3eF7xz1iLUyu/3tr9/75OaR/3fAKeWU5ssDhAxr6IsZIIhgU0SKOEt5hyt4gHcYxCccRAUlbGITAyktsBwmfVjDMEynA7CQoYkm3nxs0Z/LlsSsMvAcCK6B1gAasLe23iH4n/ghYdwSrtbE13WRfJeFqNWysGVmN1pYxkrqnSjEGkqpCA0EZehPH6AfY3mLtTSziGZqeYUajud4unMn4Y0/LCooZY0Tl6xnJctooRstM1sWRqxIAH3Y0hHIGuoQqu8Eni7TIVoAiP8+4k5dyj5FigZKsCsrKCdFM600EaGFWKjYyUH4dRgI29NMKUMxqWAfPmEDBt0xsJyjiMKieX1YynzKyBCjgjKiCFZlMQ1U5ltnurMXpcAcgifgqouYXd7iT7bAyF4xpgUQ2WylSE8syslQVgUpDKqoBjZSy2gSpAqEQw0OoJIktdSxD5MwWczDHMp4vmI6yk9J+KIquecaRBjEXHrR3SnYSROnvKqFCBY9SLkaWYZ1aPBaVEHtsS0K/b8UoFJXnx5CvNpOoWa7ToxVShhBhqjjL/QYkIPlTdrYxPasoJ4+VJIgJ0os5zw4gzLOJInNT2mmAhubn9BCBWkmsj0ZyslgOGYxjr5QmknQxnpGsB3LKabYqdQV+g2IOweIZzBZ5CRACHbIlgrHIzRE4XepUjfyJ5m591L4gXEmJoJJAguhB9VYDEfoACcGbkd6jhCndV4bSxlAMSbPEacXu1FK9uzCcSSIYAIZmikmRoYK0tnSZsrJYGJTQoYMm6nAxGAhFhUsYAWNVGLTi1bGo/RiGcModkzqniPWRoxMTpT2Q7EYhckmIO6MXjHyR4vrFmLpkk8l7zKCRDBdrC2EHO/i+aQVgxJKqHaERAT/CejJoX3HZMgWuNXQn0qSjGMsE/iWJOtZQRMDqWIWu1HLWlooYwSNLGcSY3mafanlU4qYRJKP+BkvsICBnMhMPmAoNusYziZ+wUO8wdWksKhGWEY/qsiQoe+YL4exUPLBApz83zF54VqHRUcgdCueDvOd29XynKX7PyfIJuDlAANr4LjizlIYwQzhotxUMux+RJ8yMFhNC4OcEupW1hOhhQTCcvZnPMvZTIJ6VtHAVNaxnEkUU8vz9KaZVZQyk3I+ZQiLOYFnmc2nnMoYPqWZnVmBzcHMpRoLyFBNhDW00A+hZ1nZ4esWim+b4EII6sFlAkuBXi+d7/boSg5pRVyJ+bhwHE/L5JCB+omR/b1j5C/PL6aGjSQY5hQ4CGl2phdjeZFiDmUuC2kmygYaMBjKTnzKEEzeoZIYM9mZFlqZyitEeI2+9CGCTV/eYzV1Tm5jM+XsTdIpq0tTwjDWsYie9GLX8x57Lrp862CisFdFV6K9RwaWN8i+4RpFfATL/m0dudt1Y3sNqB5KMZbzo6jTv7oOk+7MYhETaeIVRlHCVwykG4tJ00w5O/A+ZexCigUUsx3vU85GBnMCaaZRxmSa6EeSYgwiTtsbI19s2spia2XdojVzLjdfL7zzJeAOh9ti7jk/21UE2c4XO5fv4TaK55vq4yurbOyOg3csmjBidL8BA/pWS8xR3ZaT+GMRI5otSkBI00AjPbFZSx86GMgqGuhLjBX0pYp6Sml3jiNOOPkt2ebjJiam0wqwg5rUurVrVy+b3/zVwlk18ypT9veYCSGiWQtAKh92FUG295wS7fdqJdBp19soPCjoFJhAms/p1b3vgLYB1WNGjKwe3G9gv37dikqIObhrZ05LroeiOK0IUsQwSaPESZMhioXhJGZneSHb9DJFimbqmlatWbdyw4oVi3p8W79myaqmpgk0s5xyT7vmoDEbbrBIKJiU/f2DrvND1Pd48YEjYYmmuoXdl8skLNsU2bR01pIX10V6lcZKl3SLDBwwqLJf5cA+fap7lHcvLy2OFxtRIyZgksZyzl2IkCLqOII2GafHT0ozJO12qyXZ3Ni0aVPtupr2VevWrl+ZWF3ZkGquaanTqVRSmue7sFEVFmHq6WYhHiT7BzngfsthfMGbDiQFbffg+1GEokxxg92weY01BxppFoxn4hvKolWllT2qu5dnSpt7lvzu+MqeNLOavtTSixqGY7KYMgbyBW9Yxt/j88yO+oaNm1sbEvXdWrdP19gduo46KqimlGKaMZ3O1uLjcDw7XwIc4E9+Urwny2vXw+9eSN3vp6sPKFEPTO3XPUEvNxcdNJ1k0IhGLG2z26wNaTJAA3OMorN+WzmeNSxgIMvYndmcjMnXDGBHvuQTq+ihMV9XORWQWSEXJUPEcSyDvKuBxfQdLeuaR5gHoh7ibL2nvtWJcvnyNV8Glnd3+fMz/Ak8GojHaQjEnxto9hiXZhYiFUWGCURJEKWIGMWY4PTxjRNHypbS6tQShuNQYeAO+ZCseuaogesVfw6lbuEZ/3OCKMF+J+KajNf09e8gP1n9qJg36qaeO7XwNRai6uoWYedPDsy9C4ZafJutTAxAm+oTN/h0oHuB1RewVcKOL3cLPlz81oUcQiC0qQEMRwJTVE/Nql8PBVWhf4oZ1pBxDVpCDQZx0DaLDb5cKwIRGSmYaOfWIOrjIfURUTzc0uXlCF7Rox6u6PwsqDckoODDFkUCUYbcHrWdhrFBIVN4nB2BqH6YsxpG0DAz3e/4qicGEhTZXUYQCU0CUM/u89sbwV323RaJe2GMfBRQtxDZD7qeliuP3p8rIriT+bxIg/rUt4YYxX4DR7YhiXQbRJb66jPUQwwhPM9PCsad/Xfz71DD0RQS0GESEtn3x2RMWujIT1V98wgPo6mHC8MyszRgCODRMNKVOkR8RTPiWnT/JMVXhCahGVvBwJa6+odkeClfR0sgYh3MstVA2HYdb5AukGspIdtNfKpefVpTA09Wl/bRAnGh/ymHeI1Ct1L3W1D+AXr1T3hJgbjM3Qxf0Pwd+00DZx3iG0szc7ACPrlX+4XJAL+DGyYwg1yvXSuyOjVGMEQVFAHqcr78alt8e1oCEjzNYjpCzunYMkGDZqdJOwtJF4B18NhKW0qlDndm/1sB3G3GstSn4jQEhBYfmwdBSLfi9IeA1pMuGDgttE/Ds89N2kjme5CGRcM14EVtCeTx5pn5TeQudQyVsDzWsFQACVghEhKy6iSPuCRxDEg7XkcYsXULeiBcdBjY2FjOGSZSgCSEpsK5wZOwaI8/valLRVahCvFgspyG+vR+c1hdVj1OX54MKxwAfUtPCQoI3QKJcpmK6wOiS0MCbv6xSQAk1ZDITlBkd4kfEpYQo76W4eA/wdYrrTUQe3MTcz51oYMTF35cuHRMA7CfW/ovYzNlvox8r0sbZvmFx0cksB2ULs/LUh9mG9QFXt9WAopdfR68Fz02WEF7fmjeOLz3bhKI4QX1gwTwhQiNtDtocqFyNN3CHf1bI5hB0OVWltc4lRDLSkMDPUF/RT1qNLtISSdoFLwXPnhPQzwb78g04L1kgf00Ng35ch0JxWulIAbgRyPUde9te0W2RWBpgH3D2FoD8joIznVOcEPBfCYpcLSQFixO9qPH6vP+v6S/Uy4aLLXWgGleaAb+qJD+EATRgupWQwC4MKBOQxBTgw20FNhlUrDdi4R+5i0kCNd1EWoodqy4wvH/QuYL31GerV0vsoIDEcILEzTEHPDGTrIns7VuIdu80HQ18By/exf0HjpxgFYy+YRuLThe8eQvi8fXEV806AdxDAtJ02DoJywkKgGfWFjDuoATWdjr1QIqWHyCTHw6LJi4Y5LmKyrzfBKMcnh1lQZsOw3kZf0A54f4owbhoDoh+ziIHmUPBKspkHBTKO/D70WHA6D+kQqEprzW54+TCSs38GfzhyMG36+y6n9CENtnRoankHaSyw41kd1RRDvE/CVETocb3+HNZ4LaIxx1ykGYLVvc17KFqpGw/PcfgCDiS2OwfcIjrEmLhsIedoDLwrSVhPjj4SKt0H4N2nZubuko6LEHu66Gf4rPPOhiT33Li/L9dkyYE6UhwJ5/h3ozXiS0QlwLmhhBIqkHxQ2m9FBAmLm1i+TbMf8gVlZh2c1/AEWHexBaAKMSD58Vhim8CycFgRUtKIAo6Ob55y0hYM226JFtrMItJNm/n5VWaG+HLYUUhBB1iw0pcSXCEcCcg2q7MEJWSGdJAcj/B+goR8gkt7T8YQIsuMflO0Sgs4wibKndn4jfLpKCfRrDRhgktR9m1IKmhKI/VJ26fI8d4a/bLpyWI4GdWIj1VSQaLQjhmBDzXhYu/cMg+DA0ArwZAp2OYaFT5aVrRZYEPFY/lB38RH1er18pS8ArFl/Ayv2J2dry1YIQYmfLDr7FWmosC17vXS7Dp/EM33MNz9WG833DM0/DNSojf49t6Ze11Vkn4iNA52S8A889wghMpnM6uK4IAySMAJnMVMe19zdtdorT3Pu0mIW8jHmNrMe10PiW2Aglgfiem/2O4SKK+CIoeN4xvsMg6AIdQgCE8+c0BjEl8WFe4kuZ8LubBAAZBRIfzX7kRYp8wsFEeILad4qeFQ/86e+9EiYqw0WtBkwDP0CvAfh+W17bpEOC8T8Jgaf9yQ3qC+m6k4oI2aP4lladiIZxx0MrlxH3aJE4X/JKuvgmaQ6CjG6gxd/zSkIRsWB+f3gmvYZkq3Qph3hbKKsnB939J/e98M+9//N+2/2b7fst917RgpX336fiG9mjND0Tf9vGm08VhqZ5iyLCDV0NpKB6yaAFOx51qZVl5KWpkf/b8FT0uTWC4fx0/s/wXOf9zH9Xo8CxQoJB0T9fnlPjVMBnX20sqovd2NnM0Wsq+GOC6lP1Qd9fAsFiP34QzBqQrk+2DqZ/hgU7pSBCG/yObMFALcz6Up/568rHxRWKep/YfTI76OPod4LjGnAKJQTilAKJ3u6SCd2CQfw/E1kaEFje3ufe9zTwu+YFVXjveDvkG2E/8ac/e20GZc703+aDpbG/Fwq+SgiWpQX87fDQk9eEDyYMSYF6lS7xQwpNLSgeCnu24kOn5HtbQLlX1Gq89v6WzcSIkuZx2q6NbPAbFuEgaCF3VV2BaA3MwRs0CA9f/QCOYacBuKX96wcBs1fans/Vl9Zs+yZNAc7q/H/ko8UPrKaUBO/z4TuJJyUkMulNl/YLpWB6tteo9eb2S8AV9SrxbS2M3oYaQ69QCEuz9pMkHJsthBcVbnvvET127K6vlm+mnYfb226MtoWleoZrOAkBNL0RDw1dbPLc4zV5t/24o212DMPwT/HsGfFFC/4TUgdL98MR2OIF8+9/ms/57IWy6bbTGMmtfzrNZstjOFue92ynw0r2c8tjmmv+25r/16/rOv+28nffqpXdGrqOD11If08gf8BJtkiGsAR/DSTmhD0TrJ4VH1j9W/aIzHKPRHz9RaXgXYMxfgpUrfuzuyQkFbbzt3ldFzEM5lz5zVh/2qWEpv6Hy2xwZ8Or01DMC4zn2FuBaG3Tjcbw6Kzcu7lEOPF45RqAPsMK0nLXu88gCW+OLL5g1raVIfwX/BANQNkSarULhNRaSIHTN8SjkNWDF6kPae68ylwkrVvWTFoQpSqkAYJlRuAv7NECwnXrX/8/APLEYI1Fxn6iAAAAAElFTkSuQmCC',
          width: 200,
          opacity: 0.3,
          absolutePosition: { x: 210, y: 370 },
        },
        {
          // image: this.image.changingThisBreaksApplicationSecurity,
          // width: 60,
          // opacity: 1,
          // absolutePosition: { x: 500, y: 20 },
          text:"",
          absolutePosition: { x: 500, y: 20 },
        },
      ],
      footer: function (currentPage, pageCount, pageSize) {
        return [
          { text: '', alignment: 'right', fontSize: 9, margin: [0, 0, 40, 0] }//currentPage+' of '+pageCount
        ]
      },
      defaultStyle: {
        fontSize: 10
      }

    }
    pdfMake.createPdf(doc).open();
  }

  openRegistrarCertificationComponent() {

    const dialogRef = this.dialog.open(RegistrarCertificationPurposeComponent, {
      width: '600px',
      data: { purpose: this.purpose, date: this.today, person: this.lname + ', ' +  this.fname + ' ' + this.mname},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.purpose = result.result
        this.registrarCetificationPdf()
      }
    });

  }

  registrarCetificationPdf() {
    // console.log("Pogi si Kurt")

    var header = [
      {
        image: this.logo,
        width: 60,
        opacity: 1,
        absolutePosition: { x: 155, y: 40 },
      },
      {
        text: 'University of Saint Louis', bold: true, fontSize: 15, alignment: 'left',
        absolutePosition: { x: 225, y: 46 }
      },
      {
        text: 'Mabini Street, Tuguegarao City Cagayan, Philippines\nTel. No. (078) 844-1872/73\nFax No. (078) 844-0889', italics: true, fontSize: 10, alignment: 'left',
        absolutePosition: { x: 225, y: 63 }
      },
      {
        canvas:
          [
            {
              type: 'line',
              x1: 43, y1: 117,
              x2: 570, y2: 117,
              lineWidth: 2,
            },
          ]
      },
      {
        text: 'OFFICE OF THE UNIVERSITY REGISTRAR \nCERTIFICATION', italics: false, fontSize: 12, alignment: 'center', bold: true,
        absolutePosition: { x: 0, y: 125 }
      }
    ]


    var tableContent = [
      [
        { text: 'SUBJECT ID', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
        { text: 'SUBJECT TITTLE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
        { text: 'UNITS', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], },
        { text: 'GRADE', italics: false, fontSize: 11, bold: true, border: [false, true, false, true], }
      ]
    ]

    for (var x in this.registrarCertificateData) {
      tableContent.push(
        [
          //@ts-ignore
          { text: this.registrarCertificateData[x].subjectID, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
          //@ts-ignore
          { text: this.registrarCertificateData[x].subjectTitle, bold: false, alignment: 'left', border: [false, false, false, false], fontSize: 11 },
          //@ts-ignore
          { text: this.registrarCertificateData[x].units, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 },
          //@ts-ignore
          { text: this.registrarCertificateData[x].grade, bold: false, alignment: 'center', border: [false, false, false, false], fontSize: 11 }
        ]
      )
    }

    tableContent.push(
      [
        //@ts-ignore
        { text: '', border: [false, true, false, false], colSpan: 4 }
      ],
      [
        //@ts-ignore
        { text: '', border: [false, false, false, false], colSpan: 4 }
      ],
      [
        //@ts-ignore
        { text: '', border: [false, false, false, false], colSpan: 4 }
      ],
      [
        {
          //@ts-ignore
          text: '\u200B           This certification is issued this ' + this.today + ' upon the request of Mr./Mrs. ' + this.lname + ', ' + this.fname + ' ' + this.mname + ' for ' + this.purpose, italics: false, fontSize: 11, colSpan: 4, border: [false, false, false, false],
        },
      ]
    )


    var content = [
      {
        text: 'TO WHOM MAY IT CONCERN', italics: false, fontSize: 11, alignment: 'left',
        absolutePosition: { x: 45, y: 165 }
      },
      {
        text: '\u200B           This is to certify that Mr./Mrs. ' + this.lname + ', ' + this.fname + ' ' + this.mname + ' took the following subject/s to wit: ', italics: false, fontSize: 11, alignment: 'left',
        absolutePosition: { x: 45, y: 185 }
      },
      {
        text: this.convertNumberToLetter(this.global.syDisplay(this.global.syear)), italics: true, fontSize: 11, alignment: 'left', bold: true,
        absolutePosition: { x: 45, y: 215 }
      },
      {
        table: {
          layout: {
            margin: [10, 10, 10, 10] // specify the margins as an array of [left, top, right, bottom]
          },
          widths: ['15%', '70%', '*', '*'],
          body: tableContent
        },
        absolutePosition: { x: 41, y: 230 },
      }
    ]

    var footer = [
      {
        text: 'Very truly yours,', italics: false, fontSize: 11, alignment: 'left',
        absolutePosition: { x: 430, y: 32 }
      },
      {
        text: 'University Seal', italics: false, fontSize: 11, alignment: 'left',
        absolutePosition: { x: 45, y: 77 }
      },
      {
        text: 'LIZA M. EMPEDRAD, Ph.D.', italics: false, fontSize: 11, alignment: 'left', bold: true,
        absolutePosition: { x: 430, y: 77 }
      },
      {
        text: 'University Registrar', italics: false, fontSize: 11, alignment: 'left',
        absolutePosition: { x: 443, y: 88 }
      },
    ]

    var dd = {
      pageSize: 'LETTER',
      pageOrientation: 'portrait', pageMargins: [40, 170, 40, 170],
      header: header,
      content: content,
      footer: footer
    }
    pdfMake.createPdf(dd).download('Registrar Certificate ' + this.lname + ', ' + this.fname + '.pdf');
  }

  convertNumberToLetter(str) {
    if (str.includes('1st'))
      return str.replace(/1st/g, 'First');
    return str.replace(/2nd/g, 'Second');
  }

  /////uploading of ID picture and signature - Michael John Rodriguez 8/23/23
  uploadIdSignature(){
    const dialogRef = this.dialog.open(UploadIdSignatureComponent, {
      width: '850px', disableClose: false, data: { id: this.checkId, pic:this.rawIdPic, sig:this.rawSign },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'success') {
        this.keyDownFunction('onoutfocus')
      }
    });
  }
  //////

}
