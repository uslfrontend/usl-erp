import { Component, OnInit, ViewChild, Input, ElementRef  } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageCroppedEvent } from 'ngx-image-cropper';
 // import 'cropperjs/dist/cropper.css';
@Component({
  selector: 'app-upload-id-signature',
  templateUrl: './upload-id-signature.component.html',
  styleUrls: ['./upload-id-signature.component.css']
})
export class UploadIdSignatureComponent implements OnInit {





    public constructor(private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService,public dialogRef: MatDialogRef<UploadIdSignatureComponent>,@Inject(MAT_DIALOG_DATA) public data: any,) {

    }

    imageChangedEvent: any = '';
    croppedImage: any = '';
    imageChangedEvent_sign: any = '';
    croppedImage_sign: any = '';

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }
    // imageLoaded(image: HTMLImageElement) {
    //     // show cropper
    // }
    imageLoaded() {
      // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

    fileChangeEvent_sign(event: any): void {
      this.imageChangedEvent_sign = event;
    }
    imageCropped_sign(event: ImageCroppedEvent) {
        this.croppedImage_sign = event.base64;
    }
    imageLoaded_sign() {
      // show cropper
    }
    cropperReady_sign() {
        // cropper ready
    }
    loadImageFailed_sign() {
        // show message _sign
    }


  public ngOnInit() {
    // console.log(this.data.pic)
    // console.log(this.data.sig)
  }


  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }
  save(): void {
    var pic = this.croppedImage.substring(23,this.croppedImage.length)
    var sig = this.croppedImage_sign.substring(23,this.croppedImage_sign.length)
    if(this.croppedImage == '')
      pic = this.data.pic;

    if(this.croppedImage_sign == '')
      sig = this.data.sig

    this.api.putStudentPicture(this.data.id  ,
      {
        "IdPicture": pic,
        "signature": sig
      })
      .map(response => response.json())
      .subscribe(res => {
       if (res.message == null) {
        this.dialogRef.close({result:'success'});
       }
      },Error=>{
        this.global.swalAlertError(Error);
      });
  }
}
