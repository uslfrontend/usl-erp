import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadIdSignatureComponent } from './upload-id-signature.component';

describe('UploadIdSignatureComponent', () => {
  let component: UploadIdSignatureComponent;
  let fixture: ComponentFixture<UploadIdSignatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadIdSignatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadIdSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
