import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import {MatDialog} from '@angular/material';
import {PurgeServicesService} from './purge-services.service';
import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';
const swal = Swal;


@Component({
  selector: 'app-purging',
  templateUrl: './purging.component.html',
  styleUrls: ['./purging.component.scss']
})
export class PurgingComponent implements OnInit {
	 purge=[]
   message=''
  constructor(private excelService:PurgeServicesService,public global: GlobalService,private api: ApiService,public dialog: MatDialog) { }

	getpurglist(){
	  this.purge=undefined;
    this.api.getEnrollmentPurgeList(this.global.syear,this.global.domain)
	          .map(response => response.json())
	          .subscribe(res => {
	            this.purge = res.data;
              this.message=res.message
	          },Error=>{ this.purge=[];this.global.swalAlertError(Error); });  
	}
  ngOnInit() {
  	this.getpurglist()
  }
  exportAsXLSX():void {
   this.excelService.exportAsExcelFile(this.purge, 'purge-list');
  }
purgethis(){
    this.swalConfirm("You are about to purge "+this.purge.length+" student's enrollment record.","You won't be able to revert this.",'warning','Purge',""+this.purge.length+" student's enrollment record has been purged.",'purge');
}
 swalConfirm(title,text,type,button,successm,remove){
    swal.fire({
        title: title,
        html: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          if (remove=='purge') {
          	this.global.swalLoading('')
          	if (this.global.domain=="COLLEGE") {
             this.api.deleteEnrollmentPurge(this.global.syear)
                .map(response => response.json())
                .subscribe(res => {
                	this.global.swalSuccess(res.message)
                	this.getpurglist()
                },Error=>{
                  console.log(Error)
                  this.global.swalAlertError(Error)
                });
          	}
          	if (this.global.domain=="GRADUATE SCHOOL") {
              this.api.deleteEnrollmentPurgeGS(this.global.syear)
                .map(response => response.json())
                .subscribe(res => {
                  this.global.swalSuccess(res.message)
                	this.getpurglist()
                },Error=>{
                  this.global.swalAlertError(Error)
                });
          	}
          }
        }
      })
  }

}
