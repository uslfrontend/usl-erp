import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import Swal from 'sweetalert2';
const swal = Swal;
import * as XLSX from 'xlsx';
import { AcademicService } from '../academic.service';
type AOA = any[][];
import { ChangeDetectorRef } from '@angular/core';


interface ExcelData {
  idNumber: number;
  section: string;
}

@Component({
  selector: 'app-section-manager',
  templateUrl: './section-manager.component.html',
  styleUrls: ['./section-manager.component.scss']
})

export class SectionManagerComponent implements OnInit {
  tableArr = null
  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;
  datadisplay = undefined
  savetemp = false
  firstdata = []
  error = false

  constructor(public global: GlobalService, private api: ApiService, public excel: AcademicService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.loadexam()
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    this.firstdata = []
    this.error = false
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      this.global.swalClose()
      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = []
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.global.swalClose()
      this.uploadthis.nativeElement.value = ''
      if (this.data[0] != undefined) {
        this.firstdata = []
        for (var i2 = 4; i2 < this.data.length; ++i2) {
          if (this.data[i2][0] != '' && this.data[i2][0] != undefined) {
            if (this.data[i2][1] == '' || this.data[i2][1] == undefined) {
              this.data[i2][1] = 'NO INPUT'
              this.error = true
            }
            this.firstdata.push([this.data[i2][0], '', '', this.data[i2][1], ''])
          }
        }
      } else {
        this.firstdata = []
        this.global.swalAlert('Invalid Excel File!', '', 'warning')
      }
      this.loadStudentDetails()
    };
    reader.readAsBinaryString(target.files[0]);
  }

  async loadStudentDetails() {
    for (var x in this.firstdata) {
      const res = (await this.api.getStudentEnrollmentHistory(this.firstdata[x][0]).toPromise()).json();
      try {
        res.data[x].fullName
        for (var y in res.data) {
          if (this.global.syear.toString().includes(res.data[y].schoolYear)) {
            this.firstdata[x][4] = res.data[y].status
            break
          }
          this.firstdata[x][2] = res.data[y].yearOrGradeLevel
          this.firstdata[x][1] = res.data[y].fullName
        }
      } catch {
        this.error = true
        this.firstdata[x][1] = 'ID NUMBER NOT FOUND'
        this.firstdata[x][2] = ''
        this.firstdata[x][4] = ''
      }
    }
    this.cdr.detectChanges();
  }

  loadexam() {
    this.firstdata = []
  }

  funcSave() {
    var syear = ''
    if (this.global.syear.length == 7) {
      syear = this.global.syear.slice(0, -1)
    }
    this.swalConfirm("Upload Confirmation!", 'You are about to upload section of Students for <br><b>' + this.global.syDisplay(syear) + '</b>', 'info', 'Yes')
  }

  swalConfirm(title, text, type, button) {
    swal.fire({
      title: title,
      html: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {
        this.global.swalLoading('Uploading ' + this.firstdata.length + ' Records...')
        this.insertsection(0)
      }
    })
  }

  insertsection(length) {
    if (length < this.firstdata.length) {
      var syear = ''
      if (this.global.syear.length == 7) {
        syear = this.global.syear.slice(0, -1)
      }

      if (this.firstdata[length][3] == '' || this.firstdata[length][3] == undefined) {
        this.firstdata[length][3] = '(unassigned)'
      }

      if (this.firstdata[length][0] != '' && this.firstdata[length][0] != undefined) {
        this.api.putStudentSection(this.firstdata[length][0], {
          "SchoolYear": syear,
          "Section": this.firstdata[length][3]
        })
          .map(response => response.json())
          .subscribe(res => {
            this.insertsection(length + 1)
          }, Error => {
            this.global.swalAlertError(Error);
          });
      } else {
        this.insertsection(length + 1)
      }

    } else {
      this.global.swalClose()
      this.global.swalAlert("Sections uploaded!", '', 'success')
    }
  }

  downloadExcelFile() {
    this.excel.sectionManagerExcel()

  }

  funcDelete() {
    this.firstdata = []
    this.data = []
  }
}
