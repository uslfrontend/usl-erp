import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import {ExcelService} from './../../academic/curriculum/excel.service';
import {Sort} from '@angular/material/sort';
import {MatDialog} from '@angular/material';
import Swal from 'sweetalert2';

const swal = Swal;
@Component({
  selector: 'app-withdraw-conditionally-enrolled',
  templateUrl: './withdraw-conditionally-enrolled.component.html',
  styleUrls: ['./withdraw-conditionally-enrolled.component.css']
})
export class WithdrawConditionallyEnrolledComponent implements OnInit {
  config: any;
	transType='1';
	temp=[]
	tableArr=[]
  constructor(private excelService:ExcelService,public global: GlobalService,private api: ApiService,public dialog: MatDialog) { }

  ngOnInit() {
    this.config = {
      itemsPerPage: 20,
      currentPage: 1,
      totalItems: 0
    };
    this.transType='1'
    this.loaddata()
    this.updateProgress=0
  }
  
  pageChanged(event) {
    this.config.currentPage = event;
  }

  coursearr=[]
  levelList=[]
  coursevar=''
  selectedLevel=''
  updated=0
  notUpdated=0
  ctr=0
  updatingID=''
  updateProgress=0
  showUpdateProgress=false
  updating=false

  loaddata(){
  	this.tableArr=undefined
    var sy=this.global.syear, progLevel, level;
    var progLevel=this.global.domain
    if (this.global.domain=='HIGHSCHOOL') {
      this.setEmployeeBasicEdDomain()
      sy = this.global.syear.substring(0, 6)
      progLevel='High School'
    }
    this.api.getEnrollmentListToBeWithdrawn(sy,progLevel,this.transType)
      .map(response => response.json())
      .subscribe(res => {
        if(res.data!=null){
          for (var i = 0; i < res.data.length; ++i) {
            if (res.data[i].level==''||res.data[i].level==null)
              res.data[i].level="Can't Determine Level"
            if (res.data[i].course==''||res.data[i].course==null)
              res.data[i].course="No Preffered Course"
        }
          this.temp=res.data
          this.tableArr = res.data
          this.loadwithfilter();
        }
      },Error=>{
        this.global.swalAlertError(Error);
      });
  }

  sortData(sort: Sort) {
    const data = this.temp.slice();
    if (!sort.active || sort.direction === '') {
      this.temp = data;
      return;
    }

    this.temp = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'idno': return compare(a.idNumber, b.idNumber, isAsc);
        case 'name': return compare(a.name,b.name, isAsc);
        case 'course': return compare(a.courseCode,b.courseCode, isAsc);
        case 'level': return compare(a.yearOrGradeLevel,b.yearOrGradeLevel, isAsc);
        case 'reportCard': return compare(a.reportCard, b.reportCard, isAsc);
        case 'birthCert': return compare(a.birthCert, b.birthCert, isAsc);
        default: return 0;
      }
    });
      function compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
  }

  updateStatus(item){
    var sy=this.global.syear
    var withSubject=false
    if (this.global.domain=='HIGHSCHOOL') {
      sy = this.global.syear.substring(0, 6)
      withSubject=true
    }
    else{
      if(item.subjectsEnrolled_Count>0){
        withSubject=true
      }
    }
    if(this.transType=='1'){
      this.api.updateStatusPaidToAdmitted(item.idNumber,sy)
      .map(response => response.json())
      .subscribe(res => {
          this.global.swalSuccess(res.message)
          this.loaddata()
      },Error=>{
        this.global.swalAlertError(Error)
      });
    }
    else{
      if(item.reportCard==1 && item.birthCert==1){
        if(withSubject){
          this.api.updateStatusAdmittedToPaid(item.idNumber,sy)
          .map(response => response.json())
          .subscribe(res => {
              this.global.swalSuccess(res.message)
              this.loaddata()
          },Error=>{
            this.global.swalAlertError(Error)
          });
        }
        else{
          this.global.swalAlert("Subject required!","Can't update because no subject has found. Please return first the subject/s enrolled.",'warning')
        }
      }
      else{
          this.global.swalAlert("Field required!","Can't Update because Report Card or Birth Certificate is not yet submitted.",'warning')
        }
    }
  }

  updateAll(start, data, updated, notupdated){
    this.updating=true
    var end = this.temp.length
    this.showUpdateProgress=true
    if (start >= end) {
      this.global.swalAlert("Updates Infomation","Number of Students Updated ("+updated.toString()+") and not updated ("+notupdated.toString()+").",'info') 
      this.loaddata();
      this.updating=false
      this.showUpdateProgress=false
      this.updateProgress=0
      return
    }
    var sy=this.global.syear
    var withSubject=false

    if (this.global.domain=='HIGHSCHOOL') {
      sy = this.global.syear.substring(0, 6)
      withSubject=true
    }
    else{
      if(data[start].subjectsEnrolled_Count>0){
        withSubject=true
      }
    }
    if(this.transType=='1'){
      this.api.updateStatusPaidToAdmitted(data[start].idNumber,sy)
      .map(response => response.json())
      .subscribe(res => {
        this.updateAll(++start,data,++updated,notupdated)
        this.updatingID=data[start].idNumber
        this.updateProgress=Math.round((start/end)*100)
      },Error=>{
        this.global.swalAlertError(Error)
      });
    }
    else{
      if(data[start].reportCard==1 && data[start].birthCert==1 && withSubject==true){
        this.api.updateStatusAdmittedToPaid(data[start].idNumber,sy)
        .map(response => response.json())
        .subscribe(res => {
          this.updateAll(++start,data,++updated,notupdated)
          this.updatingID=data[start].idNumber
          this.updateProgress=Math.round((start/end)*100)
        },Error=>{
          this.global.swalAlertError(Error)
        });
      }
      else{
        this.updateAll(++start,data,updated,++notupdated)
        this.updateProgress=Math.round((start/end)*100)
        }
    }
  }

  getBoolean(value){
    switch(value){
         case true:
         case "true":
         case 1:
         case "1":
         case "on":
         case "yes":
             return true;
         default: 
             return false;
     }
 }

	filter=''


  exportAsXLSX():void {
    var x=[]
    for (var i = 0; i < this.temp.length; ++i) {
      x.push({
        IDNumber:this.temp[i].idNumber,
        FirstName:this.temp[i].name,
        Course: this.temp[i].courseCode,
        Level:this.temp[i].yearOrGradeLevel
      })
    }
   this.excelService.exportAsExcelFile(x, 'ConditionallyEnrolledStudents');
  }

	loadwithfilter(){
	    this.temp=[]
      var filterYear;
      if(this.global.domain=='COLLEGE'){
        filterYear=1;
      }
      else{
        if(this.selectedLevel=='Junior High School'){
          filterYear=7;
        }
        else if(this.selectedLevel=='Senior High School'){
          filterYear=11;
        }
      }
		for (var i = 0; i < this.tableArr.length; ++i) {
      if (this.tableArr[i].level==null)
        this.tableArr[i].level=''
      if (this.tableArr[i].course==null)
        this.tableArr[i].course=''

		  if ((this.tableArr[i].idNumber.includes(this.filter)||
		  	  this.tableArr[i].name.toLowerCase().includes(this.filter.toLowerCase())||
		  	  this.tableArr[i].courseCode.toLowerCase().includes(this.filter.toLowerCase()))&&
          (this.tableArr[i].yearOrGradeLevel==filterYear)
		  	) {

		  	 this.temp.push(this.tableArr[i])
		  }
		}
	}

  setEmployeeBasicEdDomain(){
    this.levelList=[];
    if(this.global.checkdomain('0010')&&this.global.checkdomain('0009')){
      this.levelList = new Array('Junior High School', 'Senior High School')
      this.selectedLevel='Junior High School'
    }
    else if(this.global.checkdomain('0010')){
      this.levelList = new Array('Senior High School')
      this.selectedLevel='Senior High School'
    }
    else {
      this.levelList = new Array('Junior High School')
      this.selectedLevel='Junior High School'
    }
  }
}
