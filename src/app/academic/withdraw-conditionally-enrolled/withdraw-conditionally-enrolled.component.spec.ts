import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawConditionallyEnrolledComponent } from './withdraw-conditionally-enrolled.component';

describe('WithdrawConditionallyEnrolledComponent', () => {
  let component: WithdrawConditionallyEnrolledComponent;
  let fixture: ComponentFixture<WithdrawConditionallyEnrolledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithdrawConditionallyEnrolledComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawConditionallyEnrolledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
