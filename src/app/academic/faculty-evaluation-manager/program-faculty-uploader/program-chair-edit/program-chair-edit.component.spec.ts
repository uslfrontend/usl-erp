import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramChairEditComponent } from './program-chair-edit.component';

describe('ProgramChairEditComponent', () => {
  let component: ProgramChairEditComponent;
  let fixture: ComponentFixture<ProgramChairEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramChairEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramChairEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
