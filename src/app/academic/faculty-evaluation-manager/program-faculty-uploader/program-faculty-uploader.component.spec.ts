import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramFacultyUploaderComponent } from './program-faculty-uploader.component';

describe('ProgramFacultyUploaderComponent', () => {
  let component: ProgramFacultyUploaderComponent;
  let fixture: ComponentFixture<ProgramFacultyUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramFacultyUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramFacultyUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
