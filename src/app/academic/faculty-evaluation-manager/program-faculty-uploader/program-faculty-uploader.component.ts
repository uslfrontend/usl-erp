import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import Swal from 'sweetalert2';
const swal = Swal;
import * as XLSX from 'xlsx';
import { AcademicService } from '../../academic.service';
import { resolve } from 'url';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddIndividualFacultyComponent } from './add-individual-faculty/add-individual-faculty.component';
import { ProgramChairEditComponent } from './program-chair-edit/program-chair-edit.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { from } from 'rxjs';
import { ConstantPool } from '@angular/compiler';


type AOA = any[][];

@Component({
  selector: 'app-program-faculty-uploader',
  templateUrl: './program-faculty-uploader.component.html',
  styleUrls: ['./program-faculty-uploader.component.scss']
})

//Workflow for getting data from API:
//1. get courses/program data - this.getProgramData()
//2. get dean's department - this.getDeanDept()
//3. get final program based on dean's department - generateFinalPrograms()
//4. generate()
//5. getfaculty data list - getFacultyList()
//6. get program title and code - for loop on generate()

//Workflow for getting data from Excel:
//1. onFileChange()
//2. search the program ID, Title, and Code ofthe uploaded data - searchProgIDbyCode()
//3. get each faculty name by faculty ID by looping it to facultyDataList- getFacultyNameById()


export class ProgramFacultyUploaderComponent implements OnInit {
  //viewing variables
  programID = ''
  id = ''
  facultyID = ''
  programTitle = ''
  programCode = ''
  programChairID = ''
  programChairName = ''
  facultyDataList = []
  programData = []
  deanDept = []
  finalDeptPrograms = undefined
  fromUpload = false
  facultyNames = []
  fullName = ''
  employeeArray = []
  finalIds = []
  options: FormGroup;
  addIndivID = ''
  programLevel = this.global.domain

  //upload variables
  data: AOA = [];
  firstData = undefined
  errorData = false

  duplicate = []
  correctData = []

  delayInMilliseconds = 1500; //1.5 seconds
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;


  constructor(public global: GlobalService, private api: ApiService, private excelService: AcademicService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getProgramData()
    // this.getDeanDept()
    // this.generateFinalPrograms()
  }

  generate() {
    // this.getFacultyNameById()
    this.fromUpload = false
    this.getFacultyList(this.programID, '', '')
    //get title and code
    for (var x = 0; x < this.finalDeptPrograms.length; x++) {
      if (this.programID == this.finalDeptPrograms[x].programId) {
        this.programTitle = this.finalDeptPrograms[x].course
        this.programCode = this.finalDeptPrograms[x].courseCode
        this.programChairID = this.finalDeptPrograms[x].programChair
        // console.log("Printlog ID & Code", this.finalDeptPrograms[x])
      }
    }
    this.getProgramChairName(this.programChairID)
    // console.log("Printlog Progchar ID", this.programChairID[x])
  }

  generateFinalPrograms() {
    this.finalDeptPrograms = []
    for (var x = 0; x < this.deanDept.length; x++) {
      for (var y = 0; y < this.programData.length; y++) {
        if (this.deanDept[x].departmentId == this.programData[y].departmentId) {
          this.finalDeptPrograms.push(this.programData[y])
        }
      }
    }
    // console.log("Printlog Final Programs", this.finalDeptPrograms)
  }

  getProgramData() {
    this.api.getOnlineRegistrationCoursesWithStrand().map(response => response.json())
      .subscribe(res => {
        this.programData = res.data
        // console.log("Printlog Program Data: ", this.programData)
        this.getDeanDept()
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getDeanDept() {
    this.api.getAuthUserViewDomains().map(response => response.json())
      .subscribe(res => {
        this.deanDept = res.data
        // console.log("Printlog dean dept details: ", this.deanDept)
        this.generateFinalPrograms()
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getFacultyList(programID, id, facultyID) {
    this.api.getFacultyListbyProgramID(programID, id, facultyID).map(response => response.json())
      .subscribe(res => {
        this.facultyDataList = res.data
        // console.log("Printlog name response: ", this.facultyDataList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  downloadExcel(): void {
    var arr = [];
    this.excelService.downloadFacultyEvalProgramExcelFormat(this.programCode, this.programTitle, this.facultyDataList)
  }

  getProgramChairName(id) {
    if (id != null) {
      this.api.getFullNameByID(id).map(response => response.json())
        .subscribe(res => {
          this.programChairName = res.data.fullName
        }, Error => {
          this.programChairName = ''
        })
    } else {
      this.programChairName = '-unassigned-'
    }
  }

  camelCase(data) {
    if (typeof data === 'string' || data instanceof String) {
      data = data.toString().toLowerCase()
      data = data.charAt(0).toUpperCase() + data.slice(1);
      return data
    } else {
      return data
    }

  }
  onFileChange(evt: any) {
    this.global.swalLoading('Uploading Excel Data...');
    /* wire up file reader */
    this.firstData = undefined
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = []
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.uploadthis.nativeElement.value = ''
      //check if the uploaded file's program ID matches the selected Program on the UI
      var extractProg = this.data[4][0].slice(6)
      // console.log("Printlog extracted: ", extractProg)
      try {
        if (extractProg != this.programCode) {
          this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
          return
        }
      } catch (err) {
        this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
        return
      }
      //reset varriables and arrays
      this.errorData = false
      this.facultyNames = []
      this.employeeArray = []
      if (this.data[0] != undefined) {
        this.facultyDataList = []
        this.data.shift();
        // this.facultyDataList = this.data
        for (var i2 = 6; i2 < this.data.length; ++i2) {
          if (this.data[i2][0] != '' && this.data[i2][0] != undefined && this.data[i2][0] != null) {
            // this.data[i2][0].toString()
            this.facultyDataList.push(this.data[i2][0])
          }
        }
        // console.log("printlog: data", this.facultyDataList)
        this.fromUpload = true
        // console.log("Printlog upload: ", this.fromUpload)
        // var duplicate = []
        // var correctData = []        
        this.duplicate = []
        this.correctData = []
        let counter = 1
        this.employeeArray = []
        this.finalIds = []
        this.facultyDataList.forEach((value) => {
          if (this.duplicate.includes(value)) {
            // console.log("Printlog: Value", value)
            this.errorData = true
            this.employeeArray.push({ id: value, name: 'Duplicated Value' })
            this.finalIds.push(value)
            counter++
          } else {
            this.api.getFullNameByID(value).map(response => response.json())
              .subscribe(res => {
                if (res.data != null) {
                  // this.errorData = false
                  this.fullName = res.data.fullName
                  this.correctData.push({ id: value, name: this.fullName })
                  // this.employeeArray = this.correctData.length;
                } else {
                  this.errorData = true
                  this.employeeArray.push({ id: value, name: res.message })
                  this.finalIds.push(value)
                }
                // console.log('counter: ', counter)
                // console.log(this.facultyDataList.length)
                if (counter == this.facultyDataList.length) {
                  // console.log(counter)
                  for (var x = 0; x < this.correctData.length; x++) {
                    this.employeeArray.push({ id: this.correctData[x].id, name: this.correctData[x].name })
                    this.finalIds.push(this.correctData[x].id)
                  }
                  // console.log(this.employeeArray)
                }
                counter++
              })
          }
          this.duplicate.push(value)
        })
      } else {
        this.facultyDataList = []
        this.global.swalAlert('Invalid Excel File!', '', 'warning')
      }
      this.global.swalClose();
    };
    reader.readAsBinaryString(target.files[0]);
  }

  deleteUploadedError(id) {
    for (var x = 0; x < this.employeeArray.length; x++) {
      if (this.employeeArray[x].id == id) {
        // console.log("Printlog index: ", x + ": " + id)
        this.employeeArray.splice(x, 1); // 2nd parameter means remove one item only
        this.finalIds.splice(x, 1); // 2nd parameter means remove one item only
        break
      }
    }
    for (var x = 0; x < this.employeeArray.length; x++) {
    // console.log("Printlog data:", this.employeeArray[x].name)
      if(this.employeeArray[x].name=='ID Not Found' || this.employeeArray[x].name=='Duplicated Value'){
        this.errorData=true
        break
      }else{
        this.errorData=false
      }
    }
  }

  uploadFacultyList() {
    this.global.swalLoading('Uploading/Replacing reords...');
    this.api.deleteFacultyList(this.programCode).map(response => response.json())
      .subscribe(res => {
        this.finalIds.forEach((value) => {
          this.api.postFacultyList(this.programID, value).map(response => response.json())
            .subscribe(res => {
              if (this.finalIds[this.finalIds.length - 1] == value) {
                this.finalIds = []
                this.ngOnInit()
                this.generate()
                this.global.swalClose();
                this.global.swalAlert("Program updated", '', 'success')
              }
            }, Error => {
              this.global.swalAlertError()
            })
        })
      }, Error => {
        this.global.swalAlertError()
      })
  }

  deleteIndividual(id) {
    this.api.deleteFacultyIndividual(id).map(response => response.json())
      .subscribe(res => {
        // console.log("Printlog deleted: ", id)
        this.global.swalSuccess('Record Deleted')
        this.generate()
      })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddIndividualFacultyComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.addIndivID = result.result;
        this.keyDownFunction('onoutfocus')
        // console.log("Printlog Lookup ID: ", result.result)
        this.uploadFacultyIndividual(this.addIndivID)
      }
    });
  }

  openDialogProgChairEdit(): void {
    // console.log("Printlog function works: ")
    const dialogRef = this.dialog.open(ProgramChairEditComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.programChairID = result.result;
        this.keyDownFunction('onoutfocus')
        // console.log("Printlog Lookup ID: ", result.result)
        this.updateProgramChair(this.programChairID)
      }
    });
  }

  keyDownFunction(event) {
    // console.log("Printlog: It works!", this.addIndivID)
    if (event.key === "Enter") {
      this.uploadFacultyIndividual(this.addIndivID)
    }
  }

  uploadFacultyIndividual(id) {
    // console.log("Printlog ID: ", id)
    // console.log("Printlog List: ", this.facultyDataList[0].facultyID)
    // console.log("Printlog List: ", this.facultyDataList)
    if (this.facultyDataList != null) {
      // console.log("Printlog notNull")
      for (var x = 0; x < this.facultyDataList.length; x++) {
        if (this.facultyDataList[x].facultyID.includes(id)) {
          this.global.swalAlert("Instructor is already in the list", '', 'warning')
          return
        }
      }
    }

    this.api.getFullNameByID(id).map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          this.api.postFacultyList(this.programID, id).map(response => response.json())
            .subscribe(res => {
              this.global.swalSuccess('Success')
              this.generate()
            }, Error => {
              this.global.swalAlert("Invalid ID", '', 'warning')
            })
        } else {
          this.global.swalAlert("Invalid ID", '', 'warning')
        }
      })
  }

  updateProgramChair(id) {
    // console.log("Printlog program ID and ID: ", this.programID, id)
    swal.fire({
      title: "Are you sure you want change the Program Chair?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        // this.savefunc()
        this.api.putProgramChairUpdate(this.programID, id).map(response => response.json())
          .subscribe(res => {
            this.global.swalSuccess('Success')
            this.getProgramChairName(id)
            this.ngOnInit()
          }, Error => {
            this.global.swalAlert("Error", '', 'warning')
          })
      }

    })
  }

  confirmDelete(id) {
    swal.fire({
      title: "Are you sure you want to remove this instructor?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Remove'
    }).then((result) => {
      if (result.value) {
        // this.savefunc()
        this.deleteIndividual(id)
      }
    })
  }


}



