import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIndividualFacultyComponent } from './add-individual-faculty.component';

describe('AddIndividualFacultyComponent', () => {
  let component: AddIndividualFacultyComponent;
  let fixture: ComponentFixture<AddIndividualFacultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIndividualFacultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIndividualFacultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
