import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../../../global.service';
import { ApiService } from './../../../../../api.service';
import { AddAreaHeadComponent } from './../add-area-head/add-area-head.component';
import Swal from 'sweetalert2';
import { domain } from 'process';
const swal = Swal;

// import { AreaUploaderComponent } from './../area-uploader.component'


// import { ProgramFacultyUploaderComponent } from './../../program-faculty-uploader.component'

@Component({
  selector: 'app-edit-area',
  templateUrl: './edit-area.component.html',
  styleUrls: ['./edit-area.component.scss']
})
export class EditAreaComponent implements OnInit {
  areaName = this.data.selectedData[0]
  areaHeadID = this.data.selectedData[1]
  areaDeptID = this.data.selectedData[2]
  areaDeptName = ''
  areaDesciption = this.data.selectedData[3]
  areaID = this.data.selectedData[4]
  deptIDList = ''
  syear = this.global.syear.slice(0, -1)

  constructor(public dialogRef: MatDialogRef<EditAreaComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService,) { }

  ngOnInit() {

    this.getDeanDept()
    // console.log("Printlog: ", this.areaID, this.areaDeptID, this.global.domain, this.syear)
  }

  generate() {

  }

  getDeanDept() {
    this.api.getAuthUserViewDomains().map(response => response.json())
      .subscribe(res => {
        if (res.data.length == 1) {
          this.areaDeptID = res.data[0].departmentId
          // console.log("Printlog areaHeadID: ", this.areaDeptID)
          return
        }
        this.deptIDList = res.data
        for (var x = 0; x < res.data.length; x++) {
          if (this.areaDeptID == res.data[x].departmentId) {
            this.areaDeptName = res.data[x].departmentName
          }
        }
        // console.log("Printlog deptName: ", this.areaDeptName)
        // console.log("Printlog deptIDList: ", this.deptIDList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  search(): void {
    const dialogRef = this.dialog.open(AddAreaHeadComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.areaHeadID = result.result
      } else {

      }
    });
  }

  save() {
    this.api.getFullNameByID(this.areaHeadID).map(response => response.json()).subscribe(res => {
      if (res.data == null) {
        this.global.swalAlert("ID not found", 'You can use the employee lookup button to search for employee name', 'warning')
        return
      }
      var data = {
        "name": this.areaName,
        "description": this.areaDesciption,
        "headID": this.areaHeadID,
        "deptID": this.areaDeptID
      }
      this.api.updateArea(this.areaID, this.syear, this.global.domain, data).map(response => response.json())
        .subscribe(res => {
          // console.log("Printlog update message: ", res.message)
          if (res.message.includes("You cannot update")) {
            this.global.swalAlert("Update failed", res.message, 'warning')
            return
          }
          if (res.message.includes("Duplicate area name")) {
            this.global.swalAlert("Duplicate found", 'An area with the name "' + this.areaName + '" already exists', 'warning')
            return
          }
          this.global.swalSuccess('Area Updated')
          // console.log("Printlog added area details: ", this.areaName, this.areaDesciption, this.areaHeadID, this.areaDeptID)
          this.dialogRef.close({ result: this.areaName });
        }, Error => {
          this.global.swalAlertError()
        })
    })
  }

  // deleteArea(){
  //   swal.fire({
  //     title: "Are you sure you want to delete this area?",
  //     text: "",
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Remove'
  //   }).then((result) => {
  //     if (result.value) {
  //       this.api.deleteArea(this.areaID).map(response => response.json()).subscribe(res => {
  //         this.global.swalSuccess(this.areaName + ' was deleted')
  //         this.dialogRef.close({ result: 'delete'})
  //       }, Error => {
  //         this.global.swalAlertError()
  //       })
  //     }
  //   })
  // }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

}
