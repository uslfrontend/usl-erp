import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIndividualAreaFacultyComponent } from './add-individual-area-faculty.component';

describe('AddIndividualAreaFacultyComponent', () => {
  let component: AddIndividualAreaFacultyComponent;
  let fixture: ComponentFixture<AddIndividualAreaFacultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIndividualAreaFacultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIndividualAreaFacultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
