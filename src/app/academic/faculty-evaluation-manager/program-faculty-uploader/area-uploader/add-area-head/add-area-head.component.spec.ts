import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAreaHeadComponent } from './add-area-head.component';

describe('AddAreaHeadComponent', () => {
  let component: AddAreaHeadComponent;
  let fixture: ComponentFixture<AddAreaHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAreaHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAreaHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
