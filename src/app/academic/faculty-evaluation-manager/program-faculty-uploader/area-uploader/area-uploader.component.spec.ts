import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaUploaderComponent } from './area-uploader.component';

describe('AreaUploaderComponent', () => {
  let component: AreaUploaderComponent;
  let fixture: ComponentFixture<AreaUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
