import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../../../global.service';
import { ApiService } from './../../../../../api.service';
import { AddAreaHeadComponent } from './../add-area-head/add-area-head.component';


// import { ProgramFacultyUploaderComponent } from './../../program-faculty-uploader.component'

@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.scss']
})
export class AddAreaComponent implements OnInit {
  areaName = ''
  areaDesciption = ''
  areaHeadID = ''
  deptIDList = []
  areaDeptID = ''

  constructor(public dialogRef: MatDialogRef<AddAreaComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService) { }

  ngOnInit() {
    this.getDeanDept()
  }

  generate() {

  }

  getDeanDept() {
    this.api.getAuthUserViewDomains().map(response => response.json())
      .subscribe(res => {
        if (res.data.length == 1) {
          this.areaDeptID = res.data[0].departmentId
          // console.log("Printlog areaHeadID: ", this.areaDeptID)
          return
        }
        this.deptIDList = res.data
        // console.log("Printlog deptIDList: ", this.deptIDList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  search(): void {
    const dialogRef = this.dialog.open(AddAreaHeadComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.areaHeadID = result.result
      } else {

      }
    });
  }

  save() {
    if (this.areaName == '' || this.areaDesciption == '' || this.areaHeadID == '' || this.areaDeptID == '') {
      this.global.swalAlert("Please fill up all fields first", '', 'warning')
      return
    }
    this.api.getFullNameByID(this.areaHeadID).map(response => response.json()).subscribe(res => {
      if(res.data==null){
        this.global.swalAlert("ID not found", 'You can use the employee lookup button to search for employee name', 'warning')
        return
      }
      var data = {
        "name": this.areaName,
        "description": this.areaDesciption,
        "headID": this.areaHeadID,
        "deptID": this.areaDeptID
      }
      this.api.postArea(data).map(response => response.json())
        .subscribe(res => {
          // console.log("Printlog message: ", res.message)
          if (res.message.includes("Duplicate Area Name")) {
            this.global.swalAlert("Duplicate found", 'An area with the name "' + this.areaName + '" already exists', 'warning')
            return
          }
          this.global.swalSuccess('Area Added')
          // console.log("Printlog added area details: ", this.areaName, this.areaDesciption, this.areaHeadID, this.areaDeptID)
          this.dialogRef.close({ result: this.areaName });
        }, Error => {
          this.global.swalAlertError()
        })
    })
  }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

}
