import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import Swal from 'sweetalert2';
const swal = Swal;
import * as XLSX from 'xlsx';
import { AcademicService } from '../../../academic.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddAreaComponent } from './add-area/add-area.component';
import { EditAreaComponent } from './edit-area/edit-area.component';
import { AddIndividualAreaFacultyComponent } from './add-individual-area-faculty/add-individual-area-faculty.component';

type AOA = any[][];

@Component({
  selector: 'app-area-uploader',
  templateUrl: './area-uploader.component.html',
  styleUrls: ['./area-uploader.component.scss']
})

export class AreaUploaderComponent implements OnInit {
  //area
  areaName = ''
  areaID = ''
  areaDeptCode = ''
  areaDeptName = ''
  areaDeptID = ''
  areaHeadName = ''
  areaHeadID = ''
  areaDescription = ''
  deptIDList = []
  deptCodeList = []
  deptList = []
  areaList = []

  //facultylist inside the area
  areaFacultyList = []
  facultyNames = []
  employeeArray = []
  fromUpload = false
  fullName = ''
  finalIds = []
  duplicate = []
  correctData = []


  constructor(public global: GlobalService, private api: ApiService, private excelService: AcademicService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getDeanDept()
  }

  generateAreaDetails() {
    for (var x = 0; x < this.areaList.length; x++) {
      if (this.areaID == this.areaList[x].id) {
        this.areaName = this.areaList[x].areaName
        // this.areaID = this.areaList[x].id
        this.areaDeptCode = this.areaList[x].deptCode
        this.areaDeptID = this.areaList[x].deptID
        this.areaDeptName = this.getAreaDeptName(this.areaDeptID)
        this.areaHeadID = this.areaList[x].headID
        this.areaHeadName = this.areaList[x].headName
        this.areaDescription = this.areaList[x].description
      }
    }
    // console.log("Printlog: ", this.areaID)
    this.generateAreaFacultyList()
  }

  generateAreaFacultyList() {
    this.fromUpload = false
    this.api.getAreaFacultyList('', '', '', '', this.areaID).map(response => response.json())
      .subscribe(res => {
        this.areaFacultyList = res.data
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getDeanDept() {
    this.api.getAuthUserViewDomains().map(response => response.json())
      .subscribe(res => {
        this.deptList = res.data
        // console.log("Printlog deptNameList: ", this.deptList)
        this.deptIDList = []
        for (var x = 0; x < res.data.length; x++) {
          this.deptIDList.push(res.data[x].departmentId)
          this.deptCodeList.push(res.data[x].departmentCode)
        }
        // console.log("Printlog deptIDList: ", this.deptIDList)
        // console.log("Printlog deptNameList: ", this.deptNameList)
        this.getArea()
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getArea() {
    this.areaList = []
    this.api.getArea('', '', '', '').map(response => response.json())
      .subscribe(res => {
        // console.log("Printlog areaList: ", res.data)
        for (var x = 0; x < res.data.length; x++) {
          for (var y = 0; y < this.deptIDList.length; y++) {
            if (res.data[x].deptID == this.deptIDList[y]) {
              res.data[x].areaName = res.data[x].areaName + " [" +this.deptCodeList[y]+"]"
              // console.log("Printlog: ", this.deptIDList[y])
              // console.log("Printlog: ", this.deptCodeList)
              this.areaList.push(res.data[x])
            }
          }
        }
        // console.log("Printlog areaList: ", this.areaList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getAreaChange(action) {
    this.areaList = []
    this.api.getArea('', '', '', '',).map(response => response.json())
      .subscribe(res => {
        // console.log("Printlog areaList: ", res.data)
        for (var x = 0; x < res.data.length; x++) {
          for (var y = 0; y < this.deptIDList.length; y++) {
            if (res.data[x].deptID == this.deptIDList[y]) {
              res.data[x].areaName = res.data[x].areaName + " [" +this.deptCodeList[y]+"]"
              // console.log("Printlog: ", this.deptIDList[y])
              // console.log("Printlog: ", this.deptCodeList)
              this.areaList.push(res.data[x])
            }
          }
        }
        if (action != 'update') {
          for (var x = 0; x < this.areaList.length; x++) {
            if (action == this.areaList[x].areaName) {
              this.areaID = this.areaList[x].id
            }
          }
        }
        this.generateAreaDetails()
        // console.log("Printlog areaList: ", this.areaList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getAreaDeptName(deptID) {
    // console.log("Printlog function works: ", deptID)
    for (var x = 0; x < this.deptList.length; x++) {
      if (deptID == this.deptList[x].departmentId) {
        return this.deptList[x].departmentName
      }
    }
  }

  openDialogAddArea(): void {
    const dialogRef = this.dialog.open(AddAreaComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.getAreaChange(result.result)
      }
    });
  }

  openDialogEditArea(): void {
    var area = this.areaName.slice(0, this.areaName.indexOf('[')-1)
    // console.log("Printlog area: ", area)
    var array = [area, this.areaHeadID, this.areaDeptID, this.areaDescription, this.areaID]
    const dialogRef = this.dialog.open(EditAreaComponent, {
      width: '600px', data: { selectedData: array }, disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.getAreaChange('update')
      }
    });
  }

  deleteArea() {
    // console.log("Printlog AreaID: ", this.areaID)
    swal.fire({
      title: "Are you sure you want to delete this area?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Remove'
    }).then((result) => {
      if (result.value) {
        this.api.deleteArea(this.areaID).map(response => response.json()).subscribe(res => {
          if(res.message.includes("You cannot delete this Area. Area was already assigned to a schedule or to schedules.")){
            this.global.swalAlert("You cannot delete this Area", 'This Area was already assigned to a schedule or to schedules', 'warning')
            return
          }
          if(res.message.includes("The DELETE statement conflicted with the REFERENCE")){
            this.global.swalAlert("Area couldn't be deleted", 'Please remove assigned faculty first bedore deleting this area', 'warning')
            return
          }
          if(res.message.includes("Duplicate Area Name")){
            this.global.swalAlert("Duplicate found", 'An area with the name "' + this.areaName + '" already exist', 'warning')
            return
          }
          console.log(res.message)
          this.global.swalSuccess(this.areaName + ' was deleted')
          this.areaName = ''
          this.areaID = ''
          this.areaDeptCode = ''
          this.areaDeptName = ''
          this.areaDeptID = ''
          this.areaHeadName = ''
          this.areaHeadID = ''
          this.areaDescription = ''
          this.deptIDList = []
          this.deptList = []
          this.areaList = []
          this.ngOnInit()
        }, Error => {
          this.global.swalAlertError()
        })
      }
    })
  }

  addAreaFacultyIndividual(): void {
    const dialogRef = this.dialog.open(AddIndividualAreaFacultyComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        // console.log("Printlog id: ", result.result)

        if (this.areaFacultyList != null) {
          // console.log("Printlog notNull")
          for (var x = 0; x < this.areaFacultyList.length; x++) {
            if (this.areaFacultyList[x].facultyID.includes(result.result)) {
              this.global.swalAlert("Instructor is already in the list", '', 'warning')
              return
            }
          }
        }
        this.api.postAreaFaculty(this.areaID, result.result).map(response => response.json())
          .subscribe(res => {
            // console.log("Printlog message: ", res.message)
            this.global.swalSuccess('Success')
            this.generateAreaFacultyList()
          }, Error => {
            this.global.swalAlertError()
          })

      }
    });
  }

  deleteIndividual(id) {
    swal.fire({
      title: "Are you sure you want to remove this instructor?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Remove'
    }).then((result) => {
      if (result.value) {
        // this.savefunc()
        this.api.deleteAreaFacultyIndividual(id).map(response => response.json())
          .subscribe(res => {
            // console.log("Printlog deleted: ", id)
            this.global.swalSuccess('Record Deleted')
            this.generateAreaFacultyList()
          })
      }
    })
  }

  downloadExcel() {
    this.excelService.downloadFacultyEvalAreaExcelFormat(this.areaName, this.areaDescription, this.areaFacultyList)
  }

  //upload variables
  data: AOA = [];
  firstData = undefined
  errorData = false
  delayInMilliseconds = 1500; //1.5 seconds
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;

  onFileChange(evt: any) {
    this.global.swalLoading('Uploading Excel Data...');
    /* wire up file reader */
    this.firstData = undefined
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = []
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.uploadthis.nativeElement.value = ''
      //check if the uploaded file's program ID matches the selected Program on the UI
      var extractProg = this.data[4][0].slice(6)
      // console.log("Printlog extracted: ", extractProg)
      try {
        if (extractProg != this.areaName) {
          this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
          return
        }
      } catch (err) {
        this.global.swalAlert("You uploaded a mismatched file", '', 'warning')
        return
      }
      //reset varriables and arrays
      this.errorData = false
      this.facultyNames = []
      this.employeeArray = []
      if (this.data[0] != undefined) {
        this.areaFacultyList = []
        this.data.shift();
        // this.facultyDataList = this.data
        for (var i2 = 6; i2 < this.data.length; ++i2) {
          if (this.data[i2][0] != '' && this.data[i2][0] != undefined && this.data[i2][0] != null) {
            // this.data[i2][0].toString()
            this.areaFacultyList.push(this.data[i2][0])
          }
        }
        // console.log("printlog: data", this.facultyDataList)
        this.fromUpload = true
        // console.log("Printlog upload: ", this.fromUpload)
        // var duplicate = []
        // var correctData = []
        this.duplicate = []
        this.correctData = []
        let counter = 1
        this.employeeArray = []
        this.finalIds = []
        this.areaFacultyList.forEach((value) => {
          if (this.duplicate.includes(value)) {
            // console.log("Printlog: Value", value)
            this.errorData = true
            this.employeeArray.push({ id: value, name: 'Duplicated Value' })
            this.finalIds.push(value)
            counter++
          } else {
            this.api.getFullNameByID(value).map(response => response.json())
              .subscribe(res => {
                if (res.data != null) {
                  // this.errorData = false
                  this.fullName = res.data.fullName
                  this.correctData.push({ id: value, name: this.fullName })
                  // this.employeeArray = this.correctData.length;
                } else {
                  this.errorData = true
                  this.employeeArray.push({ id: value, name: res.message })
                  this.finalIds.push(value)
                }
                // console.log('counter: ', counter)
                // console.log(this.facultyDataList.length)
                if (counter == this.areaFacultyList.length) {
                  // console.log(counter)
                  for (var x = 0; x < this.correctData.length; x++) {
                    this.employeeArray.push({ id: this.correctData[x].id, name: this.correctData[x].name })
                    this.finalIds.push(this.correctData[x].id)
                  }
                  // console.log(this.employeeArray)
                }
                counter++
              })
          }
          this.duplicate.push(value)
        })
      } else {
        this.areaFacultyList = []
        this.global.swalAlert('Invalid Excel File!', '', 'warning')
      }
      this.global.swalClose();
    };
    reader.readAsBinaryString(target.files[0]);
  }

  deleteUploadedError(id) {
    for (var x = 0; x < this.employeeArray.length; x++) {
      if (this.employeeArray[x].id == id) {
        // console.log("Printlog index: ", x + ": " + id)
        this.employeeArray.splice(x, 1); // 2nd parameter means remove one item only
        this.finalIds.splice(x, 1); // 2nd parameter means remove one item only
        break
      }
    }
    for (var x = 0; x < this.employeeArray.length; x++) {
    // console.log("Printlog data:", this.employeeArray[x].name)
      if(this.employeeArray[x].name=='ID Not Found' || this.employeeArray[x].name=='Duplicated Value'){
        this.errorData=true
        break
      }else{
        this.errorData=false
      }
    }
    // console.log(this.finalIds)
  }

  uploadFacultyList() {
    this.global.swalLoading('Uploading/Replacing reords...');
    // console.log("Printlog: function works")
    this.api.deleteAreaFacultyList(this.areaID).map(response => response.json())
      .subscribe(res => {
        this.finalIds.forEach((value) => {
          // console.log("Printlog values: ", value)
          this.api.postAreaFaculty(this.areaID, value).map(response => response.json())
            .subscribe(res => {
              // console.log('printlog value to be uploaded: ', value)
              if (this.finalIds[this.finalIds.length - 1] == value) {
                this.finalIds = []
                this.ngOnInit()
                this.generateAreaFacultyList()
                this.global.swalClose();
                this.global.swalAlert("Area updated", '', 'success')
              }
            }, Error => {
              this.global.swalAlertError()
            })
        })

      }, Error => {
        this.global.swalAlertError()
      })
  }

}
