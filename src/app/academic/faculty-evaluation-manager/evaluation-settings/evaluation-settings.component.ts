import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../global.service';
import { ApiService } from '../../../api.service';
import { formatDate } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-evaluation-settings',
  templateUrl: './evaluation-settings.component.html',
  styleUrls: ['./evaluation-settings.component.css']
})
export class EvaluationSettingsComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private api: ApiService, public global: GlobalService,private datePipe : DatePipe) { }

  /* ---Initialize Variables-- */
  //#region
  facultyEvaluationSettingsArray;
  settingToUpdate='';
  form: FormGroup;
  year1;
  year2;
  term;
  status;
  startDate;
  endDate;
  ctrYear1 = 0;
  ctrYear2 = 0;
  ctrTerm = 0;
  ctrStatus = 0;
  ctrStartDate=0;
  ctrEndDate=0;
  btnSaveDisabled = true;
  submitted = false;
  disableTerm = false;
  //#endregion

  ngOnInit() {
    this.getFacultyEvaluatinoSettings();
    this.form = this.formBuilder.group({
      status: ['', Validators.required],
      term: [{ value: '', disabled: false }, Validators.required],
      year1: ['', Validators.required],
      year2: ['', Validators.required],
      startDate: [{disabled:true, value:''}],
      endDate: [{disabled:true, value:''}]
    })
  }

  save()
  {
    this.submitted = true;
    if(this.f.year1.value != null && this.f.year2.value != null) {
      if (this.f.year1.value.toString().length < 4 || this.f.year1.value.toString().length > 4) {
        this.f.year1.setErrors({ 'minmaxlength': true });
      }

      if (this.f.year2.value.toString().length < 4 || this.f.year2.value.toString().length > 4) {
        this.f.year2.setErrors({ 'minmaxlength': true });
      }
    }

     //validate startDate and endDate

     if( (this.f.startDate.value ? this.datePipe.transform(this.startDate, 'MM-dd-yyyy') : null) >
     (this.f.endDate.value ? this.datePipe.transform(this.f.endDate.value,'M/d/yyyy') : null) ){
      this.f.startDate.setErrors({ 'LowerThanEndDate': true });
     }

    //  if( (this.f.startDate.value ? formatDate(this.f.startDate.value,'M/d/yyyy','en') : null) >
    //  (this.f.endDate.value ? formatDate(this.f.endDate.value,'M/d/yyyy','en') : null) ){
    //   this.f.startDate.setErrors({ 'LowerThanEndDate': true });
    //  }

    if (!this.form.errors && !this.form.invalid && !this.f.year1.errors && !this.f.year2.errors && !this.f.startDate.errors) {

      var deptGroup='';
      var SY_ToSave;
      SY_ToSave = this.f.year1.value.toString() + this.f.year2.value.toString().substr(2, 2) + this.f.term.value.toString();

      this.facultyEvaluationSettingsArray.forEach((value)=>{
        if(value.departmentId==this.settingToUpdate){
          deptGroup=value.departmentGroup;
        }
      })

      if(deptGroup == 'HS' || deptGroup == 'Elem') {
        SY_ToSave=SY_ToSave.slice(0,-1);
      }

      //save
      this.api.putFacultyEvaluationSetting(this.settingToUpdate,
        {
          "schoolYear":SY_ToSave,
          "isOpen": parseInt(this.f.status.value),
          "startDate": this.f.startDate.value ? formatDate(this.f.startDate.value,'M/d/yyyy','en') : null,
          "endDate":this.f.endDate.value ? formatDate(this.f.endDate.value,'M/d/yyyy','en') : null
        }
      )
        .map(response => response.json())
        .subscribe(res => {
          //set value
          this.resetVar();
          this.setFormValue();
          this.global.swalSuccess('Success');
        }, Error => {
          this.global.swalAlertError();
          console.log(Error);
        })

    }

  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  //for drop down list
  getFacultyEvaluatinoSettings() {
    this.facultyEvaluationSettingsArray=[];
    this.api.geFacultyEvaluationSettings('')
    .map(response=>response.json())
    .subscribe(res=>{
      //console.log(res.data);
      if(res.data != null)
      {
        res.data.forEach((value)=>{
          if(this.global.viewdomain.includes(value.departmentId))
          {
            this.facultyEvaluationSettingsArray.push(value);
          }
        })
      }
    },Error=>{
      this.global.swalAlertError();
    })
  }


  onChange(value, elementId) {
    switch (elementId) {
      case 'deptNameSelector': {
        this.resetVar();
        this.setFormValue();
        break;
      }
      case 'term': {
        //counter = 1
        if (elementId == 'term' && this.term != value.value) { this.ctrTerm = 1 }
        //counter = 0
        if (elementId == 'term' && this.term == value.value) { this.ctrTerm = 0 }
        break;
      }
      case 'year1': {
        //counter = 1
        if (elementId == 'year1' && this.year1 != value) { this.ctrYear1 = 1 }
        //counter = 0
        if (elementId == 'year1' && this.year1 == value) { this.ctrYear1 = 0 }
        break;
      }
      case 'year2': {
        //counter = 1
        if (elementId == 'year2' && this.year2 != value) { this.ctrYear2 = 1 }
        //counter = 0
        if (elementId == 'year2' && this.year2 == value) { this.ctrYear2 = 0 }
        break;
      }
      case 'status': {
        //counter = 1
        if (elementId == 'status' && this.status != value.value) { this.ctrStatus = 1 }
        //counter = 0
        if (elementId == 'status' && this.status == value.value) { this.ctrStatus = 0 }
        break;
      }
      case 'startDate': {
        if(value){
          //counter = 1
          if (elementId == 'startDate' && this.startDate != formatDate(value,'yyyy-MM-dd','en')) {
                this.ctrStartDate = 1
              }
          //counter = 0
          if (elementId == 'startDate' && this.startDate == formatDate(value,'yyyy-MM-dd','en')) {
              this.ctrStartDate = 0
            }
        }
        break;
      }
      case 'endDate': {
        if(value){
          //counter = 1
          if (elementId == 'endDate' && this.endDate != formatDate(value,'yyyy-MM-dd','en')) {
                this.ctrEndDate = 1
              }
          //counter = 0
          if (elementId == 'endDate' && this.endDate == formatDate(value,'yyyy-MM-dd','en')) {
              this.ctrEndDate = 0
            }
        }
        break;
      }
    }
    if (this.ctrTerm == 1 || this.ctrYear1 == 1 || this.ctrYear2 == 1 || this.ctrStatus == 1 || this.ctrStartDate == 1 || this.ctrEndDate == 1)  {
      this.btnSaveDisabled = false;
    }

    if (this.ctrTerm == 0 && this.ctrYear1 == 0 && this.ctrYear2 == 0 && this.ctrStatus == 0  && this.ctrStartDate == 0 && this.ctrEndDate == 0) {
      this.btnSaveDisabled = true;
    }
  }

  setFormValue(){
    this.api.geFacultyEvaluationSettings('')
    .map(response=>response.json())
    .subscribe(res=>{
      if(res.data != null)
      {
        res.data.forEach((value)=>{
          if(value.departmentId == this.settingToUpdate)
          {
            //term initialization
            if(value.departmentGroup == 'College' || value.departmentGroup == 'GS')
            {
              this.term = value.schoolYear.substr(-1, 1);
              this.f.term.setValue(value.schoolYear.substr(-1, 1));
              this.f.term.enable();
            }
            else {
              this.term = '1';
              this.f.term.setValue('1');
              this.f.term.disable();
            }
            //initialize orginal data
            this.year1=this.getsyLeft(value.schoolYear);
            this.year2=this.getsyRight(value.schoolYear);
            this.status=value.isOpen.toString();
            this.startDate=(value.startDate ? formatDate(value.startDate,'yyyy-MM-dd','en'): '');
            this.endDate=(value.endDate ? formatDate(value.endDate,'yyyy-MM-dd','en'): '');

            //initialize form values
            this.f.year1.setValue(this.getsyLeft(value.schoolYear));
            this.f.year2.setValue(this.getsyRight(value.schoolYear));
            this.f.status.setValue(value.isOpen.toString());
            this.f.startDate.setValue(value.startDate ? formatDate(value.startDate,'yyyy-MM-dd','en'): '');
            this.f.endDate.setValue(value.endDate ? formatDate(value.endDate,'yyyy-MM-dd','en'): '');

            this.btnSaveDisabled=true;
          }
        })
      }
    },Error=>{
      this.global.swalAlertError();
    })
  }

  resetVar() {
    this.term = '';
    this.year1 = '';
    this.year2 = '';
    this.submitted = false;
    this.btnSaveDisabled = true
    this.ctrTerm = 0;
    this.ctrYear1 = 0;
    this.ctrYear2 = 0;
    this.ctrStatus = 0;
  }


  cancel() {
    this.form.reset();
    Object.keys(this.form.controls).forEach(key => {
        this.form.get(key).setErrors(null) ;
    });

    this.resetVar;
    this.setFormValue();
  }

  yearchange1() {
    this.f.year2.setValue(this.f.year1.value + 1);
  }
  yearchange2() {
    this.f.year1.setValue(this.f.year2.value - 1);
  }

  getsyLeft(recordSY) {
    var y = parseInt(recordSY.substring(0, 4));
    return y;
  }

  getsyRight(recordSY) {
    var y = parseInt(recordSY.substring(0, 4)) + 1;
    return y;
  }

}
