import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { GlobalService } from '../../../global.service';

import { ApiService } from '../../../api.service';
@Component({
  selector: 'app-code-eval-details',
  templateUrl: './code-eval-details.component.html',
  styleUrls: ['./code-eval-details.component.css']
})
export class CodeEvalDetailsComponent implements OnInit {

  headInfoArray = []
  headName = ''
  headPosition = ''
  multipleHead = false;
  resultTrigger = false;
  evaluation = true;
  constructor(public dialogRef: MatDialogRef<CodeEvalDetailsComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService,private api: ApiService) { }


  ngOnInit(): void {
    this.multipleHead = false;
    this.resultTrigger = false;

    if(this.data.type == "evaluation"){
      this.evaluation = true;
      this.getHeadBasicInformation(this.data.evaluationSheetData.createdBy)

      if(this.data.evaluationSheetData.createdBy == this.global.requestid)
        this.multipleHead = true
      else
        this.multipleHead = true;
    }
    else{
      this.evaluation = false;
    }
  }

  getHeadBasicInformation(id){
    this.api.getFacEvalEmployee(id)
     .map(response=>response.json())
     .subscribe(res=>{
      console.log(res.data)
       if(res.data!=null){
        this.headInfoArray=[];
          this.headInfoArray=res.data;
          this.headName = this.headInfoArray[0].fullname
          this.headPosition = this.headInfoArray[0].position
        }

     },Error=>{
       this.global.swalAlertError(Error);
     })
  }

  reCreateEval(){


    this.api.deleteFacultyEvaluation(this.data.evaluationSheetData.id)
      .map(response => response.json())
      .subscribe(res => {
        if(res.message != null){
          this.global.swalAlert("Information",res.message,"information")
          this.resultTrigger = true;
          this.resetValues();
        }else{

        }

      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error)
      });
  }
  resetValues(){
    if(this.resultTrigger){
      this.headInfoArray = []
      this.headName = ''
      this.headPosition = ''
      this.multipleHead = false;
      this.data.evaluationSheetData = []
      this.data.subjectData = []
    }else{

    }
  }

  onNoClickclose(): void {
    if(this.resultTrigger == false)
      this.dialogRef.close({result:'cancel'});
    else
      this.dialogRef.close({result:'deleted'});
  }
}
