import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeEvalDetailsComponent } from './code-eval-details.component';

describe('CodeEvalDetailsComponent', () => {
  let component: CodeEvalDetailsComponent;
  let fixture: ComponentFixture<CodeEvalDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeEvalDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CodeEvalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
