import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';


import * as ExcelJS from "exceljs/dist/exceljs"
import * as fs from 'file-saver';
declare const ExcelJS: any;
@Component({
  selector: 'app-faculty-evaluation-table-details',
  templateUrl: './faculty-evaluation-table-details.component.html',
  styleUrls: ['./faculty-evaluation-table-details.component.css']
})
export class FacultyEvaluationTableDetailsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<FacultyEvaluationTableDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService,) { }

  codeNo = '';
  schedType = '';
  evaluated = [];
  notEvaluated = [];
  ngOnInit() {
    // console.log(this.data.selectedData)
    // console.log(this.data.students_evaluation)
    this.codeNo = this.data.selectedData.codeNo;
    if(this.data.selectedData.schedType == 1)
      this.schedType = 'Laboratory';
    else
      this.schedType = 'Lecture';


    this.data.students_evaluation.details.forEach(element => {
      if (element.evaluationSheetHeaderID) {
        this.evaluated.push(element)
      } else {
        this.notEvaluated.push(element)
      }
    });
  }

  step = 0;

  setStep(index: number) {
    this.step = index;
  }
  verifyStatus(isValid){
    if(isValid == 0)
      return 'INVALID RESPONSE';
    else
      return 'VALID RESPONSE';
  }


  generateEvaluationDetails(){
    var data=[], data2=[];
    this.notEvaluated.forEach(element => {
      data.push({
        "ID NUMBER": element.idnumber,
        "STUDENT NAME": element.name
      })
    });
    this.evaluated.forEach(element => {
      data2.push({
        "ID NUMBER": element.idnumber,
        "STUDENT NAME": element.name
      })
    });

    this.generateEvaluationDetailsExcel(data,data2)
  }

  async generateEvaluationDetailsExcel(notEvaluated,evaluated){
    const header = ["ID NUMBER", "STUDENT NAME"];
    // Create workbook and worksheet
    var workbook = new ExcelJS.Workbook();

    const worksheet = workbook.addWorksheet();
    worksheet.name = 'Code '+this.codeNo;

    var FileName = 'Code '+this.codeNo+' Evaluation Details';
    let subjectID = worksheet.addRow(['Subject ID:', this.data.selectedData.subjectID]);
    let subjectTitle = worksheet.addRow(['Subject title:', this.data.selectedData.subjectTitle]);
    let schedType = worksheet.addRow(['Schedule Type:', this.schedType]);
    let enrolledStudents = worksheet.addRow(['Officially enrolled students:', this.data.selectedData.oe]);
    let notSubmittedEvaluation = worksheet.addRow(['No. of not submitted evaluation:', this.notEvaluated.length]);
    let submittedEvaluation = worksheet.addRow(['No. of submitted evaluation:', this.evaluated.length]);

    subjectID.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    subjectTitle.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    schedType.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    enrolledStudents.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    notSubmittedEvaluation.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    submittedEvaluation.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B3').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B4').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B5').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B6').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B4').alignment = {horizontal: 'left'};
    worksheet.getCell('B5').alignment = {horizontal: 'left'};
    worksheet.getCell('B6').alignment = {horizontal: 'left'};
    worksheet.addRow(['']);

    let studNotEval = worksheet.addRow(['List of students who did not evaluate']);
    studNotEval.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    const headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
    });



    notEvaluated.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
    })

    let studEval = worksheet.addRow(['List of students who submitted their evaluation']);
    studEval.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    evaluated.forEach((element) => {
      let eachRow = [];
      header.forEach((headers) => {
          eachRow.push(element[headers])
      })
      if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
          })
      } else {
          const row = worksheet.addRow(eachRow);
          row.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: {
                  argb: 'FFFFFFFF'
              }
          };
          row.font = {
              color: {
                  argb: '00000000',
              },
              bold: false
          }
          row.eachCell((cell, number) => {
              cell.border = {
                  top: {
                      style: 'thin'
                  },
                  left: {
                      style: 'thin'
                  },
                  bottom: {
                      style: 'thin'
                  },
                  right: {
                      style: 'thin'
                  }
              };
          });
          row.alignment={
            wrapText: true
          }
      }
  })

    worksheet.getColumn(1).width = 40;
    worksheet.getColumn(2).width = 70;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 100;
    worksheet.getColumn(6).width = 7;
    worksheet.getColumn(7).width = 12;



    workbook.xlsx.writeBuffer().then((data: any) => {
        const blob = new Blob([data], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        fs.saveAs(blob, FileName + '.xlsx');
    });
  }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
}
