import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultyEvaluationTableDetailsComponent } from './faculty-evaluation-table-details.component';

describe('FacultyEvaluationTableDetailsComponent', () => {
  let component: FacultyEvaluationTableDetailsComponent;
  let fixture: ComponentFixture<FacultyEvaluationTableDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyEvaluationTableDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultyEvaluationTableDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
