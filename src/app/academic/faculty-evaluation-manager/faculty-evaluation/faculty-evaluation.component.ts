import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from '../../../global.service';
import { ApiService } from '../../../api.service';
import { MatDialog, } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';;
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

import { MatTableDataSource, MatSort, } from '@angular/material';
import { ViewChild, } from '@angular/core';
import {MatChipsModule} from '@angular/material/chips';
import Chart from 'chart.js/auto';
// import {Chart} from 'chart.js';
// import ChartDataLabels from 'chartjs-plugin-datalabels';

import { EmployeeLookupComponent } from '../../lookup/employee-lookup/employee-lookup.component';
import { FacultyEvaluationTableDetailsComponent } from './../faculty-evaluation-table-details/faculty-evaluation-table-details.component';
import { CodeEvalDetailsComponent } from './../code-eval-details/code-eval-details.component'


import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

import * as ExcelJS from "exceljs/dist/exceljs"
import * as fs from 'file-saver';
declare const ExcelJS: any;

@Component({
  selector: 'app-faculty-evaluation',
  templateUrl: './faculty-evaluation.component.html',
  styleUrls: ['./faculty-evaluation.component.css']
})
export class FacultyEvaluationComponent implements OnInit {

  facultyName = '';
  id='';
  image;
  dtrid;
  form: FormGroup;
  proglevel;
  employee
  facultyLoadArray = [];

  progressBarPercentage = 0;
  progressBarbufferValue = 0;

  facultyEvalSheetArray;


  dataSource;
  tableArr:Array<any>;

  Summary_of_Student_Evaluation_chart: any;
  Summary_of_Student_Evaluation_lab_chart: any;
  Summary_of_Student_Evaluation_lec_chart: any;
  pie_chart_lab: any;
  pie_chart_lec: any;

  arrayCommentsLec = [];
  arrayCommentslab = [];
  arrayStrongPoints = [];
  arrayDevelopingPoints = [];
  arrayPointsForImporvement = [];

  arrayStrongPointsLab = [];
  arrayDevelopingPointsLab = [];
  arrayPointsForImporvementLab = [];

  transmutedArrayValuesLab=[];
  transmutedArrayValuesLec=[];

  mode = 'indeterminate';
  pie_chart_lec_spinner = true;
  pie_chart_lab_spinner = true;

  arrayComments_spinner = true;

  lectureValidCount=0;
  laboratoryValidCount=0;
  lectureSum = 0;
  laboratorySum = 0;
  averageLec = 0;
  averageLab = 0;

  validLectureSum = 0;
  validLaboratorySum = 0;

  lectureAverageInterpretation = '';
  laboratoryAverageInterpretation = '';

  displayedColumns = ['codeNo','subjectTitle', 'schedType', 'evaluationSheetSummary','transmutedValue','oe','respondentsCount'];
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private domSanitizer: DomSanitizer,
    public global: GlobalService,
    private api: ApiService,
    private router: Router,
    private breakpointObserver: BreakpointObserver) {

     }

  ngOnInit() {
    this.proglevel=this.global.domain;
    if (this.global.domain == 'HIGHSCHOOL'){
      if(this.global.checkdomain('0010'))
        this.proglevel = "Senior High School"
      else
        this.proglevel = "High School"
    }

    this.form = this.formBuilder.group({
      rankID: ['', Validators.required],
      empTypeID: ['', Validators.required],
      dtrid: ['',Validators.required],
      empStatusID:['', Validators.required],
      active: ['', Validators.required],
      position: ['', Validators.required]
    })


  }

  get formValueGet() { return this.form.controls; }

  clearValues(){
    this.arrayCommentsLec = [];
    this.arrayCommentslab = [];
    this.arrayStrongPoints = [];
    this.arrayDevelopingPoints = [];
    this.arrayPointsForImporvement = [];
    this.arrayStrongPointsLab = [];
    this.arrayDevelopingPointsLab = [];
    this.arrayPointsForImporvementLab = [];
    this.facultyLoadArray = [];

    this.transmutedArrayValuesLab=[];
    this.transmutedArrayValuesLec=[];

    this.facultyName = '';
    this.image = '';
    this.dtrid = '';

    this.lectureValidCount=0;
    this.laboratoryValidCount=0;
    this.lectureSum = 0;
    this.laboratorySum = 0;
    this.averageLec = 0;
    this.averageLab = 0;

    this.validLectureSum = 0;
    this.validLaboratorySum = 0;

    this.progressBarPercentage=0;
    this.progressBarbufferValue = 0;

    this.mode = 'indeterminate';
    this.pie_chart_lec_spinner = true;
    this.pie_chart_lab_spinner = true;

    this.arrayComments_spinner = true;

    var SummaryChartExist = Chart.getChart("Summary_of_Student_Evaluation"); // <canvas> id
    if (SummaryChartExist != undefined)
      SummaryChartExist.destroy();

    var LabPieChartExist = Chart.getChart("pie_chart_lab"); // <canvas> id
    if (LabPieChartExist != undefined)
      LabPieChartExist.destroy();

    var LecPieChartExist = Chart.getChart("pie_chart_lec"); // <canvas> id
    if (LecPieChartExist != undefined)
      LecPieChartExist.destroy();
  }

  assignFormValue(){

    //for <input> assign to variable
    this.dtrid=this.employee[0].dtrid;

    this.formValueGet.rankID.setValue(this.employee[0].rankID);
    this.formValueGet.empTypeID.setValue(this.employee[0].empTypeID);
    this.formValueGet.dtrid.setValue(this.dtrid);
    this.formValueGet.empStatusID.setValue(this.employee[0].empStatusID);
    this.formValueGet.active.setValue(this.employee[0].active.toString());
    this.formValueGet.position.setValue(this.employee[0].position);


  }

  keyDownFunction(event) {

    var that = this;
    var trigger = false;

    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.clearValues();
      var headID = '';

      if (this.id != '') {
        if(this.id != this.global.requestid()){
          this.global.swalLoading('Retrieving Person and Employee Informations');
          if(this.global.checkrole("Dean")||this.global.checkrole("Principal")||this.global.checkrole("VP For Academics"))
            headID = '';
          else
            headID = this.global.requestid()

          this.api.getFacultyEvalSelect(headID,this.id)
            .map(response => response.json())
            .subscribe(res => {
              if (res.data.length!=0) {
                if(res.data.length>=1){

                  if(headID==""){//dean role
                    if(checkDeanPrincipalProcess(res.data)){
                      this.facultyName = res.data[0].facultyName;
                      this.getPersonIDInfo();
                    }else{
                      this.global.swalAlert('','INVALID SEARCH.<br><p style="color:red">Error: Employee does not belong to your group </p>','error');
                    }

                  }else{//PC Role
                    this.facultyName = res.data[0].facultyName;
                    this.getPersonIDInfo();
                  }
                }else{ //error employee does'nt have valid data
                  this.global.swalAlert('','INVALID SEARCH.<br><p style="color:red">Employee does not have evaluation data or employee does not belong to your group</p><br>','warning');
                }
              } else {
                this.global.swalAlert('','INVALID SEARCH.<br><p style="color:red">Employee does not have evaluation data or employee does not belong to your group</p><br>','warning');
              }
            }, Error => {
              this.global.swalAlertError(Error);
            });
        }else{
          this.global.swalAlert('','<p style="color:red">You cannot view your own evaluation</p><br>','warning');
        }

      }
    }

    function checkDeanPrincipalProcess(data){
      data.forEach(element => {
        if(that.global.viewdomain.includes(element.departmentId) && that.global.viewdomainname.includes(element.departmentCode)){
          trigger = true;
        }
      });
      if(trigger ==true)
        return true
      else
        return false
    }
  }

  getPersonIDInfo(){
    this.api.getFacultyEvalPersonIDInfo(this.id)
    .map(response => response.json())
    .subscribe(res => {
      if (res.data != null) {
        this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
        //get employee hiring info
        this.getEmployeesHiringBasicInfo();
      }
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

  dualDomainTrigger = false;

  getFacultyLoad(){
    this.global.swalLoading('Loading evaluation')
    this.api.getFacultyLoad(this.global.syear,this.proglevel,this.id,0)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        this.dualDomainTrigger = false;
        res.data.forEach(element => {
          if(this.getValidInvalidStatus(element.oe,element.evaluationSheetSummary) == "VALID"){
            if(element.schedType == 0){
              this.validLectureSum+=element.evaluationSheetSummary.transmutedValue;
              this.lectureValidCount++;
            }
            else{
              this.validLaboratorySum+=element.evaluationSheetSummary.transmutedValue;
              this.laboratoryValidCount++
            }
          }
        });
        this.facultyLoadArray = res.data
        this.dataSource = new MatTableDataSource(this.facultyLoadArray);
        this.dataSource.sort = this.sort;

        this.processCharts(this.facultyLoadArray);
      }else this.dualDomainTrigger = true;

      this.global.swalClose();
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }



  personlookup(): void {
    const dialogRef = this.dialog.open(EmployeeLookupComponent, {
      width: '600px',
      disableClose: true,
      data:{searchType:'faculty_evaluation'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }

  getEmployeesHiringBasicInfo(){
    //this.employee=undefined;
     this.form.reset();

     this.api.getFacEvalEmployee(this.id)
     .map(response=>response.json())
     .subscribe(res=>{

       if(res.data!=null){
        this.employee=[];
          this.employee=res.data;
          this.assignFormValue();

          //get faculty load
          // this.getFacultyLoad();

          this.getFacultyLoad()

        } else{
         this.formValueGet.active.setValue('1');
        }

     },Error=>{
       this.global.swalAlertError(Error);
     })
  }

  tabbing=0;
  getindex(tab){
    this.tabbing = tab.index;
  }

  getSchedType(schedType){
    if(schedType == 1)
      return "Lab";
    else
      return "Lec";
  }
  getEvaluationStatus(evaluationSheetHeader){
    if(evaluationSheetHeader == null || evaluationSheetHeader == '')
      return "Not yet created"
    else
      return "Evaluation created"
  }

  getValidResponseOverOE(oe,evaluationSheetSummary){
    if(evaluationSheetSummary){
      return evaluationSheetSummary.respondentsCount+'/'+oe;
    }else{
      return "0/"+oe;
    }
  }
  // getTransmutationInterpretation(average,toolID){
  //   this.api.getTransmutedValueAndInterpretation(average,toolID)
  //   .map(response => response.json())
  //   .subscribe(res => {
  //     if (res.data != null) {
  //       if(toolID == 1)
  //         this.lectureAverageInterpretation = res.data.interpretation
  //       else
  //         this.laboratoryAverageInterpretation = res.data.interpretation
  //     }
  //   })
  // }
  getAverage(type){
    this.lectureSum = this.validLectureSum
    this.laboratorySum = this.validLaboratorySum
    if(type == 0){
      this.averageLec = parseFloat((this.lectureSum/this.lectureValidCount).toFixed(2))
      if(this.averageLec.toString() == 'NaN')
        this.averageLec = 0;
      return this.averageLec
    }
    else{
      this.averageLab = parseFloat((this.laboratorySum/this.laboratoryValidCount).toFixed(2))
      if(this.averageLab.toString() == 'NaN')
        this.averageLab = 0;

      return this.averageLab;
    }


  }
  getValidInvalidStatus(oe,evaluationSheetSummary){
      if(evaluationSheetSummary){
        if(evaluationSheetSummary.archived == 0){
          let fiftyP = oe/2;
          if(evaluationSheetSummary.transmutedValueInterpretation){
            if(evaluationSheetSummary.respondentsCount>fiftyP)
              return 'VALID';
            else
              return 'INVALID'
          }
          else
            return 'INVALID'
        }else
          return 'ARCHIVED'
      }else{
        return "NONE"
      }

  }


  //#region //TOGGLES---------------------------------------------------------------/

  statusid = 0;
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  checked(param){
      if (this.statusid==0){
        this.statusid=0
      }
      else{
        this.statusid=1
      }
  }

  checkDomain(){
    if(this.global.domain == 'COLLEGE' || this.global.domain == 'GRADUATE SCHOOL')
      return true;
    else
      return false;
  }
  checkToggleActiveStatus(isActive){
    if (isActive==true){
      this.statusid=0
    }
    else{
      this.statusid=1
    }
  }

  checkEmployeeBasicEdDomain(){
    if(this.global.domain == "HIGHSCHOOL"){
      if(this.global.checkdomain('0010')&&this.global.checkdomain('0009'))
        return true
      else
        return false
    }else
      return false
  }

  verifyInterpretation(array,oe){

    if(array){
      if(array.transmutedValueInterpretation)
          return array.transmutedValueInterpretation;
      else
        return 'NONE'
    }else
      return "NONE"
  }

  verifyTransmutedValue(evaluationSheetSummary){
    if(evaluationSheetSummary)
      return evaluationSheetSummary.transmutedValue
    else
      return 0;
  }
  tempClassName='';
  verifyClassMask(className){
    if(this.tempClassName == className){
      return '';
    }else{
      return this.tempClassName = className;
    }

  }


  setViewDomain(domain){
    this.proglevel = domain;
  }
  //#endregion

  //#region
  //-----------------------------------------------------------------/
  createEvaluation(param,paramSubjectDetails){
    var progLevel = this.proglevel;
    var schoolYear = this.global.syear;
    var facultyID = paramSubjectDetails.facultyID;
    var subject = paramSubjectDetails.subjectTitle;
    var section = '';
    if(this.global.domain == "HIGHSCHOOL")
      section = paramSubjectDetails.section;

    var codeNo = paramSubjectDetails.codeNo;
    var headID = this.global.requestid();
    var schedType = paramSubjectDetails.schedtype
    var isActive = true;


    this.api.postFacultyEvalCreateEvalSheet({
      "progLevel": progLevel,
      "schoolYear": schoolYear,
      "facultyID": facultyID,
      "subject": subject,
      "section": section,
      "codeNo": codeNo,
      "headID": headID,
      "schedType": schedType,
      "isActive": isActive
    })
      .map(respose => respose.json())
      .subscribe(res => {
        if (res.data == null) {
          this.keyDownFunction('onoutfocus');
          this.global.swalAlert("Error", "Something went wrong. Please contact your system provider.", "");
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  //------------------------ CHARTS PROCESSING

  list_of_students_evaluation_per_code = [];
  processCharts(dataSource){

    let maskCtr = 1;
    let commentCtr = 0;
    let codeLength = 0;
    var transmutedArrayMaskedNames=[];
    var transmutedArrayComplete=[];
    var transmutedNewArrayComplete=[];
    var transmutedArrayComplete_mutated = [];
    var transmutedCodeSection = [];


    var that = this;
    var temp = ''

    this.arrayCommentsLec = [];
    this.arrayCommentslab = [];

    this.arrayStrongPoints = [];
    this.arrayDevelopingPoints = [];
    this.arrayPointsForImporvement = [];

    this.arrayStrongPointsLab = [];
    this.arrayDevelopingPointsLab = [];
    this.arrayPointsForImporvementLab = [];
    var domain = this.checkDomain();

    var codelengthCtr = 0; // comparission counter for generation loading

    var SummaryChartExist = Chart.getChart("Summary_of_Student_Evaluation"); // <canvas> id
    if (SummaryChartExist != undefined)
      SummaryChartExist.destroy();

    var LabPieChartExist = Chart.getChart("pie_chart_lab"); // <canvas> id
    if (LabPieChartExist != undefined)
      LabPieChartExist.destroy();

    var LecPieChartExist = Chart.getChart("pie_chart_lec"); // <canvas> id
    if (LecPieChartExist != undefined)
      LecPieChartExist.destroy();

    dataSource.forEach(element => {
      if(that.getValidInvalidStatus(element.oe,element.evaluationSheetSummary) == "VALID"){
        if(domain)//if domain is college or gs
          transmutedArrayComplete.push('Code '+element.codeNo) // loop for getting all valid codes
        else{
          transmutedArrayComplete.push({
            codesection: element.section,
            schedType: element.schedType,
            subject: element.subjectTitle
          }) // loop for getting all valid subjects
        }
      }

      if(element.evaluationSheetSummary!=null || element.evaluationSheetSummary!=undefined)
        codelengthCtr+=1;

    });

    if(domain){
      transmutedArrayMaskedNames = transmutedArrayComplete.filter((element, index) => { //this is to removed the duplicated names for table buttom data label
        return transmutedArrayComplete.indexOf(element) === index;
      });

      transmutedArrayMaskedNames.forEach(element => { // initialization of consolidated codes with zero initial lec and lab values
        transmutedNewArrayComplete.push({
          codeSection: element,
          subject:'',
          lecValue: 0,
          labValue: 0
        })
      });


    }else{
      transmutedArrayComplete_mutated = transmutedArrayComplete.reduce((unique, o) => {
          if(!unique.some(obj => obj.section === o.section && obj.subject === o.subject && obj.schedType !== o.schedType)) {
            unique.push(o);
          }
          return unique;
      },[]);

      transmutedArrayComplete_mutated.forEach(element => {
        transmutedArrayMaskedNames.push(element.codesection)

        transmutedCodeSection.push({
          codeSection: element.codesection,
          subject: element.subject
        });
      });


      transmutedCodeSection.forEach(element => { // initialization of consolidated codes with zero initial lec and lab values
        transmutedNewArrayComplete.push({
          codeSection: element.codeSection,
          subject: element.subject,
          lecValue: 0,
          labValue: 0
        })
      });

    }



    mutate_array_transmute(dataSource);

    this.getFacultyEvalDevelopingPointsSummary(this.id);
    this.Summary_of_Student_Evaluation_chart = new Chart("Summary_of_Student_Evaluation", {
      type: 'bar', //this denotes tha type of chart
      data: {// values on X-Axis
        labels: transmutedArrayMaskedNames,
	       datasets: [
          {
            label: "Lecture",
            data: this.transmutedArrayValuesLec,
            backgroundColor: 'rgba(75, 192, 192, 0.5)'
          },
          {
            label: "Laboratory",
            data: this.transmutedArrayValuesLab,
            backgroundColor: 'rgba(54, 162, 235, 0.5)'
          }

        ]
      },

      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });




    ////////////////graph functions

    function changeValue(type,value,codesection,subject){
      for(var i in transmutedNewArrayComplete){
        if(domain){
          if(transmutedNewArrayComplete[i].codeSection == codesection){
            if(type == 0){
              transmutedNewArrayComplete[i].lecValue = value;
              break;
            }else{
              transmutedNewArrayComplete[i].labValue = value;
              break;
            }
          }
        }else{
          if(transmutedNewArrayComplete[i].codeSection == codesection && transmutedNewArrayComplete[i].subject == subject){
            if(type == 0){
              transmutedNewArrayComplete[i].lecValue = value;
              break;
            }else{
              transmutedNewArrayComplete[i].labValue = value;
              break;
            }
          }
        }
      }
    }



    function mutate_array_transmute(data){
      codeLength = codelengthCtr;
      that.progressBarPercentage = 0
      that.progressBarbufferValue = 100/(codeLength)



      var codesection = '';
      data.forEach(element => {
        if(element.evaluationSheetSummary!=null || element.evaluationSheetSummary!=undefined){
          populate_comments(element);

          if(domain)//if domain is college or gs
            codesection = 'Code '+element.codeNo
          else
            codesection = element.section


          changeValue(
            element.schedType,
            element.evaluationSheetSummary.transmutedValue,
            codesection,
            element.subjectTitle
          );
        }else{
          changeValue(
            element.schedType,
            0,
            codesection,
            element.subjectTitle
          );
        }
      });
      populate_bar_graph();
    }

    function populate_bar_graph(){
      transmutedNewArrayComplete.forEach(element => {
        that.transmutedArrayValuesLec.push(element.lecValue)
        that.transmutedArrayValuesLab.push(element.labValue)
      });
    }

    function populate_comments(array){

      that.api.getFacultyEvalEnrolledStudentsInACodeNoSubject(that.proglevel,array.schedType,array.codeNo,array.subjectTitle,array.section)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          that.progressBarPercentage = Math.round(that.progressBarbufferValue * commentCtr)
          commentCtr++;
          var codeSection = '';
          if(that.checkDomain()){//if true, college
            codeSection = array.codeNo;
          }
          else{
            codeSection = array.section
          }

          that.list_of_students_evaluation_per_code.push({
            codeNoSection: codeSection,
            schedType: array.schedType,
            subjectTitle: array.subjectTitle,
            details: res.data
          })
          getComments(res.data,array.codeNo,array.schedType);
          if(commentCtr == codeLength)
            that.arrayComments_spinner = false;
        }
      }, Error => {
        that.global.swalAlertError(Error);
      })
    }

    function getComments(array,codeNo,schedType){
      array.forEach(element => {
        if(element.comments){
          if(that.checkDomain()){
            if(schedType == 0){
              that.arrayCommentsLec.push({
                // "class": 'Class '+ maskCtr,
                "class": 'Code '+codeNo,
                "comment": element.comments
              })
            }
            else{
              that.arrayCommentslab.push({
                // "class": 'Class '+ maskCtr,
                "class": 'Code '+codeNo,
                "comment": element.comments
              })
            }
          }else{
            if(schedType == 0){
              that.arrayCommentsLec.push({
                // "class": 'Class '+ maskCtr,
                "class": element.section,
                "comment": element.comments
              })
            }
            else{
              that.arrayCommentslab.push({
                // "class": 'Class '+ maskCtr,
                "class": element.section,
                "comment": element.comments
              })
            }
          }
        }
      });



    }

  }


  getFacultyEvalDevelopingPointsSummary(facultyID){
    var archived = 0;
    //get method for lecture
    this.api.getFacultyEvalDevelopingPointsSummary(this.proglevel,0,facultyID,archived)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          res.data.forEach(element => {
            if(element.interpretation == 'Strong Points'){
              this.arrayStrongPoints.push({
                'itemNo': element.itemNumber,
                'question': element.question
              })
            }else if(element.interpretation == 'Developing Points'){
              this.arrayDevelopingPoints.push({
                'itemNo': element.itemNumber,
                'question': element.question
              })
            }else{
              this.arrayPointsForImporvement.push({
                'itemNo': element.itemNumber,
                'question': element.question
              })
            }
          });

          this.pie_chart_lec_spinner = false;
          this.pie_chart_lec = new Chart("pie_chart_lec", {
            type: 'polarArea', //this denotes tha type of chart
            data: {// values on X-Axis
              labels: [
                'Strong points',
                'Developing points',
                'Points for improvement',
              ],
               datasets: [
                {
                  label: 'My First Dataset',
                  data: [this.arrayStrongPoints.length, this.arrayDevelopingPoints.length, this.arrayPointsForImporvement.length],
                  backgroundColor: [
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(75, 192, 192, 0.5)'
                  ],
                  hoverOffset: 4
                }
              ]
            },
            options: {
              responsive: true,
              maintainAspectRatio: false,
            }
          });
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
     //get method for laboratory
     this.api.getFacultyEvalDevelopingPointsSummary(this.proglevel,1,facultyID,archived)
     .map(response => response.json())
     .subscribe(res => {
       if (res.data != null) {
         res.data.forEach(element => {
          if(element.interpretation == 'Strong Points'){
            this.arrayStrongPointsLab.push({
              'itemNo': element.itemNumber,
              'question': element.question
            })
          }else if(element.interpretation == 'Developing Points'){
            this.arrayDevelopingPointsLab.push({
              'itemNo': element.itemNumber,
              'question': element.question
            })
          }else{
            this.arrayPointsForImporvementLab.push({
              'itemNo': element.itemNumber,
              'question': element.question
            })
          }
        });
        this.pie_chart_lab_spinner = false;
        this.pie_chart_lab = new Chart("pie_chart_lab", {
          type: 'polarArea', //this denotes tha type of chart
          data: {// values on X-Axis
            labels: [
              'Strong points',
              'Developing points',
              'Points for improvement',
            ],
             datasets: [
              {
                label: 'My First Dataset',
                data: [this.arrayStrongPointsLab.length, this.arrayDevelopingPointsLab.length, this.arrayPointsForImporvementLab.length],
                backgroundColor: [
                  'rgba(54, 162, 235, 0.5)',
                  'rgba(255, 99, 132, 0.5)',
                  'rgba(75, 192, 192, 0.5)'
                ],
                hoverOffset: 4
              }
            ]
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
          }
        });

       }
     }, Error => {
       this.global.swalAlertError(Error);
     })
  }
  //#endregion


  //#region
  //------------------------ DIALOGS - calling of modal/pop-out modules
  getCodeStudentEvalDetails(array): void {
    var students_evaluation=[]
    this.list_of_students_evaluation_per_code.forEach(element => {
      if(this.checkDomain()){ //if true, college
        if (element.codeNoSection == array.codeNo && element.schedType == array.schedType) {
          students_evaluation = element
        }
      }else{
        if (element.codeNoSection == array.section && element.schedType == array.schedType  && element.subjectTitle == array.subjectTitle) {
          students_evaluation = element
        }
      }
    });

    const dialogRef = this.dialog.open(FacultyEvaluationTableDetailsComponent, {
      width: '900px',
      disableClose: true,
      data:{selectedData:array,students_evaluation:students_evaluation}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }

  // showCodeEvalDetails(codeEvaluationSheet){
  //     const dialogRef = this.dialog.open(CodeEvalDetailsComponent, {
  //       width: '600px', disableClose: false,data:{
  //         evaluationSheetData:codeEvaluationSheet,
  //         subjectData:codeEvaluationSheet.subjectDetails,
  //         type:"computation"},
  //         });

  //         dialogRef.afterClosed().subscribe(result => {
  //           if (result==undefined) {
  //             // code...
  //           }else if (result.result=='deleted') {
  //             this.global.swalLoading('')
  //             // this.keyDownFunction('onoutfocus');
  //           }
  //         });

  // }
  //#endregion

  generateAQ(){
    var depCode = this.proglevel;
    var addQData = [];
    var facultyData = [];
    facultyData.push({
      name: this.facultyName,
      position: this.employee[0].position
    })
    if(this.proglevel == "Senior High School"){
      depCode = 'HSS'
    }else
      depCode = 'HS'

    this.api.getFacultyEvalAdditionalQuestions(depCode,this.id,0)
     .map(response=>response.json())
     .subscribe(res=>{
      if(res.data!=null){
        res.data.forEach(element => {
          addQData.push({
            "SECTION": element.section,
            "SUBJECT": element.subject,
            "ID NUMBER": element.studIDNumber,
            "STUDENT NAME": element.studName,
            "QUESTION": element.question,
            "RATE": element.additionalQuestionRate,
            "RATE DESC": element.rateDesc,
          });
        });

        this.generateAQExcel(addQData,facultyData);

      }else{

      }

     },Error=>{
       this.global.swalAlertError(Error);
     })
  }

  generateComment(){
    var facultyData = [];
    var commentDataLec = [];
    var commentDataLab = [];
    var temp = '';
    var counter = 0;
    var tempLab = '';
    var counterLab = 0;

    facultyData.push({
      name: this.facultyName,
      position: this.employee[0].position
    })

    this.arrayCommentsLec.forEach(element => {
      var classlec = '';

      if(element.class != temp){
        temp = element.class
        counter++;
        classlec =  "Class "+counter;
      }else{
        classlec =  "Class "+counter;
      }
      commentDataLec.push({
        class: classlec,
        comment: element.comment
      })
    });

    this.arrayCommentslab.forEach(element => {

      var classlab = '';

      if(element.class != temp){
        temp = element.class
        counter++;
        classlab =  "Class "+counter;
      }else{
        classlab =  "Class "+counter;
      }
      commentDataLab.push({
        class: classlab,
        comment: element.comment
      })
    });



    this.generateCommentsExcel(commentDataLec,commentDataLab,facultyData)
  }

  generatePointsSummary(){
    var SPointsData = [];
    var DevPointsData = [];
    var facultyData = [];
    var ImpPointsData = [];

    facultyData.push({
      name: this.facultyName,
      position: this.employee[0].position
    })

    SPointsData.push({
      arrayStrongPoints: pushArray(this.arrayStrongPoints),
      arrayStrongPointsLab: pushArray(this.arrayStrongPointsLab)
    });
    DevPointsData.push({
      arrayDevelopingPoints: pushArray(this.arrayDevelopingPoints),
      arrayDevelopingPointsLab: pushArray(this.arrayDevelopingPointsLab)
    });

    ImpPointsData.push({
      arrayPointsForImporvement: pushArray(this.arrayPointsForImporvement),
      arrayPointsForImporvementLab: pushArray(this.arrayPointsForImporvementLab)
    });

    function pushArray(array){
      var tempArray = [];
      array.forEach(element => {
        tempArray.push({
          'ITEM NO': element.itemNo,
          "QUESTION": element.question
        })
      });

      return tempArray;
    }

    this.generatePointsSummaryExcel(SPointsData,DevPointsData,ImpPointsData,facultyData);
  }

  async generatePointsSummaryExcel(SPointsData, DevPointsData, ImpPointsData, facultyData) {

    const header = ['ITEM NO', "QUESTION"];
    // Create workbook and worksheet
    var workbook = new ExcelJS.Workbook();

    const worksheet = workbook.addWorksheet();
    worksheet.name = 'STRONG POINTS';
    create_dev_points_sheet();
    create_imp_points_sheet();

    var FileName = "Strong points, Developing Points and Points for improvement Summary";
    let name = worksheet.addRow(['Faculty Name:', facultyData[0].name]);
    let position = worksheet.addRow(['Position:', facultyData[0].position]);

    name.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    position.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.addRow(['']);

    let title = worksheet.addRow(['Strong points, Developing Points and Points for improvement Summary']);
    title.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.addRow(['']);
    let SPoints = worksheet.addRow(['STRONG POINTS']);

    SPoints.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    let SPointsLec = worksheet.addRow(['Lecture']);
    SPointsLec.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    const headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
    });

    SPointsData[0].arrayStrongPoints.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
    })

    let SPointsLab = worksheet.addRow(['Laboratory']);
    SPointsLab.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    SPointsData[0].arrayStrongPointsLab.forEach((element) => {
      let eachRow = [];
      header.forEach((headers) => {
          eachRow.push(element[headers])
      })
      if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
          })
      } else {
          const row = worksheet.addRow(eachRow);
          row.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: {
                  argb: 'FFFFFFFF'
              }
          };
          row.font = {
              color: {
                  argb: '00000000',
              },
              bold: false
          }
          row.eachCell((cell, number) => {
              cell.border = {
                  top: {
                      style: 'thin'
                  },
                  left: {
                      style: 'thin'
                  },
                  bottom: {
                      style: 'thin'
                  },
                  right: {
                      style: 'thin'
                  }
              };
          });
          row.alignment={
            wrapText: true
          }
      }
    })
    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 150;



    workbook.xlsx.writeBuffer().then((data: any) => {
        const blob = new Blob([data], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        fs.saveAs(blob, FileName + '.xlsx');
    });

    function create_dev_points_sheet(){
      const worksheet1 = workbook.addWorksheet();
      worksheet1.name = 'DEVELOPING POINTS';

      let name1 = worksheet1.addRow(['Faculty Name:', facultyData[0].name]);
      let position1 = worksheet1.addRow(['Position:', facultyData[0].position]);

      name1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      position1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

      worksheet1.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
      worksheet1.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
      worksheet1.addRow(['']);

      let title1 = worksheet1.addRow(['Strong points, Developing Points and Points for improvement Summary']);
      title1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet1.addRow(['']);

      let SPoints1 = worksheet1.addRow(['DEVELOPING POINTS']);
      SPoints1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

      let SPointsLec1 = worksheet1.addRow(['Lecture']);
      SPointsLec1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet1.getCell('A1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

      const headerRow1 = worksheet1.addRow(header);
      headerRow1.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
      });

      DevPointsData[0].arrayDevelopingPoints.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet1.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet1.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
      })

      let SPointsLab1 = worksheet1.addRow(['Laboratory']);
      SPointsLab1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet1.getCell('A1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

      DevPointsData[0].arrayDevelopingPointsLab.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet1.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet1.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
      })

      worksheet1.getColumn(1).width = 20;
      worksheet1.getColumn(2).width = 150;


    }
    function create_imp_points_sheet(){
      const worksheet2 = workbook.addWorksheet();
      worksheet2.name = 'POINTS FOR IMPROVEMENT';

      let name2 = worksheet2.addRow(['Faculty Name:', facultyData[0].name]);
      let position2 = worksheet2.addRow(['Position:', facultyData[0].position]);

      name2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      position2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

      worksheet2.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
      worksheet2.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
      worksheet2.addRow(['']);

      let title2 = worksheet2.addRow(['Strong points, Developing Points and Points for improvement Summary']);
      title2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet2.addRow(['']);

      let SPoints2 = worksheet2.addRow(['POINTS FOR IMPROVEMENT']);
      SPoints2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

      let SPointsLec2 = worksheet2.addRow(['Lecture']);
      SPointsLec2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet2.getCell('A1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

      const headerRow2 = worksheet2.addRow(header);
      headerRow2.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
      });

      ImpPointsData[0].arrayPointsForImporvement.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet2.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet2.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
      })

      let SPointsLab2 = worksheet2.addRow(['Laboratory']);
      SPointsLab2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
      worksheet2.getCell('A1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

      ImpPointsData[0].arrayPointsForImporvementLab.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet2.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet2.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
      })

      worksheet2.getColumn(1).width = 20;
      worksheet2.getColumn(2).width = 150;
    }

  }

  async generateCommentsExcel(commentDataLec,commentDataLab,facultyData){

    const header = ['class', "comment"];
    // Create workbook and worksheet
    var workbook = new ExcelJS.Workbook();

    const worksheet = workbook.addWorksheet();
    worksheet.name = 'SUMMARY';

    var FileName = "Evaluation Comment Summary";
    let name = worksheet.addRow(['Faculty Name:', facultyData[0].name]);
    let position = worksheet.addRow(['Position:', facultyData[0].position]);

    name.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    position.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.addRow(['']);

    let SPoints = worksheet.addRow(['STRONG POINTS']);

    SPoints.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    let SPointsLec = worksheet.addRow(['Lecture']);
    SPointsLec.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    const headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
    });



    commentDataLec.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
    })

    let SPointsLab = worksheet.addRow(['Laboratory']);
    SPointsLab.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };




    commentDataLab.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
    })

    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 150;



    workbook.xlsx.writeBuffer().then((data: any) => {
        const blob = new Blob([data], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        fs.saveAs(blob, FileName + '.xlsx');
    });

  }

  async generateAQExcel(addQData, facultyData){
    const header = ['SECTION', "SUBJECT", "ID NUMBER", "STUDENT NAME", "QUESTION", "RATE", "RATE DESC"];
    // Create workbook and worksheet
    var workbook = new ExcelJS.Workbook();

    const worksheet = workbook.addWorksheet();
    worksheet.name = 'ADDITIONAL QUESTIONS';

    var FileName = "Evaluation Additional Questions Summary";
    let name = worksheet.addRow(['Faculty Name:', facultyData[0].name]);
    let position = worksheet.addRow(['Position:', facultyData[0].position]);

    name.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    position.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

    worksheet.getCell('B1').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.getCell('B2').font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
    worksheet.addRow(['']);


    const headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: {
                argb: 'FFFFFFFF'
            },
            bgColor: {
                argb: 'FFFFFFFF'
            },
        };
        cell.font = {
            color: {
                argb: '00000000',
            },
            bold: true
        }
        cell.border = {
            top: {
                style: 'thin'
            },
            left: {
                style: 'thin'
            },
            bottom: {
                style: 'thin'
            },
            right: {
                style: 'thin'
            }
        };
    });



    addQData.forEach((element) => {
        let eachRow = [];
        header.forEach((headers) => {
            eachRow.push(element[headers])
        })
        if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
                cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
        } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
            row.alignment={
              wrapText: true
            }
        }
    })

    worksheet.getColumn(1).width = 40;
    worksheet.getColumn(2).width = 70;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 100;
    worksheet.getColumn(6).width = 7;
    worksheet.getColumn(7).width = 12;



    workbook.xlsx.writeBuffer().then((data: any) => {
        const blob = new Blob([data], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        fs.saveAs(blob, FileName + '.xlsx');
    });
  }
}
