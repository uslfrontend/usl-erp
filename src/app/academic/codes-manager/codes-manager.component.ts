import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from './../../api.service';
import { GlobalService } from './../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddEditCodesComponent } from './add-edit-codes/add-edit-codes.component';
import { trigger, state, style, animate, transition } from '@angular/animations';


@Component({
  selector: 'app-codes-manager',
  templateUrl: './codes-manager.component.html',
  styleUrls: ['./codes-manager.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        'height': '*',
        'opacity': '1'
      })),
      transition('void => *', [
        style({
          'height': '0',
          'opacity': '0'
        }),
        animate('300ms ease-in')
      ]),
      transition('* => void', [
        animate('300ms ease-out', style({
          'height': '0',
          'opacity': '0'
        }))
      ])
    ])
  ]
})
export class CodesManagerComponent implements OnInit {
  // #region variables
  data = []
  appear = false
  codeString = ''
  decision = true
  codeStringSave = ''
  arraySave = []
  saved = ''
  componentState = ''
  unshift = 0
  // #endregion

  constructor(private api: ApiService, public global: GlobalService, public dialog: MatDialog) { }

  ngOnInit() {
    // this.codeString = 'H10'
    // const event = { keyCode: 13 }; // Simulating event with key code 13
    // this.keyDownFunction(event);
  }

  keyDownFunction(event) {
    this.unshift = 0
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      for (var x1 in this.data) {
        if (this.data[x1][0].codeNo == this.codeString) {
          this.data[x1].expand = true
          return
        }
      }
      this.api.getCodes(this.global.syear, this.codeString.toUpperCase()).map(response => response.json())
        .subscribe(res => {
          if (res.data.length == 0) {
            this.global.swalAlert("Code not found", "The code " + this.codeString + " you are trying to view does not exist in the database.", "warning")
            return
          }
          // course
          this.api.getCodeCourse(this.global.syear, this.codeString).map(response => response.json())
            .subscribe(res2 => {
              for (var x2 in res.data) {
                res.data[x2].course = res2.data.course
                res.data[x2].labUnits = res2.data.labUnits
                res.data[x2].lecUnits = res2.data.lecUnits
                res.data[x2].subjectCodeID = res2.data.subjectCodeID
                res.data[x2].subjectId = res2.data.subjectid
                res.data[x2].subjectTitle = res2.data.subjecttitle
                res.data[x2].units = res2.data.units
                res.data[x2].version = res2.data.version
              }
              this.unshift++
              if (this.unshift == 2) {
                this.data.unshift(res.data)
                this.data[0].expand = true
                // hide undo button
                this.codeStringSave = ""
                this.arraySave = []
                return
              }
            }, Error => {
              this.unshift++
              res.data[0].subjectTitle = 'No subject has been assigned to this code yet'
              if (this.unshift == 2) {
                this.data.unshift(res.data)
                this.data[0].expand = true
                // hide undo button
                this.codeStringSave = ""
                this.arraySave = []
                return
              }
            })

          // schedule
          this.api.getCodeSchedules(this.global.syear, this.codeString).map(response => response.json())
            .subscribe(res3 => {
              if (res3.data.length > 0) {
                //  parse data
                for (var x3 in res3.data) {
                  res.data[x3].day = this.parseDays(res3.data[x3].day)
                  res.data[x3].schedType = res3.data[x3].schedType
                  res.data[x3].recordID = res3.data[x3].recordID
                  res.data[x3].classSize = res3.data[x3].classSize;
                  res.data[x3].admitted = res3.data[x3].admitted;
                  res.data[x3].codeNo = res3.data[x3].codeNo;
                  res.data[x3].custom = res3.data[x3].custom;
                  res.data[x3].department = res3.data[x3].department;
                  res.data[x3].gender = res3.data[x3].gender;
                  res.data[x3].instructor = res3.data[x3].instructor;
                  res.data[x3].oe = res3.data[x3].oe;
                  res.data[x3].status = res3.data[x3].status;
                  res.data[x3].subjetCodeID = res3.data[x3].subjectCodeID;
                  res.data[x3].time = res3.data[x3].time;
                }
                this.unshift++
                if (this.unshift == 2) {
                  this.data.unshift(res.data)
                  this.data[0].expand = true
                  // hide undo button
                  this.codeStringSave = ""
                  this.arraySave = []
                  return
                }
              } else {
                this.unshift++
                if (this.unshift == 2) {
                  this.data.unshift(res.data)
                  this.data[0].expand = true
                  // hide undo button
                  this.codeStringSave = ""
                  this.arraySave = []
                  return
                }
              }
            }, Error => {
              this.global.swalAlertError()
            })

        }, Error => {
          this.global.swalAlertError()
        })
    }
  }

  refreshElement() {
    for (var index in this.data) {
      if (this.data[index][0].codeNo == this.codeString) {
        break
      }
    }
    this.unshift = 0
    this.api.getCodes(this.global.syear, this.codeString.toUpperCase()).map(response => response.json())
      .subscribe(res => {
        if (res.data.length == 0) {
          this.global.swalAlert("Code not found", "The code " + this.codeString + " you are trying to view does not exist in the database.", "warning")
          return
        }

        // course
        this.api.getCodeCourse(this.global.syear, this.codeString).map(response => response.json())
          .subscribe(res2 => {
            for (var x2 in res.data) {
              res.data[x2].course = res2.data.course
              res.data[x2].labUnits = res2.data.labUnits
              res.data[x2].lecUnits = res2.data.lecUnits
              res.data[x2].subjectCodeID = res2.data.subjectCodeID
              res.data[x2].subjectId = res2.data.subjectid
              res.data[x2].subjectTitle = res2.data.subjecttitle
              res.data[x2].units = res2.data.units
              res.data[x2].version = res2.data.version
            }
            this.unshift++
            if (this.unshift == 2) {
              this.data[index] = res.data
              this.data[index].expand = true
              // hide undo button
              this.codeStringSave = ""
              this.arraySave = []
              return
            }
          }, Error => {
            this.unshift++
            res.data[0].subjectTitle = 'No subject has been assigned to this code yet'
            if (this.unshift == 2) {
              this.data[index] = res.data
              this.data[index].expand = true
              // hide undo button
              this.codeStringSave = ""
              this.arraySave = []
              return
            }
          })

        // schedule
        this.api.getCodeSchedules(this.global.syear, this.codeString).map(response => response.json())
          .subscribe(res3 => {
            if (res3.data.length > 0) {
              //  parse data
              for (var x3 in res3.data) {
                res.data[x3].day = this.parseDays(res3.data[x3].day)
                res.data[x3].schedType = res3.data[x3].schedType
                res.data[x3].recordID = res3.data[x3].recordID
                res.data[x3].classSize = res3.data[x3].classSize;
                res.data[x3].admitted = res3.data[x3].admitted;
                res.data[x3].codeNo = res3.data[x3].codeNo;
                res.data[x3].custom = res3.data[x3].custom;
                res.data[x3].department = res3.data[x3].department;
                res.data[x3].gender = res3.data[x3].gender;
                res.data[x3].instructor = res3.data[x3].instructor;
                res.data[x3].oe = res3.data[x3].oe;
                res.data[x3].status = res3.data[x3].status;
                res.data[x3].subjetCodeID = res3.data[x3].subjectCodeID;
                res.data[x3].time = res3.data[x3].time;
              }
              this.unshift++
              if (this.unshift == 2) {
                this.data[index] = res.data
                this.data[index].expand = true
                // hide undo button
                this.codeStringSave = ""
                this.arraySave = []
                return
              }
            } else {
              this.unshift++
              if (this.unshift == 2) {
                this.data[index] = res.data
                this.data[index].expand = true
                // hide undo button
                this.codeStringSave = ""
                this.arraySave = []
                return
              }
            }
          }, Error => {
            this.global.swalAlertError()
          })

      }, Error => {
        this.global.swalAlertError()
      })
  }

  showCodeDetails(index, buttonElement: HTMLElement) {
    if (this.data[index].expand == true) {
      this.data[index].expand = false
    } else {
      this.data[index].expand = true
      if (buttonElement) {
        buttonElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
    }
  }

  parseDays(dayString) {
    var days = "";
    for (let i = 0; i < dayString.length; i++) {
      var day = "";
      if (dayString[i] === "M") {
        day = "1";
      } else if (dayString[i] === "T") {
        // Check for "TH" (Thursday) first
        if (i + 1 < dayString.length && dayString[i + 1] === "H") {
          day = "4";
          i++; // Skip the next character "H"
        } else {
          day = "2";
        }
      } else if (dayString[i] === "W") {
        day = "3";
      } else if (dayString[i] === "F") {
        day = "5";
      } else if (dayString[i] === "S") {
        // Check for "SU" (Sunday) first
        if (i + 1 < dayString.length && dayString[i + 1] === "U") {
          day = "7";
          i += 2; // Skip the next character "U and N"
        } else {
          day = "6";
          i += 2; // Skip the next character "A and T"
        }
      } else {
        day = dayString[i];
      }
      days += day;
    }
    return days;
  }

  getGradientColor(department: string): string {
    return '#294A70'
    switch (department) {
      case "SEAID":
        return "#660000"
      case "SBAA":
        return "#C79500";
      case "SEAS":
        return "#0000FF";
      case "SHS":
        return "#006633";
      case "GS":
        return "#077800";
      default:
        return "#660000";
    }
  }

  convertTimeFormat(timeString) {
    const [startTime, endTime] = timeString.split('-');
    const [startHour, startMinute] = startTime.split(':').map(num => parseInt(num));
    const [endHour, endMinute] = endTime.split(':').map(num => parseInt(num));
    const getAmPm = (hour) => hour >= 12 ? 'PM' : 'AM';
    const convertHour = (hour) => hour > 12 ? hour - 12 : hour === 0 ? 12 : hour;
    const startAmPm = getAmPm(startHour);
    const endAmPm = getAmPm(endHour);
    const convertedStartHour = convertHour(startHour);
    const convertedEndHour = convertHour(endHour);
    return `${convertedStartHour}:${startMinute.toString().padStart(2, '0')}${startAmPm}-${convertedEndHour}:${endMinute.toString().padStart(2, '0')}${endAmPm}`;
  }

  clearTable() {
    this.codeStringSave = this.codeString
    this.arraySave = this.data
    this.codeString = ''
    this.data = []
  }

  undo() {
    this.codeString = this.codeStringSave
    this.data = this.arraySave
    this.codeStringSave = ""
    this.arraySave = []
  }

  addEditCodes(action): void {
    // var area = this.areaName.slice(0, this.areaName.indexOf('[')-1)
    // var array = [area, this.areaHeadID, this.areaDeptID, this.areaDescription, this.areaID]
    const dialogRef = this.dialog.open(AddEditCodesComponent, {
      width: '700px', disableClose: true, data: action
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'codeAdded') {
        this.codeString = result.codeString
        const event = { keyCode: 13 }; // Simulating event with key code 13
        this.keyDownFunction(event)
      } else if (result.result == 'scheduleAdded') {
        this.refreshElement()
      }else if (result.result == 'scheduleEdited') {
        this.refreshElement()
      }
    });
  }

  editSchedule(data): void {
    const dialogRef = this.dialog.open(AddEditCodesComponent, {
      width: '700px', disableClose: true, data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'codeAdded') {
        this.codeString = result.codeString
        const event = { keyCode: 13 }; // Simulating event with key code 13
        this.keyDownFunction(event)
      } else if (result.result == 'deleteSchedule') {
        this.codeString = result.codeString
        this.refreshElement()
      } else if (result.result == 'scheduleEdited') {
        this.codeString = result.codeString
        this.refreshElement()
      }
    });
  }

  hideCodeFromView(index) {
    this.data.splice(index, 1);
  }

  isString(param) {
    return typeof param === 'string';
  }


}
