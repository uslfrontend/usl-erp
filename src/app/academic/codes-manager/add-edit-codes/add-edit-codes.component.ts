/**************************************
 *                                    *
 *  Author: [Kurt Christian Lopez]    *
 *  Date: [May - July 2023]           *
 *  Description: [Codes Manager}      *
 *                                    *
 **************************************/


// Fold: ctrl + K & ctrl + 0
// Unfold: ctrl + K & ctrl + J
import { Component, OnInit, HostListener, ChangeDetectorRef, Renderer2, } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { LookUpCurriculumComponent } from './../../curriculum/look-up-curriculum/look-up-curriculum.component';
import { SubjectsComponent } from './../../../academic/curriculum/subjects/subjects.component';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { PersonLookupComponent } from '../../lookup/person-lookup/person-lookup.component';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ClassroomManagerComponent } from '../../maintenance/classroom-manager/classroom-manager.component';
import Swal from 'sweetalert2';

// #region Zoom Control Class
@Component({
  selector: 'app-zoom',
  template: `
    <button (click)="zoomIn()">Zoom In</button>
    <button (click)="zoomOut()">Zoom Out</button>
  `,
})

// export class ZoomComponent {
//   constructor(private renderer: Renderer2) { }

//   zoomIn() {
//     this.renderer.setStyle(document.documentElement, 'zoom', '150%');
//   }

//   zoomOut() {
//     this.renderer.setStyle(document.documentElement, 'zoom', '75%');
//   }
// }
// #endregion

@Component({
  selector: 'app-add-edit-codes',
  templateUrl: './add-edit-codes.component.html',
  styleUrls: ['./add-edit-codes.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 }))
      ])
    ]),
    trigger('slideInOut', [
      state('in', style({
        'height': '*',
        'opacity': '1'
      })),
      transition('void => *', [
        style({
          'height': '0',
          'opacity': '0'
        }),
        animate('300ms ease-in')
      ]),
      transition('* => void', [
        animate('300ms ease-out', style({
          'height': '0',
          'opacity': '0'
        }))
      ])
    ])
  ]
})

export class AddEditCodesComponent implements OnInit {
  codeSeries = []
  codeString = ''
  lecUnits = 0
  labUnits = 0
  gender = ''
  classSize = 0
  action = this.data
  bothDone = 0
  swal = Swal;

  // #region Validation Validation Variables
  completeForm = false
  finalSchedules = []
  conflictSchedules = []
  hideScheduleConflictView = true
  timeDayValidation = false
  // #endregion

  // #region Add Course Variables
  typing = false
  searchData = []
  SavesArrayForsearchDataProgramFilter = []
  CO = true
  checkbox = document.getElementById('checkbox');
  selectedCourse = []
  disableCourseInput = false
  disableCheckboxForCO = false
  saveCourseString = ''
  seconds: any = null
  interval
  searchApiResponse = []
  debounceTimer: any;
  loadingSearch = false
  // #endregion

  // #region Add Room Variables
  roomString = ''
  lecture = false
  laboratory = false
  rooms = []
  selectedRoom = []
  capacity: number
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  // #endregion

  // #region Add Time Schedule Variables
  hour1 = '00'
  minute1 = '00'
  meridiem1 = 'AM'
  hour2 = '00'
  minute2 = '00'
  meridiem2 = 'AM'
  courseString = ''
  isSearchEmpty = false
  startTime = ''
  endTime = ''
  duration = ''
  // #endregion

  // #region Add Day Variables
  hideCustomDays = false
  monday = false
  tuesday = false
  wednesday = false
  thursday = false
  friday = false
  saturday = false
  sunday = false
  conflicts = {
    monday: false,
    tuesday: false,
    wednesday: false,
    thursday: false,
    friday: false,
    saturday: false,
    sunday: false
  }
  // #endregion

  // #region Add Instructor Variables
  filteredInstructors: Observable<string[]>;
  instructorString = ''
  instructors = []
  selectedInstructor = []
  // #endregion

  constructor(public dialogRef: MatDialogRef<AddEditCodesComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService, private cdr: ChangeDetectorRef, private renderer: Renderer2) { }

  // #region General Functions
  ngOnInit() {
    if (typeof this.action === "string") {
      if (this.action == 'Add Instructor') {
        this.getInstructors()
        return
      }
      if (!this.action.toLowerCase().includes('schedule'))
        this.getCodeSeries()
      else {
        this.codeString = this.action.slice(-3);
      }
      this.getClassrooms()
    } else {
      this.codeString = this.action.codeString
      this.getClassroomsFromEdit()
    }
  }

  getCodeSeries() {
    this.api.getCodeSeries(this.global.syear).map(response => response.json())
      .subscribe(res => {
        this.codeSeries = res.data
        for (var x in this.codeSeries) {
          if (!this.codeSeries[x].lastCodeNo.includes('99')) {
            this.codeString = this.incrementString(this.codeSeries[x].lastCodeNo)
            break
          }
        }
      })
  }

  incrementString(str) {
    // Extract the non-numeric and numeric parts of the string
    const nonNumeric = str.match(/[^\d]+/)[0];
    const numeric = str.match(/\d+/) ? parseInt(str.match(/\d+/)[0]) : 0;
    // Increment the numeric part
    const incrementedNumeric = numeric + 1;
    // Combine the non-numeric and incremented numeric parts
    return `${nonNumeric}${incrementedNumeric}`;
  }

  addCode() {
    if (this.codeString != null && this.codeString.length == 3) {
      // add code
      this.api.postCode({
        "codeNo": this.codeString,
        "classSize": this.classSize,
        "schoolYear": this.global.syear,
        "sections": 0,
        "description": "",
        "gender": this.gender
      }).map(response => response.json()).subscribe(res => {
        this.addCourse()
        this.addSchedule()
      })
    } else {
      this.global.swalAlert('Warning', 'Invalid Code Format', 'warning')
    }
  }

  addCourse() {
    if (this.selectedCourse.length > 0) {
      // @ts-ignore
      this.api.assignCourseToCode(this.codeString, this.selectedCourse[0][5], this.global.syear).map(response => response.json()).subscribe(res => {
        this.bothDone++
        if (this.bothDone == 2) {
          this.global.swalSuccess('Success')
          this.close('codeAdded')
        }
      })
    } else {
      this.bothDone++
      if (this.bothDone == 2) {
        this.global.swalSuccess('Success')
        this.close('codeAdded')
      }
    }
  }

  addSchedule() {
    if (this.finalSchedules.length > 0) {
      for (var x in this.finalSchedules) {
        var type = 0
        if (this.finalSchedules[x].lab) {
          type = 1
        }
        this.api.assignScheduleToCode({
          "codeno": this.codeString,
          "room": this.finalSchedules[x].room,
          "time": this.finalSchedules[x].startTime + '-' + this.finalSchedules[x].endTime,
          "day": this.finalSchedules[x].dayString,
          // @ts-ignore
          "instructor": this.selectedInstructor.employeeID,
          "schoolyear": this.global.syear,
          "type": type
        }).map(response => response.json()).subscribe(res => {
          // force this algorithm to return and save the code for refreshElement if this form is called from the main view
          if (this.action.includes('Add Schedule for')) {
            return
          }
          this.bothDone++
          if (this.bothDone == 2) {
            this.global.swalSuccess('Success')
            this.close('codeAdded')
          }
        })
      }
    } else {
      this.bothDone++
      if (this.bothDone == 2) {
        this.global.swalSuccess('Success')
        this.close('codeAdded')
      }
    }
  }

  checkIfActionIsString() {
    if (typeof this.action === "string")
      return true
  }

  displayAddScheduleForm() {
    if (typeof this.action === "string") {
      if (this.action.toLowerCase().includes('schedule')) {
        return true
      }
    } else {
      return true
    }
    return false
  }

  deleteSchedule() {
    this.swal.fire({
      title: "Are you sure you want to delete this schedule?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Remove'
    }).then((result) => {
      if (result.value) {
        this.api.deleteCourseSchedule(this.action.recordID, this.global.syear).map(response => response.json()).subscribe(res => {
          this.global.swalSuccess('Schedule Sucessfully Deleted')
          this.close('deleteSchedule')
        })
      }
    })



  }

  editSchedule() {
    // @ts-ignore
    this.api.editCourseSchedule(this.global.syear, {
      "recordid": this.action.recordID,
      // @ts-ignore
      "room": this.selectedRoom.roomNo,
      "time": this.startTime + '-' + this.endTime,
      "day": this.generateDayString(),
      "instructor": "",
      "type": 0
    }).map(response => response.json()).subscribe(res => {
      this.global.swalSuccess('Success')
      this.close('scheduleEdited')
    })
  }

  zoomIn() {
    this.renderer.setStyle(document.documentElement, 'zoom', '150%');
  }

  zoomOut() {
    this.renderer.setStyle(document.documentElement, 'zoom', '75%');
  }
  // #endregion

  // #region Validation Fucntions
  checkConflict() {
    if (this.startTime != '' && this.endTime != null) {
      if (this.duration != '' && !this.duration.includes('Invalid')) {
        if (this.monday === true) {
          this.getSchedConflict('M');
        }
        if (this.tuesday === true) {
          this.getSchedConflict('T');
        }
        if (this.wednesday === true) {
          this.getSchedConflict('W');
        }
        if (this.thursday === true) {
          this.getSchedConflict('TH');
        }
        if (this.friday === true) {
          this.getSchedConflict('F');
        }
        if (this.saturday === true) {
          this.getSchedConflict('SAT');
        }
        if (this.sunday === true) {
          this.getSchedConflict('SUN');
        }
      }
    }
  }

  getSchedConflict(day) {
    this.api.getSchedConflict("0", "2022231", {
      "day": day,
      "time": this.startTime + "-" + this.endTime,
      // @ts-ignore
      "room": this.selectedRoom.roomNo,
      "codeno": "",
      "idnumber": ""
    }).map(response => response.json()).subscribe(res => {
      if (res.data.length > 0) {
        this.timeDayValidation = false
        for (var x in res.data) {
          if (this.codeString != res.data[x].codeno)
            this.conflictSchedules.push(res.data[x])
        }
      } else {
        if (this.conflictSchedules.length == 0)
          this.timeDayValidation = true
      }
    })
  }

  hideUnhideScheduleConflict() {
    this.hideScheduleConflictView = !this.hideScheduleConflictView
  }

  isTimeframeConflict(start1, end1, start2, end2, dayString1, dayString2) {
    dayString1 = this.separateDays(dayString1);
    dayString2 = this.separateDays(dayString2);
    for (let i = 0; i < dayString1.length; i++) {
      if (dayString2.includes(dayString1[i])) {
        const [sh1, sm1] = start1.split(':');
        const start1Seconds = parseInt(sh1) * 60 + parseInt(sm1);
        const [eh1, em1] = end1.split(':');
        const end1Seconds = parseInt(eh1) * 60 + parseInt(em1);
        const [sh2, sm2] = start2.split(':');
        const start2Seconds = parseInt(sh2) * 60 + parseInt(sm2);
        const [eh2, em2] = end2.split(':');
        const end2Seconds = parseInt(eh2) * 60 + parseInt(em2);
        if (start2Seconds <= start1Seconds && start1Seconds <= end2Seconds) {
          return true;
        }
      }
    }
    return false;
  }

  addToFinalSchedule() {
    var dayString = ''
    if (this.monday === true) {
      dayString += 'M';
    }
    if (this.tuesday === true) {
      dayString += 'T';
    }
    if (this.wednesday === true) {
      dayString += 'W';
    }
    if (this.thursday === true) {
      dayString += 'TH';
    }
    if (this.friday === true) {
      dayString += 'F';
    }
    if (this.saturday === true) {
      dayString += 'SAT';
    }
    if (this.sunday === true) {
      dayString += 'SUN';
    }
    // @ts-ignore
    this.finalSchedules.push({ room: this.selectedRoom.roomNo, building: this.selectedRoom.location, capacity: this.capacity, startTime: this.startTime, endTime: this.endTime, lec: this.lecture, lab: this.laboratory, dayString: dayString })
    this.clearFields()
  }

  clearFields() {
    this.monday = false;
    this.tuesday = false;
    this.wednesday = false;
    this.thursday = false;
    this.friday = false;
    this.saturday = false;
    this.sunday = false;
    // this.capacity = null;
    this.hour1 = '00'
    this.minute1 = '00'
    this.meridiem1 = 'AM'
    this.startTime = '';
    this.hour2 = '00'
    this.minute2 = '00'
    this.meridiem2 = 'AM'
    this.endTime = '';
    this.completeForm = false
    this.timeDayValidation = false
  }

  checkIfFormIsComplete() {
    this.validateTime()
    // #region dayString
    var dayString = this.generateDayString()
    // #endregion
    this.conflictSchedules = []
    this.completeForm = false
    var complete = true
    this.timeDayValidation = false
    // room validation
    if (this.selectedRoom.length == 0) {
      complete = false
    }
    // time validation
    if (this.hour1 == '00' && this.hour2 == '00') {
      complete = false
    }
    // day validation
    if (this.monday == false && this.tuesday == false && this.wednesday == false && this.thursday == false && this.friday == false && this.saturday == false && this.sunday == false) {
      complete = false
    }

    this.checkConflict()
    if (this.conflictSchedules.length != 0) {
      complete = false
    }
    // classification validation
    if (this.lecture == false && this.laboratory == false) {
      complete = false
    }
    // selected schedules validation
    if (this.finalSchedules.length > 0) {
      for (var x in this.finalSchedules) {
        if (this.isTimeframeConflict(this.startTime, this.endTime, this.finalSchedules[x].startTime, this.finalSchedules[x].endTime, dayString, this.finalSchedules[x].dayString)) {
          this.duration = "Invalid: This schedule is in conflict with a schedule in the selected timeframe."
          complete = false
          return
        } else {
          this.duration = this.calculateDuration(this.startTime, this.endTime)
        }
      }
    }
    if (complete == true) {
      this.completeForm = true
    }
  }

  // #endregion

  // #region Course Fucntions
  removeSelectedCourse() {
    this.selectedCourse = []
    this.courseString = this.saveCourseString;
    this.typing = true
    this.disableCourseInput = false
  }

  selectCourse(courseId, courseTitle, program, version, status, courseRecordId) {
    this.disableCourseInput = true
    this.saveCourseString = this.courseString
    this.courseString = ''
    this.typing = false
    this.selectedCourse.push([courseId, courseTitle, program, version, status, courseRecordId])
  }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    const clickedElement = event.target as HTMLElement;
    const divElement = document.getElementById('your-div-id');

    if (divElement && !divElement.contains(clickedElement)) {
      this.typing = false
    }
  }

  onCheckboxChange() {
    this.CO = !this.CO
    this.searchData = []
    if (this.courseString.length > 2)
      this.keyUpFunctionForCourse(event)
  }

  keyDownFunctionForCode(event) {
    this.codeString = event.target.value.toUpperCase();
    if (event.keyCode == 8 && this.codeString.includes("FULL")) {
      this.codeString = ''
    }
    if (this.codeString.length == 1) {
      if (event.keyCode != 8) {
        var seriesStart = false
        for (var x in this.codeSeries) {
          if (this.codeString == this.codeSeries[x].seriesStart) {
            seriesStart = true
            if (this.codeSeries[x].lastCodeNo.includes('99')) {
              this.codeString = this.codeSeries[x].lastCodeNo + " - SERIES FULL!"
            } else {
              this.codeString = this.incrementString(this.codeSeries[x].lastCodeNo)
            }
          }
        }
        if (seriesStart == false) {
          this.codeString = this.codeString + '01'
        }
      }
    }
  }

  startCounting() {
    this.seconds = 0;
    this.interval = setInterval(() => {
      this.seconds += 0.01;
      this.seconds = Math.round(this.seconds * 1000) / 1000; // Round to 3 decimal places
    }, 10);
  }

  keyUpFunctionForCourse(event) {
    // loading animation for searching
    this.loadingSearch = true
    // Set the 'isSearchEmpty' variable to false since the API has not been executed yet.
    this.isSearchEmpty = false
    // Wait 1 second before proceeding
    clearTimeout(this.debounceTimer);
    this.debounceTimer = setTimeout(() => {
      // seconds
      clearInterval(this.interval)
      this.startCounting()
      //handle escape input
      if (event.keyCode === 27) {
        this.typing = false;
        this.loadingSearch = false
        return
      }
      // validation to call api only if the input string is above 2 characters
      if (this.courseString.length > 2) {
        // transfer the value of courseString to stringToSearch for API search. This will isolate courseString for the program filter
        var stringToSearch = this.courseString
        var index = stringToSearch.indexOf('*');
        if (index !== -1) {
          stringToSearch = stringToSearch.slice(0, index);
        }
        // validation to stop api search if the input is askterisk
        if (!this.courseString.includes('*')) {
          // enable checkbox for CO filter first. this is to allow the user to filter the course state when not filtering by program
          this.disableCheckboxForCO = false
          // Code to start waiting until API execution is done
          // API
          this.api.subjectLookupById(stringToSearch).map(response => response.json())
            .subscribe(res => {
              this.searchApiResponse = []
              for (var x in res.data) {
                if (this.CO == true) {
                  if (res.data[x].programStatus == 'CO')
                    this.searchApiResponse.push(res.data[x])
                }
                else
                  this.searchApiResponse.push(res.data[x])
              }
              this.api.subjectLookupByTitle(stringToSearch).map(response => response.json())
                .subscribe(res => {
                  for (var x in res.data) {
                    var shouldPush = true;
                    // Check if the response array already contains an object with the same recordID
                    for (var i = 0; i < this.searchApiResponse.length; i++) {
                      if (this.searchApiResponse[i].recordID === res.data[x].recordID) {
                        shouldPush = false;
                        break;
                      }
                    }
                    if (shouldPush) {
                      if (this.CO == true) {
                        if (res.data[x].programStatus == 'CO') {
                          this.searchApiResponse.push(res.data[x]);
                        }
                      } else {
                        this.searchApiResponse.push(res.data[x]);
                      }
                    }
                  }
                  this.searchData = this.searchApiResponse
                  // this code is used to make sure the table displays the updated data
                  this.cdr.detectChanges();
                  // search by program
                  this.api.postCurriculumLookup({
                    "programId": '',
                    "courseCode": stringToSearch,
                    "programTitle": '',
                    "major": '',
                    "version": '',
                    "departmentId": '',
                    "departmentCode": '',
                    "departmentName": '',
                    "departmentGroup": '',
                    "statusCode": '',
                    "exactMatch": false
                  }).map(response => response.json()).subscribe(res3 => {
                    try {
                      res3.data[0].programID
                      this.programSearch(res3.data, this.searchApiResponse)
                    } catch {
                      clearInterval(this.interval);
                      if (this.searchData.length == 0) {
                        this.isSearchEmpty = true
                      }
                      this.loadingSearch = false
                      return
                    }
                  })
                });
            });
        } else {
          // stop loadingSearch if the last character is askterisk
          if (this.courseString.substring(this.courseString.length - 1) === '*') {
            this.loadingSearch = false;
          }
          // disable checkbox for CO filter first. this is to avoid the user filtering the course state when filtering by program
          this.disableCheckboxForCO = true
          // save the searchData array to the SavesArrayForsearchDataProgramFilter
          if (this.SavesArrayForsearchDataProgramFilter.length == 0) {
            this.SavesArrayForsearchDataProgramFilter = this.searchData
          }
          // get the string after the askterisk and put it to a variable called toSearch variable
          var index2 = this.courseString.indexOf('*');
          var toSearch = '';
          if (index2 !== -1) {
            toSearch = this.courseString.substring(index2 + 1); // or variable = str.slice(index);
          }
          // after askterisk filter
          if (toSearch.length > 0) {
            this.searchData = []
            for (var x in this.SavesArrayForsearchDataProgramFilter) {
              if (this.SavesArrayForsearchDataProgramFilter[x].courseCode.toLowerCase().includes(toSearch.toLowerCase())) {
                this.searchData.push(this.SavesArrayForsearchDataProgramFilter[x])
              }
              if (this.SavesArrayForsearchDataProgramFilter[x].subjectTitle.toLowerCase().includes(toSearch.toLowerCase())) {
                this.searchData.push(this.SavesArrayForsearchDataProgramFilter[x])
              }
              if (this.SavesArrayForsearchDataProgramFilter[x].subjectID.toLowerCase().includes(toSearch.toLowerCase())) {
                this.searchData.push(this.SavesArrayForsearchDataProgramFilter[x])
              }
            }
            if (this.searchData.length == 0) {
              this.isSearchEmpty = true
            }
            this.loadingSearch = false
          }
          clearInterval(this.interval);
        }
      } else {
        this.searchData = []
        this.SavesArrayForsearchDataProgramFilter = []
        this.disableCheckboxForCO = false
        this.cdr.detectChanges();
      }
    }, 500);
  }

  async programSearch(programs, previousAPIResponse) {
    for (var x in programs) {
      var curriculum
      var courses
      try {
        const res = await this.api.getCurriculum(programs[x].programId).toPromise();
        curriculum = res.json();
        if (this.CO == true) {
          if (curriculum.data.statusCode == 'CO') {
            try {
              const res = await this.api.getCurriculumSubjects(programs[x].programId).toPromise();
              courses = res.json();
              for (var y in courses.data) {
                var recordId = courses.data[y].recordId;
                // Check if the recordID already exists in searchData
                var isDuplicate = previousAPIResponse.some(item => item.recordID === recordId);
                // If it's not a duplicate, push the data
                if (!isDuplicate) {
                  previousAPIResponse.push({
                    subjectID: courses.data[y].subjectId,
                    subjectTitle: courses.data[y].subjectTitle,
                    courseCode: curriculum.data.courseCode,
                    version: curriculum.data.version,
                    programStatus: curriculum.data.statusCode,
                    recordID: recordId
                  });
                }
              }
              // this code is used to make sure the table displays the updated
              this.searchData = previousAPIResponse
              if (this.searchData.length == 0) {
                this.isSearchEmpty = true
              }
              this.loadingSearch = false
              this.cdr.detectChanges();
            } catch (error) {
            }
          }
        } else {
          try {
            const res = await this.api.getCurriculumSubjects(programs[x].programId).toPromise();
            courses = res.json();
            for (var y in courses.data) {
              var recordId = courses.data[y].recordId;
              // Check if the recordID already exists in searchData
              var isDuplicate = previousAPIResponse.some(item => item.recordID === recordId);
              // If it's not a duplicate, push the data
              if (!isDuplicate) {
                previousAPIResponse.push({
                  subjectID: courses.data[y].subjectId,
                  subjectTitle: courses.data[y].subjectTitle,
                  courseCode: curriculum.data.courseCode,
                  version: curriculum.data.version,
                  programStatus: curriculum.data.statusCode,
                  recordID: recordId
                });
              }
            }
            // this code is used to make sure the table displays the updated data
            this.searchData = previousAPIResponse
            if (this.searchData.length == 0) {
              this.isSearchEmpty = true
            }
            this.loadingSearch = false
            this.cdr.detectChanges();
          } catch (error) {
          }
        }
      } catch (error) {
      }
    }
    clearInterval(this.interval);
  }

  onInput(event: any) {
    this.typing = true;
  }

  // #endregion

  // #region Room Fucntions
  getClassrooms() {
    this.api.getClassroomList().map(response => response.json())
      .subscribe(res => {
        this.rooms = res.data
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value.toString()))
          );
      })
  }

  getClassroomsFromEdit() {
    this.api.getClassroomList().map(response => response.json())
      .subscribe(res => {
        this.rooms = res.data

        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value.toString()))
          );

        for (var x in this.rooms) {
          if (this.action.room == this.rooms[x].roomNo) {
            this.selectedRoom = this.rooms[x]
            // @ts-ignore
            this.capacity = this.selectedRoom.seatingCapacity
            // @ts-ignore
            if (this.selectedRoom.type == "09") {
              this.lecture = true
              this.laboratory = false
            } else {
              this.laboratory = true
              this.lecture = false
            }
            break
          }
        }
        const [startTime, endTime] = this.action.time.split('-')
        this.hour1 = startTime.substring(0, 2)
        this.minute1 = startTime.substring(3, 5)
        this.meridiem1 = startTime.substring(5)
        this.hour2 = endTime.substring(0, 2)
        this.minute2 = endTime.substring(3, 5)
        this.meridiem2 = endTime.substring(5)

        if (this.action.dayString.includes('1')) {
          this.monday = true
        } if (this.action.dayString.includes('2')) {
          this.tuesday = true
        } if (this.action.dayString.includes('3')) {
          this.wednesday = true
        } if (this.action.dayString.includes('4')) {
          this.thursday = true
        } if (this.action.dayString.includes('5')) {
          this.friday = true
        } if (this.action.dayString.includes('6')) {
          this.saturday = true
        } if (this.action.dayString.includes('7')) {
          this.sunday = true
        }
        this.checkIfFormIsComplete()
      })
  }

  // @ts-ignore
  private _filter(value: string): Room[] { // Assuming the type of `option` is `Room`
    const filterValue = value.toLowerCase();
    return this.rooms
      .flat() // Flatten the multidimensional array
      .filter(option => option.roomNo.toLowerCase().includes(filterValue));
  }

  selectRoom(room) {
    this.roomString = ''
    this.selectedRoom = room
    this.capacity = room.seatingCapacity
    if (room.type == "09") {
      this.lecture = true
      this.laboratory = false

    } else {
      this.laboratory = true
      this.lecture = false
    }
    this.checkIfFormIsComplete()
  }

  removeSelectedRoom() {
    this.selectedRoom = []
    this.capacity = null

    const dummyElement = document.createElement('button');
    dummyElement.style.position = 'absolute';
    dummyElement.style.opacity = '0';
    document.body.appendChild(dummyElement);
    dummyElement.focus();
    document.body.removeChild(dummyElement);

    // const inputElement = document.getElementById('matFieldInput') as HTMLInputElement;
    // inputElement.blur();
    // if (inputElement) {
    //   inputElement.value = '';
    //   inputElement.style.display = 'block';
    // }
    this.laboratory = false
    this.lecture = false
    this.clearFields()
    this.finalSchedules = []
    this.timeDayValidation = false

  }

  mouseWheel(event: WheelEvent) {
  }

  removeRoomFromTimeFrame(index) {
    this.finalSchedules.splice(index, 1);
  }

  // #endregion

  // #region Time Fucntions
  keyDownFunctionForHour(event: KeyboardEvent, type) {
    if (event.key === 'Backspace') {
      event.preventDefault(); // Disable Backspace key input
      if (type === '1') {
        this.hour1 = '00';
      } else {
        this.hour2 = '00';
      }
    } else {
      if (type == '1') {
        if (event.key === 'ArrowUp') {
          this.incrementHour('1');
        } else if (event.key === 'ArrowDown') {
          this.decrementHour('1');
        } else {
          const digit = parseInt(event.key);
          if (this.hour1 !== '01' && !isNaN(digit) && digit >= 0 && digit <= 9) {
            this.hour1 = '0' + digit;
            this.hour2 = this.hour1
            this.incrementHour('2')
          } else if (this.hour1 === '01' && !isNaN(digit) && (digit === 0 || digit === 1 || digit === 2)) {
            this.hour1 = '1' + digit;
            this.hour2 = this.hour1
            this.incrementHour('2')
          } else if (!isNaN(digit)) {
            this.hour1 = '0' + digit;
            this.hour2 = this.hour1
            this.incrementHour('2')
          }
        }
      } else {
        if (event.key === 'ArrowUp') {
          this.incrementHour('2');
        } else if (event.key === 'ArrowDown') {
          this.decrementHour('2');
        } else {
          const digit = parseInt(event.key);
          if (this.hour2 !== '01' && !isNaN(digit) && digit >= 0 && digit <= 9) {
            this.hour2 = '0' + digit;
          } else if (this.hour2 === '01' && !isNaN(digit) && (digit === 0 || digit === 1 || digit === 2)) {
            this.hour2 = '1' + digit;
          } else if (!isNaN(digit)) {
            this.hour1 = '0' + digit;
            this.hour2 = '0' + (digit + 1);
          }

          // ensure that hour1 is always 1 hour behind hour2
          if (this.hour1 == this.hour2) {
            this.decrementHour('1')
          } else if (parseInt(this.hour2) < parseInt(this.hour1) && parseInt(this.hour2) != 12) {
            this.meridiem2 = (this.meridiem1 === "AM") ? "PM" : "AM";
          } else {
            this.meridiem2 = this.meridiem1
          }
        }
      }
    }
    this.checkIfFormIsComplete()
  }

  incrementHour(type) {
    let hourValue = parseInt(type === '1' ? this.hour1 : this.hour2);
    hourValue = (hourValue % 12) + 1;
    let hourValue2 = parseInt(type === '1' ? this.hour1 : this.hour2);
    hourValue2 = (hourValue % 12) + 1;
    if (type === '1') {
      this.hour1 = hourValue.toString().padStart(2, '0');
      this.meridiem1 = hourValue === 12 ? (this.meridiem1 === 'AM' ? 'PM' : 'AM') : this.meridiem1;
      // ensure that hour2 is always 1 hour ahead of hour1
      if (parseInt(this.hour2) - parseInt(this.hour1) == 0 && this.meridiem1 == this.meridiem2) {
        this.hour2 = hourValue2.toString().padStart(2, '0');
        this.meridiem2 = hourValue2 === 12 ? (this.meridiem2 === 'AM' ? 'PM' : 'AM') : this.meridiem2;
      } else if (parseInt(this.hour2) - parseInt(this.hour1) == -1 && this.meridiem1 == this.meridiem2) {
        this.hour2 = this.hour1
        this.hour2 = hourValue2.toString().padStart(2, '0');
      }
    } else {
      this.hour2 = hourValue.toString().padStart(2, '0');
      this.meridiem2 = hourValue === 12 ? (this.meridiem2 === 'AM' ? 'PM' : 'AM') : this.meridiem2;
    }
    this.checkIfFormIsComplete()
  }

  decrementHour(type) {
    let hourValue = parseInt(type === '1' ? this.hour1 : this.hour2);
    hourValue = (hourValue - 2 + 12) % 12 + 1;
    let hourValue2 = parseInt(type === '1' ? this.hour1 : this.hour2);
    hourValue2 = (hourValue % 12) - 1;
    if (type === '1') {
      this.hour1 = hourValue.toString().padStart(2, '0');
      this.meridiem1 = hourValue === 11 ? (this.meridiem1 === 'AM' ? 'PM' : 'AM') : this.meridiem1;
    } else {
      this.hour2 = hourValue.toString().padStart(2, '0');
      this.meridiem2 = hourValue === 11 ? (this.meridiem2 === 'AM' ? 'PM' : 'AM') : this.meridiem2;
      // Ensure that hour1 is always behind hour2
      if (parseInt(this.hour2) - parseInt(this.hour1) == 0 && this.meridiem1 == this.meridiem2) {
        if (parseInt(this.hour2) == 1) {
          this.hour1 = '12'
          this.meridiem1 = (this.meridiem1 === "AM") ? "PM" : "AM";
          return
        } else if (parseInt(this.hour2) == 12) {
          this.hour1 = '11'
          return
        } else {
          this.hour1 = hourValue2.toString().padStart(2, '0');
          this.meridiem1 = hourValue2 === 12 ? (this.meridiem1 === 'AM' ? 'PM' : 'AM') : this.meridiem1;
        }
      }
    }
    this.checkIfFormIsComplete()
  }

  handleWheelHourInput(event: WheelEvent, type): void {
    // Handle mouse scroll event here
    if (type == '1') {
      if (event.deltaY < 0) {
        this.incrementHour('1')
      } else if (event.deltaY > 0) {
        this.decrementHour('1')
      }
    } else {
      if (event.deltaY < 0) {
        this.incrementHour('2')
      } else if (event.deltaY > 0) {
        this.decrementHour('2')
      }
    }
    this.checkIfFormIsComplete()
  }

  timeToggle(mode) {
    switch (mode) {
      case '1':
        this.minute1 = (this.minute1 === '00') ? '30' : '00';
        break
      case '2':
        this.meridiem1 = (this.meridiem1 === 'AM') ? 'PM' : 'AM';
        if (this.hour1 > this.hour2) {
          this.meridiem2 = (this.meridiem1 === 'AM') ? 'PM' : 'AM';
        }
        break
      case '3':
        this.minute2 = (this.minute2 === '00') ? '30' : '00';
        break
      case '4':
        this.meridiem2 = (this.meridiem2 === 'AM') ? 'PM' : 'AM';
        if (this.hour1 > this.hour2) {
          this.meridiem1 = (this.meridiem2 === 'AM') ? 'PM' : 'AM';
        }
        break
    }
    this.checkIfFormIsComplete()
  }

  validateTime() {
    this.startTime = this.convertTimeToMilitary(this.hour1 + ':' + this.minute1 + this.meridiem1)
    this.endTime = this.convertTimeToMilitary(this.hour2 + ':' + this.minute2 + this.meridiem2)
    this.duration = this.calculateDuration(this.startTime, this.endTime)
  }

  calculateDuration(startTime, endTime) {
    const start = startTime.split(':');
    const end = endTime.split(':');
    const startHours = parseInt(start[0]);
    const startMinutes = parseInt(start[1]);
    var endHours = parseInt(end[0]);
    const endMinutes = parseInt(end[1]);
    // Check if the start time is after the end time or outside valid hours
    if (endHours < startHours || (endHours === startHours && endMinutes < startMinutes) || startHours < 5 || endHours > 21) {
      if (this.hour1 != '00' && this.hour2 != '00') {
        return this.duration = 'Invalid Schedule';
      } else {
        return this.duration
      }
    }
    let durationHours = endHours - startHours;
    let durationMinutes = endMinutes - startMinutes;
    if (durationMinutes < 0) {
      durationHours--;
      durationMinutes += 60;
    }
    // Check if the duration is less than one hour
    if (durationHours < 1 && durationMinutes < 60) {
      return this.duration = 'Invalid. Duration is less than one hour';
    }
    return this.formatDuration(durationHours, durationMinutes);
  }

  formatDuration(hours, minutes) {
    let duration = '';
    if (hours > 0) {
      duration += hours === 1 ? '1 Hour' : hours + ' Hours';
    }
    if (minutes > 0) {
      if (duration.length > 0) {
        duration += ' and ';
      }
      duration += minutes === 1 ? '1 Minute' : minutes + ' Minutes';
    }
    return duration;
  }

  convertTimeToMilitary(timeString) {
    const [time, ampm] = timeString.split(/(?=[AP]M)/);
    const [hours, minutes] = time.split(':');
    let convertedHours = parseInt(hours);
    let convertedMinutes = parseInt(minutes);
    if (ampm === 'PM' && convertedHours !== 12) {
      convertedHours += 12;
    } else if (ampm === 'AM' && convertedHours === 12) {
      convertedHours = 0;
    }
    const convertedHoursString = convertedHours.toString().padStart(2, '0');
    const convertedMinutesString = convertedMinutes.toString().padStart(2, '0');
    return `${convertedHoursString}:${convertedMinutesString}`;
  }
  // #endregion

  // #region Day Fucntions
  selectDay(dayString) {
    switch (dayString) {
      case 'mwf':
        if (this.monday && this.wednesday && this.friday && !this.tuesday && !this.thursday && !this.saturday) {
          this.monday = false;
          this.wednesday = false;
          this.friday = false;
        } else {
          this.monday = true;
          this.tuesday = false;
          this.wednesday = true;
          this.thursday = false;
          this.friday = true;
          this.saturday = false;
          this.sunday = false;
        }
        break;
      case 'tth':
        if (this.tuesday && this.thursday && !this.monday && !this.wednesday && !this.friday && !this.saturday) {
          this.tuesday = false;
          this.thursday = false;
        } else {
          this.monday = false;
          this.tuesday = true;
          this.wednesday = false;
          this.thursday = true;
          this.friday = false;
          this.saturday = false;
          this.sunday = false;
        }
        break;
      case 'mt':
        if (this.monday && this.tuesday && !this.wednesday && !this.thursday && !this.friday && !this.saturday) {
          this.monday = false;
          this.tuesday = false;
        } else {
          this.monday = true;
          this.tuesday = true;
          this.wednesday = false;
          this.thursday = false;
          this.friday = false;
          this.saturday = false;
          this.sunday = false;
        }
        break;
      case 'wth':
        if (this.wednesday && this.thursday && !this.monday && !this.tuesday && !this.friday && !this.saturday) {
          this.wednesday = false;
          this.thursday = false;
        } else {
          this.monday = false;
          this.tuesday = false;
          this.wednesday = true;
          this.thursday = true;
          this.friday = false;
          this.saturday = false;
          this.sunday = false;
        }
        break;
      case 'fsat':
        if (this.friday && this.saturday && !this.monday && !this.tuesday && !this.wednesday && !this.thursday) {
          this.friday = false;
          this.saturday = false;
        } else {
          this.monday = false;
          this.tuesday = false;
          this.wednesday = false;
          this.thursday = false;
          this.friday = true;
          this.saturday = true;
          this.sunday = false;
        }
        break;
      case 'm-sat':
        if (!this.monday || !this.tuesday || !this.wednesday || !this.thursday || !this.friday || !this.saturday) {
          this.monday = true;
          this.tuesday = true;
          this.wednesday = true;
          this.thursday = true;
          this.friday = true;
          this.saturday = true;
        } else {
          this.monday = false;
          this.tuesday = false;
          this.wednesday = false;
          this.thursday = false;
          this.friday = false;
          this.saturday = false;
          this.sunday = false;
        }
        break;

      case 'm':
        this.monday = !this.monday;
        break;
      case 't':
        this.tuesday = !this.tuesday;
        break;
      case 'w':
        this.wednesday = !this.wednesday;
        break;
      case 'th':
        this.thursday = !this.thursday;
        break;
      case 'f':
        this.friday = !this.friday;
        break;
      case 'sat':
        this.saturday = !this.saturday;
        break;
      case 'sun':
        this.sunday = !this.sunday;
        break;
      case 'lec':
        this.lecture = !this.lecture;
        this.laboratory = false
        break;
      case 'lab':
        this.laboratory = !this.laboratory;
        this.lecture = false
        break;
      case 'lec/lab':
        if (this.lecture && this.laboratory) {
          this.laboratory = false
          this.lecture = false
        } else {
          this.laboratory = true
          this.lecture = true
        }
        break;
    }
    this.checkIfFormIsComplete()
  }

  hideUnhideCustomDays() {
    this.hideCustomDays = !this.hideCustomDays;
  }

  separateDays(dayString) {
    const daysOfWeek = ['M', 'T', 'W', 'TH', 'F', 'SAT', 'SUN'];
    const result = [];
    let i = 0;
    while (i < dayString.length) {
      let day = dayString[i];

      if (day === 'T' && dayString[i + 1] === 'H') {
        day = 'TH';
        i += 2;
      } else {
        i++;
      }

      if (daysOfWeek.includes(day)) {
        result.push(day);
      }
    }
    return result;
  }

  convertTheDayWordToAbbreaviations(day) {
    const days = {
      sunday: "SUN",
      monday: "M",
      tuesday: "T",
      wednesday: "W",
      thursday: "TH",
      friday: "F",
      saturday: "SAT"
    };
    const lowercaseDay = day.toLowerCase();
    if (lowercaseDay in days) {
      return days[lowercaseDay];
    } else {
      return "Invalid day";
    }
  }

  generateDayString() {
    var dayString = ''
    if (this.monday === true) {
      dayString += 'M';
    }
    if (this.tuesday === true) {
      dayString += 'T';
    }
    if (this.wednesday === true) {
      dayString += 'W';
    }
    if (this.thursday === true) {
      dayString += 'TH';
    }
    if (this.friday === true) {
      dayString += 'F';
    }
    if (this.saturday === true) {
      dayString += 'SAT';
    }
    if (this.sunday === true) {
      dayString += 'SUN';
    }
    return dayString
  }

  // #endregion

  // #region Instructor Fuctions
  getInstructors() {
    this.api.getAvailableInstructors().map(response => response.json()).subscribe(res => {
      this.instructors = res.data
      this.filteredInstructors = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterInstructors(value.toString()))
        );
    })
  }

  // @ts-ignore
  private _filterInstructors(value: string): Instructors[] { // Assuming the type of `option` is `Instructors`
    const filterValue = value.toLowerCase();
    return this.instructors
      .flat() // Flatten the multidimensional array
      .filter(option => option.fullname.toLowerCase().includes(filterValue));
  }

  selectInstructor(instructor) {
    this.selectedInstructor = instructor
    this.close('saveInstructor')
  }

  removeSelectedInstructor() {
    this.selectedInstructor = []
  }

  // #endregion

  // #region Navigate Component Functions
  openCourseLookup(): void {
    // var area = this.areaName.slice(0, this.areaName.indexOf('[')-1)
    // var array = [area, this.areaHeadID, this.areaDeptID, this.areaDescription, this.areaID]
    const dialogRef = this.dialog.open(LookUpCurriculumComponent, {
      width: '1400px', disableClose: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.openCourseManager(result.result.programId)
      }
    });
  }

  openCourseManager(courseID): void {
    // var area = this.areaName.slice(0, this.areaName.indexOf('[')-1)
    // var array = [area, this.areaHeadID, this.areaDeptID, this.areaDescription, this.areaID]
    const dialogRef = this.dialog.open(SubjectsComponent, {
      width: '1400px', disableClose: false, data: courseID
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.selectCourse(result.courseID, result.courseTitle, result.program, result.version, result.status, result.recordID)
      }
    });
  }

  openInstructorLookup() {
    const dialogRef = this.dialog.open(AddEditCodesComponent, {
      width: '400px', disableClose: true, data: "Add Instructor"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.selectedInstructor = result.result
      }
    });
  }

  openClassroomManager(action) {
    var width = '90vw'
    var height = '90vh'
    if (action == 'selectSchedule') {
      width = ''
      height = ''
    }
    const dialogRef = this.dialog.open(ClassroomManagerComponent, {
      width: width,
      height: height,
      // @ts-ignore
      data: { building: this.selectedRoom.location, room: this.selectedRoom.roomNo, action: action }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'single') {
        this.clearFields()
        const [startTime, endTime] = result.time.split('-')
        this.hour1 = startTime.substring(0, 2)
        this.minute1 = startTime.substring(3, 5)
        this.meridiem1 = startTime.substring(5)
        this.hour2 = endTime.substring(0, 2)
        this.minute2 = endTime.substring(3, 5)
        this.meridiem2 = endTime.substring(5)
        switch (result.day) {
          case 'Monday':
            this.monday = true;
            break;
          case 'Tuesday':
            this.tuesday = true;
            break;
          case 'Wednesday':
            this.wednesday = true;
            break;
          case 'Thursday':
            this.thursday = true;
            break;
          case 'Friday':
            this.friday = true;
            break;
          case 'Saturday':
            this.saturday = true;
            break;
          case 'Sunday':
            this.sunday = true;
            break;
        }
        this.hideCustomDays = true
        this.checkIfFormIsComplete()


      } else if (result.result == 'multiple') {

        for (var x in result.array) {
          var [start, end] = result.array[x].vacantSched.split('-')
          start = this.convertTimeToMilitary(start)
          end = this.convertTimeToMilitary(end)
          var day = this.convertTheDayWordToAbbreaviations(result.array[x].theDay)
          // @ts-ignore
          this.finalSchedules.push({ room: this.selectedRoom.roomNo, building: this.selectedRoom.location, capacity: this.capacity, startTime: start, endTime: end, lec: this.lecture, lab: this.laboratory, dayString: day })
          this.checkIfFormIsComplete()
        }
      }
    });
  }

  openAddSchedule() {
    const dialogRef = this.dialog.open(AddEditCodesComponent, {
      width: '600px', disableClose: true, data: "Add Schedule"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        for (var x in result.result) {
          this.finalSchedules.push(result.result[x])
        }
      }
    });
  }

  close(action): void {
    if (action == 'save') {
      if (this.completeForm) {
        var dayString = ''
        if (this.monday) {
          dayString += 'M';
        }
        if (this.tuesday) {
          dayString += 'T';
        }
        if (this.wednesday) {
          dayString += 'W';
        }
        if (this.thursday) {
          dayString += 'TH';
        }
        if (this.friday) {
          dayString += 'F';
        }
        if (this.saturday) {
          dayString += 'SAT';
        }
        if (this.sunday) {
          dayString += 'SUN';
        }
        // @ts-ignore
        this.finalSchedules.push({ room: this.selectedRoom.roomNo, building: this.selectedRoom.location, capacity: this.capacity, startTime: this.startTime, endTime: this.endTime, lec: this.lecture, lab: this.laboratory, dayString: dayString })
      }
      if (this.action.includes("Add Schedule for")) {
        // adding from the main window
        this.codeString = this.action.substring(this.action.length - 3);
        this.addSchedule()
        this.dialogRef.close({ result: 'scheduleAdded' });
        return
      } else {
        // adding from the add code form
        this.dialogRef.close({ result: this.finalSchedules });
      }
    } else if (action == 'saveInstructor') {
      this.dialogRef.close({ result: this.selectedInstructor, codeString: this.codeString });
    } else if (action == 'codeAdded') {
      this.dialogRef.close({ result: 'codeAdded', codeString: this.codeString });
    } else if (action == 'deleteSchedule') {
      this.dialogRef.close({ result: 'deleteSchedule', codeString: this.codeString });
    } else if (action == 'scheduleEdited') {
      this.dialogRef.close({ result: 'scheduleEdited', codeString: this.codeString });
    } else {
      this.dialogRef.close({ result: 'cancel' });
    }
  }

  // #endregion

}
