import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodesManagerComponent } from './codes-manager.component';

describe('CodesManagerComponent', () => {
  let component: CodesManagerComponent;
  let fixture: ComponentFixture<CodesManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodesManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodesManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
