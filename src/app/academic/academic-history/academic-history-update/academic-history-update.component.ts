import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { ApiService } from './../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import {Observable } from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import Swal from 'sweetalert2';
const swal = Swal;

export interface School {
  address: string;
  companyID: string;
  companyName: string;
}

@Component({
  selector: 'app-academic-history-update',
  templateUrl: './academic-history-update.component.html',
  styleUrls: [
    './academic-history-update.component.css'
  ]
})
export class AcademicHistoryUpdateComponent implements OnInit {

  /*Initialize Variables*/
  //#region  
  term = '';
  termValue = '';
  sy = '';
  acadHistRecord;
  codeNo = '';
  subjectID = '';
  subjectTitle = '';
  school = '';
  grade = '';
  units = ';'
  setNo = '';
  year1;
  year2;
  gradeWasHidden;
  codeExists;
  disabled = true;
  gradeDisabled = true;
  btnSaveDisabled = true;
  ctrgrade = 0;
  ctrsubjectID = 0;
  ctrsubjectTitle = 0;
  ctrunits = 0;
  ctrschool = 0;
  ctrtermValue = 0;
  ctryear1 = 0;
  ctryear2 = 0;
  swalTxt = '';
  acadRecordSchool: School[] = [];
  form: FormGroup;
  submitted = false;
  schoolCtrl = new FormControl('', Validators.required);
  schools;
  filteredSchools: Observable<School[]>;

  //#endregion

  constructor(private formBuilder: FormBuilder, 
    public dialog: MatDialog, 
    public dialogRef: MatDialogRef<AcademicHistoryUpdateComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService
  ) { }

  ngOnInit() {
    this.schools = this.data.schools;
    this.acadHistRecord = this.data.AcadHistRecordData;
    this.term = this.data.term;
    this.termValue = this.getTermValue(this.term);
    this.year1 = this.getsyLeft(this.acadHistRecord[0].schoolYear);
    this.year2 = this.getsyRight(this.acadHistRecord[0].schoolYear);
    this.subjectID = this.acadHistRecord[0].subjectID;
    this.codeNo = this.acadHistRecord[0].codeNo;
    this.subjectTitle = this.acadHistRecord[0].subjectTitle;
    this.school = this.acadHistRecord[0].companyID;
    if (this.acadHistRecord[0].gradeWasHidden == 0) {
      this.grade = this.acadHistRecord[0].grade;
    } else {
      this.grade = 'Hidden';
    }
    this.units = this.acadHistRecord[0].units;
    this.setNo = this.acadHistRecord[0].setNo;
    this.gradeWasHidden = this.acadHistRecord[0].gradeWasHidden;
    this.codeExists = this.acadHistRecord[0].codeExists.toString();

    /*Set gradeDisabled based on Code Exists and Grade Hidden*/
    //#region 
    if (this.codeExists === 'True') {
      if (this.gradeWasHidden == 1) {
        this.gradeDisabled = true;
      }
      else {
        this.gradeDisabled = false;
      }
    }
    else {
      this.disabled = false;
      if (this.gradeWasHidden == 1) {
        this.gradeDisabled = true;
      }
      else {
        this.gradeDisabled = false;
      }
    }
    //#endregion    

    this.form = this.formBuilder.group({
      codeNo: [this.codeNo],
      subjectID: [this.subjectID, Validators.required],
      grade: [this.grade, Validators.required],
      subjectTitle: [this.subjectTitle, Validators.required],
      units: [this.units, Validators.required],
      setNo: [this.setNo],
      termValue: [this.termValue, Validators.required],
      year1: [this.year1, Validators.required],
      year2: [this.year2, Validators.required]
    });
    this.setFormDisable();
    this.schoolCtrl.setValue({ address: '', companyID: this.acadHistRecord[0].companyID, companyName: this.acadHistRecord[0].school });
    this.filteredSchools = this.schoolCtrl.valueChanges.pipe(
      startWith<string | School>(''),
      map(value => (typeof value === 'string' ? value : value.companyName)),
      map(companyName => (companyName ? this._filterSchools(companyName) : this.schools.slice())),
    );
  }

  displayFn(school?: School): string | undefined {
    return school ? school.companyName : undefined;
  }


  private _filterSchools(name: string): School[] {
    const filterValue = name.toLowerCase();
    return this.schools.filter(s => s.companyName.toLowerCase().includes(filterValue));
  }

  excuteDeleteApi() {
    var holdYear2 = this.f.year2.value.toString();
    var y = holdYear2.substr(-2, 2);
    var holdSchoolyear = this.f.year1.value.toString() + y + this.f.termValue.value.toString();
    var codeNumber = null;
    if (this.acadHistRecord[0].codeNo != '') { codeNumber = this.acadHistRecord[0].codeNo }

    this.api.deleteAcademicHistory(this.acadHistRecord[0].recordID, codeNumber, holdSchoolyear)
      .map(response => response.json())
      .subscribe(res => {
        this.dialogRef.close({ result: 'deleted' });
      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error)
      })
  }

  swalConfirm() {
    //this.swalTxt = 'This command is irreversible!';
    swal.fire({
      type: 'error',
      title: 'Are you sure?',
      text: "You won't be able to revert this!",  
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        //if (!this.form.errors && !this.form.invalid) {
          this.excuteDeleteApi();
        //}
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire({
          title: 'Cancelled',
          type: 'error',
          text: 'Changes are not saved!',
          timer: 1500
        })
      }
    })

  }

  isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

  save() {
    
    this.submitted = true;
   
    if(this.f.year1.value != null && this.f.year2.value != null) {
      if (this.f.year1.value.toString().length < 4 || this.f.year1.value.toString().length > 4) {
        this.f.year1.setErrors({ 'minmaxlength': true });
      }
      if (this.f.year2.value.toString().length < 4 || this.f.year2.value.toString().length > 4) {
        this.f.year2.setErrors({ 'minmaxlength': true });
      }
    }

    // if(this.schoolCtrl.value.companyID=='SLCT' || this.schoolCtrl.value.companyID =='02049') {
    //   if (this.f.companyID.value.toString().length < 2 || this.f.companyID.value.toString().length > 2) {
    //     this.f.companyID.setErrors({ 'minmaxlength': true });
    //   }
    // }

    if (!this.form.errors && !this.form.invalid 
        && !this.schoolCtrl.errors && !this.schoolCtrl.invalid
        && !this.f.year1.errors && !this.f.year2.errors) {
      var holdYear = this.f.year2.value.toString();
      var y = holdYear.substr(-2, 2);
      var codeNumber = '';
      if (this.acadHistRecord[0].codeNo != '') { codeNumber = this.acadHistRecord[0].codeNo }
      this.api.putAcademicHistory(this.acadHistRecord[0].recordID, this.acadHistRecord[0].schoolYear,
        {
          "companyId": this.schoolCtrl.value.companyID,
          "schoolYear": this.f.year1.value.toString() + y + this.f.termValue.value.toString(),
          "subjectId": this.f.subjectID.value,
          "subjectTitle": this.f.subjectTitle.value,
          "grade": this.f.grade.value.toString(),
          "units": this.f.units.value.toString(),
          "codeNo": codeNumber
        })
        .map(response => response.json())
        .subscribe(res => {
          this.submitted = true;
          this.global.swalSuccess('Success');
          this.dialogRef.close({ result: 'updated' });
        }, Error => {
          this.global.swalAlertError(Error);
          console.log(Error)
        })      
    }
  }

  delete() {
    this.swalConfirm();
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  setFormDisable() {
    if (this.gradeDisabled == true) {
      this.f.grade.disable();
    } else { this.f.grade.enable() }

    this.f.codeNo.disable();
    this.f.setNo.disable();

    if (this.disabled == true) {     
      this.f.subjectID.disable();
      this.f.subjectTitle.disable();
      this.f.units.disable();
      this.f.setNo.disable();
      this.schoolCtrl.disable();
      this.f.termValue.disable();
      this.f.year1.disable();
      this.f.year2.disable();
    }
  }

  onChange(NewValue, elementId) {
    var cID = '';
    if (typeof NewValue === 'object') {
      if(elementId=='school'){
        cID = NewValue.companyID;      
      }
    }

    if(elementId=='school' && cID==''){     
      this.schoolCtrl.setErrors({'notfound':true})
    }    

    //counter = 1
    if (elementId == 'subjectID' && this.acadHistRecord[0].subjectID != NewValue) { this.ctrsubjectID = 1 }
    if (elementId == 'subjectTitle' && this.acadHistRecord[0].subjectTitle != NewValue) { this.ctrsubjectTitle = 1 }
    if (elementId == 'grade' && this.acadHistRecord[0].grade != NewValue) { this.ctrgrade = 1 };
    if (elementId == 'units' && this.acadHistRecord[0].units != NewValue) { this.ctrunits = 1 }
    if (elementId == 'school' && this.acadHistRecord[0].school != cID) {this.ctrschool = 1;}
    if (elementId == 'termValue' && this.getTermValue(this.data.term) != NewValue) { this.ctrtermValue = 1 }
    if (elementId == 'year1' && this.getsyLeft(this.acadHistRecord[0].schoolYear) != NewValue) { this.ctryear1 = 1 }
    if (elementId == 'year2' && this.getsyRight(this.acadHistRecord[0].schoolYear) != NewValue) { this.ctryear2 = 1 }

    //counter = 0
    if (elementId == 'subjectID' && this.acadHistRecord[0].subjectID == NewValue) { this.ctrsubjectID = 0 }
    if (elementId == 'subjectTitle' && this.acadHistRecord[0].subjectTitle == NewValue) { this.ctrsubjectTitle = 0 }
    if (elementId == 'grade' && this.acadHistRecord[0].grade == NewValue) { this.ctrgrade = 0 };
    if (elementId == 'units' && this.acadHistRecord[0].units.toString() == NewValue) { this.ctrunits = 0 }
    if (elementId == 'school' && this.acadHistRecord[0].companyID == cID) { this.ctrschool = 0;}
    if (elementId == 'termValue' && this.getTermValue(this.data.term) == NewValue) { this.ctrtermValue = 0 }
    if (elementId == 'year1' && this.getsyLeft(this.acadHistRecord[0].schoolYear) == NewValue) {this.ctryear1 = 0 }
    if (elementId == 'year2' && this.getsyRight(this.acadHistRecord[0].schoolYear) == NewValue) { this.ctryear2 = 0 }



    if (
      this.ctrgrade == 1
      || this.ctrsubjectID == 1
      || this.ctrsubjectTitle == 1
      || this.ctrgrade == 1
      || this.ctrunits == 1
      || this.ctrschool == 1
      || this.ctrtermValue == 1
      || this.ctryear1 == 1
      || this.ctryear2 == 1
    ) {
      if ((!this.f.errors && !this.f.invalid)) {
        this.btnSaveDisabled = false;
      }
    }

    if (
      this.ctrgrade == 0
      && this.ctrsubjectID == 0
      && this.ctrsubjectTitle == 0
      && this.ctrgrade == 0
      && this.ctrunits == 0
      && this.ctrschool == 0
      && this.ctrtermValue == 0
      && this.ctryear1 == 0
      && this.ctryear2 == 0
    ) {
      this.btnSaveDisabled = true;
    }
  }


  getsyLeft(recordSY) {
    var y = parseInt(recordSY.substring(0, 4));
    return y;
  }

  getsyRight(recordSY) {
    var y = parseInt(recordSY.substring(0, 4)) + 1;
    return y;
  }

  yearchange1() {   
    //this.year2=this.year1.value +1;    
    this.f.year2.setValue(this.f.year1.value +1);
  }
  yearchange2() {
    //this.year1 = this.year2.value - 1;        
    this.f.year1.setValue(this.f.year2.value - 1);
  }

  getTermValue(term) {
    switch (term) {
      case 'First Semester': {
        return '1';
        break;
      }
      case 'Second Semester': {
        return '2';
        break;
      }
      case 'Summer': {
        return '3';
        break;
      }
    }
  }

  close() {
    this.dialogRef.close({ result: 'cancel' });
  }

}
