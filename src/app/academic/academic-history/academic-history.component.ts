import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { PersonLookupComponent } from './../../academic/lookup/person-lookup/person-lookup.component';
import { MatDialog, } from '@angular/material';
import { AcademicHistoryUpdateComponent } from './../academic-history/academic-history-update/academic-history-update.component';
import { AcademicHistoryAddComponent } from './academic-history-add/academic-history-add.component';
import Swal from 'sweetalert2';
const swal = Swal;

@Component({
  selector: 'app-academic-history',
  templateUrl: './academic-history.component.html',
  styleUrls: ['./academic-history.component.css']
})
export class AcademicHistoryComponent implements OnInit {

  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService) {

  }

  //declare/initilize variables
  student: any;
  person: any;
  image: any = 'assets/noimage.jpg';
  id = '';
  checkId = '';
  fname = '';
  mname = '';
  lname = '';
  suffix = '';
  lrno: '';
  schoolsArray;
  coursesArray;

  gradearray;
  acadHistRecord;
  x = '';
  y = '';
  yy = '';
  track
  average = 0;
  count = 0;
  IsWait = true;
  matToolTipTxt = '';

  ngOnInit() {
    if (this.global.activeid != '') {
      this.id = this.global.activeid
      this.keyDownFunction('onoutfocus')
    }
  }

  excuteGradeUnhideApi(sy){
    //this.global.swalSuccess('Success');
    //this.getAcademicHistory();
    this.api.putGradeHideUnhide(
      {
        'idNumber':this.id,
        'schoolYear':sy
      })
      .map(response => response.json())
      .subscribe(res => {        
        this.global.swalSuccess('Success');
        this.getAcademicHistory();
      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error)
      })      
  }

  unhideGrade(sy){
    //console.log(sy);
    this.swalConfirm(sy);
  }

  swalConfirm(sy) {
    let swalTxt = 'This command is irreversible!';
    swal.fire({
      title: 'Are you sure?',
      text: swalTxt,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
          this.excuteGradeUnhideApi(sy);
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire({
          title: 'Cancelled',
          type: 'error',
          text: 'Changes are not saved!',
          timer: 1500
        })
      }
    })

  }

  getMatToolTipTxt(gradestatus) {
    if (gradestatus === '1') {
      return 'Grade was hidden.'
    }
  }

  clear() {
    this.image = 'assets/noimage.jpg';
    this.checkId = '';
    this.fname = '';
    this.mname = '';
    this.lname = '';
    this.suffix = '';
    this.lrno = '';

    this.x = '';
    this.y = '';
    this.yy = '';
    this.average = 0;
    this.count = 0;
    this.IsWait = true;
  }

  //get academis hist record by recordID
  getAcadHistRecord(id) {
    this.acadHistRecord = [];
    if (this.gradearray != null) {
      for (var i = 0; i < this.gradearray.length; i++) {
        if (this.gradearray[i].recordID === id) {
          this.acadHistRecord.push(this.gradearray[i]);
        }
      }
    }

  }

  openDialogUpdateAcadHist(id, sy, sem) {
    this.getAcadHistRecord(id)
    const dialogRef1 = this.dialog.open(AcademicHistoryUpdateComponent, {
      width: '850px', 
      disableClose: false, 
      data: { sy: sy, term: sem, AcadHistRecordData: this.acadHistRecord, schools: this.schoolsArray },
      autoFocus: false
    });

    dialogRef1.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.getAcademicHistory();
        }
      }
    });
  }

  openDialogAddAcadHist() {    
    const dialogRef2 = this.dialog.open(AcademicHistoryAddComponent, {
      width: '850px', 
      disableClose: false, 
      data: {id: this.id, sy: this.global.syear, courses: this.coursesArray, schools: this.schoolsArray },
      autoFocus: false
    });
    
    dialogRef2.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.getAcademicHistory();
        }
      }
    });
  }

  studentlookup(): void {
    const dialogRef = this.dialog.open(PersonLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.id != '') {
        this.clear();
        this.global.swalLoading('Loading Person Information');
        this.api.getPerson(this.id)
          .map(response => response.json())
          .subscribe(res => {
            this.global.swalClose();
            if (res.message != undefined && res.message == 'Person found.') {
              this.fname = res.data.firstName;
              this.mname = res.data.middleName;
              this.lname = res.data.lastName;
              this.suffix = res.data.suffixName;
              this.getCourses();
              this.getSchools();
              //get id pic
              this.api.getPersonIDInfo(this.id)
                .map(response => response.json())
                .subscribe(res => {
                  if (res.data != null) {
                    this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
                  }
                }, Error => {
                  this.global.swalAlertError(Error);
                })
              //get AcademicHistory
              this.getAcademicHistory();
            } else {
              this.global.swalAlert(res.message, '', 'warning');
            }
          }, Error => {
            this.global.swalAlertError(Error);
          });
      }
    }
  }

  getAcademicHistory() {
    this.gradearray = undefined;
    this.api.getAcademicHistory(this.id)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data.length != 0) {
          this.gradearray = [];
          this.gradearray = res.data
          this.IsWait = false;
        } else {
          this.IsWait = false;
        }
      }, Error => {
        this.global.swalAlertError(Error)
      });
  }

  //*
  sy(x) {
    if (this.x.substring(0, 6) == x.substring(0, 6)) {
      this.x = x
      return false
    } else {
      this.x = x
      return true
    }
  }

  sem(y) {
    if (this.y == y) {
      this.y = y
      return false
    } else {
      this.y = y
      return true
    }

  }

  sem2(y, i) {
    if (this.gradearray[i + 1] != undefined) {
      if (this.gradearray[i].schoolYear != this.gradearray[i + 1].schoolYear) {
        return true
      }
      return false
    }
    return true
  }

  calcgrade(y) {
    var average = 0
    var count = 0
    for (var i = 0; i < this.gradearray.length; i++) {
      if (this.gradearray[i].schoolYear == y) {
        var x;
        if (this.gradearray[i].grade.replace(/\D/g, "") == '') {
          x = 0
        } else x = parseInt(this.gradearray[i].grade.replace(/\D/g, ""));
        average = average + x * parseFloat(this.gradearray[i].units);
        count = count + parseFloat(this.gradearray[i].units);
      }
    }
    return "Average: " + (Math.floor((average / count) * 100) / 100).toFixed(2).toString() + "%"
  }

  getsy(sem) {
    var y = parseInt(sem.substring(0, 4)) + 1;
    return "SY " + sem.substring(0, 4) + "-" + y
  }
  getsem(sem) {
    if (sem.substring(6) == '1')
      return "First Semester";
    else if (sem.substring(6) == '2')
      return "Second Semester";
    else
      return "Summer";
  }
  //*/

  getSchools() {
    this.global.swalLoading('Loading Schoools');
    this.api.getPublicAPISchools()
      .map(response => response.json())
      .subscribe(res => {
        this.schoolsArray = res;
        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError(Error)
      })
  }

  getCourses(){
    this.coursesArray=[];
    this.global.swalLoading('Loading Courses');
    this.api.getPublicAPICourses()
      .map(response => response.json())
      .subscribe(res => {
        for(var i=0; i < res.data.length; i++){
          if(res.data[i].programLevel>20){
            this.coursesArray.push(res.data[i]);
          }
        }
        this.coursesArray.sort((a, b) => (a.programTitle < b.programTitle ? -1 : 1));
        this.coursesArray.sort((a, b) => (a.version < b.version ? -1 : 1));
        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError(Error)
      })
  }
}
