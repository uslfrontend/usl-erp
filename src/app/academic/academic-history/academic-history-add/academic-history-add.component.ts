import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { ApiService } from './../../../api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import { isEmpty } from 'rxjs-compat/operator/isEmpty';

export interface Course {
  programId: string;
  program: string;
 // major: string;
  //version: string;
};

export interface School {
  address: string;
  companyID: string;
  companyName: string;
};

@Component({
  selector: 'app-academic-history-add',
  templateUrl: './academic-history-add.component.html',
  styleUrls: ['./academic-history-add.component.css']
})
export class AcademicHistoryAddComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, public dialogRef: MatDialogRef<AcademicHistoryAddComponent>, @Inject(MAT_DIALOG_DATA)
  public data: any, public global: GlobalService, private api: ApiService) { }

  /* Initialize Variables */
  //#region 
  schoolYear;
  year1;
  year2;
  termValue;
  btnSaveDisabled
  form: FormGroup;
  submitted = false;

  courses: Course[]=[];
  courseCtrl=new FormControl('', Validators.required);
  filteredCourses: Observable<Course[]>;

  schools;
  schoolCtrl = new FormControl('', Validators.required);  
  filteredSchools: Observable<School[]>;
  //#endregion

  ngOnInit() {   
    this.termValue = parseInt(this.data.sy.substring(-1, 0)+1);
    this.year1 = this.getsyLeft(this.data.sy);
    this.year2 = this.getsyRight(this.data.sy);

    var prog='';
    var maj='';

    for(var i=0; i < this.data.courses.length; i++){              
      maj=this.data.courses[i].major;      
      if(maj==='' || maj ===null ) {
        prog=this.data.courses[i].programTitle +' ('+this.data.courses[i].courseCode+') ' +' ' + this.data.courses[i].version;         
      }
      else {
        prog=this.data.courses[i].programTitle +' ('+this.data.courses[i].courseCode+') ' +' - '+ this.data.courses[i].major +' '+ this.data.courses[i].version;        
      }   
      this.courses.push({
        programId: this.data.courses[i].programId,
        program: prog
      })              
    }

    this.schools = this.data.schools;   
    
    this.form = this.formBuilder.group({
        subjectID:['', Validators.required],
        grade: ['', Validators.required],
        subjectTitle: ['', Validators.required],
        units: ['', Validators.required],
        //school: ['', Validators.required],
        termValue: ['1', Validators.required],
        year1: [this.year1, Validators.required],
        year2: [this.year2, Validators.required]
    })

    this.filteredCourses = this.courseCtrl.valueChanges.pipe(
      startWith<string | Course>(''),
      map(value => (typeof value === 'string' ? value : value.program)),
      map(program => (program ? this._filterCourses(program) : this.courses.slice())),
    );

    //this.schoolCtrl.setValue({ address: '', companyID: '02049', companyName:'University of Saint Louis Tuguegarao' });
    this.filteredSchools = this.schoolCtrl.valueChanges.pipe(
      startWith<string | School>(''),
      map(value => (typeof value === 'string' ? value : value.companyName)),
      map(companyName => (companyName ? this._filterSchools(companyName) : this.schools.slice())),
    );
  }

  //Courses
  displayFnCourse(course?: Course): string | undefined {
    return course ? course.program : undefined;
  }
  private _filterCourses(program: string): Course[] {
    const filterValue = program.toLowerCase();
    return this.courses.filter(c => c.program.toLowerCase().includes(filterValue));
  }

  //School
  displayFnSchool(school?: School): string | undefined {
    return school ? school.companyName : undefined;
  }
  private _filterSchools(name: string): School[] {
    const filterValue = name.toLowerCase();
    return this.schools.filter(s => s.companyName.toLowerCase().includes(filterValue));
  }


  onChange(NewValue, elementId) {
    var cID = '';
    if (typeof NewValue === 'object') {      
      switch (elementId){
        case 'course':{
          cID=NewValue.programId; 
          if(cID==''){      
            this.courseCtrl.setErrors({'required':true})
          } 
          break;
        }
        case 'school':{
          cID=NewValue.companyID; 
          if(cID==''){      
            this.schoolCtrl.setErrors({'required':true})
          } 
          break;
        }
      }
    }
  }


  onSubmit() {
    this.submitted = true;

    if(this.f.year1.value != null && this.f.year2.value != null) {
      if (this.f.year1.value.toString().length < 4 || this.f.year1.value.toString().length > 4) {
        this.f.year1.setErrors({ 'minmaxlength': true });
      }

      if (this.f.year2.value.toString().length < 4 || this.f.year2.value.toString().length > 4) {
        this.f.year2.setErrors({ 'minmaxlength': true });
      }
    }

    if (!this.f.errors && !this.f.invalid && !this.courseCtrl.errors 
        && !this.courseCtrl.invalid && !this.schoolCtrl.errors && !this.schoolCtrl.invalid
        && !this.f.year1.errors && !this.f.year2.errors
    ) {
      var holdYear = this.f.year2.value.toString();
      var y = holdYear.substr(-2, 2);
      var codeNumber = '';      
      this.api.postAcademicHistory(this.data.id.toString(),
        {
          "programid":this.courseCtrl.value.programId.toString(),
          "companyId": this.schoolCtrl.value.companyID.toString(),
          "schoolYear": this.f.year1.value.toString() + y + this.f.termValue.value.toString(),
          "subjectId": this.f.subjectID.value.toString(),
          "subjectTitle": this.f.subjectTitle.value.toString(),
          "grade": this.f.grade.value.toString(),
          "units": this.f.units.value.toString()
        })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess('Success');
          this.dialogRef.close({ result: 'saved' });
        }, Error => {
          this.global.swalAlertError(Error);
          console.log(Error)
        })          
    }   
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

// onReset() {
//     this.submitted = false;
//     this.form.reset();
// }


  getsyLeft(recordSY) {
    var y = parseInt(recordSY.substring(0, 4));
    return y;
  }

  getsyRight(recordSY) {
    var y = parseInt(recordSY.substring(0, 4)) + 1;
    return y;
  }

  yearchange1() {   
    //this.year2=this.year1.value +1;    
    this.f.year2.setValue(this.f.year1.value +1);
  }
  yearchange2() {
    //this.year1 = this.year2.value - 1;        
    this.f.year1.setValue(this.f.year2.value - 1);
  }

  getTermValue(term) {
    switch (term) {
      case 'First Semester': {
        return '1';
        break;
      }
      case 'Second Semester': {
        return '2';
        break;
      }
      case 'Summer': {
        return '3';
        break;
      }
    }
  }


  close() {
    this.dialogRef.close({ result: 'cancel' });
  }

}
