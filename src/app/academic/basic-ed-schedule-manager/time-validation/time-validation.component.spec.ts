import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeValidationComponent } from './time-validation.component';

describe('TimeValidationComponent', () => {
  let component: TimeValidationComponent;
  let fixture: ComponentFixture<TimeValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
