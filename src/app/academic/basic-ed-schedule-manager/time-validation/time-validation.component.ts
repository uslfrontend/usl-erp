import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';

@Component({
  selector: 'app-time-validation',
  templateUrl: './time-validation.component.html',
  styleUrls: ['./time-validation.component.scss']
})
export class TimeValidationComponent implements OnInit {
  importedTime = this.data.selectedData[0]
  time = '12:00'
  time2 = '01:00'

  constructor(public dialogRef: MatDialogRef<TimeValidationComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService) { }

  ngOnInit() {
    this.time = this.importedTime.slice(0, this.importedTime.indexOf('-'))
    this.time2 = this.importedTime.split('-').pop();
    if (this.time.includes('P')) {
      // console.log('Printlog: time is PM: ', this.time)
      var timeLeft = this.time.slice(0, this.time.indexOf(':'))
      var timeRight = this.time.split(':').pop();
      var temp = parseInt(timeLeft) + 12
      timeLeft = temp.toString()
      // console.log('Printlog timeLeft: ', timeLeft)
      this.time = timeLeft + ':' + timeRight
      // console.log(this.time)
    }
    if (this.time2.includes('P')) {
      // console.log('Printlog: time is PM: ', this.time)
      var timeLeft = this.time2.slice(0, this.time.indexOf(':'))
      var timeRight = this.time2.split(':').pop();
      var temp = parseInt(timeLeft) + 12
      timeLeft = temp.toString()
      // console.log('Printlog timeLeft: ', timeLeft)
      this.time2 = timeLeft + ':' + timeRight
      // console.log(this.time2)
    }
    this.time = this.time.replace(/[A-Z]/g, '')
    this.time2 = this.time2.replace(/[A-Z]/g, '')
  }

  sclideTime() {
    var day = true
    var timeSliced = this.time.slice(0, this.time2.indexOf(':'))
    var timeSliced2 = this.time.split(':').pop()
    if (timeSliced == '00') {
      timeSliced = '12'
      day = true
    }
    else if (timeSliced == '12') {
      timeSliced = '12'
      day = false
    }
    if (timeSliced == '13') {
      timeSliced = '01'
      day = false
    }
    if (timeSliced == '14') {
      timeSliced = '02'
      day = false
    }
    if (timeSliced == '15') {
      timeSliced = '03'
      day = false
    }
    if (timeSliced == '16') {
      timeSliced = '04'
      day = false
    }
    if (timeSliced == '17') {
      timeSliced = '05'
      day = false
    }
    if (timeSliced == '18') {
      timeSliced = '06'
      day = false
    }
    if (timeSliced == '19') {
      timeSliced = '07'
      day = false
    }
    if (timeSliced == '20') {
      timeSliced = '08'
      day = false
    }
    if (timeSliced == '21') {
      timeSliced = '09'
      day = false
    }
    if (timeSliced == '22') {
      timeSliced = '10'
      day = false
    }
    if (timeSliced == '23') {
      timeSliced = '11'
      day = false
    }
    if (timeSliced == '24') {
      timeSliced = '12'
      day = false
    }
    if (day == true) {
      this.time = timeSliced + ':' + timeSliced2 + 'AM'
    } else {
      this.time = timeSliced + ':' + timeSliced2 + 'PM'
    }
  }

  sclideTime2() {
    var day = true
    var time2Sliced = this.time2.slice(0, this.time2.indexOf(':'))
    var time2Sliced2 = this.time2.split(':').pop()
    if (time2Sliced == '00') {
      time2Sliced = '12'
      day = true
    }
    else if (time2Sliced == '12') {
      time2Sliced = '12'
      day = false
    }
    if (time2Sliced == '13') {
      time2Sliced = '01'
      day = false
    }
    if (time2Sliced == '14') {
      time2Sliced = '02'
      day = false
    }
    if (time2Sliced == '15') {
      time2Sliced = '03'
      day = false
    }
    if (time2Sliced == '16') {
      time2Sliced = '04'
      day = false
    }
    if (time2Sliced == '17') {
      time2Sliced = '05'
      day = false
    }
    if (time2Sliced == '18') {
      time2Sliced = '06'
      day = false
    }
    if (time2Sliced == '19') {
      time2Sliced = '07'
      day = false
    }
    if (time2Sliced == '20') {
      time2Sliced = '08'
      day = false
    }
    if (time2Sliced == '21') {
      time2Sliced = '09'
      day = false
    }
    if (time2Sliced == '22') {
      time2Sliced = '10'
    }
    if (time2Sliced == '23') {
      time2Sliced = '11'
      day = false
    }
    if (time2Sliced == '24') {
      time2Sliced = '12'
      day = false
    }
    if (day == true) {
      this.time2 = time2Sliced + ':' + time2Sliced2 + 'AM'
    } else {
      this.time2 = time2Sliced + ':' + time2Sliced2 + 'PM'
    }
  }

  save() {
    if(this.time2<this.time){
      // console.log("error")
      this.global.swalAlert('Format Error', 'The End Time cannot be greater than the Start Time', 'warning')
      return
    }
    this.sclideTime()
    this.sclideTime2()
    this.time = this.time + '-' + this.time2
    this.dialogRef.close({ result: this.time });
  }

  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

}
