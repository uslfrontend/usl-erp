import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import Swal from 'sweetalert2';
const swal = Swal;
import * as XLSX from 'xlsx';
import { AcademicService } from '../academic.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SearchFacultyComponent } from './search-faculty/search-faculty.component';
import { SearchAdviserComponent } from './search-adviser/search-adviser.component';
import { TimeValidationComponent } from './time-validation/time-validation.component';

import { title } from 'process';


type AOA = any[][];

@Component({
  selector: 'app-basic-ed-schedule-manager',
  templateUrl: './basic-ed-schedule-manager.component.html',
  styleUrls: ['./basic-ed-schedule-manager.component.scss']
})

export class BasicEdScheduleManagerComponent implements OnInit {
  array = []
  grade = ''
  section = ''
  sectionID = ''
  uploadedGrade = ''
  uploadedSection = ''
  uploadedAdviserName = ''
  tableArr = null
  teacher = ''
  filtereddata = []
  finaldata = []
  finalListToInsert = []
  deptIDList = []
  areaList = []
  areaListDept = []
  days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  fromUpload = false
  errors = 0
  savetemp = false
  syear = this.global.syear.slice(0, -1)
  programArray = [];
  schedulesFromDBArray = [];
  classAdviserID = ''
  classAdviserName = 'unassigned'
  index = 0
  currentEditIndex = 0
  editStateOn = false

  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;

  constructor(public dialog: MatDialog, public global: GlobalService, private api: ApiService, private excelService: AcademicService) { }


  ngOnInit() {
    this.getDeanDept()
    if (this.global.domain == 'ELEMENTARY') {
      this.programArray.push('elem');
    } else if (this.global.domain == 'HIGHSCHOOL') {
      if (this.global.viewdomainname.includes('HS')) {
        this.programArray.push('hs');
      }
      if (this.global.viewdomainname.includes('HSS')) {
        this.programArray.push('hss');
      }
    }
  }

  getSchedules(fromClear) {
    for (var x = 0; x < this.array.length; x++) {
      if (this.section == this.array[x].sectionName) {
        this.sectionID = this.array[x].sectionID
      }
    }
    if (fromClear == true) {
      fromClear = false
      return
    }
    this.schedulesFromDBArray = [];
    this.api.getBasicEdSchedules(this.syear.toString(), this.grade.toString(), this.section.toString())
      .map(response => response.json())
      .subscribe(res => {
        for (var x = 0; x < res.data.length; x++) {
          var startTime = res.data[x].time.slice(0, res.data[x].time.indexOf('-'))
          var endTime = res.data[x].time.split('-').pop();
          //put AM/PM
          var startTime1st = startTime.slice(0, startTime.indexOf(':'))
          // console.log('Printlog: ', startTime1st)
          var endTime1st = endTime.slice(0, endTime.indexOf(':'))
          // console.log('Printlog: ', endTime1st)
          if (parseInt(startTime1st) >= 7 && parseInt(startTime1st) < 12) {
            startTime = startTime + 'AM'
            // console.log('Printlog: ', startTime)
          } else {
            startTime = startTime + 'PM'
          }
          if (parseInt(endTime1st) >= 7 && parseInt(endTime1st) < 12) {
            endTime = endTime + 'AM'
          } else {
            endTime = endTime + 'PM'
          }
          res.data[x].time = startTime + '-' + endTime
        }
        this.fromUpload = false;
        this.schedulesFromDBArray = res.data;
        this.api.getClassAdviser(this.sectionID)
          .map(response => response.json())
          .subscribe(res => {
            if (res.data.adviserID == null || res.data.adviserID == '' || res.data.adviserID == undefined) {
              // console.log(res.data)
              // console.log(this.schedulesFromDBArray.length)
              if (this.schedulesFromDBArray.length == 0) {
                this.classAdviserID = ''
                this.classAdviserName = 'unassigned'
              } else {
                // console.log("this.schedulesFromDBArray.length has data")
                // console.log("this.schedulesFromDBArray ", this.schedulesFromDBArray)
                // console.log("adviserID", this.schedulesFromDBArray[0].adviserID)
                // console.log("adviserName", this.schedulesFromDBArray[0].adviserName)
                this.classAdviserID = this.schedulesFromDBArray[0].adviserID
                this.classAdviserName = this.schedulesFromDBArray[0].adviserName
              }
            } else {
              this.classAdviserID = res.data.adviserID
              this.classAdviserName = res.data.adviser
            }
          }, Error => { this.global.swalAlertError() })
      }, Error => { this.global.swalAlertError() })
  }

  onFileChange(evt: any) {
    this.global.swalLoading('Uploading Excel Data...');
    /* wire up file reader */
    this.finaldata = undefined
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = []
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.uploadthis.nativeElement.value = ''
      //clear arrays
      this.finaldata = []
      this.finalListToInsert = []
      this.fromUpload = true
      this.errors = 0
      this.index = 0
      if (this.data[0] != undefined) {
        this.uploadedGrade = this.data[4][1]
        this.uploadedSection = this.data[5][1]
        this.uploadedAdviserName = this.data[6][1]
        this.finaldata = []
        this.data.shift();
        // console.log(this.section+" "+this.uploadedSection)
        if (this.grade != this.uploadedGrade || this.section != this.uploadedSection) {
          this.fromUpload = false
          this.global.swalAlert("Grade and Section does not match", '', 'warning')
          return
        }
        if (this.classAdviserName != this.uploadedAdviserName) {
          this.fromUpload = false
          this.global.swalAlert("Adviser Mismatched", 'Please make sure you have assigned the correcect adviser', 'warning')
          return
        }
        var lowerCaseHolder = ''
        //trial to get length of final data
        var finalDataLength = 0
        for (var i2 = 7; i2 < this.data.length; ++i2) {
          if ((this.data[i2][0] != '' && this.data[i2][0] != null && this.data[i2][0] != undefined) ||
            (this.data[i2][1] != '' && this.data[i2][1] != null && this.data[i2][1] != undefined) ||
            (this.data[i2][2] != '' && this.data[i2][2] != null && this.data[i2][2] != undefined) ||
            (this.data[i2][3] != '' && this.data[i2][3] != null && this.data[i2][3] != undefined) ||
            (this.data[i2][4] != '' && this.data[i2][4] != null && this.data[i2][4] != undefined)) {
            finalDataLength++
          }
        }
        // console.log("Printlog finalDataLength: ", finalDataLength)
        //main code for pushing
        var indexCounter = 0
        var lastIndex = false
        for (var i2 = 7; i2 < this.data.length; ++i2) {
          if ((this.data[i2][0] != '' && this.data[i2][0] != null && this.data[i2][0] != undefined) ||
            (this.data[i2][1] != '' && this.data[i2][1] != null && this.data[i2][1] != undefined) ||
            (this.data[i2][2] != '' && this.data[i2][2] != null && this.data[i2][2] != undefined) ||
            (this.data[i2][3] != '' && this.data[i2][3] != null && this.data[i2][3] != undefined) ||
            (this.data[i2][4] != '' && this.data[i2][4] != null && this.data[i2][4] != undefined)) {
            //indexCounter increment and track if it is the final index
            indexCounter++
            if (indexCounter == finalDataLength) {
              lastIndex = true
            }
            //subject validation
            if (this.data[i2][0] != '' && this.data[i2][0] != null && this.data[i2][0] != undefined) {
              // console.log("Printlog if")
              if (this.data[i2][0].toLowerCase() == "recess") {
                this.data[i2][3] = 'null'
                this.data[i2][4] = ''
              }
            }
            //Time Validation
            breakTime: if (this.data[i2][1] != '' && this.data[i2][1] != null && this.data[i2][1] != undefined) {
              if (this.hasLetter(this.data[i2][1])) {
                this.data[i2][1] = '**' + this.data[i2][1]
              } else if (!this.data[i2][1].includes(':') || !this.data[i2][1].includes('-')) {
                this.data[i2][1] = '**' + this.data[i2][1]
              } else {
                if (this.data[i2][1].indexOf('-') < 3 || this.data[i2][1].indexOf('-') > 5) {
                  this.data[i2][1] = '**' + this.data[i2][1]
                  break breakTime
                }
                //validate format
                var startTime = this.data[i2][1].slice(0, this.data[i2][1].indexOf('-'))
                var endTime = this.data[i2][1].split('-').pop();
                // console.log(startTime + ' ' + endTime)
                if (startTime.indexOf(':') < 1 || startTime.indexOf(':') > 2) {
                  this.data[i2][1] = '**' + this.data[i2][1]
                  break breakTime
                }
                if (endTime.indexOf(':') < 1 || endTime.indexOf(':') > 2) {
                  this.data[i2][1] = '**' + this.data[i2][1]
                  break breakTime
                }
                //put AM/PM
                var startTime1st = startTime.slice(0, startTime.indexOf(':'))
                // console.log('Printlog: ', startTime1st)
                var endTime1st = endTime.slice(0, endTime.indexOf(':'))
                // console.log('Printlog: ', endTime1st)
                if (parseInt(startTime1st) >= 7 && parseInt(startTime1st) < 12) {
                  startTime = startTime + 'AM'
                  // console.log('Printlog: ', startTime)
                } else {
                  startTime = startTime + 'PM'
                }
                if (parseInt(endTime1st) >= 7 && parseInt(endTime1st) < 12) {
                  endTime = endTime + 'AM'
                } else {
                  endTime = endTime + 'PM'
                }
                //first part change
                if (startTime.indexOf(':') == 1) {
                  startTime = '0' + startTime
                }
                //second part change
                if (endTime.indexOf(':') == 1) {
                  endTime = '0' + endTime
                }
                this.data[i2][1] = startTime + '-' + endTime
              }
            } else {
              this.data[i2][1] = '**'
            }
            //Day Validation
            this.data[i2][2] = this.camelCase(this.data[i2][2])
            //Push
            this.pushViewData(this.data[i2][0], this.data[i2][1], this.data[i2][2], this.data[i2][3], this.data[i2][4], i2, lastIndex)
          }
        }
      } else {
        this.finaldata = []
        this.global.swalAlert('Invalid Excel File!', '', 'warning')
      }
    };
    reader.readAsBinaryString(target.files[0]);
  }

  pushViewData(subject, time, day, teacherID, area, index, lastIndex) {
    //teacher validation
    var teacherName
    // console.log("Printlog: ", teacherID)
    if (teacherID == '' || teacherID == null || teacherID == undefined) {
      teacherID = 'null'
    }
    this.api.getFullNameByID(teacherID).map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          teacherName = res.data.fullName
        } else {
          teacherName = ''
        }
        //area validation
        var areaFound = false
        if (area != '' && area != null && area != undefined) {
          area = area.trim()
          for (var x = 0; x < this.areaList.length; x++) {
            if (this.areaList[x].areaName.toLowerCase() == area.toLowerCase()) {
              area = this.areaList[x].areaName
              areaFound = true
              break
            }
          }
        }
        if (areaFound == false) {
          area = '**' + area
        }
        // Subject Validation
        if (subject != null) {
          if (subject.toLowerCase() == "recess") {
            subject = 'RECESS'
            teacherID = ''
            teacherName = ''
            area = ''
          }
        }
        // time validation
        this.finaldata.push({
          subject: subject,
          time: time,
          day: day,
          teacherId: teacherID,
          teacherName: teacherName,
          area: area,
          editSub: false,
          editTime: false,
          index: this.index,
          time2: ''
        })
        this.validate(this.index)
        this.index++
        // console.log("Errors: ", this.errors)
        if (lastIndex == true) {
          // console.log("Printlog loading should close now")
          this.global.swalClose()
        }
      }, Error => {
      })
  }

  containsSpecialCharsButNotColon(str) {
    const specialChars = /[`!@#$%^&*()_+\=\[\]{};'"\\|,.<>\/?~]/
    return specialChars.test(str)
  }

  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/
    return specialChars.test(str)
  }

  isLetter(str) {
    if (typeof str === 'string') {
      return true
    } else {
      return false
    }
  }

  hasLetter(str) {
    return /[a-zA-Z]/i.test(str);
  }

  camelCase(data) {
    if (typeof data === 'string' || data instanceof String) {
      data = data.toString().toLowerCase()
      data = data.charAt(0).toUpperCase() + data.slice(1);
      return data
    } else {
      return data
    }

  }

  generate() {
    // console.log(this.grade);
    this.api.getSection('', this.grade).map(response => response.json())
      .subscribe(res => {
        this.array = res.data
        // console.log("Printlog sectionList:", this.array)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  downloadExcel(): void {
    if (this.classAdviserName == 'unassigned') {
      // this.global.swalAlert("Please assign a class adviser first", '', 'warning')
      swal.fire({
        title: 'Please assign a class adviser first',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Assign'
      }).then((result) => {
        if (result.value) {
          this.openDialogAdviserEdit()
        }
      })
    } else {
      this.excelService.downloadBasicEdScheduleExcelFormat(this.grade, this.section, this.schedulesFromDBArray, this.classAdviserName)
    }
  }

  upload() {
    for (var x = 0; x < this.finaldata.length; x++) {
      this.finaldata[x].time = this.finaldata[x].time.replace(/[A-Z]/g, '')
      this.finalListToInsert.push({
        schoolYear: this.syear,
        gradeLevel: this.grade,
        section: this.section,
        adviserId: this.classAdviserID,
        time: this.finaldata[x].time,
        day: this.finaldata[x].day,
        subject: this.finaldata[x].subject,
        teacherId: this.finaldata[x].teacherId,
        area: this.finaldata[x].area
      })
    }
    // console.log(this.finalListToInsert)
    swal.fire({
      title: "Upload Confirmation!",
      html: 'You are about to upload schedules for S.Y.<br><b>' + this.syear + '</b><br><br>This will overwrite the previous data!',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.global.swalLoading('Uploading to database...');
        this.api.deleteSection(this.syear, this.grade, this.section).map(response => response.json())
          .subscribe(res => {
            // console.log("Printlog delete success")
            var x = 0
            this.finalListToInsert.forEach((value) => {
              // console.log('tp upload: ', value)
              this.api.uploadSection(value).map(response => response.json())
                .subscribe(res => {
                  // console.log(value)
                  // console.log('Uploaded: ', value)
                  x++
                  if (x == this.finalListToInsert.length) {
                    this.global.swalAlert("Schedule uploaded", '', 'success')
                    this.funcClear(false)
                  }
                }, Error => {
                  this.global.swalAlertError()
                  // console.log('failed')
                })
            })
            // console.log('section deleted')
          }, Error => {
            this.global.swalAlertError()
          })
      }
    })
  }

  funcClear(fromClear) {
    this.errors = 0
    this.finalListToInsert = []
    this.finaldata = []
    this.data = []
    this.fromUpload = false
    this.getSchedules(fromClear)
  }

  getDeanDept() {
    this.api.getAuthUserViewDomains().map(response => response.json())
      .subscribe(res => {
        // console.log("Printlog deptNameList: ", this.deptList)
        this.deptIDList = []
        for (var x = 0; x < res.data.length; x++) {
          this.deptIDList.push(res.data[x].departmentId)
        }
        // console.log("Printlog deptIDList: ", this.deptIDList)
        // console.log("Printlog deptNameList: ", this.deptNameList)
        this.getArea()
      }, Error => {
        this.global.swalAlertError()
      })
  }

  getArea() {
    this.areaList = []
    this.api.getArea('', '', '', '').map(response => response.json())
      .subscribe(res => {
        // console.log("Printlog areaList: ", res.data)
        for (var x = 0; x < res.data.length; x++) {
          for (var y = 0; y < this.deptIDList.length; y++) {
            if (res.data[x].deptID == this.deptIDList[y]) {
              this.areaList.push(res.data[x])
            }
          }
        }
        // console.log("Printlog areaList: ", this.areaList)
      }, Error => {
        this.global.swalAlertError()
      })
  }

  search(index): void {
    // console.log("Printlog index: ", index)
    const dialogRef = this.dialog.open(SearchFacultyComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        for (var x = 0; x < this.finaldata.length; x++) {
          if (this.finaldata[x].index == index) {
            this.finaldata[x].teacherId = result.result
            // console.log("Printlog ID: ", this.finaldata[x].teacherID)
            this.finaldata[x].teacherName = result.name
            this.checkErrors()
            return
          }
        }
      }
    });
  }

  validate(index) {
    for (var x = 0; x < this.finaldata.length; x++) {
      if (index == this.finaldata[x].index) {
        if (this.finaldata[x].subject == '' || this.finaldata[x].subject == null || this.finaldata[x].subject == undefined) {
          this.errors++
          // console.log("Subject Error: ", this.finaldata[x].subject + " " + this.finaldata[x].subject)
        }
        if (this.finaldata[x].time.includes('**')) {
          this.errors++
          // console.log("Time Error: ", this.finaldata[x].subject + " " + this.finaldata[x].time)
        }
        if (!this.days.includes(this.finaldata[x].day)) {
          this.errors++
          // console.log("Day Error: ", this.finaldata[x].subject + " " + this.finaldata[x].day)
        }
        if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].teacherName == '' || this.finaldata[x].teacherName == null || this.finaldata[x].teacherName == undefined)) {
          this.errors++
          // console.log("Teacher ID Error: ", this.finaldata[x].subject + " " + this.finaldata[x].teacherId)
        }
        if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].area.includes('**') || this.finaldata[x].area == '' || this.finaldata[x].area == null || this.finaldata[x].area == undefined)) {
          this.errors++
          // console.log("Area Error: ", this.finaldata[x].subject + " " + this.finaldata[x].area)
        }
      }
    }
  }

  checkErrors() {
    this.errors = 0
    for (var x = 0; x < this.finaldata.length; x++) {
      if (this.finaldata[x].subject == '' || this.finaldata[x].subject == null || this.finaldata[x].subject == undefined) {
        this.errors++
        // console.log("Subject Error: ", this.finaldata[x].subject + " " + this.finaldata[x].subject)
      }
      if (this.finaldata[x].time.includes('**')) {
        this.errors++
        // console.log("Time Error: ", this.finaldata[x].subject + " " + this.finaldata[x].time)
      }
      if (!this.days.includes(this.finaldata[x].day)) {
        this.errors++
        // console.log("Day Error: ", this.finaldata[x].subject + " " + this.finaldata[x].day)
      }
      if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].teacherName == '' || this.finaldata[x].teacherName == null || this.finaldata[x].teacherName == undefined)) {
        this.errors++
        // console.log("Teacher ID Error: ", this.finaldata[x].subject + " " + this.finaldata[x].teacherId)
      }
      if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].area.includes('**') || this.finaldata[x].area == '' || this.finaldata[x].area == null || this.finaldata[x].area == undefined)) {
        this.errors++
        // console.log("Area Error: ", this.finaldata[x].subject + " " + this.finaldata[x].are)
      }
    }
  }

  openDialogAdviserEdit(): void {
    // console.log("Printlog function works: ")
    const dialogRef = this.dialog.open(SearchAdviserComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        var tempID = result.result;
        var tempName = result.name;
        // console.log("Printlog Lookup ID: ", result.result)
        this.updateAdviser(tempID, tempName)
      }
    });
  }

  updateAdviser(id, name) {
    var title = 'Are you sure you want change the Class Adviser?'
    var text = ''
    var confirm = 'Yes'
    swal.fire({
      title: title,
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirm
    }).then((result) => {
      if (result.value) {
        this.classAdviserID = id
        this.classAdviserName = name
        this.finalListToInsert = []
        if (this.schedulesFromDBArray.length < 1) {
          var id1 = parseInt(this.sectionID)
          this.api.putClassAdviser(id1, this.classAdviserID).map(response => response.json())
            .subscribe(res => {
              // console.log("Printlog section adviser updated: ")
              this.global.swalAlert("Adviser Assigned", '', 'success')
              this.funcClear(true)
              return
            }, Error => {
              this.global.swalAlert("Error Assigning Adviser", '', 'warning')
              return
              // console.log('failed')
            })
        }
        for (var x = 0; x < this.schedulesFromDBArray.length; x++) {
          this.finalListToInsert.push({
            schoolYear: this.syear,
            gradeLevel: this.grade,
            section: this.section,
            adviserId: this.classAdviserID,
            time: this.schedulesFromDBArray[x].time,
            day: this.schedulesFromDBArray[x].day,
            subject: this.schedulesFromDBArray[x].subject,
            teacherId: this.schedulesFromDBArray[x].teacherID,
            area: this.schedulesFromDBArray[x].area
          })
        }
        this.api.deleteSection(this.syear, this.grade, this.section).map(response => response.json())
          .subscribe(res => {
            // console.log("Printlog delete success")
            var x = 0
            this.finalListToInsert.forEach((value) => {
              // console.log('tp upload: ', value)
              this.api.uploadSection(value).map(response => response.json())
                .subscribe(res => {
                  // console.log(value)
                  // console.log('Uploaded: ', value)
                  x++
                  if (x == this.finalListToInsert.length) {
                    var id = parseInt(this.sectionID)
                    // console.log("Printlog Section Type: ", typeof (id))
                    this.api.putClassAdviser(id, this.classAdviserID).map(response => response.json())
                      .subscribe(res => {
                        // console.log("Printlog section adviser updated: ")
                        this.global.swalAlert("Schedule updated", '', 'success')
                        this.funcClear(true)
                      }, Error => {
                        this.global.swalAlertError()
                        // console.log('failed')
                      })
                  }
                }, Error => {
                  this.global.swalAlertError()
                  // console.log('failed')
                })
            })
            // console.log('section deleted')
          }, Error => {
            this.global.swalAlertError()
          })
      }
    })
  }

  search2(index): void {
    const dialogRef = this.dialog.open(SearchFacultyComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        for (var x = 0; x < this.finaldata.length; x++) {
          if (index == this.finaldata[x]) {
            this.finaldata[x].teacherId = result.result
            this.finaldata[x].teacherName = result.name
            return
          }
        }
      } else {
        return
      }
    });
  }

  subjectEdit(subject, index) {
    if (subject.toLowerCase() == 'recess') {
      for (var x = 0; x < this.finaldata.length; x++) {
        if (index == this.finaldata[x].index) {
          this.finaldata[x].subject = "RECESS"
        }
      }
    }
    if (subject == '' || subject == null || subject == undefined) {
      this.checkErrors()
      return
    }
    if (subject.length == 1 || subject.length == 5 || subject.length == 6 || subject.length == 7) {
      this.checkErrors()
      return
    }
  }

  keyDownFunctionSubject(event, index) {
    if (event.key == 'Enter') {
      for (var x = 0; x < this.finaldata.length; x++) {
        if (index == this.finaldata[x].index) {
          if (this.finaldata[x].subject != '' && this.finaldata[x].subject != null && this.finaldata[x].subject != undefined) {
            if (this.finaldata[x].subject.toLowerCase() == 'recess') {
              this.finaldata[x].subject = 'RECESS'
            }
          }
          this.finaldata[x].editSub = false
          this.checkEditState()
          this.checkErrors()
          return
        }
      }
    }
  }

  enterEditModeSubject(index) {
    this.editStateOn = true
    this.currentEditIndex = index
    // console.log("Printlog function works. Index: ", index)
    for (var x = 0; x < this.finaldata.length; x++) {
      if (index == this.finaldata[x].index) {
        this.finaldata[x].editSub = true
        // console.log("Printlog set: ", this.finaldata[x])
        return
      }
    }
  }

  keyDownFunctionTime(event, index) {
    if (event.key == 'Enter') {
      for (var x = 0; x < this.finaldata.length; x++) {
        if (index == this.finaldata[x].index) {
          if (this.finaldata[x].time == null && this.finaldata[x].time2 == null) {
            this.finaldata[x].time = '**'
            this.finaldata[x].time = '**'
            this.finaldata[x].editTime = false
            this.checkEditState()
            this.checkErrors()
            return
          }
          this.finaldata[x].editTime = false
          this.checkEditState()
          this.checkErrors()
          this.finaldata[x].time = this.finaldata[x].time + "-" + this.finaldata[x].time2
          // console.log("Time1: ", this.finaldata[x].time)
          // console.log("Time2: ", this.finaldata[x].time2)
          return
        }
      }
    }
  }

  enterEditModeTime(index): void {
    // console.log("Printlog index: ", index)
    for (var x = 0; x < this.finaldata.length; x++) {
      if (this.finaldata[x].index == index) {
        var array = [this.finaldata[x].time]
      }
    }
    const dialogRef = this.dialog.open(TimeValidationComponent, {
      width: '600px', data: { selectedData: array }, disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        for (var x = 0; x < this.finaldata.length; x++) {
          if (this.finaldata[x].index == index) {
            this.finaldata[x].time = result.result
            this.checkErrors()
            return
          }
        }
      }
    });
  }

  editTable() {
    if (this.classAdviserName == 'unassigned') {
      // this.global.swalAlert("Please assign a class adviser first", '', 'warning')
      swal.fire({
        title: 'Please assign a class adviser first',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Assign'
      }).then((result) => {
        if (result.value) {
          this.openDialogAdviserEdit()
        }
      })
      return
    }
    this.global.swalLoading('Opening Editor...');
    var lastIndex = false
    this.finaldata = []
    this.fromUpload = true
    if (this.schedulesFromDBArray.length == 0) {
      var lastIndex = true
      this.pushViewData('', '', '', '', ' ', '', lastIndex)
      return
    }
    for (var x = 0; x < this.schedulesFromDBArray.length; x++) {
      if (x == this.schedulesFromDBArray.length - 1) {
        lastIndex = true
      }
      //subject validation
      if (this.schedulesFromDBArray[x].subject != '' && this.schedulesFromDBArray[x].subject != null && this.schedulesFromDBArray[x].subject != undefined) {
        // console.log("Printlog if")
        if (this.schedulesFromDBArray[x].subject.toLowerCase() == "recess") {
          this.schedulesFromDBArray[x].teacherID = 'null'
          this.schedulesFromDBArray[x].area = ''
        }
      } else {
        // console.log("Printlog else")
        this.schedulesFromDBArray[x].teacherID = 'null'
        this.schedulesFromDBArray[x].area = ''
      }
      this.pushViewData(
        this.schedulesFromDBArray[x].subject,
        this.schedulesFromDBArray[x].time,
        this.schedulesFromDBArray[x].day,
        this.schedulesFromDBArray[x].teacherID,
        this.schedulesFromDBArray[x].area,
        x,
        lastIndex
      )
    }
  }

  addRow() {
    this.index++
    this.finaldata.push({
      subject: '',
      time: '',
      day: '',
      teacherId: '',
      teacherName: '',
      area: '**',
      editSub: false,
      editTime: false,
      index: this.index
    })
    this.errors = 0
    for (var x = 0; x < this.finaldata.length; x++) {
      if (this.finaldata[x].subject == '' || this.finaldata[x].subject == null || this.finaldata[x].subject == undefined) {
        this.errors++
        // console.log("Subject Error: ", this.finaldata[x].subject + " " + this.finaldata[x].subject)
      }
      if (this.finaldata[x].time.includes('**')) {
        this.errors++
        // console.log("Time Error: ", this.finaldata[x].subject + " " + this.finaldata[x].time)
      }
      if (!this.days.includes(this.finaldata[x].day)) {
        this.errors++
        // console.log("Day Error: ", this.finaldata[x].subject + " " + this.finaldata[x].day)
      }
      if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].teacherName == '' || this.finaldata[x].teacherName == null || this.finaldata[x].teacherName == undefined)) {
        this.errors++
        // console.log("Teacher ID Error: ", this.finaldata[x].subject + " " + this.finaldata[x].teacherId)
      }
      if (this.finaldata[x].subject != 'RECESS' && (this.finaldata[x].area.includes('**') || this.finaldata[x].area == '' || this.finaldata[x].area == null || this.finaldata[x].area == undefined)) {
        this.errors++
        // console.log("Area Error: ", this.finaldata[x].subject + " " + this.finaldata[x].are)
      }
    }
  }

  removeRow(index) {
    for (var x = 0; x < this.finaldata.length; x++) {
      if (index == this.finaldata[x].index) {
        this.finaldata.splice(x, 1); // 2nd parameter means remove one item only
        this.checkErrors()
      }
    }
  }

  checkEditState() {
    for (var x = 0; x < this.finaldata.length; x++) {
      if (this.finaldata[x].editSub == true) {
        this.editStateOn = true
        break
      }
      if (this.finaldata[x].editTime == true) {
        this.editStateOn = true
        return
      }
      this.editStateOn = false
    }
  }

}
