import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFacultyComponent } from './search-faculty.component';

describe('SearchFacultyComponent', () => {
  let component: SearchFacultyComponent;
  let fixture: ComponentFixture<SearchFacultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFacultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFacultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
