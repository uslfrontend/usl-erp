import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
// import { ProgramFacultyUploaderComponent } from './../program-faculty-uploader.component'

@Component({
  selector: 'app-search-faculty',
  templateUrl: './search-faculty.component.html',
  styleUrls: ['./search-faculty.component.scss']
})
export class SearchFacultyComponent implements OnInit {
  x = 1
  arraystud = [];
  keyword = ''
  tempkeyword = ''

  constructor(public dialogRef: MatDialogRef<SearchFacultyComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private global: GlobalService, private api: ApiService) { }

  ngOnInit() {
  }

  select(param) {
    this.dialogRef.close({ result: param.employeeId, name: param.fullName });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.x = 0;
      if (this.keyword != '') {
        this.tempkeyword = this.keyword;
        this.arraystud = undefined;
        this.api.getEmployeeFacultyLookup(this.keyword)
          .map(response => response.json())
          .subscribe(res => {
            this.arraystud = res.data
          }, Error => {
            this.global.swalAlertError(Error)
          });
      }
    }
  }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  keyDownFunctionCODE(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.keyword == '') {
      } else {
        this.keyDownFunction('onoutfocus');
      }
    }
  }

}
