import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchAdviserComponent } from './search-adviser.component';

describe('SearchAdviserComponent', () => {
  let component: SearchAdviserComponent;
  let fixture: ComponentFixture<SearchAdviserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchAdviserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchAdviserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
