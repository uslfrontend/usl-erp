import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../../api.service';
import { GlobalService } from './../../../global.service';
import { AcademicService } from '../../academic.service';

interface YearlLevelFilter {
  name: string,
  value: string
}

@Component({
  selector: 'app-schedule-viewer',
  templateUrl: './schedule-viewer.component.html',
  styleUrls: ['./schedule-viewer.component.css']
})
export class ScheduleViewerComponent implements OnInit {

  sy = ''
  progLevel = '';
  yearLevel = '';
  arrayTemp = []
  showTable = false
  yearLevelFilters: YearlLevelFilter[]=[
    {name: "- all -", value:""}
  ];


  constructor(private api: ApiService, public global: GlobalService, private excelService: AcademicService) { }

  array = []

  ngOnInit() {
    if(this.global.domain==='HIGHSCHOOL' || this.global.domain==='ELEMENTARY'){
      this.sy=this.global.syear.slice(0,-1);
    }
    //console.log(this.sy);
    //this.sy = "202021"
    //this.sy=this.global.syear;
    //this.progLevel = 'Elem'
    this.progLevel=this.global.domain;
    this.yearLevel = 'all'
  }

  generateData() {
    this.showTable = true
    var yearAll = false
    if (this.yearLevel == 'all') {
      this.yearLevel = ''
      yearAll = true
    }
    this.api.getBasicEdSchedules(this.sy, this.progLevel, this.yearLevel).map(response => response.json())
      .subscribe(res => {
        this.array = res.data

        // if(res.data != null)
        // {
        //  res.data.forEach(element => {  

        //     if( this.yearLevelFilters.find((obj)=>{ return(obj.value === element.gradeLevel) })  == null ){  

        //       if(element.gradeLevel == 'p1'){
        //         this.yearLevelFilters.push({name:"Prep", value:element.gradeLevel});
        //       }
        //       if(element.gradeLevel == '0'){
        //         this.yearLevelFilters.push({name:"Kinder", value:element.gradeLevel});
        //       }
              
        //       this.yearLevelFilters.push({name:"Grade "+element.gradeLevel, value:element.gradeLevel});

        //     }

        //  });
        //  console.log(this.yearLevelFilters);
        // }

        //console.log(res.data);
        
        // console.log('Printlog data: ', this.array)
        //replace null values to ''
        for (var x = 0; x < this.array.length; x++) {
          if (this.array[x].gradeLevel == null) {
            this.array[x].gradeLevel = ''
          }
          if (this.array[x].section == null) {
            this.array[x].section = ''
          }
          if (this.array[x].time == null) {
            this.array[x].time = ''
          }
          if (this.array[x].day == null) {
            this.array[x].day = ''
          }
          if (this.array[x].subject == null) {
            this.array[x].subject = ''
          }
          if (this.array[x].adviserID == null) {
            this.array[x].adviserID = ''
          }
          if (this.array[x].adviserName == null) {
            this.array[x].adviserName = ''
          }
          if (this.array[x].teacherID == null) {
            this.array[x].teacherID = ''
          }
          if (this.array[x].teacherName == null) {
            this.array[x].teacherName = ''
          }
          if (this.array[x].coordinatorID == null) {
            this.array[x].coordinatorID = ''
          }
          if (this.array[x].coordinatorName == null) {
            this.array[x].coordinatorName = ''
          }
          if (this.array[x].area == null) {
            this.array[x].area = ''
          }
        }
        this.arrayTemp = res.data
      }
      )
    if (yearAll == true) {
      this.yearLevel = 'all'
      yearAll = false
    }
  }

  selectAll() {
    this.yearLevel = 'all'
  }

  fgrade = ''
  fsection = ''
  ftime = ''
  fday = ''
  fsubject = ''
  fadvisername = ''
  fteachername = ''
  fcoordinatorname = ''
  farea = ''
  keyDownFunction() {
    this.array = []
    for (var i = 0; i < this.arrayTemp.length; ++i) {
      if (
        (this.arrayTemp[i].gradeLevel == this.fgrade || this.fgrade == '')
        && (this.arrayTemp[i].section).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.fsection.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].time).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.ftime.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].day == this.fday || this.fday == '')
        && (this.arrayTemp[i].subject).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.fsubject.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].adviserName).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.fadvisername.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].teacherName).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.fteachername.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].coordinatorName).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.fcoordinatorname.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
        && (this.arrayTemp[i].area).replace(/\s/g, '').toLowerCase().replace(/,/g, '').includes(this.farea.replace(/\s/g, '').toLowerCase().replace(/,/g, ''))
      ) {
        this.array.push(this.arrayTemp[i])
      }
    }
  }

  exportExcel(): void {
		var arr = [];
		for (var x = 0; x < this.array.length; x++) {
			arr.push(
				{
					"gradeLevel": this.array[x].gradeLevel,
					"section": this.array[x].section,
					"time": this.array[x].time,
					"day": this.array[x].day,
					"subject": this.array[x].subject,
					"adviserName": this.array[x].adviserName,
					"teacherName": this.array[x].teacherName,
					"coordinatorName": this.array[x].coordinatorName,
					"area": this.array[x].area
				}
			)
		}

		this.excelService.generateBasicEdScheduleExcel(arr)

	}


}
