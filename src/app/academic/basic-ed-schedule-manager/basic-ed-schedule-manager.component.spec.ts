import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicEdScheduleManagerComponent } from './basic-ed-schedule-manager.component';

describe('BasicEdScheduleManagerComponent', () => {
  let component: BasicEdScheduleManagerComponent;
  let fixture: ComponentFixture<BasicEdScheduleManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicEdScheduleManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicEdScheduleManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
