import { Component, OnInit } from '@angular/core';

import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
@Component({
  selector: 'app-college-preenrollment-popup',
  templateUrl: './college-preenrollment-popup.component.html',
  styleUrls: ['./college-preenrollment-popup.component.scss']
})
export class CollegePreenrollmentPopupComponent implements OnInit {

  constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<CollegePreenrollmentPopupComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService,private api: ApiService) { }
record
idpicturevar
dSignvar
birthcertvar
reportCardvar
popvar
idpicturevarsave
dSignvarsave
birthcertvarsave
reportCardvarsave
popsave
pdate
encryp
serverdate
preenrolmentLevel='';
ngOnInit() {
	this.Loaddata()
  this.preenrolmentLevel = this.data.data.level
}
  Loaddata() {
    this.global.swalLoading('')
     this.api.getPublicAPICurrentServerTime()
      .map(response => response.json())
      .subscribe(res => {
        var today:any = new Date(res.data);
        var dd = String(today.getDate()-1).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        this.serverdate = today
        this.api.getEnrollmentPreEnrollmentSubmittedRequirement(this.global.checknosemester(this.global.syear)+'?idNumber='+this.data.data.idNumber)
		  .map(response => response.json())
      .subscribe(res => {

            this.record=res.data
		        this.idpicturevar="data:image/png;base64,"+res.data[0].idPicture
            this.dSignvar = "data:image/png;base64,"+res.data[0].signature
		        this.birthcertvar="data:image/png;base64,"+res.data[0].birthCert
            this.reportCardvar = "data:image/png;base64,"+res.data[0].reportCard
		        this.popvar="data:image/png;base64,"+res.data[0].proofOfPayment
		        this.idpicturevarsave=res.data[0].idPicture
            this.dSignvarsave = res.data[0].signature
		        this.birthcertvarsave=res.data[0].birthCert
            this.reportCardvarsave = res.data[0].reportCard
		        this.popsave=res.data[0].proofOfPayment
		        this.pdate = res.data[0].dateOfPayment

            this.api.getEnrollmentEncryptedString(this.data.data.idNumber)
			        .map(response => response.json())
			        .subscribe(res => {
			          this.encryp=res.data
			          this.api.getgetphpfile('acctgapis.php?action=VerifyPreEnrollment&en='+encodeURIComponent(this.encryp)+'&year='+encodeURIComponent(this.serverdate.substring(this.serverdate.length - 4)))
			            .map(response => response.json())
			            .subscribe(res => {
			              	if (res.data) {
			                  var pdate
                        pdate = new Date(this.pdate).toLocaleString();
                        this.api.putEnrollmentPreEnrollmentSubmittedRequirement(+this.data.data.idNumber+'/'+this.global.checknosemester(this.global.syear),{
                          "IdPicture": this.idpicturevarsave,
                          "IdPictureStatus": this.record[0].idPicture_Status,
                          "IdPictureRemarks": this.record[0].idPicture_Remarks,
                          "BirthCert": this.birthcertvarsave,
                          "BirthCertStatus": this.record[0].birthCert_Status,
                          "BirthCertRemarks": this.record[0].birthCert_Remarks,
                          "ProofOfPayment": this.popsave,
                          "ProofOfPaymentStatus": 2,
                          "ProofOfPaymentRemarks": this.record[0].birthCert_Remarks,
                          "DateOfPayment": pdate,
                          "PaymentType": 1,
                          "reportCard": this.reportCardvarsave,
                          "reportCardStatus": this.record[0].reportCardStatus,
                          "reportCardRemarks": this.record[0].reportCardRemarks,
                          "signature": this.dSignvarsave,
                          "signatureStatus": this.record[0].signatureStatus,
                          "signatureRemarks": this.record[0].signatureRemarks
                          })
                          .map(response => response.json())
                          .subscribe(res => {
                                this.check=1
                                this.record[0].proofOfPayment_Status=2
                                this.global.swalClose()
                          },Error=>{
                            this.global.swalAlertError(Error)
                          });
			                }else{
                				this.global.swalClose()
			                }
                },Error=>{
                  window.history.back();
                  this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
                  });
			         },Error=>{
			            window.history.back();
			            this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
			          });
              },Error=>{
                //console.log(Error);
                this.global.swalAlertError(Error);
              });
       },Error=>{
                //console.log(Error);
                this.global.swalAlertError(Error);
                console.log(Error)
              });
  }

  opennewpic(x){
    const base64ImageData = x;
	const contentType = 'image/png';

	const byteCharacters = atob(base64ImageData.substr(`data:${contentType};base64,`.length));
	const byteArrays = [];

	for (let offset = 0; offset < byteCharacters.length; offset += 1024) {
	    const slice = byteCharacters.slice(offset, offset + 1024);

	    const byteNumbers = new Array(slice.length);
	    for (let i = 0; i < slice.length; i++) {
	        byteNumbers[i] = slice.charCodeAt(i);
	    }

	    const byteArray = new Uint8Array(byteNumbers);

	    byteArrays.push(byteArray);
	}
	const blob = new Blob(byteArrays, {type: contentType});
	const blobUrl = URL.createObjectURL(blob);

	window.open(blobUrl, '_blank');
  }
  verification(id){
  	var x = ''
    // console.log(this.record[0])
  	if(this.global.checkaccess(':Enrollment:PreEnrollmentSubmittedReqOSASPut')){
  		if (this.record[0].idPicture_Status===0) {
  			x=x+'*ID Picture status is required<br>'
  		}
  		if (this.record[0].idPicture_Status===1&& (this.record[0].idPicture_Remarks==''||this.record[0].idPicture_Remarks==null)) {
  			x=x+'*ID Picture remark is required<br>'
  		}
  		if (this.record[0].birthCert_Status===0) {
  			x=x+'*Birth Certificate status is required<br>'
  		}
  		if (this.record[0].birthCert_Status===1&& (this.record[0].birthCert_Remarks==''||this.record[0].birthCert_Remarks==null)) {
  			x=x+'*Birth Certificate remark is required<br>'
  		}
      // console.log(this.record[0].signatureStatus, this.record[0].reportCardStatus)
      if ((this.record[0].signatureStatus===0||this.record[0].signatureStatus===null) && this.preenrolmentLevel != 'Junior High School') {
  			x=x+'*Signature status is required<br>'
  		}
  		if ((this.record[0].signatureStatus===1)&& (this.record[0].signatureRemarks==''||this.record[0].signatureRemarks==null)&& this.preenrolmentLevel != 'Junior High School') {
  			x=x+'*Signature remark is required<br>'
  		}
      if (this.record[0].reportCardStatus===0&& this.preenrolmentLevel != 'Junior High School') {
  			x=x+'*Report card status is required<br>'
  		}
  		if (this.record[0].reportCardStatus===1&& (this.record[0].reportCardRemarks==''||this.record[0].reportCardRemarks==null)&& this.preenrolmentLevel != 'Junior High School') {
  			x=x+'*Report card remark is required<br>'
  		}

  	}

  	if(this.global.checkaccess(':Enrollment:PreEnrollmentSubmittedReqAcctgPut')){

  		if (this.record[0].proofOfPayment_Status===0) {
  			x=x+'*Payment status is required<br>'
  		}
  		if (this.record[0].proofOfPayment_Status===1&& (this.record[0].proofOfPayment_Remarks==''||this.record[0].proofOfPayment_Remarks==null)) {
  			x=x+'*Payment remark is required<br>'
  		}
  	}
  	if(x==''){
	  	this.global.swalLoading('')
	  	if(this.global.checkaccess(':Enrollment:PreEnrollmentSubmittedReqOSASPut')){
	  	  this.api.putEnrollmentPreEnrollmentSubmittedRequirementOSAS(id +'/'+this.global.checknosemester(this.global.syear),
	  	  	{
            "IdPicture": this.idpicturevarsave,
            "IdPictureStatus": this.record[0].idPicture_Status,
            "IdPictureRemarks": this.record[0].idPicture_Remarks,
            "BirthCert": this.birthcertvarsave,
            "BirthCertStatus":this.record[0].birthCert_Status,
            "BirthCertRemarks": this.record[0].birthCert_Remarks,
            "ProofOfPayment": this.popsave,
            "reportCard": this.reportCardvarsave,
            "reportCardStatus": this.record[0].reportCardStatus,
            "reportCardRemarks": this.record[0].reportCardRemarks,
            "signature": this.dSignvarsave,
            "signatureStatus":this.record[0].signatureStatus,
            "signatureRemarks": this.record[0].signatureRemarks
	  	  	})
		        .map(response => response.json())
		        .subscribe(res => {
		        	if (this.record[0].idPicture_Status==2&&this.idpicturevarsave!='') {
		        		this.api.putStudentPicture(id ,
	  	  					{
                    "IdPicture": this.idpicturevarsave,
                    "signature": this.dSignvarsave
                  })
					        .map(response => response.json())
					        .subscribe(res => {
					        },Error=>{
				            this.global.swalAlertError(Error);
				          });
		        	}
		        	this.global.swalSuccess(res.message)
		        	this.check=1
		       },Error=>{
	            this.global.swalAlertError(Error);
	          });

	  	}
	  	if(this.global.checkaccess(':Enrollment:PreEnrollmentSubmittedReqAcctgPut')){
	  		this.api.putEnrollmentPreEnrollmentSubmittedRequirementAcctg(id +'/'+this.global.checknosemester(this.global.syear) ,
	  	  	{
			  "ProofOfPayment":this.popsave,
			  "ProofOfPaymentStatus": this.record[0].proofOfPayment_Status,
			  "ProofOfPaymentRemarks":this.record[0].proofOfPayment_Remarks
			})
		        .map(response => response.json())
		        .subscribe(res => {
		        	this.global.swalSuccess(res.message)
		        	this.check=1
		       },Error=>{
	            this.global.swalAlertError(Error);
	            console.log(Error)
	          });
	  	}
	  }else{
	  	this.global.swalAlert("Required fields warning!",x,'warning')
	  }
  }

  check=0
	onNoClick(): void {

	    this.dialogRef.close(this.check);
	  }
}
