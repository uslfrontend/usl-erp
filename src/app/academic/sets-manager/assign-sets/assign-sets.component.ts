import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';

import Swal from 'sweetalert2';
const swal = Swal;
import { MatTableDataSource, MatSort, } from '@angular/material';
import { ViewChild } from '@angular/core';

import { AlternativeCodeComponent } from './../../enrollment-manager/alternative-code/alternative-code.component';



@Component({
  selector: 'app-assign-sets',
  templateUrl: './assign-sets.component.html',
  styleUrls: ['./assign-sets.component.scss']
})
export class AssignSetsComponent implements OnInit {
	arraysubjects=[]
  title
  dataSource;
  tableArr:Array<any>;
  codeno='';


  tempconflict=[];
  Codecheckvariable=0

  displayedColumns = ['codeno','subjectId','subjectTitle','day','time','roomNumber','units', "classSize",'oe','res','action'];
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<AssignSetsComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService,private api: ApiService) { }

  ngOnInit() {
    if (this.data.indi==null) {
      this.displayedColumns = ['codeno','subjectId','subjectTitle','day','time','roomNumber','units', "classSize",'oe','res','action'];
    }else
      this.displayedColumns = ['codeno','subjectId','subjectTitle','day','time','roomNumber','units', "classSize",'oe','res'];
  	this.title=this.data.x.setDescription
	  this.getsubjects(0)
  }
  subj=[]
	getsubjects(x){
    this.arraysubjects=undefined
    this.api.getCodeSetDetails(this.data.x.headerId)
      .map(response => response.json())
      .subscribe(res => {
        this.arraysubjects=[]
        this.arraysubjects=res.data
        this.subj = []
        for (var i = 0; i < res.data.length; ++i) {
          var subjectId = res.data[i].subjectId
          var units2 = res.data[i].units
          var codeNo = res.data[i].codeNo
          var subjectTitle = res.data[i].subjectTitle

          if (i>0) {
            var x=i
            var check = 1
            if (codeNo==res.data[x-1].codeNo) {
              codeNo='';
              units2=0;
              subjectId='';
              subjectTitle='';
            }
            if (i>1){
              if (codeNo==res.data[x-2].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>2){
              if (codeNo==res.data[x-3].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>4){
              if (codeNo==res.data[x-4].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>4){
              if (codeNo==res.data[x-5].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
          }
          this.subj.push({
            classSize: res.data[i].classSize,
            codeNo: codeNo,
            day: res.data[i].day,
            detailId: res.data[i].detailId,
            headerId: res.data[i].headerId,
            roomNumber: res.data[i].roomNumber,
            subjectId: subjectId,
            subjectTitle: subjectTitle,
            time: res.data[i].time,
            units: units2,
            oe: res.data[i].oe,
            res: res.data[i].res,
          })

        }
        this.dataSource = new MatTableDataSource(this.subj);
        this.dataSource.sort = this.sort;
        if (x!=0) {
        }
      },Error=>{
        this.global.swalAlertError(Error)
      });
	}

checkvisible(x){
 if (x==''){return true}return false
}

deleteCode(codeno,id){
    this.swalConfirm("Confirm Delete","You are about to Delete the code "+codeno,'warning','Drop code','Code has been dropped','deleteCode',id);
}
  swalConfirm(title,text,type,button,successm,swaltype,id){
    var codeNo = id;
    swal.fire({
      title: title,
      html: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          if (swaltype=='deleteCode') {
             this.api.deleteCodeSetDetail(id)
                .map(response => response.text())
                .subscribe(res => {
                  this.getsubjects(0);
                },Error=>{
                  this.global.swalAlertError(Error)
                });
          }else if(swaltype =='alternative'){
            //opens the alternative component from enrollment manager module to add an alternative code
            this.openDialogalternativecode(codeNo);
          }
        }
      })
  }

   close(): void {
       this.dialogRef.close({result:'cancel'});
  }
  getCodeWarning(res){
    var proceed = 0;
    var codeWarning = '';
    let ctr=0;
    if (this.tempconflict.length>0) {
      var conflict = ''
      for (var l = 0; l < this.tempconflict.length; ++l) {
        conflict = conflict + "* "+this.tempconflict[l].codeNo+"<br>"
      }
      this.tempconflict=[];
      // ctr = counter for numbering the code warnings.
      ctr+=1;
      codeWarning = codeWarning + ctr+". Code "+this.codeno+" is in conflict with the following codes:<br>"+conflict+"<br>";
    }
    for (var i = 0; i < res.data.length; ++i) {
      //oe = Officially enrolled = paid
      //res = Reserved = admitted
      if ((res.data[i].oe+res.data[i].res)>=res.data[i].classSize) {
          ctr+=1;
          codeWarning = codeWarning + ctr+'. Maximum student has been reached.<br>';
      }
    }
    for (var i3 = 0; i3 < this.arraysubjects.length; ++i3) {
      if (res.data[0].subjectTitle==this.arraysubjects[i3].subjectTitle||res.data[0].subjectID==this.arraysubjects[i3].subjectID) {
        ctr+=1;
        codeWarning = codeWarning + ctr+". "+res.data[0].subjectID+" - "+res.data[0].subjectTitle+" already exist!<br>"
      }
    }

    if (codeWarning!='') {
      this.swalConfirm("Code adding failed!","Unable to add code because of the following: <br>"+codeWarning+"<br>Do you want to choose a code alternative?","warning",'OK',"Code added",'alternative',res.data[0])
    }else if (proceed == 0) {
      this.addCode(this.data.x.headerId,this.codeno);
    }
  }

  codeConflictCheck(res,codeno,ctr){
    this.api.getCodeSetDetailsConflictSchedules(res.data[this.Codecheckvariable].day+'/'+res.data[this.Codecheckvariable].time+'/'+res.data[this.Codecheckvariable].codeNo+'/'+this.data.x.headerId+'/'+this.global.syear)
      .map(response => response.json())
      .subscribe(res2 => {
        this.tempconflict=this.tempconflict.concat(res2.data)
        if (ctr>1) {
          this.Codecheckvariable++;
          this.codeConflictCheck(res,codeno,ctr-1);
        }else{
          this.getCodeWarning(res)
        }
      },Error=>{
        this.global.swalAlertError(Error);
      });
  }

  settemp=true
	keyDownFunctionCODE(event){
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {

      if (this.settemp) {
        this.settemp = false
        if (this.codeno=='') {
              this.global.swalAlert("Code does not exist","",'warning');
        }else{
          let x = 1;
          for (var i = 0; i < this.arraysubjects.length; ++i) {
            if (this.arraysubjects[i].codeNo.toLowerCase()==this.codeno.toLowerCase()) {
              x=0;
            }
          }

          if (x==1) {
            this.api.getEnrollmentEnrolledSubjectsCodeNo(this.codeno,this.data.syear,"true")
              .map(response => response.json())
              .subscribe(res => {
                if (res.data.length!=0) {
                  this.tempconflict=[];
                  this.Codecheckvariable=0
                  this.codeConflictCheck(res,this.codeno,res.data.length);
                }else
                  this.global.swalAlert("Code not found","Code "+this.codeno+" does not exist",'warning');
                this.settemp = true
              },Error=>{
                  this.settemp = true
                  this.tableArr=[];
                  this.global.swalClose()
                });
          }else{
            this.settemp = true
            this.global.swalAlert('Code:'+this.codeno,"Already exist!",'warning');
          }

        }
    	}
	  }
  }
  addCode(headerID,codeNo){
    this.api.postCodeSetDetail({
      "headerId": headerID,
      "codeNo": codeNo
    })
      .map(response => response.json())
      .subscribe(res => {
        this.codeno = ''
        this.settemp = true
        //this.global.swalSuccess(res.message)
        this.getsubjects(1)
        //this.dialogRef.close({result:'save',val:this.sy});
      },Error=>{
        this.global.swalAlertError(Error);
        console.log(Error)
      });
  }


  ////////////////////
  openDialogalternativecode(x): void {
    const dialogRef = this.dialog.open(AlternativeCodeComponent, {
      width: '75%', data:{data:x,subj:this.arraysubjects,callOutType:"setManager"}, disableClose: false
    });

    dialogRef.afterClosed().subscribe(result => {
        if (result==undefined) {
          // code...
        }else if (result.see=='success') {
          this.codeno = result.result;
          this.keyDownFunctionCODE('onoutfocus')
        }else if (result.see=='bypass') {
          //this is the result when "force add/enroll" button is clicked from the alternative code module
          this.codeno = result.result;
          this.addCode(this.data.x.headerId,this.codeno);
        }
    });

  }
}
