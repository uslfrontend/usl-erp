import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonInformationComponent } from './../../general/person-information/person-information.component';
import { UpdatePersonComponent } from './../../general/update-person/update-person.component';
import { HomeComponent } from './../../home/home.component';
import { ApplicationManagerComponent } from './../../hris/application-manager/application-manager.component';
import { EmployeeInformationComponent } from './../../hris/employee-information/employee-information.component';
import { LeaveManagementComponent } from './../../hris/leave-management/leave-management.component';
import { UserManagementComponent } from './../../control-panel/user-management/user-management.component';
import { StudentInformationComponent } from './../../academic/student-information/student-information.component';
import { AdmissionComponent } from './../../academic/admission/admission.component';
import { SchoolYearSettingComponent } from './../../control-panel/school-year-setting/school-year-setting.component';
import { EnrollmentManagerComponent } from './../../academic/enrollment-manager/enrollment-manager.component';
import { StudentRegistrationComponent } from './../../academic/student-registration/student-registration.component';
import { SetsManagerComponent } from './../../academic/sets-manager/sets-manager.component';
import { PurgingComponent } from './../../academic/purging/purging.component';
import { CurriculumComponent } from './../../academic/curriculum/curriculum.component';
import { SubjectsComponent } from './../../academic/curriculum/subjects/subjects.component';
import { DepartmentsComponent } from './../../hris/hris-maintenance/departments/departments.component';
import { PositionsComponent } from './../../hris/hris-maintenance/positions/positions.component';
import { RanksComponent } from './../../hris/hris-maintenance/ranks/ranks.component';
import { ItemsComponent } from './../../hris/hris-maintenance/items/items.component';
import { StudentEvaluationComponent } from './../../academic/student-evaluation/student-evaluation.component';
import { UserManagementStudentComponent } from './../../control-panel/user-management-student/user-management-student.component';
import { RoleManagementComponent } from './../../control-panel/role-management/role-management.component';
import { CodeProjectionComponent } from './../../reports/academic/pre-enrolment/code-projection/code-projection.component';
import { PlacementSummaryComponent } from './../../reports/academic/pre-enrolment/placement-summary/placement-summary.component';
import { EnrolmentListComponent } from './../../reports/ched/enrolment-list/enrolment-list.component';
import { EnrolmentSummaryComponent } from './../../reports/ched/enrolment-summary/enrolment-summary.component';
import { NstpComponent } from './../../reports/ched/nstp/nstp.component';
import { ClassListComponent } from './../../reports/academic/faculty/class-list/class-list.component';
import { CodeSummaryComponent } from './../../reports/academic/faculty/code-summary/code-summary.component';
import { AppManagerComponent } from './../../control-panel/app-manager/app-manager.component';
import { EnrollmentSummaryComponent } from './../../academic/enrollment-summary/enrollment-summary.component';
import { ExamScheduleComponent } from './../../academic/exam-schedule/exam-schedule.component';
import { StudentEnrollmentListComponent } from './../../reports/academic/student/student-enrollment-list/student-enrollment-list.component';
import { ScheduleManagerComponent } from './../../hris/schedule/schedule-manager/schedule-manager.component';
import { CompanyComponent } from './../../academic/maintenance/company/company.component';
import { OnlineRegistrationComponent } from './../../academic/online-registration/online-registration.component';
import { ApplicantSummaryComponent } from './../../reports/academic/applicant-summary/applicant-summary.component';
import { PlacementManagerComponent } from './../../academic/placement-manager/placement-manager.component';
import { EntranceExamScheduleManagerComponent } from './../../academic/maintenance/entrance-exam-schedule-manager/entrance-exam-schedule-manager.component';
import { StudentEnrollmentStatisticComponent } from './../../reports/academic/student/student-enrollment-statistic/student-enrollment-statistic.component';
import { StudentEnrollmentSummaryComponent } from './../../reports/academic/student/student-enrollment-summary/student-enrollment-summary.component';
import { G9ElectiveComponent } from './../../academic/g9-elective/g9-elective.component';
import { ModalityComponent } from './../../academic/modality/modality.component';
import { SectionManagerComponent } from './../../academic/section-manager/section-manager.component';
import { BasicEdScheduleManagerComponent } from '../../academic/basic-ed-schedule-manager/basic-ed-schedule-manager.component';
import { ReportModalitiesComponent } from './../../reports/academic/report-modalities/report-modalities.component';
import { OGSComponent } from './../../academic/ogs/ogs.component';
import { EmployeeProfileComponent } from './../../reports/hris/employee-profile/employee-profile.component';
import { CollegePreenrollmentComponent } from './../../academic/college-preenrollment/college-preenrollment.component';
import { CollegePreenrolmentstatDepartmentComponent } from './../../reports/academic/pre-enrolment/college/college-preenrolmentstat-department/college-preenrolmentstat-department.component';
import { CollegePreenrolmentstatCourseComponent } from './../../reports/academic/pre-enrolment/college/college-preenrolmentstat-course/college-preenrolmentstat-course.component';
import { ApplicantListReportRecommendedComponent } from './../../reports/academic/pre-enrolment/applicant-list-report-recommended/applicant-list-report-recommended.component';

import { StudentProfilesComponent } from './../../reports/academic/student/student-profiles/student-profiles.component';
import { StudentCorporateEmailComponent } from './../../reports/academic/student/student-corporate-email/student-corporate-email.component';
import { CorporateEmailComponent } from './../../control-panel/corporate-email/corporate-email.component';
import { StudentAcademicAchieverComponent } from './../../reports/academic/student/student-academic-achiever/student-academic-achiever.component';
import { AcademicHistoryComponent } from './../../academic/academic-history/academic-history.component';
import { EmployeeManagerComponent } from './../../hris/employee-manager/employee-manager.component';
import { ClearOnlineSubmittedRequirementsComponent } from './../../academic/maintenance/clear-online-submitted-requirements/clear-online-submitted-requirements.component';

import { SeminarsTrainingsComponent } from './../../reports/hris/seminars-trainings/seminars-trainings.component';

import { FacultyEvaluationComponent } from './../../academic/faculty-evaluation-manager/faculty-evaluation/faculty-evaluation.component';
import { EvaluationSettingsComponent } from './../../academic/faculty-evaluation-manager/evaluation-settings/evaluation-settings.component';
import { ProgramFacultyUploaderComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/program-faculty-uploader.component';

import { ScheduleViewerComponent } from './../../academic/basic-ed-schedule-manager/schedule-viewer/schedule-viewer.component';
import { AddIndividualFacultyComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/add-individual-faculty/add-individual-faculty.component';
import { ProgramChairEditComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/program-chair-edit/program-chair-edit.component';
import { AreaUploaderComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/area-uploader.component';
import { AddAreaHeadComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-area-head/add-area-head.component';
import { AddAreaComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-area/add-area.component';
import { EditAreaComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/edit-area/edit-area.component';
import { AddIndividualAreaFacultyComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-individual-area-faculty/add-individual-area-faculty.component';
import { SearchAdviserComponent } from './../../academic/basic-ed-schedule-manager/search-adviser/search-adviser.component';
import { SearchFacultyComponent } from './../../academic/basic-ed-schedule-manager/search-faculty/search-faculty.component';
import { TimeValidationComponent } from './../../academic/basic-ed-schedule-manager/time-validation/time-validation.component';

import { CodeLookupComponent } from './../../tools/code-lookup/code-lookup.component';
import { PersonFinderComponent } from './../../tools/person-finder/person-finder.component';
import { ClassroomManagerComponent } from './../../academic/maintenance/classroom-manager/classroom-manager.component';
import { GradesHidingUnhidingComponent } from './../../academic/grades-hiding-unhiding/grades-hiding-unhiding.component';
import { DtrViewerComponent } from './../../reports/hris/dtr-viewer/dtr-viewer.component';
import { CodesManagerComponent } from '../../academic/codes-manager/codes-manager.component';
import { AccountsReceivableComponent } from '../../reports/fmis/accounts-receivable/accounts-receivable.component';
import { AccountsPayableComponent } from '../../reports/fmis/accounts-payable/accounts-payable.component';
import { GeneralLedgerComponent } from '../../reports/fmis/general-ledger/general-ledger.component';
import { CollectionComponent } from '../../fmis/accounts-receivable/collection/collection.component';
import { PayrollComponent } from '../../fmis/accounts-payable/payroll/payroll.component';
import { ORManagerComponent } from '../../fmis/or-manager/or-manager.component';
import { AccountsVerificationManagerComponent } from '../../fmis/accounts-verification-manager/accounts-verification-manager.component';
import { PaymentPersonLookupComponent } from '../../fmis/accounts-verification-manager/payment-person-lookup/payment-person-lookup.component';
import { RegistrarCertificationPurposeComponent } from '../../academic/student-information/registrar-certification-purpose/registrar-certification-purpose.component';
import { OtrComponent } from '../../academic/otr/otr.component';
import { DtrUploaderComponent } from '../../hris/dtr-uploader/dtr-uploader.component';
import { OtherInformationComponent } from '../../hris/employee-information/other-information/other-information.component';
import { EmploymentRecordsComponent } from '../../hris/employee-information/employment-records/employment-records.component';
import { RankRecordBatchUploaderComponent } from '../../hris/employee-manager/rank-record-batch-uploader/rank-record-batch-uploader.component';
import { AddEditCodesComponent } from '../../academic/codes-manager/add-edit-codes/add-edit-codes.component';
import { JobPostingComponent } from '../../hris/job-posting/job-posting.component';


import { CheckDisbursementComponent } from '../../fmis/accounts-payable/check-disbursement/check-disbursement.component';
import { WithdrawConditionallyEnrolledComponent } from '../../academic/withdraw-conditionally-enrolled/withdraw-conditionally-enrolled.component';
import { AssessmentSetupComponent } from '../../fmis/accounts-receivable/assessment-setup/assessment-setup.component';
import { RequirementlistComponent } from '../../reports/academic/student/requirementlist/requirementlist.component';
import { ActiveEmployeeMasterlistComponent } from '../../reports/hris/active-employee-masterlist/active-employee-masterlist.component';
import { SchoolgradfromComponent } from '../../reports/academic/schoolgradfrom/schoolgradfrom.component';
import { LrlistComponent } from '../../reports/academic/student/lrlist/lrlist.component';

import { EndOfSerivceBatchComponent } from '../../hris/employee-information/employee-records/end-of-serivce-batch/end-of-serivce-batch.component';
import { DissolutionAndMergingComponent } from '../../academic/codes-manager/dissolution-and-merging/dissolution-and-merging.component';


const routes: Routes = [

  { path: 'Applicant-List-Report-Recommended-Report', component: ApplicantListReportRecommendedComponent, outlet: 'div' },
  { path: 'College-PreenrollmentStat-Department-Report', component: CollegePreenrolmentstatDepartmentComponent, outlet: 'div' },
  { path: 'College-PreenrollmentStat-Course-Report', component: CollegePreenrolmentstatCourseComponent, outlet: 'div' },
  { path: 'Electronic-Grading-Sheet', component: OGSComponent, outlet: 'div' },
  { path: 'College-Preenrollment', component: CollegePreenrollmentComponent, outlet: 'div' },
  { path: 'Learning-Modality-Report', component: ReportModalitiesComponent, outlet: 'div' },
  { path: 'BE-Schedule-Manager', component: BasicEdScheduleManagerComponent, outlet: 'div' },
  { path: 'Section-Manager', component: SectionManagerComponent, outlet: 'div' },
  { path: 'Modality', component: ModalityComponent, outlet: 'div' },
  { path: 'Grade-9-Elective', component: G9ElectiveComponent, outlet: 'div' },
  { path: 'Report-Enrollment-Summary', component: StudentEnrollmentSummaryComponent, outlet: 'div' },
  { path: 'Report-Enrollment-Statistic', component: StudentEnrollmentStatisticComponent, outlet: 'div' },
  { path: 'placement-manager', component: PlacementManagerComponent, outlet: 'div' },
  { path: 'person-information', component: PersonInformationComponent, outlet: 'div' },
  { path: 'applicant-summary', component: ApplicantSummaryComponent, outlet: 'div' },
  { path: 'update-person', component: UpdatePersonComponent, outlet: 'div' },
  { path: 'home', component: HomeComponent, outlet: 'div' },
  { path: 'application-manager', component: ApplicationManagerComponent, outlet: 'div' },
  { path: 'employee-information', component: EmployeeInformationComponent, outlet: 'div' },
  { path: 'user-management', component: UserManagementComponent, outlet: 'div' },
  { path: 'student-information', component: StudentInformationComponent, outlet: 'div' },
  { path: 'admission', component: AdmissionComponent, outlet: 'div' },
  { path: 'sy-setting', component: SchoolYearSettingComponent, outlet: 'div' },
  { path: 'Enrollment-Manager', component: EnrollmentManagerComponent, outlet: 'div' },
  { path: 'Student-Registration', component: StudentRegistrationComponent, outlet: 'div' },
  { path: 'Sets-Manager', component: SetsManagerComponent, outlet: 'div' },
  { path: 'leave-management', component: LeaveManagementComponent, outlet: 'div' },
  { path: 'purge-list', component: PurgingComponent, outlet: 'div' },
  { path: 'curriculum', component: CurriculumComponent, outlet: 'div' },
  { path: 'subjects', component: SubjectsComponent, outlet: 'div' },
  { path: 'hris-maintenance-departments', component: DepartmentsComponent, outlet: 'div' },
  { path: 'hris-maintenance-positions', component: PositionsComponent, outlet: 'div' },
  { path: 'hris-maintenance-ranks', component: RanksComponent, outlet: 'div' },
  { path: 'hris-maintenance-items', component: ItemsComponent, outlet: 'div' },
  { path: 'student-evaluation', component: StudentEvaluationComponent, outlet: 'div' },
  { path: 'user-management-student', component: UserManagementStudentComponent, outlet: 'div' },
  { path: 'role-management', component: RoleManagementComponent, outlet: 'div' },
  { path: 'report-code-projection', component: CodeProjectionComponent, outlet: 'div' },
  { path: 'report-placement-summary', component: PlacementSummaryComponent, outlet: 'div' },
  { path: 'ched/enrolment-summary', component: EnrolmentSummaryComponent, outlet: 'div' },
  { path: 'ched/enrolment-list', component: EnrolmentListComponent, outlet: 'div' },
  { path: 'ched/NSTP', component: NstpComponent, outlet: 'div' },
  { path: 'Faculty/Classlist', component: ClassListComponent, outlet: 'div' },
  { path: 'Faculty/Code-summary', component: CodeSummaryComponent, outlet: 'div' },
  { path: 'app-manager', component: AppManagerComponent, outlet: 'div' },
  { path: 'enrollment-summary', component: EnrollmentSummaryComponent, outlet: 'div' },
  { path: 'exam-schedule', component: ExamScheduleComponent, outlet: 'div' },
  { path: 'student/enrolment-list', component: StudentEnrollmentListComponent, outlet: 'div' },
  { path: 'hris-schedule-manager', component: ScheduleManagerComponent, outlet: 'div' },
  { path: 'academic-company', component: CompanyComponent, outlet: 'div' },
  { path: 'online-registration', component: OnlineRegistrationComponent, outlet: 'div' },
  { path: 'entrance-exam-schedule-manager', component: EntranceExamScheduleManagerComponent, outlet: 'div' },
  { path: 'Report-Employee-Profile', component: EmployeeProfileComponent, outlet: 'div' },

  { path: 'Report-Student-Profile', component: StudentProfilesComponent, outlet: 'div' },
  { path: 'Report-Student-Corporate-Email', component: StudentCorporateEmailComponent, outlet: 'div' },
  { path: 'Corporate-Email', component: CorporateEmailComponent, outlet: 'div' },
  { path: 'Report-Student-Academic-Achievers', component: StudentAcademicAchieverComponent, outlet: 'div' },
  { path: 'Academic-academic-history', component: AcademicHistoryComponent, outlet: 'div' },
  { path: 'HRIS-Employee-Manager', component: EmployeeManagerComponent, outlet: 'div' },
  { path: 'Academic-Maintenance-Clear-Online-Submitted-Requirements', component: ClearOnlineSubmittedRequirementsComponent, outlet: 'div' },
  { path: 'Report-Seminars-Trainings', component: SeminarsTrainingsComponent, outlet: 'div' },

  { path: 'Faculty-Evaluation', component: FacultyEvaluationComponent, outlet: 'div' },
  { path: 'Faculty-Evaluation-settings', component: EvaluationSettingsComponent, outlet: 'div' },
  { path: 'Faculty-Evaluation-uploader', component: ProgramFacultyUploaderComponent, outlet: 'div' },

    {path: 'Schedule-Viewer-Component', component: ScheduleViewerComponent, outlet: 'div'},
    {path: 'Add-Individual-Faculty-Component', component: AddIndividualFacultyComponent, outlet: 'div'},
    {path: 'Program-Chair-Edit-Component', component: ProgramChairEditComponent, outlet: 'div'},
    {path: 'Area-Uploader-Component', component: AreaUploaderComponent, outlet: 'div'},
    {path: 'Add-Area-Component', component: AddAreaComponent, outlet: 'div'},
    {path: 'Add-Area-Head-Component', component: AddAreaHeadComponent, outlet: 'div'},
    {path: 'Edit-Area-Component', component: EditAreaComponent, outlet: 'div'},
    {path: 'Add-Individual-Area-Faculty-Component', component: AddIndividualAreaFacultyComponent, outlet: 'div'},
    {path: 'Search-Adviser-Component', component: SearchAdviserComponent, outlet: 'div'},
    {path: 'Search-Faculty-Component', component: SearchFacultyComponent, outlet: 'div'},
    {path: 'Time-Validation-Component', component: TimeValidationComponent, outlet: 'div'},
    {path: 'Code-Lookup-Component', component: CodeLookupComponent, outlet: 'div'},
    {path: 'Person-Finder-Component', component: PersonFinderComponent, outlet: 'div'},
    {path: 'Classroom-Manager-Component', component: ClassroomManagerComponent, outlet: 'div'},
    {path: 'Grades-hiding-unhiding-Component', component: GradesHidingUnhidingComponent, outlet: 'div'},
    {path: 'Dtr-Viewer-Component', component: DtrViewerComponent, outlet: 'div'},
    {path: 'Codes-Manager-Component', component: CodesManagerComponent, outlet: 'div'},
    {path: 'Accounts-Receivable-Component', component: AccountsReceivableComponent, outlet: 'div'},
    {path: 'Accounts-Payable-Component', component: AccountsPayableComponent, outlet: 'div'},
    {path: 'General-Ledger-Component', component: GeneralLedgerComponent, outlet: 'div'},
    {path: 'Collections-Component', component: CollectionComponent, outlet: 'div'},
    {path: 'Payroll-Component', component: PayrollComponent, outlet: 'div'},
    {path: 'OR-Manager-Component', component: ORManagerComponent, outlet: 'div'},
    {path: 'Accounts-Verification-Manager', component: AccountsVerificationManagerComponent, outlet: 'div'},
    {path: 'Payment-Person-Lookup', component: PaymentPersonLookupComponent, outlet: 'div'},
    {path: 'Registrar-Certification-Purpose', component: RegistrarCertificationPurposeComponent, outlet: 'div'},
    {path: 'otr-component', component: OtrComponent, outlet: 'div'},
    {path: 'Dtr-Uploader-Component', component: DtrUploaderComponent, outlet: 'div'},
  { path: 'Schedule-Viewer-Component', component: ScheduleViewerComponent, outlet: 'div' },
  { path: 'Add-Individual-Faculty-Component', component: AddIndividualFacultyComponent, outlet: 'div' },
  { path: 'Program-Chair-Edit-Component', component: ProgramChairEditComponent, outlet: 'div' },
  { path: 'Area-Uploader-Component', component: AreaUploaderComponent, outlet: 'div' },
  { path: 'Add-Area-Component', component: AddAreaComponent, outlet: 'div' },
  { path: 'Add-Area-Head-Component', component: AddAreaHeadComponent, outlet: 'div' },
  { path: 'Edit-Area-Component', component: EditAreaComponent, outlet: 'div' },
  { path: 'Add-Individual-Area-Faculty-Component', component: AddIndividualAreaFacultyComponent, outlet: 'div' },
  { path: 'Search-Adviser-Component', component: SearchAdviserComponent, outlet: 'div' },
  { path: 'Search-Faculty-Component', component: SearchFacultyComponent, outlet: 'div' },
  { path: 'Time-Validation-Component', component: TimeValidationComponent, outlet: 'div' },
  { path: 'Code-Lookup-Component', component: CodeLookupComponent, outlet: 'div' },
  { path: 'Person-Finder-Component', component: PersonFinderComponent, outlet: 'div' },
  { path: 'Classroom-Manager-Component', component: ClassroomManagerComponent, outlet: 'div' },
  { path: 'Grades-hiding-unhiding-Component', component: GradesHidingUnhidingComponent, outlet: 'div' },
  { path: 'Dtr-Viewer-Component', component: DtrViewerComponent, outlet: 'div' },
  { path: 'Codes-Manager-Component', component: CodesManagerComponent, outlet: 'div' },
  { path: 'Accounts-Receivable-Component', component: AccountsReceivableComponent, outlet: 'div' },
  { path: 'Accounts-Payable-Component', component: AccountsPayableComponent, outlet: 'div' },
  { path: 'General-Ledger-Component', component: GeneralLedgerComponent, outlet: 'div' },
  { path: 'Collections-Component', component: CollectionComponent, outlet: 'div' },
  { path: 'Payroll-Component', component: PayrollComponent, outlet: 'div' },
  { path: 'CheckDisbursement-Component', component: CheckDisbursementComponent, outlet: 'div' },
  { path: 'Withdraw-CES', component: WithdrawConditionallyEnrolledComponent, outlet: 'div' },
    {path: 'Other-Information-Component', component: OtherInformationComponent, outlet: 'div'},
    {path: 'Employment-Records-Component', component: EmploymentRecordsComponent, outlet: 'div'},
    {path: 'Rank-Record-Batch-Uploader-Component', component: RankRecordBatchUploaderComponent, outlet: 'div'},
    { path: 'Assessment-Setup', component: AssessmentSetupComponent, outlet: 'div' },




    {path: 'Other-Information-Component', component: OtherInformationComponent, outlet: 'div'},
    {path: 'Employment-Records-Component', component: EmploymentRecordsComponent, outlet: 'div'},
    {path: 'Add-Edit-Codes', component: AddEditCodesComponent, outlet: 'div'},
    {path: 'Job-Posting-Components', component: JobPostingComponent, outlet: 'div'},
    {path: 'RequirementList-Component', component: RequirementlistComponent, outlet: 'div'},
    {path: 'Active-Employee-Masterlist-Components', component: ActiveEmployeeMasterlistComponent, outlet: 'div'},
    {path: 'School-grad-from-Component', component: SchoolgradfromComponent, outlet: 'div'},
    {path: 'Lr-list-Component', component: LrlistComponent, outlet: 'div'},
   
    
    {path: 'End-Of-Serivce-Batch-Component', component: EndOfSerivceBatchComponent, outlet: 'div'},
    {path: 'Dissolution-And-Merging-Component', component: DissolutionAndMergingComponent, outlet: 'div'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AfterLoginRouteModule { }
