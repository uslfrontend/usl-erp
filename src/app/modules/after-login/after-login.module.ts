import { NgModule } from '@angular/core';
 import { AfterLoginRouteModule } from './after-login-route.module';
 import { PersonInformationComponent } from './../../general/person-information/person-information.component';
 import { UpdatePersonComponent } from './../../general/update-person/update-person.component';
import { MaterialModule } from './../../material.module';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './../../home/home.component';
import { ApplicationManagerComponent } from './../../hris/application-manager/application-manager.component';
import { EmployeeInformationComponent } from './../../hris/employee-information/employee-information.component';
import { LeaveManagementComponent } from './../../hris/leave-management/leave-management.component';
import { UserManagementComponent } from './../../control-panel/user-management/user-management.component';
import { StudentInformationComponent } from './../../academic/student-information/student-information.component';
import { AdmissionComponent } from './../../academic/admission/admission.component';
import { SchoolYearSettingComponent } from './../../control-panel/school-year-setting/school-year-setting.component';
import { EnrollmentManagerComponent } from './../../academic/enrollment-manager/enrollment-manager.component';
import { StudentRegistrationComponent } from './../../academic/student-registration/student-registration.component';
import { SetsManagerComponent } from './../../academic/sets-manager/sets-manager.component';
import { PurgingComponent } from './../../academic/purging/purging.component';
import { CurriculumComponent } from './../../academic/curriculum/curriculum.component';
import { SubjectsComponent } from './../../academic/curriculum/subjects/subjects.component';
import { DepartmentsComponent } from './../../hris/hris-maintenance/departments/departments.component';
import { PositionsComponent } from './../../hris/hris-maintenance/positions/positions.component';
import { RanksComponent } from './../../hris/hris-maintenance/ranks/ranks.component';
import { ItemsComponent } from './../../hris/hris-maintenance/items/items.component';
import { StudentEvaluationComponent } from './../../academic/student-evaluation/student-evaluation.component';
import { UserManagementStudentComponent } from './../../control-panel/user-management-student/user-management-student.component';
import { RoleManagementComponent } from './../../control-panel/role-management/role-management.component';
import { CodeProjectionComponent } from './../../reports/academic/pre-enrolment/code-projection/code-projection.component';
import { PlacementSummaryComponent } from './../../reports/academic/pre-enrolment/placement-summary/placement-summary.component';
import { EnrolmentListComponent } from './../../reports/ched/enrolment-list/enrolment-list.component';
import { EnrolmentSummaryComponent } from './../../reports/ched/enrolment-summary/enrolment-summary.component';
import { NstpComponent } from './../../reports/ched/nstp/nstp.component';
import { ClassListComponent } from './../../reports/academic/faculty/class-list/class-list.component';
import { CodeSummaryComponent } from './../../reports/academic/faculty/code-summary/code-summary.component';
import { AppManagerComponent } from './../../control-panel/app-manager/app-manager.component';
import { EnrollmentSummaryComponent } from './../../academic/enrollment-summary/enrollment-summary.component';
import { ExamScheduleComponent } from './../../academic/exam-schedule/exam-schedule.component';
import { StudentEnrollmentListComponent } from './../../reports/academic/student/student-enrollment-list/student-enrollment-list.component';
import { ScheduleManagerComponent } from './../../hris/schedule/schedule-manager/schedule-manager.component';
import { CompanyComponent } from './../../academic/maintenance/company/company.component';
import { NgxPaginationModule} from 'ngx-pagination';
import { OnlineRegistrationComponent } from './../../academic/online-registration/online-registration.component';
import { OnlineRegistrationUpdateComponent } from './../../academic/online-registration/online-registration-update/online-registration-update.component';
import { ApplicantSummaryComponent } from './../../reports/academic/applicant-summary/applicant-summary.component';
import { PersonAssignApplicantComponent } from './../../general/person-information/person-assign-applicant/person-assign-applicant.component';
import { PlacementManagerComponent } from './../../academic/placement-manager/placement-manager.component';
import { EntranceExamScheduleManagerComponent } from './../../academic/maintenance/entrance-exam-schedule-manager/entrance-exam-schedule-manager.component';
import { ModifyScheduleComponent } from './../../hris/schedule/pop-ups/modify-schedule/modify-schedule.component';
import { PopUpComponent } from './../../academic/maintenance/entrance-exam-schedule-manager/pop-up/pop-up.component';
import { StudentEnrollmentStatisticComponent } from './../../reports/academic/student/student-enrollment-statistic/student-enrollment-statistic.component';
import { StudentEnrollmentSummaryComponent } from './../../reports/academic/student/student-enrollment-summary/student-enrollment-summary.component';
import { G9ElectiveComponent } from './../../academic/g9-elective/g9-elective.component';
import { G9ElectiveUpdateComponent } from './../../academic/g9-elective/g9-elective-update/g9-elective-update.component';
import { ModalityComponent } from './../../academic/modality/modality.component';
import { SectionManagerComponent } from './../../academic/section-manager/section-manager.component';
import { UploadSectionComponent } from './../../academic/section-manager/upload-section/upload-section.component';
import { BasicEdScheduleManagerComponent } from './../../academic/basic-ed-schedule-manager/basic-ed-schedule-manager.component';
import { ReportModalitiesComponent } from './../../reports/academic/report-modalities/report-modalities.component';
import { SesPopupComponent } from './../../reports/academic/student/student-enrollment-summary/ses-popup/ses-popup.component';
import { OnlineRegistrationVerifyComponent } from './../../academic/online-registration/online-registration-verify/online-registration-verify.component';
import { PlacementReportcardComponent } from './../../academic/placement-manager/placement-reportcard/placement-reportcard.component';
import { OGSComponent } from './../../academic/ogs/ogs.component';
import { AdmittedDialogComponent } from './../../reports/academic/faculty/class-list/admitted-dialog/admitted-dialog.component';

import { EmployeeProfileComponent } from './../../reports/hris/employee-profile/employee-profile.component';

import { SharedServicesService } from './../../reports/shared-services.service';

import { CollegePreenrollmentComponent } from './../../academic/college-preenrollment/college-preenrollment.component';
import { CollegePreenrollmentPopupComponent } from './../../academic/college-preenrollment/college-preenrollment-popup/college-preenrollment-popup.component';

import { EmployeeProfileDetailsComponent } from './../../reports/hris/employee-profile/employee-profile-details/employee-profile-details.component';
import { CollegePreenrolmentstatDepartmentComponent } from './../../reports/academic/pre-enrolment/college/college-preenrolmentstat-department/college-preenrolmentstat-department.component';
import { CollegePreenrolmentstatCourseComponent } from './../../reports/academic/pre-enrolment/college/college-preenrolmentstat-course/college-preenrolmentstat-course.component';
import { ApplicantListReportRecommendedComponent } from './../../reports/academic/pre-enrolment/applicant-list-report-recommended/applicant-list-report-recommended.component';
import { ApplicantTextBlastComponent } from './../../academic/online-registration/applicant-text-blast/applicant-text-blast.component';

import { LoadingApplicantTextBlastComponent } from './../../academic/online-registration/applicant-text-blast/loading-applicant-text-blast/loading-applicant-text-blast.component';

import {StudentProfilesComponent} from './../../reports/academic/student/student-profiles/student-profiles.component';
import{AgeComponent} from './../../reports/academic/student/student-profiles/age/age.component';
import {GenderComponent} from './../../reports/academic/student/student-profiles/gender/gender.component';
import {ReligionComponent} from  './../../reports/academic/student/student-profiles/religion/religion.component';
import {HomePlaceComponent} from './../../reports/academic/student/student-profiles/home-place/home-place.component';
import {SeniorHighschoolGraduatedFromComponent} from './../../reports/academic/student/student-profiles/senior-highschool-graduated-from/senior-highschool-graduated-from.component';
import {JuniorHighschoolGraduatedFromComponent} from './../../reports/academic/student/student-profiles/junior-highschool-graduated-from/junior-highschool-graduated-from.component';
import {StudentCorporateEmailComponent} from './../../reports/academic/student/student-corporate-email/student-corporate-email.component';
import {CorporateEmailComponent} from './../../control-panel/corporate-email/corporate-email.component';
import {StudentAcademicAchieverComponent} from './../../reports/academic/student/student-academic-achiever/student-academic-achiever.component';
import {AcademicHistoryComponent} from './../../academic/academic-history/academic-history.component';
import {AcademicHistoryUpdateComponent} from './../../academic/academic-history/academic-history-update/academic-history-update.component';
import {AcademicHistoryAddComponent}from './../../academic/academic-history/academic-history-add/academic-history-add.component';
import {EmployeeManagerComponent} from './../../hris/employee-manager/employee-manager.component';
import {ManageActiveSyComponent} from './../../control-panel/school-year-setting/manage-sy/manage-active-sy/manage-active-sy.component';
import { ManageOnlineRegistrationSettingComponent } from '../../control-panel/school-year-setting/manage-sy/manage-online-registration-setting/manage-online-registration-setting.component';
import {DtridLookupComponent} from './../../hris/employee-manager/dtrid-lookup/dtrid-lookup.component';
import {ClearOnlineSubmittedRequirementsComponent} from './../../academic/maintenance/clear-online-submitted-requirements/clear-online-submitted-requirements.component';
import{ClearOnlineSubmittedRequirementsApplicantComponent} from './../../academic/maintenance/clear-online-submitted-requirements/applicant/applicant.component';
import{ClearOnlineSubmittedRequirementsPreenrollmentComponent} from './../../academic/maintenance/clear-online-submitted-requirements/preenrollment/preenrollment.component';

import { SeminarsTrainingsComponent } from './../../reports/hris/seminars-trainings/seminars-trainings.component';



import { FacultyEvaluationComponent } from './../../academic/faculty-evaluation-manager/faculty-evaluation/faculty-evaluation.component';
import { EvaluationSettingsComponent } from './../../academic/faculty-evaluation-manager/evaluation-settings/evaluation-settings.component';
import { ProgramFacultyUploaderComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/program-faculty-uploader.component';

import { ScheduleViewerComponent } from './../../academic/basic-ed-schedule-manager/schedule-viewer/schedule-viewer.component';
import { ClassScheduleComponent } from './../../academic/student-information/class-schedule/class-schedule.component';
import { BasicedClassScheduleComponent } from './../../academic/student-information/basiced-class-schedule/basiced-class-schedule.component';
import { CodeEvalDetailsComponent } from './../../academic/faculty-evaluation-manager/code-eval-details/code-eval-details.component';
import { AddIndividualFacultyComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/add-individual-faculty/add-individual-faculty.component';
import { ProgramChairEditComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/program-chair-edit/program-chair-edit.component';
import { AreaUploaderComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/area-uploader.component';
import { AddAreaHeadComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-area-head/add-area-head.component';
import { AddAreaComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-area/add-area.component';
import { EditAreaComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/edit-area/edit-area.component';
import { AddIndividualAreaFacultyComponent } from './../../academic/faculty-evaluation-manager/program-faculty-uploader/area-uploader/add-individual-area-faculty/add-individual-area-faculty.component';
import { SearchAdviserComponent } from './../../academic/basic-ed-schedule-manager/search-adviser/search-adviser.component';
import { SearchFacultyComponent } from './../../academic/basic-ed-schedule-manager//search-faculty/search-faculty.component';
import { TimeValidationComponent } from './../../academic/basic-ed-schedule-manager/time-validation/time-validation.component';

import { CodeLookupComponent } from './../../tools/code-lookup/code-lookup.component';
import { PersonFinderComponent } from './../../tools/person-finder/person-finder.component';

import { FacultyEvaluationTableDetailsComponent } from './../../academic/faculty-evaluation-manager/faculty-evaluation-table-details/faculty-evaluation-table-details.component';
import {MatChipsModule} from '@angular/material/chips';

import { ClassroomManagerComponent } from './../../academic/maintenance/classroom-manager/classroom-manager.component';
import { GradesHidingUnhidingComponent } from './../../academic/grades-hiding-unhiding/grades-hiding-unhiding.component';
import { DtrViewerComponent } from './../../reports/hris/dtr-viewer/dtr-viewer.component';
import { CodesManagerComponent } from '../../academic/codes-manager/codes-manager.component';
import { AccountsReceivableComponent } from '../../reports/fmis/accounts-receivable/accounts-receivable.component';
import { AccountsPayableComponent } from '../../reports/fmis/accounts-payable/accounts-payable.component';
import { GeneralLedgerComponent } from '../../reports/fmis/general-ledger/general-ledger.component';

import { CollectionComponent } from '../../fmis/accounts-receivable/collection/collection.component';
import { PayrollComponent } from '../../fmis/accounts-payable/payroll/payroll.component';
import { ORManagerComponent } from '../../fmis/or-manager/or-manager.component';
import { AssessmentSetupComponent } from '../../fmis/accounts-receivable/assessment-setup/assessment-setup.component';

import { AccountsVerificationManagerComponent } from '../../fmis/accounts-verification-manager/accounts-verification-manager.component';
import { PaymentPersonLookupComponent } from '../../fmis/accounts-verification-manager/payment-person-lookup/payment-person-lookup.component';
import { DtrUploaderComponent } from '../../hris/dtr-uploader/dtr-uploader.component';
import { RegistrarCertificationPurposeComponent } from '../../academic/student-information/registrar-certification-purpose/registrar-certification-purpose.component';
import { OtrComponent } from '../../academic/otr/otr.component';
import { OtherInformationComponent } from '../../hris/employee-information/other-information/other-information.component';
import { EmploymentRecordsComponent } from '../../hris/employee-information/employment-records/employment-records.component';
import { CheckDisbursementComponent } from '../../fmis/accounts-payable/check-disbursement/check-disbursement.component';
import { CheckVoucherComponent } from '../../fmis/general-ledger/check-voucher/check-voucher.component';
import { JournalVoucherComponent } from '../../fmis/general-ledger/journal-voucher/journal-voucher.component';
import { WithdrawConditionallyEnrolledComponent } from '../../academic/withdraw-conditionally-enrolled/withdraw-conditionally-enrolled.component';
import { RankRecordBatchUploaderComponent } from '../../hris/employee-manager/rank-record-batch-uploader/rank-record-batch-uploader.component';
import { AddEditCodesComponent } from '../../academic/codes-manager/add-edit-codes/add-edit-codes.component';
import { JobPostingComponent } from '../../hris/job-posting/job-posting.component';
import { RequirementlistComponent } from '../../reports/academic/student/requirementlist/requirementlist.component';


import { ActiveEmployeeMasterlistComponent } from '../../reports/hris/active-employee-masterlist/active-employee-masterlist.component';
import { UploadIdSignatureComponent } from '../../academic/student-information/upload-id-signature/upload-id-signature.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SchoolgradfromComponent } from '../../reports/academic/schoolgradfrom/schoolgradfrom.component';
import { LrlistComponent } from '../../reports/academic/student/lrlist/lrlist.component';
import { EndOfSerivceBatchComponent } from '../../hris/employee-information/employee-records/end-of-serivce-batch/end-of-serivce-batch.component';
import { DissolutionAndMergingComponent } from '../../academic/codes-manager/dissolution-and-merging/dissolution-and-merging.component';

@NgModule({
  imports: [
    CommonModule,
    AfterLoginRouteModule,
    MaterialModule,
    NgxPaginationModule,MatChipsModule,ImageCropperModule

  ],
  declarations: [
    PersonInformationComponent,
    UpdatePersonComponent,
    HomeComponent,
    ApplicationManagerComponent,
    EmployeeInformationComponent,
    LeaveManagementComponent,
    UserManagementComponent,
    StudentInformationComponent,
    AdmissionComponent,
    SchoolYearSettingComponent,
    EnrollmentManagerComponent,
    StudentRegistrationComponent,
    SetsManagerComponent,
    PurgingComponent,
    CurriculumComponent ,
    SubjectsComponent,
    DepartmentsComponent,
    PositionsComponent,
    RanksComponent,
    ItemsComponent,
    StudentEvaluationComponent,
    UserManagementStudentComponent,
    RoleManagementComponent,
    CodeProjectionComponent,
    PlacementSummaryComponent,
    EnrolmentListComponent,
    EnrolmentSummaryComponent,
    NstpComponent,
    ClassListComponent,
    CodeSummaryComponent,
    AppManagerComponent,
    EnrollmentSummaryComponent,
    ExamScheduleComponent,
    StudentEnrollmentListComponent,
    ScheduleManagerComponent,
    CompanyComponent,
    OnlineRegistrationComponent,
    OnlineRegistrationUpdateComponent,
    ApplicantSummaryComponent,
    PersonAssignApplicantComponent,
    PlacementManagerComponent,
    EntranceExamScheduleManagerComponent,
    ModifyScheduleComponent,
    PopUpComponent,
    StudentEnrollmentStatisticComponent,
    G9ElectiveComponent,
    StudentEnrollmentSummaryComponent,
    G9ElectiveUpdateComponent,
    ModalityComponent,
    SectionManagerComponent,
    UploadSectionComponent,
    BasicEdScheduleManagerComponent,
    ReportModalitiesComponent,
    SesPopupComponent,
    OnlineRegistrationVerifyComponent,
    PlacementReportcardComponent,
    OGSComponent,
    AdmittedDialogComponent,
    EmployeeProfileComponent,
    CollegePreenrollmentComponent,
    CollegePreenrollmentPopupComponent,
    EmployeeProfileDetailsComponent,
    CollegePreenrolmentstatDepartmentComponent,
    CollegePreenrolmentstatCourseComponent,
    ApplicantListReportRecommendedComponent,
    ApplicantTextBlastComponent,
    LoadingApplicantTextBlastComponent,
    StudentProfilesComponent,
    AgeComponent,
    GenderComponent,
    ReligionComponent,
    HomePlaceComponent,
    SeniorHighschoolGraduatedFromComponent,
    JuniorHighschoolGraduatedFromComponent,
    StudentCorporateEmailComponent,
    CorporateEmailComponent,
    StudentAcademicAchieverComponent,
    AcademicHistoryComponent,
    AcademicHistoryUpdateComponent,
    AcademicHistoryAddComponent,
    EmployeeManagerComponent,
    ManageActiveSyComponent,
    ManageOnlineRegistrationSettingComponent,
    DtridLookupComponent,
    ClearOnlineSubmittedRequirementsComponent,
    ClearOnlineSubmittedRequirementsApplicantComponent,
    ClearOnlineSubmittedRequirementsPreenrollmentComponent,
    SeminarsTrainingsComponent,

    FacultyEvaluationComponent,
    EvaluationSettingsComponent,
    ProgramFacultyUploaderComponent,CodeEvalDetailsComponent,

    ScheduleViewerComponent,
    ClassScheduleComponent,
    BasicedClassScheduleComponent,
    AddIndividualFacultyComponent,
    ProgramChairEditComponent,
    AreaUploaderComponent,
    AddAreaComponent,
    AddAreaHeadComponent,
    EditAreaComponent,
    AddIndividualAreaFacultyComponent,
    FacultyEvaluationTableDetailsComponent,
    SearchFacultyComponent,
    SearchAdviserComponent,

    TimeValidationComponent,
    CodeLookupComponent,
    PersonFinderComponent,
    ClassroomManagerComponent,
    GradesHidingUnhidingComponent,
    DtrViewerComponent,
    CodesManagerComponent,
    AccountsReceivableComponent,
    AccountsPayableComponent,
    GeneralLedgerComponent,
    CollectionComponent,
    PayrollComponent,
    ORManagerComponent,

    AccountsVerificationManagerComponent,
    PaymentPersonLookupComponent,
    DtrUploaderComponent,
    RegistrarCertificationPurposeComponent,
    OtrComponent,
    OtherInformationComponent,
    EmploymentRecordsComponent,
    CheckDisbursementComponent,
    CheckVoucherComponent,
    JournalVoucherComponent,
    WithdrawConditionallyEnrolledComponent,
    RankRecordBatchUploaderComponent,
    AddEditCodesComponent,
    JobPostingComponent,
    AssessmentSetupComponent,
    RequirementlistComponent,
    ActiveEmployeeMasterlistComponent,
    UploadIdSignatureComponent,
    SchoolgradfromComponent,
    LrlistComponent,
    EndOfSerivceBatchComponent,
    DissolutionAndMergingComponent
  ],
   entryComponents: [
    ScheduleManagerComponent,
    LeaveManagementComponent,
    OnlineRegistrationUpdateComponent,
    PersonAssignApplicantComponent,
    ModifyScheduleComponent,
    PopUpComponent,
    G9ElectiveUpdateComponent,
    UploadSectionComponent,
    SesPopupComponent,
    OnlineRegistrationVerifyComponent,
    PlacementReportcardComponent,
    AdmittedDialogComponent,
    CollegePreenrollmentPopupComponent,
    EmployeeProfileDetailsComponent,
    ApplicantTextBlastComponent,
    LoadingApplicantTextBlastComponent,
    AcademicHistoryUpdateComponent,
    AcademicHistoryAddComponent,
    DtridLookupComponent,
    SeminarsTrainingsComponent,
    ClassScheduleComponent,
    BasicedClassScheduleComponent,
    AddAreaComponent,
    AddAreaHeadComponent,
    EditAreaComponent,
    AddIndividualAreaFacultyComponent,
    FacultyEvaluationTableDetailsComponent,
    SearchFacultyComponent,
    SearchAdviserComponent,
    UploadIdSignatureComponent
   ]
})
export class AfterLoginModule { }
