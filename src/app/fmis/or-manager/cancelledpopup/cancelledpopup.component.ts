import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { GlobalService } from './../../../global.service';

@Component({
  selector: 'app-cancelledpopup',
  templateUrl: './cancelledpopup.component.html',
  styleUrls: ['./cancelledpopup.component.css']
})
export class CancelledpopupComponent {
  reason: string = '';

  constructor(public dialogRef: MatDialogRef<CancelledpopupComponent>, public global: GlobalService) { }

  cancel() {
    this.dialogRef.close();
  }

  submit() {
    // Check if the reason field is not empty
    if (this.reason.trim()) {
      this.dialogRef.close({ reason: this.reason.trim() });
    }else{
      this.global.swalAlert("Error","Please fill out remarks","warning")
    }
  }
}