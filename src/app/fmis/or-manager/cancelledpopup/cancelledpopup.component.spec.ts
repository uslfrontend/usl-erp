import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelledpopupComponent } from './cancelledpopup.component';

describe('CancelledpopupComponent', () => {
  let component: CancelledpopupComponent;
  let fixture: ComponentFixture<CancelledpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
