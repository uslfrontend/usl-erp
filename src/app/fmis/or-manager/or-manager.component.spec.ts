import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ORManagerComponent } from './or-manager.component';

describe('ORManagerComponent', () => {
  let component: ORManagerComponent;
  let fixture: ComponentFixture<ORManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ORManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ORManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
