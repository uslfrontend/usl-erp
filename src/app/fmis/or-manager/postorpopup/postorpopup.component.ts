import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-postorpopup',
  templateUrl: './postorpopup.component.html',
  styleUrls: ['./postorpopup.component.css']
})
export class PostorpopupComponent {


  constructor(
    public dialogRef: MatDialogRef<PostorpopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.dialogRef.close('yes');
  }

}
