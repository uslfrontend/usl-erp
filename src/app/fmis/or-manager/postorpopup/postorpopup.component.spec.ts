import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostorpopupComponent } from './postorpopup.component';

describe('PostorpopupComponent', () => {
  let component: PostorpopupComponent;
  let fixture: ComponentFixture<PostorpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostorpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostorpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
