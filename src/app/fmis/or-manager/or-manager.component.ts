import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { CancelledpopupComponent } from './cancelledpopup/cancelledpopup.component';
import { PostorpopupComponent } from './postorpopup/postorpopup.component';
import { MatDialog } from '@angular/material/dialog';

import Swal from 'sweetalert2';
const swal = Swal;


@Component({
  selector: 'app-or-manager',
  templateUrl: './or-manager.component.html',
  styleUrls: ['./or-manager.component.css']
})
export class ORManagerComponent implements OnInit {

  // transNumber='1521334';
  // ORinfoArray
  officialReceiptARLedgers = []
  // officialReceiptDetails=[]


  transNumber: number;
  id: number;
  remarks: string;
  status: number;
  ORinfoArray: any;
  isPrinted: boolean = false;
  isPosted: boolean = false;
  isCancelled: boolean = false;
  isExpanded1 = true;
  reason: string;

  constructor(public global: GlobalService, private api: ApiService, private dialog: MatDialog) { }

  ngOnInit() {

    this.ORinfo();

  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.ORinfo();

    }
  }
  

  ORinfo() {
    if (!this.transNumber) {
      return;
    }
    this.api.getOfficialReceiptInfo(this.transNumber).map(response => response.json()).subscribe(result => {
      if (result.data == null && result.message == 'Sequence contains no elements') {
        this.global.swalAlert(result.message, '', 'warning');
      } else {
        this.ORinfoArray = result;
        if (result && result.data && result.data.officialReceiptHeader) {
          this.isPrinted = (this.ORinfoArray.data.officialReceiptHeader.printed === 1);
          this.isPosted = (this.ORinfoArray.data.officialReceiptHeader.posted === 1);
          this.isCancelled = (this.ORinfoArray.data.officialReceiptHeader.status === 1);
          this.id = this.ORinfoArray.data.officialReceiptHeader.id;

        } else {
          // Handle the case where the object is null or undefined
        } 
      }
    }, (error) => {
      console.log(error);
    });
  }


  postORDialog() {
    // const dialogRef = this.dialog.open(PostorpopupComponent, {
    //   width: '450px',
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   if (result === 'yes') {
    //     this.postOR();
    //   }
    // });
    this.swalConfirm("Confirm", "Are you sure you want to post the Official Receipt?", "info", "Post", "Official Receipt Posted");
    
  }
  swalConfirm(title,text,type,button,successm){
    swal.fire({
        title: title,
        html: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          this.postOR();
        }
      })
  }
  
               
  postOR() {
    // Check if the OR is already posted
    if (this.isPosted) {
      console.warn('The OR is already posted.');
      this.global.swalAlertError('Official Receipt already posted.');
    } else {
      this.isPosted = false;
      if (this.ORinfoArray && this.ORinfoArray.data && this.ORinfoArray.data.officialReceiptHeader) {
        this.api.postOfficialReceipt(this.id).map(response => response.json()).subscribe(result => {
          this.id = this.ORinfoArray.id;
          this.ORinfoArray.data.officialReceiptHeader.posted = 1;
          this.isPosted = true;
          this.global.swalAlert("Posted Successfully", "", 'success');
          this.keyDownFunction('onoutfocus')
        },
          (error) => {
            this.global.swalAlertError(error);
          });
      }
    }
  }


  cancelledDialog() {          
    const dialogRef = this.dialog.open(CancelledpopupComponent, {
      width: '600px',
    });
      
    dialogRef.afterClosed().subscribe((result) => {
      if (result && result.reason) {
        // Update the status field to 1 and set the remarks to the reason provided
        const updatedData = { 
          ...this.ORinfoArray.data.officialReceiptHeader,
          remarks: result.reason,
          status: 1,
        };
        this.api.putOfficialReceiptCancelled(this.transNumber, updatedData.status, updatedData.remarks).subscribe(response => {
          this.ORinfoArray.data.officialReceiptHeader.status = 1;
          this.isCancelled = true;
          this.global.swalAlert("Official Receipt is Successfully Cancelled", "", 'success');
          this.keyDownFunction('onoutfocus')
        }, error => {
          console.log(error);
        });
      } 
    });
  }

}

