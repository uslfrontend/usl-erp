import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  arrSubjects: any;
  totalUnits: any
  totaltfAmount: any
  totallabfee: any
  totalContact: any
  constructor(public dialogRef: MatDialogRef<SubjectsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.arrSubjects = data.arrSubjects;
     this.totalUnits = data.totalUnits;
     this.totaltfAmount = data.totaltfAmount;
    this.totallabfee = data.totallabfee;
    this.totalContact = data.totalContact;
  }
  onNoClickclose(): void {
    
    this.dialogRef.close({ result: 'cancel' });
  }
  ngOnInit() {
  
  }

}
