import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.css']
})
export class AssessmentComponent implements OnInit {
  // idNumber = '2000177'
  // schoolyear = '2022231'
  arrAssessment: any;
  arrDues: any;
  totalTF: any
  totalMF: any
  totalSF: any
  totalCredit: any
  totalDebit: any
  totalContactFee: any
  totalLabFee: any
  totalcompFee: any
  totalprevBalance: any
  desc: any
  prelims: any
  midterms: any
  finals: any
  statusfinals: any
  statusmidterm: any
  statusprelim: any
  entrancestatus: any
  duePrelim: any
  dueMidterm: any
  dueFinals: any
  totalDues:any

  //----------------//
  Entrance: any
  EntranceStatus: any
  EntranceDue: any

  Prelims1st: any
  Prelims1stStatus: any
  Prelims1stDue: any

  Midterms2nd: any
  Midterms2ndStatus: any
  Midterms2ndDue: any

  Grading3rd: any
  Grading3rdStatus: any
  Grading3rdDue: any

  Finals: any
  FinalsStatus: any
  FinalsDue: any
//----------------//

  constructor(public http: HttpClient, public dialogRef: MatDialogRef<AssessmentComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService) {
    this.arrAssessment = data.arrAssessment;
    this.arrDues = data.arrDues;
    this.totalTF = data.totalTF
    this.totalMF = data.totalMF
    this.totalSF = data.totalSF
    this.totalCredit = data.totalCredit
    this.totalDebit = data.totalDebit
    this.totalContactFee = data.totalContactFee
    this.totalLabFee = data.totalLabFee
    this.totalcompFee = data.totalcompFee
    this.totalprevBalance = data.totalprevBalance
    this.desc = data.desc
    this.prelims = data.prelims
    this.midterms = data.midterms
    this.finals = data.finals
    this.statusfinals = data.statusfinals
    this.statusmidterm = data.statusmidterm
    this.statusprelim = data.statusprelim
    this.entrancestatus = data.entrancestatus
    this.duePrelim = data.duePrelim
    this.dueMidterm = data.dueMidterm
    this.dueFinals = data.dueFinals
    this.totalDues = data.totalDues

//-----------------------------------//
    this.Entrance = data.Entrance
    this.EntranceStatus = data.EntranceStatus
    this.EntranceDue = data.EntranceDue
  
    this.Prelims1st = data.Prelims1st
    this.Prelims1stStatus = data.Prelims1stStatus
    this.Prelims1stDue = data.Prelims1stDue
  
    this.Midterms2nd = data.Midterms2nd
    this.Midterms2ndStatus = data.Midterms2ndStatus
    this.Midterms2ndDue = data.Midterms2ndDue
  
    this.Grading3rd = data.Grading3rd
    this.Grading3rdStatus = data.Grading3rdStatus
    this.Grading3rdDue = data.Grading3rdDue
  
    this.Finals = data.Finals
    this.FinalsStatus = data.FinalsStatus
    this.FinalsDue = data.FinalsDue
//------------------------------------//
  }
  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
  ngOnInit() {

    // const alphabet = "abcdefghijklmnopqrstuvwxyz";
    // const tableData = [{ name: "Abulug"}, { name: "Aparri"},{ name: "Amulung"},
    // { name: "Baggao"},
    // { name: "Buguey"},
    // { name: "Calayan"},
    // { name: "Claveria"},
    // { name: "Camalaniugan"}, ];

    // for (let i = 0; i < alphabet.length; i++) {
    //   const columnName = alphabet[i];
    //   tableData.forEach((item) => {
    //     if (item.name[0].toLowerCase() === columnName) {
    //       item[columnName] = item.name;
    //     } else {
    //       item[columnName] = null;
    //     }
    //   });
    // }

    // console.table(tableData);
    // function addColumnsByAlphabet(tableData: { name }[]) {
    //   const alphabet = "abcdefghijklmnopqrstuvwxyz";

    //   alphabet.split("").forEach((columnName) => {
    //     tableData.forEach((item) => {
    //       item[columnName] = item.name[0].toLowerCase() === columnName ? item.name : null;
    //     });
    //   });
    // }

    // const tableData = [
    //   { name: "Abulug" },
    //   { name: "Aparri" },
    //   { name: "Amulung" },
    //   { name: "Baggao" },
    //   { name: "Buguey" },
    //   { name: "Calayan" },
    //   { name: "Claveria" },
    //   { name: "Camalaniugan" },
    // ];

    // addColumnsByAlphabet(tableData);
    // console.table(tableData);

  }

  checkAssessmentStatus(tf) {
    if (tf == 1)
      return '✓'
    else
      return ''
  }
  getTotalAssessment() {
    // console.log('temparary',this.tempArray)
    const totalAmount = this.arrAssessment.reduce((acc, curr) => acc + curr.amount, 0);

    return parseFloat(totalAmount.toFixed(2));
  }
}
