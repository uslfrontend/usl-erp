import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PersonLookupComponent } from './../../../academic/lookup/person-lookup/person-lookup.component';
import { MatDialog } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatExpansionModule } from '@angular/material/expansion';
import { MiscellaneousComponent } from '../miscellaneous/miscellaneous.component';
import { AssessmentComponent } from '../assessment/assessment.component';
import { LedgerComponent } from '../ledger/ledger.component';
import { SubjectsComponent } from '../subjects/subjects.component';
import { HttpClient } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { StudentlookupComponent } from '../studentlookup/studentlookup.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from "@angular/router";


// import { DueDate } from 'igniteui-angular-excel';


@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {


  // -----------SAMPLE------------//
  // options: string[] = ['Elem', 'HSSci', 'College', 'GS'];
  value: string;
  viewValue: string;
  Fees = [
    { value: 'tuition', viewValue: 'Tuition Fees', remarks: 'This option covers the cost of tuition for the semester.' },
    { value: 'books', viewValue: 'Textbook Fees', remarks: 'This option covers the cost of required textbooks for the semester.' },
    { value: 'housing', viewValue: 'Housing Fees', remarks: 'This option covers the cost of on-campus housing for the semester.' },
    { value: 'meal', viewValue: 'Meal Plan Fees', remarks: 'This option covers the cost of a meal plan for the semester.' }
  ];
  totalBalance = '9,234.79'
  previousBalance = '6,389.09'
  entranceFee = 'CLEARED'
  tuitionFee = '2500'
  dormFee = '1500'
  // prelims = 'CLEARED'
  // midterms = '2500'
  // finals = '3500'
  or = ' 00002'
  ordate = '03/07/2023'
  //--------------------------//

  // ----------FMIS---------- //
  image: any = 'assets/noimage.jpg';
  schoolYear = ''
  department = ''
  studentinfo
  name = ''
  address = '';
  course = ''
  gender = ''
  year = ''
  status = ''
  idno = ''
  idNumber = ''
  schoolyearFormatted = ''

  TestDUES: []
  arrAssessment: any[] = [];
  arrLedger: any[] = [];
  arrSubjects: any[] = [];
  arrDues: any[] = [];
  ArrParticulars = []
  tempArrayMF = []
  tempArraySF = []
  tempArrayTF = []
  totalTF = 0
  totalMF = 0
  totalSF = 0

  totalCredit = 0
  tempArrayCredit = []
  totalDebit = 0
  tempArrayDebit = []

  contactFee = []
  totalContactFee = 0

  labFee = []
  totalLabFee = 0

  compFee = []
  totalcompFee = 0

  PrevBalance = []
  totalprevBalance = 0

  DuesEntrance = []
  desc = ''
  entrancestatus: any
  dueEntrance = ''


  DuesPrelims = []
  prelims = ''
  statusprelim: any
  duePrelim = ''

  DuesMidterms = []
  midterms = ''
  statusmidterm: any
  dueMidterm = ''

  DuesFinals = []
  finals = ''
  statusfinals: any
  dueFinals = ''

  totalDues = 0;

  tempArrayParticulars = []
  filteredParticulars: Observable<any[]>;
  particulars = ''
  myControl = new FormControl();

  totalfees = 0

  currentStatus: any
  amountDue: any

  a
  c
  y1
  y2

  proglevel = '';
  activeterm = 0;
  term

  totalUnits = 0
  totaltfAmount = 0
  totallabfee = 0
  totalContact = 0


  // Dues = []

  Entrance = ''
  Prelims1st = ''
  Midterms2nd = ''
  Grading3rd = ''
  Finals = ''

  EntranceStatus: any
  EntranceDue: any

  Prelims1stStatus
  Prelims1stDue

  Midterms2ndStatus
  Midterms2ndDue

  Grading3rdStatus
  Grading3rdDue

  FinalsStatus
  FinalsDue
  // prelimsDues:any []= [];
  //--------------------------//

  // province ='Cagayan'
  // towncities =''
  // 0800346 HSSci
  // 1501170 Elem
  // 1802766 GS
  OTRinfo = ''
  Address = ''
  AdditionalRemarks = ''
  courseCollege = ''
  courseDoctoral = ''
  courseMasteral = ''
  coursePB = ''
  courseTV = ''
  fullName = ''
  highSchool = ''
  IDNumber = ''
  idPicture = ''
  intermediate = ''
  remarks = ''

  constructor(public http: HttpClient, public dialog: MatDialog, public global: GlobalService, private api: ApiService, private domSanitizer: DomSanitizer, private cd: ChangeDetectorRef, private cookieService: CookieService, private router: Router) { }


  Test() {
    // console.log(this.c, this.y1.toString(), '-', this.y2.toString())
    // console.log(this.proglevel)
    // console.log(this.b)
    // console.log('SY',this.global.allsyoptions)
    // console.log('GlobSettings',this.global.sysetting)
    // console.log('Settings',this.syDisplay)
    // console.log('SchoolYear',this.schoolYear)
    // console.log('Formatted',this.y1.toString()+this.y2.toString().substr(1).substr(1)+this.term.toString())
    // console.log('Level',this.proglevel)
    // console.log('Term',this.term)
    // console.log('--------')
    console.log(this.TestDUES)
  }

  ngOnInit() {
    this.filteredParticulars = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterParticulars(value))
    );
    if (this.idNumber != '') {
      this.idNumber
      this.keyDownFunction('onoutfocus')
    }

  }
  getStatusColor(status: string): string {
    switch (status) {
      case 'Admitted':
        return 'gray';
      case 'Enrolled':
        return 'black';
      case 'Dropped':
        return 'black';
      case 'Withdrawn with Permission':
        return 'red';
      case 'Paid':
        return 'green';
      default:
        return 'black'; // default color for unknown status
    }
  }


  //SchoolYearFormat
  syDisplay(x) {
    this.y1 = x.substring(0, 4)
    this.y2 = parseInt(this.y1) + 1
    this.a = this.y1.toString() + " - " + this.y2.toString();
    this.term = x.substring(6, 7)
    this.c
    if (this.term == '1')
      this.c = "1st Sem"
    else if (this.term == '2')
      this.c = "2nd Sem"
    else if (this.term == '3')
      this.c = "Summer"
    else
      this.c = ""
    return this.c + " SY " + this.a

  }
  yearchange1() {
    this.y2 = this.y1 + 1;
  }
  yearchange2() {
    this.y1 = this.y2 - 1;
  }

  //search
  studentlookup(): void {
    const dialogRef = this.dialog.open(StudentlookupComponent, {
      width: '600px', disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.idNumber = result.result;
        this.keyDownFunction('onoutfocus')

      }
    });
  }

  //
  convertToNumber(value: string | number): number {
    return value === 'CLEARED' ? 0 : Number(value);
  }

  //CLEAR VALUE
  clear() {
    this.arrLedger = []
    this.arrAssessment = []
    this.arrSubjects = []

    this.tempArrayMF = []
    this.tempArraySF = []
    this.tempArrayTF = []

    this.tempArrayCredit = []
    this.tempArrayDebit = []
    this.totalCredit = 0
    this.totalDebit = 0
    this.totalTF = 0

    this.totalMF = 0
    this.totalSF = 0

    this.contactFee = []
    this.totalContactFee = 0

    this.labFee = []
    this.totalLabFee = 0

    this.compFee = []
    this.totalcompFee = 0

    this.PrevBalance = []
    this.totalprevBalance = 0

    this.entranceFee = ''
    this.statusprelim = ''
    this.statusmidterm = ''
    this.statusfinals = ''
    this.duePrelim = ''
    this.dueMidterm = ''
    this.dueFinals = ''
    this.totalDues = 0
    this.particulars = ''
    this.totalfees = 0
    this.currentStatus = ''
    this.amountDue = 0
    this.particulars = ''
    this.totalUnits = 0
    this.totaltfAmount = 0
    this.totallabfee = 0
    this.totalContact = 0

    this.Entrance = ''
    this.Prelims1st = ''
    this.Midterms2nd = ''
    this.Grading3rd = ''
    this.Finals = ''

    this.EntranceStatus = 0
    this.EntranceDue = ''

    this.Prelims1stStatus
    this.Prelims1stDue

    this.Midterms2ndStatus
    this.Midterms2ndDue

    this.Grading3rdStatus
    this.Grading3rdDue

    this.FinalsStatus
    this.FinalsDue

    // this.Dues = []
  }

  //DIALOG
  Miscellaneous() {
    const dialogRef = this.dialog.open(MiscellaneousComponent, {
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  Assessment() {
    const dialogRef = this.dialog.open(AssessmentComponent, {
      data: {
        arrAssessment: this.arrAssessment,
        arrDues: this.arrDues,
        totalTF: this.totalTF,
        totalMF: this.totalMF,
        totalSF: this.totalSF,
        totalCredit: this.totalCredit,
        totalDebit: this.totalDebit,
        totalContactFee: this.totalContactFee,
        totalLabFee: this.totalLabFee,
        totalcompFee: this.totalcompFee,
        totalprevBalance: this.totalprevBalance,
        desc: this.desc,
        prelims: this.prelims,
        midterms: this.midterms,
        finals: this.finals,
        statusfinals: this.statusfinals,
        statusmidterm: this.statusmidterm,
        statusprelim: this.statusprelim,
        entrancestatus: this.entrancestatus,
        duePrelim: this.duePrelim,
        dueMidterm: this.dueMidterm,
        dueFinals: this.dueFinals,
        totalDues: this.totalDues,

        Entrance: this.Entrance,
        EntranceStatus: this.EntranceStatus,
        EntranceDue: this.EntranceDue,

        Prelims1st: this.Prelims1st,
        Prelims1stStatus: this.Prelims1stStatus,
        Prelims1stDue: this.Prelims1stDue,

        Midterms2nd: this.Midterms2nd,
        Midterms2ndStatus: this.Midterms2ndStatus,
        Midterms2ndDue: this.Midterms2ndDue,

        Grading3rd: this.Grading3rd,
        Grading3rdStatus: this.Grading3rdStatus,
        Grading3rdDue: this.Grading3rdDue,

        Finals: this.Finals,
        FinalsStatus: this.FinalsStatus,
        FinalsDue: this.FinalsDue

      }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  Ledger() {
    const dialogRef = this.dialog.open(LedgerComponent, {
      data: {
        arrLedger: this.arrLedger
      }
    });
    dialogRef.afterClosed().subscribe(result => {

      // console.log(`Dialog result: ${result}`);
    });
  }
  Subjects() {
    const dialogRef = this.dialog.open(SubjectsComponent, {
      data: {
        arrSubjects: this.arrSubjects,
        totalUnits: this.totalUnits,
        totaltfAmount: this.totaltfAmount,
        totallabfee: this.totallabfee,
        totalContact: this.totalContact
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  //ActiveConfigLocal
  setschoolyear() {
    let errorMsg = '';

    // Check if the entered year is valid
    if (this.y1 < 1900 || this.y2 > 2999) {
      errorMsg += '*Must Enter Appropriate School Year\n';
    }

    // Check if a program level is selected
    if (!this.proglevel) {
      errorMsg += '*Program level is required\n';
    }

    // Check if the active term is required
    if (this.proglevel === 'College' || this.proglevel === 'GS') {
      if (!this.term) {
        errorMsg += '*Active Term is required\n';
      }
    } else {
      this.term = '1'; // Set default term for elementary and high school
    }

    // Check if the selected school year is valid
    let validSchoolYear = false;
    const schoolYearWithTerm = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
    // console.log('SchoolYearWithTerm',schoolYearWithTerm)
    for (let i = 0; i < this.global.allsyoptions.length; i++) {
      if (schoolYearWithTerm === this.global.allsyoptions[i].syWithSem) {
        validSchoolYear = true;

      }
    }

    if (!validSchoolYear) {
      errorMsg += '*Invalid school year setting!\n';

    }

    if (errorMsg) {
      if (this.global.allsyoptions.length === 0) {
        this.global.swalAlert('No School Year Found!', 'Please contact your system administrator!', 'warning');
      } else {
        this.global.swalAlert(errorMsg, '', 'warning');
      }
      return;
    }

    // Update the school year variable
    if (this.term === '3') {
      // Summer term
      this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + '3';
    } else {
      // Regular semester
      this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
    }

    // Save the program level and school year to cookies
    // this.cookieService.set('domain', this.proglevel);
    // this.cookieService.set('year', this.schoolYear);
    this.schoolYear = this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString();
    console.log('Save', this.schoolYear)

    // Display the updated school year and term
    // this.syDisplay(this.schoolYear);
    // this.schoolYear = this.syDisplay(this.y1.toString() + this.y2.toString().substr(1).substr(1) + this.term.toString());

    this.UpdateFunction()

  }
  //
  private _filterParticulars(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.tempArrayParticulars.filter(a => a.particulars.toLowerCase().includes(filterValue));
  }
  onOptionSelected(event) {
    this.particulars = event.option.value;
    console.log('PARTICULAR', this.particulars)
  }
  //





  //MAIN First execution
  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.idNumber != '') {
        this.clear();

        this.global.swalLoading('Loading Person Information');

        // get student info
        this.api.getPOS_DuesParticulars().map(response => response.json()).subscribe(result => {
          if (result.data != null) {

            this.ArrParticulars = result.data
            // console.log(this.ArrParticulars)
            for (var x = 0; x < this.ArrParticulars.length; x++) {

              this.tempArrayParticulars.push({
                particulars: this.ArrParticulars[x].particulars
              })
            }
            // console.log(this.tempArrayParticulars)
          }
          else {
            this.global.swalClose()
            // this.global.swalAlert('NOTE!', 'DUESPARTICULAR NULL', 'warning')
          }
        },
          Error => {
            this.global.swalAlertError(Error);
          });

        this.api.getPOS_StudentInfo(this.idNumber, this.schoolYear, this.department).map(response => response.json()).subscribe(result => {

          if (result.data != null) {
            // if(this.department == result.data.department && this.department == null){
            // console.log(this.idNumber)
            this.studentinfo = result.data;
            this.name = result.data.name;
            this.status = result.data.status;
            this.course = result.data.course;
            this.year = result.data.year;
            this.gender = result.data.gender.toUpperCase();
            this.address = result.data.address.toUpperCase();
            this.idno = result.data.idno;
            this.idNumber = result.data.idNumber;
            // this.schoolYear = result.data.schoolYear
            this.schoolYear = this.global.syear
            this.schoolyearFormatted = this.syDisplay(this.schoolYear)
            // this.schoolYear = this.global.syDisplay(this.schoolYear)
            this.proglevel = result.data.department
            //get id pic
            this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + result.data.idPicture);
            //----------------------------------------------------------//

            if (this.status === 'Enrolled' || this.status === 'Dropped' || this.status === 'Withdrawn with Permission' || this.status === 'Paid') {
              this.api.getPOS_Assessment(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {

                if (result.data == null) {
                  this.Back2CurrentSY()
                }
                if (result.data != null) {
                  this.totalfees = result.data
                  this.arrAssessment = result.data.assessmentSelects;
                  this.arrLedger = result.data.aRLedgerSelects;
                  this.arrSubjects = result.data.studentSubjectWithFeesSelects;


                  // console.table(this.arrSubjects)
                  for (let x = 0; x < this.arrSubjects.length; x++) {
                    this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                    this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                    this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                    this.totalContact = this.totalContact + this.arrSubjects[x].contact;
                  }

                  // console.log('Total Contact', this.totalContact)
                  // console.log('Total Lab Fee', this.totallabfee)
                  // console.log('Total Amount', this.totaltfAmount)
                  // console.log("Total Units: ", this.totalUnits);

                  //-------------ARLedgerSelects-------------//
                  for (var x = 0; x < this.arrLedger.length; x++) {

                    if (this.arrLedger[x].transType == "C") {
                      this.tempArrayCredit.push({
                        amount: this.arrLedger[x].amount,
                        transType: this.arrLedger[x].transType
                      })

                    }
                    if (this.arrLedger[x].transType == "D") {
                      this.tempArrayDebit.push({
                        amount: this.arrLedger[x].amount,
                        transType: this.arrLedger[x].transType
                      })

                    }

                    if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                      this.PrevBalance.push({
                        amount: this.arrLedger[x].amount,
                        particulars: this.arrLedger[x].particulars,
                        transType: this.arrLedger[x].transType
                      })
                      // console.log(this.PrevBalance)
                    }
                  }

                  //this.TestDUES = result.data.previousBalance
                  // console.log(this.arrLedger)

                  for (x = 0; x < this.PrevBalance.length; x++) {
                    if (this.PrevBalance[x].transType === 'C') {
                      this.totalprevBalance -= this.PrevBalance[x].amount;
                    } else if (this.PrevBalance[x].transType === 'D') {
                      this.totalprevBalance += this.PrevBalance[x].amount;
                    }
                  }

                  for (var x = 0; x < this.tempArrayCredit.length; x++) {
                    this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
                  }


                  for (var x = 0; x < this.tempArrayDebit.length; x++) {
                    this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
                  }

                  // console.log(this.totalCredit)
                  // console.log(this.totalDebit)


                  //-------------AsssessmentSelects-------------//
                  for (var x = 0; x < this.arrAssessment.length; x++) {
                    if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                      this.tempArrayMF.push({
                        amount: this.arrAssessment[x].amount,
                        mf: this.arrAssessment[x].mf,

                      })
                    }
                    if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                      this.tempArraySF.push({
                        amount: this.arrAssessment[x].amount,
                        sf: this.arrAssessment[x].sf,

                      })
                    }
                    if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                      this.tempArrayTF.push({
                        amount: this.arrAssessment[x].amount,
                        tf: this.arrAssessment[x].tf,

                      })
                    }

                    if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                      this.contactFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }

                    if (this.arrAssessment[x].description == "LAB FEE") {

                      this.labFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }

                    if (this.arrAssessment[x].description == "COMPUTER FEE") {

                      this.compFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }


                  }

                  for (var x = 0; x < this.labFee.length; x++) {
                    this.totalLabFee = this.totalLabFee + this.labFee[x].amount

                  }

                  for (var x = 0; x < this.contactFee.length; x++) {
                    this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

                  }

                  for (var x = 0; x < this.compFee.length; x++) {
                    this.totalcompFee = this.totalcompFee + this.compFee[x].amount

                  }


                  for (var x = 0; x < this.tempArrayMF.length; x++) {
                    this.totalMF = this.totalMF + this.tempArrayMF[x].amount

                  }
                  // console.log('totalMF', this.totalMF)


                  for (var x = 0; x < this.tempArraySF.length; x++) {
                    this.totalSF = this.totalSF + this.tempArraySF[x].amount
                  }

                  // console.log('totalSF', this.totalSF)


                  for (var x = 0; x < this.tempArrayTF.length; x++) {
                    this.totalTF = this.totalTF + this.tempArrayTF[x].amount

                  }
                  // console.log('totalTF', this.totalTF)
                }
                else {
                  this.global.swalClose()
                  // this.global.swalAlert(result.message, 'ASSESSMENT NULL', 'warning')
                }
              }, Error => {
                this.global.swalAlertError(Error);
              });

            }

            else {
              this.api.getPOS_AssessmentNew(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
                if (result.data != null) {
                  this.totalfees = result.data
                  this.arrAssessment = result.data.assessmentSelects;
                  this.arrLedger = result.data.aRLedgerSelects;
                  this.arrSubjects = result.data.studentSubjectWithFeesSelects;


                  // console.table(this.arrSubjects)
                  for (let x = 0; x < this.arrSubjects.length; x++) {
                    this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                    this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                    this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                    this.totalContact = this.totalContact + this.arrSubjects[x].contact;
                  }

                  // console.log('Total Contact', this.totalContact)
                  // console.log('Total Lab Fee', this.totallabfee)
                  // console.log('Total Amount', this.totaltfAmount)
                  // console.log("Total Units: ", this.totalUnits);

                  //-------------ARLedgerSelects-------------//
                  for (var x = 0; x < this.arrLedger.length; x++) {

                    if (this.arrLedger[x].transType == "C") {
                      this.tempArrayCredit.push({
                        amount: this.arrLedger[x].amount,
                        transType: this.arrLedger[x].transType
                      })

                    }
                    if (this.arrLedger[x].transType == "D") {
                      this.tempArrayDebit.push({
                        amount: this.arrLedger[x].amount,
                        transType: this.arrLedger[x].transType
                      })

                    }

                    if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                      this.PrevBalance.push({
                        amount: this.arrLedger[x].amount,
                        particulars: this.arrLedger[x].particulars,
                        transType: this.arrLedger[x].transType
                      })
                      // console.log(this.PrevBalance)
                    }
                  }

                  //this.TestDUES = result.data.previousBalance
                  // console.log(this.arrLedger)

                  for (x = 0; x < this.PrevBalance.length; x++) {
                    if (this.PrevBalance[x].transType === 'C') {
                      this.totalprevBalance -= this.PrevBalance[x].amount;
                    } else if (this.PrevBalance[x].transType === 'D') {
                      this.totalprevBalance += this.PrevBalance[x].amount;
                    }
                  }

                  for (var x = 0; x < this.tempArrayCredit.length; x++) {
                    this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
                  }


                  for (var x = 0; x < this.tempArrayDebit.length; x++) {
                    this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
                  }

                  // console.log(this.totalCredit)
                  // console.log(this.totalDebit)


                  //-------------AsssessmentSelects-------------//
                  for (var x = 0; x < this.arrAssessment.length; x++) {
                    if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                      this.tempArrayMF.push({
                        amount: this.arrAssessment[x].amount,
                        mf: this.arrAssessment[x].mf,

                      })
                    }
                    if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                      this.tempArraySF.push({
                        amount: this.arrAssessment[x].amount,
                        sf: this.arrAssessment[x].sf,

                      })
                    }
                    if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                      this.tempArrayTF.push({
                        amount: this.arrAssessment[x].amount,
                        tf: this.arrAssessment[x].tf,

                      })
                    }

                    if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                      this.contactFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }

                    if (this.arrAssessment[x].description == "LAB FEE") {

                      this.labFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }

                    if (this.arrAssessment[x].description == "COMPUTER FEE") {

                      this.compFee.push({
                        description: this.arrAssessment[x].description,
                        amount: this.arrAssessment[x].amount,

                      })
                    }


                  }

                  for (var x = 0; x < this.labFee.length; x++) {
                    this.totalLabFee = this.totalLabFee + this.labFee[x].amount

                  }

                  for (var x = 0; x < this.contactFee.length; x++) {
                    this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

                  }

                  for (var x = 0; x < this.compFee.length; x++) {
                    this.totalcompFee = this.totalcompFee + this.compFee[x].amount

                  }


                  for (var x = 0; x < this.tempArrayMF.length; x++) {
                    this.totalMF = this.totalMF + this.tempArrayMF[x].amount

                  }
                  // console.log('totalMF', this.totalMF)


                  for (var x = 0; x < this.tempArraySF.length; x++) {
                    this.totalSF = this.totalSF + this.tempArraySF[x].amount
                  }

                  // console.log('totalSF', this.totalSF)


                  for (var x = 0; x < this.tempArrayTF.length; x++) {
                    this.totalTF = this.totalTF + this.tempArrayTF[x].amount

                  }
                  // console.log('totalTF', this.totalTF)
                }
                else {
                  this.global.swalClose()
                  // this.global.swalAlert(result.message, 'ASSESSMENTNEW NULL', 'warning')
                }
              }, Error => {
                this.global.swalAlertError(Error);
              });
            }
            //----------------------------------------------------------//

            //codes here
            this.api.getPOS_DuesBreakdown(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
              if (result.data != null) {

                this.TestDUES = result.data.previousBalance
                this.arrDues = result.data.dueDates;
                // console.log('Dues', this.arrDues)
                // let totalStatusFees = 0;
                var currentDate = new Date();

                for (var x = 0; x < this.arrDues.length; x++) {

                  if (this.arrDues[x].description == "Entrance Fee") {
                    this.Entrance = this.arrDues[x].description
                    this.EntranceStatus = this.convertToNumber(this.arrDues[x].status);
                    this.EntranceDue = this.arrDues[x].dueDate
                    console.log(this.Entrance, this.EntranceStatus, this.EntranceDue)
                  }

                  if (this.arrDues[x].description == "1st Grading" || this.arrDues[x].description == "Prelims") {
                    this.Prelims1st = this.arrDues[x].description
                    this.Prelims1stStatus = this.convertToNumber(this.arrDues[x].status);
                    this.Prelims1stDue = this.arrDues[x].dueDate
                    console.log(this.Prelims1st, this.Prelims1stStatus, this.Prelims1stDue)
                  }

                  if (this.arrDues[x].description == "2nd Grading" || this.arrDues[x].description == "Midterms") {
                    this.Midterms2nd = this.arrDues[x].description
                    this.Midterms2ndStatus = this.convertToNumber(this.arrDues[x].status);
                    this.Midterms2ndDue = this.arrDues[x].dueDate
                    console.log(this.Midterms2nd, this.Midterms2ndStatus, this.Midterms2ndDue)
                  }

                  if (this.arrDues[x].description == "3rd Grading") {
                    this.Grading3rd = this.arrDues[x].description
                    this.Grading3rdStatus = this.convertToNumber(this.arrDues[x].status);
                    this.Grading3rdDue = this.arrDues[x].dueDate
                    console.log(this.Grading3rd, this.Grading3rdStatus, this.Grading3rdDue)
                  }

                  if (this.arrDues[x].description == "Finals") {
                    this.Finals = this.arrDues[x].description
                    this.FinalsStatus = this.convertToNumber(this.arrDues[x].status);
                    this.FinalsDue = this.arrDues[x].dueDate
                    console.log(this.Finals, this.FinalsStatus, this.FinalsDue)
                  }

                }
                // for (var x = 0; x < this.arrDues.length; x++) {
                //   var dueDit = new Date(this.arrDues[x].dueDate)
                //   if (this.arrDues[x].description == "Entrance Fee") {
                //     this.desc = this.arrDues[x].description
                //     this.entrancestatus = this.convertToNumber(this.arrDues[x].status);
                //     this.dueEntrance = this.arrDues[x].dueDate
                //   }

                //   if (this.arrDues[x].description == "Prelims") {
                //     this.prelims = this.arrDues[x].description
                //     if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
                //       this.statusprelim = 0.00; // status will be = CLEARED
                //       this.statusmidterm = this.convertToNumber(this.arrDues[x].status);
                //     }
                //     else
                //       this.statusprelim = this.convertToNumber(this.arrDues[x].status)
                //     this.duePrelim = this.arrDues[x].dueDate
                //   }

                //   if (this.arrDues[x].description == "Midterms") {

                //     this.midterms = this.arrDues[x].description
                //     if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
                //       this.statusmidterm = 0.00;
                //       this.statusfinals = this.arrDues[x].status;
                //     }
                //     else {
                //       if (this.convertToNumber(this.arrDues[x].status).toString() != 'CLEARED') {
                //         this.statusmidterm = parseInt(this.statusmidterm) + this.convertToNumber(this.arrDues[x].status)
                //       }
                //     }
                //     this.dueMidterm = this.arrDues[x].dueDate
                //   }

                //   if (this.arrDues[x].description == "Finals") {

                //     this.finals = this.arrDues[x].description
                //     if (this.statusfinals > 0)
                //       this.statusfinals = parseInt(this.statusfinals) + this.convertToNumber(this.arrDues[x].status)
                //     else
                //       this.statusfinals = this.convertToNumber(this.arrDues[x].status)
                //     this.dueFinals = this.arrDues[x].dueDate
                //   }

                // }

                if (this.statusmidterm.toString() == "CLEARED") {
                  this.currentStatus = "Finals"
                  this.amountDue = this.statusfinals
                } else if (this.statusfinals.toString() != "CLEARED") {
                  this.currentStatus = "Midterms";
                  this.amountDue = this.statusmidterm;
                } else if (this.statusmidterm.toString() != "CLEARED") {
                  this.currentStatus = "Prelims";
                  this.amountDue = this.statusprelim;
                } else if (this.entrancestatus.toString() != "CLEARED") {
                  this.currentStatus = "Entrance Fee";
                  this.amountDue = this.entrancestatus;
                } else {
                  this.currentStatus = "All dues are cleared.";
                }

                if (this.Prelims1stStatus !== 'CLEARED') {
                  this.totalDues += Number(this.Prelims1stStatus);
                }
                if (this.Midterms2ndStatus !== 'CLEARED') {
                  this.totalDues += Number(this.Midterms2ndStatus);
                }
                if (this.Grading3rdStatus !== 'CLEARED') {
                  this.totalDues += Number(this.Grading3rdStatus);
                }
                if (this.FinalsStatus !== 'CLEARED') {
                  this.totalDues += Number(this.FinalsStatus);
                }

                // console.log('Total Dues', this.totalDues)
                this.global.swalClose()
              }
              else {
                this.global.swalClose()
                // this.global.swalAlert(result.message, 'DEUSBREAKDOWN NULL', 'warning')
              }
            }, Error => {
              this.global.swalAlertError(Error);
            });


          }
          else {
            this.global.swalAlert('NOTE!', 'NO Student Information is associated with this ID Number', 'warning')

          }
        },
          Error => {
            this.global.swalAlertError(Error);
          });
      }
    }
  }

  //UpdatingSY
  UpdateFunction() {


    if (this.idNumber != '') {
      this.clear();
      this.global.swalLoading('Loading Person Information');

      // get student info
      this.api.getPOS_DuesParticulars().map(response => response.json()).subscribe(result => {
        if (result.data != null) {

          this.ArrParticulars = result.data
          // console.log(this.ArrParticulars)
          for (var x = 0; x < this.ArrParticulars.length; x++) {

            this.tempArrayParticulars.push({
              particulars: this.ArrParticulars[x].particulars
            })
          }
          // console.log(this.tempArrayParticulars)
        }
        else {
          this.global.swalClose()
          // this.global.swalAlert('NOTE!', 'DUESPARTICULAR NULL', 'warning')
        }
      },
        Error => {
          this.global.swalAlertError(Error);
        });

      this.api.getPOS_StudentInfo(this.idNumber, this.schoolYear, this.department).map(response => response.json()).subscribe(result => {

        if (result.data != null) {
          // if(this.department == result.data.department && this.department == null){
          // console.log(this.idNumber)
          this.studentinfo = result.data;
          this.name = result.data.name;
          this.status = result.data.status;
          this.course = result.data.course;
          this.year = result.data.year;
          this.gender = result.data.gender.toUpperCase();
          this.address = result.data.address.toUpperCase();
          this.idno = result.data.idno;
          this.idNumber = result.data.idNumber;
          // this.schoolYear = result.data.schoolYear
          // this.schoolYear = this.global.syear
          this.schoolyearFormatted = this.syDisplay(this.schoolYear)
          // this.schoolYear = this.global.syDisplay(this.schoolYear)
          this.proglevel = result.data.department
          //get id pic
          this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + result.data.idPicture);
          //----------------------------------------------------------//

          if (this.status === 'Enrolled' || this.status === 'Dropped' || this.status === 'Withdrawn with Permission' || this.status === 'Paid') {
            this.api.getPOS_Assessment(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {

              if (result.data == null) {

                this.Back2CurrentSY()

              }

              if (result.data != null) {
                this.totalfees = result.data
                this.arrAssessment = result.data.assessmentSelects;
                this.arrLedger = result.data.aRLedgerSelects;
                this.arrSubjects = result.data.studentSubjectWithFeesSelects;


                // console.table(this.arrSubjects)
                for (let x = 0; x < this.arrSubjects.length; x++) {
                  this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                  this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                  this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                  this.totalContact = this.totalContact + this.arrSubjects[x].contact;
                }

                // console.log('Total Contact', this.totalContact)
                // console.log('Total Lab Fee', this.totallabfee)
                // console.log('Total Amount', this.totaltfAmount)
                // console.log("Total Units: ", this.totalUnits);

                //-------------ARLedgerSelects-------------//
                for (var x = 0; x < this.arrLedger.length; x++) {

                  if (this.arrLedger[x].transType == "C") {
                    this.tempArrayCredit.push({
                      amount: this.arrLedger[x].amount,
                      transType: this.arrLedger[x].transType
                    })

                  }
                  if (this.arrLedger[x].transType == "D") {
                    this.tempArrayDebit.push({
                      amount: this.arrLedger[x].amount,
                      transType: this.arrLedger[x].transType
                    })

                  }

                  if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                    this.PrevBalance.push({
                      amount: this.arrLedger[x].amount,
                      particulars: this.arrLedger[x].particulars,
                      transType: this.arrLedger[x].transType
                    })
                    // console.log(this.PrevBalance)
                  }
                }

                //this.TestDUES = result.data.previousBalance
                // console.log(this.arrLedger)

                for (x = 0; x < this.PrevBalance.length; x++) {
                  if (this.PrevBalance[x].transType === 'C') {
                    this.totalprevBalance -= this.PrevBalance[x].amount;
                  } else if (this.PrevBalance[x].transType === 'D') {
                    this.totalprevBalance += this.PrevBalance[x].amount;
                  }
                }

                for (var x = 0; x < this.tempArrayCredit.length; x++) {
                  this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
                }


                for (var x = 0; x < this.tempArrayDebit.length; x++) {
                  this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
                }

                // console.log(this.totalCredit)
                // console.log(this.totalDebit)


                //-------------AsssessmentSelects-------------//
                for (var x = 0; x < this.arrAssessment.length; x++) {
                  if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                    this.tempArrayMF.push({
                      amount: this.arrAssessment[x].amount,
                      mf: this.arrAssessment[x].mf,

                    })
                  }
                  if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                    this.tempArraySF.push({
                      amount: this.arrAssessment[x].amount,
                      sf: this.arrAssessment[x].sf,

                    })
                  }
                  if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                    this.tempArrayTF.push({
                      amount: this.arrAssessment[x].amount,
                      tf: this.arrAssessment[x].tf,

                    })
                  }

                  if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                    this.contactFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }

                  if (this.arrAssessment[x].description == "LAB FEE") {

                    this.labFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }

                  if (this.arrAssessment[x].description == "COMPUTER FEE") {

                    this.compFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }


                }

                for (var x = 0; x < this.labFee.length; x++) {
                  this.totalLabFee = this.totalLabFee + this.labFee[x].amount

                }

                for (var x = 0; x < this.contactFee.length; x++) {
                  this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

                }

                for (var x = 0; x < this.compFee.length; x++) {
                  this.totalcompFee = this.totalcompFee + this.compFee[x].amount

                }


                for (var x = 0; x < this.tempArrayMF.length; x++) {
                  this.totalMF = this.totalMF + this.tempArrayMF[x].amount

                }
                // console.log('totalMF', this.totalMF)


                for (var x = 0; x < this.tempArraySF.length; x++) {
                  this.totalSF = this.totalSF + this.tempArraySF[x].amount
                }

                // console.log('totalSF', this.totalSF)


                for (var x = 0; x < this.tempArrayTF.length; x++) {
                  this.totalTF = this.totalTF + this.tempArrayTF[x].amount

                }
                // console.log('totalTF', this.totalTF)
              }
              else {
                this.global.swalClose()
                // this.global.swalAlert(result.message, 'ASSESSMENT NULL', 'warning')
              }
            }, Error => {
              this.global.swalAlertError(Error);
            });

          }

          else {
            this.api.getPOS_AssessmentNew(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
              if (result.data != null) {
                this.totalfees = result.data
                this.arrAssessment = result.data.assessmentSelects;
                this.arrLedger = result.data.aRLedgerSelects;
                this.arrSubjects = result.data.studentSubjectWithFeesSelects;


                // console.table(this.arrSubjects)
                for (let x = 0; x < this.arrSubjects.length; x++) {
                  this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                  this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                  this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                  this.totalContact = this.totalContact + this.arrSubjects[x].contact;
                }

                // console.log('Total Contact', this.totalContact)
                // console.log('Total Lab Fee', this.totallabfee)
                // console.log('Total Amount', this.totaltfAmount)
                // console.log("Total Units: ", this.totalUnits);

                //-------------ARLedgerSelects-------------//
                for (var x = 0; x < this.arrLedger.length; x++) {

                  if (this.arrLedger[x].transType == "C") {
                    this.tempArrayCredit.push({
                      amount: this.arrLedger[x].amount,
                      transType: this.arrLedger[x].transType
                    })

                  }
                  if (this.arrLedger[x].transType == "D") {
                    this.tempArrayDebit.push({
                      amount: this.arrLedger[x].amount,
                      transType: this.arrLedger[x].transType
                    })

                  }

                  if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                    this.PrevBalance.push({
                      amount: this.arrLedger[x].amount,
                      particulars: this.arrLedger[x].particulars,
                      transType: this.arrLedger[x].transType
                    })
                    // console.log(this.PrevBalance)
                  }
                }

                //this.TestDUES = result.data.previousBalance
                // console.log(this.arrLedger)

                for (x = 0; x < this.PrevBalance.length; x++) {
                  if (this.PrevBalance[x].transType === 'C') {
                    this.totalprevBalance -= this.PrevBalance[x].amount;
                  } else if (this.PrevBalance[x].transType === 'D') {
                    this.totalprevBalance += this.PrevBalance[x].amount;
                  }
                }

                for (var x = 0; x < this.tempArrayCredit.length; x++) {
                  this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
                }


                for (var x = 0; x < this.tempArrayDebit.length; x++) {
                  this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
                }

                // console.log(this.totalCredit)
                // console.log(this.totalDebit)


                //-------------AsssessmentSelects-------------//
                for (var x = 0; x < this.arrAssessment.length; x++) {
                  if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                    this.tempArrayMF.push({
                      amount: this.arrAssessment[x].amount,
                      mf: this.arrAssessment[x].mf,

                    })
                  }
                  if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                    this.tempArraySF.push({
                      amount: this.arrAssessment[x].amount,
                      sf: this.arrAssessment[x].sf,

                    })
                  }
                  if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                    this.tempArrayTF.push({
                      amount: this.arrAssessment[x].amount,
                      tf: this.arrAssessment[x].tf,

                    })
                  }

                  if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                    this.contactFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }

                  if (this.arrAssessment[x].description == "LAB FEE") {

                    this.labFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }

                  if (this.arrAssessment[x].description == "COMPUTER FEE") {

                    this.compFee.push({
                      description: this.arrAssessment[x].description,
                      amount: this.arrAssessment[x].amount,

                    })
                  }


                }

                for (var x = 0; x < this.labFee.length; x++) {
                  this.totalLabFee = this.totalLabFee + this.labFee[x].amount

                }

                for (var x = 0; x < this.contactFee.length; x++) {
                  this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

                }

                for (var x = 0; x < this.compFee.length; x++) {
                  this.totalcompFee = this.totalcompFee + this.compFee[x].amount

                }


                for (var x = 0; x < this.tempArrayMF.length; x++) {
                  this.totalMF = this.totalMF + this.tempArrayMF[x].amount

                }
                // console.log('totalMF', this.totalMF)


                for (var x = 0; x < this.tempArraySF.length; x++) {
                  this.totalSF = this.totalSF + this.tempArraySF[x].amount
                }

                // console.log('totalSF', this.totalSF)


                for (var x = 0; x < this.tempArrayTF.length; x++) {
                  this.totalTF = this.totalTF + this.tempArrayTF[x].amount

                }
                // console.log('totalTF', this.totalTF)9
              }
              else {
                this.global.swalClose()
                // this.global.swalAlert(result.message, 'ASSESSMENTNEW NULL', 'warning')
              }
            }, Error => {
              this.global.swalAlertError(Error);
            });
          }
          //----------------------------------------------------------//

          //codes here
          this.api.getPOS_DuesBreakdown(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
            if (result.data != null) {

              this.TestDUES = result.data.previousBalance
              this.arrDues = result.data.dueDates;
              // console.log('Dues', this.arrDues)
              // let totalStatusFees = 0;
              var currentDate = new Date();

              for (var x = 0; x < this.arrDues.length; x++) {

                if (this.arrDues[x].description == "Entrance Fee") {
                  this.Entrance = this.arrDues[x].description
                  this.EntranceStatus = this.convertToNumber(this.arrDues[x].status);
                  this.EntranceDue = this.arrDues[x].dueDate
                  console.log(this.Entrance, this.EntranceStatus, this.EntranceDue)
                }

                if (this.arrDues[x].description == "1st Grading" || this.arrDues[x].description == "Prelims") {
                  this.Prelims1st = this.arrDues[x].description
                  this.Prelims1stStatus = this.convertToNumber(this.arrDues[x].status);
                  this.Prelims1stDue = this.arrDues[x].dueDate
                  console.log(this.Prelims1st, this.Prelims1stStatus, this.Prelims1stDue)
                }

                if (this.arrDues[x].description == "2nd Grading" || this.arrDues[x].description == "Midterms") {
                  this.Midterms2nd = this.arrDues[x].description
                  this.Midterms2ndStatus = this.convertToNumber(this.arrDues[x].status);
                  this.Midterms2ndDue = this.arrDues[x].dueDate
                  console.log(this.Midterms2nd, this.Midterms2ndStatus, this.Midterms2ndDue)
                }

                if (this.arrDues[x].description == "3rd Grading") {
                  this.Grading3rd = this.arrDues[x].description
                  this.Grading3rdStatus = this.convertToNumber(this.arrDues[x].status);
                  this.Grading3rdDue = this.arrDues[x].dueDate
                  console.log(this.Grading3rd, this.Grading3rdStatus, this.Grading3rdDue)
                }

                if (this.arrDues[x].description == "Finals") {
                  this.Finals = this.arrDues[x].description
                  this.FinalsStatus = this.convertToNumber(this.arrDues[x].status);
                  this.FinalsDue = this.arrDues[x].dueDate
                  console.log(this.Finals, this.FinalsStatus, this.FinalsDue)
                }

              }
              // for (var x = 0; x < this.arrDues.length; x++) {
              //   var dueDit = new Date(this.arrDues[x].dueDate)
              //   if (this.arrDues[x].description == "Entrance Fee") {
              //     this.desc = this.arrDues[x].description
              //     this.entrancestatus = this.convertToNumber(this.arrDues[x].status);
              //     this.dueEntrance = this.arrDues[x].dueDate
              //   }

              //   if (this.arrDues[x].description == "Prelims") {
              //     this.prelims = this.arrDues[x].description
              //     if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
              //       this.statusprelim = 0.00; // status will be = CLEARED
              //       this.statusmidterm = this.convertToNumber(this.arrDues[x].status);
              //     }
              //     else
              //       this.statusprelim = this.convertToNumber(this.arrDues[x].status)
              //     this.duePrelim = this.arrDues[x].dueDate
              //   }

              //   if (this.arrDues[x].description == "Midterms") {

              //     this.midterms = this.arrDues[x].description
              //     if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
              //       this.statusmidterm = 0.00;
              //       this.statusfinals = this.arrDues[x].status;
              //     }
              //     else {
              //       if (this.convertToNumber(this.arrDues[x].status).toString() != 'CLEARED') {
              //         this.statusmidterm = parseInt(this.statusmidterm) + this.convertToNumber(this.arrDues[x].status)
              //       }
              //     }
              //     this.dueMidterm = this.arrDues[x].dueDate
              //   }

              //   if (this.arrDues[x].description == "Finals") {

              //     this.finals = this.arrDues[x].description
              //     if (this.statusfinals > 0)
              //       this.statusfinals = parseInt(this.statusfinals) + this.convertToNumber(this.arrDues[x].status)
              //     else
              //       this.statusfinals = this.convertToNumber(this.arrDues[x].status)
              //     this.dueFinals = this.arrDues[x].dueDate
              //   }

              // }

              if (this.statusmidterm.toString() == "CLEARED") {
                this.currentStatus = "Finals"
                this.amountDue = this.statusfinals
              } else if (this.statusfinals.toString() != "CLEARED") {
                this.currentStatus = "Midterms";
                this.amountDue = this.statusmidterm;
              } else if (this.statusmidterm.toString() != "CLEARED") {
                this.currentStatus = "Prelims";
                this.amountDue = this.statusprelim;
              } else if (this.entrancestatus.toString() != "CLEARED") {
                this.currentStatus = "Entrance Fee";
                this.amountDue = this.entrancestatus;
              } else {
                this.currentStatus = "All dues are cleared.";
              }

              if (this.Prelims1stStatus !== 'CLEARED') {
                this.totalDues += Number(this.Prelims1stStatus);
              }
              if (this.Midterms2ndStatus !== 'CLEARED') {
                this.totalDues += Number(this.Midterms2ndStatus);
              }
              if (this.Grading3rdStatus !== 'CLEARED') {
                this.totalDues += Number(this.Grading3rdStatus);
              }
              if (this.FinalsStatus !== 'CLEARED') {
                this.totalDues += Number(this.FinalsStatus);
              }

              // console.log('Total Dues', this.totalDues)
              this.global.swalClose()
            }
            else {
              this.global.swalClose()
              // this.global.swalAlert(result.message, 'DEUSBREAKDOWN NULL', 'warning')
            }
          }, Error => {
            this.global.swalAlertError(Error);
          });


        }
        else {
          this.global.swalAlert('NOTE!', 'NO Student Information is associated with this ID Number', 'warning')
        }
      },
        Error => {
          this.global.swalAlertError(Error);
        });
    }
  }

  //RefreshBack2Current
  Back2CurrentSY() {//main

    this.clear();
    this.global.swalLoading('Loading Person Information');

    // get student info
    this.api.getPOS_DuesParticulars().map(response => response.json()).subscribe(result => {
      if (result.data != null) {

        this.ArrParticulars = result.data
        // console.log(this.ArrParticulars)
        for (var x = 0; x < this.ArrParticulars.length; x++) {

          this.tempArrayParticulars.push({
            particulars: this.ArrParticulars[x].particulars
          })
        }
        // console.log(this.tempArrayParticulars)
      }
      else {
        this.global.swalClose()
        // this.global.swalAlert('NOTE!', 'DUESPARTICULAR NULL', 'warning')
      }
    },
      Error => {
        this.global.swalAlertError(Error);
      });

    this.api.getPOS_StudentInfo(this.idNumber, this.schoolYear, this.department).map(response => response.json()).subscribe(result => {

      if (result.data != null) {
        // if(this.department == result.data.department && this.department == null){
        // console.log(this.idNumber)
        this.studentinfo = result.data;
        this.name = result.data.name;
        this.status = result.data.status;
        this.course = result.data.course;
        this.year = result.data.year;
        this.gender = result.data.gender.toUpperCase();
        this.address = result.data.address.toUpperCase();
        this.idno = result.data.idno;
        this.idNumber = result.data.idNumber;
        this.schoolYear = result.data.schoolYear
        // this.schoolYear = this.global.syear
        this.schoolyearFormatted = this.syDisplay(this.schoolYear)
        // this.schoolYear = this.global.syDisplay(this.schoolYear)
        this.proglevel = result.data.department
        //get id pic
        this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + result.data.idPicture);
        //----------------------------------------------------------//

        if (this.status === 'Enrolled' || this.status === 'Dropped' || this.status === 'Withdrawn with Permission' || this.status === 'Paid') {
          this.api.getPOS_Assessment(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {


            if (result.data != null) {
              this.totalfees = result.data
              this.arrAssessment = result.data.assessmentSelects;
              this.arrLedger = result.data.aRLedgerSelects;
              this.arrSubjects = result.data.studentSubjectWithFeesSelects;


              // console.table(this.arrSubjects)
              for (let x = 0; x < this.arrSubjects.length; x++) {
                this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                this.totalContact = this.totalContact + this.arrSubjects[x].contact;
              }

              // console.log('Total Contact', this.totalContact)
              // console.log('Total Lab Fee', this.totallabfee)
              // console.log('Total Amount', this.totaltfAmount)
              // console.log("Total Units: ", this.totalUnits);

              //-------------ARLedgerSelects-------------//
              for (var x = 0; x < this.arrLedger.length; x++) {

                if (this.arrLedger[x].transType == "C") {
                  this.tempArrayCredit.push({
                    amount: this.arrLedger[x].amount,
                    transType: this.arrLedger[x].transType
                  })

                }
                if (this.arrLedger[x].transType == "D") {
                  this.tempArrayDebit.push({
                    amount: this.arrLedger[x].amount,
                    transType: this.arrLedger[x].transType
                  })

                }

                if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                  this.PrevBalance.push({
                    amount: this.arrLedger[x].amount,
                    particulars: this.arrLedger[x].particulars,
                    transType: this.arrLedger[x].transType
                  })
                  // console.log(this.PrevBalance)
                }
              }

              //this.TestDUES = result.data.previousBalance
              // console.log(this.arrLedger)

              for (x = 0; x < this.PrevBalance.length; x++) {
                if (this.PrevBalance[x].transType === 'C') {
                  this.totalprevBalance -= this.PrevBalance[x].amount;
                } else if (this.PrevBalance[x].transType === 'D') {
                  this.totalprevBalance += this.PrevBalance[x].amount;
                }
              }

              for (var x = 0; x < this.tempArrayCredit.length; x++) {
                this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
              }


              for (var x = 0; x < this.tempArrayDebit.length; x++) {
                this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
              }

              // console.log(this.totalCredit)
              // console.log(this.totalDebit)


              //-------------AsssessmentSelects-------------//
              for (var x = 0; x < this.arrAssessment.length; x++) {
                if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                  this.tempArrayMF.push({
                    amount: this.arrAssessment[x].amount,
                    mf: this.arrAssessment[x].mf,

                  })
                }
                if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                  this.tempArraySF.push({
                    amount: this.arrAssessment[x].amount,
                    sf: this.arrAssessment[x].sf,

                  })
                }
                if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                  this.tempArrayTF.push({
                    amount: this.arrAssessment[x].amount,
                    tf: this.arrAssessment[x].tf,

                  })
                }

                if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                  this.contactFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }

                if (this.arrAssessment[x].description == "LAB FEE") {

                  this.labFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }

                if (this.arrAssessment[x].description == "COMPUTER FEE") {

                  this.compFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }


              }

              for (var x = 0; x < this.labFee.length; x++) {
                this.totalLabFee = this.totalLabFee + this.labFee[x].amount

              }

              for (var x = 0; x < this.contactFee.length; x++) {
                this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

              }

              for (var x = 0; x < this.compFee.length; x++) {
                this.totalcompFee = this.totalcompFee + this.compFee[x].amount

              }


              for (var x = 0; x < this.tempArrayMF.length; x++) {
                this.totalMF = this.totalMF + this.tempArrayMF[x].amount

              }
              // console.log('totalMF', this.totalMF)


              for (var x = 0; x < this.tempArraySF.length; x++) {
                this.totalSF = this.totalSF + this.tempArraySF[x].amount
              }

              // console.log('totalSF', this.totalSF)


              for (var x = 0; x < this.tempArrayTF.length; x++) {
                this.totalTF = this.totalTF + this.tempArrayTF[x].amount

              }
              // console.log('totalTF', this.totalTF)
            }
            else {
              this.global.swalClose()
              // this.global.swalAlert(result.message, 'ASSESSMENT NULL', 'warning')
            }
          }, Error => {
            this.global.swalAlertError(Error);
          });

        }

        else {
          this.api.getPOS_AssessmentNew(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
            if (result.data != null) {
              this.totalfees = result.data
              this.arrAssessment = result.data.assessmentSelects;
              this.arrLedger = result.data.aRLedgerSelects;
              this.arrSubjects = result.data.studentSubjectWithFeesSelects;


              // console.table(this.arrSubjects)
              for (let x = 0; x < this.arrSubjects.length; x++) {
                this.totalUnits = this.totalUnits + this.arrSubjects[x].units;
                this.totaltfAmount = this.totaltfAmount + this.arrSubjects[x].tfAmount;
                this.totallabfee = this.totallabfee + this.arrSubjects[x].labFee;
                this.totalContact = this.totalContact + this.arrSubjects[x].contact;
              }

              // console.log('Total Contact', this.totalContact)
              // console.log('Total Lab Fee', this.totallabfee)
              // console.log('Total Amount', this.totaltfAmount)
              // console.log("Total Units: ", this.totalUnits);

              //-------------ARLedgerSelects-------------//
              for (var x = 0; x < this.arrLedger.length; x++) {

                if (this.arrLedger[x].transType == "C") {
                  this.tempArrayCredit.push({
                    amount: this.arrLedger[x].amount,
                    transType: this.arrLedger[x].transType
                  })

                }
                if (this.arrLedger[x].transType == "D") {
                  this.tempArrayDebit.push({
                    amount: this.arrLedger[x].amount,
                    transType: this.arrLedger[x].transType
                  })

                }

                if (this.arrLedger[x].particulars == "PREVIOUS BALANCE") {
                  this.PrevBalance.push({
                    amount: this.arrLedger[x].amount,
                    particulars: this.arrLedger[x].particulars,
                    transType: this.arrLedger[x].transType
                  })
                  // console.log(this.PrevBalance)
                }
              }

              //this.TestDUES = result.data.previousBalance
              // console.log(this.arrLedger)

              for (x = 0; x < this.PrevBalance.length; x++) {
                if (this.PrevBalance[x].transType === 'C') {
                  this.totalprevBalance -= this.PrevBalance[x].amount;
                } else if (this.PrevBalance[x].transType === 'D') {
                  this.totalprevBalance += this.PrevBalance[x].amount;
                }
              }

              for (var x = 0; x < this.tempArrayCredit.length; x++) {
                this.totalCredit = this.totalCredit + this.tempArrayCredit[x].amount
              }


              for (var x = 0; x < this.tempArrayDebit.length; x++) {
                this.totalDebit = this.totalDebit + this.tempArrayDebit[x].amount
              }

              // console.log(this.totalCredit)
              // console.log(this.totalDebit)


              //-------------AsssessmentSelects-------------//
              for (var x = 0; x < this.arrAssessment.length; x++) {
                if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 0) {

                  this.tempArrayMF.push({
                    amount: this.arrAssessment[x].amount,
                    mf: this.arrAssessment[x].mf,

                  })
                }
                if (this.arrAssessment[x].mf == 1 && this.arrAssessment[x].sf == 1) {

                  this.tempArraySF.push({
                    amount: this.arrAssessment[x].amount,
                    sf: this.arrAssessment[x].sf,

                  })
                }
                if (this.arrAssessment[x].tf == 1 && this.arrAssessment[x].sf == 0 && this.arrAssessment[x].mf == 0) {

                  this.tempArrayTF.push({
                    amount: this.arrAssessment[x].amount,
                    tf: this.arrAssessment[x].tf,

                  })
                }

                if (this.arrAssessment[x].description == "LAB CONTACT FEE") {

                  this.contactFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }

                if (this.arrAssessment[x].description == "LAB FEE") {

                  this.labFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }

                if (this.arrAssessment[x].description == "COMPUTER FEE") {

                  this.compFee.push({
                    description: this.arrAssessment[x].description,
                    amount: this.arrAssessment[x].amount,

                  })
                }


              }

              for (var x = 0; x < this.labFee.length; x++) {
                this.totalLabFee = this.totalLabFee + this.labFee[x].amount

              }

              for (var x = 0; x < this.contactFee.length; x++) {
                this.totalContactFee = this.totalContactFee + this.contactFee[x].amount

              }

              for (var x = 0; x < this.compFee.length; x++) {
                this.totalcompFee = this.totalcompFee + this.compFee[x].amount

              }


              for (var x = 0; x < this.tempArrayMF.length; x++) {
                this.totalMF = this.totalMF + this.tempArrayMF[x].amount

              }
              // console.log('totalMF', this.totalMF)


              for (var x = 0; x < this.tempArraySF.length; x++) {
                this.totalSF = this.totalSF + this.tempArraySF[x].amount
              }

              // console.log('totalSF', this.totalSF)


              for (var x = 0; x < this.tempArrayTF.length; x++) {
                this.totalTF = this.totalTF + this.tempArrayTF[x].amount

              }
              // console.log('totalTF', this.totalTF)
            }
            else {
              this.global.swalClose()
              // this.global.swalAlert(result.message, 'ASSESSMENTNEW NULL', 'warning')
            }
          }, Error => {
            this.global.swalAlertError(Error);
          });
        }
        //----------------------------------------------------------//

        //codes here
        this.api.getPOS_DuesBreakdown(this.idNumber, this.schoolYear).map(response => response.json()).subscribe(result => {
          if (result.data != null) {

            this.TestDUES = result.data.previousBalance
            this.arrDues = result.data.dueDates;
            // console.log('Dues', this.arrDues)
            // let totalStatusFees = 0;
            var currentDate = new Date();

            for (var x = 0; x < this.arrDues.length; x++) {

              if (this.arrDues[x].description == "Entrance Fee") {
                this.Entrance = this.arrDues[x].description
                this.EntranceStatus = this.convertToNumber(this.arrDues[x].status);
                this.EntranceDue = this.arrDues[x].dueDate
                console.log(this.Entrance, this.EntranceStatus, this.EntranceDue)
              }

              if (this.arrDues[x].description == "1st Grading" || this.arrDues[x].description == "Prelims") {
                this.Prelims1st = this.arrDues[x].description
                this.Prelims1stStatus = this.convertToNumber(this.arrDues[x].status);
                this.Prelims1stDue = this.arrDues[x].dueDate
                console.log(this.Prelims1st, this.Prelims1stStatus, this.Prelims1stDue)
              }

              if (this.arrDues[x].description == "2nd Grading" || this.arrDues[x].description == "Midterms") {
                this.Midterms2nd = this.arrDues[x].description
                this.Midterms2ndStatus = this.convertToNumber(this.arrDues[x].status);
                this.Midterms2ndDue = this.arrDues[x].dueDate
                console.log(this.Midterms2nd, this.Midterms2ndStatus, this.Midterms2ndDue)
              }

              if (this.arrDues[x].description == "3rd Grading") {
                this.Grading3rd = this.arrDues[x].description
                this.Grading3rdStatus = this.convertToNumber(this.arrDues[x].status);
                this.Grading3rdDue = this.arrDues[x].dueDate
                console.log(this.Grading3rd, this.Grading3rdStatus, this.Grading3rdDue)
              }

              if (this.arrDues[x].description == "Finals") {
                this.Finals = this.arrDues[x].description
                this.FinalsStatus = this.convertToNumber(this.arrDues[x].status);
                this.FinalsDue = this.arrDues[x].dueDate
                console.log(this.Finals, this.FinalsStatus, this.FinalsDue)
              }

            }
            // for (var x = 0; x < this.arrDues.length; x++) {
            //   var dueDit = new Date(this.arrDues[x].dueDate)
            //   if (this.arrDues[x].description == "Entrance Fee") {
            //         this.desc = this.arrDues[x].description
            //         this.entrancestatus = this.convertToNumber(this.arrDues[x].status);
            //         this.dueEntrance = this.arrDues[x].dueDate
            //       }

            //   if (this.arrDues[x].description == "Prelims") {
            //         this.prelims = this.arrDues[x].description
            //         if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
            //           this.statusprelim = 0.00; // status will be = CLEARED
            //           this.statusmidterm = this.convertToNumber(this.arrDues[x].status);
            //         }
            //         else
            //           this.statusprelim = this.convertToNumber(this.arrDues[x].status)
            //         this.duePrelim = this.arrDues[x].dueDate
            //       }

            //   if (this.arrDues[x].description == "Midterms") {

            //         this.midterms = this.arrDues[x].description
            //         if (currentDate.toLocaleDateString() > dueDit.toLocaleDateString()) {
            //           this.statusmidterm = 0.00;
            //       this.statusfinals = this.arrDues[x].status;
            //         }
            //         else {
            //           if (this.convertToNumber(this.arrDues[x].status).toString() != 'CLEARED') {
            //             this.statusmidterm = parseInt(this.statusmidterm) + this.convertToNumber(this.arrDues[x].status)
            //           }
            //         }
            //         this.dueMidterm = this.arrDues[x].dueDate
            //       }

            //       if (this.arrDues[x].description == "Finals") {

            //         this.finals = this.arrDues[x].description
            //         if (this.statusfinals > 0)
            //           this.statusfinals = parseInt(this.statusfinals) + this.convertToNumber(this.arrDues[x].status)
            //         else
            //           this.statusfinals = this.convertToNumber(this.arrDues[x].status)
            //         this.dueFinals = this.arrDues[x].dueDate
            //       }

            //     }

            if (this.statusmidterm.toString() == "CLEARED") {
              this.currentStatus = "Finals"
              this.amountDue = this.statusfinals
            } else if (this.statusfinals.toString() != "CLEARED") {
              this.currentStatus = "Midterms";
              this.amountDue = this.statusmidterm;
            } else if (this.statusmidterm.toString() != "CLEARED") {
              this.currentStatus = "Prelims";
              this.amountDue = this.statusprelim;
            } else if (this.entrancestatus.toString() != "CLEARED") {
              this.currentStatus = "Entrance Fee";
              this.amountDue = this.entrancestatus;
            } else {
              this.currentStatus = "All dues are cleared.";
            }

            if (this.Prelims1stStatus !== 'CLEARED') {
              this.totalDues += Number(this.Prelims1stStatus);
            }
            if (this.Midterms2ndStatus !== 'CLEARED') {
              this.totalDues += Number(this.Midterms2ndStatus);
            }
            if (this.Grading3rdStatus !== 'CLEARED') {
              this.totalDues += Number(this.Grading3rdStatus);
            }
            if (this.FinalsStatus !== 'CLEARED') {
              this.totalDues += Number(this.FinalsStatus);
            }

            // console.log('Total Dues', this.totalDues)
            this.global.swalClose()
          }
          else {
            this.global.swalClose()
            // this.global.swalAlert(result.message, 'DEUSBREAKDOWN NULL', 'warning')
          }
        }, Error => {
          this.global.swalAlertError(Error);
        });


      }
      else {
        this.global.swalAlert('NOTE!', 'NO Student Information is associated with this ID Number', 'warning')

      }
    },
      Error => {
        this.global.swalAlertError(Error);
      });
  }
}