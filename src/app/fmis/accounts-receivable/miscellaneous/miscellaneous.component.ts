import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-miscellaneous',
  templateUrl: './miscellaneous.component.html',
  styleUrls: ['./miscellaneous.component.css']
})
export class MiscellaneousComponent implements OnInit {

  history = [
    { value: 'enrollement', viewValue: 'Enrollment History',},
    { value: 'payment', viewValue: 'Payment History',},
    { value: 'permit', viewValue: 'Permit History',},
    { value: 'clearance', viewValue: 'Clearance History',}
  ];
  filteredArray
  constructor(public dialogRef: MatDialogRef<MiscellaneousComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
  ngOnInit() {
  }

}
