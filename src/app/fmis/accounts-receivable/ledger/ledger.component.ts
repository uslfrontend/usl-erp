import { Component, Inject, OnInit, ChangeDetectorRef} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-ledger',
  templateUrl: './ledger.component.html',
  styleUrls: ['./ledger.component.css']
})
export class LedgerComponent implements OnInit {

  // schoolyear = '2022231'
  // idNumber = '2000177'
  arrLedger: any;
  balance: number = 0; // define the balance property

  constructor(public http: HttpClient, public dialogRef: MatDialogRef<LedgerComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService,  private cd: ChangeDetectorRef) {
    // this.arrLedger = data.arrLedger; 
    this.arrLedger = []; 
    
    data.arrLedger.forEach(element => {
      this.arrLedger.push(
        {
          amount: element.amount,
          particulars:element.particulars,
          recordID:element.recordID,
          refDate:element.refDate,
          refNo:element.refNo,
          transType:element.transType,
          balance: this.getBalance(element.transType, element.amount)
        }
      )
    });
    
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  ngOnInit() {
    // this.api.getPosAssessment(this.idNumber, this.schoolyear).map(response => response.json()).subscribe(result => {
    //   if (result.data != null) {
    //     this.arr = result.data.aRLedgerSelects; // extract the array from the object
    //     console.log('Assessment', this.arr);
    //   }     
    // });

  }
  getBalance(transType, amount) {
    // console.log(transType, amount)
    if (transType == 'D') {
      this.balance +=amount;
    } else if (transType == 'C') {
      this.balance -= amount;
    }
    // return this.balance
    return parseFloat(this.balance.toFixed(2));
  }
  
}

