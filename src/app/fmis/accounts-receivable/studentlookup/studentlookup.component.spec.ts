import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentlookupComponent } from './studentlookup.component';

describe('StudentlookupComponent', () => {
  let component: StudentlookupComponent;
  let fixture: ComponentFixture<StudentlookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentlookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentlookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
