import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentSetupComponent } from './assessment-setup.component';

describe('AssessmentSetupComponent', () => {
  let component: AssessmentSetupComponent;
  let fixture: ComponentFixture<AssessmentSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
