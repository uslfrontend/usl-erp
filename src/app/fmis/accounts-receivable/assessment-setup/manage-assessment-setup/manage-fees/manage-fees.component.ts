import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../../../global.service';
import { FmisAPIService } from './../../../../../services/fmis-api.service';

@Component({
  selector: 'app-manage-fees',
  templateUrl: './manage-fees.component.html',
  styleUrls: ['./manage-fees.component.scss']
})
export class ManageFeesComponent implements OnInit {

  idesc
  itf
  imf
  isf
  ioncePerSY
  inotSummer
  iperUnit

  FeesList = []
  selectedFType
  selectedFees
  selectedFeesID
  feesMode="Save New"
  feesHeader="Add Fees"
  feesSubHeader="You are about to save new fees"

  showDesc = false
  showError = false
  eMessage=""

  constructor(public dialog: MatDialog,private http: Http,private global: GlobalService, private fmisAPI: FmisAPIService,
    public dialogRef: MatDialogRef<ManageFeesComponent>,@Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit() {
    console.log(this.data)
    this.loadFees()
    if(this.data.x==1){   
      this.feesMode="Save New"
      this.feesHeader="Add Fees"
      this.feesSubHeader="You are about to save new fees"
    }
    else{
      this.feesMode="Save Changes"
      this.feesHeader="Edit Fees"
      this.feesSubHeader="You are about to update fees"
      this.selectedFees=this.data.data[0].feesID
      this.itf=this.data.data[0].tf
      this.imf=this.data.data[0].mf
      this.isf=this.data.data[0].sf
      this.iperUnit=this.data.data[0].perUnit
      this.ioncePerSY=this.data.data[0].oncePerSY
      this.inotSummer=this.data.data[0].notSummer
    }
    this.selectedFeesID=this.data.data[0].fid
    this.selectedFType=this.data.ftype
    console.log(this.selectedFeesID)
  }
  FTypeFeesList = []
  descList =[]
  LNO
  loadFees(){
    var lno
    this.fmisAPI.getARSetupConstFee()
    .map(response => response.json())
    .subscribe(res => {
      this.FeesList=res.data
      if(res.data.length>0){
        for (var i = 0; i < res.data.length; ++i) {
          if (!(this.descList.includes(res.data[i].feesDesc))) {
            this.descList.push(res.data[i].feesDesc);
          }
        }
        lno=res.data[res.data.length-1].feesID
      }
      if(!isNaN(Number(lno))){
        if((lno.length>1)){
          this.LNO=(Number(lno)+1).toString()
        }
        else{
          this.LNO="0"+(Number(lno)+1).toString()
        }
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  checked(f){
    this.itf=false
    this.imf=false
    this.isf=false
    if(f==1){
      this.itf=true
    }
    else if(f==2){
      this.imf=true
    }
    else if(f==3){
      this.isf=true
    }
  }

  showDescription(value){
    this.showDesc=false
    if(value>0){
      this.showDesc=true
      return
    }
  }

  saveDesciption(){
    console.log(this.idesc)
    if(this.idesc!=""||this.idesc!=undefined){
      if (!(this.descList.includes(this.idesc))) {
        this.fmisAPI.postARSetUpConstFee(this.LNO,this.idesc)
        .map(response => response.json())
        .subscribe(res => {
          this.loadFees()
          this.showDesc=false
          this.showError=false
        }, Error => {
          console.log(Error)
          this.showError=true
          this.eMessage=Error.message
        });
      }
      else{
        this.showError=true
        this.eMessage="Description is already existed."
      }
    }
    else{
      this.showError=true
      this.eMessage="Fill up the Description."
    }
    console.log(this.eMessage)
  }

  cancelDescription(){
    this.showDesc=false
  }

  SaveFees(){
    let parUpdate = {};
    parUpdate['typeID'] = this.selectedFType;
    parUpdate['feesID'] = this.selectedFees;
    parUpdate['tf'] = this.convertBoolean(this.itf);
    parUpdate['mf'] = this.convertBoolean(this.imf);
    parUpdate['sf'] = this.convertBoolean(this.isf);
    parUpdate['oncePerSY'] = this.convertBoolean(this.ioncePerSY);
    parUpdate['notSummer'] = this.convertBoolean(this.inotSummer);
    parUpdate['perUnit'] = this.convertBoolean(this.iperUnit);

    if(this.feesMode=="Save Changes"){
      this.fmisAPI.putARSetUpFees(this.selectedFeesID,parUpdate)
      .map(response => response.json())
      .subscribe(res => {
        this.dialogRef.close({result:'success'});
        this.global.swalSuccess(res.message)
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
    }
    else{
      console.log(parUpdate)
      this.fmisAPI.postARSetUpFees(parUpdate)
      .map(response => response.json())
      .subscribe(res => {
        this.dialogRef.close({result:'success'});
        this.global.swalSuccess(res.message)
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
    }
  }

  convertBoolean(value){
    if(value==true){
      return 1
    }
    else if(value==false){
      return 0
    }
    else{
      return value
    }
  }

  onNoClickclose(): void {
       this.dialogRef.close({result:'cancel'});
  }

}
