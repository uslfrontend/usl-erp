import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../../../global.service';
import { FmisAPIService } from './../../../../../services/fmis-api.service';

@Component({
  selector: 'app-manage-schoolyear',
  templateUrl: './manage-schoolyear.component.html',
  styleUrls: ['./manage-schoolyear.component.css']
})
export class ManageSchoolyearComponent implements OnInit {

	year1;
	year2;
	activeterm=''

  constructor(public dialog: MatDialog,private http: Http,private global: GlobalService, private fmisAPI: FmisAPIService,public dialogRef: MatDialogRef<ManageSchoolyearComponent>,@Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit() {
    var dt = new Date();
    this.year1 = dt.getFullYear()
    this.yearchange1()
    console.log(this.data)
  }


  yearchange1(){
         this.year2=this.year1+1;
       }
       yearchange2(){
         this.year1=this.year2-1;
       }
    onNoClickclose(): void {
         this.dialogRef.close({result:'cancel'});
    }
  
  description(sem) {
    var SEM
    if (sem == 1)
      SEM = "1st Sem"
    else if (sem == 2)
      SEM = "2nd Sem"
    else
      SEM = "Summer"
    return SEM + " S.Y. " + this.year1.toString()+"-"+this.year2.toString()
  }
  InsertSY(){
    if (this.activeterm=='') {
      alert("*Active term is required!")
    }
    else{
      var schoolyear = this.year1.toString() + this.year2.toString().substring(2,4) + this.activeterm;
        var found = false;
      for(var i = 0; i < this.data.data.length; i++) {
          if (this.data.data[i].schoolYear == schoolyear) {
              found = true;
              break;
          }
      }
      if (found) {
        this.global.swalAlert('Adding failed!','School year already Exist!', 'warning')
      }
      else{
        this.global.swalLoading('');
        console.log(schoolyear+' '+this.description(this.activeterm))
        this.fmisAPI.postARSetUpSchoolYear(schoolyear,this.description(this.activeterm))
        .map(response => response.json())
        .subscribe(res => {
          this.dialogRef.close({result:'success'});
          this.global.swalSuccess(res.message)
        },Error=>{
          this.global.swalAlertError(Error);
        });
      }
    }
  }
}