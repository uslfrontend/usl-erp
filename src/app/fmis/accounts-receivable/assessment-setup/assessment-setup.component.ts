import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../global.service';
import { ApiService } from './../../../api.service';
import { FmisAPIService } from './../../../services/fmis-api.service';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ManageSchoolyearComponent } from './manage-assessment-setup/manage-schoolyear/manage-schoolyear.component';
import { formatDate } from '@angular/common'

import Swal from 'sweetalert2';
import { importType } from '@angular/compiler/src/output/output_ast';
import { ManageFeesComponent } from './manage-assessment-setup/manage-fees/manage-fees.component';
const swal = Swal;

@Component({
  selector: 'app-assessment-setup',
  templateUrl: './assessment-setup.component.html',
  styleUrls: ['./assessment-setup.component.css']
})
export class AssessmentSetupComponent implements OnInit {
  psForm: FormGroup;
  SchoolYearList = []
  LockAssessmentList = []
  PaymentScheduleList = []
  TuitionFeeRateList = []
  FeesList = []
  FTypeFeesList = []
  FeesRateList = []

  selectedTab = 0
  suMode="Save Changes"
  setUpHeader="Edit Mode"
  setUpSubHeader="You are about to update the setup"

  DepartmentList = [{
      DID: 'GS',
      DName: 'Graduate School'
    },{
      DID: 'College',
      DName: 'College Department'
    },{
      DID: 'HS',
      DName: 'High School Department'
    },{
      DID: 'Elem',
      DName: 'Elementary Department'
    },{
      DID: 'PS',
      DName: 'Pre-School Department'
    }];
  selectedDept='GS'

  ProgramList = [{
    progID: '30',
    progName: 'Technical Vocational'
  },{
    progID: '40',
    progName: 'Prebaccalaureate'
  },{
    progID: '50',
    progName: 'Baccalaureate'
  },{
    progID: '60',
    progName: 'Post Baccalaureate'
  },{
    progID: '70',
    progName: 'MD or LLB'
  },{
    progID: '80',
    progName: 'Masteral'
  },{
    progID: '90',
    progName: 'Doctoral'
  }];
  selectedProgram='30'

  selectedFType

  sylist
  selectedsy
  selectedsem = ''

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private api: ApiService, private fmisAPI: FmisAPIService, public global: GlobalService, private router: Router, private cookieService: CookieService) { }

  ngOnInit() {
    this.loadSYDD()
    this.selectedsem = this.global.syear;
    // if(this.global.checkaccess(':Admission:SYSettingsGet') && this.global.checkaccess(':Enrollment:EnrollmentSettingValueMapperGet')){
    //   this.activate(this.selectedsem);
    // }
  }

  SyArr=[]
  SYList=[]
  loadSYDD(){
    this.SyArr=[]
    this.SYList=[]
    this.fmisAPI.getARSetUpSchoolYear()
    .map(response => response.json())
    .subscribe(res => {
      if(res.data!=null){
        for (var i = 0; i < res.data.length; ++i) {
          if (!(this.SyArr.includes(res.data[i].schoolYear))) {
            this.SyArr.push(res.data[i].schoolYear)
            this.SYList.push({schoolYear:res.data[i].schoolYear,description:res.data[i].description})
          }
        }
        this.SchoolYearList=this.SYList;
        // this.selectedsem=res.data[0].schoolYear
        this.loadAssessmentLock(this.selectedsem)
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  getindex(value) {
    this.selectedTab=value.index;
    this.loadActiveTab()
  }

  loadActiveTab(){
    if(this.selectedTab==0){
      this.loadAssessmentLock(this.selectedsem)
    }
    else if(this.selectedTab==1){
      this.selectedDept='GS'
      this.loadPaymentSchedules(this.selectedDept)
    }
    else if(this.selectedTab==2){
      this.selectedProgram='30'
      this.loadTuitionFeeRate(this.selectedProgram)
    }
    else if(this.selectedTab==3){
      this.LoadFeeType()
      this.selectedFType='01'
      this.loadFees(this.selectedFType)
    }
  }

  loadAssessmentLock(sy){
    this.fmisAPI.getARSetUpSchoolYear_SY(sy)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data!=null){
        this.LockAssessmentList=res.data
      }
      else{
        this.SchoolYearList=null;
      }
      this.setUpHeader="Assessment Lock [ Edit Mode ]"
      this.setUpSubHeader="You are about to update the assessment lock setup."
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  saveARLock() {
    this.global.swalLoading('');
    
    for(var i = 0; i < this.LockAssessmentList.length; i++) {
      let parUpdate = {};
    
      parUpdate['description'] = this.LockAssessmentList[i].description;
      parUpdate['lockFeesSetup'] = this.convertBoolean(this.LockAssessmentList[i].lockFeesSetup);
      parUpdate['lockAssessment'] = this.convertBoolean(this.LockAssessmentList[i].lockAssessment);
      parUpdate['deptGroup'] = this.LockAssessmentList[i].departmentGroup;
      this.fmisAPI.putARSetUpSchoolYear(this.selectedsem,parUpdate)
      .map(response => response.json())
      .subscribe(res => {
        this.global.swalAlert(res.message, "", 'success');
        this.loadAssessmentLock(this.selectedsem)
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
    }
  }

  loadPaymentSchedules(department){
    var schoolYear = this.selectedsem
    if(department=="HS" || department=="Elem" || department=="PS"){
      schoolYear=schoolYear.substring(0, 6)
    }
    this.fmisAPI.getARSetupPaymentSchedules(department,schoolYear)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        this.PaymentScheduleList=res.data
        this.suMode="Save Changes"
        this.setUpHeader="Payment Schedules [ Edit Mode ]"
        this.setUpSubHeader="You are about to update the payment schedule setup."
      }
      else{
        this.fmisAPI.getARSetupPaymentSchedulesForNew(department,schoolYear)
        .map(response => response.json())
        .subscribe(result => {
          if(result.data.length>0){
            this.PaymentScheduleList=result.data
          }
          else{
            this.LoadBlankPaymentSchedule(department,schoolYear)
          }
          this.suMode="Save New"
          this.setUpHeader="Payment Schedules [ New Mode ]"
          this.setUpSubHeader="You are about to add new payment schedule setup."
        },Error=>{
          this.global.swalAlertError(Error);
        });
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  PSLIst=[]
  LoadBlankPaymentSchedule(dept,sy){
    this.PSLIst=[]
    if(dept=="HS" || dept=="Elem" || dept=="PS"){
      this.PSLIst.push(
        {psid:"ps1",schoolYear:sy,department:dept,period:"1st Grading",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps2",schoolYear:sy,department:dept,period:"2nd Grading",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps3",schoolYear:sy,department:dept,period:"3rd Grading",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps4",schoolYear:sy,department:dept,period:"Finals",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')})
    }
    else if(dept=="College" && sy.substring(7,1)!='3'){
      this.PSLIst.push(
        {psid:"ps1",schoolYear:sy,department:dept,period:"Prelims",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps2",schoolYear:sy,department:dept,period:"Midterms",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps3",schoolYear:sy,department:dept,period:"Finals",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')})
    }
    else{
      this.PSLIst.push(
        {psid:"ps1",schoolYear:sy,department:dept,period:"Midterms",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')},
        {psid:"ps2",schoolYear:sy,department:dept,period:"Finals",dueDate:formatDate(new Date(), 'MM/dd/yyyy', 'en')})
    }
    this.PaymentScheduleList=this.PSLIst
  }

  savePS() {
    this.global.swalLoading('');
    var psid
    for(var i = 0; i < this.PaymentScheduleList.length; i++) {
      psid=this.PaymentScheduleList[i].psid;
      let parUpdate = {};
      parUpdate['department'] = this.convertBoolean(this.PaymentScheduleList[i].department);
      parUpdate['period'] = this.convertBoolean(this.PaymentScheduleList[i].period);
      parUpdate['dueDate'] = formatDate(this.PaymentScheduleList[i].dueDate, 'MM/dd/yyyy', 'en');

      if(this.suMode=="Save Changes"){
        parUpdate['schoolYear'] = this.PaymentScheduleList[i].schoolYear;
        this.fmisAPI.putARSetUpPaymentSched(psid,parUpdate)
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalAlert(res.message, "", 'success');
          this.loadPaymentSchedules(this.selectedDept)
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      }
      else{
        parUpdate['schoolYear'] = this.selectedsem;
        this.fmisAPI.postARSetUpPaymentSched(parUpdate)
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalAlert(res.message, "", 'success');
          this.loadPaymentSchedules(this.selectedDept)
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      }
    }
  }
  
  deleteSetup(id,ttype){
    var trans
    if(ttype==1){
      trans="payment schedule"
    }
    else if(trans==2){
      trans="tuition fee rate"
    }
    swal.fire({
      title: "Are you sure you want to delete this "+trans+"?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Remove'
    }).then((result) => {
      if (result.value) {
        if(ttype==1){
          this.fmisAPI.deleteARSetUpPaymentSched(id)
          .map(response => response.json())
          .subscribe(res => {
            this.global.swalSuccess('Schedule Sucessfully Deleted')
            this.loadPaymentSchedules(this.selectedDept)
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
        }
        else if(ttype==2){
          this.fmisAPI.deleteARSetUpTuitionFeeRate(id)
          .map(response => response.json())
          .subscribe(res => {
            this.global.swalSuccess('Schedule Sucessfully Deleted')
            this.loadTuitionFeeRate(this.selectedProgram)
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
        }
      }
    })
  }

  loadTuitionFeeRate(progLevel){
    var schoolYear = this.selectedsem.substring(0, 6)
    this.fmisAPI.getARSetupTuitionFeeRate(schoolYear,progLevel)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        this.TuitionFeeRateList=res.data
        this.suMode="Save Changes"
        this.setUpHeader="Tuition Fee Rates [ Edit Mode ]"
        this.setUpSubHeader="You are about to update the tuition fee rate setup."
      }
      else{
        this.fmisAPI.getARSetupTuitionFeeRateForNew(schoolYear,progLevel)
        .map(response => response.json())
        .subscribe(result => {
          if(result.data.length>0){
            this.TuitionFeeRateList=result.data
          }
          else{
            this.LoadBlankTuitionFeeRate(schoolYear,progLevel)
          }
          this.suMode="Save New"
          this.setUpHeader="Tuition Fee Rates [ New Mode ]"
          this.setUpSubHeader="You are about to add new tuition fee rate setup."
        },Error=>{
          this.global.swalAlertError(Error);
        });
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  TFRLIst=[]
  TfrArr=[]
  LoadBlankTuitionFeeRate(sy, progLevel){
    this.TfrArr=[]
    this.TFRLIst=[]
    this.fmisAPI.getAllCourses()
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        for (var i = 0; i < res.data.length; ++i) {
          if ((res.data[i].programLevel==progLevel)&&(res.data[i].statusCode=='CO')&&!(this.TfrArr.includes(res.data[i].courseCode))) {
            this.TfrArr.push(res.data[i].courseCode);
            this.TFRLIst.push({tfRateID:i,courseCode:res.data[i].courseCode,tf:0});
          }
        }
        this.TuitionFeeRateList=this.TFRLIst
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  saveTFR(){
    // this.global.swalLoading('');
    var tfrid
    for(var i = 0; i < this.TuitionFeeRateList.length; i++) {
      tfrid=this.TuitionFeeRateList[i].tfRateID;
      let parUpdate = {};
      parUpdate['courseCode'] = this.TuitionFeeRateList[i].courseCode;
      parUpdate['tf'] = this.TuitionFeeRateList[i].tf;
      parUpdate['typeID'] = null;

      if(this.suMode=="Save Changes"){
        parUpdate['schoolYear'] = this.TuitionFeeRateList[i].schoolYear.substring(0, 6);
        parUpdate['programLevel'] = this.TuitionFeeRateList[i].programLevel;
        this.fmisAPI.putARSetUpTuitionFeeRate(tfrid,parUpdate)
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalAlert(res.message, "", 'success');
          this.loadTuitionFeeRate(this.selectedProgram);
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      }
      else{
        parUpdate['schoolYear'] = this.selectedsem.substring(0, 6);
        parUpdate['programLevel'] = this.selectedProgram;
        console.log(parUpdate)
        this.fmisAPI.postARSetUpTuitionFeeRate(parUpdate)
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalAlert(res.message, "", 'success');
          this.loadTuitionFeeRate(this.selectedProgram);
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      }
    }
  }

  loadFees(ftype){
    this.fmisAPI.getARSetupFees(ftype)
    .map(response => response.json())
    .subscribe(res => {
      this.FeesList=res.data
      if(res.data.length>0){
        for (var i = 0; i < res.data.length; ++i) {
          if (!(this.FTypeFeesList.includes(res.data[i].fid))) {
            this.FTypeFeesList.push(res.data[i].fid);
          }
        }
      }
      this.setUpHeader="Fees"
      this.setUpSubHeader="You are managing the fees setup."
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  feesDData = []
  openFeesDialog(x,item): void {
    this.feesDData=[]
    if(x==1){
      this.feesDData=this.FTypeFeesList
    }
    else if(x==2){
      this.feesDData=item
    }
    console.log(this.feesDData)
    const dialogRef = this.dialog.open(ManageFeesComponent, {
      width: '540px', disableClose: false, data: { x: x, data: this.feesDData, ftype:this.selectedFType }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result == 'success') {
          this.loadFees(this.selectedFType)
        }
      }
    });
  }

  loadFeesRate(ftype){
    var schoolYear = this.selectedsem.substring(0, 6)
    this.fmisAPI.getARSetupFeesRate(schoolYear,ftype)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        this.FeesRateList=res.data
        this.suMode="Save Changes"
        this.setUpHeader="Fees Breakdown [ Edit Mode ]"
        this.setUpSubHeader="You are about to update the fees breakdown setup."
      }
      else{
        this.fmisAPI.getARSetupFeesRateForNew(schoolYear,ftype)
        .map(response => response.json())
        .subscribe(result => {
          if(result.data.length>0){
            this.FeesRateList=result.data
          }
          else{
            this.LoadBlankFeesRate(schoolYear,ftype)
          }
          this.suMode="Save New"
          this.setUpHeader="Fees Breakdown [ New Mode ]"
          this.setUpSubHeader="You are about to add new fees breakdown setup."
        },Error=>{
          this.global.swalAlertError(Error);
        });
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  FRLIst=[]
  frArr=[]
  LoadBlankFeesRate(sy, ftype){
    this.frArr=[]
    this.FRLIst=[]
    this.fmisAPI.getARSetupFees(ftype)
    .map(response => response.json())
    .subscribe(res => {
      if(res.data.length>0){
        for (var i = 0; i < res.data.length; ++i) {
          if ((res.data[i].programLevel==ftype)&&(res.data[i].statusCode=='CO')&&!(this.frArr.includes(res.data[i].courseCode))) {
            this.frArr.push(res.data[i].courseCode);
            this.FRLIst.push({tfRateID:i,courseCode:res.data[i].courseCode,tf:0});
          }
        }
        this.TuitionFeeRateList=this.FRLIst
      }
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }

  FTLIst=[]
  LoadFeeType(){
    this.FTLIst=[]
    this.fmisAPI.getARSetupFeeType()
    .map(response => response.json())
    .subscribe(res => {
      this.FTLIst=res.data
    },Error=>{
      this.global.swalAlertError(Error);
    });
  }
  
  openSchoolYearDialog(x): void {
    const dialogRef = this.dialog.open(ManageSchoolyearComponent, {
      width: '540px', disableClose: false, data: { x: x, data: this.SchoolYearList }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result == 'success') {
          this.loadSYDD()
        }
      }
    });
  }

  sem = 2;
  display(x) {
    var y = x.substring(0, 4)
    var z = parseInt(y) + 1
    var a = y.toString() + " - " + z.toString();
    var b = x.substring(6, 7)
    var c
    if (b == 1)
      c = "First Semester"
    else if (b == 2)
      c = "Second Semester"
    else
      c = "Summer"
    return "School Year " + a + " " + c
  }

  convertBoolean(value){
    if(value==true){
      return 1
    }
    else if(value==false){
      return 0
    }
    else{
      return value
    }
  }

}
