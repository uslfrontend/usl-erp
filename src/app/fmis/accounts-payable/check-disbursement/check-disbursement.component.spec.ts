import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckDisbursementComponent } from './check-disbursement.component';

describe('CheckDisbursementComponent', () => {
  let component: CheckDisbursementComponent;
  let fixture: ComponentFixture<CheckDisbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckDisbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckDisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
