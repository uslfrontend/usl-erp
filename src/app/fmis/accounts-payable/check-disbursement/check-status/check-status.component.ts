import { Component, OnInit, Inject } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-check-status',
  templateUrl: './check-status.component.html',
  styleUrls: ['./check-status.component.css']
})
export class CheckStatusComponent implements OnInit {

  submitted = false;
  CPStatus = [];
  StatusHistory = [];
  selectedStatus;
  selectedCP = [];

  constructor(public global: GlobalService, private api: ApiService, public dialogRef: MatDialogRef<CheckStatusComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
    this.global.swalLoading('Loading...')
    this.getStatusHistory(this.data.cpid)
    this.global.swalClose()
    console.log(this.global.option)
  }


  getStatusHistory(cpid) {
    this.api.getCheckPaymentStatusHistory(cpid).map(response => response.json()).subscribe(res => {
      this.StatusHistory = res.data
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

}
