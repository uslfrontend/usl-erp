import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CheckStatusComponent } from './check-status/check-status.component';
import Swal from 'sweetalert2';
const swal = Swal;

import { ExcelService } from './../../../academic/curriculum/excel.service';

@Component({
  selector: 'app-check-disbursement',
  templateUrl: './check-disbursement.component.html',
  styleUrls: ['./check-disbursement.component.css']
})
export class CheckDisbursementComponent implements OnInit {

  config: any;
  sylist = []

  CPyears = [];
  CheckPayments = [];
  tempCheckPayments = [];
  CPStatus = [];
  CheckbookTypes = [];
  selectedYear;
  selectedCP = [];

  fDTSNo = ''
  fCheckDate = ''
  fCheckNo = ''
  fPayee = ''
  fParticulars = ''
  fCVNo = ''
  fStatus=''
  fCheckType=''

  constructor(private excelService: ExcelService, public global: GlobalService, private api: ApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.config = {
      itemsPerPage: 20,
      currentPage: 1,
      totalItems: 0
    };
    this.getYear();
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  filterTable() {
    this.CheckPayments = []
    for (var x in this.tempCheckPayments) {
      if (
        this.tempCheckPayments[x].dtsNo.includes(this.fDTSNo) &&
        this.tempCheckPayments[x].checkNo.toString().includes(this.fCheckNo.toLocaleLowerCase()) &&
        this.tempCheckPayments[x].payee.toLowerCase().includes(this.fPayee.toLocaleLowerCase())
      ) {
        this.CheckPayments.push(this.tempCheckPayments[x])
      }
    }
  }

  openStatusHistoryDialog(item): void {
    console.log(item);
    const dialogRef = this.dialog.open(CheckStatusComponent, {
      height: '910px',
      width: '1010px',
      data: { cpid: item.cpid }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.data.updated == 1) {
        this.getCheckPayments()
      }
    });
  }

  getYear() {
    this.api.getCheckPaymentYear().map(response => response.json()).subscribe(res => {
      console.log(res.data)
      this.CPyears = res.data
      this.selectedYear = res.data[0].year
      this.getCheckPayments()
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

  getCheckPayments() {
    this.global.swalLoading('Loading check payments...');
    if (this.selectedYear != null) {
      this.api.getCheckPayments(this.selectedYear).map(response => response.json()).subscribe(res => {
        if (res.data != null) {
          this.CheckPayments = res.data;
          this.tempCheckPayments = res.data;
          console.log(res.data)
          this.global.swalClose();
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
    }
  }

  changeYear(year) {
    this.selectedYear = year;
    this.getCheckPayments()
  }

  changePayment() {
    console.log(this.selectedCP)
  }

  selectedCheckPayment(cpid) {
    console.log(cpid);
  }

  getStatus() {
    this.api.getCheckPaymentStatuses().map(response => response.json()).subscribe(res => {
      this.CPStatus = res.data
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

  getCheckBookTypes() {
    this.api.getCheckBookTypes().map(response => response.json()).subscribe(res => {
      console.log(res.data)
      this.CheckbookTypes = res.data
    }, Error => {
      this.global.swalAlertError(Error);
    })
  }

}
