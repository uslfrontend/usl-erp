import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsVerificationManagerComponent } from './accounts-verification-manager.component';

describe('AccountsVerificationManagerComponent', () => {
  let component: AccountsVerificationManagerComponent;
  let fixture: ComponentFixture<AccountsVerificationManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsVerificationManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsVerificationManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
