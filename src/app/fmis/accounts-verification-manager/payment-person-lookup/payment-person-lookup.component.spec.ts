import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentPersonLookupComponent } from './payment-person-lookup.component';

describe('PaymentPersonLookupComponent', () => {
  let component: PaymentPersonLookupComponent;
  let fixture: ComponentFixture<PaymentPersonLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPersonLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPersonLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
