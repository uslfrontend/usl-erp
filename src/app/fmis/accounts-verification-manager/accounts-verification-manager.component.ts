import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';
import { PaymentPersonLookupComponent } from './payment-person-lookup/payment-person-lookup.component';



@Component({
  selector: 'app-accounts-verification-manager',
  templateUrl: './accounts-verification-manager.component.html',
  styleUrls: ['./accounts-verification-manager.component.css']
})
export class AccountsVerificationManagerComponent implements OnInit {
  personInfo = ''
  records = []
  recordsDisplay = []
  id = ''
  filterId = ''
  filterName = ''
  filterDateVerified = ''
  config: any;




  constructor(public dialog: MatDialog, public global: GlobalService, private api: ApiService) { }

  ngOnInit() {

    this.config = {
      itemsPerPage: 8,
      currentPage: 1,
      totalItems: 0
    };

    var sy = this.global.syear
    if (this.global.domain != 'COLLEGE' && this.global.domain != 'GRADUATE SCHOOL') {
      if (sy == 6) {
        return
      }
      sy = sy.slice(0, -1);
    }
    this.api.getPaymentVerification(sy)
      .map(response => response.json())
      .subscribe(res => {
        this.records = res.data
        this.recordsDisplay = res.data
        // console.log(res.data)
      })
  }

  studentlookup(): void {
    const dialogRef = this.dialog.open(PaymentPersonLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.displayStudentInfo()
      }
    });
  }

  keyDownFunction(event) {
    this.personInfo = ''
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.displayStudentInfo()
    }
  }

  displayStudentInfo() {
    this.api.checkPerson(this.id)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data == null){
          this.global.swalAlert("ID number does not exist", res.message, 'warning')
          return
        }
        this.personInfo = res.data
      }, Error => {
      })
  }

  verify() {
    var sy = this.global.syear
    if (this.global.domain != 'COLLEGE' && this.global.domain != 'GRADUATE SCHOOL') {
      if (sy == 6) {
        return
      }
      sy = sy.slice(0, -1);
    }

    this.api.postPaymentVerification(this.id, sy)
      .map(response => response.json())
      .subscribe(res => {
        this.ngOnInit()
      })
  }

  filter() {
    this.recordsDisplay = []
    for (var x in this.records) {
      if (
        this.records[x].idNumber.includes(this.filterId) &&
        this.records[x].name.toLowerCase().includes(this.filterName.toLocaleLowerCase()) &&
        this.records[x].dateVerified.toLowerCase().includes(this.filterDateVerified.toLocaleLowerCase())
      ) {
        this.recordsDisplay.push(this.records[x])
      }
    }
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  checkIfRecordExist() {
    for (var x in this.records) {
      if (this.records[x].idNumber.includes(this.id)) {
        return true
      }
    }
    return false
  }

}
