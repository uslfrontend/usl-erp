import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeLookupComponent } from './code-lookup.component';

describe('CodeLookupComponent', () => {
  let component: CodeLookupComponent;
  let fixture: ComponentFixture<CodeLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
