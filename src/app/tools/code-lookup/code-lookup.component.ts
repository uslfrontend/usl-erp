
import { Component, OnInit,} from '@angular/core';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatTableDataSource, MatSort } from '@angular/material';
import { ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';



import Swal from 'sweetalert2';
const swal = Swal;

@Component({
  selector: 'app-code-lookup',
  templateUrl: './code-lookup.component.html',
  styleUrls: ['./code-lookup.component.css']
})
export class CodeLookupComponent implements OnInit {
  deps = new FormControl()
	departments;
	findby;
	exact=false
	string

  moduleID='';
  subjectStatus = '';

  dataSource;
  tableArr:Array<any>=[];

      displayedColumns = ['codeno', 'subjectid', 'descriptivetitle', 'day', 'time', 'room', 'units','cs','oe','res','department','action'];
      @ViewChild(MatSort, { static: false }) sort: MatSort;
  constructor(private domSanitizer: DomSanitizer,public global: GlobalService,private api: ApiService) { }

  ngOnInit() {
    // this.moduleID=this.data.moduleID;
      this.exact=false
      this.api.getPublicAPIDepartments()
            .map(response => response.json())
            .subscribe(res => {
              this.departments=res;
            },Error=>{
              this.global.swalAlertError(Error)
            });
  }

  keyDownFunction(event){
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.searcht();
    }
  }

  checkbox(){
  	if (this.exact==false)
  	 this.exact=true
  	else
  	this.exact=false
  }



  daydisplay(x){
    x = x.replace("U", "TH");
    x = x.replace("R", "SAT");
    x = x.replace("D", "SUN");
    return x;
  }

  searcht(){
  	this.tableArr=undefined;
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
  	let x='true'
  	if (this.exact==false) x='false'
      this.api.getCodeLookUpSubjectsfind(this.findby+'/'+this.string+'/'+this.global.syear+'/'+x)
      .map(response => response.json())
      .subscribe(res => {
          this.tableArr=res.data;
          this.dataSource = new MatTableDataSource(res.data);
          this.dataSource.sort = this.sort;
      },Error=>{
        this.tableArr=[];
        this.global.swalClose()
      });
  }

  seeclass(x){

    if(x.status == 2){
      this.subjectStatus = 'Code closed';
      return 'codegray';
    }else if(x.status == 3){
      this.subjectStatus = 'Code dissolved';
      return 'codegray';
    }
    else{

      var go = 0;

        x.day = x.day.replace("TH", "U");
        x.day = x.day.replace("SAT", "R");
        x.day = x.day.replace("SUN", "D");

      if ((x.oe+x.res)>=x.classSize) {
        return 'codered';
      }

      var comp1
      var comp2

      if (x.time.substring(5,7)=='AM') {
        comp1 =  parseInt(x.time.substring(0,2) + x.time.substring(3,5));
      }else{
        comp1 = parseInt(x.time.substring(0,2) + x.time.substring(3,5)) + 1200
      }

      if (x.time.substring(13,15)=='AM') {
        comp2 =  parseInt(x.time.substring(8,10) + x.time.substring(11,13));
      }else{
        if (parseInt(x.time.substring(8,10) + x.time.substring(11,13))<1259&&parseInt(x.time.substring(8,10) + x.time.substring(11,13))>=1200) {
          comp2 =  parseInt(x.time.substring(8,10) + x.time.substring(11,13))
        }else
          comp2 =  parseInt(x.time.substring(8,10) + x.time.substring(11,13)) + 1200
      }


      if (go==0) {
        return 'normal';
      }
      if (go>0) {
        return 'codeorange';
      }
    }



  }

}
