import { Component, OnInit } from "@angular/core";
import { GlobalService } from "./../../global.service";
import { ApiService } from "./../../api.service";
import { DomSanitizer } from "@angular/platform-browser";
import { MatTableDataSource, MatSort } from "@angular/material";
import { ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-person-finder",
  templateUrl: "./person-finder.component.html",
  styleUrls: ["./person-finder.component.css"],
})
export class PersonFinderComponent implements OnInit {
  x = 1;
  arraystud = [];
  keyword = "";
  tempkeyword = "";

  exactmatch = false;
  fullname = "";
  dob = "";
  gender = "";
  idnumber = "";
  constructor(
    private domSanitizer: DomSanitizer,
    public global: GlobalService,
    private api: ApiService
  ) {}

  ngOnInit() {}
  select(param) {
    // this.dialogRef.close({result:id});
    this.fullname = param.fullName;
    this.dob = param.dateOfBirth;
    this.gender = param.gender;
    this.idnumber = param.idNumber;
  }

  getGender(g) {
    if (g == "M") return "Male";
    else if (g == "F") return "Female";
    else return "";
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == "onoutfocus") {
      this.x = 0;
      if (this.keyword != "") {
        this.tempkeyword = this.keyword;
        this.arraystud = undefined;
        this.api
          .getPersonFinder(this.keyword, this.exactmatch)
          .map((response) => response.json())
          .subscribe(
            (res) => {
              this.arraystud = res.data[0];
              this.arraystud = this.arraystud.concat(res.data[1]);
              this.arraystud = this.arraystud.concat(res.data[2]);
            },
            (Error) => {
              this.global.swalAlertError(Error);
            }
          );
      }
    }
  }

  close(): void {
    //  this.dialogRef.close({result:'cancel'});
  }
  keyDownFunctionCODE(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == "onoutfocus") {
      if (this.keyword == "") {
      } else {
        this.keyDownFunction("onoutfocus");
      }
    }
  }
}
