import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../../global.service';
import { ApiService } from '../../../../api.service';
import { formatDate } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-manage-online-registration-setting',
  templateUrl: './manage-online-registration-setting.component.html',
  styleUrls: ['./manage-online-registration-setting.component.css']
})
export class ManageOnlineRegistrationSettingComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private api: ApiService, public global: GlobalService) { }

  /* ---Initialize Variables-- */
  //#region
  termOptions = [];
  onlineRegistrationSettingsArray;
  onlineRegSettingName;
  onlineRegSettingLevel;
  onlineRegSettingToUpdate = '';
  form: FormGroup;
  year1;
  year2;
  term;
  status;
  startDate;
  ctrYear1 = 0;
  ctrYear2 = 0;
  ctrTerm = 0;
  ctrStatus = 0;
  ctrStartDate=0;
  btnSaveDisabled = true;
  submitted = false;
  disableTerm = false;
  //#endregion

  ngOnInit() {
    this.getOnlineRegSettingAll();
    this.form = this.formBuilder.group({
      status: ['', Validators.required],
      term: [{ value: '', disabled: false }, Validators.required],
      year1: ['', Validators.required],
      year2: ['', Validators.required],
      startDate: [{disabled:true, value:''}],
    })

    //console.log(this.global.domain);
  }

  save() {
    this.submitted = true;
    if(this.f.year1.value != null && this.f.year2.value != null) {
      if (this.f.year1.value.toString().length < 4 || this.f.year1.value.toString().length > 4) {
        this.f.year1.setErrors({ 'minmaxlength': true });
      }

      if (this.f.year2.value.toString().length < 4 || this.f.year2.value.toString().length > 4) {
        this.f.year2.setErrors({ 'minmaxlength': true });
      }
    }

    if (!this.form.errors && !this.form.invalid && !this.f.year1.errors && !this.f.year2.errors) {
      var SY_ToSave;
      SY_ToSave = this.f.year1.value.toString() + this.f.year2.value.toString().substr(2, 2) + this.f.term.value.toString();
      if(this.global.domain == 'ELEMENTARY' || this.global.domain == 'HIGHSCHOOL') {
        SY_ToSave=SY_ToSave.slice(0,-1);
      }
     /*  console.log(SY_ToSave);
      console.log(this.onlineRegSettingToUpdate);
      console.log(this.f.status.value);
      console.log(this.onlineRegSettingToUpdate);
      console.log(formatDate(this.f.startDate.value,'M/d/yyyy','en')); */
      //save
      this.api.putOnlineRegSetting(
        {
          "code": this.onlineRegSettingToUpdate,
          "schoolyear": SY_ToSave,
          "status": this.f.status.value,
          "startDate": this.f.startDate.value ? formatDate(this.f.startDate.value,'M/d/yyyy','en') : null
        }
      )
        .map(response => response.json())
        .subscribe(res => {
          //set value
          this.resetVar();
          this.setFormValue();
          this.global.swalSuccess('Success');
        }, Error => {
          this.global.swalAlertError();
          console.log(Error);
        })

    }
  }

  cancel() {
    this.resetVar;
    this.setFormValue();
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  setFormValue() {
    this.api.getMaintenanceOnlineRegSettings(this.onlineRegSettingToUpdate)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          this.onlineRegSettingName = res.data[0].name;
          this.onlineRegSettingLevel = res.data[0].level;
          if (this.onlineRegSettingToUpdate == res.data[0].code) {
            if (res.data[0].level == "06") {
              this.term = res.data[0].schoolYear.substr(-1, 1);
              this.f.term.setValue(res.data[0].schoolYear.substr(-1, 1));
              this.f.term.enable();
            } else {
              this.term = '1';
              this.f.term.setValue('1');
              this.f.term.disable();
            }
            this.year1 = this.getsyLeft(res.data[0].schoolYear);
            this.year2 = this.getsyRight(res.data[0].schoolYear);
            this.status = res.data[0].status.toString();
            this.startDate=res.data[0].startDate ? formatDate(res.data[0].startDate,'yyyy-MM-dd','en'): '';

            this.f.year1.setValue(this.getsyLeft(res.data[0].schoolYear));
            this.f.year2.setValue(this.getsyRight(res.data[0].schoolYear));
            this.f.status.setValue(res.data[0].status.toString());

            this.f.startDate.setValue(res.data[0].startDate ?
              formatDate(res.data[0].startDate,'yyyy-MM-dd','en'): '');
          }
        }
      }, Error => {
        this.global.swalAlertError();
      });
  }

  resetVar() {
    this.term = '';
    this.year1 = '';
    this.year2 = '';
    this.submitted = false;
    this.btnSaveDisabled = true
    this.ctrTerm = 0;
    this.ctrYear1 = 0;
    this.ctrYear2 = 0;
    this.ctrStatus = 0;
    this.onlineRegSettingName = '';
    this.onlineRegSettingLevel = '';
  }

  onChange(value, elementId) {
    switch (elementId) {
      case 'onlineRegSettingToUpdate': {
        this.resetVar();
        this.setFormValue();
        break;
      }
      case 'term': {
        //counter = 1
        if (elementId == 'term' && this.term != value.value) { this.ctrTerm = 1 }
        //counter = 0
        if (elementId == 'term' && this.term == value.value) { this.ctrTerm = 0 }
        break;
      }
      case 'year1': {
        //counter = 1
        if (elementId == 'year1' && this.year1 != value) { this.ctrYear1 = 1 }
        //counter = 0
        if (elementId == 'year1' && this.year1 == value) { this.ctrYear1 = 0 }
        break;
      }
      case 'year2': {
        //counter = 1
        if (elementId == 'year2' && this.year2 != value) { this.ctrYear2 = 1 }
        //counter = 0
        if (elementId == 'year2' && this.year2 == value) { this.ctrYear2 = 0 }
        break;
      }
      case 'status': {
        //counter = 1
        if (elementId == 'status' && this.status != value.value) { this.ctrStatus = 1 }
        //counter = 0
        if (elementId == 'status' && this.status == value.value) { this.ctrStatus = 0 }
        break;
      }
      case 'startDate': {
        if(value){
          // console.log(value ? formatDate(value,'yyyy-MM-dd','en') : '');
          // console.log(this.startDate);
          // console.log(this.ctrStartDate);
          //this.startDate
          //counter = 1
          if (elementId == 'startDate' && (this.startDate !=
              value ? formatDate(value,'yyyy-MM-dd','en') : '')
            ) {
                this.ctrStartDate = 1
              }
          //counter = 0
          if (elementId == 'startDate' && this.startDate ==
              value ? formatDate(value,'yyyy-MM-dd','en') : ''
            ) {
              this.ctrStartDate = 0
            }
        }
        break;
      }
    }
    if (this.ctrTerm == 1 || this.ctrYear1 == 1 || this.ctrYear2 == 1 || this.ctrStatus == 1 || this.ctrStartDate == 1)  {
      this.btnSaveDisabled = false;
    }

    if (this.ctrTerm == 0 && this.ctrYear1 == 0 && this.ctrYear2 == 0 && this.ctrStatus == 0 && this.ctrStartDate == 0) {
      this.btnSaveDisabled = true;
    }
  }

  getOnlineRegSettingAll() {
    this.onlineRegistrationSettingsArray = undefined;
    this.global.swalLoading('Retrieving Online Registration Settings')
    this.api.getMaintenanceOnlineRegSettings('')
      .map(response => response.json())
      .subscribe(res => {
        this.onlineRegistrationSettingsArray = [];
        //console.log('Domain: ' +this.global.domain);
        //console.log(this.global.viewdomainname);
        //console.log(this.global.viewdomain);
        //console.log(this.global.
        //console.log(res.data);
       if(this.global.domain == 'COLLEGE'){
        //this.onlineRegistrationSettingsArray = res.data
         for(var i=0; i < res.data.length; i++){
            if(res.data[i].level == '06' && res.data[i].appName == "Online Registration" && !this.global.viewdomain.includes(['36','93'])) {
              this.onlineRegistrationSettingsArray.push(res.data[i]);
            }
           if(res.data[i].level == '06' && this.global.viewdomain.includes('36') && res.data[i].appName == "Myportal") {
             this.onlineRegistrationSettingsArray.push(res.data[i]);
           }
         }
       }
       if(this.global.domain == 'HIGHSCHOOL'){
        for(var i=0; i < res.data.length; i++){
          if(res.data[i].level == '04' && this.global.viewdomainname.includes('HS')) { //Junior High
            this.onlineRegistrationSettingsArray.push(res.data[i]);
          }
          if(res.data[i].level == '05' && this.global.viewdomainname.includes('HSS')) { //Junior High
            this.onlineRegistrationSettingsArray.push(res.data[i]);
          }
        }
       }

       if(this.global.domain == 'ELEMENTARY'){
        for(var i=0; i < res.data.length; i++){
          if(res.data[i].level == '01' || res.data[i].level == '02' || res.data[i].level == '03') {
            this.onlineRegistrationSettingsArray.push(res.data[i]);
          }
        }
       }

        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError();
      });
  }

  yearchange1() {
    this.f.year2.setValue(this.f.year1.value + 1);
  }
  yearchange2() {
    this.f.year1.setValue(this.f.year2.value - 1);
  }

  getsyLeft(recordSY) {
    var y = parseInt(recordSY.substring(0, 4));
    return y;
  }

  getsyRight(recordSY) {
    var y = parseInt(recordSY.substring(0, 4)) + 1;
    return y;
  }

}
