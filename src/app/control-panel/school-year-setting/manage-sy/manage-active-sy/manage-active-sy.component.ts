import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../global.service';
import { ApiService } from './../../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-manage-active-sy',
  templateUrl: './manage-active-sy.component.html',
  styleUrls: ['./manage-active-sy.component.css']
})
export class ManageActiveSyComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private api: ApiService, public global: GlobalService) { }

  //var initialization
  //#region
  activeSY;
  conflictToCheckSY;
  year1;
  year2;
  activeterm;
  configSYToUpdate = '0';
  form: FormGroup;
  btnSaveDisabled = true;
  submitted = false;
  ctrTerm = 0;
  ctrYear1 = 0;
  ctrYear2 = 0;
  //#endregion

  ngOnInit() {
    this.form = this.formBuilder.group({
      activeterm: ['', Validators.required],
      year1: ['', Validators.required],
      year2: ['', Validators.required]
    })
    this.getActiveSY()
  }

  getActiveSY() {
    this.api.getMaintenanceActiveSY()
      .map(response => response.json())
      .subscribe(res => {
        this.activeSY = res.data.schoolYear;
        this.conflictToCheckSY = res.data.syToCheckConflict;
        this.setFormValue();
      }, Error => {
        this.global.swalAlertError();
      })
  }

  setFormValue() {
    if (this.configSYToUpdate == '0') {
      this.activeterm = this.activeSY.substr(-1, 1);
      this.year1 = this.getsyLeft(this.activeSY);
      this.year2 = this.getsyRight(this.activeSY);

      //set value of form      
      this.f.activeterm.setValue(this.activeterm);
      this.f.year1.setValue(this.year1);
      this.f.year2.setValue(this.year2);

    } else {
      this.activeterm = this.conflictToCheckSY.substr(-1, 1);
      this.year1 = this.getsyLeft(this.conflictToCheckSY);
      this.year2 = this.getsyRight(this.conflictToCheckSY);

      //set value of form      
      this.f.activeterm.setValue(this.activeterm);
      this.f.year1.setValue(this.year1);
      this.f.year2.setValue(this.year2);
    }
  }

  resetVar() {
    this.activeterm = '';
    this.year1 = '';
    this.year2 = '';
    this.submitted = true;
    this.btnSaveDisabled = true
    this.ctrTerm = 0;
    this.ctrYear1 = 0;
    this.ctrYear2 = 0
  }

  save() {
    this.submitted = true;
    if(this.f.year1.value != null && this.f.year2.value != null) {
      if (this.f.year1.value.toString().length < 4 || this.f.year1.value.toString().length > 4) {
        this.f.year1.setErrors({ 'minmaxlength': true });
      }

      if (this.f.year2.value.toString().length < 4 || this.f.year2.value.toString().length > 4) {
        this.f.year2.setErrors({ 'minmaxlength': true });
      }
    }

    if (!this.form.errors && !this.form.invalid && !this.f.year1.errors && !this.f.year2.errors) {
      //save
      var activeSY_toSave;
      var conflictToCheckSY_toSave;

      if (this.configSYToUpdate == '0') {
        activeSY_toSave = this.f.year1.value.toString() + this.f.year2.value.toString().substr(2, 2) + this.f.activeterm.value.toString();
        conflictToCheckSY_toSave = this.conflictToCheckSY;
      } else {
        conflictToCheckSY_toSave = this.f.year1.value.toString() + this.f.year2.value.toString().substr(2, 2) + this.f.activeterm.value.toString();
        activeSY_toSave = this.activeSY;
      }

      this.api.putMaintenanceActiveSY(
        {
          "schoolyear": activeSY_toSave,
          "SYToCheckConflict": conflictToCheckSY_toSave
        }
      )
        .map(response => response.json())
        .subscribe(res => {
          //set initial value
          this.resetVar();
          this.getActiveSY;
          this.global.swalSuccess('Success');
        }, Error => {
          this.global.swalAlertError();
          console.log(Error);
        })

    }
  }

  cancel() {
    this.resetVar();
    this.getActiveSY();

  }


  onChange(value, elementId) {
    switch (elementId) {
      case 'configSYToUpdate': {
        this.resetVar();
        this.getActiveSY();
        break;
      }
      case 'activeterm': {
        //counter = 1
        if (elementId == 'activeterm' && this.activeterm != value.value) { this.ctrTerm = 1 }
        //counter = 0
        if (elementId == 'activeterm' && this.activeterm == value.value) { this.ctrTerm = 0 }
        break;
      }
      case 'year1': {
        //counter = 1
        if (elementId == 'year1' && this.year1 != value) { this.ctrYear1 = 1 }
        //counter = 0
        if (elementId == 'year1' && this.year1 == value) { this.ctrYear1 = 0 }
        break;
      }
      case 'year2': {
        //counter = 1
        if (elementId == 'year2' && this.year2 != value) { this.ctrYear2 = 1 }
        //counter = 0
        if (elementId == 'year2' && this.year2 == value) { this.ctrYear2 = 0 }
        break;
      }
    }

    if (this.ctrTerm == 1 || this.ctrYear1 == 1 || this.ctrYear2 == 1) {
      this.btnSaveDisabled = false;
    }

    if (this.ctrTerm == 0 && this.ctrYear1 == 0 && this.ctrYear2 == 0) {
      this.btnSaveDisabled = true;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  yearchange1() {
    this.f.year2.setValue(this.f.year1.value + 1);
  }
  yearchange2() {
    this.f.year1.setValue(this.f.year2.value - 1);
  }

  getsyLeft(recordSY) {
    var y = parseInt(recordSY.substring(0, 4));
    return y;
  }

  getsyRight(recordSY) {
    var y = parseInt(recordSY.substring(0, 4)) + 1;
    return y;
  }

}
