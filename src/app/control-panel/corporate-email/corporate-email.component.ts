import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { StudentLookupComponent } from './../../academic/lookup/student-lookup/student-lookup.component';

import Swal from 'sweetalert2';
const swal = Swal;

@Component({
  selector: 'app-corporate-email',
  templateUrl: './corporate-email.component.html',
  styleUrls: ['./corporate-email.component.css']
})
export class CorporateEmailComponent implements OnInit {

  tabbing = 0;
  image2: any = 'assets/noimage.jpg';
  name2: any = '';
  position2: any = '';
  id2: string = '';
  checkid2: string = '';
  bday = '';
  submit2: boolean = false;
  corporateEmail_Array = null;
  enrollmentStatus = '';
  emailFound: boolean = false;
  enrollmentHistory_Array = null;
  swalTxt;
  newSY=this.global.syear;

  constructor(public dialog: MatDialog
    , private domSanitizer: DomSanitizer
    , public global: GlobalService
    , private api: ApiService
  ) { }

  ngOnInit() {
    if(this.global.domain==='HIGHSCHOOL' || this.global.domain==='ELEMENTARY'){
      this.newSY=this.global.syear.slice(0,-1);
    }
  }

  getindex(tab) {
    this.tabbing = tab.index;
    this.clear2();
  }

  excutePostApi(){      
    this.api.postCorporateEmailCreate({      
      'idNumber': this.id2,
      'schoolYear': this.newSY,
      'createdAt': 2
    })
      .map(response => response.json())
      .subscribe(res => {  
        this.emailFound=true;          
        //get corporate email details
        this.corporateEmail_Array = [];
        this.api.getCorporateEmailDetails(this.id2)
          .map(response => response.json())
          .subscribe(res => {
            this.global.swalClose();
            this.corporateEmail_Array = res.data;
          })

      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error)
      });     
  }

  excuteDeleteApi(){
    this.corporateEmail_Array=undefined;
    this.api.deleteCorporateEmail(this.id2)
      .map(response => response.json())
      .subscribe(res => {            
        //get corporate email details        
        this.corporateEmail_Array = [];
        this.api.getCorporateEmailDetails(this.id2)
          .map(response => response.json())
          .subscribe(res => {
            this.global.swalClose();
            this.corporateEmail_Array = res.data;
            this.emailFound=false;
          })

      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error)
      });     
  }

  swalConfirm(cmd) {    
    if(cmd=='update'){
      this.swalTxt='The student might not see his/her corporate email details in his/her portal!';
    }else if(cmd==='delete'){
      this.swalTxt='This command is irreversible!';
    }else if(cmd==='create'){
      this.swalTxt='';
    }
    swal.fire({
      title: 'Are you sure?',
      text: this.swalTxt,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {            
        if(cmd==='update' || cmd=='create')
        {
          this.excutePostApi();
        }else if(cmd==='delete'){
          //console.log('Deleted');
          this.excuteDeleteApi();         
        }                                      
      }else if(result.dismiss===swal.DismissReason.cancel){
        swal.fire({
          title:'Cancelled',
          type: 'error',
          text: 'Changes are not saved!',
          timer: 1500
        })        
      }
    })
  }


  onSubmit(cmd) {
    if (this.enrollmentStatus.toLowerCase() === 'Paid'.toLowerCase()) {
      this.swalConfirm(cmd);
      console.log(cmd);
    }
    else {
      this.global.swalAlert('Student was not officially enrolled in the Active Configuration SY selected!', '', 'warning');
    }
  }

  keyDownFunction2(event) {      
      if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.name2='';
      this.position2='';
      this.bday='';
      this.checkid2='';
      this.enrollmentStatus='';
      this.emailFound=false;      
      if (this.id2 != '') {
        this.global.swalLoading('Loading Person Information');
        this.api.getStudent(this.id2, this.newSY, this.global.domain)
          .map(response => response.json())
          .subscribe(res => {            
            //console.log(res.data);           

            if (res.message != undefined && res.message == 'Student found.') {
              this.image2 = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
              this.submit2 = true;
              if (res.data.suffixName == null) {
                res.data.suffixName = ''
              }
              this.name2 = res.data.firstName + " " + res.data.middleName + " " + res.data.lastName + "" + res.data.suffixName;              
              this.checkid2 = res.data.idNumber
              this.bday = res.data.dateOfBirth     

              this.enrollmentHistory_Array = [];
              //get enrollment history
              this.api.getStudentEnrollmentHistory(this.id2)
                .map(response => response.json())
                .subscribe(res => {                 
                  this.enrollmentHistory_Array = res.data;
                  if (this.enrollmentHistory_Array != null) {
                    for (let a of this.enrollmentHistory_Array) {
                      if (this.newSY == a.schoolYear) {                        
                        this.enrollmentStatus = a.status;
                        this.position2 = a.course + ' - ' + a.yearOrGradeLevel;
                      }
                      //console.log(this.enrollmentStatus);
                    }
                  }                 
                })

              //get corpo email details
              this.corporateEmail_Array = [];
              this.api.getCorporateEmailDetails(this.id2)
                .map(response => response.json())
                .subscribe(res => {    
                  this.corporateEmail_Array=res.data;                                        
                  if (this.corporateEmail_Array != null) {
                    this.emailFound = true;
                    this.corporateEmail_Array = res.data;
                  }                
                })
            } else {
              this.global.swalAlert(res.message, '', 'warning');
            }
            this.global.swalClose();
          }, Error => {
            this.corporateEmail_Array = [];
            this.global.swalAlertError(Error);
          });

      }
    }
  }

  studlookup(): void {
    const dialogRef = this.dialog.open(StudentLookupComponent, {
      width: '600px', disableClose: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          this.id2 = result.result;
          this.keyDownFunction2('onoutfocus')
        }
      }
    });
  }

  clear2() {
    this.id2 = '';
    this.image2 = 'assets/noimage.jpg';
    this.name2 = '';
    this.position2 = '';
    this.bday = '';
    this.enrollmentStatus = '';
    this.corporateEmail_Array = null;
    this.emailFound = true;
  }


}
