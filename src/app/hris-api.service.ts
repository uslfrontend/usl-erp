import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class HrisApiService {

  constructor(private http: Http, public global: GlobalService) { }

  //------------------------------------------CHRIS-HRIS-----------------------------------------//

  getHRISMaintenanceEmpStatus() {
    return this.http.get(this.global.api + 'HRISMaintenance/EmpStatus/', this.global.option);
  }

  getHRISMaintenanceEmpType() {
    return this.http.get(this.global.api + 'HRISMaintenance/EmpType/', this.global.option);
  }

  getHRISMaintenanceRank() {
    return this.http.get(this.global.api + 'HRISMaintenance/Rank/', this.global.option);
  }

  getHRISMaintenanceCompany() {
    return this.http.get(this.global.api + 'HRISMaintenance/Company/', this.global.option);
  }

  getHRISMaintenanceTraningType() {
    return this.http.get(this.global.api + 'HRISMaintenance/TraningType/', this.global.option);
  }

  getHRISMaintenancePSGC() {
    return this.http.get(this.global.api + 'HRISMaintenance/PSGC/', this.global.option);
  }

  getHRISMaintenanceTrainingsAndSeminarsStatus() {
    return this.http.get(this.global.api + 'HRISMaintenance/TrainingsAndSeminarsStatus/', this.global.option);
  }

  getHRISMaintenanceLeaveTypes() {
    return this.http.get(this.global.api + 'HRISMaintenance/LeaveTypes/', this.global.option);
  }

  getHRISMaintenanceLeaveMatrixes() {
    return this.http.get(this.global.api + 'HRISMaintenance/LeaveMatrixes/', this.global.option);
  }

  getHRISMaintenanceLeaveActions() {
    return this.http.get(this.global.api + 'HRISMaintenance/LeaveActions/', this.global.option);
  }

  getHRISMaintenanceLeaveStatuses() {
    return this.http.get(this.global.api + 'HRISMaintenance/LeaveStatuses/', this.global.option);
  }

  getHRISMaintenanceDepartmentsPerEmployeeAppoitment(idNumber) {
    return this.http.get(this.global.api + 'HRISMaintenance/DepartmentsPerEmployeeAppoitment/' + idNumber, this.global.option);
  }

  getHRISMaintenanceResearchTypes() {
    return this.http.get(this.global.api + 'HRISMaintenance/ResearchTypes/', this.global.option);
  }

  getHRISMaintenanceDepartment() {
    return this.http.get(this.global.api + 'HRISMaintenance/Department/', this.global.option);
  }

  postHRISMaintenanceInsertDepartment(data) {
    return this.http.post(this.global.api + 'HRISMaintenance/InsertDepartment/', data, this.global.option)
  }

  putHRISMaintenanceUpdateDepartment(idNumber, data) {
    return this.http.put(this.global.api + 'HRISMaintenance/UpdateDepartment/', idNumber + data, this.global.option)
  }

  deleteHRISMaintenanceDeleteDepartment(idNumber) {
    return this.http.delete(this.global.api + 'HRISMaintenance/DeleteDepartment/' + idNumber, this.global.option)
  }

  getHRISMaintenancePositionList() {
    return this.http.get(this.global.api + 'HRISMaintenance/PositionList/', this.global.option);
  }

  postHRISMaintenanceInsertPosition(data) {
    return this.http.post(this.global.api + 'HRISMaintenance/InsertPosition/', data, this.global.option)
  }

  putHRISMaintenanceUpdatePosition(idNumber, data) {
    return this.http.put(this.global.api + 'HRISMaintenance/UpdatePosition/', idNumber + data, this.global.option)
  }

  deleteHRISMaintenanceDeletePosition(idNumber) {
    return this.http.delete(this.global.api + 'HRISMaintenance/DeletePosition/' + idNumber, this.global.option)
  }

  getHRISMaintenanceRankList() {
    return this.http.get(this.global.api + 'HRISMaintenance/RankList/', this.global.option);
  }

  postHRISMaintenanceInsertRank(data) {
    return this.http.post(this.global.api + 'HRISMaintenance/InsertRank/', data, this.global.option)
  }

  putHRISMaintenanceUpdateRank(idNumber, data) {
    return this.http.put(this.global.api + 'HRISMaintenance/UpdateRank/', idNumber + data, this.global.option)
  }

  deleteHRISMaintenanceDeleteRank(idNumber) {
    return this.http.delete(this.global.api + 'HRISMaintenance/DeleteRank/' + idNumber, this.global.option)
  }

  getHRISMaintenanceItems(deptId, employeeId) {
    return this.http.get(this.global.api + 'HRISMaintenance/Items/' + '?' + 'departmentId=' + deptId + '&employeeId=' + employeeId, this.global.option);
  }

  postHRISMaintenanceInsertItem(data) {
    return this.http.post(this.global.api + 'HRISMaintenance/InsertItem/', data, this.global.option)
  }

  putHRISMaintenanceUpdateItem(itemid, data) {
    return this.http.put(this.global.api + 'HRISMaintenance/UpdateItem/', itemid + data, this.global.option)
  }

  deleteHRISMaintenanceDeleteItem(itemid) {
    return this.http.delete(this.global.api + 'HRISMaintenance/DeleteItem/' + itemid, this.global.option)
  }

  getHRISMaintenanceHeadsOfOffices() {
    return this.http.get(this.global.api + 'HRISMaintenance/HeadsOfOffices/', this.global.option);
  }

  getHRISMaintenanceClassifications() {
    return this.http.get(this.global.api + 'HRISMaintenance/Classifications/', this.global.option);
  }

  getHRISMaintenanceRetirementTypes() {
    return this.http.get(this.global.api + 'HRISMaintenance/RetirementTypes/', this.global.option);
  }

  getHRISMaintenanceSpeakingEngagementRoles() {
    return this.http.get(this.global.api + 'HRISMaintenance/SpeakingEngagementRoles/', this.global.option);
  }

  getEmployeePersonalInformation(idNumber) {
    return this.http.get(this.global.api + 'Employee/PersonalInfo/' + idNumber, this.global.option);
  }

  getEmployee(idNumber) {
    return this.http.get(this.global.api + 'Employee/' + idNumber, this.global.option);
  }

  getEmployeeAppointment(idNumber) {
    return this.http.get(this.global.api + 'Employee/Appointment/' + idNumber, this.global.option);
  }

  getEmployeeAppointmentbyActive(idNumber, active) {
    return this.http.get(this.global.api + 'Employee/Appointment/' + idNumber + '?active=' + active, this.global.option);
  }


  getEmployeePerson(idNumber) {
    return this.http.get(this.global.api + 'Employee/Person/' + idNumber, this.global.option);
  }

  putEmployeePerson(idNumber, data) {
    return this.http.put(this.global.api + 'Employee/Person/' + idNumber, data, this.global.option);
  }

  getEmployeeContract(idNumber) {
    return this.http.get(this.global.api + 'Employee/Contract/' + idNumber, this.global.option);
  }

  getEmployeeRank(idNumber) {
    return this.http.get(this.global.api + 'Employee/Rank/' + idNumber, this.global.option);
  }

  getEmployeeChrildren(idNumber) {
    return this.http.get(this.global.api + 'Employee/Children/' + idNumber, this.global.option);
  }

  getEmployeeEducationalBackground(idNumber) {
    return this.http.get(this.global.api + 'Employee/EducationalBackground/' + idNumber, this.global.option);
  }

  getEmployeeEligibility(idNumber) {
    return this.http.get(this.global.api + 'Employee/Eligibility/' + idNumber, this.global.option);
  }

  getEmployeeSeminarsAndTrainings(idNumber) {
    return this.http.get(this.global.api + 'Employee/SeminarsAndTrainings/' + idNumber, this.global.option);
  }

  getEmployeeWorkExperience(idNumber) {
    return this.http.get(this.global.api + 'Employee/WorkExperience/' + idNumber, this.global.option);
  }

  getEmployeeOrganization(idNumber) {
    return this.http.get(this.global.api + 'Employee/Organization/' + idNumber, this.global.option);
  }

  getEmployeeResearch(idNumber) {
    return this.http.get(this.global.api + 'Employee/Research/' + idNumber, this.global.option);
  }

  getEmployeeCommunityExtension(idNumber) {
    return this.http.get(this.global.api + 'Employee/CommunityExtension/' + idNumber, this.global.option);
  }

  getEmployeeAwards(idNumber) {
    return this.http.get(this.global.api + 'Employee/Awards/' + idNumber, this.global.option);
  }

  getEmployeeSpeakingEngagement(idNumber) {
    return this.http.get(this.global.api + 'Employee/SpeakingEngagement/' + idNumber, this.global.option);
  }

  deleteEmployeeChild(childID, id) {
    return this.http.delete(this.global.api + 'Employee/DeleteChild/' + childID + '/' + id, this.global.option)
  }

  deleteEmployeeDeleteEducationalBackground(educBgId) {
    return this.http.delete(this.global.api + 'Employee/DeleteEducationalBackground/' + educBgId, this.global.option)
  }

  deleteEmployeeEligibility(id) {
    return this.http.delete(this.global.api + 'Employee/Eligibility/' + id, this.global.option)
  }

  putEmployeeSeminarAndTraining(id, data) {
    return this.http.put(this.global.api + 'Employee/UpdateSeminarAndTraining/' + id, data, this.global.option)
  }

  deleteEmployeeSeminarAndTraining(id) {
    return this.http.delete(this.global.api + 'Employee/DeleteSeminarAndTraining/' + id, this.global.option)
  }

  putEmployeeOrganization(id, data) {
    return this.http.put(this.global.api + 'Employee/Organization/' + id, data, this.global.option)
  }

  deleteEmployeeOrganization(id) {
    return this.http.delete(this.global.api + 'Employee/Organization/' + id, this.global.option)
  }

  deleteEmployeeWorkExperience(id) {
    return this.http.delete(this.global.api + 'Employee/WorkExperience/' + id, this.global.option)
  }

  putEmployeeUpdateResearch(id, data) {
    return this.http.put(this.global.api + 'Employee/UpdateResearch/', id + data, this.global.option)
  }

  deleteEmployeeResearch(id) {
    return this.http.delete(this.global.api + 'Employee/DeleteResearch/' + id, this.global.option)
  }

  putEmployeeAward(id, data) {
    return this.http.put(this.global.api + 'Employee/Award/', id + data, this.global.option)
  }

  deleteEmployeeAward(id) {
    return this.http.delete(this.global.api + 'Employee/Award/' + id, this.global.option)
  }

  deleteEmployeeSpeakingEngagement(id) {
    return this.http.delete(this.global.api + 'Employee/SpeakingEngagement/' + id, this.global.option)
  }

  deleteEmployeeAppointment(id) {
    return this.http.delete(this.global.api + 'Employee/Appointment/' + id, this.global.option)
  }

  deleteEmployeeContract(id) {
    return this.http.delete(this.global.api + 'Employee/Contract/' + id, this.global.option)
  }

  deleteEmployeeRank(id, evalID) {
    return this.http.delete(this.global.api + 'Employee/Rank/' + id + '?evaluationid=' + evalID, this.global.option)
  }

  deleteEmployeeCommunityExtension(id) {
    return this.http.delete(this.global.api + 'Employee/CommunityExtension/' + id, this.global.option)
  }

  postEmployeeAward(data) {
    return this.http.post(this.global.api + 'Employee/Award/', data, this.global.option)
  }

  postEmployeeChildren(id, data) {
    return this.http.post(this.global.api + 'Employee/Child/' + id, data, this.global.option)
  }

  postEmployeeCommunityExtension(id, data) {
    return this.http.post(this.global.api + 'Employee/CommunityExtension/' + id, data, this.global.option)
  }

  putEmployeeCommunityExtension(id, data) {
    return this.http.put(this.global.api + 'Employee/CommunityExtension/' + id, data, this.global.option)
  }

  postEmployeeEducationalBackground(id, data) {
    return this.http.post(this.global.api + 'Employee/EducationalBackground/' + id, data, this.global.option)
  }

  getPublicAPIProgramNames(prog) {
    return this.http.get(this.global.api + 'PublicAPI/ProgramNames/' + prog, this.global.option)
  }

  putEmployeeEducationalBackground(id, data) {
    return this.http.put(this.global.api + 'Employee/EducationalBackground/' + id, data, this.global.option)
  }

  postEmployeeEligibility(id, data) {
    return this.http.post(this.global.api + 'Employee/Eligibility/' + id, data, this.global.option)
  }

  putEmployeeEligibility(id, data) {
    return this.http.put(this.global.api + 'Employee/Eligibility/' + id, data, this.global.option)
  }

  putEmployeeAppointment(id, data) {
    return this.http.put(this.global.api + 'Employee/Appointment/' + id, data, this.global.option)
  }

  postEmployeeAppointment(id, data) {
    return this.http.post(this.global.api + 'Employee/Appointment/' + id, data, this.global.option)
  }

  putEmployeeContract(id, data) {
    return this.http.put(this.global.api + 'Employee/Contract/' + id, data, this.global.option)
  }

  postEmployeeContract(id, data) {
    return this.http.post(this.global.api + 'Employee/Contract/' + id, data, this.global.option)
  }

  postEmployeeRank(id, data) {
    return this.http.post(this.global.api + 'Employee/Rank/' + id, data, this.global.option)
  }

  putEmployeeRank(id, evalID, data) {
    return this.http.put(this.global.api + 'Employee/Rank/' + id + '?evaluationid=' + evalID, data, this.global.option)
  }

  postEmployeeOrganization(id, data) {
    return this.http.post(this.global.api + 'Employee/Organization/' + id, data, this.global.option)
  }

  postEmployeeResearch(id, data) {
    return this.http.post(this.global.api + 'Employee/InsertResearch/' + id, data, this.global.option)
  }

  putEmployeeReasearch(id, data) {
    return this.http.put(this.global.api + 'Employee/UpdateResearch/' + id, data, this.global.option)
  }

  postEmployeeSeminarAndTraining(id, data) {
    return this.http.post(this.global.api + 'Employee/InsertSeminarAndTraining/' + id, data, this.global.option)
  }

  postEmployeeSpeakingEngagement(data) {
    return this.http.post(this.global.api + 'Employee/SpeakingEngagement/', data, this.global.option)
  }

  putEmployeeSpeakingEngagement(id, data) {
    return this.http.put(this.global.api + 'Employee/SpeakingEngagement/' + id, data, this.global.option)
  }

  postEmployeeSpeakingEngagementRole(data) {
    return this.http.post(this.global.api + 'Employee/SpeakingEngagementRole/', data, this.global.option)
  }

  deleteEmployeeSpeakingEngagementRole(id) {
    return this.http.delete(this.global.api + 'Employee/SpeakingEngagementRole/' + id, this.global.option)
  }

  putEmployeeChild(id, data) {
    return this.http.put(this.global.api + 'Employee/Child/' + id, data, this.global.option)
  }

  putEmployeePersonalInfo(id, data) {
    return this.http.put(this.global.api + 'Employee/PersonalInfo/' + id, data, this.global.option)
  }

  postEmployeeWorkExperience(id, data) {
    return this.http.post(this.global.api + 'Employee/WorkExperience/' + id, data, this.global.option)
  }

  putEmployeeWorkExperience(id, data) {
    return this.http.put(this.global.api + 'Employee/WorkExperience/' + id, data, this.global.option)
  }

  getHRISMaintenanceEOSType() {
    return this.http.get(this.global.api + 'HRISMaintenance/EndOfServiceTypes/', this.global.option);
  }

  postEmployeeEOS(employeeID, type, eOSDate, remarks) {
    return this.http.post(this.global.api + 'Employee/EndOfService/' + employeeID + '/' + type + '/' + eOSDate.toLocaleString() + '?remarks=' + remarks, '', this.global.option)
  }

  getEmployeeEndOfService(employeeID) {
    return this.http.get(this.global.api + 'Employee/EndOfService/' + '?employeeID=' + employeeID, this.global.option);
  }

  deleteEmployeeEOS(employeeID, type) {
    return this.http.delete(this.global.api + 'Employee/EndOfService/' + employeeID + '/' + type, this.global.option)
  }

  putEmployeeEOS(employeeID, type, eOSDate, remarks) {
    return this.http.put(this.global.api + 'Employee/EndOfService/' + employeeID + '/' + type + '/' + eOSDate.toLocaleString() + '?remarks=' + remarks, '', this.global.option)
  }

  getHRISMaintenanceAppointmentCategories() {
    return this.http.get(this.global.api + 'HRISMaintenance/AppointmentCategories/', this.global.option);
  }

  getHRISMaintenanceEvaluationStatuses() {
    return this.http.get(this.global.api + 'HRISMaintenance/EvaluationStatuses', this.global.option);
  }

  getHRISMaintenanceRankStatuses() {
    return this.http.get(this.global.api + 'HRISMaintenance/RankStatuses', this.global.option);
  }


  getHRISMaintenanceDepartmentbyID(id) {
    return this.http.get(this.global.api + 'HRISMaintenance/Department?departmentID=' + id, this.global.option);
  }

  getReportHRISEmployeeProfileByDept(DID) {
    return this.http.get(this.global.api + 'ReportHRIS/EmployeeProfileByDept/' + DID, this.global.option);
  }

  getReportHrisPerson(id) {
    return this.http.get(this.global.api + 'ReportHRIS/Person/' + id, this.global.option);
  }

  getReportActiveEmployeesMasterList() {
    return this.http.get(this.global.api + 'ReportHRIS/ActiveEmployeesMasterList/', this.global.option);
  }

  putLockUnlockEmployeeAccount(id, x) {
    return this.http.put(this.global.api + 'Employee/LockUnlockEmployeeAccount/' + id + '/' + x, {}, this.global.option)
  }

  getAccountUser(idNumber, includeIDPic) {
    return this.http.get(this.global.api + 'Employee/Account/User/' + idNumber + '/' + "?includeIDPic=" + includeIDPic, this.global.option)
  }


  //-----------------------------------------------------------------------------------------------------------//'

}
