import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrispersonLookupComponent } from './hrisperson-lookup.component';

describe('HrispersonLookupComponent', () => {
  let component: HrispersonLookupComponent;
  let fixture: ComponentFixture<HrispersonLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrispersonLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrispersonLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
