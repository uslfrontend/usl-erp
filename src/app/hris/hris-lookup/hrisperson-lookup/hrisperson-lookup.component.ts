

import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../global.service';
import { ApiService } from '../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';


@Component({
  selector: 'app-hrisperson-lookup',
  templateUrl: './hrisperson-lookup.component.html',
  styleUrls: ['./hrisperson-lookup.component.css']
})
export class HrispersonLookupComponent implements OnInit {


  x = 1
  arraystud = [];
  newArrayStud = [];
  keyword = ''
  tempkeyword = ''
  hide = 0

  exactmatch = false

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<HrispersonLookupComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService) { }

  ngOnInit() {
  }

  select(id) {
    this.dialogRef.close({ result: id });
  }
  keyDownFunction(event) {
    // console.log(this.keyword)
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.x = 0;
      if (this.keyword != '') {
        this.newArrayStud = [];
        this.tempkeyword = this.keyword;
        this.arraystud = undefined;
        this.api.getPersonLookup(this.keyword, true)
          .map(response => response.json())
          .subscribe(res => {
            
            if (res.data != '') {
              // console.log(res.data[0])
              // console.log()
              if(res.data.length>1){
                this.arraystud = res.data[0]
                .concat(res.data[1])
                .concat(res.data[2])
                // .filter(obj => obj.fullName.toLowerCase().includes(this.keyword.toLowerCase()))
                // .sort((a, b) => a.fullName.localeCompare(b.fullName));
              }else{
                this.arraystud = res.data[0]
                // .filter(obj => obj.fullName.toLowerCase().includes(this.keyword.toLowerCase()))
                // .sort((a, b) => a.fullName.localeCompare(b.fullName));
              }
              

              // console.log(this.arraystud)

              // for (var x = 0; x < this.arraystud.length; x++) {
              //   var lastname = this.arraystud[x].fullName.slice(0, this.arraystud[x].fullName.indexOf(','))
              //   if (this.keyword.toLowerCase() == lastname.toLowerCase()) {
              //     this.newArrayStud.push({
              //       dateOfBirth: this.arraystud[x].dateOfBirth,
              //       enrolled: this.arraystud[x].enrolled,
              //       fullName: this.arraystud[x].fullName,
              //       idNumber: this.arraystud[x].idNumber,
              //       personType: this.arraystud[x].personType,
              //     })
              //   }
              // }

              // let keywordCTR = this.keyword.length
              // const unique2 = this.arraystud.filter((obj, index) => {
              //   return index === this.arraystud.findIndex(o =>  obj.fullName.substring(0,keywordCTR) === o.fullName.substring(0,keywordCTR));
                
              // });


              // console.log(unique2)
              // console.log(this.arraystud)

              // console.log(this.newArrayStud)
              // this.arraystud = this.newArrayStud.concat(this.arraystud);


            } else {
              this.arraystud = []
            }

          }, Error => {
            this.global.swalAlertError(Error)
          });
      }
      // console.log(this.arraystud)
    }
  }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
  keyDownFunctionCODE(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.keyword == '') {
      } else {
        this.keyDownFunction('onoutfocus');
      }
    }
  }
}
