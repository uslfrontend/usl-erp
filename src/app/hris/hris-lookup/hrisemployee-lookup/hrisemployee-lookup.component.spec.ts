import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrisemployeeLookupComponent } from './hrisemployee-lookup.component';

describe('HrisemployeeLookupComponent', () => {
  let component: HrisemployeeLookupComponent;
  let fixture: ComponentFixture<HrisemployeeLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrisemployeeLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrisemployeeLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
