import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../global.service';
import { ApiService } from './../../../api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-hrisemployee-lookup',
  templateUrl: './hrisemployee-lookup.component.html',
  styleUrls: ['./hrisemployee-lookup.component.css']
})
export class HrisemployeeLookupComponent implements OnInit {

  x = 1
  arraystud = [];
  keyword = ''
  tempkeyword = ''
  newArrayStud = []

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<HrisemployeeLookupComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public global: GlobalService, private api: ApiService) { }

  ngOnInit() {
  }

  select(param) {
    this.dialogRef.close({ result: param.employeeId, name: param.fullName, data: param });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.x = 0;
      if (this.keyword != '') {
        this.newArrayStud = []
        this.tempkeyword = this.keyword;
        this.arraystud = undefined;
        try {
          if (this.data.searchType == 'faculty_evaluation') {
            this.api.getFacultyEvalPersonLookup(this.keyword)
              .map(response => response.json())
              .subscribe(res => {
                if (res.data != '') {
                  this.arraystud = res.data;
                  this.arraystud.sort((a, b) => a.fullName.localeCompare(b.fullName));
                } else {
                  this.arraystud = []
                }

                // sort by name
              }, Error => {
                this.global.swalAlertError(Error);
              });
          }
        } catch (error) {
          this.api.getEmployeeEmployeeLookup(this.keyword)
            .map(response => response.json())
            .subscribe(res => {
              if (res.data != '') {
                this.arraystud = res.data;
                // console.log(this.arraystud)
                this.arraystud.sort((a, b) => a.fullName.localeCompare(b.fullName));
                for (var x = 0; x < this.arraystud.length; x++) {
                  var lastname = this.arraystud[x].fullName.slice(0, this.arraystud[x].fullName.indexOf(','))
                  if (this.keyword.toLowerCase() == lastname.toLowerCase()) {
                    this.newArrayStud.push({
                      employeeId: this.arraystud[x].employeeId,
                      dtrid: this.arraystud[x].dtrid,
                      fullName: this.arraystud[x].fullName,
                      position: this.arraystud[x].position,
  
                    })
                  }
                }
              }
              else {
                this.arraystud = []
              }
              // sort by name
            }, Error => {
              this.global.swalAlertError(Error);
            });
        }
      }
    }
  }


  close(): void {
    this.dialogRef.close({ result: 'cancel' });
  }
  keyDownFunctionCODE(event) {
    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      if (this.keyword == '') {
      } else {
        this.keyDownFunction('onoutfocus');
      }
    }
  }
}
