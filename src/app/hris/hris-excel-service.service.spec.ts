import { TestBed } from '@angular/core/testing';

import { HrisExcelServiceService } from './hris-excel-service.service';

describe('HrisExcelServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HrisExcelServiceService = TestBed.get(HrisExcelServiceService);
    expect(service).toBeTruthy();
  });
});
