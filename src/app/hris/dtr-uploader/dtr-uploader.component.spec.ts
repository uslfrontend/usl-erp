import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtrUploaderComponent } from './dtr-uploader.component';

describe('DtrUploaderComponent', () => {
  let component: DtrUploaderComponent;
  let fixture: ComponentFixture<DtrUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtrUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtrUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
