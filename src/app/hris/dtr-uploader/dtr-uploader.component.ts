import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { MatDialog, } from '@angular/material';
import { HrisExcelServiceService } from '../hris-excel-service.service';
import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-dtr-uploader',
  templateUrl: './dtr-uploader.component.html',
  styleUrls: ['./dtr-uploader.component.scss']
})
export class DtrUploaderComponent implements OnInit {
  data = []
  error = false
  timeFormatRegex = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/;
  failedToUpload = []

  // excel variables
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  @ViewChild('uploadthis', { static: true }) uploadthis;

  percentageValue = 0;
  loadingSpinner = false;
  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService, private excelService: HrisExcelServiceService) {
  }

  ngOnInit() {
  }

  downloadExcel(): void {
    var arr = [];
    this.excelService.downloadFacultyEvalProgramExcelFormat()
  }

  onFileChange(evt: any) {
    // this.global.swalLoading('Uploading Excel Data...');
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      this.data = []
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {
        type: 'binary',
        cellDates: true,
        dateNF: 'dd/mm/yyyy'
      });
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      /* reset variables */
      this.error = false
      this.data = []
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.data.splice(0, 1);
      //filter empty strings
      this.data = this.data.filter(row => !row.some(elem => elem === ""));
      this.uploadthis.nativeElement.value = ''
      this.validateNameAndTime()
    };
    reader.readAsBinaryString(target.files[0]);
  }

  async validateNameAndTime() {
    let buffervalue = 100 / (this.data.length);
    // this.global.swalLoading('Uploading Excel Data...'+this.percentageValue+'%');
    this.loadingSpinner = true;
    for (let x = 0; x < this.data.length; x++) {
      try {
        this.percentageValue = Math.round(buffervalue * x)
        this.data[x][0] = this.removeQuotes(this.data[x][0])
        const response = await this.api.getEmployeeInfoByDtrId(this.data[x][0]).toPromise();
        const res = response.json();

        // Do whatever you need to do with the API response before moving on to the next index
        this.data[x][4] = this.formatDate(this.data[x][4])

        // Restructure data array
        this.data[x] = ([this.data[x][0], res.data[0].fullname, this.data[x][4]]);
      } catch (error) {
        // Handle the error as needed
        this.data[x] = ([this.data[x][0], "Unrecognized DTR ID: " + this.data[x][1], this.data[x][4]]);
        this.error = true;
      }
      if (x == this.data.length - 1) {
        this.loadingSpinner = false;
      }
    }
  }

  async upload() {
    this.global.swalLoading('Uploading to Server...');
    const maxRetries = 5;
    for (let x = 0; x < this.data.length; x++) {
      let retries = 0;
      while (retries < maxRetries) {
        try {
          const response = await this.api.postEmployeeDtr(this.data[x][0], this.data[x][2]).toPromise();
          const res = response.json();
          // Do whatever you need to do with the API response before moving on to the next index
          if (x == this.data.length - 1) {
            this.global.swalClose();
            this.global.swalSuccess("DTR Uploaded")
            this.error = true
          }
          break; // break out of retry loop if successful
        } catch (error) {
          retries++;
          // Handle the error as needed
          this.global.swalLoading(`Failed to upload data for index ${x} Retrying...`)
          if (retries >= maxRetries) {
            while (x < this.data.length) {
              this.failedToUpload.push(this.data[x])
              x++
            }
            // console.error(`Reached maximum number of retries (${maxRetries}) for index ${x}. Giving up.`)
            this.excelService.downloadFailedToUploadDTR(this.failedToUpload)
            this.global.swalAlert('No Internet Connection.', 'The remaining DTR that has not been uploaded will be imported to excel. Check your downloads folder.', 'warning')
            break; // break out of retry loop if maximum retries reached
          }
          // Add some delay between retries to avoid overwhelming the server
          await new Promise(resolve => setTimeout(resolve, 1000));
          // this.global.swalClose
        }
      }
    }
  }

  removeQuotes(str) {
    return str.replace(/'/g, '');
  }

  formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const hour = date.getHours().toString().padStart(2, '0');
    const minute = date.getMinutes().toString().padStart(2, '0');
    const second = date.getSeconds().toString().padStart(2, '0');
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  }

}


