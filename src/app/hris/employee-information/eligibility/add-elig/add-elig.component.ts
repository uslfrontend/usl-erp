import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { HrisApiService } from '../../../../hris-api.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-elig',
  templateUrl: './add-elig.component.html',
  styleUrls: ['./add-elig.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AddEligComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddEligComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private HrisApi: HrisApiService,
    private FormBuilder: FormBuilder,
    private http: Http
  ) {

  }

  form: FormGroup;
  exam
  rating
  date
  lnumber
  edate

  ngOnInit() {

    this.form = this.FormBuilder.group({
      exam: ['', Validators.required],
      rating: ['', Validators.required],
      date: ['', Validators.required],
      lnumber: ['', Validators.required],
      edate: ['', Validators.required],
    });
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {

		this.form.markAllAsTouched();
    if(this.form.get('exam').value != '' && (this.form.get('date').value != 'Invalid Date' && this.form.get('lnumber').value != '') &&
    this.form.get('rating').value != ''){
      let date = new Date(this.form.get('date').value).toLocaleString();
      if(this.form.get('edate').value != 'Invalid Date' && this.form.get('edate').value != '' && this.form.get('edate').value != 'Invalid Date'){
        let edate = new Date(this.form.get('edate').value).toLocaleString();
        this.form.get('edate').patchValue(edate);
      }
     
      this.form.get('date').patchValue(date);
  
      this.global.swalLoading('Adding Eligibility');
      this.HrisApi.postEmployeeEligibility(this.data.selectedID, {
        "examination": this.form.get('exam').value,
        "datepassed": this.form.get('date').value,
        "rating": this.form.get('rating').value,
        "licenseNumber": this.form.get('lnumber').value,
        "expirationDate": this.form.get('edate').value,
      })
        // this.http.post(this.global.api + 'Employee/Eligibility/' + , this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalClose();
          this.global.swalAlert('Eligibility created successfully.', "", 'success');
          this.dialogRef.close({ result: "Adding Success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
  
    }
   
  }
}
