import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { HrisApiService } from '../../../../hris-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-elig',
  templateUrl: './update-elig.component.html',
  styleUrls: ['./update-elig.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateEligComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateEligComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService

  ) { }

  exam
  rating
  date
  lnumber
  edate
  form: FormGroup;

  ngOnInit() {


    this.form = this.FormBuilder.group({
      exam: ['', Validators.required],
      rating: ['', Validators.required],
      date: ['', Validators.required],
      lnumber: ['', Validators.required],
      edate: ['', Validators.required],
    });

    //console.log(this.data.selectedEID)
    this.exam = this.data.selectedData.examination;
    this.rating = this.data.selectedData.rating;
    var sdate = new Date(this.data.selectedData.datepassed);
    this.date = sdate;
    var d = new Date(this.data.selectedData.expirationDate);
    this.edate = d;
    this.lnumber = this.data.selectedData.licenseNumber;

    this.form.get('exam').patchValue(this.exam);
    this.form.get('rating').patchValue(this.rating);
    this.form.get('date').patchValue(this.date);
    this.form.get('edate').patchValue(this.edate);
    this.form.get('lnumber').patchValue(this.lnumber);

  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {

    this.form.markAllAsTouched();
    if (this.form.get('exam').value != '' &&
      (this.form.get('date').value != 'Invalid Date' && this.form.get('date').value != '') && this.form.get('rating').value != '' &&
      this.form.get('lnumber').value != '' && this.form.get('lnumber').value != null && this.form.get('lnumber').value != undefined) {
      this.global.swalLoading('Updating Eligibility');

      let date = new Date(this.form.get('date').value).toLocaleString();
      // console.log(this.form.get('edate').value)
      if(this.form.get('edate').value != 'Invalid Date' && this.form.get('edate').value != '' && this.form.get('edate').value != 'Invalid Date'){
        let edate = new Date(this.form.get('edate').value).toLocaleString();
        this.form.get('edate').patchValue(edate);
      }
    


      this.form.get('date').patchValue(date);
 
      this.HrisApi.putEmployeeEligibility(+this.data.selectedEID, {
        "examination": this.form.get('exam').value,
        "datepassed": this.form.get('date').value,
        "rating": this.form.get('rating').value,
        "licenseNumber": this.form.get('lnumber').value,
        "expirationDate": this.form.get('edate').value,
      })
        // this.http.put(this.global.api+'Employee/Eligibility/',this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalClose();
          this.global.swalAlert(res.message, "", 'success');
          this.dialogRef.close({ result: "Update success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });

    }
  }

}
