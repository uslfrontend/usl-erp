import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalService } from '../../../global.service';
import { ApiService } from './../../../api.service';
import { HttpClient } from '@angular/common/http';
import { HrisApiService } from '../../../hris-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({

  selector: 'app-update-name',
  templateUrl: './update-name.component.html',
  styleUrls: ['./update-name.component.css']
})

export class UpdateNameComponent implements OnInit {


  id: any
  personNameInfo: any
  name: any
  fname: any
  lname: any
  mname: any
  suffix: any
  gender: any
  cstatus: any
  tempDB: any
  form: FormGroup;
  genderArray = [
    { label: 'Male', value: 'M' },
    { label: 'Female', value: 'F' }
  ]


constructor(
  public http: HttpClient,
  public dialogRef: MatDialogRef<UpdateNameComponent>, @Inject(MAT_DIALOG_DATA)
public data: any,
  private global: GlobalService,
  private FormBuilder: FormBuilder,
  private HrisApi: HrisApiService) {

  this.id = data.id
  this.personNameInfo = data.personNameInfo
  this.name = data.name
  this.fname = data.fname
  this.lname = data.lname
  this.mname = data.mname
  this.suffix = data.suffix
  this.gender = data.gender
  this.cstatus = data.cstatus
  this.tempDB = data.tempDB
 
}

ngOnInit() {
  // console.log(this.gender)

  this.form = this.FormBuilder.group({
    fname: ['', Validators.required],
    lname: ['', Validators.required],
  });

  this.form.get('fname').patchValue(this.fname);
  this.form.get('lname').patchValue(this.lname);


}

onNoClickclose(): void {
  this.dialogRef.close({ result: 'cancel' });
}

updatename() {

  this.global.swalLoading('Loading item resources')
  this.HrisApi.getEmployeePerson(this.id)
    .map(response => response.json())
    .subscribe(res => {
      this.personNameInfo = res.data;
      this.fname = res.data.firstName
      this.mname = res.data.middleName
      this.lname = res.data.lastName
      this.suffix = res.data.suffixName
      this.gender = res.data.gender
      this.cstatus = res.data.civilStatus
      this.tempDB = res.data.dateOfBirth
      this.global.swalClose();

    }, Error => {
      //console.log(Error);
      this.global.swalAlertError();
      //console.log(Error)
    });
  // this.updatenamevar = true

  this.onNoClickclose()
}

updatenameapi() {

  this.form.markAllAsTouched();
  if (this.form.get('fname').value != '' && this.form.get('lname').value != '') {
    var error = '';
    if (error == '') {
      var x = ''
      let date = new Date(this.tempDB).toLocaleString();
      this.global.swalLoading('Updating...');
      this.HrisApi.putEmployeePerson(this.id,
        {
          'FirstName': this.form.get('fname').value,
          'MiddleName': this.mname,
          'LastName': this.form.get('lname').value,
          'SuffixName': this.suffix,
          'Gender': this.gender,
          'CivilStatus': this.cstatus,
          'DateOfBirth': this.tempDB,
        })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
          // this.updatenamevar = false
          this.onNoClickclose()
        }, Error => {
          this.global.swalAlertError();
        });
    } else {
      this.global.swalAlert('The Following error has Occured', error, 'error');
    }

  }

}


}
