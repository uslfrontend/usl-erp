import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-update-porg',
  templateUrl: './update-porg.component.html',
  styleUrls: ['./update-porg.component.css']
})
export class UpdatePorgComponent implements OnInit {

  org
  position
  statusid
  idn
  date

  form: FormGroup;

  checked() {
    if (this.statusid == 0) this.statusid = 1
    else this.statusid = 0
  }

  constructor(public dialogRef: MatDialogRef<UpdatePorgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private httpC: HttpClient,
    private HrisApi: HrisApiService
  ) { }
  ngOnInit() {

    this.form = this.FormBuilder.group({
      position: ['', Validators.required],
      org: ['', Validators.required],
      date: ['', Validators.required],
    });


    this.idn = this.data.idnum
    this.position = this.data.selectedData.position
    this.org = this.data.selectedData.organization

    if (this.data.selectedData.statusID == 1 || this.data.selectedData.statusID == null) {
      this.statusid = 1;

    } else
      this.statusid = this.data.selectedData.statusID
    var d = new Date(this.data.selectedData.expirationDate);
    this.date = d;
    //console.log(this.global.requestid())

    this.form.get('position').patchValue(this.position);
    this.form.get('org').patchValue(this.org);
    this.form.get('date').patchValue(this.date);

  }


  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    this.form.markAllAsTouched();
    if (this.form.get('position').value != '' &&
      this.form.get('org').value != '' &&
      this.form.get('date').value != '') {
      let date = new Date(this.form.get('date').value).toLocaleString();
      this.global.swalLoading('Updating Organization');
      this.HrisApi.putEmployeeOrganization(this.data.selectedPID, {
        "position": this.form.get('position').value,
        "organization": this.form.get('org').value,
        "statusID": this.statusid,
        "approvedBy": this.global.requestid(),
        "expirationDate": date,
      })
        // this.http.put(this.global.api + 'Employee/Organization/' + , this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalAlert('Professional membership to organization updated successfully', "", 'success');
          this.dialogRef.close({ result: "Update success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      this.global.swalClose()
    }



  }

}
