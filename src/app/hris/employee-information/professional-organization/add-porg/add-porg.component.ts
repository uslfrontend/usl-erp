import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-add-porg',
  templateUrl: './add-porg.component.html',
  styleUrls: ['./add-porg.component.css']
})
export class AddPorgComponent implements OnInit {

  org
  position
  date
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddPorgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private httpC: HttpClient,
  ) { }


  ngOnInit() {
    this.form = this.FormBuilder.group({
      position: ['', Validators.required],
      org: ['', Validators.required],
      date: ['', Validators.required],
    });
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {

    this.form.markAllAsTouched();
    if (this.form.get('position').value != '' &&
      this.form.get('org').value != ''    )  
      // && this.form.get('date').value != ''
      {
        // console.log(this.form.get('date').value)
      if(this.form.get('date').value != ''){
        let date = new Date(this.form.get('date').value).toLocaleString();
        this.form.get('date').patchValue(date)
      }
   

      this.global.swalLoading('Adding Organization');
      this.HrisApi.postEmployeeOrganization(this.data.selectedID, {
        "position": this.form.get('position').value,
        "organization": this.form.get('org').value,
        "expirationDate": this.form.get('date').value
      })
        // this.http.post(this.global.api + 'Employee/Organization/' + , this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalClose();
          this.global.swalAlert('Professional membership to organization created successfully.', "", 'success');
          this.dialogRef.close({ result: "Adding Success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
    }
  }
}
