import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-add-we',
  templateUrl: './add-we.component.html',
  styleUrls: ['./add-we.component.css']
})
export class AddWeComponent implements OnInit {

  position
  company
  sdate
  edate
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddWeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private fb: FormBuilder,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private httpC: HttpClient,
  ) { }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      position: ['', Validators.required],
      company: ['', Validators.required],
      sdate: ['', Validators.required],
      edate: ['', Validators.required],

    });
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    this.form.markAllAsTouched();
    if (this.form.get('company').value != '' && this.form.get('position').value != '' &&
      this.form.get('sdate').value != '' && this.form.get('edate').value != '') {

      let sdate = new Date(this.form.get('sdate').value).toLocaleString();
      let edate = new Date(this.form.get('edate').value).toLocaleString();
      // console.log(sdate)
      // console.log(edate)
      if (this.form.get('sdate').value > this.form.get('edate').value) {
        this.global.swalAlert('Invalid date range', "", 'error');
      } else {
        this.global.swalLoading('Adding Work Experience');
        this.http.post(this.global.api + 'Employee/WorkExperience/' + this.data.selectedID, {
          "position": this.form.get('position').value,
          "company": this.form.get('company').value,
          "sdate": sdate,
          "edate": edate,
        }, this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalClose();
            this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "Adding Success" });
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
      }
    }
  }
}
