import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';



@Component({
  selector: 'app-update-we',
  templateUrl: './update-we.component.html',
  styleUrls: ['./update-we.component.css']
})
export class UpdateWeComponent implements OnInit {

  position
  company
  sdate
  edate
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<UpdateWeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private fb: FormBuilder,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private httpC: HttpClient,
  ) { }

  ngOnInit() {
    this.form = this.FormBuilder.group({
      position: ['', Validators.required],
      company: ['', Validators.required],
      sdate: ['', Validators.required],
      edate: ['', Validators.required],

    });

    this.position = this.data.selectedData.position
    this.company = this.data.selectedData.companyname

    var sd = new Date(this.data.selectedData.startdate);
    var ed = new Date(this.data.selectedData.enddate);
    this.sdate = sd;
    this.edate = ed;

    this.form.get('position').patchValue(this.position);
    this.form.get('company').patchValue(this.company);
    this.form.get('sdate').patchValue(this.sdate);
    this.form.get('edate').patchValue(this.edate);


  }


  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    this.form.markAllAsTouched();
    if (this.form.get('company').value != '' && this.form.get('position').value != '' &&
    this.form.get('sdate').value != '' && this.form.get('edate').value != '') {
      if (this.form.get('sdate').value > this.form.get('edate').value) {
        this.global.swalAlert('Invalid date range', "", 'error');
      } else {

        let sdate = new Date(this.form.get('sdate').value).toLocaleString();
        let edate = new Date(this.form.get('edate').value).toLocaleString();
        this.global.swalLoading('Updating Work Experience');
        this.HrisApi.putEmployeeWorkExperience(this.data.selectedWID, {
          "position": this.form.get('position').value,
          "company": this.form.get('company').value,
          "sdate": sdate,
          "edate": edate,
        })
          // this.http.put(this.global.api + 'Employee/WorkExperience/' + , this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "Update success" });
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
    

      }
    }
  
  }
}
