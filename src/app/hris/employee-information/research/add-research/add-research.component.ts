import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { HrisApiService } from '../../../../hris-api.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-add-research',
  templateUrl: './add-research.component.html',
  styleUrls: ['./add-research.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AddResearchComponent implements OnInit {

  rtitle: any = '';
  rmarks: any = '';
  cyear: any = '';
  date: any = '';
  resTypeArr = '';
  RID = '';
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddResearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private http: Http
  ) {
    this.HrisApi.getHRISMaintenanceResearchTypes()
      // this.http.get(this.global.api+'HRISMaintenance/ResearchTypes',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        //console.table(res.data)
        this.resTypeArr = res.data;
      }, Error => {
        this.global.swalAlertError();
      });
  }

  ngOnInit() {
    this.form = this.FormBuilder.group({
      rtitle: ['', Validators.required],
      date: ['', Validators.required],
      cyear: ['', Validators.required],
      // rmarks: ['', Validators.required],
      RID: ['', Validators.required],
    });
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });

  }

  save(): void {
    //console.log(this.data.selectedID);
    this.form.markAllAsTouched();
    if (this.form.get('rtitle').value != ''  &&
      this.form.get('cyear').value != '' && this.form.get('RID').value != '' &&
      this.form.get('date').value != '') {

      const copyYear = this.form.get('cyear').value.toString();

      if (copyYear.length <= 4) {
        let date = new Date(this.form.get('date').value).toLocaleString();
        this.global.swalLoading('Adding Research');
        this.HrisApi.postEmployeeResearch(this.data.selectedID, {
          "title": this.form.get('rtitle').value,
          "dateCompleted": date,
          "remarks": this.rmarks,
          "copyrightYear": this.form.get('cyear').value.toString(),
          "researchTypeID": this.form.get('RID').value,
        })
          // this.http.post(this.global.api + 'Employee/InsertResearch/' +, this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "Adding Success" });
          }, Error => {
            this.global.swalAlertError();
            // console.log(Error)
          });
        this.global.swalClose()
      } else {
        this.global.swalAlert('Copyright Year must be 4 digits', "", 'warning');
      }
    }
  }



}
