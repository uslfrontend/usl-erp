import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-update-research',
  templateUrl: './update-research.component.html',
  styleUrls: ['./update-research.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateResearchComponent implements OnInit {

  rtitle
  rmarks
  cyear
  date
  idn
  statusid

  RID = ''
  resTypeArr = ''

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<UpdateResearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private HrisApi: HrisApiService,
    private FormBuilder: FormBuilder,
    private http: Http
  ) {
    this.http.get(this.global.api + 'HRISMaintenance/ResearchTypes', this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        //console.table(res.data)
        this.resTypeArr = res.data;
      }, Error => {
        this.global.swalAlertError();
      });
  }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      rtitle: ['', Validators.required],
      date: ['', Validators.required],
      cyear: ['', Validators.required],
      rmarks: ['', Validators.required],
      RID: ['', Validators.required],
    });

    this.idn = this.data.idnum
    this.rtitle = this.data.selectedData.title
    this.rmarks = this.data.selectedData.remarks
    this.cyear = this.data.selectedData.copyrightYear

    if (this.data.selectedData.statusID == 1 || this.data.selectedData.statusID == null) {
      this.statusid = 1;

    } else
      this.statusid = this.data.selectedData.statusID
    var sdate = new Date(this.data.selectedData.dateCompleted);
    this.date = sdate;
    this.RID = this.data.selectedData.researchTypeID.toString();

    
    this.form.get('rtitle').patchValue(this.rtitle);
    this.form.get('rmarks').patchValue(this.rmarks);
    this.form.get('cyear').patchValue(this.cyear);
    this.form.get('date').patchValue(this.date);
    this.form.get('RID').patchValue(this.RID);


  }

  checked() {
    if (this.statusid == 0) this.statusid = 1
    else this.statusid = 0

    console.log(this.statusid);
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    if (this.form.get('rtitle').value != '' && this.form.get('rmarks').value != '' &&
    this.form.get('cyear').value != '' && this.form.get('RID').value != '' &&
    this.form.get('date').value != '') {
      const copyYear = this.form.get('cyear').value.toString();

      if (copyYear.length <= 4) {
        let date = new Date(this.form.get('date').value).toLocaleString();
        this.global.swalLoading('Updating Research');
        this.HrisApi.putEmployeeReasearch(this.data.selectedRID, {
          "title": this.form.get('rtitle').value,
          "dateCompleted": date,
          "remarks": this.form.get('rmarks').value,
          "copyrightYear": this.form.get('cyear').value,
          "researchTypeID": this.form.get('RID').value,
          "statusID": this.statusid,
          "approvedBy": this.global.requestid(),
      
        })
          // this.http.put(this.global.api + 'Employee/UpdateResearch/' + , this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "Update success" });
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
      }else{
        this.global.swalAlert('Year must be 4 digits', "", 'warning');
      }
    }
  }
}
