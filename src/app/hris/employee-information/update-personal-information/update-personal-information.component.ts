import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../global.service';
import { AddressLookupComponent } from './../../../academic/student-information/address-lookup/address-lookup.component';
import { HrisApiService } from '../../../hris-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-personal-information',
  templateUrl: './update-personal-information.component.html',
  styleUrls: ['./update-personal-information.component.scss']
})
export class UpdatePersonalInformationComponent implements OnInit {
  array;
  civilS: any = '';
  permPSGC: any = '';
  homeaddress: any = '';
  currPSGC: any = '';
  curraddress: any = '';
  emailAdd: any = '';
  telno: any = '';
  telno2: any = '';
  street1: any = '';
  street2: any = '';
  tin: any = '';
  sssNo: any = '';
  philHealthNo: any = '';
  pagIbigNo: any = '';
  civilstatus: any = '';
  spouse: any = '';
  dateofmarriage: any = '';
  nameOfChurch: any = '';
  occupation: any = '';
  dateofbirth: any = '';
  religion: any = '';
  nationality: any = '';
  SID: any = '';
  idpicture: any = '';
  signature: any = '';
  form: FormGroup;
  CedNo = '';
  PlaceIssued = '';
  DateIssued: any = '';
  PTRNo = '';
  checker = true
  constructor(public dialogRef: MatDialogRef<UpdatePersonalInformationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private FormBuilder: FormBuilder,
    private http: Http,
    private HrisAPi: HrisApiService) {

  }

  ngOnInit() {


    this.form = this.FormBuilder.group({
      dateofmarriage: ['', Validators.required],
      spouse: ['', Validators.required],
      nameOfChurch: ['', Validators.required],

    });

    // let mdate = "";
    // // let bdate = new Date(this.data.selectedData.dateofbirth).toLocaleString();
    // if(this.data.selectedData.dateofmarriage.length>0)
    //   mdate = new Date(this.data.selectedData.dateofmarriage).toLocaleString();
    // console.log(mdate)
    this.array = this.data.selectedData;
    this.idpicture = this.data.selectedData.idPicture;
    this.signature = this.data.selectedData.signature;
    this.homeaddress = this.data.selectedData.permanentaddress;
    this.curraddress = this.data.selectedData.currentaddress;
    this.emailAdd = this.data.selectedData.emailAdd
    this.telno = this.data.selectedData.telno
    this.telno2 = this.data.selectedData.mobileno
    this.street1 = this.data.selectedData.street1
    this.street2 = this.data.selectedData.street2
    this.tin = this.data.selectedData.tin
    this.sssNo = this.data.selectedData.sssNo
    this.philHealthNo = this.data.selectedData.philHealthNo
    this.pagIbigNo = this.data.selectedData.pagIbigNo
    this.civilstatus = this.data.selectedData.civilstatus
    // console.log(this.civilstatus)
    if (this.civilstatus == 'M') {
      // this.checker = false
    }
    this.spouse = this.data.selectedData.spouse

    var a = new Date(this.data.selectedData.dateofmarriage);
    this.dateofmarriage = a
    if (this.spouse != null) {
      // this.checker = false
    }
    // console.log('checker',this.checker)
    this.nameOfChurch = this.data.selectedData.nameOfChurch
    this.occupation = this.data.selectedData.occupation
    this.dateofbirth = this.data.selectedData.dateofbirth
    this.religion = this.data.selectedData.religion
    this.nationality = this.data.selectedData.nationality
    this.SID = this.data.selectedID
    this.currPSGC = this.data.selectedData.psgC2
    this.permPSGC = this.data.selectedData.psgC1

    this.CedNo = this.data.selectedData.cedulaNo;
    this.PlaceIssued = this.data.selectedData.cedula_PlacedIssued;
    this.DateIssued = this.data.selectedData.cedula_DateIssued;
    this.PTRNo = this.data.selectedData.ptrOtrNo;

    this.form.get('dateofmarriage').patchValue(this.dateofmarriage);
    this.form.get('spouse').patchValue(this.spouse);
    this.form.get('nameOfChurch').patchValue(this.nameOfChurch);


  }
  onNoClick(): void {
    this.dialogRef.close(0);
  }
  save(): void {

    this.dialogRef.close({ result: 1, data: this.array });
  }

  openDialog(lookup): void {
    const dialogRef = this.dialog.open(AddressLookupComponent, {
      width: '500px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        if (lookup == 1) {
          this.permPSGC = result.data;
          this.homeaddress = result.result;
          //console.log(result.data+"--"+result.result);
        } else {
          this.currPSGC = result.data;
          this.curraddress = result.result;

        }
        //console.log(result)
      }
    });

  }

  csType() {
    // console.log(this.civilstatus)
    if (this.civilstatus == "S") {
      this.form.get('spouse').patchValue('');
      this.form.get('dateofmarriage').patchValue('');
      this.form.get('nameOfChurch').patchValue('');
      this.checker = true
    } else if (this.civilstatus == 'W') {
      this.nameOfChurch = this.form.get('nameOfChurch').value

    } else if (this.civilstatus == 'M') {
      this.form.get('nameOfChurch').patchValue(this.nameOfChurch);

    }
  }

  checkCivilStatus() {
    if (this.civilstatus == "S")
      return false
    else if (this.civilstatus == "M" || this.civilstatus == "W")
      return true
    else
      return false
  }

  savePersonalInfo() {

    // console.log(this.checkCivilStatus)
    // console.log(this.form.get('dateofmarriage').value)

    if (this.civilstatus == 'M') {
 

      if (this.form.get('dateofmarriage').value != '' && this.form.get('spouse').value != '' &&
        this.form.get('nameOfChurch').value != '' && this.form.get('nameOfChurch').value != null
        && this.form.get('nameOfChurch').value != undefined) {

        let mdate = "";
        if (this.checkCivilStatus() == true) {
          mdate = this.form.get('dateofmarriage').value

        } else {

          this.form.get('nameOfChurch').patchValue('');
          this.form.get('spouse').patchValue('');
          this.form.get('dateofmarriage').patchValue('');

        }

        let bdate = new Date(this.dateofbirth).toLocaleString();
        const dateofmarriage = this.form.get('dateofmarriage').value

        if (dateofmarriage.length > 0)

          mdate = new Date(this.form.get('dateofmarriage').value).toLocaleString();
        this.form.get('dateofmarriage').patchValue(mdate);
        // console.log(this.form.get('dateofmarriage').value)

        // this.global.swalLoading('Updating Personal Info');
        this.HrisAPi.putEmployeePersonalInfo(this.SID, {
          "IDPicture": this.idpicture,
          "Signature": this.signature,
          "sss": this.sssNo,
          "tin": this.tin,
          "pagibig": this.pagIbigNo,
          "philhealth": this.philHealthNo,
          "dob": bdate,
          "civilstatus": this.civilstatus,
          "spouse": this.form.get('spouse').value,
          "occupation": this.occupation,
          "church": this.form.get('nameOfChurch').value,
          "dom": this.form.get('dateofmarriage').value,
          "nationality": this.nationality,
          "religion": this.religion,
          "telno1": this.telno,
          "telno2": this.telno2,
          "street1": this.street1,
          "psgc2": this.currPSGC,
          "street2": this.street2,
          "psgc1": this.permPSGC,
          "emailAdd": this.emailAdd,
          "CedulaNo": this.CedNo,
          "PrtOtrNo": this.PTRNo,
          "CedulaPlacedIssued": this.PlaceIssued,
          "CedulaDateIssued": this.DateIssued,
        })
          // this.http.put(this.global.api + 'Employee/PersonalInfo/' , this.global.option)
          .map(response => response.json())
          .subscribe(res => {

            // console.log(res.message)
            // if (res.message == 'Spouse, Date of Marriage, and Name of Church is required.') {
            //   this.global.swalAlert(res.message, "", 'error');

            // } else 
            if (res.message == 'Personal information updated successfully.') {
              // this.global.swalAlert(res.message, "", 'success');
              this.dialogRef.close({ result: "update success" });
            }
            // else {
            //   this.global.swalAlert('Date of Marriage is required.', "", 'error');
            // }
          }, Error => {
            this.global.swalAlertError();
            // console.log(Error)
          });

      } else {
        this.form.markAllAsTouched();
      }
    } else if (this.civilstatus == 'W') {

      if (this.form.get('dateofmarriage').value == '' &&
        this.form.get('spouse').value == '') {
        this.form.markAllAsTouched();
      } else {

        let mdate = "";
        if (this.checkCivilStatus() == true) {
          mdate = this.form.get('dateofmarriage').value


        } else {

          this.nameOfChurch = ''
          this.form.get('spouse').patchValue('');
          this.form.get('dateofmarriage').patchValue('');
        }

        let bdate = new Date(this.dateofbirth).toLocaleString();
        const dateofmarriage = this.form.get('dateofmarriage').value

        if (dateofmarriage.length > 0)

          mdate = new Date(this.form.get('dateofmarriage').value).toLocaleString();
        this.form.get('dateofmarriage').patchValue(mdate);
        // console.log(this.form.get('dateofmarriage').value)

        // this.global.swalLoading('Updating Personal Info');
        this.HrisAPi.putEmployeePersonalInfo(this.SID, {
          "IDPicture": this.idpicture,
          "Signature": this.signature,
          "sss": this.sssNo,
          "tin": this.tin,
          "pagibig": this.pagIbigNo,
          "philhealth": this.philHealthNo,
          "dob": bdate,
          "civilstatus": this.civilstatus,
          "spouse": this.form.get('spouse').value,
          "occupation": this.occupation,
          "church": this.nameOfChurch,
          "dom": this.form.get('dateofmarriage').value,
          "nationality": this.nationality,
          "religion": this.religion,
          "telno1": this.telno,
          "telno2": this.telno2,
          "street1": this.street1,
          "psgc2": this.currPSGC,
          "street2": this.street2,
          "psgc1": this.permPSGC,
          "emailAdd": this.emailAdd,
          "CedulaNo": this.CedNo,
          "PrtOtrNo": this.PTRNo,
          "CedulaPlacedIssued": this.PlaceIssued,
          "CedulaDateIssued": this.DateIssued,
        })
          // this.http.put(this.global.api + 'Employee/PersonalInfo/' , this.global.option)
          .map(response => response.json())
          .subscribe(res => {

            // console.log(res.message)
            // if (res.message == 'Spouse, Date of Marriage, and Name of Church is required.') {
            //   this.global.swalAlert(res.message, "", 'error');

            // } else 
            if (res.message == 'Personal information updated successfully.') {
              // this.global.swalAlert(res.message, "", 'success');
              this.dialogRef.close({ result: "update success" });
            }
            // else {
            //   this.global.swalAlert('Date of Marriage is required.', "", 'error');
            // }
          }, Error => {
            this.global.swalAlertError();
            // console.log(Error)
          });


      }

    } else if (this.civilstatus == 'S') {
      let mdate = "";
      if (this.checkCivilStatus() == true) {
        mdate = this.form.get('dateofmarriage').value
      } else {
        this.nameOfChurch = ''
        this.form.get('spouse').patchValue('');
        this.form.get('dateofmarriage').patchValue('');
      }

      let bdate = new Date(this.dateofbirth).toLocaleString();
      const dateofmarriage = this.form.get('dateofmarriage').value

      if (dateofmarriage.length > 0)

        mdate = new Date(this.form.get('dateofmarriage').value).toLocaleString();
      this.form.get('dateofmarriage').patchValue(mdate);
      // console.log(this.form.get('dateofmarriage').value)

      // this.global.swalLoading('Updating Personal Info');
      this.HrisAPi.putEmployeePersonalInfo(this.SID, {
        "IDPicture": this.idpicture,
        "Signature": this.signature,
        "sss": this.sssNo,
        "tin": this.tin,
        "pagibig": this.pagIbigNo,
        "philhealth": this.philHealthNo,
        "dob": bdate,
        "civilstatus": this.civilstatus,
        "spouse": this.form.get('spouse').value,
        "occupation": this.occupation,
        "church": this.nameOfChurch,
        "dom": this.form.get('dateofmarriage').value,
        "nationality": this.nationality,
        "religion": this.religion,
        "telno1": this.telno,
        "telno2": this.telno2,
        "street1": this.street1,
        "psgc2": this.currPSGC,
        "street2": this.street2,
        "psgc1": this.permPSGC,
        "emailAdd": this.emailAdd,
        "CedulaNo": this.CedNo,
        "PrtOtrNo": this.PTRNo,
        "CedulaPlacedIssued": this.PlaceIssued,
        "CedulaDateIssued": this.DateIssued,
      })
        // this.http.put(this.global.api + 'Employee/PersonalInfo/' , this.global.option)
        .map(response => response.json())
        .subscribe(res => {

          // console.log(res.message)
          // if (res.message == 'Spouse, Date of Marriage, and Name of Church is required.') {
          //   this.global.swalAlert(res.message, "", 'error');

          // } else 
          if (res.message == 'Personal information updated successfully.') {
            // this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "update success" });
          }
          // else {
          //   this.global.swalAlert('Date of Marriage is required.', "", 'error');
          // }
        }, Error => {
          this.global.swalAlertError();
          // console.log(Error)
        });
    }


  }
}
