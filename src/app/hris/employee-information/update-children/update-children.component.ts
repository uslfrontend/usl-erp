import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { HrisApiService } from '../../../hris-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-update-children',
  templateUrl: './update-children.component.html',
  styleUrls: ['./update-children.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateChildrenComponent implements OnInit {


  cfname: any = '';
  cmname: any = '';
  clname: any = '';
  cdoBirth: any = '';
  SCID: any = '';
  cusltid: any = '';
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<UpdateChildrenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private HrisApi: HrisApiService,
    private FormBuilder: FormBuilder,
    private http: Http) { }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      cfname: ['', Validators.required],
      clname: ['', Validators.required],
      cdoBirth: ['', Validators.required],


    });

    //console.table(this.data.selectedData);
    this.cfname = this.data.selectedData.firstName;
    var bdate = new Date(this.data.selectedData.dateOfBirth);
    this.cmname = this.data.selectedData.middleName;
    this.clname = this.data.selectedData.lastName;
    this.cdoBirth = bdate;
    this.SCID = this.data.selectedCID;
    this.cusltid = this.data.selectedData.usltid;


    this.form.get('cfname').patchValue(this.cfname);
    this.form.get('clname').patchValue(this.clname);
    this.form.get('cdoBirth').patchValue(this.cdoBirth);

  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  click() {
    console.log();
  }

  save(): void {
    this.form.markAllAsTouched();
    if(this.form.get('cfname').value != '' && this.form.get('clname').value !='' && this.form.get('cdoBirth').value != ''){
      let cdoBirth = new Date(this.cdoBirth).toLocaleString();
      this.form.get('cdoBirth').patchValue(cdoBirth);
      this.global.swalLoading('Updating Child Info');
      this.HrisApi.putEmployeeChild(this.SCID, {
        "firstname": this.form.get('cfname').value,
          "middlename": this.cmname,
          "lastname": this.form.get('clname').value,
          "dateofbirth": this.form.get('cdoBirth').value,
          "usltid": this.cusltid,
      })
        // this.http.put(this.global.api+'Employee/Child/'+,this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalAlert(res.message, "", 'success');
          this.dialogRef.close({ result: "Update success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
    }
    
  }
}

