import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GlobalService } from '../../../global.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewChild, ElementRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { HrisemployeeLookupComponent } from '../../hris-lookup/hrisemployee-lookup/hrisemployee-lookup.component';
import { AppointmentsComponent } from '../employee-records/appointments/appointments.component';
import { ContractsComponent } from '../employee-records/contracts/contracts.component';
import { RankRecordsComponent } from '../employee-records/rank-records/rank-records.component';
import { UpdateNameComponent } from '../update-name/update-name.component';
import { HrisApiService } from '../../../hris-api.service';

@Component({
  selector: 'app-employment-records',
  templateUrl: './employment-records.component.html',
  styleUrls: ['./employment-records.component.css']
})

export class EmploymentRecordsComponent implements OnInit {
  image: any = 'assets/noimage.jpg';
  signature: any = 'assets/nosignature.jpg';
  name: any = '';
  position: any = '';
  idnumber: any = '';
  id: any = '';
  dtridnum: any = '';
  

  appointmentArr;
  contractArr;
  rankRecordsArr;

  empDeptID
  employeeDeptIDAr: string[] = [];

  showContent: boolean = false;

  imageTestData

  fname
  mname
  lname
  suffix
  gender
  cstatus
  tempDB
  personNameInfo

  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, private global: GlobalService, private http: Http, private HrisApi: HrisApiService) {

 
  }

  WorkDisplayedColumns = ['position', 'companyname', 'startdate', 'enddate'];
  @ViewChild(MatSort, { static: false }) sort4: MatSort;
  @ViewChild('paginator4', { static: false }) paginator4: MatPaginator;

  ngOnInit() {
    if (this.global.hrisId != '') {
      this.id = this.global.hrisId
      this.keyDownFunction('onoutfocus')
    }
    //console.log(this.global.requestid());
  }


  ngAfterViewInit() {

  }

  employeelookup(): void {

    const dialogRef = this.dialog.open(HrisemployeeLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });

  }

  result
  keyDownFunction(event) {

    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {

      this.global.hrisId = this.id
      //console.log("1st")
      this.employeeDeptIDAr.length = 0;
      if (this.id != '') {
        if (this.id == this.global.requestid()) {
          this.proceedProc();
        } else {
          if (this.global.checkrole('HRMO')) {
            this.proceedProc();
          } else {
            this.checkAppointment();
          }

        }
      }
      else {
        this.showContent = false;
      }
      // code...
    }
  }

  proceedProc() {
    this.global.swalLoading('Loading Person Information');
    this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
      //  console.log(res);
      this.global.swalClose();
      if (res.message != "IDNumber does not exist.") {
        this.showContent = true;
        this.name = res.data[0].fullname;
        this.position = res.data[0].position;
        this.idnumber = res.data[0].idnumber;
        this.dtridnum = "DTR ID#: " + res.data[0].dtrid;
 
        this.getPersonalInfo();
     
        this.getAppointment();
        this.getContract();
        this.getRankRecords();

      } else {
        //console.log('1111')
        this.clear();
        this.global.swalAlert("Employee not found", '', 'warning');
      }
    }, Error => {
      this.clear();
      this.global.swalAlertError();
    });
  }



  async nameUpdate() {
    try {
      const response = await this.HrisApi.getEmployeePerson(this.id).toPromise();
      const res = response.json();
      this.personNameInfo = res.data;
      this.fname = res.data.firstName
      this.mname = res.data.middleName
      this.lname = res.data.lastName
      this.suffix = res.data.suffixName
      this.gender = res.data.gender
      this.cstatus = res.data.civilStatus
      this.tempDB = res.data.dateOfBirth

      // console.log(this.cstatus)
      // console.log(this.tempDB)

      const dialogRef = this.dialog.open(UpdateNameComponent, {
        data: {
          id: this.id,
          personNameInfo: this.personNameInfo,
          name: this.name,
          fname: this.fname,
          mname: this.mname,
          lname: this.lname,
          suffix: this.suffix,
          gender: this.gender,
          cstatus: this.cstatus,
          tempDB: this.tempDB
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        // console.log(Dialog result: ${result});
        this.updateEmployeeName()

      });
    } catch (error) {
      this.global.swalAlertError();
    }
  }

  updateEmployeeName() {
    this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
      // console.log(res);
      this.global.swalClose();
      if (res.message != "IDNumber does not exist.") {
        this.showContent = true;
        this.name = res.data[0].fullname;
        this.position = res.data[0].position;
        this.idnumber = res.data[0].idnumber;
        this.dtridnum = "DTR ID#: " + res.data[0].dtrid;
      } else {
        //console.log('1111')
        this.clear();
        this.global.swalAlert("Employee not found", '', 'warning');
      }

    }, Error => {
      this.clear();
      this.global.swalAlertError();
    });
  }

  checkAppointment() {
    this.HrisApi.getEmployeeAppointment(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentArr = res.data;
        //console.log("VIEW DOMAINS UNDER THE USER:");
        //console.log(this.global.viewdomain);

        //console.log("checking of appointment:");
        // console.log(this.appointmentArr);
        // console.log(res.data[0]);
        // console.log(res.data);
        if (res.data) {
          for (var x = 0; x < res.data.length; x++) {
            //if(res.data[x].classificationID == 2 &&res.data[x].active == 1) //used when getting only the fulltime employee's
            if (res.data[x].active == 1) {
              //this.empDeptID = res.data[x].departmentID.toString();//used when getting the first active appointment
              //x=res.data.length; //used when getting the first active appointment

              this.employeeDeptIDAr.push(res.data[x].departmentID.toString());


            }
            else {
              this.global.swalAlert('', 'Employee is a part time and has no active appointment', 'error');
            }
          }
        }
        else {
          this.global.swalAlert('', 'Employee has no active appointment', 'error');
        }

        // console.log(this.employeeDeptIDAr.length)

        if (this.employeeDeptIDAr.length == 1) {
          //////THIS DECISION PROCESS ONLY FIRES UP WHEN THERE'S ONLY ONE APPOINTMENT FOR A CERTAIN EMPLOYEE
          this.empDeptID = this.employeeDeptIDAr[0].toString();
          //console.log(this.empDeptID);
          this.validateAppointment(this.global.checkdomain(this.empDeptID));

        } else if (this.employeeDeptIDAr.length == 0) {
          //  console.log("employee does not have assigned appointment yet")
        }
        else {
          //////THIS DECISION PROCESS ONLY FIRES UP WHEN THERE'S MORE THAN ONE APPOINTMENT FOR A CERTAIN EMPLOYEE
          for (var i = this.employeeDeptIDAr.length - 1; i >= 0; i--) {
            if (this.global.checkdomain(this.employeeDeptIDAr[i]) == true) {
              this.result = true;
            }
            else {
              this.result = false;

            }
          }
          this.validateAppointment(this.result);
        }


        //console.log(this.employeeDeptIDAr)

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  validateAppointment(param) {
    if (param == true) {
      this.proceedProc();
    }
    else {
      this.clear();
      this.global.swalAlert('', 'Employee does not belong to your department/office', 'error');
    }
  }

  updatenamevar = false
  updatename() {
    if (this.updatenamevar == true) {

      this.updatenamevar = false
      this.proceedProc();
    }
    else {
      this.global.swalLoading('Loading item resources')
      this.HrisApi.getEmployeePerson(this.id)
        .map(response => response.json())
        .subscribe(res => {
          this.personNameInfo = res.data;
          this.fname = res.data.firstName
          this.mname = res.data.middleName
          this.lname = res.data.lastName
          this.suffix = res.data.suffixName
          this.gender = res.data.gender
          this.cstatus = res.data.civilStatus
          this.tempDB = res.data.dateOfBirth
          this.global.swalClose();

        }, Error => {
          //console.log(Error);
          this.global.swalAlertError();
          //console.log(Error)
        });
      this.updatenamevar = true
    }
  }

  updatenameapi() {

    var error = '';
    if (this.fname == '') {
      error = error + '*First name must not be blank<br>';
    }
    if (this.lname == '') {
      error = error + '*Last name must not be blank<br>';
    }


    if (error == '') {
      var x = ''
      let date = new Date(this.tempDB).toLocaleString();
      this.global.swalLoading('Updating...');
      this.HrisApi.putEmployeePerson(this.id,
        {
          'FirstName': this.fname,
          'MiddleName': this.mname,
          'LastName': this.lname,
          'SuffixName': this.suffix,
          'Gender': this.gender,
          'CivilStatus': this.cstatus,
          'DateOfBirth': this.tempDB,
        })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
          this.updatenamevar = false
          this.proceedProc();
        }, Error => {
          this.global.swalAlertError();
        });
    } else {
      this.global.swalAlert('The Following error has Occured', error, 'error');
    }
  }

  getAppointment() {
    //  this.global.swalLoading('Loading item resources')

    this.HrisApi.getEmployeeAppointment(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentArr = res.data;
        //  console.log(this.appointmentArr)
        //  this.global.swalClose();

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getContract() {
    this.global.swalLoading('Loading item resources')
    this.HrisApi.getEmployeeContract(this.id)
      //  this.http.get(this.global.api+'Employee/Contract/'+this.id,this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.contractArr = res.data;
        //  console.log(this.contractArr)
        this.global.swalClose();

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getRankRecords() {
    //  this.global.swalLoading('Loading item resources')
    this.HrisApi.getEmployeeRank(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.rankRecordsArr = res.data;
        //  this.global.swalClose();

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }


  getPersonalInfo() {
    this.HrisApi.getEmployeePersonalInformation(this.id)
      .map(response => response.json())
      .subscribe(res => {
        // this.pinfo = res.data;
        this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
        this.imageTestData = res.data.idPicture;
        this.signature = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.signature);
     

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  ////////////////EMPLOYEE RECORDS////////////////////////////////////////////////////////////////////////////////////////////

  addAppointmentOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AppointmentsComponent, {

        width: '600px', disableClose: false,
        data: { selectedID: this.idnumber, type: "add" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          console.log(result.result)

          if (result.result == 'Adding Success') {

            this.getAppointment();
          }
        }
      });
    }
  }

  updateAppointmentOpenDialog(a) {

    const dialogRef = this.dialog.open(AppointmentsComponent, {
      width: '600px', disableClose: false, data: {
        selectedID: this.idnumber,
        selectedData: a,
        type: "update",
      },  autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateEmployeeName()
      if (result != undefined) {
        //  console.log(result.result)

        if (result.result == 'Update success') {
          this.getAppointment();
        }
      }
    });
  }

  deleteAppointment(selectedid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeAppointment(selectedid)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert('Appointment removed successfully.', "", 'success');
              this.getAppointment();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }

  addContractOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(ContractsComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber, type: "add" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          this.getContract();
        }
      });
    }

  }

  updateContractOpenDialog(c) {
    const dialogRef = this.dialog.open(ContractsComponent, {
      width: '600px', disableClose: false, data: { selectedID: this.idnumber, selectedData: c, type: "update" },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getContract();
      }
    });
  }

  deleteContract(selectedid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeContract(selectedid)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert(res.message, "", 'success');
              this.getContract();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }

  addRankRecordOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(RankRecordsComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber, type: "add" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          this.getRankRecords();
        }
      });
    }

  }

  updateRankRecordOpenDialog(c) {
    const dialogRef = this.dialog.open(RankRecordsComponent, {
      width: '600px', disableClose: false, data: { selectedID: this.idnumber, selectedData: c, type: "update" },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getRankRecords();
      }
    });
  }

  deleteRankRecord(selectedid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeRank(selectedid, selectedid)
          // this.http.delete(this.global.api+'Employee/Rank/'+selectedid+'?evaluationid='+selectedid,this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(selectedid)

            if (res.message != undefined) {
              this.global.swalAlert('Rank removed successfully.', "", 'success');
              this.getRankRecords();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  clear() {
    this.image = 'assets/noimage.jpg';

    this.signature = 'assets/nosignature.jpg';
    this.name = '';
    this.position = '';
    this.id = '';
    this.idnumber = '';
    this.dtridnum = '';

  }

  tabbing = 0;
  getindex(tab) {
    this.tabbing = tab.index;
  }
}

export interface PeriodicElement {
  lName: any;
  fName: any;
  mName: any;
  dBirth: any;
  usltIDNum: any;
}
