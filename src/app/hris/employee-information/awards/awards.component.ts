import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../global.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../hris-api.service';


@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.css']
})
export class AwardsComponent implements OnInit {

  type
  EmpID
  Venue
  EventTitle
  AwardReceived
  DateAwarded
  AwardingBody

  statusid = 0;
  targetUpdateID

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AwardsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService) { }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      EventTitle: ['', Validators.required],
      AwardReceived: ['', Validators.required],
      AwardingBody: ['', Validators.required],
      Venue: ['', Validators.required],
      DateAwarded: ['', Validators.required],
    });

    this.type = this.data.type;
    this.EmpID = this.data.selectedID;
    this.targetUpdateID = this.data.selectedEID
    if (this.type == "Update") {
      if (this.data.selectedData.statusId == 1 || this.data.selectedData.statusId == null) {
        this.statusid = 1;
      } else
        this.statusid = this.data.selectedData.statusId

      this.EventTitle = this.data.selectedData.eventTitle;
      this.AwardReceived = this.data.selectedData.awardReceived;
      this.AwardingBody = this.data.selectedData.awardingBody;
      var date = new Date(this.data.selectedData.dateAwarded);
      this.DateAwarded = date;
      this.Venue = this.data.selectedData.venue;

      this.form.get('EventTitle').patchValue(this.EventTitle);
      this.form.get('AwardReceived').patchValue(this.AwardReceived);
      this.form.get('AwardingBody').patchValue(this.AwardingBody);
      this.form.get('Venue').patchValue(this.Venue);
      this.form.get('DateAwarded').patchValue(this.DateAwarded);

    }
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  checked() {
    if (this.statusid == 0) this.statusid = 1
    else this.statusid = 0
  }

  save(): void {
    this.form.markAllAsTouched();
    if (this.form.get('EventTitle').value != '' && this.form.get('AwardReceived').value != '' && this.form.get('AwardingBody').value != '' &&
    this.form.get('Venue').value != '' && this.form.get('DateAwarded').value != '') {
      let date = new Date(this.form.get('DateAwarded').value).toLocaleString();
      if (this.type == "Update") {
        this.global.swalLoading('');
        this.HrisApi.putEmployeeAward(this.targetUpdateID, {
          "EventTitle": this.form.get('EventTitle').value,
          "AwardReceived": this.form.get('AwardReceived').value,
          "AwardingBody": this.form.get('AwardingBody').value,
          "Venue": this.form.get('Venue').value,
          "DateAwarded": date,
          "statusId": this.statusid,
        }).map(response => response.json()).subscribe(res => {
          //console.log(res)
          this.global.swalClose();
          this.global.swalAlert("Award updated successfully.", "", 'success');
          this.dialogRef.close({ result: "Update success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
      } else {
        // console.log(this.form.get('EventTitle').value, this.form.get('AwardReceived').value,this.form.get('AwardingBody').value,
        // this.form.get('Venue').value, this.form.get('DateAwarded').value,)
        this.HrisApi.postEmployeeAward({
          "EventTitle": this.form.get('EventTitle').value,
          "AwardReceived": this.form.get('AwardReceived').value,
          "AwardingBody": this.form.get('AwardingBody').value,
          "Venue": this.form.get('Venue').value,
          "DateAwarded": date,
        })
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalClose();
            this.global.swalAlert("Award created successfully.", "", 'success');
            this.dialogRef.close({ result: "Adding Success" });
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
      }
    }

  }
}
