import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-update-sa-t',
  templateUrl: './update-sa-t.component.html',
  styleUrls: ['./update-sa-t.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdateSaTComponent implements OnInit {

  progTypeArr: any = [];
  ptype
  ptypeid

  idn
  seminardesc
  companyname
  venue
  sdate
  edate
  STID
  appBy

  statusid

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<UpdateSaTComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private httpC: HttpClient,
  ) {
    this.STID = this.data.selectedData.trainingTypeID
    this.http.get(this.global.api + 'HRISMaintenance/TraningType', this.global.option)
      .map(response => response.json())
      .subscribe(res => {

        this.progTypeArr = res.data;
      }, Error => {
        this.global.swalAlertError();
      });
  }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      seminardesc: ['', Validators.required],
      companyname: ['', Validators.required],
      sdate: ['', Validators.required],
      edate: ['', Validators.required],
      STID: ['', Validators.required],
      venue: ['', Validators.required],
    })

    //console.log(this.global.requestid()+this.data.idnum)
    this.idn = this.data.idnum
    this.companyname = this.data.selectedData.companyname
    this.ptype = this.data.selectedData.trainingType
    this.seminardesc = this.data.selectedData.seminardescription
    this.venue = this.data.selectedData.venue
    var sd = new Date(this.data.selectedData.startdate);
    var ed = new Date(this.data.selectedData.enddate);
    this.sdate = sd;
    this.edate = ed;
    this.STID = this.data.selectedData.trainingTypeID.toString();

    if (this.data.selectedData.statusID == 1 || this.data.selectedData.statusID == null) {
      this.statusid = 1;
    } else
      this.statusid = this.data.selectedData.statusID
    //console.log(this.statusid)


    this.form.get('seminardesc').patchValue(this.seminardesc);
    this.form.get('companyname').patchValue(this.companyname);
    this.form.get('sdate').patchValue(this.sdate);
    this.form.get('edate').patchValue(this.edate);
    this.form.get('STID').patchValue(this.STID);
    this.form.get('venue').patchValue(this.venue);
  }

  checked() {
    if (this.statusid == 0) this.statusid = 1
    else this.statusid = 0
  }

  display(c) {
    //console.log(c.trainingType)
    this.STID = c.trainingtypeID;
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    if (this.form.get('seminardesc').value != '' && this.form.get('companyname').value != '' &&
      this.form.get('sdate').value != '' && this.form.get('edate').value != '' &&
      this.form.get('STID').value != '' && this.form.get('venue').value != '') {
      if (this.form.get('sdate').value > this.form.get('edate').value) {
        this.global.swalAlert('Invalid date range', "", 'error');
      } else {
        let sdate = new Date(this.form.get('sdate').value).toLocaleString();
        let edate = new Date(this.form.get('edate').value).toLocaleString();
        //console.log(this.data.selectedSID+"--"+this.seminardesc+"--"+ this.companyname+"--"+this.sdate+"--"+this.edate+"--"+this.STID+"--"+this.venue+"--"+this.statusid)
        this.global.swalLoading('Updating Seminar and Training');
        this.HrisApi.putEmployeeSeminarAndTraining(this.data.selectedSID, {
          "seminardesc": this.form.get('seminardesc').value,
          "companyname": this.form.get('companyname').value,
          "sdate": sdate,
          "edate": edate,
          "trainingTypeID": this.form.get('STID').value,
          "venue": this.form.get('venue').value,
          "statusID": this.statusid,
          "approvedBy": this.global.requestid()
        })
          // this.http.put(this.global.api + 'Employee/UpdateSeminarAndTraining/' + , this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(res)
            this.global.swalClose();
            this.global.swalAlert(res.message, "", 'success');
            this.dialogRef.close({ result: "Update success" });
          }, Error => {
            this.global.swalAlertError();
            console.log(Error)
          });
      }
    }
  }
}
