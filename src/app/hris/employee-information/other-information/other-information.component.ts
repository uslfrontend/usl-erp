

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GlobalService } from '../../../global.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewChild, ElementRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { UpdatePersonalInformationComponent } from '../update-personal-information/update-personal-information.component';
import { AddSaTComponent } from '../seminars-trainings/add-sa-t/add-sa-t.component';
import { UpdateSaTComponent } from '../seminars-trainings/update-sa-t/update-sa-t.component';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { AddResearchComponent } from '../research/add-research/add-research.component';
import { UpdateResearchComponent } from '../research/update-research/update-research.component';
import { AddComExtComponent } from '../community-extension/add-com-ext/add-com-ext.component';
import { UpdateComExtComponent } from '../community-extension/update-com-ext/update-com-ext.component';
import { AddPorgComponent } from '../professional-organization/add-porg/add-porg.component';
import { UpdatePorgComponent } from '../professional-organization/update-porg/update-porg.component';
import { HrisemployeeLookupComponent } from '../../hris-lookup/hrisemployee-lookup/hrisemployee-lookup.component';
import { UpdateNameComponent } from '../update-name/update-name.component';
import { HrisApiService } from '../../../hris-api.service';


import { AwardsComponent } from '../awards/awards.component';
import { SpeakingEngagementComponent } from '../speaking-engagement/speaking-engagement.component';



import jsPDF from 'jspdf';


@Component({
  selector: 'app-other-information',
  templateUrl: './other-information.component.html',
  styleUrls: ['./other-information.component.css']
})

export class OtherInformationComponent implements OnInit {
  image: any = 'assets/noimage.jpg';
  signature: any = 'assets/nosignature.jpg';
  name: any = '';
  position: any = '';
  idnumber: any = '';
  id: any = '';
  dtridnum: any = '';

  pinfo

  childrenInfo

  ChildrenArr;
  EducTableArr;
  EligTableArr;
  SeminarTableArr;

  WorkTableArr;
  OrgTableArr;
  ResearchTableArr;
  ComTableArr;

  awardsArr;
  speakingEngArr;

  appointmentArr;
  contractArr;
  rankRecordsArr;

  empDeptID
  employeeDeptIDAr: string[] = [];

  ctr = 0;

  config: any;
  collection = { count: 60, data: [] };

  showContent: boolean = false;

  imageTestData

  fname
  mname
  lname
  suffix
  gender
  cstatus
  tempDB
  personNameInfo

  constructor(public dialog: MatDialog, private domSanitizer: DomSanitizer, private global: GlobalService, private http: Http, private HrisApi: HrisApiService) {

    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };
  }

  WorkDisplayedColumns = ['position', 'companyname', 'startdate', 'enddate'];
  @ViewChild(MatSort, { static: false }) sort4: MatSort;
  @ViewChild('paginator4', { static: false }) paginator4: MatPaginator;

  ngOnInit() {
    if (this.global.hrisId != '') {
      this.id = this.global.hrisId
      this.keyDownFunction('onoutfocus')
    }
    //console.log(this.global.requestid());
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngAfterViewInit() {

  }
  employeelookup(): void {

    const dialogRef = this.dialog.open(HrisemployeeLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result;
        this.keyDownFunction('onoutfocus')
      }
    });

  }

  result
  keyDownFunction(event) {

    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {

      this.global.hrisId = this.id
      //console.log("1st")
      this.employeeDeptIDAr.length = 0;
      if (this.id != '') {
        if (this.id == this.global.requestid()) {
          this.proceedProc();
        } else {
          if (this.global.checkrole('HRMO')) {
            this.proceedProc();
          } else {
            this.checkAppointment();
          }

        }
      }
      else {
        this.showContent = false;
      }
      // code...
    }
  }


  checkAppointment() {
    this.HrisApi.getEmployeeAppointment(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentArr = res.data;
        //console.log("VIEW DOMAINS UNDER THE USER:");
        //console.log(this.global.viewdomain);

        //console.log("checking of appointment:");
        // console.log(this.appointmentArr);
        // console.log(res.data[0]);
        // console.log(res.data);
        if (res.data) {
          for (var x = 0; x < res.data.length; x++) {
            //if(res.data[x].classificationID == 2 &&res.data[x].active == 1) //used when getting only the fulltime employee's
            if (res.data[x].active == 1) {
              //this.empDeptID = res.data[x].departmentID.toString();//used when getting the first active appointment
              //x=res.data.length; //used when getting the first active appointment

              this.employeeDeptIDAr.push(res.data[x].departmentID.toString());


            }
            else {
              this.global.swalAlert('', 'Employee is a part time and has no active appointment', 'error');
            }
          }
        }
        else {
          this.global.swalAlert('', 'Employee has no active appointment', 'error');
        }

        // console.log(this.employeeDeptIDAr.length)

        if (this.employeeDeptIDAr.length == 1) {
          //////THIS DECISION PROCESS ONLY FIRES UP WHEN THERE'S ONLY ONE APPOINTMENT FOR A CERTAIN EMPLOYEE
          this.empDeptID = this.employeeDeptIDAr[0].toString();
          //console.log(this.empDeptID);
          this.validateAppointment(this.global.checkdomain(this.empDeptID));

        } else if (this.employeeDeptIDAr.length == 0) {
          console.log("employee does not have assigned appointment yet")
        }
        else {
          //////THIS DECISION PROCESS ONLY FIRES UP WHEN THERE'S MORE THAN ONE APPOINTMENT FOR A CERTAIN EMPLOYEE
          for (var i = this.employeeDeptIDAr.length - 1; i >= 0; i--) {
            if (this.global.checkdomain(this.employeeDeptIDAr[i]) == true) {
              this.result = true;
            }
            else {
              this.result = false;

            }
          }
          this.validateAppointment(this.result);
        }


        //console.log(this.employeeDeptIDAr)

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  validateAppointment(param) {
    if (param == true) {
      this.proceedProc();
    }
    else {
      this.clear();
      this.global.swalAlert('', 'Employee does not belong to your department/office', 'error');
    }
  }

  proceedProc() {
    this.global.swalLoading('Loading Person Information');
    this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
      // console.log(res);
      this.global.swalClose();
      if (res.message != "IDNumber does not exist.") {
        this.showContent = true;
        this.name = res.data[0].fullname;
        this.position = res.data[0].position;
        this.idnumber = res.data[0].idnumber;
        this.dtridnum = "DTR ID#: " + res.data[0].dtrid;
        this.getBasicInfo();
        this.getPersonalInfo();
        // this.getEducAttainment();
        // this.getElgibilities();
        this.getSeminars();
        // this.getWork();
        this.getOrganization();
        this.getResearch();
        this.getComExt();
        this.getAppointment();
        // this.getContract();
        // this.getRankRecords();
        this.getSpeakingEngagement();
        this.getAwards();
      } else {
        //console.log('1111')
        this.image = 'assets/noimage.jpg';
        this.signature = 'assets/nosignature.jpg';
        this.name = '';
        this.position = '';
        this.idnumber = '';
        this.dtridnum = '';
    
        this.global.swalAlert("Employment record not found.", '', 'warning');
        this.showContent = false
      }
    }, Error => {
      this.clear();
      this.global.swalAlertError();
    });
  }

  getAppointment() {
    //  this.global.swalLoading('Loading item resources')

    this.HrisApi.getEmployeeAppointment(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentArr = res.data;
        //  this.global.swalClose();

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }



  updatenamevar = false
  updatename() {
    if (this.updatenamevar == true) {

      this.updatenamevar = false
      this.proceedProc();
    }
    else {
      this.global.swalLoading('Loading item resources')
      this.HrisApi.getEmployeePerson(this.id)
        .map(response => response.json())
        .subscribe(res => {
          this.personNameInfo = res.data;
          this.fname = res.data.firstName
          this.mname = res.data.middleName
          this.lname = res.data.lastName
          this.suffix = res.data.suffixName
          this.gender = res.data.gender
          this.cstatus = res.data.civilStatus
          this.tempDB = res.data.dateOfBirth
          this.global.swalClose();

        }, Error => {
          //console.log(Error);
          this.global.swalAlertError();
          //console.log(Error)
        });
      this.updatenamevar = true
    }

  }

  updatenameapi() {

    var error = '';
    if (this.fname == '') {
      error = error + '*First name must not be blank<br>';
    }
    if (this.lname == '') {
      error = error + '*Last name must not be blank<br>';
    }

    if (error == '') {
      var x = ''
      let date = new Date(this.tempDB).toLocaleString();
      this.global.swalLoading('Updating...');
      this.HrisApi.putEmployeePerson(this.id,
        {
          'FirstName': this.fname,
          'MiddleName': this.mname,
          'LastName': this.lname,
          'SuffixName': this.suffix,
          'Gender': this.gender,
          'CivilStatus': this.cstatus,
          'DateOfBirth': this.tempDB,
        })
        .map(response => response.json())
        .subscribe(res => {
          this.global.swalSuccess(res.message);
          this.updatenamevar = false
          this.proceedProc();
        }, Error => {
          this.global.swalAlertError();
        });
    } else {
      this.global.swalAlert('The Following error has Occured', error, 'error');
    }

  }

  getBasicInfo() {
    this.HrisApi.getEmployeeChrildren(this.id)
      .map(response => response.json())
      .subscribe(res => {
        //console.log("Children"+res.data)
        this.ChildrenArr = res.data;
        //console.table(res.data);
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getPersonalInfo() {
    this.HrisApi.getEmployeePersonalInformation(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.pinfo = res.data;
        this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
        this.imageTestData = res.data.idPicture;
        this.signature = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.signature);

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getSeminars() {
    this.HrisApi.getEmployeeSeminarsAndTrainings(this.id)
      .map(response => response.json())
      .subscribe(res => {
        //console.table(res.data);
        this.SeminarTableArr = res.data;
        if (this.SeminarTableArr != undefined || this.SeminarTableArr != null) {
          var count = Object.keys(this.SeminarTableArr).length;
          this.ctr = count
        }

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getOrganization() {
    this.HrisApi.getEmployeeOrganization(this.id)
      .map(response => response.json())
      .subscribe(res => {
        //console.log(res);
        this.OrgTableArr = res.data;

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }
  getResearch() {
    this.HrisApi.getEmployeeResearch(this.id)
      .map(response => response.json())
      .subscribe(res => {
        //console.log(res);
        this.ResearchTableArr = res.data;
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }
  getComExt() {
    this.HrisApi.getEmployeeCommunityExtension(this.id)
      .map(response => response.json())
      .subscribe(res => {
        //console.log(res);
        this.ComTableArr = res.data;
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }
  getAwards() {//console.log("aaa")
    this.HrisApi.getEmployeeAwards(this.id)
      .map(response => response.json())
      .subscribe(res => {

        //console.log(res.data)
        this.awardsArr = res.data;
        //console.log(this.global.CommunityExtArray)
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }
  getSpeakingEngagement() {
    this.HrisApi.getEmployeeSpeakingEngagement(this.id)
      .map(response => response.json())
      .subscribe(res => {  //console.log(res.data)
        this.speakingEngArr = res.data;
        //console.log(this.global.CommunityExtArray)
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }


  // async nameUpdate() {
  //   try {
  //     const response = await this.HrisApi.getEmployeePerson(this.id).toPromise();
  //     const res = response.json();
  //     this.personNameInfo = res.data;
  //     this.fname = res.data.firstName
  //     this.mname = res.data.middleName
  //     this.lname = res.data.lastName
  //     this.suffix = res.data.suffixName
  //     this.gender = res.data.gender
  //     this.cstatus = res.data.civilStatus
  //     this.tempDB = res.data.dateOfBirth

  //     // console.log(this.cstatus)
  //     // console.log(this.tempDB)

  //     const dialogRef = this.dialog.open(UpdateNameComponent, {
  //       data: {
  //         id: this.id,
  //         personNameInfo: this.personNameInfo,
  //         name: this.name,
  //         fname: this.fname,
  //         mname: this.mname,
  //         lname: this.lname,
  //         suffix: this.suffix,
  //         gender: this.gender,
  //         cstatus: this.cstatus,
  //         tempDB: this.tempDB
  //       }
  //     });
  //     dialogRef.afterClosed().subscribe(result => {
  //       // console.log(Dialog result: ${result});
  //       this.updateEmployeeName()

  //     });
  //   } catch (error) {
  //     this.global.swalAlertError();
  //   }
  // }

  nameUpdate() {
    this.HrisApi.getEmployeePerson(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.personNameInfo = res.data;
        this.fname = res.data.firstName
        this.mname = res.data.middleName
        this.lname = res.data.lastName
        this.suffix = res.data.suffixName
        this.gender = res.data.gender
        this.cstatus = res.data.civilStatus
        this.tempDB = res.data.dateOfBirth

        const dialogRef = this.dialog.open(UpdateNameComponent, {
          data: {
            id: this.id,
            personNameInfo: this.personNameInfo,
            name: this.name,
            fname: this.fname,
            mname: this.mname,
            lname: this.lname,
            suffix: this.suffix,
            gender: this.gender,
            cstatus: this.cstatus,
            tempDB: this.tempDB
          }, autoFocus: false
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log(Dialog result: ${result});
          this.updateEmployeeName()
        })

      })

    
  }

  updateEmployeeName() {
    this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
      // console.log(res);
      this.global.swalClose();
      if (res.message != "IDNumber does not exist.") {
        this.showContent = true;
        this.name = res.data[0].fullname;
        this.position = res.data[0].position;
        this.idnumber = res.data[0].idnumber;
        this.dtridnum = "DTR ID#: " + res.data[0].dtrid;
      } else {
        //console.log('1111')
        this.clear();
        this.global.swalAlert("Employee not found", '', 'warning');
      }

    }, Error => {
      this.clear();
      this.global.swalAlertError();
    });
  }

  editInfoOpenDialog(): void {
    const dialogRef = this.dialog.open(UpdatePersonalInformationComponent, {
      width: '900px', data: { selectedData: this.pinfo, selectedID: this.idnumber }

    });

    dialogRef.afterClosed().subscribe(res => {
      if (res == undefined) {

      } else
        if (res.result == 0) {
          //console.log("----")
          //console.log(res.data)
        }
        else {
          //console.log(res);
          this.keyDownFunction('onoutfocus');
        }
      //console.log(result);
    });
  }

  addSaTOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AddSaTComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.id },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getSeminars();
          }
        }
      });
    }
  }

  approveSat(SatData) {
    this.global.swalLoading('Updating Seminar and Training');
    //console.log(SatData.seminarid+' - '+SatData.seminardescription+' - '+SatData.companyname+' - '+SatData.startdate+' - '+SatData.enddate+' - '+SatData.trainingTypeID+' - '+SatData.venue;

    this.HrisApi.putEmployeeSeminarAndTraining(SatData.seminarid, {
      "seminardesc": SatData.seminardescription,
      "companyname": SatData.companyname,
      "sdate": SatData.startdate,
      "edate": SatData.enddate,
      "trainingTypeID": SatData.trainingTypeID,
      "venue": SatData.venue,
      "statusID": 1,
      "approvedBy": this.global.requestid()
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        this.global.swalAlert("Success", "", 'success');
        this.getSeminars();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }
  updateSaTOpenDialog(SaTData): void {
    //console.log(this.idnumber);
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(UpdateSaTComponent, {
        width: '600px', disableClose: false, data: { selectedData: SaTData, selectedSID: SaTData.seminarid, idnum: this.idnumber },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getSeminars();
          }
        }
      });
    }
  }

  deleteSat(SID) {

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {

        this.HrisApi.deleteEmployeeSeminarAndTraining(SID)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              this.global.swalAlert(res.message, "", 'success');
              this.getSeminars();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }


  approveOrg(ot) {
    this.global.swalLoading('Updating organization');
    this.HrisApi.putEmployeeOrganization(ot.organizationid, {
      "position": ot.position,
      "organization": ot.organization,
      "statusID": 1,
      "approvedBy": this.global.requestid()
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        //this.global.swalClose();
        this.global.swalAlert("Professional membership to organization updated successfully.", "", 'success');
        this.getOrganization();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }
  updateOrgOpenDialog(porgData): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(UpdatePorgComponent, {
        width: '600px', disableClose: false, data: { selectedData: porgData, selectedPID: porgData.organizationid, idnum: this.idnumber }
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getOrganization();
          }
        }
      });
    }
  }

  deleteOrg(PID) {

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeOrganization(PID)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message == 'Organization removed successfully.') {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert('Professional membership to organization removed successfully.', "", 'success');
              this.getOrganization();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }

  addOrgOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AddPorgComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.id }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getOrganization();
          }
        }
      });
    }
  }


  ////////////////RESEARCH////////////////////////////////////////////////////////////////////////////////
  approveRes(rt) {
    this.global.swalLoading('Updating research');
    this.HrisApi.putEmployeeUpdateResearch(rt.researchID, {
      "title": rt.title,
      "dateCompleted": rt.dateCompleted,
      "remarks": rt.remarks,
      "copyrightYear": rt.copyrightYear,
      "statusID": 1,
      "approvedBy": this.global.requestid()
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        //this.global.swalClose();
        this.global.swalAlert("Success", "", 'success');
        this.getResearch();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }

  addResearchOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AddResearchComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getResearch();
          }
        }
      });
    }
  }

  updateResearchOpenDialog(resData): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(UpdateResearchComponent, {
        width: '600px', disableClose: false, data: { selectedData: resData, selectedRID: resData.researchID, idnum: this.idnumber },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getResearch();
          }
        }
      });
    }
  }
  deleteResearch(RID) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeResearch(RID)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert(res.message, "", 'success');
              this.getResearch();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });

  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  ////////////////COMMUNITY EXTENSIONS////////////////////////////////////////////////////////////////////////////////
  approveCE(ct) {
    this.global.swalLoading('Updating research');
    this.HrisApi.putEmployeeCommunityExtension(ct.ceid, {
      "activity": ct.activity,
      "dateConducted": ct.dateConducted,
      "location": ct.location,
      "StatusID": 1,
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        //this.global.swalClose();
        this.global.swalAlert("Success", "", 'success');
        this.getComExt();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }

  addComExtOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AddComExtComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getComExt();
          }
        }
      });
    }
  }
  updateComExtOpenDialog(cData): void {
    // console.log(cData.ceid)
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(UpdateComExtComponent, {
        width: '600px', disableClose: false, data: { selectedData: cData, selectedCID: cData.ceid, idnum: this.idnumber },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getComExt();
          }
        }
      });
    }
  }

  deleteComExt(RID) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeCommunityExtension(RID)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert('Community Engagement removed successfully.', "", 'success');
              this.getComExt();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });

  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  ////////////////AWARDS ////////////////////////////////////////////////////////////////////////////////
  approveAw(e) {
    this.global.swalLoading('Updating research');

    this.HrisApi.putEmployeeAward(e.id, {
      "EventTitle": e.eventTitle,
      "AwardReceived": e.awardReceived,
      "AwardingBody": e.awardingBody,
      "Venue": e.venue,
      "DateAwarded": e.dateAwarded,
      "StatusId": 1
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        //this.global.swalClose();
        this.global.swalAlert("Success", "", 'success');
        this.getAwards();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }
  addAwOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(AwardsComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.id, type: "Add" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getAwards();
          }
        }
      });
    }
  }
  updateAwOpenDialog(eData, eid): void {
    //console.log(cData.ceid)
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(AwardsComponent, {
        width: '600px', disableClose: false, data: { selectedData: eData, selectedEID: eData.id, selectedID: this.idnumber, type: "Update" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getAwards();
          }
        }
      });
    }
  }
  deleteAw(AID) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeAward(AID)
          .map(response => response.json())
          .subscribe(res => {

            this.global.swalAlert('Award removed successfully.', "", 'success');
            this.getAwards();
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });

  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  ////////////////SPEAKING ENGAGEMENT////////////////////////////////////////////////////////////////////////////////
  approveSE(e) {
    this.global.swalLoading('Updating research');

    this.HrisApi.putEmployeeSpeakingEngagement(e.id, {
      "EmployeeId": e.employeeId,
      "ConferenceTitle": e.conferenceTitle,
      "EventOrganizer": e.eventOrganizer,
      "StartDate": e.startDate,
      "EndDate": e.endDate,
      "Venue": e.venue,
      "statusId": 1,
    })
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res)
        //this.global.swalClose();
        this.global.swalAlert("Success", "", 'success');
        this.getSpeakingEngagement();
      }, Error => {
        this.global.swalAlertError();
        console.log(Error)
      });
  }

  addSEOpenDialog(): void {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(SpeakingEngagementComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.id, type: "Add" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          //console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getSpeakingEngagement();
          }
        }
      });
    }
  }
  updateSEOpenDialog(eData, eid): void {
    //console.log(cData.ceid)
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      //console.log(eligData.eligibilityid);
      const dialogRef = this.dialog.open(SpeakingEngagementComponent, {
        width: '600px', disableClose: false, data: { selectedData: eData, selectedEID: eData.id, selectedID: this.idnumber, type: "Update" },
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        //this.keyDownFunction('onoutfocus');
        //console.log(result);
        if (result != undefined) {
          if (result.result == 'Update success') {
            this.getSpeakingEngagement();
          }
        }
      });
    }
  }
  deleteSE(AID) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeSpeakingEngagement(AID)
          .map(response => response.json())
          .subscribe(res => {

            this.global.swalAlert('Professional Engagement removed successfully.', "", 'success');
            this.getSpeakingEngagement();
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });

  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  clear() {
    this.image = 'assets/noimage.jpg';

    this.signature = 'assets/nosignature.jpg';
    this.name = '';
    this.position = '';
    this.id = '';
    this.idnumber = '';
    this.dtridnum = '';

  }

  tabbing = 0;
  getindex(tab) {
    this.tabbing = tab.index;
  }

}

export interface PeriodicElement {
  lName: any;
  fName: any;
  mName: any;
  dBirth: any;
  usltIDNum: any;
}


