import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
// import { KeyboardEvent } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';
import { ApiService } from '../../../../api.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ElementRef, ViewChild } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
	selector: 'app-rank-records',
	templateUrl: './rank-records.component.html',
	styleUrls: ['./rank-records.component.css']
})
export class RankRecordsComponent implements OnInit {

	@ViewChild('Input', { static: true }) Input: ElementRef<HTMLInputElement>;

	targetupdateID;
	sy;
	rank;
	department;
	position;
	status;
	term;
	evaluation;
	recommendation;
	tempArr = ''

	form: FormGroup;

	rankArr;
	deptArr;
	posArr;
	classArr;
	tocArr;
	evalStatArr;
	appointmentArr;
	rankStatusArr;

	year1;
	year2;

	schoolYear;

	rrArr: string[] = []
	rrValue = new FormControl('', [Validators.required]);
	rrOptions: Observable<string[]>;

	departmentArr: string[] = []
	deparmentValue = new FormControl('', [Validators.required]);
	deparmentOptions: Observable<string[]>;

	positionArr: string[] = []
	positionValue = new FormControl('', [Validators.required]);
	positionOptions: Observable<string[]>;

	options: string[] = [];
	myControl = new FormControl('');
	filteredOptions: Observable<string[]>;

	separatorKeysCodes: number[] = [ENTER, COMMA];
	recommendations = new FormControl();
	filteredRecommendations: Observable<string[]>;
	option: string[] = [];
	allRecommendations: string[] = [];

	constructor(public dialogRef: MatDialogRef<RankRecordsComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public dialog: MatDialog,
		private global: GlobalService,
		private http: Http,
		private api: ApiService,
		private FormBuilder: FormBuilder,
		private HrisApi: HrisApiService,
		private httpC: HttpClient,) {

		if (global.syear != null) {
			this.year1 = global.syear.substring(0, 4)
			this.year2 = (parseInt(global.syear.substring(0, 4)) + 1).toString()
			this.schoolYear = this.year1 + '-' + this.year2
		}

		this.global.swalLoading('Loading appointment resources')
		this.HrisApi.getHRISMaintenanceRankList()
			// this.http.get(this.global.api+'HRISMaintenance/RankList',this.global.option)
			.map(response => response.json())
			.subscribe(res => {
				this.rankArr = res.data;

				for (var i = 0; i < this.rankArr.length; ++i) {
					this.rrArr.push(this.rankArr[i].rank)
				}

				// console.log(this.rrArr)
				this.global.swalLoading('Loading appointment resources')
				this.HrisApi.getHRISMaintenanceDepartment()
					// this.http.get(this.global.api+'HRISMaintenance/Department',this.global.option)
					.map(response => response.json())
					.subscribe(res => {
						this.deptArr = res.data;
						for (var i = 0; i < this.deptArr.length; ++i) {
							this.departmentArr.push(this.deptArr[i].departmentName)
						}
						// console.log(this.departmentArr)
						this.HrisApi.getHRISMaintenancePositionList()
							// this.http.get(this.global.api+'HRISMaintenance/PositionList',this.global.option)
							.map(response => response.json())
							.subscribe(res => {
								this.posArr = res.data;
								for (var i = 0; i < this.posArr.length; ++i) {
									this.positionArr.push(this.posArr[i].position)
								}

								this.HrisApi.getHRISMaintenanceClassifications()
									// this.http.get(this.global.api+'HRISMaintenance/Classifications',this.global.option)
									.map(response => response.json())
									.subscribe(res => {
										this.classArr = res.data;

										this.HrisApi.getHRISMaintenanceEmpStatus()
											// this.http.get(this.global.api+'HRISMaintenance/EmpStatus',this.global.option)
											.map(response => response.json())
											.subscribe(res => {
												this.tocArr = res.data;

												this.HrisApi.getHRISMaintenanceEvaluationStatuses().map(response => response.json())
													.subscribe(res => {
														this.evalStatArr = res.data

														this.HrisApi.getHRISMaintenanceRankStatuses()
															.map(response => response.json())
															.subscribe(res => {
																this.rankStatusArr = res.data;

																this.HrisApi.getEmployeeAppointmentbyActive(this.data.selectedID, 1)
																	.map(response => response.json())
																	.subscribe(res => {
																		this.appointmentArr = res.data;


																	}, Error => {
																		this.global.swalAlertError();
																	});

															}, Error => {
																this.global.swalAlertError();
															});

													}, Error => {
														this.global.swalAlertError();
													});
											}, Error => {
												this.global.swalAlertError();
											});

									}, Error => {
										this.global.swalAlertError();
									});

							}, Error => {
								this.global.swalAlertError();
							});

					}, Error => {
						this.global.swalAlertError();
					});

				this.global.swalClose();
			}, Error => {
				this.global.swalAlertError();
			});
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.departmentArr.filter(option => option.toLowerCase().includes(filterValue));
	}

	private _filter1(value: string): string[] {
		const filterValue1 = value.toLowerCase();
		return this.positionArr.filter(option1 => option1.toLowerCase().includes(filterValue1));
	}

	private _filter2(value: string): string[] {
		const filterValue2 = value.toLowerCase();
		return this.options.filter(options => options.toLowerCase().includes(filterValue2));
	}

	private _filter3(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.allRecommendations.filter((recommendation) => recommendation.toLowerCase().includes(filterValue));
	}

	private _filter4(value: string): string[] {
		const filterValue = value.toLowerCase();
		return this.rrArr.filter(option => option.toLowerCase().includes(filterValue));
	}

	add(event: MatChipInputEvent): void {

		if (event.value != '') {
			let value = (event.value || '').trim();

			// Check if the value already ends with a semicolon
			if (!value.endsWith(';')) {
				value += ';'; // Append semicolon if not present
			}

			// Add our value as a chip
			if (value) {
				this.option.push(value);
			}

			// Clear the input value
			event.input.value = '';

			// Reset the recommendations
			this.recommendations.setValue(null);
		}

	}

	remove(rec: string): void {
		const index = this.option.indexOf(rec);

		if (index >= 0) {
			this.option.splice(index, 1);
		}
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		const value = event.option.viewValue;
		if (value != 'With very high student\'s evaluation( );') {


			// Add the selected fruit
			if (value && !this.option.includes(value)) {
				this.option.push(value);
			}

			// Clear the input value
			this.Input.nativeElement.value = '';

			this.recommendations.setValue(null);
		} else {
			this.recommendations.setValue('With very high student\'s evaluation( );')
		}

	}

	ngOnInit() {

		this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
		this.positionOptions = this.positionValue.valueChanges.pipe(startWith(''), map(value => this._filter1(value)));
		this.filteredOptions = this.myControl.valueChanges.pipe(startWith(''), map(value => this._filter2(value)),);
		this.filteredRecommendations = this.recommendations.valueChanges.pipe(startWith(null),
			map((rec: string | null) => (rec ? this._filter3(rec) : this.allRecommendations.slice())));
		this.rrOptions = this.rrValue.valueChanges.pipe(startWith(''), map(value => this._filter4(value)));

		this.form = this.FormBuilder.group({
			// sy: ['', Validators.required],
			rank: ['', Validators.required],
			// department: ['', Validators.required],
			position: ['', Validators.required],
			status: ['', Validators.required],
			term: ['', Validators.required],
			evaluation: ['', Validators.required],
			recommendation: ['', Validators.required],
			evaluationStatus: ['', Validators.required],
			rankStatusID: ['0', Validators.required],
			appointmentID: ['', Validators.required],
		});

		if (this.data.type == "update") {
			// console.log(this.data)
			const sy1 = this.data.selectedData.sy.substring(0, 4)
			const sy2 = this.data.selectedData.sy.substring(5, 9)
			this.year1 = sy1
			this.year2 = sy2
			this.schoolYear = this.year1 + '-' + this.year2
			// this.form.get('sy').patchValue(this.data.selectedData.sy);

			this.form.get('rank').patchValue(this.data.selectedData.rank);
			this.rrValue.setValue(this.data.selectedData.rank);
			this.deparmentValue.setValue(this.data.selectedData.department);
			this.positionValue.setValue(this.data.selectedData.position);
			// this.form.get('department').patchValue(this.data.selectedData.department);
			// this.form.get('position').patchValue(this.data.selectedData.position);
			this.form.get('status').patchValue(this.data.selectedData.status);
			this.form.get('term').patchValue(this.data.selectedData.empStatusID);
			this.form.get('evaluation').patchValue(this.data.selectedData.evaluation);
			// this.form.get('recommendation').patchValue();
			this.form.get('rankStatusID').patchValue(this.data.selectedData.rankStatusID.toString());
			this.form.get('evaluationStatus').patchValue(this.data.selectedData.evaluationStatusID.toString());
			this.form.get('appointmentID').patchValue(this.data.selectedData.appointmentID.toString());
			if (this.data.selectedData.recommendation) {
				const result: string[] = this.splitSentenceIntoArray(this.data.selectedData.recommendation);
				this.option = result
			}

			// this.evalChecker()
			// console.log(this.option)
			// this.allRecommendations = this.data.selectedData.recommendation
			// this.myControl.setValue(this.data.selectedData.recommendation)
			this.targetupdateID = this.data.selectedData.evaluationid;
			// this.updateCharacterCount()
			this.commentsList()
		}

	}


	appointmentPopulate() {
		for (var x = 0; x < this.appointmentArr.length; x++) {
			if (this.form.get('appointmentID').value == this.appointmentArr[x].appointmentID.toString()) {
				this.deparmentValue.setValue(this.appointmentArr[x].departmentName);
				this.positionValue.setValue(this.appointmentArr[x].position);
				const sDate = this.appointmentArr[x].startdate.substring(6, 10)
				const eDate = this.appointmentArr[x].enddate.substring(6, 10)
				this.year1 = sDate
				this.year2 = eDate
				this.schoolYear = this.year1 + '-' + this.year2
			}
		}
	}

	evalChecker() {
		const evaluationValue = this.form.get('evaluation').value;

		if (evaluationValue != null && !isNaN(evaluationValue)) {
			const evalLength = parseFloat(evaluationValue);

			if (!isNaN(evalLength) && evalLength >= 75 && evalLength <= 100) {
				this.form.get('evaluationStatus').patchValue('1');
			} else if (!isNaN(evalLength) && evalLength >= 50 && evalLength < 75) {
				this.form.get('evaluationStatus').patchValue('2');
			} else {
				this.form.get('evaluationStatus').patchValue('0');
			}
		}

		this.commentsList();
	}


	evalStatus() {
		if (this.form.get('evaluationStatus').value == 0) {
			this.form.get('evaluation').patchValue(0)
		}
		else {
			this.form.get('evaluation').patchValue(null)
		}

	}

	textareaValue: string = '';
	characterCount: number = 0;

	// updateCharacterCount() {
	// 	if (this.myControl.value != null || this.myControl.value != undefined) {
	// 		this.textareaValue = this.myControl.value;
	// 		this.characterCount = this.textareaValue.length;
	// 	}
	// 	// this.deleteAdditionalText(event);
	// }

	yearchange1() {
		this.year2 = this.year1 + 1;
		this.schoolYear = this.year1 + '-' + this.year2
	}

	yearchange2() {
		this.year1 = this.year2 - 1;
		this.schoolYear = this.year1 + '-' + this.year2
	}

	onNoClickclose(): void {
		this.dialogRef.close({ result: 'cancel' });
	}

	generateSentence(phrases: string[]): string {
		let sentence: string = phrases.join(' ');
		sentence = sentence.charAt(0).toUpperCase() + sentence.slice(1) + ' ';
		return sentence;
	}

	splitSentenceIntoArray(sentence: string): string[] {
		const phrases: string[] = sentence.split(';').map((phrase) => phrase.trim());

		const uniquePhrases: string[] = [];
		phrases.forEach((phrase) => {
			if (phrase && !uniquePhrases.includes(phrase)) {
				uniquePhrases.push(phrase + ';');
			}
		});

		return uniquePhrases;
	}

	// add() {

	// 	const dialogRef = this.dialog.open(AddRecommendationsComponent, {
	// 		width: '600px', disableClose: false, data: { place: 'here' }, autoFocus: false
	// 	});

	// 	dialogRef.afterClosed().subscribe(result => {
	// 		if (result != undefined) {

	// 		}
	// 	});
	// }

	commentsList() {
		this.options = []
		if (this.form.get('evaluationStatus').value == 1) {
			this.allRecommendations = [
				'Accelerated in rank (5 consecutive years with 91 and above rating);',
				'Granted regular status as faculty;',
				'Given warning on excessive absences and tardiness;',
				'Given warning on excessive absences;',
				'Given warning on excessive tardiness;',
				'Passed but  not rehired due to administrative case;',
				'Passed but terminated due to administrative case;',
				'Pegged in rank due to non-submission of research;',
				'Pegged in rank pending required Master\'s or Doctorate degree;',
				'Promoted in rank;',
				'Promotion in rank is upon submission of research;',
				'Rehired;',
				'With very high overall performance evaluation rating;',
				'With very high student\'s evaluation( );',
				'With zero absence and tardiness;',
				'With zero absence;',
				'With zero tardiness;']
			this.filteredRecommendations = this.recommendations.valueChanges.pipe(startWith(null),
				map((rec: string | null) => (rec ? this._filter3(rec) : this.allRecommendations.slice())));
		} else if (this.form.get('evaluationStatus').value == 2) {
			this.allRecommendations = [
				'Non-renewal of contract/end of contract due to failed rating and due to administrative case;',
				'Non-renewal of contract/end of contract;',
				'Rehired but given warning warning on consecutive retention in rank;',
				'Rehired but given warning warning on consecutive retention in rank;',
				'Rehired and given another chance due to high students\' evaluation but subject to availability of loads;',
				'Rehired and given last chance due to high students\' evaluation but subject to availability of loads;',
				'With very low students\' evaluation ( );',
				'With excessive absences and tardiness;',
				'With excessive absences;',
				'With excessive tardiness;'];
			this.filteredRecommendations = this.recommendations.valueChanges.pipe(startWith(null),
				map((rec: string | null) => (rec ? this._filter3(rec) : this.allRecommendations.slice())));
		}

		// this.updateCharacterCount()
	}

	fillInDepartment() {
		this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
	}

	fillInRank() {
		this.rrOptions = this.rrValue.valueChanges.pipe(startWith(''), map(value => this._filter4(value)));
	}

	save(): void {

		const result: string = this.generateSentence(this.option);
		this.rrValue.markAllAsTouched();
		this.positionValue.markAllAsTouched();
		this.deparmentValue.markAllAsTouched();
		this.form.markAllAsTouched();
		if ((this.year1 != '' && this.year2 != '') &&
			this.rrValue.value != '' &&
			this.deparmentValue.value != '' &&
			this.positionValue.value != '' &&
			this.form.get('status').value != '' &&
			this.form.get('term').value != '' &&
			// this.form.get('evaluation').value != null 
			this.form.get('evaluationStatus').value != '' &&
			this.form.get('appointmentID').value != '' &&
			this.form.get('rankStatusID').value != '') {

			if (this.form.get('evaluation').value == '' || this.form.get('evaluation').value == null) {
				this.form.get('evaluation').patchValue(0)
				// console.log(this.form.get('evaluation').value, 'here')
			}

			if (this.data.type == "update") {

				//console.log('sdate:'+this.sdate+"--"+'edate:'+this.edate);
				this.global.swalLoading('Rank-Record'); 0
				this.HrisApi.putEmployeeRank(this.targetupdateID, this.targetupdateID, {
					"sy": this.schoolYear.toString(),
					"rank": this.rrValue.value,
					"department": this.deparmentValue.value,
					"position": this.positionValue.value,
					"status": this.form.get('status').value,
					"term": this.form.get('term').value,
					"evaluation": this.form.get('evaluation').value,
					"recommendation": result,
					"appointmentID": this.form.get('appointmentID').value,
					"rankStatusID": this.form.get('rankStatusID').value,
					"evaluationStatusID": this.form.get('evaluationStatus').value,
				})
					// this.http.put(this.global.api+'Employee/Rank/'+,this.global.option)
					.map(response => response.json())
					.subscribe(res => {
						// console.log(res)
						//this.global.swalClose();
						this.global.swalAlert("Rank updated successfully.", "", 'success');
						this.dialogRef.close({ result: "Update success" });

					}, Error => {
						this.global.swalAlertError();
						// console.log(Error)
					});

			} else {


				this.global.swalLoading('Adding Rank-Record');
				this.HrisApi.postEmployeeRank(this.data.selectedID, {
					"sy": this.schoolYear.toString(),
					"rank": this.rrValue.value,
					"department": this.deparmentValue.value,
					"position": this.positionValue.value,
					"status": this.form.get('status').value,
					"term": this.form.get('term').value,
					"evaluation": this.form.get('evaluation').value,
					"recommendation": result,
					"appointmentID": this.form.get('appointmentID').value,
					"rankStatusID": this.form.get('rankStatusID').value,
					"evaluationStatusID": this.form.get('evaluationStatus').value,
				})
					// this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
					.map(response => response.json())
					.subscribe(res => {

						// console.log(res)
						this.global.swalClose();
						this.global.swalAlert('Rank created Successfully', "", 'success');
						this.dialogRef.close({ result: "Adding Success" });

					}, Error => {
						this.global.swalAlertError();
						// console.log(Error)
					});
			}
		}
	}
}
