import { Component, OnInit } from '@angular/core';
import { HrisApiService } from '../../../../hris-api.service';
import { GlobalService } from './../../../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-end-of-service',
  templateUrl: './end-of-service.component.html',
  styleUrls: ['./end-of-service.component.css']
})
export class EndOfServiceComponent implements OnInit {

  selectedEOSType

  eDate
  Remarks = ''
  eosTypeArr = []
  activeType = 1
  active = 1
  id = ''
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EndOfServiceComponent>,
    private HrisApi: HrisApiService,
    private global: GlobalService,
    public dialog: MatDialog,
    private FormBuilder: FormBuilder,
  ) { }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      selectedEOSType: ['', Validators.required],
      eDate: ['', Validators.required],
    });

    this.global.swalLoading('Loading resources')
    this.HrisApi.getHRISMaintenanceEOSType().map(response => response.json()).subscribe(res => {
      this.eosTypeArr = res.data

      this.global.swalClose();
    }, Error => {
      this.global.swalAlertError();
    });

    // console.log(this.data)

    if (this.data.type == "update") {

      this.selectedEOSType = this.data.updateData.eosType
      this.Remarks = this.data.updateData.remarks
      this.activeType = 0

      const dateString = this.data.updateData.eosDate;
      const parts = dateString.split("/");
      const year = parseInt(parts[2]);
      const month = parseInt(parts[0]) - 1;
      const day = parseInt(parts[1]);
      const date = new Date(year, month, day);
      const phTimezoneOffset = 8 * 60 * 60 * 1000; // 8 hours ahead of UTC
      const localDate = new Date(date.getTime() + phTimezoneOffset);
      const formattedDate = localDate.toISOString()

      // console.log(formattedDate)
      this.eDate = formattedDate
      this.form.get('selectedEOSType').patchValue(this.selectedEOSType);
      this.form.get('eDate').patchValue(this.eDate);

    } else {
      this.active = 1
      this.activeType = 1
    }

  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  checked() {

    if (this.active == 0) {
      this.active = 1
    }
    else {
      this.active = 0
    }
  }

  save() {

    this.form.markAllAsTouched();

    if (this.form.get('selectedEOSType').value != '' && this.form.get('eDate').value != '' ) {

      if (this.data.type == "add") {
        Swal.fire({
          title: 'Appointment/s will be deactivated.\nStatus will become Inactive.',
          text: "Are you sure?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, save it!'

        }).then((result) => {
          if (result.value) {

            const date = new Date(this.form.get('eDate').value);
            const offsetMs = date.getTimezoneOffset() * 60 * 1000;
            const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
            const formattedDate = localDate.toISOString()
            this.eDate = formattedDate

            for (var x = 0; x < this.eosTypeArr.length; x++) {
              if (this.form.get('selectedEOSType').value == this.eosTypeArr[x].eosType) {
                this.selectedEOSType = this.eosTypeArr[x].id
                this.form.get('selectedEOSType').patchValue(this.selectedEOSType);
              }
            }

            this.form.get('eDate').patchValue(this.eDate);

            // this.global.swalLoading('Adding End of Service');
            this.HrisApi.postEmployeeEOS(
              this.data.selectedID,
              this.form.get('selectedEOSType').value,
              this.form.get('eDate').value,
              this.Remarks).map(response => response.json()).subscribe(res => {

                // this.global.swalAlert('End of Service created successfully.', "", 'success');
                this.dialogRef.close({ result: "Adding Success" });

              }, Error => {
                this.global.swalAlertError();
                // console.log(Error)
              });

            Swal.fire(
              'Created!',
              'End of Service created successfully.',
              'success'
            )
          }
        })

      }
      else if (this.data.type == "update") {

        const date = new Date(this.form.get('eDate').value);
        const offsetMs = date.getTimezoneOffset() * 60 * 1000;
        const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
        const formattedDate = localDate.toISOString()
        this.eDate = formattedDate

        for (var x = 0; x < this.eosTypeArr.length; x++) {
          if (this.form.get('selectedEOSType').value == this.eosTypeArr[x].eosType) {
            this.selectedEOSType = this.eosTypeArr[x].id
            this.form.get('selectedEOSType').patchValue(this.selectedEOSType);
          }
        }

        this.form.get('eDate').patchValue(this.eDate);

        this.global.swalLoading('Updating End of Service');
        this.HrisApi.putEmployeeEOS(
          this.data.selectedID,
          this.form.get('selectedEOSType').value,
          this.form.get('eDate').value,
          this.Remarks).map(response => response.json()).subscribe(res => {
            // console.log(res)
            // this.global.swalClose();
            this.global.swalAlert('End of Service updated successfully.', "", 'success');
            this.dialogRef.close({ result: "Update Success" });

          }, Error => {
            this.global.swalAlertError();
            // console.log(Error)
          });
      }
    }
  }
}

