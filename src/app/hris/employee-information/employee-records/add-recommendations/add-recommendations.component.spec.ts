import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRecommendationsComponent } from './add-recommendations.component';

describe('AddRecommendationsComponent', () => {
  let component: AddRecommendationsComponent;
  let fixture: ComponentFixture<AddRecommendationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRecommendationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
