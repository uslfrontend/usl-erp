import { Component, OnInit } from '@angular/core';
import { HrisApiService } from '../../../../hris-api.service';
import { GlobalService } from '../../../../global.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-end-of-serivce-batch',
  templateUrl: './end-of-serivce-batch.component.html',
  styleUrls: ['./end-of-serivce-batch.component.scss']
})
export class EndOfSerivceBatchComponent implements OnInit {

  constructor(

    private hrisApi: HrisApiService,
    public global: GlobalService,
    private FormBuilder: FormBuilder) {
    this.configList = {
      itemsPerPage: 15,
      currentPage: 1,
      totalItems: this.ctr
    };

  }

  idNumber: string = '';
  name: string = '';
  department: string = '';
  position: string = '';
  eosType: string = '';
  eosDate: string = '';
  remarks: string = '';
  date
  form: FormGroup;

  masterlistArray = []
  filteredList = []
  tempArray = []
  eosTypeArr = []

  configList: any
  ctr = 0

  ngOnInit() {
    this.form = this.FormBuilder.group({
      selectedEOSType: ['', Validators.required],
      eDate: ['', Validators.required],
    });

    this.getMasterList()
    this.getEOSType()
  }

  getEOSType() {
    this.global.swalLoading('Loading resources')
    this.hrisApi.getHRISMaintenanceEOSType().map(response => response.json()).subscribe(res => {
      this.eosTypeArr = res.data

      this.global.swalClose();
    }, Error => {
      this.global.swalAlertError();
    });

  }

  getMasterList() {
    this.hrisApi.getReportActiveEmployeesMasterList().subscribe(response => {
      const res = response.json();
      this.masterlistArray = res.data;
      this.global.swalClose();

      // Transform the data

      this.filteredList = this.masterlistArray.map(employee => {

        const formattedEmployee = {
          Idnumber: employee.idnumber,
          Name: `${employee.lastname}, ${employee.firstname} ${employee.middlename}`,
          Department: employee.departmentName,
          Position: employee.position
        };

        return formattedEmployee;
      });

      for (var x = 0; x < this.filteredList.length; x++) {

        this.filteredList[x].eosType = ''
        this.filteredList[x].eosDate = ''
        this.filteredList[x].remarks = ''
      }

      this.tempArray = this.filteredList

    });
  }

  pageChangedList(event) {
    this.configList.currentPage = event;
  }

  filterClear() {

    this.idNumber = ''
    this.name = ''
    this.department = ''
    this.position = ''
    this.eosType = ''
    this.eosDate = ''
    this.remarks = ''
  }
2
  changeDate(idNumber, dateValue, department, position) {

    const date = new Date(dateValue);
    const offsetMs = date.getTimezoneOffset() * 60 * 1000;
    const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
    const formattedDate = localDate.toISOString()
    this.date = formattedDate

    for (var x = 0; x < this.filteredList.length; x++) {
      if (this.filteredList[x].Idnumber == idNumber && this.filteredList[x].Department == department && this.filteredList[x].Position == position) {
        this.filteredList[x].eosDate = this.date

      }
    }
  }

  filterall(event: any): void {

    this.filteredList = this.tempArray.filter(employee => {

      return (
        (this.idNumber === '' || (employee.Idnumber && employee.Idnumber.toLowerCase().includes(this.idNumber.toLowerCase()))) &&
        (this.name === '' || (employee.Name && employee.Name.toLowerCase().includes(this.name.toLowerCase()))) &&
        (this.department === '' || (employee.Department && employee.Department.toLowerCase().includes(this.department.toLowerCase()))) &&
        (this.position === '' || (employee.Position && employee.Position.toLowerCase().includes(this.position.toLowerCase()))) &&
        (this.eosType === '' || (employee.eosType && employee.eosType.toLowerCase().includes(this.eosType.toLowerCase()))) &&
        (this.eosDate === '' || (employee.eosDate && employee.eosDate.toLowerCase().includes(this.eosDate.toLowerCase()))) &&
        (this.remarks === '' || (employee.remarks && employee.remarks.toLowerCase().includes(this.remarks.toLowerCase())))

      );
    });

  }

  removeEmployee(id: number, employeeName: string, department: string, position: string): void {
    Swal.fire({
      title: 'Are you sure?',
      text: `Do you want to remove ${employeeName} with position ${position}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.filteredList = this.filteredList.filter(employee => {
          // Check if both idNumber and position match
          return !(employee.Idnumber === id && employee.Department === department && employee.Position === position);
        });

        Swal.fire(
          'Removed!',
          'Employee has been removed.',
          'success'
        )
      }
    })
  }

  removeEmp(id: number, department: string, position: string): void {
    // console.log( department);

    this.filteredList = this.filteredList.filter(employee => {
      // Check if both idNumber and position match
      return !(employee.Idnumber === id && employee.Department === department && employee.Position === position);
    });
  }

  save() {
    let isValid = true;

    // Check each employee in the filteredList
    this.filteredList.forEach((employee) => {
      if (
        employee.eosType === null ||
        employee.eosType === '' ||
        employee.eosDate === null ||
        employee.eosDate === ''
      ) {
        isValid = false;
        return; // Exit the loop if any employee has invalid data
      }
    });

    if (!isValid) {
      // Show an error message or take appropriate action for invalid input
      Swal.fire({
        title: 'Error',
        text: 'Please fill in the required fields (EOS Type and EOS Date) for all employees.',
        type: 'error'
      });
      return; // Exit the function
    }

    // If all employees have valid data, proceed with the rest of the code
    Swal.fire({
      title: 'Are you sure?',
      text: 'Appointment/s will be deactivated.\nStatus will become Inactive.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, save it!'
    }).then((result) => {
      if (result.value) {
        this.filteredList.forEach((employee) => {
          this.hrisApi.postEmployeeEOS(
            employee.Idnumber,
            employee.eosType,
            employee.eosDate,
            employee.remarks
          ).map((response) => response.json()).subscribe(
            (res) => {

              this.removeEmp(employee.Idnumber, employee.Department, employee.Position)
            },
            (Error) => {
              this.global.swalAlertError();

            }
          );
        });
        // this.global.swalLoading('Adding End of Service');
        Swal.fire(
          'Created!',
          'End of Service created successfully.',
          'success'
        );
      }
    });
  }

  individualSave(Idnumber, eosType, eosDate, remarks, department, position) {

    if (eosType && eosDate) {

      Swal.fire({
        title: 'Are you sure?',
        text: 'Appointment/s will be deactivated.\nStatus will become Inactive.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, save it!'
      }).then((result) => {
        if (result.value) {
          this.hrisApi.postEmployeeEOS(Idnumber, eosType, eosDate, remarks)
            .map((response) => response.json())
            .subscribe(
              (res) => {

                this.removeEmp(Idnumber, department, position);
              },
              (Error) => {
                this.global.swalAlertError();
              }
            );

          // this.global.swalLoading('Adding End of Service');
          Swal.fire(
            'Created!',
            'End of Service created successfully.',
            'success'
          );
        }
      });

    } else {

      Swal.fire({
        title: 'Error',
        text: 'Please fill in the required fields (EOS Type and EOS Date).',
        type: 'error'
      });
    }
  }

}
