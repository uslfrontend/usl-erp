import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';


@Component({
	selector: 'app-contracts',
	templateUrl: './contracts.component.html',
	styleUrls: ['./contracts.component.css']
})
export class ContractsComponent implements OnInit {

	classificationsArr;
	selectedClass;
	sdate;
	edate;
	active = 0;
	targetupdateID;
	form: FormGroup;
	description;

	constructor(public dialogRef: MatDialogRef<ContractsComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public dialog: MatDialog,
		private global: GlobalService,
		private http: Http,
		private fb: FormBuilder,
		private HrisApi: HrisApiService,
		private FormBuilder: FormBuilder,
		private httpC: HttpClient,) {

		this.global.swalLoading('Loading appointment resources')
		this.HrisApi.getHRISMaintenanceClassifications()
			// this.http.get(this.global.api+'HRISMaintenance/Classifications',this.global.option)
			.map(response => response.json())
			.subscribe(res => {
				this.classificationsArr = res.data;

				this.global.swalClose();
			}, Error => {
				this.global.swalAlertError();
			});
	}

	ngOnInit() {

		this.form = this.FormBuilder.group({
			selectedClass: ['', Validators.required],
			sdate: ['', Validators.required],
			edate: ['', Validators.required],

		});

		if (this.data.type == "update") {

			this.targetupdateID = this.data.selectedData.contractid;
			var sd = new Date(this.data.selectedData.startdate);
			var ed = new Date(this.data.selectedData.enddate);
			this.sdate = sd;
			this.edate = ed;
			this.selectedClass = this.data.selectedData.termsOfContract
			//console.log(this.selectedClass);
			this.description = this.data.selectedData.description;
			if (this.data.selectedData.active == "False")
				this.active = 0;
			else
				this.active = 1;

			this.form.get('sdate').patchValue(this.sdate);
			this.form.get('edate').patchValue(this.edate);
			this.form.get('selectedClass').patchValue(this.selectedClass);

		}
	}

	checked() {
		if (this.active == 0) {
			this.active = 1
		}
		else {
			this.active = 0
		}
	}

	onNoClickclose(): void {
		this.dialogRef.close({ result: 'cancel' });
	}


	save(): void {
		// console.log(this.form.get('sdate').value,this.form.get('edate').value,this.form.get('selectedClass').value)

		this.form.markAllAsTouched();

		if (this.form.get('selectedClass').value != '' &&
			this.form.get('sdate').value != '' &&
			this.form.get('edate').value != '' ) {

			if (this.form.get('selectedClass').value == undefined) {
				this.global.swalAlert('Select Contract', '', 'warning')
				// }else if(this.description == undefined){
				// 	this.global.swalAlert('Select ','','warning')
			} else if (this.form.get('sdate').value >this.form.get('edate').value ||this.form.get('sdate').value == undefined && this.form.get('edate').value == undefined) {
				this.global.swalAlert('Invalid Date Range', '', 'warning')
			} else {
				let sdate = new Date(this.form.get('sdate').value).toLocaleString();
				let edate = new Date(this.form.get('edate').value).toLocaleString();

				this.form.get('sdate').patchValue(sdate);
				this.form.get('edate').patchValue(edate);

				if (this.data.type == "update") {
					// console.log('sdate:'+this.sdate+"--"+'edate:'+this.edate);
					this.global.swalLoading('Updating Contract');
					this.HrisApi.putEmployeeContract(this.targetupdateID, {
						"sdate": this.form.get('sdate').value,
						"edate": this.form.get('edate').value,
						"classification": this.form.get('selectedClass').value,
						"description": this.description,
						"active": this.active,
					})
						// this.http.put(this.global.api + 'Employee/Contract/' + , this.global.option)
						.map(response => response.json())
						.subscribe(res => {
							// console.log(res)
							//this.global.swalClose();
							this.global.swalAlert("Contract updated successfully.", "", 'success');
							//   this.dialogRef.close({result:"Update success"});

						}, Error => {
							this.global.swalAlertError();
							console.log(Error)
						});

				} else {

					this.global.swalLoading('Adding Contract');
					this.HrisApi.postEmployeeContract(this.data.selectedID, {
						"sdate": this.form.get('sdate').value,
						"edate": this.form.get('edate').value,
						"classification": this.form.get('selectedClass').value,
						"description": this.description,
					})
						// this.http.post(this.global.api + 'Employee/Contract/' + , this.global.option)
						.map(response => response.json())
						.subscribe(res => {
							// console.log(res)
							this.global.swalClose();
							this.global.swalAlert('Contract created successfully.', "", 'success');
							//   this.dialogRef.close({result:"Adding Success"});

						}, Error => {
							this.global.swalAlertError();
							console.log(Error)
						});

				}

			}



		}

	}
	}
