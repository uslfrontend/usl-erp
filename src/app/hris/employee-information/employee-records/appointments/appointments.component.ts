import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';
import { ApiService } from '../../../../api.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {

  selectedDept
  selecteditem
  employeeType
  selectedDepartment
  selectedPosition
  status

  deptArr = []
  newDeptArr = []
  itemArr = []
  tempArr = []

  targetupdateID
  itemid
  dateappointed

  sdate
  edate
  active
  statusid

  tempVar
  rank
  empType
  empCat

  main

  checker: boolean = false

  activeStatus: boolean = false

  form: FormGroup;

  departmentArr: string[] = []
  deparmentValue = new FormControl('', [Validators.required]);
  deparmentOptions: Observable<string[]>;

  posArr: string[] = []
  posValue = new FormControl('', [Validators.required]);
  posOptions: Observable<string[]>;


  appointmentDate: any
  startDate: any
  endDate: any


  constructor(public dialogRef: MatDialogRef<AppointmentsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private FormBuilder: FormBuilder,
    private HrisApi: HrisApiService,
    private api: ApiService,
    private httpC: HttpClient,) {

    this.global.swalLoading('Loading appointment resources')
    this.HrisApi.getHRISMaintenanceDepartment()
      // this.http.get(this.global.api+'HRISMaintenance/Department',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.deptArr = res.data;
        // console.log(res.data)
        for (var i = 0; i < this.deptArr.length; ++i) {
          this.departmentArr.push(this.deptArr[i].departmentName)
        }

        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError();
      });
  }

  private _filter1(value: string): string[] {
    const filterValue1 = typeof value === 'string' ? value.toLowerCase() : '';
    return this.posArr.filter(option => option.toLowerCase().includes(filterValue1));
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.departmentArr.filter(option => option.toLowerCase().includes(filterValue));
  }


  ngOnInit() {

    this.tempArr = this.data.array

    this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value || ' ')));

    this.posOptions = this.posValue.valueChanges.pipe(startWith(''), map(value => this._filter1(value)));

    this.getEmpCategory()
    this.getMatOptionEmpTypeItems()

    this.form = this.FormBuilder.group({
      selectedDept: ['', Validators.required],
      selecteditem: ['', Validators.required],
      dateappointed: ['', Validators.required],
      sdate: ['', Validators.required],
      edate: ['', Validators.required],
      disabledSdate: [{ value: null, disabled: true }, Validators.required],
      disabledEdate: [{ value: null, disabled: true }, Validators.required],
      empTypeID: ['', Validators.required],
      empCatID: ['', Validators.required],

    });

    this.statusid = 1

    if (this.data.arraylength == '0') {
      this.main = this.data.main
      this.statusid = this.data.main
      this.activeStatus = true
    } else {
      this.main = 0
      this.activeStatus = false
    }

    if (this.data.type == "update") {

      this.form.get('selectedDept').patchValue(this.data.selectedData.departmentID);

      this.selectedDept = this.data.selectedData.departmentID;

      this.getItem();

      this.selecteditem = this.data.selectedData.itemID
      // console.log(this.selectedDept)

      //  console.log( this.selectedDept+'-'+this.selecteditem)
      this.targetupdateID = this.data.selectedData.appointmentID;
      this.itemid = this.data.selectedData.itemID;
      this.statusid = this.data.selectedData.hasEffectivity;
      this.main = this.data.selectedData.main
      this.startDate = new Date(this.data.selectedData.startdate);
      this.endDate = new Date(this.data.selectedData.enddate);
      this.appointmentDate = new Date(this.data.selectedData.dateAppointed);
      // this.sdate = sd;
      // this.edate = ed;
      // this.dateappointed = da;
      this.active = this.data.selectedData.active;

      this.getDepartment()

      this.selecteditem = this.data.selectedData.position
      this.posValue.setValue(this.selecteditem);
      this.form.get('selecteditem').patchValue(this.selecteditem);
      this.form.get('dateappointed').patchValue(this.dateappointed);
      this.form.get('sdate').patchValue(this.sdate);
      this.form.get('edate').patchValue(this.edate);
      this.form.get('empTypeID').patchValue(this.data.selectedData.appointmentTypeID)
      this.form.get('empCatID').patchValue(this.data.selectedData.appointmentCategoryID)

      for (var x = 0; x < this.data.array.length; x++) {
        if (this.data.array[x].main == 1) {

          if (this.data.array[x].appointmentID == this.data.selectedData.appointmentID) {
            this.checker = false

          } else {

            this.checker = true
          }
          return
        } else {
          this.checker = false

        }
      }
    }

    if (this.data.type == "add") {

      for (var x = 0; x < this.data.array.length; x++) {
        if (this.data.array[x].main == 1) {
          this.checker = true
        }
      }
    }

  }

  fillInDepartment() {
    this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter(value || ' ')));
  }


  onDateChange(event: MatDatepickerInputEvent<Date>) {
    // const selectedDate = event.value;
    // const sixMonthsLater = new Date(selectedDate);
    // sixMonthsLater.setMonth(sixMonthsLater.getMonth() + 6);

    // const date = new Date(sixMonthsLater);
    // const offsetMs = date.getTimezoneOffset() * 60 * 1000;
    // const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
    // const formattedDate = localDate.toISOString()

    // this.form.get('edate').patchValue(formattedDate)
  }

  getDepartment() {
    this.HrisApi.getHRISMaintenanceDepartment()
      // this.http.get(this.global.api+'HRISMaintenance/Department',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.newDeptArr = res.data;
        for (var x = 0; x < this.newDeptArr.length; x++) {
          if (this.data.selectedData.departmentID == this.newDeptArr[x].departmentID) {
            this.selectedDept = this.newDeptArr[x].departmentName
            this.deparmentValue.setValue(this.selectedDept);
          }
        }
        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError();
      });
  }


  checked() {
    if (this.statusid == 0) {

      this.statusid = 1
    }
    else {
      this.statusid = 0
    }

  }

  checkMain() {
    if (this.main == 0) {

      this.main = 1
    }
    else {
      this.main = 0
    }
  }

  checkActive() {
    if (this.active == 0) {

      this.active = 1
    }
    else {
      this.active = 0
    }
    // console.log(this.active)
  }


  getEmpCategory() {
    this.empCat = []
    this.HrisApi.getHRISMaintenanceAppointmentCategories()
      .map(response => response.json())
      .subscribe(res => {
        this.empCat = res.data;
        // console.log(res.data)
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getMatOptionEmpTypeItems() {
    this.empType = [];
    this.api.getHRISMaintenanceEmpType()
      .map(response => response.json())
      .subscribe(res => {
        this.empType = res.data;
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getItem() {
    // console.log(this.posValue.value)

    this.posValue.setValue('')
    this.tempVar = []
    this.posArr = []

    this.tempVar = this.deparmentValue.value
    this.form.get('selectedDept').patchValue(this.deparmentValue.value)

    // console.log(this.form.get('selectedDept').value)

    for (var x = 0; x < this.deptArr.length; x++) {
      if (this.form.get('selectedDept').value == this.deptArr[x].departmentName) {
        this.selectedDept = this.deptArr[x].departmentID
        this.form.get('selectedDept').patchValue(this.selectedDept)
        // console.log('tempVar', this.selectedDept)
      }
    }
    if (this.form.get('selectedDept').value == '') {
      this.form.get('selectedDept').patchValue(this.selectedDept)

    }
    // console.log(this.form.get('selectedDept').value)
    this.HrisApi.getHRISMaintenanceItems(this.form.get('selectedDept').value, this.data.selectedID)
      // this.http.get(this.global.api+'HRISMaintenance/Items/?departmentId='+item+'&employeeId='+this.data.selectedID,this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.itemArr = []
        this.posArr = []
        this.itemArr = res.data;

        for (var i = 0; i < this.itemArr.length; ++i) {
          this.posArr.push(this.itemArr[i].fullposition)

        }

        this.posOptions = this.posValue.valueChanges.pipe(startWith(''), map(value => this._filter1(value)));
        // console.log(this.posArr)
        // console.log(this.posArr)
      }, Error => {
        this.global.swalAlertError();
      });

    this.form.get('selectedDept').patchValue(this.tempVar);
    this.deparmentValue.setValue(this.tempVar)
    this.selecteditem = ''

  }

  onNoClickclose(): void {
    this.dialogRef.close({});
  }

  handleDateChange(event: MatDatepickerInputEvent<Date>) {
    // console.log(this.appointmentDate)
    this.startDate = this.appointmentDate
    // const selectedDate: Date = event.value;
    // const date = new Date(selectedDate);
    // const offsetMs = date.getTimezoneOffset() * 60 * 1000;
    // const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
    // const formattedDate = localDate.toISOString()
    // this.form.get('sdate').patchValue(formattedDate)
    // this.form.get('dateappointed').patchValue(formattedDate);
    // // console.log(event.value)
  }

  eDate(event: MatDatepickerInputEvent<Date>) {
    if (this.form.get('edate').value != 'Invalid Date') {
      const selectedDate: Date = event.value;
      const date = new Date(selectedDate);
      const offsetMs = date.getTimezoneOffset() * 60 * 1000;
      const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
      const formattedDate = localDate.toISOString()
      this.form.get('edate').patchValue(formattedDate)
    }

  }

  sDate(event: MatDatepickerInputEvent<Date>) {
    if (this.form.get('sdate').value != 'Invalid Date') {
      const selectedDate: Date = event.value;
      const date = new Date(selectedDate);
      const offsetMs = date.getTimezoneOffset() * 60 * 1000;
      const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
      const formattedDate = localDate.toISOString()
      this.form.get('sdate').patchValue(formattedDate)
    }
  }

  dateAppointed() {
    if (this.form.get('dateappointed').value != 'Invalid Date') {
      const date = new Date(this.form.get('sdate').value);
      const offsetMs = date.getTimezoneOffset() * 60 * 1000;
      const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
      const formattedDate = localDate.toISOString()
      this.form.get('dateappointed').patchValue(formattedDate)
    }
  }

  formatTime(inputTime) {
    if (inputTime != 'Invalid Date') {
      const date = new Date(inputTime);
      date.setDate(date.getDate());
      date.setHours(date.getHours() + 8); // Adjust for the Philippines Time (GMT+8)
      const formattedTime = date.toISOString();

      return formattedTime;
    }

  }




  save(): void {

    this.posValue.markAllAsTouched();
    this.deparmentValue.markAllAsTouched();
    this.form.markAllAsTouched();

    if (this.deparmentValue.value != '' &&
      this.posValue.value != '' &&
      this.form.get('dateappointed').value != ''
    ) {


      if (this.form.get('sdate').value >= this.form.get('edate').value) {
        this.global.swalAlert('Invalid Date Range!', 'The start date cannot be later than or equal to the end date. Please make sure the start date comes before the end date.', 'warning');
        return
      }

      else {
        this.selecteditem = this.posValue.value

        for (var x = 0; x < this.itemArr.length; x++) {
          if (this.selecteditem == this.itemArr[x].fullposition)
            this.selecteditem = this.itemArr[x].itemid

          this.posValue.setValue(this.selecteditem)
        }

        if (this.data.type == "update") {

          if (this.form.get('sdate').value >= this.form.get('edate').value) {
            this.global.swalAlert('Invalid Date Range!', 'The start date cannot be later than or equal to the end date. Please make sure the start date comes before the end date.', 'warning');
            return
          } else {

            this.global.swalLoading('Updating Appointment');
            this.HrisApi.putEmployeeAppointment(this.targetupdateID, {
              "itemid": this.posValue.value,
              "dateappointed": this.formatTime(this.appointmentDate),
              "expiring": this.statusid,
              "sdate": this.formatTime(this.startDate),
              "edate": this.formatTime(this.endDate),
              "active": this.active,
              "appointmentTypeID": this.form.get('empTypeID').value,
              "appointmentCategoryID": this.form.get('empCatID').value,
              "main": this.main
            }
            )
              // this.http.put(this.global.api+'Employee/Appointment/'+,this.global.option)
              .map(response => response.json())
              .subscribe(res => {
                // console.log(res)
                this.global.swalClose();
                this.global.swalAlert("Appointment updated successfully.", "", 'success');
                this.dialogRef.close({ result: "Update success" });

              }, Error => {
                this.global.swalAlertError();
                // console.log(Error)
              });
          }

        } else {

          if (this.statusid == 0) {
            this.sdate = this.form.get('dateappointed').value;
            this.edate = "";
          }

          this.global.swalLoading('Adding Appointment');
          this.HrisApi.postEmployeeAppointment(this.data.selectedID, {
            "itemid": this.posValue.value,
            "dateappointed": this.formatTime(this.appointmentDate),
            "expiring": this.statusid,
            "sdate": this.formatTime(this.startDate),
            "edate": this.formatTime(this.endDate),
            "appointmentTypeID": this.form.get('empTypeID').value,
            "appointmentCategoryID": this.form.get('empCatID').value,
            "main": this.main
          })
            // this.http.post(this.global.api + 'Employee/Appointment/' + , this.global.option)
            .map(response => response.json())
            .subscribe(res => {
              // console.log(res)
              this.global.swalClose();
              this.global.swalAlert("Appointment created successfully.", '', 'success');
              this.dialogRef.close({ result: "Adding Success" });

            }, Error => {
              this.global.swalAlertError();
              // console.log(Error)
            });

          // console.log({
          //   "itemid": this.posValue.value,
          //   "dateappointed": this.form.get('dateappointed').value,
          //   "expiring": this.statusid,
          //   "sdate": this.form.get('sdate').value,
          //   "edate": this.form.get('edate').value,
          //   "appointmentTypeID": this.form.get('empTypeID').value,
          //   "appointmentCategoryID": this.form.get('empCatID').value,
          //   "main": this.main
          // })
        }
      }
    }
    // this.dateAppointed()
  }

}

