import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HrisApiService } from '../../../../hris-api.service';

@Component({
  selector: 'app-add-com-ext',
  templateUrl: './add-com-ext.component.html',
  styleUrls: ['./add-com-ext.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AddComExtComponent implements OnInit {

  activity
  date
  location
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddComExtComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private HrisApi: HrisApiService,
    private FormBuilder: FormBuilder,
    private http: Http
  ) {

  }

  ngOnInit() {

    this.form = this.FormBuilder.group({
      activity: ['', Validators.required],
      date: ['', Validators.required],
      location: ['', Validators.required],
    });

  }
  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save(): void {
    
    this.form.markAllAsTouched();
    if( this.form.get('activity').value != '' && this.form.get('date').value != '' && this.form.get('location').value != ''){
      let date = new Date(this.form.get('date').value).toLocaleString();
      this.global.swalLoading('Adding Community Extension');

      this.HrisApi.postEmployeeCommunityExtension(this.data.selectedID, {
        "activity":  this.form.get('activity').value,
        "dateConducted": date,
        "location": this.form.get('location').value,
      })
        // this.http.post(this.global.api+'Employee/CommunityExtension/'+,this.global.option)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.global.swalAlert('Community Engagement created successfully.', "", 'success');
          this.dialogRef.close({ result: "Adding Success" });
        }, Error => {
          this.global.swalAlertError();
          console.log(Error)
        });
    }
   
    
  }

}
