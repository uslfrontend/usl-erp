import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as ExcelJS from "exceljs/dist/exceljs"
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class HrisExcelServiceService {

  constructor(private datePipe: DatePipe) { }

  async downloadFacultyEvalProgramExcelFormat() {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = ("DTR Uploader")
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A3').value = "DTR Uploader"
    worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A3').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getColumn('A').width = 17;
    worksheet.getColumn('B').width = 25;
    worksheet.mergeCells('A1:B1');
    worksheet.mergeCells('A2:B2');
    worksheet.mergeCells('A3:B3');


    let headerRow = worksheet.addRow(['DTR ID', 'Time Record']);
    this.rowBoldAndOutline(headerRow)

    let dataRow
    for (var x = 0; x < 1000; x++) {
      dataRow = worksheet.addRow(['', '']);
      this.leftRightOutline(dataRow)
      if (x == 199) {
        this.leftRightBottomOutline(dataRow)
      }
      dataRow.eachCell((cell, number) => {
        if (cell._address.includes('')) {
          cell._address = null
          cell.protection = {
            locked: false,
          };
        }
      })
    }


    worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A4').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('B4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('B4').alignment = { vertical: 'top', horizontal: 'center' };

    //make format of the cells as text
    worksheet.getColumn(1).numFmt = '@';
    worksheet.getColumn(2).numFmt = '@';

    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  async downloadFailedToUploadDTR(data) {
    //new excel file
    var workbook = new ExcelJS.Workbook();
    //add sheet
    const worksheet = workbook.addWorksheet();
    var FileName = ("DTR Uploader")
    await worksheet.protect('CiCT#2020')
    let h1 = worksheet.addRow(['University of Saint Louis']);
    h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h1.alignment = { vertical: 'top', horizontal: 'center' };
    let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
    h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    h2.alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('A3').value = "DTR Uploader"
    worksheet.getCell('A3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A3').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getColumn('A').width = 17;
    worksheet.getColumn('B').width = 25;
    worksheet.getColumn('C').width = 25;
    worksheet.mergeCells('A1:C1');
    worksheet.mergeCells('A2:C2');
    worksheet.mergeCells('A3:C3');


    let headerRow = worksheet.addRow(['DTR ID', 'Name', 'Time Record']);
    this.rowBoldAndOutline(headerRow)

    let dataRow
    for (var x = 0; x < data.length; x++) {
      dataRow = worksheet.addRow([data[x][0], data[x][1], data[x][2]]);
      this.leftRightOutline(dataRow)
      if (x == 199) {
        this.leftRightBottomOutline(dataRow)
      }
      dataRow.eachCell((cell, number) => {
        if (cell._address.includes('')) {
          cell._address = null
          cell.protection = {
            locked: false,
          };
        }
      })
    }

    worksheet.getCell('A4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('A4').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('B4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('B4').alignment = { vertical: 'top', horizontal: 'center' };
    worksheet.getCell('C4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
    worksheet.getCell('C4').alignment = { vertical: 'top', horizontal: 'center' };

    //make format of the cells as text
    worksheet.getColumn(1).numFmt = '@';
    worksheet.getColumn(2).numFmt = '@';
    worksheet.getColumn(3).numFmt = '@';

    //download file
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      });
      fs.saveAs(blob, FileName + '.xlsx');
    });
  }

  //make entire rown bold and outlined
  rowBoldAndOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'FFFFFFFF'
        },
        bgColor: {
          argb: 'FFFFFFFF'
        },
      };
      cell.font = {
        color: {
          argb: '00000000',
        },
        bold: true
      }
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire row outlined
  rowOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make the left and right of the cell outlined
  leftRightOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire the bottom of the cell outlined
  leftRightBottomOutline(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.border = {
        left: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        }
      };
    });
    return cellRow
  }

  //make entrire row bold
  rowBold(cellRow) {
    cellRow.eachCell((cell, number) => {
      cell.font = {
        color: {
          argb: '00000000',
        },
        bold: true
      }
    });
    return cellRow
  }

  //make entire rown bold and centered
  rowBoldAndCenter(cellRow) {

  }

}
