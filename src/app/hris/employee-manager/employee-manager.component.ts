import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../global.service';
import { ApiService } from './../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';

import { HrispersonLookupComponent } from '../hris-lookup/hrisperson-lookup/hrisperson-lookup.component';
import { HrisemployeeLookupComponent } from '../hris-lookup/hrisemployee-lookup/hrisemployee-lookup.component';
import { DtridLookupComponent } from './../../hris/employee-manager/dtrid-lookup/dtrid-lookup.component';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

import { EndOfServiceComponent } from '../employee-information/employee-records/end-of-service/end-of-service.component';
import { AppointmentsComponent } from '../employee-information/employee-records/appointments/appointments.component';
import { ContractsComponent } from '../employee-information/employee-records/contracts/contracts.component';
import { RankRecordsComponent } from '../employee-information/employee-records/rank-records/rank-records.component';
import { HrisApiService } from '../../hris-api.service';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-employee-manager',
  templateUrl: './employee-manager.component.html',
  styleUrls: [
    // './../../../../src/assets/bootstrap.min.css',
    './employee-manager.component.css'
  ]
})

export class EmployeeManagerComponent implements OnInit {

  //@ViewChild('dviInner',{static:false}) dviInner:ElementRef;

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private domSanitizer: DomSanitizer, public global: GlobalService, private api: ApiService, private HrisApi: HrisApiService) {
    this.configRooms = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr
    };

    this.configRooms1 = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.ctr1
    };

  }

  /* Variable initialization */
  //#region 
  user;
  employee;
  employeeAll;
  dtrIdAssignedto;
  image: any = 'assets/noimage.jpg';
  id = '';
  fname = '';
  mname = '';
  lname = '';
  suffix = '';
  dob = '';
  rank;
  empStatus;
  empType;
  dtrid;
  datehired;
  lastIssuedDTRID;
  idnumber: any = '';
  form: FormGroup;
  showContent: boolean = false;
  headerShowContent: boolean = false;


  position = '';
  dtridnum = '';

  submitted = false;
  btnSaveDisabled = true;
  btnCancelDisabled = true;
  dtrIdDuplicate = false;
  accountLocked = true;

  ctrRank = 0;
  ctrType = 0;
  ctrDTRId = 0;
  ctrStatus = 0;
  ctrDateHired = 0;
  ctrActive = 0;

  eosArr
  eosTypeArr
  appointmentArr;
  contractArr;
  rankRecordsArr;
  newArr
  recordArr
  rankid

  date
  record
  type
  temp

  main = 1
  value = 0

  tempTypeID
  tempDateHired
  tempRank
  tempStatus
  Status

  configRooms: any
  ctr = 0

  configRooms1: any
  ctr1 = 0

  //#endregion
  ngOnInit() {

    if (this.global.hrisId != '') {
      this.id = this.global.hrisId
      this.keyDownFunction('onoutfocus')
    }

    this.form = this.formBuilder.group({
      rankID: [{ value: null, disabled: true }, Validators.required],
      empTypeID: [{ value: null, disabled: true }, Validators.required],
      dtrid: ['', Validators.required],
      empStatusID: [{ value: null, disabled: true }, Validators.required],
      datehired: [{ disabled: true, value: '' }],
      active: [{ value: null, disabled: true }, Validators.required],
    })

    this.getMatOptionEmpStatusItems();
    this.getMatOptionEmpTypeItems();
    this.getMatOptionRankItems();
    this.getAllEmployee();

  }

  pageChangedRooms(event) {
    this.configRooms.currentPage = event;
  }

  pageChangedRoomsRR(event) {
    this.configRooms1.currentPage = event;
  }


  populateAppointment() {
    console.log('here')
  }

  tabbing = 0;
  getindex(tab) {
    this.tabbing = tab.index;
  }

  checkMain() {
    if (this.main == 0) {

      this.main = 1
    }
    else {
      this.main = 0
    }

  }

  openDialogDTRIDLookUp() {
    const dialogRef1 = this.dialog.open(DtridLookupComponent, {
      width: '600px',
      disableClose: true,
      data: { AllEmployeeData: this.employeeAll },
      autoFocus: false
    });

    dialogRef1.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result != 'cancel') {
          var x = result.result;
          var y: number = parseInt(x) + 1;

          if (y.toString().length <= 4) {
            x = 0 + y.toString();
          } else {
            x = y.toString();
          }
          this.f.dtrid.patchValue(x);
        }
      }
    });
  }

  getEosType() {
    // this.global.swalLoading('Loading resources')
    this.HrisApi.getHRISMaintenanceEOSType().map(response => response.json()).subscribe(res => {
      this.eosTypeArr = res.data

      // this.global.swalClose();
    }, Error => {
      this.global.swalAlertError();
    });
  }

  getEOSRecord() {

    this.HrisApi.getEmployeeEndOfService(this.id).map(response => response.json())
      .subscribe(res => {
        this.eosArr = res.data
        for (var x = 0; x < this.eosArr.length; x++) {
          const year = this.eosArr[x].eosDate.slice(0, 4)
          const month = this.eosArr[x].eosDate.slice(5, 7)
          const day = this.eosArr[x].eosDate.slice(8, 10)
          this.eosArr[x].eosDate = month + '/' + day + '/' + year
        }
      })
  }

  getAllEmployee() {
    this.form.reset();
    //  this.global.swalLoading('Retrieving Module Resources');
    this.api.getEmployeeAll('')
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          //this.employeeAll=[];
          this.employeeAll = res.data;
          this.employeeAll.sort((a, b) => b.dtrid - a.dtrid);
          // products.sort((a,b)=>a.title.rendered > b.title.rendered)                                                         
          //console.log(this.employeeAll); 
        } else {
          this.f.active.setValue('0');
        }
        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }


  onChange(value, elementId) {
    if (this.employee) {
      switch (elementId) {
        case 'rankID': {
          //counter = 1
          if (elementId == 'rankID' && this.employee[0].rankID != value.value) { this.ctrRank = 1 }
          //counter = 0
          if (elementId == 'rankID' && this.employee[0].rankID == value.value) { this.ctrRank = 0 }
          break;
        }
        case 'empTypeID': {
          //counter = 1
          if (elementId == 'empTypeID' && this.employee[0].empTypeID != value.value) { this.ctrType = 1 }
          //counter = 0
          if (elementId == 'empTypeID' && this.employee[0].empTypeID == value.value) { this.ctrType = 0 }
          break;
        }
        case 'dtrid': {
          //console.log(value);
          //counter = 1
          if (elementId == 'dtrid' && this.dtrid != value) { this.ctrDTRId = 1 }
          //counter = 0
          if (elementId == 'dtrid' && this.dtrid == value) { this.ctrDTRId = 0 }
          break;
        }
        case 'empStatusID': {
          //counter = 1
          if (elementId == 'empStatusID' && this.employee[0].empStatusID != value.value) { this.ctrStatus = 1 }
          //counter = 0
          if (elementId == 'empStatusID' && this.employee[0].empStatusID == value.value) { this.ctrStatus = 0 }
          break;
        }
        case 'datehired': {
          //counter = 1
          if (elementId == 'datehired' && this.datehired != value) { this.ctrDateHired = 1 }
          //counter = 0
          if (elementId == 'datehired' && this.datehired == value) { this.ctrDateHired = 0 }
          break;
        }
        case 'active': {
          //counter = 1
          if (elementId == 'active' && this.employee[0].active != value.value) { this.ctrActive = 1 }
          //counter = 0
          if (elementId == 'active' && this.employee[0].active == value.value) { this.ctrActive = 0 }
          break;
        }
      }
      //1 use ||
      if (this.ctrRank == 1 || this.ctrType == 1 || this.ctrDTRId == 1 || this.ctrStatus == 1 || this.ctrDateHired == 1 || this.ctrActive == 1) {
        this.btnSaveDisabled = false;
        this.btnCancelDisabled = false;
      }
      //0 use &&
      if (this.ctrRank == 0 && this.ctrType == 0 && this.ctrDTRId == 0 && this.ctrStatus == 0 && this.ctrDateHired == 0 && this.ctrActive == 0) {
        this.btnSaveDisabled = true;
        this.btnCancelDisabled = true;
      }
    } else {
      if (value != null) {
        this.btnCancelDisabled = false;
        this.btnSaveDisabled = false;
      }
    }
  }

  cancel() {
    this.clear();
    this.getEmployeesHiringBasicInfo();
  }

  update() {

    if (this.f.dtrid.value.length != 5) {
      this.f.dtrid.setErrors({ 'minmaxlength': true });
    }

    let date = new Date(this.f.datehired.value).toLocaleString();
    if (!this.form.errors && !this.form.invalid && !this.f.dtrid.errors) {


      this.api.putEmployee(this.id,
        {
          "dtrid": this.f.dtrid.value,
          "dateHired": date,
          "empTypeID": this.f.empTypeID.value,
          "empStatusID": this.f.empStatusID.value,
          "rankID": this.f.rankID.value,
          "active": this.f.active.value
        }
      )
        .map(response => response.json())
        .subscribe(res => {

          this.global.swalSuccess('Success');
        }, Error => {
          this.global.swalAlertError();
        });
    }


  }

  add() {
    this.getdtrIdandPosition()
    // console.log(this.f.empTypeID.value, this.f.rankID.value,)
    let date = new Date(this.f.datehired.value).toLocaleString();
    // console.log(this.f.active.value)
    this.api.postEmployee(
      {
        "employeeID": this.id.toString(),
        "dtrid": this.f.dtrid.value.toString(),
        "dateHired": date,
        "empTypeID": this.f.empTypeID.value,
        "empStatusID": this.f.empStatusID.value,
        "rankID": this.f.rankID.value,
      }
    )
      .map(response => response.json())
      .subscribe(res => {
        this.global.swalSuccess('Success');
      }, Error => {
        this.global.swalAlertError();
      });
  }

  register() {
    this.api.postRegisterEmployee(
      {
        "username": this.id,
        "password": 'cicm_2019', //employee default password
        "confirmPassword": 'cicm_2019'
      }
    )
      .map(response => response.json())
      .subscribe(res => {
        this.getAccount();
      }, Error => {
        this.global.swalAlertError();
      })
  }

  lockAccount(x) {
    this.HrisApi.putLockUnlockEmployeeAccount(this.id, x)
      .map(response => response.json())
      .subscribe(res => {
        this.getAccount();
        if (x == 1) {
          this.accountLocked = true;
        }
        else {
          this.accountLocked = false;
        }
      }, Error => {
        this.global.swalAlertError();
      })
  }

  updateBHI() {
    this.api.getEmployee(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.employee = res.data
        this.dtrid = this.employee[0].dtrid;
        this.datehired = this.employee[0].datehired;

        this.f.rankID.setValue(this.employee[0].rankID);
        this.f.empTypeID.setValue(this.employee[0].empTypeID);
        this.f.dtrid.setValue(this.dtrid);
        this.f.empStatusID.setValue(this.employee[0].empStatusID);
        this.f.datehired.setValue(this.datehired);
        this.f.active.setValue(this.employee[0].active.toString());
      })
  }

  save() {

    Swal.fire({
      title: "Are you sure?",
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'

    }).then((result) => {
      if (result.value) {

        this.submitted = true;
        if (this.f.datehired.value == null) {
          this.f.datehired.setErrors({ 'required': true })
        }
        else {
          this.f.datehired.setErrors({ 'required': false })
        }
        if (this.f.dtrid.value != null) {
          if (this.f.dtrid.value.length != 5) {
            this.f.dtrid.setErrors({ 'minmaxlength': true });
          }
        }

        this.api.getEmployeeByDTRID(this.f.dtrid.value)
          .map(response => response.json())
          .subscribe(res => {
            if (res.data != null) {
              if ((this.id.trimLeft()).trimRight() != (res.data[0].idnumber.trimLeft().trimRight())) {
                this.dtrIdAssignedto = res.data[0].fullname;
                this.f.dtrid.setErrors({ 'duplicate': true })
              }
            }
            if (!this.form.errors && !this.form.invalid && this.f.datehired.value != null && !this.f.dtrid.errors) {
              //save
              if (this.employee) {
                //update
                this.update();
                //if this.user==null and active==1 register user 
                //if this.user!=null and active=0 lock account
                //if this.user!=null and active=1 unlock account
                if (this.user == null && this.f.active.value == '1') {
                  this.register();
                  //console.log('Account Created');
                }
                if (this.user != null && this.f.active.value == '0') {
                  this.lockAccount(1);
                  //console.log('Account Locked');
                }
                if (this.user != null && this.f.active.value == '1') {
                  this.lockAccount(0);
                  //console.log('Account Unlocked');
                }
              } else {
                //add employee hiring basic info
                this.add();
                //if this.user==null register user  
                if (this.user == null) {
                  this.register();
                }
              }

              this.getEmployeesHiringBasicInfo()
            }
          }, Error => {
            this.global.swalAlertError();
          })

      }
    })



  }
  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  assignFormValue() {

    this.date = ''
    this.record = ''

    if (this.appointmentArr.length != 0 && this.appointmentArr == undefined && this.employee[0].empTypeID == 0) {

      // for (let i = 0; i < this.employee.length; i++) {

      //   const dateString = this.employee[i].datehired;
      //   const date: Date = new Date(dateString);
      //   const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
      //   const formattedDate: string = date.toLocaleDateString('en-US', options);

      //   this.newArr.push({

      //     dateAppointed: formattedDate,
      //     hasEffectivity: this.employee[i].active,
      //     appointmentTypeID: this.employee[i].empTypeID,
      //   });
      // }

      const oldestAppointment = this.newArr.reduce((oldest, current) => {
        const oldestDate = new Date(oldest.dateAppointed);
        const currentDate = new Date(current.dateAppointed);
        return currentDate < oldestDate ? current : oldest;
      }, this.newArr[0]);

      // console.log(oldestAppointment)
      const date = new Date(oldestAppointment.dateAppointed);
      const offsetMs = date.getTimezoneOffset() * 60 * 1000;
      const localDate = new Date(date.getTime() - offsetMs + (8 * 60 * 60 * 1000));
      const formattedDate = localDate.toISOString()

      this.date = formattedDate
      this.record = oldestAppointment.appointmentTypeID

      this.dtrid = this.employee[0].dtrid;

      this.api.putEmployee(this.id,
        {
          "dtrid": this.dtrid,
          "dateHired": this.date,
          "empTypeID": this.record,
          "empStatusID": this.employee[0].empStatusID,
          "rankID": this.employee[0].rankID,
          "active": this.employee[0].active
        }
      )
        .map(response => response.json())
        .subscribe(res => {
          this.api.getEmployee(this.id)
            .map(response => response.json())
            .subscribe(res => {
              this.employee = res.data
              this.dtrid = this.employee[0].dtrid;
              this.datehired = this.employee[0].datehired;

              this.f.rankID.setValue(this.employee[0].rankID);
              this.f.empTypeID.setValue(this.employee[0].empTypeID);
              this.f.dtrid.setValue(this.dtrid);
              this.f.empStatusID.setValue(this.employee[0].empStatusID);
              this.f.datehired.setValue(this.datehired);
              this.f.active.setValue(this.employee[0].active.toString());
            })
        }, Error => {
          this.global.swalAlertError();
        });

    }
    // console.log(this.recordArr)
    if (this.recordArr.length != 0 && this.recordArr != undefined && this.recordArr != null && this.employee[0].empStatusID == 0 && this.employee[0].rankID == 0) {

      const oldestRankRecord = this.recordArr.reduce((oldest, current) => {
        const oldestDate = oldest.sy
        const currentDate = current.sy
        return currentDate < oldestDate ? current : oldest;
      }, this.recordArr[0]);

      if (this.rankRecordsArr != undefined) {
        this.Status = parseInt(oldestRankRecord.empStatusID)
      }

      // this.f.empStatusID.setValue(empstatus);

      for (var x = 0; x < this.rank.length; x++) {
        if (oldestRankRecord.rank == this.rank[x].rank) {
          this.rankid = this.rank[x].rankid
        }
      }

      this.datehired = this.employee[0].datehired;
      this.type = this.employee[0].empTypeID

      this.api.putEmployee(this.id,
        {
          "dtrid": this.dtrid,
          "dateHired": this.datehired,
          "empTypeID": this.type,
          "empStatusID": this.Status,
          "rankID": this.rankid,
          "active": this.employee[0].active
        }
      )
        .map(response => response.json())
        .subscribe(res => {

          this.api.getEmployee(this.id)
            .map(response => response.json())
            .subscribe(res => {
              this.employee = res.data
              this.dtrid = this.employee[0].dtrid;
              this.datehired = this.employee[0].datehired;

              this.f.rankID.setValue(this.employee[0].rankID);
              this.f.empTypeID.setValue(this.employee[0].empTypeID);
              this.f.dtrid.setValue(this.dtrid);
              this.f.empStatusID.setValue(this.employee[0].empStatusID);
              this.f.datehired.setValue(this.datehired);
              this.f.active.setValue(this.employee[0].active.toString());
            })


        }, Error => {
          this.global.swalAlertError();
        });
    }

    this.dtrid = this.employee[0].dtrid;
    this.datehired = this.employee[0].datehired;

    this.f.rankID.setValue(this.employee[0].rankID);
    this.f.empTypeID.setValue(this.employee[0].empTypeID);
    this.f.dtrid.setValue(this.dtrid);
    this.f.empStatusID.setValue(this.employee[0].empStatusID);
    this.f.datehired.setValue(this.datehired);
    this.f.active.setValue(this.employee[0].active.toString());


  }

  clear() {
    this.dtrIdAssignedto = '';
    this.employee = undefined;
    this.user = undefined;
    this.dtrid = '';
    this.datehired = '';
    this.form.reset();
    this.submitted = false;
    this.btnSaveDisabled = true;
    this.btnCancelDisabled = true;
    this.dtrIdDuplicate = false;
    this.ctrRank = 0;
    this.ctrType = 0;
    this.ctrDTRId = 0;
    this.ctrStatus = 0;
    this.ctrDateHired = 0;
    this.ctrActive = 0;

  }
  clear1() {
    this.fname = ''
    this.lname = ''
    this.mname = ''
    this.dtridnum = ''
    this.position = ''
  }

  personlookup(): void {

    const dialogRef = this.dialog.open(HrispersonLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result.trim();
        this.keyDownFunction('onoutfocus')
      }
    });

  }

  employeelookup(): void {

    const dialogRef = this.dialog.open(HrisemployeeLookupComponent, {
      width: '600px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        this.id = result.result.trim();
        this.keyDownFunction('onoutfocus')
      }
    });

  }

  getMatOptionEmpStatusItems() {
    this.empStatus = [];
    this.api.getHRISMaintenanceEmpStatus()
      .map(response => response.json())
      .subscribe(res => {
        this.empStatus = res.data;
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getMatOptionEmpTypeItems() {
    this.empType = [];
    this.api.getHRISMaintenanceEmpType()
      .map(response => response.json())
      .subscribe(res => {
        this.empType = res.data;
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getMatOptionRankItems() {
    this.rank = [];
    this.api.getHRISMaintenanceRank()
      .map(response => response.json())
      .subscribe(res => {
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].active) {
            this.rank.push(res.data[i]);
          }
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getdtrIdandPosition() {
    this.position = ''
    this.dtridnum = ''
    this.idnumber = ''
    // this.global.swalLoading('Loading Person Information');
    this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
      // console.log(res);
      // this.global.swalClose();
      if (res.message != "IDNumber does not exist.") {
        // this.name = res.data[0].fullname;
        this.position = res.data[0].position;
        this.idnumber = res.data[0].idnumber;
        // console.log(this.idnumber)
        this.dtridnum = "DTR ID#: " + res.data[0].dtrid;
      }
    })
  }

  getEmployeesHiringBasicInfo() {
    //this.employee=undefined;   

    this.getdtrIdandPosition()
    this.form.reset();
    // this.global.swalLoading('Retrieving Employee Informations');
    this.api.getEmployee(this.id)
      .map(response => response.json())
      .subscribe(res => {

        if (res.data != null) {
          this.employee = [];
          this.employee = res.data;
          this.assignFormValue();

        } else {
          // console.log('123')

          this.getAppointment();
          this.getContract();
          this.getRankRecords();
          this.getEOSRecord();
          this.getEosType();
          this.getAllEmployee();

          this.f.active.setValue('1');
          this.f.rankID.setValue(0);
          this.f.empTypeID.setValue(0);
          this.f.empStatusID.setValue(0);

        }
        // this.global.swalClose();
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getPersonIDInfo() {
    this.api.getPersonIDInfo(this.id)
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          this.image = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + res.data.idPicture);
        }
        else {
          this.image = ''
        }
      }, Error => {
        this.global.swalAlertError(Error);
      })
  }

  getAccount() {
    this.accountLocked = true;

    this.HrisApi.getAccountUser(this.id, '0')
      .map(response => response.json())
      .subscribe(res => {
        if (res.data != null) {
          this.user = [];
          this.user = res.data;
          if (res.data.lockoutEnabled == 0) {
            this.accountLocked = false;
          }
        }
      }, Error => {
        this.global.swalAlertError();
      })
  }

  keyDownFunction(event) {
    this.id.trim()
    this.temp = ''

    if (event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
      this.global.hrisId = this.id
      this.global.swalLoading('Loading Person Information');

      if (this.id != '') {
        this.HrisApi.getEmployee(this.id).map(response => response.json()).subscribe(res => {
          this.temp = res.data
          if (res.message == null) {

            this.getAppointment();
            this.getContract();
            this.getRankRecords();
            this.getEOSRecord();
            this.getEosType();

            // if (this.temp[0].empTypeID != 0) {
            //   this.form.get('empTypeID').enable()
            // }

            // if (this.temp[0].rankID != 0 || this.temp[0].empStatusID != 0) {
            //   this.form.get('rankID').enable()
            //   this.form.get('empStatusID').enable()
            // }

            this.form.get('empTypeID').enable()
            this.form.get('rankID').enable()
            this.form.get('empStatusID').enable()
            this.form.get('active').enable()

          } else {
            this.form.get('rankID').disable({ onlySelf: true, emitEvent: false });
            this.form.get('empTypeID').disable({ onlySelf: true, emitEvent: false });
            this.form.get('active').disable({ onlySelf: true, emitEvent: false });
            this.form.get('empStatusID').disable({ onlySelf: true, emitEvent: false });

            this.eosArr = []
            this.eosTypeArr = []
            this.appointmentArr = []
            this.contractArr = []
            this.rankRecordsArr = []
            this.headerShowContent = false
            this.showContent = false
            this.clear1();
            this.clear()
            this.f.active.setValue('1');

            this.image = 'assets/noimage.jpg'


            this.global.swalClose()
            this.global.swalAlert('Employment record not found.', '', 'warning');

            // window.history.back()
          }
        }, Error => {
          this.clear1();
          this.global.swalAlertError();
        });


        this.api.getPerson(this.id).map(response => response.json()).subscribe(res => {

          if (res.message != undefined && res.message == 'Person found.') {
            this.showContent = true
            this.headerShowContent = true
            this.fname = res.data.firstName;
            this.mname = res.data.middleName;
            this.lname = res.data.lastName;
            this.suffix = res.data.suffixName;
            this.dob = res.data.dateOfBirth;

            //get PersonIDInfo
            this.getPersonIDInfo();

            //get Account
            this.getAccount();

            //Retrieve Employee Basic Info
            this.getEmployeesHiringBasicInfo();

            this.f.active.setValue('1');
            this.global.swalClose()
          }
        }, Error => {
          this.clear1()
          this.global.swalAlertError(Error);
        });

        // if (this.showContent == false) {
        //   this.clear();
        // }

        // this.global.swalLoading('Retrieving Person and Employee Informations');
      }
      if (this.id == '') {
        this.global.swalClose()
        this.global.swalAlert('Enter ID Number.', '', 'warning');
      }
    }

  }


  getAppointment() {
    //  this.global.swalLoading('Loading item resources')
    this.newArr = []
    this.HrisApi.getEmployeeAppointment(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentArr = res.data;
        //  console.log(this.appointmentArr)
        //  this.global.swalClose();
        if (this.appointmentArr != null) {
          for (let i = 0; i < this.appointmentArr.length; i++) {
            this.newArr.push({
              dateAppointed: this.appointmentArr[i].dateAppointed,
              appointmentTypeID: this.appointmentArr[i].appointmentTypeID,
            });
          }
        } else {
          this.appointmentArr = []
        }
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getContract() {
    // this.global.swalLoading('Loading item resources')
    this.HrisApi.getEmployeeContract(this.id)
      //  this.http.get(this.global.api+'Employee/Contract/'+this.id,this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.contractArr = res.data;
        //  console.log(this.contractArr)
        // this.global.swalClose();

      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  getRankRecords() {
    this.recordArr = []
    //  this.global.swalLoading('Loading item resources')
    this.HrisApi.getEmployeeRank(this.id)
      .map(response => response.json())
      .subscribe(res => {
        this.rankRecordsArr = res.data

        // console.log(this.rankRecordsArr);
        //  this.global.swalClose();

        if (this.rankRecordsArr != null) {
          for (var x = 0; x < this.rankRecordsArr.length; x++) {
            this.recordArr.push({
              sy: this.rankRecordsArr[x].sy,
              rank: this.rankRecordsArr[x].rank,
              empStatusID: this.rankRecordsArr[x].empStatusID,
            });
          }
          // this.rankRecordsArr = res.data;
          this.rankRecordsArr.sort((a, b) => b.sy.localeCompare(a.sy));
        } else {
          this.rankRecordsArr = []
        }
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  addAppointmentOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      if (this.appointmentArr == null) {
        this.appointmentArr = []

      }
      const dialogRef = this.dialog.open(AppointmentsComponent, {

        width: '600px', disableClose: false,
        data: { selectedID: this.idnumber, type: "add", main: 1, active: 1, arraylength: this.appointmentArr.length, array: this.appointmentArr },
        autoFocus: false,
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          // console.log(result.result)

          if (result.result == 'Adding Success') {
            this.getEmployeesHiringBasicInfo();
            this.getAppointment();
          }
        }
      });
    }
  }

  updateAppointmentOpenDialog(a) {

    const dialogRef = this.dialog.open(AppointmentsComponent, {
      width: '600px', disableClose: false, data: {
        selectedID: this.idnumber,
        selectedData: a,
        type: "update",
        array: this.appointmentArr
      }, autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.updateEmployeeName()
      this.getdtrIdandPosition()
      if (result != undefined) {
        //  console.log(result.result)

        if (result.result == 'Update success') {
          this.getAppointment();
          this.getEmployeesHiringBasicInfo();
        }
      }
    });
  }

  deleteEOS(selectedType) {
    // console.log(this.id, selectedType)

    for (var x = 0; x < this.eosTypeArr.length; x++) {
      if (selectedType == this.eosTypeArr[x].eosType) {
        selectedType = this.eosTypeArr[x].id
      }
    }

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeEOS(this.id, selectedType)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {

              this.global.swalAlert('End of Service removed successfully.', "", 'success');
              this.getEOSRecord();
            }
          }, Error => {

            this.global.swalAlertError();

          });
      }
      else {

      }
    });
  }

  deleteAppointment(selectedid) {

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeAppointment(selectedid)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert('Appointment removed successfully.', "", 'success');
              this.getAppointment();
              this.getEmployeesHiringBasicInfo();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();

            //console.log(Error)
          });
      }
      else {

      }
    });
    this.getAppointment();
  }


  addContractOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(ContractsComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber, type: "add" }, autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          this.getContract();
        }
      });
    }
  }

  addEndOfServiceOpenDialog(eosArr) {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(EndOfServiceComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber, type: "add", active: '1', validation: eosArr }, autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
          if (result.result == 'Adding Success') {
            this.getEOSRecord();
            this.getAppointment()
            this.getEmployeesHiringBasicInfo();
          }
        }
      });
    }
  }

  updateEndOfServiceOpenDialog(c) {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(EndOfServiceComponent, {
        width: '600px', disableClose: false, data: { selectedID: this.idnumber, type: "update", updateData: c, active: '0' }, autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {

          if (result.result == 'Update Success') {
            this.getEOSRecord();
            this.getEmployeesHiringBasicInfo();

          }
        }
      });
    }
  }

  updateContractOpenDialog(c) {
    const dialogRef = this.dialog.open(ContractsComponent, {
      width: '600px', disableClose: false, data: { selectedID: this.idnumber, selectedData: c, type: "update" }, autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getContract();
        this.getEmployeesHiringBasicInfo();

      }
    });
  }

  deleteContract(selectedid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeContract(selectedid)
          .map(response => response.json())
          .subscribe(res => {

            if (res.message != undefined) {
              //console.log(this.id+"----"+SCID.childID)
              this.global.swalAlert(res.message, "", 'success');
              this.getContract();
            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }

  addRankRecordOpenDialog() {
    if (this.id == '') {
      this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
    } else {
      const dialogRef = this.dialog.open(RankRecordsComponent, {
        width: '600px', disableClose: true, data: { selectedID: this.idnumber, type: "add" }, autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {

        if (result != undefined) {
          this.getRankRecords();
          this.getEmployeesHiringBasicInfo();
        }
      });
    }

  }

  updateRankRecordOpenDialog(c) {
    const dialogRef = this.dialog.open(RankRecordsComponent, {
      width: '600px', disableClose: true, data: { selectedID: this.idnumber, selectedData: c, type: "update" }, autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getRankRecords();
        this.getEmployeesHiringBasicInfo()
      }
    });
  }

  deleteRankRecord(selectedid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected item" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        this.HrisApi.deleteEmployeeRank(selectedid, selectedid)
          // this.http.delete(this.global.api+'Employee/Rank/'+selectedid+'?evaluationid='+selectedid,this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            // console.log(selectedid)

            if (res.message != undefined) {
              this.global.swalAlert('Rank removed successfully.', "", 'success');
              this.getRankRecords();

            }
          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
      }
      else {

      }
    });
  }


}