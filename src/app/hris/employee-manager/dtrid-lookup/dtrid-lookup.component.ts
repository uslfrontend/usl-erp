import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-dtrid-lookup',
  templateUrl: './dtrid-lookup.component.html',
  styleUrls: ['./dtrid-lookup.component.css']
})
export class DtridLookupComponent implements OnInit {

  /* --Variable initialization-- */
  //#region 
  employeeAllData;
  employeeAllDataTemp;
  config: any;
  dtrid;
  //#endregion

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<DtridLookupComponent>, @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    this.employeeAllData=this.data.AllEmployeeData;
    this.employeeAllDataTemp=this.data.AllEmployeeData;
    //console.log(this.employeeAllData);

    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: 0
    };
  }

  select(id){
    this.dialogRef.close({result:id});  
  }

  close(): void {
      this.dialogRef.close({result:'cancel'});
  }

  Filter(){
    this.employeeAllData = [];
    for (var i = 0; i < this.employeeAllDataTemp.length; ++i) {
      if (this.employeeAllDataTemp[i].dtrid.startsWith(this.dtrid)) {
        this.employeeAllData.push(this.employeeAllDataTemp[i])
      }
    }
  }

  pageChanged(event){
    this.config.currentPage = event;
  }

}
