import { Component, OnInit } from '@angular/core';
import { HrisApiService } from '../../../hris-api.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { GlobalService } from '../../../global.service';
import { map, startWith } from 'rxjs/operators';
import { of } from 'rxjs';
import { RankRecordsComponent } from '../../employee-information/employee-records/rank-records/rank-records.component';
import { MatDialog } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { ElementRef, ViewChild } from '@angular/core';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-rank-record-batch-uploader',
  templateUrl: './rank-record-batch-uploader.component.html',
  styleUrls: ['./rank-record-batch-uploader.component.css']
})
export class RankRecordBatchUploaderComponent implements OnInit {

  @ViewChild('Input', { static: true }) Input: ElementRef<HTMLInputElement>;

  deptArr
  DID
  employeeArr
  displayArr = []

  rankArr = []
  selectedRank

  dept = ''
  pos = ''
  sDate = ''
  eDate = ''
  appointmentId = ''

  posArr;
  classArr;
  tocArr;
  evalStatArr;
  appointmentArr;
  rankStatusArr;
  rankRecordsArr;
  rankRecord;

  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  departmentArr: string[] = []
  deparmentValue = new FormControl();
  deparmentOptions: Observable<string[]>;

  positionArr: string[] = []
  positionValue = new FormControl();
  positionOptions: Observable<string[]>;

  separatorKeysCodes: number[] = [ENTER, COMMA];
  recommendations = new FormControl();
  filteredRecommendations: Observable<string[]>;
  option: string[] = [];
  allRecommendations: string[] = [];

  configRooms: any
  ctr = 0

  constructor(
    private HrisApi: HrisApiService,
    public global: GlobalService,
    public dialog: MatDialog,

  ) {

    this.configRooms = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.ctr
    };

    this.global.swalLoading('Loading appointment resources')
    this.HrisApi.getHRISMaintenanceRankList()
      // this.http.get(this.global.api+'HRISMaintenance/RankList',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.rankArr = res.data;

        this.HrisApi.getHRISMaintenanceDepartment()
          // this.http.get(this.global.api+'HRISMaintenance/Department',this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            this.deptArr = res.data;
            for (var i = 0; i < this.deptArr.length; ++i) {
              this.departmentArr.push(this.deptArr[i].departmentName)
            }

            this.HrisApi.getHRISMaintenancePositionList()
              // this.http.get(this.global.api+'HRISMaintenance/PositionList',this.global.option)
              .map(response => response.json())
              .subscribe(res => {
                this.posArr = res.data;
                // console.log(res.data)
                for (var i = 0; i < this.posArr.length; ++i) {
                  this.positionArr.push(this.posArr[i].position)
                }

                this.HrisApi.getHRISMaintenanceClassifications()
                  // this.http.get(this.global.api+'HRISMaintenance/Classifications',this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {
                    this.classArr = res.data;

                    this.HrisApi.getHRISMaintenanceEmpStatus()
                      // this.http.get(this.global.api+'HRISMaintenance/EmpStatus',this.global.option)
                      .map(response => response.json())
                      .subscribe(res => {
                        this.tocArr = res.data;

                        this.HrisApi.getHRISMaintenanceEvaluationStatuses().map(response => response.json())
                          .subscribe(res => {
                            this.evalStatArr = res.data

                            this.HrisApi.getHRISMaintenanceRankStatuses()
                              .map(response => response.json())
                              .subscribe(res => {
                                this.rankStatusArr = res.data;

                              }, Error => {
                                this.global.swalAlertError();
                              });

                          }, Error => {
                            this.global.swalAlertError();
                          });
                      }, Error => {
                        this.global.swalAlertError();
                      });

                  }, Error => {
                    this.global.swalAlertError();
                  });

              }, Error => {
                this.global.swalAlertError();
              });

          }, Error => {
            this.global.swalAlertError();
          });

        this.global.swalClose();
      }, Error => {
        this.global.swalAlertError();
      });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


  private _filter1(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.departmentArr.filter(option => option.toLowerCase().includes(filterValue));
  }


  private _filter2(value: string): string[] {
    const filterValue1 = value.toLowerCase();
    return this.positionArr.filter(option1 => option1.toLowerCase().includes(filterValue1));
  }

  private _filter3(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allRecommendations.filter((recommendation) => recommendation.toLowerCase().includes(filterValue));
  }

  ngOnInit() {

    this.filteredOptions = this.myControl.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
    this.deparmentOptions = this.deparmentValue.valueChanges.pipe(startWith(''), map(value => this._filter1(value)));
    this.positionOptions = this.positionValue.valueChanges.pipe(startWith(''), map(value => this._filter2(value)));
    this.filteredRecommendations = this.recommendations.valueChanges.pipe(startWith(null),
      map((rec: string | null) => (rec ? this._filter3(rec) : this.allRecommendations.slice())));

    this.getDept()

    if (this.global.syear != null) {
      for (var x = 0; x < this.displayArr.length; x++) {
        this.displayArr[x].year1Model = this.global.syear.substring(0, 4).toString()
        // this.displayArr[x].year2Model  = (parseInt(this.global.syear.substring(0, 4)) + 1).toString()
        // this.schoolYear = this.year1 + '-' + this.year2
      }
      // console.log(this.global.syear.substring(0, 4).toString())
    }

  }

  getDept() {

    this.HrisApi.getHRISMaintenanceDepartment().map(response => response.json()).subscribe(res => {
      this.deptArr = res.data;
      for (var i = 0; i < this.deptArr.length; ++i) {
        this.options.push(res.data[i].departmentName)
      }
      // console.log(this.departmentArr)
    }, Error => {
      this.global.swalAlertError();
    });

  }

  keyDownFunction(event) {

    if (event.keyCode == 13 || event.keyCode == 9) {
      this.generate()
    }

  }


  generate() {

    this.DID = ''
    this.displayArr = []
    for (var x = 0; x < this.deptArr.length; x++) {
      if (this.myControl.value == this.deptArr[x].departmentName) {
        this.DID = this.deptArr[x].departmentID
      }
    }

    if (this.DID == '') {
      this.global.swalAlert("Department not found!", '', 'warning')
      // console.log(this.displayArr)
    } else {
      this.employeeArr = undefined

      this.HrisApi.getReportHRISEmployeeProfileByDept(this.DID)
        .map(response => response.json())
        .subscribe(res => {
          // console.log(res)
          this.employeeArr = [];
          if (res.data == null) {
            this.global.swalAlert(res.message, '', 'warning')
          } else

            this.employeeArr = res.data;
          this.employeeArr.sort((a, b) => {
            const nameA = a.name.toLowerCase();
            const nameB = b.name.toLowerCase();

            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            return 0;
          });

          this.getData()

          // console.log(this.rankArr)
        }, Error => {
          this.employeeArr = [];
          this.global.swalAlertError();
        });
    }
  }

  async getData() {
    this.global.swalLoading('Loading Information.');
    for (var x = 0; x < this.employeeArr.length; x++) {

      try {
        const response = await this.HrisApi.getEmployeeAppointmentbyActive(this.employeeArr[x].employeeId, 1).toPromise();
        const res = response.json();
        this.displayArr.push({ idNumber: this.employeeArr[x].employeeId, employeeName: this.employeeArr[x].name, appointments: res.data });

      } catch (error) {

      }

    }

    for (var x = 0; x < this.displayArr.length; x++) {

      this.displayArr[x].rankModel = ''
      this.displayArr[x].appointmentModel = ''
      this.displayArr[x].departmentModel = ''
      this.displayArr[x].positionModel = ''
      this.displayArr[x].statusModel = ''
      this.displayArr[x].rankStatusModel = ''
      this.displayArr[x].evaluationModel = ''
      this.displayArr[x].evaluationStatusModel = ''
      this.displayArr[x].tocModel = ''
      this.displayArr[x].recommendationModel = ''
      this.displayArr[x].recommendationArr = []
      this.displayArr[x].year1Model = ''
      this.displayArr[x].year2Model = ''

    }

    this.global.swalClose()
  }

  year1Change(id, value) {
    for (var x = 0; x < this.displayArr.length; x++) {
      if (id == this.displayArr[x].idNumber) {
        this.displayArr[x].year1Model = value
        this.displayArr[x].year2Model = this.displayArr[x].year1Model + 1
      }
    }

  }

  year2Change(id, value) {
    for (var x = 0; x < this.displayArr.length; x++) {
      if (id == this.displayArr[x].idNumber) {
        this.displayArr[x].year2Model = value
        this.displayArr[x].year1Model = this.displayArr[x].year2Model - 1
      }
    }
  }

  eval(id, evalValue) {

    const evaluationValue = evalValue;
    for (var x = 0; x < this.displayArr.length; x++) {

      if (id == this.displayArr[x].idNumber) {

        if (evaluationValue != null && !isNaN(evaluationValue)) {
          const evalLength = parseFloat(evaluationValue);

          if (!isNaN(evalLength) && evalLength >= 75 && evalLength <= 100) {
            this.displayArr[x].evaluationStatusModel = '1'
            this.recommendation(1);

          } else if (!isNaN(evalLength) && evalLength >= 50 && evalLength < 75) {
            this.displayArr[x].evaluationStatusModel = '2'
            this.recommendation(2);

          } else  {
            this.displayArr[x].evaluationStatusModel = ''
          }
        }
      }
    }
  }

  appointmentPopulate(appointment, id) {
    this.dept = ''
    this.pos = ''
    this.sDate = ''
    this.eDate = ''

    this.HrisApi.getEmployeeAppointmentbyActive(id, 1).map(response => response.json()).subscribe(res => {
      for (var z = 0; z < res.data.length; z++) {
        if (appointment == res.data[z].appointmentID) {
          this.dept = res.data[z].departmentName
          this.pos = res.data[z].position
          this.sDate = res.data[z].startdate
          this.eDate = res.data[z].enddate

          for (var x = 0; x < this.displayArr.length; x++) {
            if (appointment == this.displayArr[x].appointmentModel) {
              this.displayArr[x].departmentModel = this.dept
              this.displayArr[x].positionModel = this.pos
              this.displayArr[x].year1Model = this.sDate.substring(6, 10)
              this.displayArr[x].year2Model = this.eDate.substring(6, 10)
            }
          }

          this.rankRecordsArr = []
          //  this.global.swalLoading('Loading item resources')
          this.HrisApi.getEmployeeRank(id).map(response => response.json()).subscribe(res => {
            this.rankRecordsArr = res.data;

            //  this.global.swalClose();
            for (var x = 0; x < res.data.length; x++) {
              if (appointment == res.data[x].appointmentID) {
                for (var y = 0; y < this.displayArr.length; y++) {
                  if (id == this.displayArr[y].idNumber) {
                    this.displayArr[y].rankModel = this.rankRecordsArr[x].rank
                    this.displayArr[y].year1Model = this.rankRecordsArr[x].sy.substring(0, 4)
                    this.displayArr[y].year2Model = this.rankRecordsArr[x].sy.substring(5, 9)
                    this.displayArr[y].departmentModel = this.rankRecordsArr[x].department
                    this.displayArr[y].positionModel = this.rankRecordsArr[x].position
                    this.displayArr[y].statusModel = this.rankRecordsArr[x].status
                    this.displayArr[y].rankStatusModel = this.rankRecordsArr[x].rankStatusID.toString()
                    this.displayArr[y].evaluationModel = this.rankRecordsArr[x].evaluation
                    this.displayArr[y].evaluationStatusModel = this.rankRecordsArr[x].evaluationStatusID.toString()
                    this.displayArr[y].tocModel = this.rankRecordsArr[x].empStatusID.toString()
                    const result: string[] = this.splitSentenceIntoArray(this.rankRecordsArr[x].recommendation);
                    this.displayArr[y].recommendationArr = result

                  }
                }
              }
            }

          }, Error => {
            //console.log(Error);
            this.global.swalAlertError();
            //console.log(Error)
          });
        }
      }
    })

  }


  splitSentenceIntoArray(sentence: string): string[] {
    const phrases: string[] = sentence.split(';').map((phrase) => phrase.trim());

    const uniquePhrases: string[] = [];
    phrases.forEach((phrase) => {
      if (phrase && !uniquePhrases.includes(phrase)) {
        uniquePhrases.push(phrase + ';');
      }
    });

    return uniquePhrases;
  }

  hasEmptyValues(): boolean {
    if (!this.displayArr || this.displayArr.length === 0) {
      return true; // Modify the condition to return true when displayArr is null or its length is 0
    }

    for (const item of this.displayArr) {
      if (
        Object.entries(item).some(
          ([key, value]) => (value === '' || value === null) && key !== 'recommendationModel'
        )
      ) {
        return true;
      }
    }

    return false;
  }

  generateSentence(phrases: string[]): string {
    let sentence: string = phrases.join(' ');
    sentence = sentence.charAt(0).toUpperCase() + sentence.slice(1) + ' ';
    return sentence;
  }

  tempVar
  save() {

    this.tempVar = []
    Swal.fire({
      title: 'Rank Records will be saved.',
      text: "Are you sure?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, save it!'

    }).then((result) => {
      if (result.value) {
        for (var x = 0; x < this.displayArr.length; x++) {

          if (
            this.displayArr[x].year1Model != '' &&
            this.displayArr[x].year2Model != '' &&
            this.displayArr[x].rankModel != '' &&
            this.displayArr[x].departmentModel != '' &&
            this.displayArr[x].positionModel != '' &&
            this.displayArr[x].statusModel != '' &&
            this.displayArr[x].tocModel != '' &&
            this.displayArr[x].evaluationModel != '' &&
            this.displayArr[x].appointmentModel != '' &&
            this.displayArr[x].rankStatusModel != '' &&
            this.displayArr[x].evaluationStatusModel != ''
          ) {

            const result: string = this.generateSentence(this.displayArr[x].recommendationArr);

            this.HrisApi.postEmployeeRank(this.displayArr[x].idNumber, {
              "sy": this.displayArr[x].year1Model.toString() + '-' + this.displayArr[x].year2Model.toString(),
              "rank": this.displayArr[x].rankModel,
              "department": this.displayArr[x].departmentModel,
              "position": this.displayArr[x].positionModel,
              "status": this.displayArr[x].statusModel,
              "term": this.displayArr[x].tocModel,
              "evaluation": this.displayArr[x].evaluationModel,
              "recommendation": result,
              "appointmentID": this.displayArr[x].appointmentModel,
              "rankStatusID": this.displayArr[x].rankStatusModel,
              "evaluationStatusID": this.displayArr[x].evaluationStatusModel,
            })
              // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
              .map(response => response.json())
              .subscribe(res => {

              }, Error => {
                this.global.swalAlertError();
                // console.log(Error)
              });

            this.tempVar.push({ idNumber: this.displayArr[x].idNumber, fName: this.displayArr[x].employeeName })
          }
        }

        Swal.fire({
          title: 'Success!',
          html: `<div style="text-align: left;" class="swal-scroll-container">${this.tempVar.map(item => `<p>${item.idNumber} - ${item.fName}</p>`).join('')}</div>`,
          type: 'success',
          onOpen: () => {
            if (this.tempVar.length >= 5) {
              const container = Swal.getPopup().querySelector('.swal-scroll-container') as HTMLElement;
              container.style.maxHeight = '200px';
              container.style.overflowY = 'auto';
              container.style.textAlign = 'left'
            }
          }
        });
      }
    })

  }

  add(id, event: MatChipInputEvent): void {
    if (event.value && event.value.trim() !== '') {
      const value = event.value.trim();

      for (var x = 0; x < this.displayArr.length; x++) {
        if (id == this.displayArr[x].idNumber) {
          if (!this.displayArr[x].recommendationArr.includes(value)) { // Check if value already exists
            this.displayArr[x].recommendationArr.push(value + ';');
          }
        }
      }

      if (value && !this.option.includes(value)) { // Check if value already exists
        this.option.push(value);
      }

      // Clear the input value
      event.input.value = '';

      this.recommendations.setValue(null);
    }
  }



  remove(id, rec: string): void {

    for (var x = 0; x < this.displayArr.length; x++) {
      if (id == this.displayArr[x].idNumber) {

        // console.log(this.displayArr[x].recommendationArr)
        const index = this.displayArr[x].recommendationArr.indexOf(rec);
        if (index >= 0) {
          this.displayArr[x].recommendationArr.splice(index, 1);
        }
      }
    }

  }

  selected(id, event: MatAutocompleteSelectedEvent): void {
    const value = event.option.viewValue;
    if (value != 'With very high student\'s evaluation( );') {

      // Add the selected fruit
      for (var x = 0; x < this.displayArr.length; x++) {
        if (id == this.displayArr[x].idNumber) {
          if (value && !this.displayArr[x].recommendationArr.includes(value)) {
            this.displayArr[x].recommendationArr.push(value);
          }
        }
      }

      this.displayArr.find(a => a.idNumber === id).recommendationModel = null;

      this.recommendations.setValue(null);
    }


  }

  // id = '1201831'
  // idNumber = this.id
  // addRankRecordOpenDialog() {
  //   if (this.id == '') {
  //     this.global.swalAlert("Please check the ID number of the employee.", "", 'warning');
  //   } else {
  //     const dialogRef = this.dialog.open(RankRecordsComponent, {
  //       width: '600px', disableClose: false, data: { selectedID: this.idNumber, type: "add" }, autoFocus: false
  //     });

  //     dialogRef.afterClosed().subscribe(result => {

  //       if (result != undefined) {

  //       }
  //     });
  //   }
  // }

  pageChangedRooms(event) {
    this.configRooms.currentPage = event;

  }

  recommendation(value) {

    if (value == 1) {
      this.allRecommendations = [
        'Accelerated in rank (5 consecutive years with 91 and above rating);',
        'Granted regular status as faculty;',
        'Given warning on excessive absences and tardiness;',
        'Given warning on excessive absences;',
        'Given warning on excessive tardiness;',
        'Passed but  not rehired due to administrative case;',
        'Passed but terminated due to administrative case;',
        'Pegged in rank due to non-submission of research;',
        'Pegged in rank pending required Master\'s or Doctorate degree;',
        'Promoted in rank;',
        'Promotion in rank is upon submission of research;',
        'Rehired;',
        'With very high overall performance evaluation rating;',
        'With very high student\'s evaluation( );',
        'With zero absence and tardiness;',
        'With zero absence;',
        'With zero tardiness;']

    } else if (value == 2) {
      this.allRecommendations = [
        'Non-renewal of contract/end of contract due to failed rating and due to administrative case;',
        'Non-renewal of contract/end of contract;',
        'Rehired but given warning warning on consecutive retention in rank;',
        'Rehired but given warning warning on consecutive retention in rank;',
        'Rehired and given another chance due to high students\' evaluation but subject to availability of loads;',
        'Rehired and given last chance due to high students\' evaluation but subject to availability of loads;',
        'With very low students\' evaluation ( );',
        'With excessive absences and tardiness;',
        'With excessive absences;',
        'With excessive tardiness;'];

    }
  }


  positionValueChange(valueModel, id) {
    let observableValueModel: Observable<string>;

    if (valueModel.pipe && typeof valueModel.pipe === 'function') {
      // valueModel is already an observable
      observableValueModel = valueModel;
    } else {
      // valueModel is not an observable, convert it to one using `of`
      observableValueModel = of(valueModel);
    }

    this.positionValue.setValue(valueModel);

    this.positionOptions = observableValueModel.pipe(
      startWith(''),
      map(value => this._filter2(value))
    );
  }

  recommendationValueChange(valueModel, id) {

    let observableValueModel;

    if (valueModel.pipe && typeof valueModel.pipe === 'function') {
      // valueModel is already an observable
      observableValueModel = valueModel;
    } else {
      // valueModel is not an observable, convert it to one using `of`
      observableValueModel = of(valueModel);
    }

    this.recommendations.setValue(valueModel);

    this.filteredRecommendations = observableValueModel.pipe(
      startWith(null),
      map((rec: string | null) => (rec ? this._filter3(rec) : this.allRecommendations.slice()))
    );
  }

  departmentValueChange(valueModel, id) {
    let observableValueModel: Observable<string>;

    if (valueModel.pipe && typeof valueModel.pipe === 'function') {
      // valueModel is already an observable
      observableValueModel = valueModel;
    } else {
      // valueModel is not an observable, convert it to one using `of`
      observableValueModel = of(valueModel);
    }

    this.deparmentValue.setValue(valueModel);

    this.deparmentOptions = observableValueModel.pipe(
      startWith(''),
      map(value => this._filter1(value))
    );
  }

  removeEmployee(id: number, employeeName): void {

    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to remove " + employeeName + '?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.displayArr = this.displayArr.filter(employee => employee.idNumber !== id);
        // console.log(this.displayArr);
        Swal.fire(
          'Removed!',
          'Employee has been removed.',
          'success'
        )
      }
    })
    // If the user confirms the action to delete the file, execute the delete action and display a success message
  }
}
