import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankRecordBatchUploaderComponent } from './rank-record-batch-uploader.component';

describe('RankRecordBatchUploaderComponent', () => {
  let component: RankRecordBatchUploaderComponent;
  let fixture: ComponentFixture<RankRecordBatchUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankRecordBatchUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankRecordBatchUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
