import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http, public global: GlobalService) { }

  getPublicNotifications()
  {
    return this.http.get(this.global.api + 'PublicAPI/Notifications', this.global.option)
  }
  getPublicAPICurrentServerTime() {
    return this.http.get(this.global.api + 'PublicAPI/CurrentServerTime', this.global.option)
  }

  getAuthUserRoles() {
    return this.http.get(this.global.api + 'Auth/UserRoles', this.global.option)
  }

  getPublicAPIDepartments() {
    return this.http.get(this.global.api + 'PublicAPI/Departments', this.global.option)
  }

  getPublicAPIProgramLevels() {
    return this.http.get(this.global.api + 'PublicAPI/ProgramLevels', this.global.option)
  }

  getAccess(x = null) {
    if (x == null)
      x = this.global.requestid()

    return this.http.get(this.global.api + 'Access/' + x, this.global.option)
  }

  getPublicAPISYOptionsList() {
    return this.http.get(this.global.api + 'PublicAPI/SYOptionsList', this.global.option)
  }

  getAuthUserViewDomains() {
    return this.http.get(this.global.api + 'Auth/UserViewDomains', this.global.option)
  }

  getAuthUserInfo() {
    return this.http.get(this.global.api + 'Auth/UserInfo', this.global.option)
  }

  putAccountChangePassword(data) {
    return this.http.put(this.global.api + 'Account/ChangePassword', data, this.global.option)
  }

  postAuthlogin(data, option) {
    return this.http.post(this.global.api + 'Auth/login', data, option)
  }

  getOnlineRegistrationApplicantFind(text) {
    return this.http.get(text, this.global.option)
  }
  getText(text) {
    return this.http.get(text, this.global.option)
  }

  getPersonLookup(lname, bool) {
    return this.http.get(this.global.api + 'Person/Lookup/' + lname + "/" + bool, this.global.option)
  }

  /*--Added by Loui---*/
  getPersonIDInfo(id): Observable<any> {
    return this.http.get(this.global.api + 'Person/IDInfo/' + id, this.global.option)
  }

  getPublicAPICourses() {
    return this.http.get(this.global.api + 'PublicAPI/Courses', this.global.option)
  }

  /* -------------- */

  postPerson(idseries, data) {
    return this.http.post(this.global.api + 'Person?idseries=' + idseries, data, this.global.option)
  }

  putOnlineRegistrationApplicantIDNumber(applicantNo, data) {
    return this.http.put(this.global.api + 'OnlineRegistration/Applicant/IDNumber/' + applicantNo, data, this.global.option)
  }

  putPlacement(id, data) {
    return this.http.put(this.global.api + 'Placement/' + id, data, this.global.option)
  }

  getPerson(idno,) {
    return this.http.get(this.global.api + 'Person/' + idno, this.global.option)
  }

  putPerson(data) {
    return this.http.put(this.global.api + 'Person', data, this.global.option)
  }

  getClientApplication() {
    return this.http.get(this.global.api + "ClientApplication", this.global.option)
  }

  deleteClientApplication(id) {
    return this.http.delete(this.global.api + 'ClientApplication/' + id, this.global.option)
  }

  getClientApplicationAccessLists() {
    return this.http.get(this.global.api + 'ClientApplication/AccessLists', this.global.option)
  }

  postClientApplication(data) {
    return this.http.post(this.global.api + 'ClientApplication', data, this.global.option)
  }

  putClientApplication(id, data) {
    return this.http.put(this.global.api + 'ClientApplication/' + id, data, this.global.option)
  }

  putClientApplicationAccess(id, data) {
    return this.http.put(this.global.api + 'ClientApplication/Access/' + id, data, this.global.option)
  }

  getRoleRoles() {
    return this.http.get(this.global.api + 'Role/Roles', this.global.option)
  }

  getRole(id) {
    return this.http.get(this.global.api + 'Role/' + id, this.global.option)
  }

  getRoleAccessLists() {
    return this.http.get(this.global.api + 'Role/AccessLists', this.global.option)
  }

  deleteRole(id) {
    return this.http.delete(this.global.api + 'Role/' + id, this.global.option)
  }

  getMaintenanceSYOptionsList() {
    return this.http.get(this.global.api + 'Maintenance/SYOptionsList', this.global.option)
  }

  deleteMaintenanceSYOptionsList(sy) {
    return this.http.delete(this.global.api + 'Maintenance/SYSettings/' + sy, this.global.option)
  }
  getAdmissionSYSettings(syWithSem) {
    return this.http.get(this.global.api + 'Admission/SYSettings/' + syWithSem, this.global.option)
  }
  getEnrollmentEnrollmentSettingValueMapper(a) {
    return this.http.get(this.global.api + 'Enrollment/EnrollmentSettingValueMapper/' + a, this.global.option)
  }

  putMaintenanceSYSettings(selectedsy, data) {
    return this.http.put(this.global.api + 'Maintenance/SYSettings/' + selectedsy, data, this.global.option)
  }

  /* ---Addded by Loui */
  getMaintenanceOnlineRegSettings(code) {
    return this.http.get(this.global.api + 'Maintenance/OnlineRegistration/Settings/' + "?code=" + code, this.global.option)
  }
  /* ------- */

  postRole(data) {
    return this.http.post(this.global.api + 'Role', data, this.global.option)
  }

  putRole(id, data) {
    return this.http.put(this.global.api + 'Role/' + id, data, this.global.option)
  }

  deleteAccessViewDomain(id) {
    return this.http.delete(this.global.api + 'Access/ViewDomain/' + id, this.global.option)
  }

  postAccessViewDomain(data) {
    return this.http.post(this.global.api + 'Access/ViewDomain/', data, this.global.option)
  }

  getAccessViewDomain(id) {
    return this.http.get(this.global.api + 'Access/ViewDomain/' + id, this.global.option)
  }
  getAccessViewDomainByRole(id) {
    return this.http.get(this.global.api + 'Access/ViewDomainByRole/' + id, this.global.option)
  }

  getPublicAPIOffices(active) {
    return this.http.get(this.global.api + 'PublicAPI/Offices?active=' + active, this.global.option)
  }

  postAccess(data) {
    return this.http.post(this.global.api + 'Access/', data, this.global.option)
  }

  postAccountregisteruser(data) {
    return this.http.post(this.global.api + 'Account/registeruser', data, this.global.option)
  }
  postAccountregisteruserStudent(data) {
    return this.http.post(this.global.api + 'Account/registeruserstudent', data, this.global.option)
  }
  getEmployeePersonalInfo(id) {
    return this.http.get(this.global.api + 'Employee/PersonalInfo/' + id, this.global.option)
  }

  getEmployee(id) {
    return this.http.get(this.global.api + 'Employee/' + id, this.global.option)
  }

  putAccountLockUnlock(id, x) {
    return this.http.put(this.global.api + 'Account/LockUnlock/' + id + '/' + x, {}, this.global.option)
  }

  getStudent(id, syear, domain) {
    return this.http.get(this.global.api + 'Student/' + id + '/' + syear + '/' + domain, this.global.option)
  }

  getStudentClassSchedule(id, syear) {
    return this.http.get(this.global.api + 'Student/EnrolledSubjects/' + id + '/' + syear, this.global.option)
  }
  getStudentBasicEdStudentSchedule(sy, section, id): Observable<any> {
    return this.http.get(this.global.api + 'Student/BasicEdStudent/Schedule/' + sy + '/' + section + '/' + id, this.global.option)
      .map(response => response.json())
  }
  getStudentBasicEdEnrollmentElective(sy, id): Observable<any> {
    return this.http.get(this.global.api + 'Student/BasicEdStudent/Elective/' + sy + '?idNumber=' + id, this.global.option)
      .map(response => response.json())
  }

  //Ched Reports
  //#region
  //---------------------------------------------------------------------------------------------------------------------//
  getReportCHEDNSTPCHED(sy) {
    return this.http.get(this.global.api + "ReportCHED/NSTPCHED/" + sy, this.global.option)
  }

  getReportCHEDEnrollmentSummaryCHED(sy, proglevel) {
    return this.http.get(this.global.api + "ReportCHED/EnrollmentSummaryCHED/" + sy + "?level=" + proglevel, this.global.option)
  }

  getReportCHEDEnrollmentListCHED(sy, proglevel, pageSize, pageNumber, ex) {
    return this.http.get(this.global.api + "ReportCHED/EnrollmentListCHED/" + sy + "?level=" + proglevel, this.global.option)
  }

  getReportCHEDPersonSignatureInfo(id) {
    return this.http.get(this.global.api + "ReportCHED/PersonSignatureInfo/" + id, this.global.option)
  }
  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

  getReportSummaryApplicantSummaryCountProgLevel(csdate, cedate) {
    return this.http.get(this.global.api + "ReportSummary/ApplicantSummaryCount/ProgLevel/?dateFrom=" + csdate + "&dateTo=" + cedate, this.global.option)
  }

  getReportSummaryApplicantSummaryCountPreferredCourse(start, end, csdate, cedate) {
    return this.http.get(this.global.api + "ReportSummary/ApplicantSummaryCount/PreferredCourse/" + start + "/" + end + "?dateFrom=" + csdate + "&dateTo=" + cedate, this.global.option)
  }

  getReportTeacherCodeSummary(syear, CNum, dept) {
    return this.http.get(this.global.api + "ReportTeacher/CodeSummary/" + syear + "?codeNo=" + CNum + "&department=" + dept, this.global.option)
  }

  getReportTeacherCodeSummaryNoDept(syear, CNum) {
    return this.http.get(this.global.api + "ReportTeacher/CodeSummary/" + syear + "?codeNo=" + CNum, this.global.option)
  }

  getReportTeacherCodeSummaryFacultyID(syear, instructorId) {
    return this.http.get(this.global.api + "ReportTeacher/CodeSummary/" + syear + "?instructorId=" + instructorId, this.global.option)
  }

  getReportTeacherCodeSummaryFacultyID2(syear, instructorId, departmentF) {
    return this.http.get(this.global.api + "ReportTeacher/CodeSummary/" + syear + "?instructorId=" + instructorId + "&departmentF=" + departmentF, this.global.option)
  }

  getReportTeacherClassList(syear, CNum) {
    return this.http.get(this.global.api + 'ReportTeacher/ClassList/' + CNum + '/' + syear, this.global.option)
  }


  getReportTeacherIDInfo(idNumber) {
    return this.http.get(this.global.api + 'ReportTeacher/IDInfo/' + idNumber, this.global.option)
  }

  getReportTeacherAdmittedInCode(CNum, syear) {
    return this.http.get(this.global.api + 'ReportTeacher/AdmittedInCode/' + CNum + '/' + syear, this.global.option)
  }

  getReportSummaryApplicantSummaryListRecommended(sy) {
    return this.http.get(this.global.api + "ReportSummary/ApplicantSummaryList/Recommended/" + sy, this.global.option)
  }

  getReportSummarySubjectProjection(syear, term, dept) {
    // return this.http.get(this.global.api + 'ReportSummary/SubjectProjection/?csy=' + syear + "&projectTerm=" + term + "&departmentId=" + dept, this.global.option)
    return this.http.get(this.global.api + 'ReportSummary/SubjectProjection/' + syear + "/" + term + "/" + dept, this.global.option)
  }

  getReportSummaryPreEnrollmentStatisticsCourse(syear, option) {
    return this.http.get(this.global.api + 'ReportSummary/PreEnrollmentStatistics/Course/' + syear + '/' + option, this.global.option)
  }

  getReportSummaryPreEnrollmentStatisticsDepartment(syear, option) {
    return this.http.get(this.global.api + 'ReportSummary/PreEnrollmentStatistics/Department/' + syear + '/' + option, this.global.option)
  }

  getOnlineRegistrationCoursesWithStrand() {
    return this.http.get(this.global.api + 'OnlineRegistration/CoursesWithStrand/', this.global.option)
  }

  getReportSummaryPlacementSummary(csdate, cedate, course, result) {
    return this.http.get(this.global.api + "ReportSummary/PlacementSummary/" + csdate + "/" + cedate + "?firstChoiceCourseCode=" + course + "&firstChoiceResult=" + result, this.global.option)
  }

  getReportSummaryPreferredLearningModalityList(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/PreferredLearningModalityList/' + sy + "?level=" + proglevel, this.global.option)
  }

  postReportSummaryLookup(data) {
    return this.http.post(this.global.api + 'ReportSummary/Lookup', data, this.global.option)
  }

  getReportSummaryEnrollmentList(text) {
    return this.http.get(this.global.api + "ReportSummary/EnrollmentList/" + text, this.global.option)
  }

  getReportSummaryEnrollmentStatistics(sy, proglevel) {
    return this.http.get(this.global.api + "ReportSummary/EnrollmentStatistics/" + sy + "?level=" + proglevel, this.global.option)
  }

  getReportSummaryEnrollmentSummary(sy, proglevel) {
    return this.http.get(this.global.api + "ReportSummary/EnrollmentSummary/" + sy + "?level=" + proglevel, this.global.option)
  }

  getReportSummaryEnrolledStudentsList(sy, proglevel, status) {
    return this.http.get(this.global.api + "ReportSummary/EnrolledStudentsList/" + sy + "/" + proglevel + "/" + status, this.global.option)
  }

  getAdmissionCourses(domain, arr, yearlevel) {
    return this.http.get(this.global.api + 'Admission/Courses/' + domain + '/' + arr + '/' + yearlevel, this.global.option)
  }

  getStudentAcademicHistory(id, sy = '') {
    return this.http.get(this.global.api + 'Student/AcademicHistory/' + id + '/' + sy, this.global.option)
  }

  //---- Academic History Manager Added by Loui---//
  //#region
  getAcademicHistory(id) {
    return this.http.get(this.global.api + 'AcademicHistory/' + id, this.global.option)
  }

  postAcademicHistory(idNumber, data) {
    return this.http.post(this.global.api + 'AcademicHistory/' + idNumber, data, this.global.option)
  }

  putAcademicHistory(recordId, schoolYear, data) {
    return this.http.put(this.global.api + 'AcademicHistory/' + recordId + '/' + schoolYear, data, this.global.option)
  }

  deleteAcademicHistory(recordId, codeNo, schoolYear) {
    return this.http.delete(this.global.api + 'AcademicHistory/' + recordId + '/' + codeNo + '/' + schoolYear, this.global.option)
  }

  //#endregion

  getStudentEnrollmentHistory(id) {
    return this.http.get(this.global.api + 'Student/EnrollmentHistory/' + id, this.global.option)
  }

  getAdmissionRetentionPolicyLastSY(id) {
    return this.http.get(this.global.api + 'Admission/RetentionPolicyLastSY/' + id, this.global.option)
  }

  getStudentDemography(id, sy) {
    return this.http.get(this.global.api + 'Student/Demography/' + id + '/' + sy, this.global.option)
  }

  getAdmissionRetentionPolicySpecificSubjectType(id) {
    return this.http.get(this.global.api + 'Admission/RetentionPolicySpecificSubjectType/' + id, this.global.option)
  }

  getAdmissionRetentionPolicyWholeSY(id) {
    return this.http.get(this.global.api + 'Admission/RetentionPolicyWholeSY/' + id, this.global.option)
  }

  postAdmissionRegisterStudent(data) {
    return this.http.post(this.global.api + 'Admission/RegisterStudent', data, this.global.option)
  }

  putStudentPersonalInfo(id, data) {
    return this.http.put(this.global.api + 'Student/PersonalInfo?idNumber=' + id, data, this.global.option)
  }

  putStudentPersonInfo(data) {
    return this.http.put(this.global.api + 'Student/PersonInfo', data, this.global.option)
  }

  getStudentPersonInfo(id) {
    return this.http.get(this.global.api + 'Student/PersonInfo/' + id, this.global.option)
  }

  putStudentCollegeEnrollmentRequirement(id, data) {
    return this.http.put(this.global.api + 'Student/CollegeEnrollmentRequirement/' + id, data, this.global.option)
  }

  putStudentEnrollmentRequirement(id, sy, data) {
    return this.http.put(this.global.api + 'Student/EnrollmentRequirement/' + id + '/' + sy, data, this.global.option)
  }

  putStudentSacramentsReceived(id, data) {
    return this.http.put(this.global.api + 'Student/SacramentsReceived/' + id, data, this.global.option)
  }

  deleteStudentFamilyMember(id, id2) {
    return this.http.delete(this.global.api + 'Student/FamilyMember/' + id + '/' + id2, this.global.option)
  }

  deleteStudentEducBG(id, id2) {
    return this.http.delete(this.global.api + 'Student/EducBG/' + id + '/' + id2, this.global.option)
  }

  getStudentParent(memberIdNumber) {
    return this.http.get(this.global.api + 'Student/Parent?parentId=' + memberIdNumber, this.global.option)
  }

  getStudentEducBG(id) {
    return this.http.get(this.global.api + 'Student/EducBG/' + id, this.global.option)
  }

  putStudentBasicInfo(id, data) {
    return this.http.put(this.global.api + 'Student/BasicInfo?idNumber=' + id, data, this.global.option)
  }

  putStudentContactInfo(id, data) {
    return this.http.put(this.global.api + 'Student/ContactInfo?idNumber=' + id, data, this.global.option)
  }

  getPublicAPISchools() {
    return this.http.get(this.global.api + 'PublicAPI/Schools', this.global.option)
  }

  getStudentEducBGHS(id, data) {
    return this.http.put(this.global.api + 'Student/EducBGHS/' + id, data, this.global.option)
  }

  getStudentFamilyBG(id) {
    return this.http.get(this.global.api + 'Student/FamilyBG/' + id, this.global.option)
  }

  postStudentParentGuardian(id, data) {
    return this.http.post(this.global.api + 'Student/ParentGuardian/' + id, data, this.global.option)
  }

  putStudentParentGuardian(id, data) {
    return this.http.put(this.global.api + 'Student/ParentGuardian/' + id, data, this.global.option)
  }

  postStudentFamilyMember(id, data) {
    return this.http.post(this.global.api + 'Student/FamilyMember/' + id, data, this.global.option)
  }

  getPublicAPIProgramNames(prog) {
    return this.http.get(this.global.api + 'PublicAPI/ProgramNames/' + prog, this.global.option)
  }

  putStudentEducBG(id, data) {
    return this.http.put(this.global.api + 'Student/EducBG/' + id, data, this.global.option)
  }

  postStudentEducBG(id, data) {
    return this.http.post(this.global.api + 'Student/EducBG/' + id, data, this.global.option)
  }

  getPublicAPIProvinces() {
    return this.http.get(this.global.api + 'PublicAPI/Provinces', this.global.option)
  }

  getPublicAPITownsCities(province) {
    return this.http.get(this.global.api + 'PublicAPI/TownsCities/' + province, this.global.option)
  }

  getPublicAPIBarangays(province, town) {
    return this.http.get(this.global.api + 'PublicAPI/Barangays/' + province + '/' + town, this.global.option)
  }

  getStudentEvaluationStudentInfo(id, sy, domain) {
    return this.http.get(this.global.api + 'StudentEvaluation/StudentInfo/' + id + "/" + sy + '/' + domain, this.global.option)
  }

  getStudentEvaluationCurriculum(progid) {
    return this.http.get(this.global.api + 'StudentEvaluation/Curriculum/' + progid, this.global.option)
  }

  getStudentEvaluation(id, progid) {
    return this.http.get(this.global.api + 'StudentEvaluation/' + id + '/' + progid, this.global.option)
  }

  getStudentEvaluationAcademicHistoryEvaluation(id) {
    return this.http.get(this.global.api + 'StudentEvaluation/AcademicHistoryEvaluation/' + id, this.global.option)
  }

  getStudentEvaluationSubjectAcademicHistoryMapping(id, progid) {
    return this.http.get(this.global.api + 'StudentEvaluation/SubjectAcademicHistoryMapping/' + id + '/' + progid, this.global.option)
  }

  getStudentEvaluationStatusList() {
    return this.http.get(this.global.api + 'StudentEvaluation/StatusList', this.global.option)
  }

  postStudentEvaluation(data) {
    return this.http.post(this.global.api + 'StudentEvaluation/', data, this.global.option)
  }

  deleteStudentEvaluation(id, progid) {
    return this.http.delete(this.global.api + 'StudentEvaluation/' + id + "/" + progid, this.global.option)
  }

  postStudentEvaluationSubjectAcademicHistoryMapping(data) {
    return this.http.post(this.global.api + 'StudentEvaluation/SubjectAcademicHistoryMapping/', data, this.global.option)
  }

  postStudentEvaluationHistory(data) {
    return this.http.post(this.global.api + 'StudentEvaluation/History', data, this.global.option)
  }

  putStudentEvaluationSubjectAcademicHistoryMapping(id, data) {
    return this.http.put(this.global.api + 'StudentEvaluation/SubjectAcademicHistoryMapping/' + id, data, this.global.option)
  }

  deleteStudentEvaluationSubjectAcademicHistoryMapping(id) {
    return this.http.delete(this.global.api + 'StudentEvaluation/SubjectAcademicHistoryMapping/' + id, this.global.option)
  }
  getCodeSetHeaders(sy, value) {
    return this.http.get(this.global.api + 'Code/SetHeaders/' + sy + '/' + value, this.global.option)
  }

  deleteCodeSetHeader(id) {
    return this.http.delete(this.global.api + 'Code/SetHeader/' + id, this.global.option)
  }

  getCodeSetDetails(id) {
    return this.http.get(this.global.api + 'Code/SetDetails/' + id, this.global.option)
  }

  deleteCodeSetDetail(id) {
    return this.http.delete(this.global.api + 'Code/SetDetail/' + id, this.global.option)
  }

  getCodeSetDetailsConflictSchedules(text) {
    return this.http.get(this.global.api + 'Code/SetDetails/ConflictSchedules/' + text, this.global.option)
  }

  getEnrollmentEnrolledSubjectsCodeNo(codeno, sy, bool) {
    return this.http.get(this.global.api + 'Enrollment/EnrolledSubjects/Code No/' + codeno + '/' + sy + '/' + bool, this.global.option)
  }

  postCodeSetDetail(data) {
    return this.http.post(this.global.api + 'Code/SetDetail', data, this.global.option)
  }
  getCodeCurriculum(programId) {
    return this.http.get(this.global.api + 'Code/Curriculum/' + programId, this.global.option)
  }
  putCodeSetHeader(id, data) {
    return this.http.put(this.global.api + 'Code/SetHeader/' + id, data, this.global.option)
  }
  postCodeSetHeader(data) {
    return this.http.post(this.global.api + 'Code/SetHeader', data, this.global.option)
  }

  putStudentSection(id, data) {
    return this.http.put(this.global.api + 'Student/Section/' + id, data, this.global.option)
  }

  getEnrollmentPurgeList(sy, domain) {
    return this.http.get(this.global.api + 'Enrollment/PurgeList/' + sy + '/' + domain, this.global.option)
  }

  deleteEnrollmentPurge(sy) {
    return this.http.delete(this.global.api + 'Enrollment/Purge/' + sy, this.global.option)
  }


  deleteEnrollmentPurgeGS(sy) {
    return this.http.delete(this.global.api + 'Enrollment/PurgeGS/' + sy, this.global.option)
  }

  getOnlineRegistrationProgramLevel() {
    return this.http.get(this.global.api + 'OnlineRegistration/ProgramLevel')
  }

  getOnlineRegistrationApplicants(sy, splevel, eplevel) {
    return this.http.get(this.global.api + 'OnlineRegistration/Applicants/' + sy + '?programLevel=' + splevel + '&eProgramLevel=' + eplevel, this.global.option)
  }

  deleteOnlineRegistrationApplicant(id) {
    return this.http.delete(this.global.api + 'OnlineRegistration/Applicant/' + id, this.global.option)
  }

  getPublicAPIStrands() {
    return this.http.get(this.global.api + 'PublicAPI/Strands')
  }
  getOnlineRegistrationApplicant(sy, applicantNo) {
    return this.http.get(this.global.api + 'OnlineRegistration/Applicant/' + sy + "/" + applicantNo)
  }
  getOnlineRegistration(id) {
    return this.http.get(this.global.api + 'OnlineRegistration/' + id, this.global.option)
  }

  getOnlineRegistrationApplicantGuidance(id, data) {
    return this.http.put(this.global.api + 'OnlineRegistration/ApplicantGuidance/' + id, data, this.global.option)
  }

  getOnlineRegistrationApplicantAcctg(id, data) {
    return this.http.put(this.global.api + 'OnlineRegistration/ApplicantAcctg/' + id, data, this.global.option)
  }

  putOnlineRegistrationPlacement(id, data) {
    return this.http.put(this.global.api + 'OnlineRegistration/Placement/' + id, data, this.global.option)
  }
  putOnlineRegistrationApplicant(id, data) {
    return this.http.put(this.global.api + 'OnlineRegistration/Applicant/' + id, data, this.global.option)
  }
  getOnlineRegistrationApplicantNoUplodedProofOfPayment(sy) {
    return this.http.get(this.global.api + 'OnlineRegistration/Applicant/NoUplodedProofOfPayment/' + sy, this.global.option)
  }
  getOnlineRegistrationPaymentFollowUpSMSHist(sy, id) {
    return this.http.get(this.global.api + 'OnlineRegistration/Payment/FollowUpSMSHist/' + sy + "?applicantNo=" + id, this.global.option)

  }

  getgetphpfile(text) {
    return this.http.get(this.global.acctgApi + 'getphpfile/' + text)
  }
  postOnlineRegistrationPaymentFollowUpSMSHist(data) {
    return this.http.post(this.global.api + 'OnlineRegistration/Payment/FollowUpSMSHist', data, this.global.option)
  }

  getOnlineGradingSheetStatus(id) {
    return this.http.get(this.global.api + 'OnlineGradingSheet/Status/' + id, this.global.option)
  }

  getEmployeePortalCodeSummary(sy, cnum) {
    return this.http.get(this.global.api + 'OnlineGradingSheet/CodeNoInfo/' + sy + '/' + cnum, this.global.option)
  }


  getOnlineGradingSheet(cnum, sy) {
    return this.http.get(this.global.api + 'OnlineGradingSheet/' + cnum + '/' + sy, this.global.option)
  }

  getOnlineGradingSheetGrades(cnum, sy) {
    return this.http.get(this.global.api + 'OnlineGradingSheet/Grades/' + cnum + '/' + sy, this.global.option)
  }


  postOnlineGradingSheetStatus(data) {
    return this.http.post(this.global.api + 'OnlineGradingSheet/Status', data, this.global.option)
  }

  putOnlineGradingSheetClassEGS(cnum, data) {
    return this.http.put(this.global.api + 'OnlineGradingSheet/ClassEGS/' + cnum, data, this.global.option)
  }


  getEnrollmentPreferredLearningModality(sy, dept) {
    return this.http.get(this.global.api + 'Enrollment/PreferredLearningModality/' + sy + "?departmentId=" + dept, this.global.option)
  }

  getEnrollmentLearningModalities(sy) {
    return this.http.get(this.global.api + 'Enrollment/LearningModalities/' + sy, this.global.option)
  }

  deleteEnrollmentLearningModality(id, eid, sy) {
    return this.http.delete(this.global.api + 'Enrollment/LearningModality/' + id + '/' + eid + '/' + sy, this.global.option)
  }

  postMaintenanceCompany(data) {
    return this.http.post(this.global.api + 'Maintenance/Company', data, this.global.option)
  }
  putMaintenanceCompany(id, data) {
    return this.http.put(this.global.api + 'Maintenance/Company/' + id, data, this.global.option)
  }
  getStudentStudentLookup(keyword, domain) {
    return this.http.get(this.global.api + 'Student/StudentLookup/' + keyword + '/' + domain, this.global.option)
  }


  getEmployeeEmployeeLookup(keyword) {
    return this.http.get(this.global.api + 'Employee/EmployeeLookup/' + keyword, this.global.option)
  }

  deleteMaintenanceScheduleBasicEd(firstdata, sy) {
    return this.http.delete(this.global.api + 'Maintenance/Schedule/BasicEd/' + firstdata + '/' + sy.slice(0, -1), this.global.option)
  }

  postMaintenanceScheduleBasicEd(data) {
    return this.http.post(this.global.api + 'Maintenance/Schedule/BasicEd', data, this.global.option)
  }

  getEnrollmentHSEnrollmentStudentElective(sy) {
    return this.http.get(this.global.api + 'Enrollment/HSEnrollment/Student/Elective/' + sy, this.global.option)
  }

  getEnrollmentHSEnrollmentElectives(sy) {
    return this.http.get(this.global.api + 'Enrollment/HSEnrollment/Electives/' + sy, this.global.option)
  }
  deleteEnrollmentHSEnrollmentStudentElective(id, eid, sy) {
    return this.http.delete(this.global.api + 'Enrollment/HSEnrollment/Student/Elective/' + id + '/' + eid + '/' + sy, this.global.option)
  }

  postEnrollmentHSEnrollmentStudentElective(data) {
    return this.http.post(this.global.api + 'Enrollment/HSEnrollment/Student/Elective/', data, this.global.option)
  }
  postEnrollmentLearningModality(data) {
    return this.http.post(this.global.api + 'Enrollment/LearningModality', data, this.global.option)
  }

  getCodeExamSchedule() {
    return this.http.get(this.global.api + 'Code/ExamSchedule', this.global.option)
  }

  deleteCodeExamSchedule() {
    return this.http.delete(this.global.api + 'Code/ExamSchedule', this.global.option)
  }

  postCodeExamSchedule(data) {
    return this.http.post(this.global.api + 'Code/ExamSchedule', data, this.global.option)
  }

  getEnrollmentSetDetails(x) {
    return this.http.get(this.global.api + 'Enrollment/SetDetails/' + x, this.global.option)
  }

  deleteEnrollmentEnrolledSubjects(id, codeno, sy) {
    return this.http.delete(this.global.api + 'Enrollment/EnrolledSubjects/' + id + '/' + codeno + '/' + sy, this.global.option)
  }
  getEnrollmentEnrollment(id) {
    return this.http.get(this.global.api + 'Enrollment/Enrollment/' + id, this.global.option)
  }

  getEnrollmentSetHeaders(sy, value) {
    return this.http.get(this.global.api + 'Enrollment/SetHeaders/' + sy + '/' + value, this.global.option)
  }
  putEnrollmentWithdraw(id, sy, value, data) {
    return this.http.put(this.global.api + 'Enrollment/Withdraw/' + id + '/' + sy + '/' + value, data, this.global.option)
  }

  putEnrollmentRetractWithdrawal(id, sy, value, data) {
    return this.http.put(this.global.api + 'Enrollment/RetractWithdrawal/' + id + '/' + sy + '/' + value, data, this.global.option)
  }
  getEnrollmentConflictSchedules(text) {
    return this.http.get(this.global.api + 'Enrollment/ConflictSchedules/' + text, this.global.option)
  }

  getEnrollmentCodeRequisitesGet(text) {
    return this.http.get(this.global.api + 'Enrollment/CodeRequisitesGet/' + text, this.global.option)
  }

  postEnrollmentEnrolledSubjects(id, length, sy, data) {
    return this.http.post(this.global.api + 'Enrollment/EnrolledSubjects/' + id + '/' + length + '/' + sy, data, this.global.option)
  }
  postEnrollmentEnrolledSubjectsBypassConflict(text, data) {
    return this.http.post(this.global.api + 'Enrollment/EnrolledSubjects/BypassConflict/' + text, data, this.global.option)
  }
  getEnrollmentEnrolledSubjects(id, sy) {
    return this.http.get(this.global.api + 'Enrollment/EnrolledSubjects/' + id + '/' + sy, this.global.option)
  }
  getEnrollmentEnrolledSubjectsfind(text) {
    return this.http.get(this.global.api + 'Enrollment/EnrolledSubjects/' + text, this.global.option)
  }


  getCurriculumProgramStatus() {
    return this.http.get(this.global.api + 'Curriculum/ProgramStatus', this.global.option)
  }

  postCurriculumLookup(data) {
    return this.http.post(this.global.api + 'Curriculum/Lookup', data, this.global.option)
  }

  deleteCurriculum(id) {
    return this.http.delete(this.global.api + 'Curriculum/' + id, this.global.option)
  }


  getCurriculumSubjectPrerequisite(recordId) {
    return this.http.get(this.global.api + 'Curriculum/Subject/Prerequisite/' + recordId, this.global.option)
  }

  postCurriculumSubjectPrerequisite(data) {
    return this.http.post(this.global.api + 'Curriculum/Subject/Prerequisite/', data, this.global.option)
  }

  deleteCurriculumSubjectPrerequisite(recordId, rid) {
    return this.http.delete(this.global.api + 'Curriculum/Subject/Prerequisite/' + recordId + "/" + rid, this.global.option)
  }

  getCurriculumSubjectOtherRequisite(recordId) {
    return this.http.get(this.global.api + 'Curriculum/Subject/OtherRequisite/' + recordId, this.global.option)
  }

  postCurriculumSubjectOtherRequisite(data) {
    return this.http.post(this.global.api + 'Curriculum/Subject/OtherRequisite', data, this.global.option)
  }

  deleteCurriculumSubjectOtherRequisite(id) {
    return this.http.delete(this.global.api + 'Curriculum/Subject/OtherRequisite/' + id, this.global.option)
  }

  getCurriculumSubjects(progid) {
    return this.http.get(this.global.api + 'Curriculum/Subjects/' + progid, this.global.option)
  }

  getCurriculum(id) {
    return this.http.get(this.global.api + 'Curriculum/' + id, this.global.option)
  }
  deleteCurriculumSubject(id, rid) {
    return this.http.delete(this.global.api + 'Curriculum/Subject/' + id + "?recordId=" + rid, this.global.option)
  }

  getCurriculumSubjectListWithPrerequisites(id) {
    return this.http.get(this.global.api + 'Curriculum/SubjectListWithPrerequisites/' + id, this.global.option)
  }


  deleteCurriculumSubjectPreRequisite(id, rid) {
    return this.http.delete(this.global.api + 'Curriculum/Subject/Prerequisite/' + id + "/" + rid, this.global.option)
  }
  getCurriculumSubjectType() {
    return this.http.get(this.global.api + 'Curriculum/SubjectType', this.global.option)
  }


  getCurriculumSubject(recordId) {
    return this.http.get(this.global.api + 'Curriculum/Subject/' + recordId, this.global.option)
  }

  postCurriculumSubject(data) {
    return this.http.post(this.global.api + 'Curriculum/Subject', data, this.global.option)
  }
  putCurriculumSubject(id, data) {
    return this.http.put(this.global.api + 'Curriculum/Subject/' + id, data, this.global.option)
  }

  getCurriculumRetentionPolicy(id) {
    return this.http.get(this.global.api + 'Curriculum/RetentionPolicy/' + id, this.global.option)
  }
  deleteCurriculumRetentionPolicy(rid) {
    return this.http.delete(this.global.api + 'Curriculum/RetentionPolicy/' + rid, this.global.option)
  }

  putCurriculumRetentionPolicy(id, data) {
    return this.http.put(this.global.api + 'Curriculum/RetentionPolicy/' + id, data, this.global.option)
  }

  postCurriculumRetentionPolicy(data) {
    return this.http.post(this.global.api + 'Curriculum/RetentionPolicy', data, this.global.option)
  }

  getCurriculumCurriculumRetentionPolicySubjectType(programId) {
    return this.http.get(this.global.api + 'Curriculum/CurriculumRetentionPolicySubjectType/' + programId, this.global.option)
  }

  deleteCurriculumCurriculumRetentionPolicySubjectType(rid) {
    return this.http.delete(this.global.api + 'Curriculum/CurriculumRetentionPolicySubjectType/' + rid, this.global.option)
  }

  putCurriculumCurriculumRetentionPolicySubjectType(data) {
    return this.http.post(this.global.api + 'Curriculum/CurriculumRetentionPolicySubjectType', data, this.global.option)
  }

  postCurriculumCurriculumRetentionPolicySubjectType(data) {
    return this.http.post(this.global.api + 'Curriculum/CurriculumRetentionPolicySubjectType', data, this.global.option)
  }

  getCurriculumCourses(proglevel) {
    return this.http.get(this.global.api + 'Curriculum/Courses/' + proglevel, this.global.option)
  }

  getCurriculumProgramLevel() {
    return this.http.get(this.global.api + 'Curriculum/ProgramLevel', this.global.option)
  }


  getCurriculumProgramDiscipline() {
    return this.http.get(this.global.api + 'Curriculum/ProgramDiscipline', this.global.option)
  }

  getCurriculumExamLevelMatrix() {
    return this.http.get(this.global.api + 'Curriculum/ExamLevelMatrix', this.global.option)
  }

  getCurriculumAccreditation() {
    return this.http.get(this.global.api + 'Curriculum/Accreditation', this.global.option)
  }

  getCurriculumProgramNormalLength() {
    return this.http.get(this.global.api + 'Curriculum/ProgramNormalLength', this.global.option)
  }

  getCurriculumStrand() {
    return this.http.get(this.global.api + 'Curriculum/Strand', this.global.option)
  }

  postCurriculum(data) {
    return this.http.post(this.global.api + 'Curriculum', data, this.global.option)
  }
  putCurriculum(id, data) {
    return this.http.put(this.global.api + 'Curriculum/' + id, data, this.global.option)
  }

  deleteEnrollmentPreEnrollmentSubmittedRequirement(id, sy) {
    return this.http.delete(this.global.api + 'Enrollment/PreEnrollment/SubmittedRequirement/' + id + '/' + sy, this.global.option)
  }

  getEnrollmentPreEnrollmentSubmittedRequirement(text) {
    return this.http.get(this.global.api + 'Enrollment/PreEnrollment/SubmittedRequirement/' + text, this.global.option)
  }
  getEnrollmentEncryptedString(idNumber) {
    return this.http.get(this.global.api + 'Enrollment/EncryptedString/' + idNumber, this.global.option)
  }
  putEnrollmentPreEnrollmentSubmittedRequirement(text, data) {
    return this.http.put(this.global.api + 'Enrollment/PreEnrollment/SubmittedRequirement/' + text, data, this.global.option)
  }
  putStudentPicture(id, data) {
    return this.http.put(this.global.api + 'Student/Picture/' + id, data, this.global.option)
  }
  putEnrollmentPreEnrollmentSubmittedRequirementAcctg(text, data) {
    return this.http.put(this.global.api + 'Enrollment/PreEnrollment/SubmittedRequirement/Acctg/' + text, data, this.global.option)
  }
  putEnrollmentPreEnrollmentSubmittedRequirementOSAS(text, data) {
    return this.http.put(this.global.api + 'Enrollment/PreEnrollment/SubmittedRequirement/OSAS/' + text, data, this.global.option)
  }
  postAdmissionAdmitStudent(data) {
    return this.http.post(this.global.api + 'Admission/AdmitStudent', data, this.global.option)
  }

  //added by dbadmin (loui)
  //#region
  //---------------------------------------------------------------------------------------------------------------------//
  getReportSummaryStudentProfileAge(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileAge/' + sy + '/' + proglevel, this.global.option)
  }
  getReportSummaryStudentProfileGender(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileGender/' + sy + '/' + proglevel, this.global.option)
  }
  getReportSummaryStudentProfileReligion(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileReligion/' + sy + '/' + proglevel, this.global.option)
  }
  getReportSummaryStudentProfileHomePlace(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileHomePlace/' + sy + '/' + proglevel, this.global.option)
  }
  getReportSummaryStudentProfileCollegeSHSGraduatedFrom(sy) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileCollegeSHSGraduatedFrom/' + sy, this.global.option)
  }
  getReportSummaryStudentProfileSHSJHSGraduatedFrom(sy) {
    return this.http.get(this.global.api + 'ReportSummary/StudentProfileSHSJHSGraduatedFrom/' + sy, this.global.option)
  }
  getReportSummaryStudentCorporateEmailAll(sy, proglevel) {
    return this.http.get(this.global.api + 'ReportSummary/CorporateEmailAll/' + sy + '/' + proglevel, this.global.option)
  }

  getCorporateEmailDetails(idNumber) {
    return this.http.get(this.global.api + 'Account/CorporateEmail/' + idNumber, this.global.option)
  }

  postCorporateEmailCreate(data) {
    return this.http.post(this.global.api + 'Account/CorporateEmail', data, this.global.option)
  }

  deleteCorporateEmail(id) {
    return this.http.delete(this.global.api + 'Account/CorporateEmail/' + id, this.global.option)
  }
  getReportSummaryAcademicAchievers(csy, doNotIncludePic) {
    return this.http.get(this.global.api + 'ReportSummary/AcademicAchievers/' + csy + "?doNotIncludePic=" + doNotIncludePic, this.global.option)
  }
  getReportSummaryAcademicAchieversWithPic(csy, DepartmentID, doNotIncludePic) {
    return this.http.get(this.global.api + 'ReportSummary/AcademicAchievers/' + csy + "?DepartmentID=" + DepartmentID + "?doNotIncludePic=" + doNotIncludePic, this.global.option)
  }

  getHRISMaintenanceEmpStatus() {
    return this.http.get(this.global.api + 'HRISMaintenance/EmpStatus/', this.global.option);
  }

  getHRISMaintenanceEmpType() {
    return this.http.get(this.global.api + 'HRISMaintenance/EmpType/', this.global.option);
  }

  getHRISMaintenanceRank() {
    return this.http.get(this.global.api + 'HRISMaintenance/Rank/', this.global.option);
  }

  getMaintenanceActiveSY() {
    return this.http.get(this.global.api + 'Maintenance/ActiveSchoolYear/', this.global.option);
  }

  putMaintenanceActiveSY(data) {
    return this.http.put(this.global.api + 'Maintenance/ActiveSchoolYear/', data, this.global.option);
  }

  getAccountUser(idNumber, includeIDPic) {
    return this.http.get(this.global.api + 'Account/User/' + idNumber + '/' + "?includeIDPic=" + includeIDPic, this.global.option)
  }

  putEmployee(idnumber, data) {
    return this.http.put(this.global.api + 'Employee/' + idnumber, data, this.global.option)
  }

  postEmployee(data) {
    return this.http.post(this.global.api + 'Employee', data, this.global.option)
  }

  getEmployeeAll(active) {
    return this.http.get(this.global.api + 'Employee/All/' + active, this.global.option)
  }

  getEmployeeByDTRID(dtrID) {
    return this.http.get(this.global.api + 'Employee/ByDTRId/' + dtrID, this.global.option)
  }

  postRegisterEmployee(data) {
    return this.http.post(this.global.api + 'Employee/RegisterEmployee', data, this.global.option)
  }

  putLockUnlockEmployeeAccount(id, x) {
    return this.http.put(this.global.api + 'Employee/LockUnlockEmployeeAccount/' + id + '/' + x, {}, this.global.option)
  }

  putOnlineRegSetting(data) {
    return this.http.put(this.global.api + 'Maintenance/OnlineRegistration/Settings/', data, this.global.option);
  }

  putGradeHideUnhide(data) {
    return this.http.put(this.global.api + 'Student/GradeHideUnhide/', data, this.global.option);
  }

  getOnlineApplicants(schoolYear, submittedReq, level, subLevel = '') {
    return this.http.get(this.global.api + 'Maintenance/OnlineApplicant/' + schoolYear + '/' + submittedReq + '/' + level + '/' + "?subLevel=" + subLevel, this.global.option)
  }

  putOnlineApplicant(data) {
    return this.http.put(this.global.api + 'Maintenance/OnlineApplicant/', data, this.global.option);
  }

  getPreEnrolledStudents(schoolYear, submittedReq, level, subLevel = '') {
    return this.http.get(this.global.api + 'Maintenance/PreEnrolledStudent/' + schoolYear + '/' + submittedReq + '/' + level + '/' + "?subLevel=" + subLevel, this.global.option)
  }

  putreEnrolledStudent(data) {
    return this.http.put(this.global.api + 'Maintenance/PreEnrolledStudent/', data, this.global.option);
  }

  getSYOptionDates(sy) {
    return this.http.get(this.global.api + 'Maintenance/SYOptionDate/' + sy, this.global.option);
  }

  postSYOptionDate(data) {
    return this.http.post(this.global.api + 'Maintenance/SYOptionDate/', data, this.global.option);
  }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

  //FACULTY EVALUATION
  //#region
  //---------------------------------------------------------------------------------------------------------------------//
  getFacultyLoad(sy, proglevel, facultyID, archived) {
    var syear = this.global.syear;
    if (proglevel != 'COLLEGE' && 'GRADUATE SCHOOL') {
      syear = this.global.syear.substring(0, 6);
      return this.http.get(this.global.api + 'FacultyEvaluation/FacultyLoad/' + syear + '/' + proglevel + '/' + facultyID + '?archived=' + archived, this.global.option)
    } else
      return this.http.get(this.global.api + 'FacultyEvaluation/FacultyLoad/' + syear + '/' + proglevel + '/' + facultyID, this.global.option)
  }

  postFacultyEvalCreateEvalSheet(data) {
    return this.http.post(this.global.api + 'FacultyEvaluation/EvaluationSheet/', data, this.global.option);
  }

  deleteFacultyEvaluation(id) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/EvaluationSheet/' + id, this.global.option)
  }
  getFacultyEvalSheet(sy, proglevel, facultyID) {
    var syear = this.global.syear;
    if (proglevel != 'COLLEGE' && 'GRADUATE SCHOOL')
      syear = this.global.syear.substring(0, 6);

    return this.http.get(this.global.api + 'FacultyEvaluation/EvaluationSheet/' + syear + '/' + proglevel + '/' + facultyID, this.global.option)
  }

  //10/04/2022
  getFullNameByID(IDNumber) {
    return this.http.get(this.global.api + 'Maintenance/PersonFullName/' + IDNumber, this.global.option)
  }

  //10/06/2022
  getFacultyListbyProgramID(programID, id, facultyID) {
    return this.http.get(this.global.api + 'FacultyEvaluation/ProgramProgramChairFaculty/' + programID + '?id=' + id + '&facultyID=' + facultyID, this.global.option)
  }

  //10/11/2022
  postFacultyList(programID, facultyID) {
    return this.http.post(this.global.api + 'FacultyEvaluation/ProgramProgramChairFaculty/' + programID + '/' + facultyID, {}, this.global.option)
  }

  //10/11/2022
  deleteFacultyList(courseCode) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/ProgramProgramChairFaculty/All/' + courseCode, this.global.option)
  }

  //10/12/2022
  deleteFacultyIndividual(id) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/ProgramProgramChairFaculty/One/' + id, this.global.option)
  }

  //10/13/2022
  getEmployeeFacultyLookup(keyword) {
    return this.http.get(this.global.api + 'FacultyEvaluation/EmployeeLookup/' + keyword, this.global.option)
  }

  //10/19/2022
  putProgramChairUpdate(programID, programChairID) {
    return this.http.put(this.global.api + 'FacultyEvaluation/ProgramProgramChairUpdate/' + programID + '/' + programChairID, {}, this.global.option)
  }

  getFacultyEvalPersonLookup(keyword) {
    return this.http.get(this.global.api + 'FacultyEvaluation/EmployeeLookup/' + keyword, this.global.option)
  }

  getFacultyEvalPersonIDInfo(id): Observable<any> {
    return this.http.get(this.global.api + 'FacultyEvaluation/Person/IDInfo/' + id, this.global.option)
  }

  getFacultyEvalPerson(id) {
    return this.http.get(this.global.api + 'FacultyEvaluation/Person/' + id, this.global.option)
  }
  getFacultyEvalSelect(headID, facultyID) {
    if (headID)
      return this.http.get(this.global.api + 'FacultyEvaluation/ProgramAreaFaculty_Select?headID=' + headID + '&facultyID=' + facultyID, this.global.option)
    else
      return this.http.get(this.global.api + 'FacultyEvaluation/ProgramAreaFaculty_Select?facultyID=' + facultyID, this.global.option)

  }

  getFacultyEvalEnrolledStudentsInACodeNoSubject(progLevel, schedType, codeNo, subject, section) {

    var sy = this.global.syear;
    if (progLevel != 'COLLEGE' && progLevel != 'GRADUATE SCHOOL') {
      sy = this.global.syear.substring(0, 6);
      return this.http.get(this.global.api + 'FacultyEvaluation/EnrolledStudentsInACodeNoSubject/' + sy + '/' + progLevel + '/' + schedType + '?subject=' + subject + '&section=' + section, this.global.option)
    } else {
      return this.http.get(this.global.api + 'FacultyEvaluation/EnrolledStudentsInACodeNoSubject/' + sy + '/' + progLevel + '/' + schedType + '?codeNo=' + codeNo, this.global.option)
    }
  }

  getFacultyEvalDevelopingPointsSummary(progLevel, schedType, facultyID, archived) {
    var sy = this.global.syear;
    if (progLevel != 'COLLEGE' && progLevel != 'GRADUATE SCHOOL')
      sy = this.global.syear.substring(0, 6);
    return this.http.get(this.global.api + 'FacultyEvaluation/DevelopingPointsSummary/' + sy + '/' + progLevel + '/' + schedType + '/' + facultyID + '/' + archived, this.global.option)
  }
  //10/27/2022
  postArea(data) {
    return this.http.post(this.global.api + 'FacultyEvaluation/Area', data, this.global.option)
  }

  //10/27/2022
  getArea(id, areaName, deptID, headID) {
    return this.http.get(this.global.api + 'FacultyEvaluation/Area/' + "?id=" + id + "&areaName=" + areaName + "&deptID=" + deptID + "&headID=" + headID, this.global.option)
  }

  //10/28/2022
  deleteArea(id) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/Area/' + id, this.global.option)
  }

  //11/07/2022
  updateArea(areaID, schoolYear, programLevel, data) {
    return this.http.put(this.global.api + 'FacultyEvaluation/Area/' + areaID + '/' + schoolYear + '/' + programLevel, data, this.global.option)
  }

  //11/08/2022
  postAreaFaculty(areaID, facultyID) {
    return this.http.post(this.global.api + 'FacultyEvaluation/AreaFaculty/' + areaID + '/' + facultyID, {}, this.global.option)
  }

  //11/08/2022
  getAreaFacultyList(id, areaName, deptID, headID, areaID) {
    return this.http.get(this.global.api + 'FacultyEvaluation/AreaFaculty/' + '?id=' + id + '&areaName=' + areaName + '&deptID=' + deptID + '&headID=' + headID + "&areaID=" + areaID, this.global.option)
  }

  //11/08/2022
  deleteAreaFacultyIndividual(id) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/AreaFacultyOne/' + id, this.global.option)
  }

  //10/11/2022
  deleteAreaFacultyList(areaID) {
    return this.http.delete(this.global.api + 'FacultyEvaluation/AreaFacultyAll/' + areaID, this.global.option)
  }

  //11/17/2022
  getFacEvalEmployee(id) {
    return this.http.get(this.global.api + 'FacultyEvaluation/Employee/' + id, this.global.option)
  }

  //11/25/22
  getTransmutedValueAndInterpretation(average, toolID) {
    //toolID = 1: lecture | toolID = 2: laboratory
    return this.http.get(this.global.api + 'FacultyEvaluation/TransmutedValueAndInterpretation/' + parseFloat(average) + '/' + toolID, this.global.option)
  }

  getFacultyEvalAdditionalQuestions(progLevel, facultyID, archived) {
    var sy = this.global.syear;
    if (progLevel != 'COLLEGE' && progLevel != 'GRADUATE SCHOOL')
      sy = this.global.syear.substring(0, 6);
    return this.http.get(this.global.api + 'FacultyEvaluation/EvaluationSheetAnswerAdditionalQuestionList/' + sy + '/' + facultyID + '/' + archived + '/' + progLevel, this.global.option)
  }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

  //BASIC ED UPLOADER
  //#region
  //---------------------------------------------------------------------------------------------------------------------//

  //9/15/2022
  getBasicEdSchedules(schooYear, yearLevel, section) {
    return this.http.get(this.global.api + 'Maintenance/Schedule/BasicEd/' + schooYear + '?yearLevel=' + yearLevel + '&section=' + section, this.global.option)
  }

  //9/23/2022
  getSection(deptInitial, yearLevel) {
    return this.http.get(this.global.api + 'PublicAPI/Sections/' + "?deptInitial=" + deptInitial + "&yearLevel=" + yearLevel, this.global.option)
  }

  //Loui//10142022---------------Faculty Evaluation Settings----------------------------------//
  geFacultyEvaluationSettings(deptID) {
    return this.http.get(this.global.api + 'FacultyEvaluation/FacultyEvalSetting/' + deptID, this.global.option)
  }
  //10/25/2022
  deleteSection(schoolYear, gradeLevel, section) {
    return this.http.delete(this.global.api + 'Maintenance/Schedule/BasicEd/' + schoolYear + '/' + gradeLevel + '/' + section, this.global.option)
  }

  //11/10/2022
  uploadSection(data) {
    return this.http.post(this.global.api + 'Maintenance/Schedule/BasicEd', data, this.global.option)
  }

  //11/22/2022
  putClassAdviser(sectionID, adviserID) {
    return this.http.put(this.global.api + 'Maintenance/Section/' + sectionID + '/' + adviserID, {}, this.global.option)
  }

  //11/22/2022
  getClassAdviser(sectionID) {
    return this.http.get(this.global.api + 'Maintenance/Section/' + sectionID, this.global.option)
  }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion


  putFacultyEvaluationSetting(deptID, data) {
    return this.http.put(this.global.api + 'FacultyEvaluation/FacultyEvalSetting/' + deptID, data, this.global.option)
  }
  //------------------------------------------------------------------------------------------//

  //Mico//01232023--------------------Tools---------------------------------------------------//
  getCodeLookUpSubjectsfind(text) {
    return this.http.get(this.global.api + 'Tools/CodeLookup/' + text, this.global.option)
  }

  getPersonFinder(lname, bool) {
    return this.http.get(this.global.api + 'Tools/PersonFinder/' + lname + "/" + bool, this.global.option)
  }
  //------------------------------------------------------------------------------------------//

  //Mico//02012023--------------------Reports/HRIS/DTR-viewer---------------------------------------------------//
  getDTR(id, Sdate, Edate) {
    return this.http.get(this.global.api + 'ReportHRIS/DTR/' + id + '/' + Sdate + '/' + Edate, this.global.option)
  }
  getDTRPersonIDInfo(id): Observable<any> {
    return this.http.get(this.global.api + 'ReportHRIS/PersonIDInfo/' + id, this.global.option)
  }
  getDTREmployee(id) {
    return this.http.get(this.global.api + 'ReportHRIS/EmployeeBasicInfo/' + id, this.global.option)
  }

  //------------------------------------------------------------------------------------------//
  //-----------------------------------Chris/2/2/2023------------------------------------------//

  getStudentInfoGHU(id, schoolYear) {
    return this.http.get(this.global.api + 'GradeHideUnhide/StudentInfo/' + id + "/" + schoolYear, this.global.option)
  }

  getAcademicHistoryGHU(id) {
    return this.http.get(this.global.api + 'GradeHideUnhide/AcademicHistory/' + id, this.global.option)
  }

  getPersonInformationGHU(id) {
    return this.http.get(this.global.api + 'GradeHideUnhide/Person/' + id, this.global.option)
  }
  getStudentIncompletePayment(id, schoolYear) {
    return this.http.get(this.global.api + 'GradeHideUnhide/' + id + "/" + schoolYear, this.global.option)
  }

  postStudentIncompletePayment(data) {
    return this.http.post(this.global.api + 'GradeHideUnhide/', data, this.global.option)
  }

  putStudentIncompletePayment(data) {
    return this.http.put(this.global.api + 'GradeHideUnhide/', data, this.global.option)
  }

  //------------------------------------------------------------------------------------------//


  // ------------------------Glenn/ClassroomManager/02/02/23-------------------//
  getClassroomTypes() {
    return this.http.get(this.global.api + 'Classroom/ClassroomTypes', this.global.option)
  }
  getClassroom(roomno) {
    return this.http.get(this.global.api + 'Classroom/' + roomno, this.global.option)
  }
  getClassroomList() {
    return this.http.get(this.global.api + 'Classroom/List', this.global.option)
  }
  postClassroom(data) {
    return this.http.post(this.global.api + 'Classroom/', data, this.global.option)
  }
  putClassroom(roomNo, data) {
    return this.http.put(this.global.api + 'Classroom/' + roomNo, data, this.global.option)
  }
  deleteClassroom(roomNo) {
    return this.http.delete(this.global.api + 'Classroom/' + roomNo, this.global.option)
  }

  // ------------------------Chris/ClassroomManager//03/03/23-------------------//


  getClassroomSchedule(schoolYear, roomNumber) {
    return this.http.get(this.global.api + 'Classroom/ClassroomSchedule/' + schoolYear + '?roomNo=' + roomNumber, this.global.option)
  }

  getClassroomAvailable(schoolYear, roomNo, startTime, endTime, day, type) {
    return this.http.get(this.global.api + 'Classroom/ClassroomAvailability/' + schoolYear + '?' + 'roomNo=' + roomNo + '&startTime=' + startTime + '&endTime=' + endTime + '&day=' + day + '&type=' + type, this.global.option)
  }
  //----------------------------------------------0------------------------------//

  // ------------------------JUDY/ORManager//3/22/23-------------------//

  getOfficialReceiptInfo(transNumber) {
    return this.http.get(this.global.api + 'FMIS_OfficialReceipt/ORInfo?transNumber=' + transNumber, this.global.option)
  }
  postOfficialReceipt(id) {
    return this.http.post(this.global.api + 'FMIS_OfficialReceipt/PostOR/' + id, {}, this.global.option)
  }
  putOfficialReceiptCancelled(transNumber, status, remarks) {
    return this.http.put(this.global.api + 'FMIS_OfficialReceipt/CancelOR/' + transNumber + '/' + status + '/' + remarks, {}, this.global.option)
  }
  //----------------------------------------------------------------------------//



  //---------------------------------------Glenn/FMIS_POS/03/09/2023/----------------------//

  getPOS_StudentInfo(idNumber, schoolyear, department) {
    return this.http.get(this.global.api + 'FMIS_POS/StudentInfo/' + idNumber, this.global.option)
  }
  getPOS_Lookup(keyword, exactmatch) {
    return this.http.get(this.global.api + 'FMIS_POS/PersonLookup/' + keyword + '/' + exactmatch, this.global.option)
  }
  getPOS_Assessment(idNumber, schoolyear) {
    return this.http.get(this.global.api + 'FMIS_POS/Assessment/' + idNumber + '/' + schoolyear, this.global.option)
  }
  getPOS_AssessmentNew(idNumber, schoolyear) {
    return this.http.get(this.global.api + 'FMIS_POS/AssessmentNew/' + idNumber + '/' + schoolyear, this.global.option)
  }
  getPOS_DuesBreakdown(idNumber, schoolyear) {
    return this.http.get(this.global.api + 'FMIS_POS/DuesBreakdown/' + idNumber + '/' + schoolyear, this.global.option)
  }
  getPOS_DuesParticulars() {
    return this.http.get(this.global.api + 'FMIS_POS/Particulars', this.global.option)
  }
  //--------------------------------------------------------------------------------------//

  //FMIS Accounts Verification Manager --added by Kurtyy 21/03/2023
  //#region

  checkPerson(idNumber) {
    return this.http.get(this.global.api + 'FMIS_PaymentVerification/Person/' + idNumber, this.global.option)
  }

  getPaymentVerification(schoolYear) {
    return this.http.get(this.global.api + 'FMIS_PaymentVerification/PaymentVerification/' + schoolYear, this.global.option)
  }

  postPaymentVerification(idNumber, schoolYear) {
    return this.http.post(this.global.api + 'FMIS_PaymentVerification/PaymentVerification/' + idNumber + '/' + schoolYear, {}, this.global.option)
  }

  updatePaymentVerification(recordId, status, remarks) {
    return this.http.put(this.global.api + 'FMIS_PaymentVerification/PaymentVerification/' + recordId + '/' + status + '/' + remarks, this.global.option)
  }

  deletePaymentVerification(recordId) {
    return this.http.delete(this.global.api + 'FMIS_PaymentVerification/PaymentVerification/' + recordId, this.global.option)
  }

  getPaymentVerificationPersonLookup(keyword, exactmatch) {
    return this.http.get(this.global.api + 'FMIS_PaymentVerification/Person/Lookup/' + keyword + '/' + exactmatch, this.global.option)
  }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

  //ACADEMICS Registrar Certification --added by Kurtyy 27/03/2023
  //#region

  getRegistrarCertification(idNumber, schoolYear) {
    return this.http.get(this.global.api + 'Student/EnrolledSubjectsCertification/' + idNumber + '/' + schoolYear, this.global.option)
  }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

//---------------------------------------------OTR/04/14/23--------------------------------------------------------------//
getOTRInfo(idNumber){
  return this.http.get(this.global.api + 'OTR/OTRInfo/' + idNumber, this.global.option)
}
getOTREnrolledCourse(idNumber){
  return this.http.get(this.global.api + 'OTR/Student/EnrolledCourse/' + idNumber, this.global.option)
}
getOTRDetails(idNumber, otrType){
  return this.http.get(this.global.api + 'OTR/OTRDetails/' + idNumber +'/' + otrType, this.global.option)
}
putOTRDetails(idNumber, data){
  return this.http.put(this.global.api + 'OTR/OTRDetails/' + idNumber , data, this.global.option)
}
getPersonInfo(idNumber){
  return this.http.get(this.global.api+ 'Student/PersonInfo/' + idNumber, this.global.option)
}
getEducBg(idNumber){
  return this.http.get(this.global.api+ 'Student/EducBG/' + idNumber, this.global.option)
}
getPersonInformation(idNumber){
  return this.http.get(this.global.api + 'Student/PersonInfo/' + idNumber, this.global.option)
}
getAdmissionCred(idNumber, otrType){
  return this.http.get(this.global.api + 'OTR/OTRAdmissionCredential/' + idNumber +'/' + otrType, this.global.option)
}
getStudentInfo(idNumber, schoolYear, level){
  return this.http.get(this.global.api + 'Student/' + idNumber + '/' + schoolYear + '/' + level, this.global.option)
}
getRegistrarCertificationInfo(idNumber, schoolYear) {
  return this.http.get(this.global.api + 'Student/EnrolledSubjectsCertification/' + idNumber + '/' + schoolYear, this.global.option)
}
getGraduationRecord(idNumber){
  return this.http.get(this.global.api +'OTR/GraduationRecord/' + idNumber, this.global.option)
}
putGraduationRecord(idNumber, data){
  return this.http.put(this.global.api + 'OTR/GraduationRecord/' + idNumber , data, this.global.option)
}
deleteGradudationRecord(idNumber, programID){
  return this.http.delete(this.global.api + 'OTR/GraduationRecord/' + idNumber + '/' + programID, this.global.option)
}


  // getInfo(idNumber, schoolYear, level){
  //   return this.http.get(this.global.api + 'Student/' + idNumber + '/' + schoolYear + '/' + level, this.global.option)
  // }
  //
  //DTR Uploader --added by Kurtyy 11/04/2023
  //#region

  getEmployeeInfoByDtrId(dtrID) {
    return this.http.get(this.global.api + 'DTRUploader/Employee/' + dtrID, this.global.option)
  }

  getDtrDetails(idNumber, sDate, eDate) {
    return this.http.get(this.global.api + 'DTRUploader/DTR/' + idNumber + '/' + sDate + '/' + eDate, this.global.option)
  }

  postEmployeeDtr(dtrID, timeRecord) {
    return this.http.post(this.global.api + 'DTRUploader/Attendace/' + dtrID + '/' + timeRecord, {}, this.global.option)
  }

  // ------------------------Abrito/CheckDisbursement/03/08/23-------------------//
  getCheckBooks() {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckBooks', this.global.option)
  }
  getCheckBookTypes() {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckBookTypes', this.global.option)
  }
  getCheckPaymentStatuses() {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckPaymentStatuses', this.global.option)
  }
  getCheckPayments(year) {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckPayments/' + year, this.global.option)
  }
  getCheckPaymentStatusHistory(transactionID) {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckPaymentStatusHistory/' + transactionID, this.global.option)
  }
  getCheckPaymentTemp() {
    return this.http.put(this.global.api + 'FMIS_CheckDisbursement/CheckPaymentTemp', this.global.option)
  }
  getCheckPaymentYear() {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/CheckPaymentYear', this.global.option)
  }
  getNextCheckNo(CBTypeCode) {
    return this.http.get(this.global.api + 'FMIS_CheckDisbursement/NextCheckNo/' + CBTypeCode, this.global.option)
  }
  //-------------------------------------------------------------------------------------//


  // --------- Abrito - Withdraw Conditionallt Enrolled Students 05/10/2023 -------------------//
  getEnrollmentListToBeWithdrawn(sy, proglevel, condition) {
    return this.http.get(this.global.api + 'Enrollment/EnrollmentListToBeWithdrawn/' + sy + '/' + proglevel + '/' + condition, this.global.option)
  }
  updateStatusPaidToAdmitted(idnumber, schoolyear) {
    return this.http.put(this.global.api + 'Enrollment/UpdateStatusPaidToAdmitted/' + idnumber + '/' + schoolyear, '', this.global.option)
  }
  updateStatusAdmittedToPaid(idnumber, schoolyear) {
    return this.http.put(this.global.api + 'Enrollment/UpdateStatusAdmittedToPaid/' + idnumber + '/' + schoolyear, '', this.global.option)
  }
  //-------------------------------------------------------------------------------------//

  // deleteEmployeeDtr(){
  //   return this.http.get(this.global.api + 'DTRUploader/Employee/' + dtrID +, this.global.option)
  // }

  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion

  //Codes Manager --added by Kurtyy 17/04/2023
  //#region

  postCode(requestBody) {
    return this.http.post(this.global.api + 'CodesManager/Code', requestBody, this.global.option);
  }

  getSYSettings(schoolYear) {
    return this.http.get(this.global.api + 'CodesManager/SYSettings/' + schoolYear, this.global.option);
  }

  getCodes(schoolYear, codeNo) {
    return this.http.get(this.global.api + 'CodesManager/Codes/' + schoolYear + '?codeNo=' + codeNo, this.global.option);
  }

  updateCode(codeNo, updatedCodeValue) {
    return this.http.put(this.global.api + 'CodesManager/Code/' + codeNo, updatedCodeValue, this.global.option);
  }

  getCodeCourse(schoolYear, codeNo) {
    return this.http.get(this.global.api + 'CodesManager/CodeSubject/' + schoolYear + '?codeNo=' + codeNo, this.global.option);
  }

  getCodeSchedules(schoolYear, codeNo) {
    return this.http.get(this.global.api + 'CodesManager/CodeSubjectSchedule/' + schoolYear + '/' + codeNo, this.global.option);
  }

  getCodeSeries(codeNo) {
    return this.http.get(this.global.api + 'CodesManager/LastCodeNoInserted/' + codeNo, this.global.option);
  }

  subjectLookupByTitle(subjectTitle) {
    return this.http.get(this.global.api + 'CodesManager/SubjectLookup' + '?searchByTitle=' + 1 + '&subjectTitle=' + subjectTitle, this.global.option);
  }

  subjectLookupById(subjectID) {
    return this.http.get(this.global.api + 'CodesManager/SubjectLookup' + '?searchByTitle=' + 0 + '&subjectID=' + subjectID, this.global.option);
  }

  getSchedConflict(roomOrFaculty, syear, requestBody) {
    return this.http.post(this.global.api + 'CodesManager/SchedConflict/' + syear + '/' + roomOrFaculty, requestBody, this.global.option);
  }

  getAvailableInstructors() {
    return this.http.get(this.global.api + 'CodesManager/Instructors', this.global.option);
  }

  assignCourseToCode(codeNo, subjectRecordID, schoolYear) {
    return this.http.post(this.global.api + 'CodesManager/CodeSubject/' + codeNo + '/' + subjectRecordID + '/' + schoolYear, {}, this.global.option);
  }

  assignScheduleToCode(requestBody) {
    return this.http.post(this.global.api + 'CodesManager/CodeSubjectSchedule', requestBody, this.global.option);
  }

  editCourseSchedule(schoolYear, requestBody) {
    return this.http.put(this.global.api + 'CodesManager/CodeSubjectSchedule/' + schoolYear, requestBody, this.global.option);
  }

  deleteCourseSchedule(recordID, schoolYear) {
    return this.http.delete(this.global.api + 'CodesManager/CodeSubjectSchedule/' + schoolYear + '/' + recordID, this.global.option);
  }


  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion


  //SMS Facility --added by Mico July/05/2023
  //#region



  getSMSLeft(param) {
    return this.http.post(this.global.api + 'Placement/SMSCreditLeft/' + param, '', this.global.option);

  }




  //---------------------------------------------------------------------------------------------------------------------//
  //#endregion


  //#region Reports: Requirement-SchoolGraduated-LR Lists//
  getStudentRequirementChecklist(schoolYear, level) {
    return this.http.get(this.global.api + 'ReportSummary/Requirement/' + schoolYear + '/' + level, this.global.option);
  }
  getLRNChecklist(schoolYear, level) {
    return this.http.get(this.global.api + 'ReportSummary/LearnerReferenceList/' + schoolYear + '/' + level, this.global.option);
  }

  getSchoolGradfromChecklist(schoolYear, level) {
    return this.http.get(this.global.api + 'ReportSummary/SchoolGraduatedFrom/' + schoolYear + '/' + level, this.global.option);
  }

  //#endregion
}
