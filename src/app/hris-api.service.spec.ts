import { TestBed } from '@angular/core/testing';

import { HrisApiService } from './hris-api.service';

describe('HrisApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HrisApiService = TestBed.get(HrisApiService);
    expect(service).toBeTruthy();
  });
});
