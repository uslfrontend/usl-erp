import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { GlobalService } from './../global.service';

@Injectable({
  providedIn: 'root'
})
export class FmisAPIService {

  constructor(private http: Http, public global: GlobalService) { }

  //---------------------------------------Abrito/FMIS/AR/Assessment Setup----------------------//

  postARSetUpSchoolYear(schoolyear,desc) {
    return this.http.post(this.global.api + 'FMIS_ARSetUp/SchoolTerm/'+schoolyear+'/'+desc,'', this.global.option)
  }

  getARSetUpSchoolYear() {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/SchoolTerm', this.global.option)
  }

  getARSetUpSchoolYear_SY(schoolYear) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/SchoolTerm?schoolYear='+schoolYear, this.global.option)
  }

  putARSetUpSchoolYear(schoolYear, data) {
    return this.http.put(this.global.api + 'FMIS_ARSetUp/SchoolTerm/' + schoolYear, data, this.global.option)
  }
// Payment Schedule
  getARSetupPaymentSchedules(department, schoolYear) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/PaymentSchedCurrent?schoolYear='+schoolYear+'&department='+department, this.global.option)
  }

  getARSetupPaymentSchedulesForNew(department, schoolYear) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/PaymentSchedPrev/'+schoolYear+'?department='+department, this.global.option)
  }

  postARSetUpPaymentSched(data) {
    return this.http.post(this.global.api + 'FMIS_ARSetUp/PaymentSched', data, this.global.option)
  }

  putARSetUpPaymentSched(psid, data) {
    return this.http.put(this.global.api + 'FMIS_ARSetUp/PaymentSched/' + psid, data, this.global.option)
  }

  deleteARSetUpPaymentSched(psid) {
    return this.http.delete(this.global.api + 'FMIS_ARSetUp/PaymentSched/' + psid, this.global.option)
  }
// Tuition Fee Rate
  getARSetupTuitionFeeRate(schoolYear, progLevel) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/TuitionFeeRate?schoolYear='+schoolYear+'&programLevel='+progLevel, this.global.option)
  }

  getARSetupTuitionFeeRateForNew(schoolYear,progLevel) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/TuitionFeeRatePrevSY/'+schoolYear+'?programLevel='+progLevel, this.global.option)
  }

  getAllCourses() {
    return this.http.get(this.global.api + 'PublicAPI/Courses', this.global.option)
  }

  postARSetUpTuitionFeeRate(data) {
    return this.http.post(this.global.api + 'FMIS_ARSetUp/TuitionFeeRate', data, this.global.option)
  }

  putARSetUpTuitionFeeRate(tfrid, data) {
    return this.http.put(this.global.api + 'FMIS_ARSetUp/TuitionFeeRate/' + tfrid, data, this.global.option)
  }

  deleteARSetUpTuitionFeeRate(tfrid) {
    return this.http.delete(this.global.api + 'FMIS_ARSetUp/TuitionFeeRate/' + tfrid, this.global.option)
  }
// Fees Rate
  getARSetupFeesRate(schoolYear, ftype) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/FeesRate/'+schoolYear+'/'+ftype, this.global.option)
  }

  getARSetupFeesRateForNew(schoolYear,ftype) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/FeesRatePrevSY/'+schoolYear+'/'+ftype, this.global.option)
  }

  getARSetupFeeType() {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/FeesType', this.global.option)
  }

  postARSetUpFeesRate(data) {
    return this.http.post(this.global.api + 'FMIS_ARSetUp/FeesRate', data, this.global.option)
  }

  putARSetUpFeesRate(frid, data) {
    return this.http.put(this.global.api + 'FMIS_ARSetUp/FeesRate/' + frid, data, this.global.option)
  }

  deleteARSetUpFeesRate(frid) {
    return this.http.delete(this.global.api + 'FMIS_ARSetUp/FeesRate/' + frid, this.global.option)
  }
// Fees
  getARSetupFees(ftype) {
    return this.http.get(this.global.api + 'FMIS_ARSetUp/Fees?typeID='+ftype, this.global.option)
  }

  postARSetUpFees(data) {
    return this.http.post(this.global.api + 'FMIS_ARSetUp/Fees', data, this.global.option)
  }

  putARSetUpFees(frid, data) {
    return this.http.put(this.global.api + 'FMIS_ARSetUp/Fees/' + frid, data, this.global.option)
  }

  deleteARSetUpFees(frid) {
    return this.http.delete(this.global.api + 'FMIS_ARSetUp/Fees/' + frid, this.global.option)
  }
  // Constant Fees
    getARSetupConstFee() {
      return this.http.get(this.global.api + 'FMIS_ARSetUp/CONSTFees', this.global.option)
    }
  
    postARSetUpConstFee(feesid,feesdesc) {
      return this.http.post(this.global.api + 'FMIS_ARSetUp/CONSTFees/'+feesid+'/'+feesdesc,'', this.global.option)
    }
  
    putARSetUpConstFee(frid, data) {
      return this.http.put(this.global.api + 'FMIS_ARSetUp/CONSTFees/' + frid, data, this.global.option)
    }
  
    deleteARSetUpConstFee(frid) {
      return this.http.delete(this.global.api + 'FMIS_ARSetUp/CONSTFees/' + frid, this.global.option)
    }


  //--------------------------------------------------------------------------------------//
}
