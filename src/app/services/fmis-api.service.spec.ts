import { TestBed } from '@angular/core/testing';

import { FmisAPIService } from './fmis-api.service';

describe('FmisAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FmisAPIService = TestBed.get(FmisAPIService);
    expect(service).toBeTruthy();
  });
});
